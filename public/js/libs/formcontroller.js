function SlFormController(wrapper, options) {
    var self = this;
    
    this.MODE_EDIT = 'edit';
    this.MODE_VIEW = 'view';
    
    function SlFormField(form, options) {
        var _self = this;
        
        var _options = _.extend({
            name: '',
            type: 'text',
            roles: ['render'],
            label: '',
            value: '',
            form: form
        }, options || {});
        
        this.getName = function() {
            return _options.name;
        };
        
        this.get = function(what) {
            var method = 'get'+what.ucFirst();
            if(_self.hasOwnProperty(method)) {
                return _self[method]();
            }
            return _options.hasOwnProperty(what)?_options[what]:null;
        };
        
        this.serialize = function() {
            var data = {};
            _.each(_options, function(v, k){
                data[k] = _self.get(k);
            });
            return data;
        };
        
        this.render = function() {
            return _.template(_self.get('form').getTemplate(_self.get('type')), _self.serialize());
        };
    }
    
    var _options = $.extend(true, {
        wrapper: $(wrapper || options.wrapper),
        alias: '',
        templates: {},
        mode: self.MODE_EDIT,
        fields: {
            id: {
                name: 'id',
                roles: ['render'],
                label: 'id',
                type: 'hidden'
            }
        }
    }, self.DEFAULTS, options || {});
    
    this.getWrapper = function() {
        if(!_options.wrapper) {
            throw new SlError('No wrapper given', 200);
        }
        return _options.wrapper;
    };
    
    var _fields = {};
    
    this.addField = function(f) {
        if(!(f instanceof SlFormField)) {
            throw new SlError('field must be instance of SlFormField', 201);
        }
        if(self.fieldExists(f.getName())) {
            throw new SlError('Field named "'+f.getName()+'" already exists', 202);
        }
        _fields[f.getName()] = f;
        return self;
    };
    
    this.createField = function(data) {
        return self.addField(new SlFormField(self, data));
    };
    
    this.fieldExists = function(name) {
        return _fields.hasOwnProperty(name);
    };
    
    this.getField = function(name) {
        return self.fieldExists(name)?_fields[name]:null;
    };
    
    this.getFields = function() {
        return _fields;
    };
    
    // Загружаем данные о полях из настроек
    _.each(_options.fields, function(f) {
        try {
            self.addField(f)
        } catch(e) {
            if(!(e instanceof SlError)) throw e;
            if(e.code == 201) {
                self.createField(f);
            } else if(e.code === 202) {
                // Такое поле уже есть
            } else {
                throw e;
            }
        }
    });
    
    this.getTemplate = function(type) {
        return _options.templates.hasOwnProperty(type)?_options.templates[type]:'';
    };
    
    this.render = function() {
        var $form = self.getWrapper().find('form');
        if(!$form || !$form.length) {
            $form = $('<form />').appendTo(self.getWrapper());
        }
        _.each(self.getFields(), function(field){
            var selector = '[name="'+field.getName()+'"]';
            var $field = $(selector, $form).length;
            if(!$field || !$field.length) {
                $field = $form.append(field.render());
            }
        });
    };
    
    this.loadTemplates = function() {
        $.ajax({
            type: 'POST',
            cache: false,
            url: '/home/main/ajaxformtemplates',
            data: {
                alias: _options.alias
            },
            success: function(data) {
                if(data.result) {
                    _options.templates = _.extend(data.templates, _options.template);
                } else {
                    $.alert(data.description);
                }
            }
        });
    };
    
    self.render();
}

SlFormController.prototype.DEFAULTS = {
    templates: {
        
    }
};

$(document).ready(function() {
    // Загрузка шаблонов форм
    $.ajax({
        type: 'POST',
        cache: false,
        url: '/home/main/ajaxformtemplates',
        success: function(data) {
            if(data.result) {
                SlFormController.prototype.DEFAULTS.templates = data.templates;
            } else {
                console.log(data.description);
            }
        }
    });
});