function SlForms(options) {
    // Singleton
    if(arguments.callee._singletonInstance) {
        return arguments.callee._singletonInstance;
    }
    arguments.callee._singletonInstance = this;
    
    var self = this;
    
    function Form(data) {
        var self = this;
        
        var _options = _.extend({
            alias: null,
            type: null,
            fields: {}
        }, data || {});
        
        this.getAlias = function() {
            return _options.alias;
        };
        
        this.getType = function() {
            return _options.type;
        };
        
        this.getFields = function() {
            return _options.fields;
        };
        
        this.serialize = function() {
            var data = {};
            _.each(_options, function(v, k){
                var method = 'get'+k.ucFirst();
                if(self.hasOwnProperty(method) && (typeof self[method] === 'function')) {
                    data[k] = self[method]();
                }
                return data[k] = v;
            });
            return data;
        };
        
        if(!this.getAlias() || !self.getType()) {
            throw new SlError('alias and type params are required', 502);
        }
        
        _.each(_options.fields, function(data, name){
            if(!data.hasOwnProperty('name')) {
                _options.fields[name].name = name;
            }
        });
    }
    
    var _options = $.extend(true, {
        loadMissed: true
    }, options || {});
    
    var _container = {
        _detailed: {},
    };
    
    this.option = function(name) {
        if(arguments.length > 1) {
            _options[name] = arguments[1];
            return self;
        } else {
            return _options.hasOwnProperty(name)?_options[name]:null;
        }
    };
    
    /**
     * 
     * @param {String} alias
     * @param {String} type
     * @throws {SlError} If unsupported type given
     * @returns {unresolved}
     */
    this.getForm = function(alias, type, loadMissed, options) {
        type = type || '_detailed';
        options = _.extend({
            async: false
        }, options || {});
        if(!_container.hasOwnProperty(type)) {
            throw new SlError('Wrong type given', 500);
        }
        if(_container[type].hasOwnProperty(alias)) {
            return _container[type][alias];
        } else {
            if(!/^[a-zA-Z]+\.[a-zA-Z]+$/.test(alias)) {
                throw new SlError('Wrong alias param given', 501);
            }
            loadMissed = (arguments.length <= 2)?self.option('loadMissed'):loadMissed;
            if(loadMissed) {
                $.ajax({
                    type: 'POST',
                    cache: false,
                    async: options.async,
                    url: '/'+alias.split('.').join('/')+'/ajaxform',
                    data: {
                        fieldset: type
                    },
                    success: function(data) {
                        if(data.result) {
                            self.addForm({
                                alias: alias,
                                type: type,
                                fields: data.fields
                            });
                        } else {
                            console.log(data.description || 'Error quering data');
                        }
                    }
                });
                return self.getForm(alias, type, false);
            }
        }
    };
    
    this.addForm = function(form) {
        if(form instanceof Form) {
            _container[form.getType()][form.getAlias()] = form;
            return self;
        } else {
            if(typeof form === 'object') {
                return self.addForm(new Form(form));
            } else if(typeof form == 'function') {
                return self.addForm(new Form(form(self)));
            } else {
                throw new SlError('Unsupported form param', 503);
            }
        }
    };
    
    this.checkForms = function(forms, options) {
        options = _.extend({
            async: true
        }, options || {});
        _.each(forms, function(data){
            try {
                if(typeof data === 'string') {
                    data = {
                        alias: data
                    };
                }
                self.getForm(data.alias, data.type || false, true, options);
            } catch(e) {
                if(!(e instanceof SlError)) throw e;
                console.log('Check form error: '+e.message+' ('+e.code+')');
            }
        });
    };
}
$(document).ready(function(){
    $('.slGlobalFormsData span').each(function(){
        new SlForms().addForm($(this).data());
    });
});