<?php
return array (
  'model' => 
  array (
    'name' => 
    array (
      'label' => 'Коротко',
      'type' => 'text',
    ),
    'description' => 
    array (
      'label' => 'Подробно',
      'type' => 'text',
      
    ),
    'create' => 
    array (
      'label' => 'Дата создания',
      'type' => 'date',
      
    ),
    'id' => 
    array (
      'label' => 'ID',
      'type' => 'hidden',
    ),
    'active' => 
    array (
      'label' => 'ACTIVE',
      'type' => 'checkbox',
    ),
    'archived' => 
    array (
      'label' => 'ARCHIVED',
      'type' => 'text',
    ),
  ),
);
