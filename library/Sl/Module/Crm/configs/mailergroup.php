<?php
return array (
  'model' => 
  array (
    'name' => 
    array (
      'label' => 'Название',
      'type' => 'text',
      'required' => true,  
    ),
    'description' => 
    array (
      'label' => 'Описание',
      'type' => 'textarea',
    ),
    'create' => 
    array (
      'label' => 'CREATE',
      'type' => 'text',
    ),
    'id' => 
    array (
      'label' => 'ID',
      'type' => 'hidden',
    ),
    'active' => 
    array (
      'label' => 'ACTIVE',
      'type' => 'checkbox',
    ),
    'archived' => 
    array (
      'label' => 'ARCHIVED',
      'type' => 'text',
    ),
  ),
);
