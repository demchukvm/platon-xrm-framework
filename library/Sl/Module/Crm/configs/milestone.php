<?php
return array (
  'model' => 
  array (
    'start' => 
    array (
      'label' => 'START',
      'type' => 'date',
    ),
    'end' => 
    array (
      'label' => 'END',
      'type' => 'date',
    ),
    'name' => 
    array (
      'label' => 'NAME',
      'type' => 'text',
    ),
    'create' => 
    array (
      'label' => 'CREATE',
      'type' => 'text',
    ),
    'id' => 
    array (
      'label' => 'ID',
      'type' => 'text',
    ),
    'active' => 
    array (
      'label' => 'ACTIVE',
      'type' => 'checkbox',
    ),
    'archived' => 
    array (
      'label' => 'ARCHIVED',
      'type' => 'text',
    ),
    'extend' => 
    array (
      'label' => 'EXTEND',
      'type' => 'text',
    ),
    'description' => 
    array (
      'label' => 'DESCRIPTION',
      'type' => 'text',
    ),
    'status' => 
    array (
      'label' => 'STATUS',
      'type' => 'text',
    ),
   
  ),
);
