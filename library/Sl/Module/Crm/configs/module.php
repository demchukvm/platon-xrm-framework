<?php
return array (
  'forms' => 
  array (
    'model_mailergroup_form' => 
    array (
      'modulerelation_mailergroupemail' => 
      array (
        'label' => 'Email',
        'sort_order' => 20,
        'request_fields' => 
        array (
          1 => 'emailemaildetails:name',
          2 => 'emailemaildetails:city',
          3 => 'emailemaildetails:phone',
          4 => 'emailemaildetails:country',
          5 => 'emailemaildetails:company',
          6 => 'emailemaildetails:ballans',
        ),
      ),
      'modulerelation_mailergroupcustomer' => 
      array (
        'label' => 'Клиент',
      ),
      'name' => 
      array (
        'label' => 'Название',
        'type' => 'text',
        'sort_order' => 10,
      ),
      'description' => 
      array (
        'label' => 'Описание',
        'type' => 'textarea',
        'sort_order' => 200,
      ),
    ),
    'model_task_form' => 
    array (
      'modulerelation_taskmailergroup' => 
      array (
        'label' => 'Группы рассылки',
        'request_fields' => 
        array (
          1 => 'name',
          2 => 'description',
        ),
      ),
      'modulerelation_taskmaintag' => 
      array (
        'label' => 'Link with',
        /* 'required' => true,*/
        'sort_order' => 100,
        'strong' => 1,
      ),
      
      'modulerelation_tasktag' => 
      array (
        'label' => 'Tags',
        /* 'required' => true,*/
        'sort_order' => 170,
        'strong' => 1,
      ), 
        
      'modulerelation_milestonetask' => 
      array (
        'label' => 'Milestone',
        /* 'required' => true, */
        'sort_order' => 150,
        'field_filters' => 
        array (
          0 => 
          array (
            'field' => 'milestonemaintag:id',
            'matching' => 'in',
            'value' => 'modulerelation_taskmaintag',
            'strong' => true,
          ),
        
            1 => array (
                'field' => 'archived',
                'matching' => 'eq',
                'value' => 'val:0',
              ), 
          ),
      ),
      'modulerelation_taskuser' => 
      array (
        'label' => 'Assign to',
        'field_filters' => 
        array (
        ),
        'request_fields' => 
        array (
          1 => 'name',
          2 => 'login',
          3 => 'email',
          4 => 'userroles:name',
        ),
      ),
      'modulerelation_taskfile' => 
      array (
        'label' => 'Прикрепить файл',
      ),
      'modulerelation_taskemail' => 
      array (
        'label' => 'Email',
        'request_fields' => 
        array (
          0 => 'emailemaildetails:name',
          1 => 'emailemaildetails:city',
          2 => 'emailemaildetails:phone',
          3 => 'emailemaildetails:country',
          4 => 'emailemaildetails:company',
          5 => 'emailemaildetails:ballans',
        ),
      ),
      'modulerelation_tasknote' => 
      array (
        'label' => 'Заметки',
        'include_decorator' => 'FilesListTable',
        'show_field' => 
        array (
          0 => 'description',
          1 => 'create',
        ),
        'show_field_label' => 
        array (
          0 => 'Событие',
          1 => 'Результат',
          2 => 'Дата',
        ),
      ),
    ),
    'model_note_form' => 
    array (
      'deascription' => 
      array (
        'class' => 'autosize',
      ),
    ),
  ),
  'modulerelations' => 
  array (
    0 => 
    array (
      'type' => '21',
      'db_table' => 'Sl\\Module\\Crm\\Modulerelation\\Table\\Taskuser',
    ),
    1 => 
    array (
      'type' => '2',
      'db_table' => 'Sl\\Module\\Crm\\Modulerelation\\Table\\Tasknote',
    ),
    2 => 
    array (
      'type' => '22',
      'db_table' => 'Sl\\Module\\Crm\\Modulerelation\\Table\\Taskemail',
    ),
    3 => 
    array (
      'type' => '4',
      'db_table' => 'Sl\\Module\\Crm\\Modulerelation\\Table\\Taskfile',
    ),
    4 => 
    array (
      'type' => '22',
      'db_table' => 'Sl\\Module\\Crm\\Modulerelation\\Table\\Mailergroupemail',
    ),
    5 => 
    array (
      'type' => '22',
      'db_table' => 'Sl\\Module\\Crm\\Modulerelation\\Table\\Taskmailergroup',
    ),
    6 => 
    array (
      'type' => '22',
      'db_table' => 'Sl\\Module\\Crm\\Modulerelation\\Table\\Mailergroupcustomer',
    ),
    7 => 
    array (
      'type' => '22',
      'db_table' => 'Sl\\Module\\Crm\\Modulerelation\\Table\\Mailgrouplead',
    ),
    8 => 
    array (
      'type' => '12',
      'db_table' => 'Sl\\Module\\Crm\\Modulerelation\\Table\\Milestonetask',
      'options' => 
      array (
      ),
    ),
    9 => 
    array (
      'type' => '21',
      'db_table' => 'Sl\\Module\\Crm\\Modulerelation\\Table\\Taskmaintag',
      'options' => 
      array (
      ),
    ),
    10 => 
    array (
      'type' => '21',
      'db_table' => 'Sl\\Module\\Crm\\Modulerelation\\Table\\Milestonemaintag',
      'options' => 
      array (
      ),
    ),
    11 => 
    array (
      'type' => '12',
      'db_table' => 'Sl\\Module\\Crm\\Modulerelation\\Table\\Relatednexttask',
      'options' => 
      array (
      ),
    ),
    12 => 
    array (
      'type' => '21',
      'db_table' => 'Sl\\Module\\Crm\\Modulerelation\\Table\\Milestoneuser',
      'options' => 
      array (
      ),
    ),
  ),
  'lists' => 
  array (
    'task_type' => 
    array (
      1 => 'Напоминание',
      2 => 'Задача',
      3 => 'Рассылка',
    ),
    'task_status' => 
    array (
      1 => 'Черновик',
      2 => 'В работе',
      3 => 'Выполнена',
    ),
    'task_remind' => 
    array (
      1 => '1 мин.',
      5 => '5 мин.',
      15 => '15 мин.',
      30 => '30 мин.',
      60 => '1 час',
      120 => '2 часа',
    ),
  ),
  'listview_options' => 
  array (
    'task' => 
    array (
      'name' => 
      array (
        'order' => 10,
        'label' => 'Название',
        'searchable' => true,
        'sortable' => true,
      ),
      'task_set' => 
      array (
        'order' => 20,
        'label' => 'Задача',
      ),
      'type' => 
      array (
        'order' => 30,
        'label' => 'Тип',
        'searchable' => true,
        'select' => true,
        'sortable' => true,
      ),
      'status' => 
      array (
        'order' => 40,
        'label' => 'Status',
        'searchable' => true,
        'select' => true,
        'sortable' => true,
      ),
      'description' => 
      array (
        'order' => 50,
        'label' => 'Description',
      ),
      'remind' => 
      array (
        'order' => 60,
        'label' => 'Напомнить за',
        'searchable' => true,
        'select' => true,
        'sortable' => true,
      ),
      'taskuser.name' => 
      array (
        'order' => 65,
        'label' => 'Ответственный',
      ),
      'date_start' => 
      array (
        'order' => 70,
        'label' => 'Start date',
      ),
      'date_finish' => 
      array (
        'order' => 80,
        'label' => 'Deadline',
        'type' => 'datetime',
        'sortable' => true,
        'searchable' => true,
      ),
      'tasknote.name' => 
      array (
        'order' => 90,
        'label' => 'Последняя заметка',
      ),
      'archived' => 
      array (
        'order' => 1000,
        'label' => 'ARCHIVED',
      ),
    ),
    'note' => 
    array (
      'name' => 
      array (
        'order' => 10,
        'label' => 'Название',
      ),
      'description' => 
      array (
        'order' => 20,
        'label' => 'Описание',
      ),
      'archived' => 
      array (
        'order' => 30,
        'label' => 'ARCHIVED',
      ),
    ),
    'mailergroup' => 
    array (
      'name' => 
      array (
        'order' => 10,
        'label' => 'Название',
      ),
      'description' => 
      array (
        'order' => 20,
        'label' => 'Описание',
      ),
      'archived' => 
      array (
        'order' => 30,
        'label' => 'ARCHIVED',
      ),
    ),
    'mailstone' => 
    array (
      'name' => 
      array (
        'order' => 10,
        'label' => 'Name',
      ),
      'start' => 
      array (
        'order' => 20,
        'label' => 'Start',
      ),
      'end' => 
      array (
        'order' => 30,
        'label' => 'End',
      ),
    ),
  ),
  'detailed' => 
  array (
    'task' => 
    array (
      'modulerelation_taskmailergroup' => 
      array (
        'label' => 'Группы рассылки',
        'request_fields' => 
        array (
          1 => 'name',
          2 => 'description',
        ),
      ),
      'modulerelation_taskuser' => 
      array (
        'label' => 'Ответственный',
        'field_filters' => 
        array (
          0 => 
          array (
            'field' => 'userroles:id',
            'matching' => 'in',
            'value' => 'val:33,31,1,29',
          ),
        ),
        'request_fields' => 
        array (
          1 => 'name',
          2 => 'login',
          3 => 'email',
          4 => 'userroles:name',
        ),
      ),
      'modulerelation_taskfile' => 
      array (
        'label' => 'Прикрепить файл',
      ),
      'modulerelation_taskemail' => 
      array (
        'label' => 'Email',
        'request_fields' => 
        array (
          0 => 'emailemaildetails:name',
          1 => 'emailemaildetails:city',
          2 => 'emailemaildetails:phone',
          3 => 'emailemaildetails:country',
          4 => 'emailemaildetails:company',
          5 => 'emailemaildetails:ballans',
        ),
      ),
      'modulerelation_tasknote' => 
      array (
        'label' => 'Заметки',
        'include_decorator' => 'FilesListTable',
        'show_field' => 
        array (
          0 => 'description',
          1 => 'create',
        ),
        'show_field_label' => 
        array (
          0 => 'Событие',
          1 => 'Результат',
          2 => 'Дата',
        ),
      ),
    ),
  ),
  'navigation_pages' => 
  array (
    0 => 
    array (
      'label' => 'CRM',
      'order' => 70,
      'icon' => 'calendar',
      'id' => 'crm',
    ),
    1 => 
    array (
      'module' => 'crm',
      'controller' => 'mailergroup',
      'action' => 'list',
      'label' => 'Группы рассылки',
      'parent' => 'crm',
    ),
    2 => 
    array (
      'module' => 'crm',
      'controller' => 'note',
      'action' => 'list',
      'label' => 'Заметки',
      'parent' => 'crm',
    ),
  ),
  'left_navigation_pages' => 
  array (
    1 => 
    array (
      'module' => 'crm',
      'controller' => 'task',
      'action' => 'list',
      'label' => 'Tasks',
      'id' => 'tasks',
      'order' => 40,
      'icon' => 'fa fa-tasks  fa-lg',
    ),
    2 => 
    array (
      'module' => 'crm',
      'controller' => 'milestone',
      'action' => 'list',
      'label' => 'Milestones',
      'id' => 'tasks',
      'order' => 40,
      'icon' => 'fa fa-clock-o  fa-lg',
    ),
  ),
);
