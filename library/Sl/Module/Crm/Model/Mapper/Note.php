<?php
namespace Sl\Module\Crm\Model\Mapper;

class Note extends \Sl_Model_Mapper_Abstract {
	protected function _getMappedDomainName() {
        return '\Sl\Module\Crm\Model\Note';
    }

    protected function _getMappedRealName() {
        return '\Sl\Module\Crm\Model\Table\Note';
    }
}