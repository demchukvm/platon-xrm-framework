<?php

namespace Sl\Module\Crm\Model\Mapper;

class Task extends \Sl_Model_Mapper_Abstract {

    protected $_custom_mandatory_fields = array('date_start', 'date_finish', 'description', 'name', 'status', 'projectorder');

    protected function _getMappedDomainName() {
        return '\Sl\Module\Crm\Model\Task';
    }

    protected function _getMappedRealName() {
        return '\Sl\Module\Crm\Model\Table\Task';
    }

    public function findActiviteTask(\Sl\Module\Crm\Model\Task $task) {

        $current_user = \Zend_Auth::getInstance()->getIdentity();
        $user_id = $current_user->getId();
        $taskuser_relation = \Sl_Modulerelation_Manager::getRelations($task, 'taskuser');
        if (!$taskuser_relation) {
            throw new \Exception('Can\'t find "taskuser" relation');
        }
        $data = ($current_user) ? $this->_getDbTable()->findActiviteTask($user_id, $taskuser_relation) : 0;

        foreach ($data as $row) {
            $rows[$row['type']][] = $row;
        }
        return $rows;
    }

    public function calculateTaskLength(\Sl\Module\Crm\Model\Task $task) {

        $date_start = $task->getDateStart(true);
        $date_finish = $task->getDateFinish(true);

        $h_mapper = \Sl_Model_Factory::mapper('holiday', 'crm');

        $length = intval($date_finish->diff($date_start)->format('%d'));

        if ($h_mapper) {
            
            return $h_mapper->countWorkDays($date_start, $date_finish);
        }
        return $length;
    }

    public function calculateTaskDateFinish(\Sl\Module\Crm\Model\Task $task, $days) {

        $date_start = $task->getDateStart(true);
        $h_mapper = \Sl_Model_Factory::mapper('holiday', 'crm');
        $date_finish = clone $date_start;
        $date_finish = $h_mapper ? $h_mapper->getWorkEnd($date_start, $days) : $date_finish->add(new \DateInterval('P' . $days . 'D'));
        
        $task ->setDateFinish($date_finish);
        return $task;
    }
    
    
    public function getMaxTaskOrderForMilestone($milestone){
        $order = 0;
        $relation = \Sl_Modulerelation_Manager::getRelations($this->_model(), 'milestonetask');
        if ($relation instanceof \Sl\Modulerelation\Modulerelation){
            $order = $this->_getDbTable() -> findMaxOrderByRelation ($milestone, $relation);
        }
        return $order + 10;
    }
}
