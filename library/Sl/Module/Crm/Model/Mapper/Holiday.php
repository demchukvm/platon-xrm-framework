<?php

namespace Sl\Module\Crm\Model\Mapper;

class Holiday extends \Sl_Model_Mapper_Abstract {

    protected function _getMappedDomainName() {
        return '\Sl\Module\Crm\Model\Holiday';
    }

    protected function _getMappedRealName() {
        return '\Sl\Module\Crm\Model\Table\Holiday';
    }
    
    public function countWorkDays(\DateTime $date_start, \DateTime $date_finish){
        
        $length = intval($date_finish->diff($date_start)->format('%d'));
        $free_days = 0;
        //print_r(array($date_start, $date_finish));
        if ($length) {
            
            $day = $date_start->format('N');
            $free_days = floor($length / 7)  * 2;
            //$tail = $length % 7;
            $f = $date_finish->format('N');
            $f = $f==7?8:$f;
            if ($f > $day) {
                if (($f-$d) > 1 && $f == 8){
                    $free_days +=2;
                } elseif($f == 7) {
                    $free_days +=1;
                }
                
            } elseif($f < $day){
                if ($day == 7) $free_days +=1;
                else $free_days +=2;
            }
            
            if ($h_mapper){
                $holidays = $this->getPlaydays($date_start, $date_finish);
                $free_days += count($holidays);
            }
         //   print_r(array('countWorkDays',$date_start, $date_finish, $length, $free_days, $f, $day));
        }    
        return ($length - $free_days);
    }
    
    
    public function getWorkEnd(\DateTime $date_start, $days){
       
        $free_days = 0;
        $date_end = clone $date_start;
        if ($days) {
            $day = $date_start->format('N');
            $whole_days = floor($days / 5)  * 7;
            $tail = $days % 5;
            $whole_days += intval($tail);
            
            if (($day == 7 && $tail) || ($day + $tail) == 7) {
               
                $whole_days +=2;
            } elseif( ($tail + $day) > 6){
               
                $whole_days +=3;
            }
            
          
            $date_end -> add(new \DateInterval('P'.$whole_days.'D'));
            
            $h = $this->getPlaydays($date_start, $date_end);
            
            //Якщо в цей період випали свята, обрахувати новий день кінця
            if (count($h)) {
                $date_end = $this ->getWorkEnd($date_end, count($h));
            }
        }    
        //print_r(array($date_start, $days, $whole_days, $date_end, $tail, $day));
        return $date_end;
    }
    
    public function getPlaydays(\DateTime $date_start, \DateTime $date_finish) {
        return $this->fetchAll(' WEEKDAY(date) < 5 AND date BETWEEN "'.$date_start->format(\Sl_Model_Abstract::FORMAT_DATE).'" AND "'.$date_finish->format(\Sl_Model_Abstract::FORMAT_DATE).'" ');
    }
    
}
