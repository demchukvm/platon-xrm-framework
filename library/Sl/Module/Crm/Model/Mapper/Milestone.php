<?php

namespace Sl\Module\Crm\Model\Mapper;

class Milestone extends \Sl_Model_Mapper_Abstract {

    protected $_custom_mandatory_fields = array('start', 'end', 'description', 'name', 'status', 'modulerelation_milestoneuser', 'projectorder');

    protected function _getMappedDomainName() {
        return '\Sl\Module\Crm\Model\Milestone';
    }

    protected function _getMappedRealName() {
        return '\Sl\Module\Crm\Model\Table\Milestone';
    }

    public function recalcBorders(\Sl\Module\Crm\Model\Milestone $milestone, $save = false) {

        $task_db = \Sl_Model_Factory::dbTable('task', 'crm');
        $relation = \Sl_Modulerelation_Manager::getRelations($milestone, 'milestonetask');

        if ($task_db instanceof \Zend_Db_Table_Abstract && $relation instanceof \Sl\Modulerelation\Modulerelation) {

            $arr = $task_db->findBordersByMilestone($milestone, $relation);
            
            $arr = is_object($arr) ? (array) $arr : $arr;
            if (isset($arr['start']) && isset($arr['finish'])) {
                try {
                    $start = date_create_from_format($milestone::FORMAT_TIMESTAMP, $arr['start']);
                    $finish = date_create_from_format($milestone::FORMAT_TIMESTAMP, $arr['finish']);
                    $to_edit = false;
                    
                    if ($milestone->getStart(true) > $start) {
                        $milestone->setStart($start);
                        $to_edit = true;
                    }
                    if ($milestone->getEnd(true) < $finish) {
                        $milestone->setEnd($finish);
                        $to_edit = true;
                    }
                    
                    $to_edit && $save && $milestone = $this->save($milestone, true);
                } catch (\Exception $e) {
                    print_r($e->getMessage());
                    print_r($e->getTrace);
                }
            }
        }
        return $milestone;
    }
    
    public function getMaxMilestoneOrderForTag ($tag) {
        $order = 0;
        $relation = \Sl_Modulerelation_Manager::getRelations($this->_model(), 'milestonemaintag');
        if ($relation instanceof \Sl\Modulerelation\Modulerelation){
            $order = $this->_getDbTable() -> findMaxOrderByRelation ($tag, $relation);
        }
        return $order + 10; 
    }
   
}
