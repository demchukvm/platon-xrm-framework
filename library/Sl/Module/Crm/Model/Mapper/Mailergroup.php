<?php
namespace Sl\Module\Crm\Model\Mapper;

class Mailergroup extends \Sl_Model_Mapper_Abstract {
	protected function _getMappedDomainName() {
        return '\Sl\Module\Crm\Model\Mailergroup';
    }

    protected function _getMappedRealName() {
        return '\Sl\Module\Crm\Model\Table\Mailergroup';
    }
}