<?php

namespace Sl\Module\Crm\Model;

class Task extends \Sl_Model_Abstract implements \Sl\Module\Crm\Addition\Calendardata {

    protected $_name;
    protected $_projectorder;
    protected $_task_set;
    protected $_type;
    protected $_status;
    protected $_description;
    protected $_remind;
    protected $_date_start;
    protected $_date_finish;
    protected $_mail_topic;
    protected $_mail_body;
    protected $_send_email;
    protected $_lists = array(
        /*     'status' => 'task_status', */
        'remind' => 'task_remind',
        'type' => 'task_type',
    );

    public function setProjectorder($projectorder) {
        $this->_projectorder = $projectorder;
        return $this;
    }

    public function setName($name) {
        $this->_name = $name;
        return $this;
    }

    public function setMailBody($mail_body) {
        $this->_mail_body = $mail_body;
        return $this;
    }

    public function setSendEmail($send_email) {
        $this->_send_email = $send_email;
        return $this;
    }

    public function setMailTopic($mail_topic) {
        $this->_mail_topic = $mail_topic;
        return $this;
    }

    public function setTaskSet($task_set) {
        $this->_task_set = $task_set;
        return $this;
    }

    public function setType($type) {
        $this->_type = $type;
        return $this;
    }

    public function setStatus($status) {
        $this->_status = $status;
        return $this;
    }

    public function setDescription($description) {
        $this->_description = $description;
        return $this;
    }

    public function setRemind($remind) {
        $this->_remind = $remind;
        return $this;
    }

    public function setDateStart($date_start) {

        $this->_date_start = $date_start instanceof \DateTime ? $date_start->format(self::FORMAT_TIMESTAMP) : $date_start;
        return $this;
    }

    public function setDateFinish($date_finish) {
        $this->_date_finish = $date_finish instanceof \DateTime ? $date_finish->format(self::FORMAT_TIMESTAMP) : $date_finish;
        return $this;
    }

    public function getName() {
        return $this->_name;
    }

    public function getMailBody() {
        return $this->_mail_body;
    }

    public function getSendEmail() {
        return $this->_send_email;
    }

    public function getMailTopic() {
        return $this->_mail_topic;
    }

    public function getTaskSet() {
        return $this->_task_set;
    }

    public function getType() {
        return $this->_type;
    }

    public function getStatus() {
        return $this->_status;
    }

    public function getDescription() {
        return $this->_description;
    }

    public function getRemind() {
        return $this->_remind;
    }

    public function getDateStart($as_object = null) {
        if ($as_object) {
            return \DateTime::createFromFormat(self::FORMAT_TIMESTAMP, $this->getDateStart());
        }
        return $this->_date_start;
    }

    public function getDateFinish($as_object = null) {
        if ($as_object) {
            return \DateTime::createFromFormat(self::FORMAT_TIMESTAMP, $this->getDateFinish());
        }
        return $this->_date_finish;
    }

    public function fetchCompleteness() {

        return $this->_status;
    }

    public function fetchFinalDate($as_object = false) {
        if ($as_object) {
            return \DateTime::createFromFormat(self::FORMAT_TIMESTAMP, $this->getDateFinish());
        }

        return $this->_date_finish;
    }

    public function fetchName() {

        return $this->_name;
    }

    public function fetchStartDate($as_object = false) {
        if ($as_object) {
            return \DateTime::createFromFormat(self::FORMAT_TIMESTAMP, $this->getDateStart());
        }
        return $this->_date_start;
    }

    public function getProjectorder() {
        return $this->_projectorder;
    }

}
