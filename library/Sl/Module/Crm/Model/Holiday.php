<?php
namespace Sl\Module\Crm\Model;

class Holiday extends \Sl_Model_Abstract {

	protected $_name;
	protected $_date;
	protected $_loged = false;

	public function setName ($name) {
		$this->_name = $name;
		return $this;
	}
	
	public function setDate($date) {
		if($date instanceof \DateTime) {
			$date = $date->format(self::FORMAT_DATE);
		}
		$this->_date = $date;
		return $this;
	}

	public function getName () {
		return $this->_name;
	}
	
	public function getDate($as_object = false) {
		if($as_object) {
			return \DateTime::createFromFormat(self::FORMAT_DATE, $this->getDate());
		}
		return $this->_date;
	}



}