<?php

namespace Sl\Module\Crm\Model;

class Milestone extends \Sl_Model_Abstract implements \Sl\Module\Crm\Addition\Calendardata {

    protected $_start;
    protected $_projectorder;
    protected $_status;
    protected $_description;
    protected $_end;
    protected $_name;
    protected $__today;
    protected $__date_start;
    protected $__date_end;

    const LEVEL_CALM = 0;
    const LEVEL_WARN = 50;
    const LEVEL_ALARM = 100;

    public function setProjectorder($projectorder) {
        $this->_projectorder = $projectorder;
        return $this;
    }

    public function assessLevel(\DateTime $date = null) {
        $date = $date instanceof \DateTime ? $date : $this->_getToday();
        if ($this->getStatus() == 100 || $this->getStart(true) > $date) {
            return self::LEVEL_CALM;
        }

        if ($this->getStart(true) <= $this->_getToday() && $this->getEnd(true) > $date) {
            return self::LEVEL_WARN;
        }
        return self::LEVEL_ALARM;
    }

    protected function _getToday() {
        if (!$this->__today instanceof \DateTime) {
            $this->__today = new \DateTime;
        }
        return $this->__today;
    }

    public function setStatus($status) {
        $this->_status = $status;
        return $this;
    }

    public function setDescription($description) {
        $this->_description = $description;
        return $this;
    }

    public function setStart($start) {
        $this->__date_start = null;
        if ($start instanceof \DateTime) {
            $this->__date_start = $start;
            $start = $start->format(self::FORMAT_DATE);
        }
        $this->_start = $start;
        return $this;
    }

    public function setEnd($end) {
        $this->__date_end = null;
        if ($end instanceof \DateTime) {
            $this->__date_end = $end;
            $end = $end->format(self::FORMAT_DATE);
        }
        $this->_end = $end;
        return $this;
    }

    public function setName($name) {
        $this->_name = $name;
        return $this;
    }

    public function getStart($as_object = false) {
        if ($as_object) {
            if (!$this->__date_start instanceof \DateTime) {
                $this->__date_start = strlen($this->getStart()) > 10 ? \DateTime::createFromFormat(self::FORMAT_TIMESTAMP, $this->getStart()) : \DateTime::createFromFormat(self::FORMAT_DATE, $this->getStart());
            }
            return $this->__date_start;
        }
        return $this->_start;
    }

    public function getEnd($as_object = false) {
        if ($as_object) {
            if (!$this->__date_end instanceof \DateTime) {
                $this->__date_end = strlen($this->getEnd()) > 10 ? \DateTime::createFromFormat(self::FORMAT_TIMESTAMP, $this->getEnd()) : \DateTime::createFromFormat(self::FORMAT_DATE, $this->getEnd());
            }
            return $this->__date_end;
        }
        return $this->_end;
    }

    public function getName() {
        return $this->_name;
    }

    public function fetchCompleteness() {
        
    }

    public function fetchFinalDate($as_object = false) {

        if ($as_object) {
            return \DateTime::createFromFormat(self::FORMAT_DATE, $this->getEnd());
        }
        return $this->_end;
    }

    public function fetchName() {
        return $this->_name;
    }

    public function fetchStartDate($as_object = false) {
        if ($as_object) {
            return \DateTime::createFromFormat(self::FORMAT_DATE, $this->getStart());
        }
        return $this->_start;
    }

    public function getDescription() {
        return $this->_description;
    }

    public function getStatus() {
        return $this->_status;
    }

    public function getProjectorder() {
        return $this->_projectorder;
    }

}
