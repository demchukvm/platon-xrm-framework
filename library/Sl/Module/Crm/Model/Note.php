<?php
namespace Sl\Module\Crm\Model;

class Note extends \Sl_Model_Abstract {

	protected $_name;
	protected $_description;

	public function setName ($name) {
		$this->_name = $name;
		return $this;
	}
	public function setDescription ($description) {
		$this->_description = $description;
		return $this;
	}

	public function getName () {
		return $this->_name;
	}
	public function getDescription () {
		return $this->_description;
	}



}