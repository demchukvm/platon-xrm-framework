<?php
namespace Sl\Module\Crm\Model\Table;

class Milestone extends \Sl\Model\DbTable\DbTable {
	protected $_name = 'crm_milestone';
	protected $_primary = 'id';


    public function findMaxOrderByRelation(\Sl_Model_Abstract $object, \Sl\Modulerelation\Modulerelation $relation) {
        $select = $this->getAdapter()->select();
        $select->from($this->info('name'), array(new \Zend_Db_Expr('MAX( '.$this->info('name').'.projectorder ) as ord')));
        $select = $this->_buildInnerJoin($select, $object, $relation);
        $select -> where($this->info('name').'.archived < 1 ');
        $select -> where($relation->getName().'.id = '.$object->getId());
        $select -> where($relation->getName().'.active > 0 ');
        $select -> where($this->info('name').'.active > 0 ');
        
        return $this->getAdapter()->fetchOne($select);
    }
        
        
}