<?php

namespace Sl\Module\Crm\Model\Table;

class Task extends \Sl\Model\DbTable\DbTable {

    protected $_name = 'crm_task';
    protected $_primary = 'id';

    public function findActiviteTask($user_id, \Sl\Modulerelation\Modulerelation $taskuser_relation) {

        $select = $this->getAdapter()->select();

        $select->from($this->_name);

        $task = \Sl_Model_Factory::object($this);
        $user = $taskuser_relation->getRelatedObject($task);

        $select = $this->_buildInnerJoin($select, $user, $taskuser_relation);

        $select->where($taskuser_relation->getName() . '.id = ?', $user_id);
        $select->where('status = ?', 2);
        $select->where("type in ('1','2')");
        $select->where("date_start < DATE_ADD(NOW(),INTERVAL remind MINUTE)");


        return $this->getAdapter()->fetchAll($select, array(), \Zend_Db::FETCH_ASSOC);
    }

    public function findBordersByMilestone(\Sl\Module\Crm\Model\Milestone $milestone, \Sl\Modulerelation\Modulerelation $relation) {
        $select = $this->getAdapter()->select();
        $select->from($this->info('name'), array(new \Zend_Db_Expr('MIN( '.$this->info('name').'.date_start ) AS start'), new \Zend_Db_Expr('MAX( '.$this->info('name').'.date_finish ) AS finish')));
        $select = $this->_buildInnerJoin($select, $milestone, $relation);
        $select -> where($this->info('name').'.archived < 1 ');
        $select -> where('milestonetask.id = '.$milestone->getId());
        $select -> where('milestonetask.active > 0 ');
        $select -> where($this->info('name').'.active > 0 ');
        
        return $this->getAdapter()->fetchRow($select);
    }
    
    public function findMaxOrderByRelation(\Sl_Model_Abstract $object, \Sl\Modulerelation\Modulerelation $relation) {
        $select = $this->getAdapter()->select();
        $select->from($this->info('name'), array(new \Zend_Db_Expr('MAX( '.$this->info('name').'.projectorder ) as ord')));
        $select = $this->_buildInnerJoin($select, $object, $relation);
        $select -> where($this->info('name').'.archived < 1 ');
        $select -> where($relation->getName().'.id = '.$object->getId());
        $select -> where($relation->getName().'.active > 0 ');
        $select -> where($this->info('name').'.active > 0 ');
        
        return $this->getAdapter()->fetchOne($select);
    }
    
}
