<?php

namespace Sl\Module\Crm\Controller;

class Task extends \Sl_Controller_Model_Action {

    /**
     * Актион отправки рассылки
     */
    public function sendmailerAction() {

        $Obj = \Sl_Model_Factory::mapper($this->getModelName(), $this->_getModule())->findAllowExtended($this->getRequest()->getParam('id', 0));
        if (!$Obj) {
            if (false === $this->getRequest()->getParam('id', false)) {
                $Obj = \Sl_Model_Factory::object($this->getModelName(), $this->_getModule());
                $Obj = \Sl_Model_Factory::mapper($Obj)->prepareNewObject($Obj);
            } else
                throw new \Sl_Exception_Model('Illegal ' . $this->getModelName() . ' id');
        }
        $taskmailergroups = $Obj->fetchRelated('taskmailergroup');
        foreach ($taskmailergroups as $taskmailergroup) {
            $mailergroup = \Sl_Model_Factory::mapper($taskmailergroup->findModelName(), $taskmailergroup->findModuleName())->findExtended($taskmailergroup->getId(), array('mailergroupcustomer'));

            $customers = $mailergroup->fetchRelated('mailergroupcustomer');

            foreach ($customers as $customer) {
                $customer = \Sl_Model_Factory::mapper($customer->findModelName(), $customer->findModuleName())->findExtended($customer->getId(), array('customeremails'));
                $emails = $customer->fetchRelated('customeremails');

                foreach ($emails as $email) {
                    $em[$email->getId()] = \Sl_Model_Factory::mapper($email->findModelName(), $email->findModuleName())->find($email->getId());
                }
            }
        }

        $mail = new \Zend_Mail('UTF-8');

        foreach ($em as $email) {
            $mail->addTo($email->getMail());
        }

        foreach ($Obj->fetchRelated('taskfile') as $key => $value) {
            // print_r();
            $at = new \Zend_Mime_Part(file_get_contents($value->getLocation()));
            $at_header = $at->getHeadersArray();

            $at->type = $at_header[0][1];
            $at->disposition = \Zend_Mime::DISPOSITION_ATTACHMENT;
            $at->encoding = \Zend_Mime::ENCODING_BASE64;
            $at->filename = basename($value);
            $mail->addAttachment($at);
        }

        $mail->setSubject($Obj->mail_topic);
        $mail->setBodyHtml($Obj->mail_body);
        $mail->send();

        $Obj->setStatus(3);
        \Sl_Model_Factory::mapper($Obj)->save($Obj, false, false);

        $forward_url = '/crm/task/edit/id/' . $Obj->getId();
        $this->_redirect($forward_url);
    }

    public function ajaxtaskactiviteAction() {


        $field_resource_taskactivite = \Sl_Service_Acl::joinResourceName(array(
                    'type' => \Sl_Service_Acl::RES_TYPE_MVC,
                    'module' => 'crm',
                    'controller' => 'task',
                    'action' => 'edit'
        ));

        $priv_access_taskactivite = \Sl_Service_Acl::isAllowed($field_resource_taskactivite, \Sl_Service_Acl::PRIVELEGE_ACCESS);

        if ($priv_access_taskactivite && !$this->view->is_iframe) {

            $task_model = \Sl_Model_Factory::object('\Sl\Module\Crm\Model\Task');
            $task_data = \Sl_Model_Factory::mapper($task_model)->findActiviteTask($task_model);
            $this->view->data = $task_data;
            $this->view->content_task = $this->view->render('task/task.phtml');
            $this->view->count_task = count($task_data[2]);
            $this->view->content_remind = $this->view->render('task/remind.phtml');
            $this->view->count_remind = count($task_data[1]);
        }
    }

    public function ajaxopencloseAction() {
        $result = true;
        try {
            $Obj = \Sl_Model_Factory::mapper($this->getModelName(), $this->_getModule())->find($this->getRequest()->getParam('id', 0));
            if ($Obj) {
               $Obj-> setStatus( $Obj->getStatus() < 100 ? 100 : 0 ) ;
               \Sl_Model_Factory::mapper($Obj) -> save($Obj);
            } else {
                $result = false;
                $this->view->data = 'Can not find Object';
            }
        } catch (\Exception $e) {
            $result = false;
            $this->view->data = $e->getMessage();
            $this->view->trace = $e->getTrace();
        }
        
        $this -> view -> result = $result;
        
    }
    
    

}
