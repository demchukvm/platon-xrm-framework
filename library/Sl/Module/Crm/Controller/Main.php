<?php

namespace Sl\Module\Crm\Controller;

class Main extends \Sl_Controller_Action {

    const NEXT_TASK_RELATION = 'relatednexttask';
    const TASK_MILESTONE_RELATION = 'milestonetask';
    const MILESTONEUSER_RELATION = 'milestoneuser';
    const TASKUSER_RELATION = 'taskuser';

    /**
     * Екшн створений для того, щоб для кожного системного email-a  наповнити дані з customeremails у сутність 
     * emaildetails
     */
    protected $_base_tag_id;

    public function recoveryemailAction() {

        $email_obj = \Sl_Model_Factory::object('email', 'home');
        $emailemaildetails_rel = \Sl_Modulerelation_Manager::getRelations($email_obj, 'emailemaildetails');
        if (!($emailemaildetails_rel instanceof \Sl\Modulerelation\Modulerelation))
            throw new \Exception('There is no emailemaildetails relation');
        $options = $emailemaildetails_rel->getOption('customeremails');
        if (!is_array($options))
            throw new \Exception('There is no emailemaildetails options for customeremails');
        $emails = \Sl_Model_Factory::mapper('email', 'home')->fetchAll();
        $i = 0;
        foreach ($emails as $email) {
            echo $i++ . ' ';
            $email = \Sl_Model_Factory::mapper('email', 'home')->findRelation($email, 'customeremails');
            $email = \Sl_Model_Factory::mapper('email', 'home')->findRelation($email, 'emailemaildetails');

            if (!count($email->fetchRelated('customeremails'))) {
                $email = \Sl_Model_Factory::mapper('email', 'home')->findRelation($email, 'contactemail');
                if (!count($email->fetchRelated('contactemail'))) {
//і нафіга тут цей if?
                }
                echo 'continue' . $email . '<br>';
                continue;
            }

            if (count($email->fetchRelated('emailemaildetails'))) {

                echo 'continue 2 ' . $email . '<br>';
                continue;
            }

            echo $email . '<br>';

            $obj = $email->fetchOneRelated('customeremails');
            $emaildetails_obj = \Sl_Model_Factory::object('\Sl\Module\Home\Model\Emaildetails');

            foreach ($options as $set_val => $get_val) {
                $emaildetails_obj->{$emaildetails_obj->buildMethodName($set_val, 'set')}(\Sl\Module\Home\Listener\Emaildetails::getDataForSet($obj, $get_val));
            }

            $emaildetails_obj->assignRelated('emailemaildetails', array($email));

            try {

                \Sl_Model_Factory::mapper($emaildetails_obj)->save($emaildetails_obj, false, false);
            } catch (\Exception $e) {
                print_r($e);
            }
        }
        die('ok');
    }

    protected function _savecalendarevent() {

        $event = $this->getRequest()->getParam('edited_event', FALSE);
        if ($event) {

            $sourse_array = \Sl\Service\CalendarConfig::read()->toArray();
            $start = $sourse_array[$event['alias']]['start'];
            $end = $sourse_array[$event['alias']]['end'];
            $object = \Sl\Service\Helper::getModelByAlias($event['alias']);

            $object = \Sl_Model_Factory::mapper($object)->find($event['id']);
            $start_set = $object->buildMethodName($start, 'set');
            $end_set = $object->buildMethodName($end, 'set');
            $object->{$start_set}($event['start']);
            $object->{$end_set}($event['end']);
            \Sl_Model_Factory::mapper($object)->save($object);
        }
    }

    public function ajaxcalendarAction() {

        $this->view->result = true;
        //$base_tag_id = $this->getRequest()->getParam('tag_id', '');
        try {
            $base_tag_id = $this->getRequest()->getParam('tag_id', '');
            self::_savecalendarevent();


            $source_array = array();
            $result_config = array();

            $source_array = \Sl\Service\CalendarConfig::read()->toArray();

            foreach ($source_array as $alias => $values) {

                $alias_config = self::_getCalendarConfig($alias, $values);
                $result_config = array_merge($result_config, $alias_config);
            }
            if (count($result_config)) {
                /*  if (!(is_array($calendar_source))) {

                  } else {
                  foreach ($calendar_source as $key => $value) {
                  $sourse_array[$value] = \Sl\Service\CalebdarConfig::read($value);
                  }
                  //Описати збір даних по аліасу, який приходить.
                  } */
                //    $this->view->tree_array = $this->_tree_array; 
                // $this->view->tag_id = $this->_base_tag_id;
                $this->view->calendar_tasks = $result_config;
                $this->view->result = true;
            }
        } catch (\Exception $e) {
            $this->view->result = false;
            $this->view->description = $e->getMessage();
        }
    }

    protected function _getCalendarConfig($alias, $values) {

        $tasks = array();
        $date_start = $this->getRequest()->getParam('start_date', FALSE);
        $date_finish = $this->getRequest()->getParam('end_date', FALSE);
        //  $date_start = date_create_from_format($date_start); 
        //$date_finish = date_create_from_format($date_finish); 




        $model = \Sl\Service\Helper::getModelnameByAlias($alias);
        $module = \Sl\Service\Helper::getModulenameByAlias($alias);
        $where = 'active = 1 and (' . $values['end'] . ' >= "' . $date_start . '" or ' . $values['start'] . ' between "' . $date_start . '" and "' . $date_finish . '")';
        $objects = \Sl_Model_Factory::mapper($model, $module)->fetchAll($where);
        if (count($objects)) {
            $cur_object = current($objects);
            $start_resource = \Sl_Service_Acl::isAllowed(array(
                        $cur_object,
                        $values['start'],
                            ), \Sl_Service_Acl::PRIVELEGE_UPDATE);
            $end_resource = \Sl_Service_Acl::isAllowed(array(
                        $cur_object,
                        $values['end'],
                            ), \Sl_Service_Acl::PRIVELEGE_UPDATE);
            if (!$end_resource || !$start_resource) {
                $editable = false;
            } else
                $editable = true;
        }
        foreach ($objects as $object) {
            $relation = \Sl_Modulerelation_Manager::getRelations($object, $values['tag_rel']);
            $object = \Sl_Model_Factory::mapper($object)->findRelation($object, $values['tag_rel']);
            $tag = $object->fetchOneRelated($values['tag_rel']);

            ($tag instanceof \Sl_Model_Abstract ? $masterrel_name = $tag->getMasterRelation() : $masterrel_name = '');
            ($tag instanceof \Sl_Model_Abstract ? $tag_relation = \Sl_Modulerelation_Manager::getRelations($tag, $masterrel_name) : $tag_relation = '');
            ($tag instanceof \Sl_Model_Abstract ? $rem_object = $tag_relation->getRelatedObject($tag)->findModelName() : $rem_object = '');
            //   if ($tag_relation instanceof \Sl\Modulerelation\Modulerelation) {
            //     $rem_object = $tag_relation->getRelatedObject($tag);
            //$this->_tree_array[$rem_object->findModelName()][$tag->getId()] = $tag->__toString();
            //}



            $tasks[] = array(
                'start' => $object->fetchStartDate(),
                'end' => $object->fetchFinalDate(),
                'title' => $object->fetchName(),
                'obj_id' => $object->getId(),
                'alias' => $alias,
                'allDay' => false,
                'editable' => $editable,
                'color' => '',
                'rem_obj' => $rem_object,
                'id' => $object->getId() . $alias,
                'tag' => ($tag instanceof \Sl_Model_Abstract ? $tag->__toString() : $this->view->translate('others')),
                'tag_id' => ($tag instanceof \Sl_Model_Abstract ? $tag->getId() : 'others'),
                'masterel' => $masterrel_name,
            );
        }

        return $tasks;
    }

    public function calendarAction() {

        $tag_id = $this->getRequest()->getParam('tag_id', '');

        try {
            $tag = \Sl_Model_Factory::mapper('tag', 'kcomments')->find($tag_id);
            $this->view->tag_id = $tag_id;
            //TO DO винести діставання інформації по тегу у окремий метод
            if ($tag instanceof \Sl_Model_Abstract) {
                $this->view->tag_name = $tag->__toString();
                $this->view->title = $tag->__toString();
                $masterrel_name = $tag->getMasterRelation();
                $tag_relation = \Sl_Modulerelation_Manager::getRelations($tag, $masterrel_name);
                if ($tag_relation instanceof \Sl\Modulerelation\Modulerelation) {
                    $rem_object = $tag_relation->getRelatedObject($tag);
                }
                $this->view->tag_remobj = ($rem_object instanceof \Sl_Model_Abstract ? $rem_object->findModelName() : '');
            } else {
                $this->view->tag_name = '';
                $this->view->tag_remobj = '';
            }
        } catch (\Exception $e) {
            $this->view->result = false;
            $this->view->description = $e->getMessage();
        }

        $this->view->content = $this->view->render('main/calendar.phtml');
    }

    public function ajaxganttAction() {

        $this->view->result = true;
        try {

            $gantt_tag = $this->getRequest()->getParam('gantt_tag', FALSE);
            $gantt_subaction = $this->getRequest()->getParam('subaction', FALSE);
            $task = \Sl_Model_Factory::object('task', 'crm');
            $milestone = \Sl_Model_Factory::object('milestone', 'crm');
            switch ($gantt_subaction) {
                case 'updateTask' : $task_id = $this->_parseId($this->getRequest()->getParam('id', FALSE));

                    if ($task_id > 0) {
                        $task = \Sl_Model_Factory::mapper($task)->find($task_id);
                        $task->setDateStart($this->getRequest()->getParam('date_start'))
                                ->setName($this->getRequest()->getParam('name'))
                                ->setDateFinish($this->getRequest()->getParam('date_finish'))
                                ->setStatus($this->getRequest()->getParam('status', 0))
                                ->setDescription($this->getRequest()->getParam('description'));

                        if ($user_id = $this->getRequest()->getParam('user', false)) {
                            $task->assignRelated('taskuser', array($user_id));
                        } else {
                            $task->assignRelated('taskuser', array());
                        }

                        $task = \Sl_Model_Factory::mapper($task)->save($task, true);
                    }
                    break;
                case 'createTask' : $task->setDateStart($this->getRequest()->getParam('date_start'));

                    $task->setName($this->getRequest()->getParam('name'));
                    $task->setDateFinish($this->getRequest()->getParam('date_finish'));
                    $task->setDescription($this->getRequest()->getParam('description'));
                    $gantt_tag > 0 &&
                            $task->assignRelated('taskmaintag', array($gantt_tag));

                    $milestonne_id = $this->_parseId($this->getRequest()->getParam('parent', false));
                    ($milestonne_id > 0) &&
                            $task->assignRelated(self::TASK_MILESTONE_RELATION, array($milestonne_id));

                    if ($user_id = $this->getRequest()->getParam('user', false)) {
                        $task->assignRelated('taskuser', array($user_id));
                    }

                    $task = \Sl_Model_Factory::mapper($task)->save($task, true);
                    $this->view->id = $this->_makeId($task);


                    break;
                case 'updateMilestone': $ms_id = $this->_parseId($this->getRequest()->getParam('id', FALSE));
                    if ($ms_id > 0) {
                        $milestone = \Sl_Model_Factory::mapper($milestone)->find($ms_id);
                        $milestone->setStart($this->getRequest()->getParam('date_start'))
                                ->setName($this->getRequest()->getParam('name'))
                                ->setStatus($this->getRequest()->getParam('status', 0))
                                ->setEnd($this->getRequest()->getParam('date_finish'))
                                ->setDescription($this->getRequest()->getParam('description'));

                        if ($user_id = $this->getRequest()->getParam('user', false)) {
                            $milestone->assignRelated('milestoneuser', array($user_id));
                        } else {
                            $milestone->assignRelated('milestoneuser', array());
                        }

                        $milestone = \Sl_Model_Factory::mapper($milestone)->save($milestone, true);
                    }
                    break;
                case 'deleteTask' : $task_id = $this->_parseId($this->getRequest()->getParam('id', FALSE));
                    if ($task_id > 0) {
                        $task = \Sl_Model_Factory::mapper($task)->find($task_id)->setArchived(1);

                        \Sl_Model_Factory::mapper($task)->save($task);
                    }

                    break;
                case 'deleteMilestone' : $ms_id = $this->_parseId($this->getRequest()->getParam('id', FALSE));
                    if ($ms_id > 0) {
                        $milestone = \Sl_Model_Factory::mapper($milestone)->find($ms_id)->setArchived(1);

                        \Sl_Model_Factory::mapper($milestone)->save($milestone);
                    }

                    break;
                case 'createMilestone' : $milestone->setStart($this->getRequest()->getParam('date_start'));

                    $milestone->setName($this->getRequest()->getParam('name'));
                    $milestone->setEnd($this->getRequest()->getParam('date_finish'));
                    $milestone->setDescription($this->getRequest()->getParam('description'));
                    $gantt_tag > 0 &&
                            $milestone->assignRelated('milestonemaintag', array($gantt_tag));

                    if ($user_id = $this->getRequest()->getParam('user', false)) {
                        $milestone->assignRelated('milestoneuser', array($user_id));
                    }

                    $milestone = \Sl_Model_Factory::mapper($milestone)->save($milestone, true);
                    $this->view->id = $this->_makeId($milestone);


                    break;


                case 'relateTasks':
                    $relate_arr = $this->getRequest()->getParam('relation', array());


                    if ($relate_arr['source'] && $relate_arr['target']) {


                        $source = \Sl_Model_Factory::mapper($task)->findExtended($this->_parseId($relate_arr['source']), array(self::NEXT_TASK_RELATION));
                        $related = $source->fetchRelated(self::NEXT_TASK_RELATION);
                        $related [$this->_parseId($relate_arr['target'])] = $this->_parseId($relate_arr['target']);
                        $source->assignRelated(self::NEXT_TASK_RELATION, $related);

                        \Sl_Model_Factory::mapper($source)->save($source);
                    } else {
                        $this->view->result = false;
                    }
                    break;
                case 'deleteRelation' : $relate_arr = $this->getRequest()->getParam('relation', array());


                    if ($relate_arr['source'] && $relate_arr['target']) {


                        $source = \Sl_Model_Factory::mapper($task)->findExtended($this->_parseId($relate_arr['source']), array(self::NEXT_TASK_RELATION));
                        $related = $source->fetchRelated(self::NEXT_TASK_RELATION);
                        unset($related [$this->_parseId($relate_arr['target'])]);
                        $source->assignRelated(self::NEXT_TASK_RELATION, $related);
                        \Sl_Model_Factory::mapper($source)->save($source);
                    } else {
                        $this->view->result = false;
                    }

                    break;
                case 'reorderTasks': $objs = $this->getRequest()->getParam('obj', array());  
                                     foreach ($objs as $obj){
                                         $task = \Sl_Model_Factory::mapper('task', 'crm')->find($obj['id']);
                                         if ($task) {
                                             $task->setProjectorder($obj['projectorder']);
                                             \Sl_Model_Factory::mapper($task)->save($task, false, false);
                                         }
                                     }
                                     break; 
                case 'reorderMilestones':   $objs = $this->getRequest()->getParam('obj', array());  
                                            foreach ($objs as $obj){
                                                $task = \Sl_Model_Factory::mapper('milestone', 'crm')->find($obj['id']);
                                                if ($task) {
                                                    $task->setProjectorder($obj['projectorder']);
                                                    \Sl_Model_Factory::mapper($task)->save($task, false, false);
                                                }
                                            } break;
                default:
                    $sourse_array = array();
                    $result_config = array();

                    $acl_allows = array();


                    //$model = \Sl\Service\Helper::getModelByAlias($alias);
                    //Визначаємо. чи доступні для редагування початок і кінець.


                    $acl_allows['task'] = \Sl_Service_Acl::isAllowed(array(
                                $task,
                                'date_start',
                                    ), \Sl_Service_Acl::PRIVELEGE_UPDATE) &&
                            \Sl_Service_Acl::isAllowed(array(
                                $task,
                                'date_finish',
                                    ), \Sl_Service_Acl::PRIVELEGE_UPDATE) &&
                            \Sl_Service_Acl::isAllowed(array(
                                $task,
                                'name',
                                    ), \Sl_Service_Acl::PRIVELEGE_UPDATE) &&
                            \Sl_Service_Acl::isAllowed(array(
                                $task,
                                'description',
                                    ), \Sl_Service_Acl::PRIVELEGE_UPDATE);


                    $this->view->allows = $acl_allows;
            }

          


            //////////////////////////////////////////
            // get users by tag_id: 
            //  get product
            if ($gantt_tag > 0) {

                self::_getGanttTasks($gantt_tag);
                $tag = \Sl_Model_Factory::mapper('tag', 'kcomments')->find($gantt_tag);
                if ($tag instanceof \Application\Module\Kcomments\Model\Tag) {
                    if ($tag->getMasterRelation() == 'maintagproduct') {
                        $product = $tag->fetchOneRelated($tag->getMasterRelation());
                    } elseif ($tag->getMasterRelation() == 'maintagdeal') {
                        $deal = $tag->fetchOneRelated($tag->getMasterRelation());
                        $rel_name = 'productdeal';
                        !$deal->issetRelated($rel_name) && $deal = \Sl_Model_Factory::mapper($deal)->findRelation($deal, $rel_name);
                        $product = $deal->fetchOneRelated($rel_name);
                    }
                }
            
                $userModel = \Sl_Model_Factory::object('user', 'auth');
                $users_arr = array();
                if ($product instanceof \Application\Module\Business\Model\Product) {
                    $fs = \Sl\Model\Identity\Fieldset\Factory::build($userModel, 'listview');

                    $fs->createField('id', array('roles' => array('from')));


                    $base_filter = array(
                        'type' => 'multi',
                        'comps' => array(
                            array(
                                'type' => 'eq',
                                'field' => 'active',
                                'value' => '1'
                            ),
                            array(
                                'type' => 'eq',
                                'field' => 'circleusers.departmentmaincircle.departmentproduct.id',
                                'value' => $product->getId()
                            ), 
                            array(
                            'type' => 'multi',
                            'comparison' => \Sl\Model\Identity\Fieldset\Comparison\Multi::COMPARISON_OR,
                            'comps' => array(
                                array(
                                    'field' => 'system',
                                    'type' => 'eq',
                                    'value' => 0,
                                ),
                                array(
                                    'field' => 'system',
                                    'type' => 'isnull',
                                    'value' => 1,
                                ),
                            ),
                        ),

                        ),
                    );
                    $fs ->addComps(array($base_filter));

                    $ds = new \Sl\Model\Identity\Dataset\Xls();
                    //Для restrictions
                    $event = new \Sl\Event\Fieldset('prepareAjax', array(
                        'fieldset' => $fs,
                        'filters' => array(),
                    ));
                    \Sl_Event_Manager::trigger($event);

                    $ds->setFieldset($fs);

                    $ds = \Sl_Model_Factory::mapper($userModel)->fetchDataset($ds);



                    $prep_data = array();
                    //echo '<pre>'.print_r(array($ds->getData(), $ds->getOption('sql_source')), true).'</pre>';die;
                    foreach ($ds->getData() as $item) {
                        $prep_data[] = $item['id'];
                    }

                    if (count($prep_data)){
                        $users = \Sl_Model_Factory::mapper($userModel)->fetchAll('id in ('.implode(',',$prep_data).')');


                        foreach($users as $user) {
                            //$arr = $user -> toArray();
                            $arr['id'] = $user->getId();
                            $arr['name'] = $user->getName();
                            $arr['initials'] = $user->findInitials();
                            $arr['color'] = $user->getColor();
                            $users_arr[]= $arr;
                        }

                    }


                }
                $this->view->users = $users_arr;
            }
            
            


            //
            //
            //
            //////////////////////////////////////////
        } catch (\Exception $e) {
            $this->view->result = false;
            $this->view->description = $e->getMessage();
            $this->view->trace = $e->getTrace();
        }
    }
    
    public function ganttxlsexportAction(){
         set_time_limit(300);
         if ( $tag_id = $this->getRequest()->getParam('tag_id', false) ) {
            
            $mapper = \Sl_Model_Factory::mapper('task', 'crm');
            $ms_mapper = \Sl_Model_Factory::mapper('milestone', 'crm');

            $milestones = $ms_mapper->fetchByRelated($tag_id, 'milestonemaintag',null,null,null, ' start ASC ');

            $tasks = $this->getRequest()->getParam('just_milestones', FALSE) ? array() : $mapper->fetchByRelated($tag_id, 'taskmaintag', null,null,null, ' date_start ASC ');

            $headers = array(
                'Name', 'Assigned to', 'Start', 'End', 'Description', 'Condition'
            );
            $data = array();
            foreach ($milestones as $m) {
                $m = $m->issetRelated(self::MILESTONEUSER_RELATION) ? $m : $ms_mapper->findRelation($m, self::MILESTONEUSER_RELATION);
                $us = $m->fetchOneRelated(self::MILESTONEUSER_RELATION);
                $r = array(
                    true,
                    $m->__toString(),
                    ($us instanceof \Sl_Model_Abstract?$us->__toString():''),
                    $m -> getStart(),
                    $m -> getEnd(),
                    $m -> getDescription(),
                    $m -> getStatus().'%'
                );
                $data[] = $r;
                foreach ($tasks as $t) {
                    $t = $t->issetRelated(self::MILESTONEUSER_RELATION) ? $t : $mapper->findRelation($t, self::TASKUSER_RELATION);
                    $t = $t->issetRelated(self::TASK_MILESTONE_RELATION) ? $t : $mapper->findRelation($t, self::TASK_MILESTONE_RELATION);
                    $_m = $t->fetchOneRelated(self::TASK_MILESTONE_RELATION);
                    $us = $t->fetchOneRelated(self::TASKUSER_RELATION);
                    
                    if ($_m instanceof \Sl_Model_Abstract && $_m->getId() == $m->getId()){
                        $r = array(
                            false,
                            $t->__toString(),
                            ($us instanceof \Sl_Model_Abstract?$us->__toString():''),
                            substr($t -> getDateStart(),0,10),
                            substr($t -> getDateFinish(),0,10),
                            $t -> getDescription(),
                            $t -> getStatus().'%'
                        );
                        $data[] = $r;
                    }
                }
                
            }
            
            foreach ($tasks as $t) {
                   
                    if (!($_m instanceof \Sl_Model_Abstract && $_m->getId())){
                        $r = array(
                            false,
                            $t->__toString(),
                            ($us instanceof \Sl_Model_Abstract?$us->__toString():''),
                            substr($t -> getDateStart(),0,10),
                            substr($t -> getDateFinish(),0,10),
                            $t -> getDescription(),
                            $t -> getStatus().'%'
                        );
                        $data[] = $r;
                    }
                    
                }
            
            while (ob_get_level()) {
                ob_end_clean();
            }
             if (class_exists('PHPExcel')) {
                $excel = new \PHPExcel();
                $ws = $excel->getActiveSheet();
                

                $cur_col = 0;
                $cur_row = 1;

                foreach ($headers as $value) {
                    $value = html_entity_decode($value, ENT_COMPAT | ENT_HTML401, 'UTF-8');
                    $cell = $ws->getCellByColumnAndRow($cur_col, $cur_row)->setValue($value);
                    //$ws->getStyleByColumnAndRow($cur_col, $cur_row)->getFont()->setBold(true);
                    $cur_col++;
                }
                

                foreach ($data as $values) {
                    $cur_row++;
                    $cur_col = 0;
                    $b = array_shift($values);
                    
                    foreach ($values as $value) {
                        $cell = $ws->getCellByColumnAndRow($cur_col, $cur_row)->setValue($value);
                        if ($b) $ws->getStyleByColumnAndRow($cur_col, $cur_row)->getFont()->setBold(true);
                        $cur_col++;
                    }
                }

                header('Content-Description: File Transfer');
                header('Content-Encoding: UTF-8');
                header('Content-type: text/csv; charset=UTF-8');
                header('Content-Disposition: attachment; filename=gantt.xlsx');
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Pragma: public');

                $writer = new \PHPExcel_Writer_Excel2007($excel);
                echo $writer->save('php://output');
            } else {
                header('Content-Description: File Transfer');
                header('Content-Encoding: UTF-8');
                header('Content-type: text/csv; charset=UTF-8');
                header('Content-Disposition: attachment; filename=export.csv');
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Pragma: public');
                $this->outputCSV($data);
            }
             
         }
         
        die;
    }
    
    protected function _getGanttTasks($tag_id) {

        $tasks = $links = array();
        $min_date = false;
        $max_date = false;
        
                
        $mapper = \Sl_Model_Factory::mapper('task', 'crm');
        $ms_mapper = \Sl_Model_Factory::mapper('milestone', 'crm');

        $milestones = $ms_mapper->fetchByRelated($tag_id, 'milestonemaintag', null, null, null,  array(' projectorder ASC ', ' start ASC '));

        $objects = $this->getRequest()->getParam('just_milestones', FALSE) ? array() : $mapper->fetchByRelated($tag_id, 'taskmaintag', null, null, null,  array(' projectorder ASC' , 'date_start ASC'));
        
        $max_dates = array();
        $ms_ordering = 0;
        $task_ordering = array();
        
        foreach ($milestones as $object) {
            $dStart = new \DateTime($object->getStart());
            $dEnd = new \DateTime($object->getEnd());
            
            $min_date = ($min_date && $min_date < $dStart)?$min_date:$dStart;
            $max_date = ($max_date && $max_date > $dEnd)?$max_date:$dEnd;
            $ms_ordering= max($object->getProjectorder(),$ms_ordering+10);
            
            //$dDiff = $dStart->diff($dEnd);
            $object = $object->issetRelated(self::MILESTONEUSER_RELATION) ? $object : $ms_mapper->findRelation($object, self::MILESTONEUSER_RELATION);
            $us = $object->fetchOneRelated(self::MILESTONEUSER_RELATION);
            $max_dates[$object -> getId()] = $dStart;
                    
            $tasks[] = array(
                'start_date' => $dStart->format($object::FORMAT_TIMESTAMP),
                'end_date' => $dEnd->format($object::FORMAT_TIMESTAMP),
//                'duration' => $dDiff->format('%a'),
                'parent' => '',
                'progress' => $object->getStatus() / 100,
                'text' => $object->__toString(),
                'description' => $object->getDescription(),
                'id' => $this->_makeId($object),
                'level' => $object->assessLevel(),
                'open' => true,
                'type' => 'ms',
                'assigs' => array(),
                'user' => ($us instanceof \Sl_Model_Abstract ? $us->getId() : ''),
                'username' => ($us instanceof \Sl_Model_Abstract ? $us . '' : ''),
                'userinits' => ($us instanceof \Sl_Model_Abstract ? $us->findInitials() : ''),
                'usercolor' => ($us instanceof \Sl_Model_Abstract ? $us->getColor() : ''),
                'projectorder' => $ms_ordering
            );
        }

        
        foreach ($objects as $object) {
            $dStart = new \DateTime($object->getDateStart());
            $dEnd = new \DateTime($object->getDateFinish());
           // $dDiff = $dStart->diff($dEnd);
            
            $min_date = $min_date > $dStart?$dStart:$min_date;
            $max_date = $max_date < $dEnd?$dEnd:$max_date;
            
            $object = $object->issetRelated(self::NEXT_TASK_RELATION) ? $object : $mapper->findRelation($object, self::NEXT_TASK_RELATION);
            $object = $object->issetRelated(self::TASK_MILESTONE_RELATION) ? $object : $mapper->findRelation($object, self::TASK_MILESTONE_RELATION);
            $object = $object->issetRelated(self::TASKUSER_RELATION) ? $object : $mapper->findRelation($object, self::TASKUSER_RELATION);

            $ms = $object->fetchOneRelated(self::TASK_MILESTONE_RELATION);
            $us = $object->fetchOneRelated(self::TASKUSER_RELATION);
            
            if ($ms instanceof \Sl_Model_Abstract && $dEnd > $max_dates[$ms -> getId()] ) $max_dates[$ms -> getId()] = $dEnd;
            $i = $ms instanceof \Sl_Model_Abstract?$ms->getId():0;
            $task_ordering[$i] = max(intval($task_ordering[$i]) +10, $object->getProjectorder());
            
            $tasks[] = array(
                'start_date' => $object->getDateStart(),
                'end_date' => $object->getDateFinish(),
                //'duration' => $dDiff->format('%d'),
                'parent' => is_object($ms) ? $this->_makeId($ms) : '',
                'progress' => $object->getStatus() / 100,
                'text' => $object->__toString(),
                'description' => $object->getDescription(),
                'id' => $this->_makeId($object),
                'open' => true,
                'assigs' => array(),
                'user' => ($us instanceof \Sl_Model_Abstract ? $us->getId() : ''),
                'username' => ($us instanceof \Sl_Model_Abstract ? $us . '' : ''),
                'userinits' => ($us instanceof \Sl_Model_Abstract ? $us->findInitials() : ''),
                'usercolor' => ($us instanceof \Sl_Model_Abstract ? $us->getColor() : ''),
                'projectorder' => $task_ordering[$i] 
            );

            foreach ($object->fetchRelated(self::NEXT_TASK_RELATION) as $id => $related) {
                $link = array(
                    'id' => $this->_makeId($object, $id) . '_' . $this->_makeId($object),
                    'source' => $this->_makeId($object),
                    'target' => $this->_makeId($object, $id),
                    'type' => 0
                );
                $links[] = $link;
            }
        }



        if (!$this->getRequest()->getParam('just_milestones', FALSE)) {
            foreach ($milestones as $object) {
                //$dEnd = new \DateTime($object->getEnd());
                $dEnd = $max_dates[$object->getId()];
                $tasks[] = array(
                    'start_date' => $dEnd->format($object::FORMAT_TIMESTAMP),
                    'end_date' => $dEnd->format($object::FORMAT_TIMESTAMP),
                    'duration' => 0,
                    'parent' => $this->_makeId($object),
                    'progress' => 0,
                    'text' => '',
                    'description' => '',
                    'id' => 'new-ms-' . $this->_makeId($object),
                    'open' => true,
                    'assigs' => array(),
                    'projectorder' => $task_ordering[$object->getId()] + 10
                );
            }
        }

        $min_date = $min_date?$min_date:new \DateTime;
        $max_date = $max_date?$max_date:new \DateTime;
        
        $max_date-> add(new \DateInterval('P3M'));
        $min_date-> sub(new \DateInterval('P3M'));
        
        $holidays = \Sl_Model_Factory::mapper('holiday','crm')->fetchAll('date BETWEEN "'.$min_date->format(\Sl_Model_Abstract::FORMAT_DATE).'" AND "'.$max_date->format(\Sl_Model_Abstract::FORMAT_DATE).'" ');
        $holidays_arr = array();
        foreach ($holidays as $h) {
            $holidays_arr[] = $h->getDate();
        }
        
        $this->view->minDate = $min_date->format(\Sl_Model_Abstract::FORMAT_DATE);
        $this->view->maxDate = $max_date->format(\Sl_Model_Abstract::FORMAT_DATE);
        $this->view->holidays = $holidays_arr;
        $this->view->tasks = $tasks;
        $this->view->links = $links;
    }

    protected function _parseId($id) {
        $id = explode('_', $id);
        return $id[1];
    }

    protected function _makeId(\Sl_Model_Abstract $obj, $id = false) {

        return $obj->findModelName() . '_' . ($id ? $id : $obj->getId());
    }

}
