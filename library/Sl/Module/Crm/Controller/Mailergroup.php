<?php

namespace Sl\Module\Crm\Controller;

class Mailergroup extends \Sl_Controller_Model_Action {

    const SCRIPT_LIST_VIEW_BASE_PATH = '/Sl/View/Scripts/List';
    


    public function editAction() {
        $errors = $this->getRequest()->getParam('errors', array());

        $Obj = \Sl_Model_Factory::mapper($this->getModelName(), $this->_getModule())->findAllowExtended($this->getRequest()->getParam('id', 0));

        if (!$Obj) {
            if (false === $this->getRequest()->getParam('id', false)) {
                $Obj = \Sl_Model_Factory::object($this->getModelName(), $this->_getModule());
                $Obj = \Sl_Model_Factory::mapper($Obj)->prepareNewObject($Obj);
            } else
                throw new \Sl_Exception_Model('Illegal ' . $this->getModelName() . ' id');
        }
        
        $Locker = \Sl_Model_Factory::object('\Sl\Module\Home\Model\Locker');
        if (!\Sl_Model_Factory::mapper($Locker)->checkModel($Obj)) {
            if (\Sl_Service_Acl::isAllowed(\Sl_Service_Acl::joinResourceName(array(
                                'type' => \Sl_Service_Acl::RES_TYPE_MVC,
                                'module' => $this->_getModule(),
                                'controller' => $this->getModelName(),
                                'action' => 'detailed'
                    )))) {
                $error_url = $this->view->url(array(
                    'module' => $this->_getModule(),
                    'controller' => $this->getModelName(),
                    'action' => 'detailed',
                    'id' => $Obj->getId()
                ));
                \Sl\Module\Home\Service\Errors::addError('Форму обрабатывает другой пользователь!');
                $this->_redirect($error_url);
            } else {
                $this->_redirect($forward_url);
                throw new Exception(\Zend_Registry::get('Zend_Translate')->translate("Форму обрабатывает другой пользователь!"), 1);
            }
        }
        $this->view->headScript()->appendFile('/home/main/locker.js');

        \Sl_Event_Manager::trigger(new \Sl_Event_Action('beforeEditAction', array(
                    'model' => $Obj,
                    'view' => $this->view,
                )));



        $form = \Sl_Form_Factory::build($Obj, true);
        $this->view->form = $form;
        $this->view->subtitle = $Obj->__toString();
        \Sl_Event_Manager::trigger(new \Sl\Event\Modelaction('before', array(
            'model'     => $Obj,
            'view'      => $this->view,
            'request'   => $this->getRequest(),
        )));
        if ($this->getRequest()->isPost()) {
            \Sl_Event_Manager::trigger(new \Sl\Event\Modelaction('beforePost', array(
                'model'     => $Obj,
                'view'      => $this->view,
                'request'   => $this->getRequest(),
            )));
            if ($form->isValid($this->getRequest()->getParams())) {

                $Obj->setOptions($this->getRequest()->getParams());

                try {
                    \Sl_Model_Factory::mapper($Obj)->save($Obj);
                } catch (\Exception $e) {
                    throw new Exception($e->getMessage(), 1);
                }
                \Sl_Event_Manager::trigger(new \Sl\Event\Modelaction('afterPost', array(
                    'model'     => $Obj,
                    'view'      => $this->view,
                    'request'   => $this->getRequest(),
                )));
                $forward_url = $this->getRequest()->getParam(\Sl_Form_Factory::AFTER_SAVE_URL_INPUT, '');

                if (!strlen($forward_url)) {
                    $forward_url = '/';

                    \Sl_Service_Acl::setContext($this->getRequest());

                    if (\Sl_Service_Acl::isAllowed(\Sl_Service_Acl::joinResourceName(array(
                                        'type' => \Sl_Service_Acl::RES_TYPE_MVC,
                                        'module' => $this->_getModule(),
                                        'controller' => $this->getModelName(),
                                        'action' => 'list'
                                    )))) {
                        $forward_url = \Sl\Service\Helper::listUrl($Obj);
                        /* $this->view->url(array(
                          'module' => $this->_getModule(),
                          'controller' => $this->getModelName(),
                          'action' => 'list',
                          'params'=>array()
                          )); */
                    }
                }
                if ($this->view->is_iframe) {
                    $this->_forward('closeiframe');
                } else {
                    $this->_redirect($forward_url);
                }
            } else {

                $form->populate($this->getRequest()->getParams());
                $this->view->errors = $errors;


                $this->view->errors += $form->getMessages();
            }
        }
        
        $this->view->calc_script = \Sl\Serializer\Serializer::getCalculatorsJS($Obj);

        \Sl_Event_Manager::trigger(new \Sl\Event\Modelaction('after', array(
            'model'     => $Obj,
            'view'      => $this->view,
            'request'   => $this->getRequest(),
        )));

        \Sl_Event_Manager::trigger(new \Sl_Event_Action('afterEditAction', array(
                    'model' => $Obj,
                    'view' => $this->view,
                )));

        $this->customFormList($Obj);
    }

    public function createAction() {
        
        
        parent::createAction();
        $this->customFormList($Obj);
        
        
    }
    
    function customFormList($Obj)
    {
        
        $this->view->headScript()->appendFile('/home/main/nlist.js');
        $this->view->headLink()->appendStylesheet('/home/main/nlist.css');
        
        $selected = $this->getRequest()->getParam('selected', array());
        if($Obj)
        $selected = array_keys($Obj->fetchRelated('mailergroupcustomer'));
       
        //$default_object = \Sl_Model_Factory::object($this->getModelName(), $this->_getModule());
        $default_object = \Sl_Model_Factory::object('customer', 'customers');
        $filter_fields = $this->getRequest()->getParam('filter_fields', array());
        $is_iframe = $this->getRequest()->getParam('is_iframe');
        
       
        
        
        $group_actions = \Sl\Service\Groupactions::getGroupActionNames($default_object);
        $allowed_group_actions = array();
        
        if (is_array($group_actions) && count($group_actions)){
            
            
            foreach($group_actions as $action){
                $resource = \Sl_Service_Acl::joinResourceName(array(
                    'type' => \Sl_Service_Acl::RES_TYPE_MVC,
                    'module' =>  $this->getRequest()->getParam('module'),
                    'controller' => $this->getRequest()->getParam('controller'),
                    'action' => $action
                ));
                
                if(\Sl_Service_Acl::isAllowed($resource, \Sl_Service_Acl::PRIVELEGE_ACCESS))
                $allowed_group_actions[] = $action;
            }
        }
        
        $content = $this->getDTTemplate($default_object, null, count($allowed_group_actions)?'checkbox':false, $selected, $filter_fields, array(), array(), false, $is_iframe);
        $this->view->title = null;
        $this->view->subtitle = null;
        $this->view->controller = $this->getRequest()->getControllerName();
        $this->view->content = $content;
    }

      public static function getDTTemplate(\Sl_Model_Abstract $object, \Sl\Model\Identity\Identity $identity = null, $check_type = false, array $selected = array(), array $filter_fields = array(), array $calculators = array(), array $customs = array(), $is_popup = false, $is_iframe = false) {
        //  error_reporting(E_ERROR);
          if (is_null($identity)) {
            $identity = \Sl_Model_Factory::identity($object);
        }

        if ($calculators) {
            $identity->setCurrentCalculator(array_shift($calculators));
        }

        $translator = \Zend_Registry::get('Zend_Translate');

        $fields = $identity->getCalculatedObjectFields(true, true, true);

        
        foreach ($fields as &$field) {
            $field['column_name'] = $field['name'];
            $field['tr_name'] = $translator->translate($field['label']);
        }

        $table = new \Sl_View(array('scriptPath' => LIBRARY_PATH . self::SCRIPT_LIST_VIEW_BASE_PATH));
        $table->fields = $fields;

       

        //$table->context_menu = $object->contextMenu('list_item');

        $table->check_type = $check_type;
        //$table->is_popup = $is_popup;
        //$table->is_iframe = $is_iframe;
        $table->filter_fields = $filter_fields;
        //$table->returnfields = $returnfields;

        $table->selected_data = $selected;
      //  print_r( $table->selected_data);
        $table->customs = $customs;
        $table->handling = $identity->findHandling();
       // $table->calcs_data = $identity->getCalculator() ? get_class($identity->getCalculator()) : '';
        $table->request_data = array_map(function($el) {
                    return $el['name'];
                }, $identity->getObjectFields(true, true, true));
        
        //$table->export_entry_point = \Sl\Service\Helper::exportListUrl($object);
        $table->ajax_base_url = \Sl\Service\Helper::ajaxListUrl($object);
        $table->selected_strings_url = implode('/', array('', $object->findModuleName(), $object->findModelName(), 'ajaxdetailed'));
        $table->base_object = $object;
        $table->selected_items_entry_point = '/customers/customer/ajaxselecteditems';
        return $table->render('dt/table.php');
    }

}