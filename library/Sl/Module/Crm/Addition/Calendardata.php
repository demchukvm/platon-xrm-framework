<?php

namespace Sl\Module\Crm\Addition;

interface Calendardata  {
    
    public function fetchStartDate($as_object = false);
    
    public function fetchFinalDate($as_object = false);
    
    public function fetchCompleteness();
    
    public function fetchName();
    
}

