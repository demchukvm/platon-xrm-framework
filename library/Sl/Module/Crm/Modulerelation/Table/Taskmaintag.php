<?php
namespace Sl\Module\Crm\Modulerelation\Table;

class Taskmaintag extends \Sl\Modulerelation\DbTable {
	protected $_name = 'crm_task_main_tag';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Sl\Module\Crm\Model\Task' => array(
			'columns' => 'task_id',
			'refTableClass' => 'Sl\Module\Crm\Model\Table\Task',
		'refColums' => 'id'),
                		'Application\Module\Kcomments\Model\Tag' => array(
			'columns' => 'tag_id',
			'refTableClass' => 'Application\Module\Kcomments\Model\Table\Tag',
				'refColums' => 'id'	),
	);
}