<?php
namespace Sl\Module\Crm\Modulerelation\Table;

class Taskemail extends \Sl\Modulerelation\DbTable {
	protected $_name = 'crm_taskemail';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Sl\Module\Crm\Model\Task' => array(
			'columns' => 'task_id',
			'refTableClass' => 'Sl\Module\Crm\Model\Table\Task',
		'refColums' => 'id'),
                		'Sl\Module\Home\Model\Email' => array(
			'columns' => 'email_id',
			'refTableClass' => 'Sl\Module\Home\Model\Table\Email',
				'refColums' => 'id'	),
	);
}