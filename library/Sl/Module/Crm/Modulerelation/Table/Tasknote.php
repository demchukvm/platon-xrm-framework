<?php
namespace Sl\Module\Crm\Modulerelation\Table;

class Tasknote extends \Sl\Modulerelation\DbTable {
	protected $_name = 'crm_tasknote';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Sl\Module\Crm\Model\Task' => array(
			'columns' => 'task_id',
			'refTableClass' => 'Sl\Module\Crm\Model\Table\Task',
		'refColums' => 'id'),
                		'Sl\Module\Crm\Model\Note' => array(
			'columns' => 'note_id',
			'refTableClass' => 'Sl\Module\Crm\Model\Table\Note',
				'refColums' => 'id'	),
	);
}