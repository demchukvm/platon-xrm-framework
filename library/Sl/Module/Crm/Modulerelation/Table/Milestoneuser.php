<?php
namespace Sl\Module\Crm\Modulerelation\Table;

class Milestoneuser extends \Sl\Modulerelation\DbTable {
	protected $_name = 'crm_milestone_user';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Sl\Module\Crm\Model\Milestone' => array(
			'columns' => 'milestone_id',
			'refTableClass' => 'Sl\Module\Crm\Model\Table\Milestone',
		'refColums' => 'id'),
                		'Sl\Module\Auth\Model\User' => array(
			'columns' => 'user_id',
			'refTableClass' => 'Sl\Module\Auth\Model\Table\User',
				'refColums' => 'id'	),
	);
}