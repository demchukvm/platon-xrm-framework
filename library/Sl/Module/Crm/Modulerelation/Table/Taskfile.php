<?php
namespace Sl\Module\Crm\Modulerelation\Table;

class Taskfile extends \Sl\Modulerelation\DbTable {
	protected $_name = 'crm_taskfile';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Sl\Module\Crm\Model\Task' => array(
			'columns' => 'task_id',
			'refTableClass' => 'Sl\Module\Crm\Model\Table\Task',
		'refColums' => 'id'),
                		'Sl\Module\Home\Model\File' => array(
			'columns' => 'file_id',
			'refTableClass' => 'Sl\Module\Home\Model\Table\File',
				'refColums' => 'id'	),
	);
}