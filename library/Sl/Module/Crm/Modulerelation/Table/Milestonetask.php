<?php
namespace Sl\Module\Crm\Modulerelation\Table;

class Milestonetask extends \Sl\Modulerelation\DbTable {
	protected $_name = 'crm_milestone_task';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Sl\Module\Crm\Model\Milestone' => array(
			'columns' => 'milestone_id',
			'refTableClass' => 'Sl\Module\Crm\Model\Table\Milestone',
		'refColums' => 'id'),
                		'Sl\Module\Crm\Model\Task' => array(
			'columns' => 'task_id',
			'refTableClass' => 'Sl\Module\Crm\Model\Table\Task',
				'refColums' => 'id'	),
	);
}