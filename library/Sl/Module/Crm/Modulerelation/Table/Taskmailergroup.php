<?php
namespace Sl\Module\Crm\Modulerelation\Table;

class Taskmailergroup extends \Sl\Modulerelation\DbTable {
	protected $_name = 'crm_taskmailergroup';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Sl\Module\Crm\Model\Task' => array(
			'columns' => 'task_id',
			'refTableClass' => 'Sl\Module\Crm\Model\Table\Task',
		'refColums' => 'id'),
                		'Sl\Module\Crm\Model\Mailergroup' => array(
			'columns' => 'mailergroup_id',
			'refTableClass' => 'Sl\Module\Crm\Model\Table\Mailergroup',
				'refColums' => 'id'	),
	);
}