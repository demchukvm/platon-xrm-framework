<?php
namespace Sl\Module\Crm\Modulerelation\Table;

class Relatednexttask extends \Sl\Modulerelation\DbTable {
	protected $_name = 'crm_related_next_task';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Sl\Module\Crm\Model\Task' => array(
			'columns' => 'task_id',
			'refTableClass' => 'Sl\Module\Crm\Model\Table\Task',
		'refColums' => 'id'),
                		'reverse' => array(
			'columns' => 'task2_id',
			'refTableClass' => 'Sl\Module\Crm\Model\Table\Task',
				'refColums' => 'id'	),
	);
}