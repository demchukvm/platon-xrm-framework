<?php
namespace Sl\Module\Crm\Modulerelation\Table;

class Mailergroupcustomer extends \Sl\Modulerelation\DbTable {
	protected $_name = 'crm_mailergroupcustomer';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Sl\Module\Crm\Model\Mailergroup' => array(
			'columns' => 'mailergroup_id',
			'refTableClass' => 'Sl\Module\Crm\Model\Table\Mailergroup',
		'refColums' => 'id'),
                		'Sl\Module\Customers\Model\Customer' => array(
			'columns' => 'customer_id',
			'refTableClass' => 'Sl\Module\Customers\Model\Table\Customer',
				'refColums' => 'id'	),
	);
}