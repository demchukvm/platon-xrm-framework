<?php
namespace Sl\Module\Crm\Modulerelation\Table;

class Mailgrouplead extends \Sl\Modulerelation\DbTable {
	protected $_name = 'crm_mailgrouplead';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Sl\Module\Crm\Model\Mailergroup' => array(
			'columns' => 'mailergroup_id',
			'refTableClass' => 'Sl\Module\Crm\Model\Table\Mailergroup',
		'refColums' => 'id'),
                		'Sl\Module\Customers\Model\Lead' => array(
			'columns' => 'lead_id',
			'refTableClass' => 'Sl\Module\Customers\Model\Table\Lead',
				'refColums' => 'id'	),
	);
}