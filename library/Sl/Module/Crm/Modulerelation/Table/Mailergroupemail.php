<?php
namespace Sl\Module\Crm\Modulerelation\Table;

class Mailergroupemail extends \Sl\Modulerelation\DbTable {
	protected $_name = 'crm_mailergroupemail';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Sl\Module\Crm\Model\Mailergroup' => array(
			'columns' => 'mailergroup_id',
			'refTableClass' => 'Sl\Module\Crm\Model\Table\Mailergroup',
		'refColums' => 'id'),
                		'Sl\Module\Home\Model\Email' => array(
			'columns' => 'email_id',
			'refTableClass' => 'Sl\Module\Home\Model\Table\Email',
				'refColums' => 'id'	),
	);
}