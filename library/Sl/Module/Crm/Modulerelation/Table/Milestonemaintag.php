<?php
namespace Sl\Module\Crm\Modulerelation\Table;

class Milestonemaintag extends \Sl\Modulerelation\DbTable {
	protected $_name = 'crm_milestone_main_tag';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Sl\Module\Crm\Model\Milestone' => array(
			'columns' => 'milestone_id',
			'refTableClass' => 'Sl\Module\Crm\Model\Table\Milestone',
		'refColums' => 'id'),
                		'Application\Module\Kcomments\Model\Tag' => array(
			'columns' => 'tag_id',
			'refTableClass' => 'Application\Module\Kcomments\Model\Table\Tag',
				'refColums' => 'id'	),
	);
}