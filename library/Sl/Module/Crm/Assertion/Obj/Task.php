<?

namespace Sl\Module\Crm\Assertion\Obj;

class Task implements \Zend_Acl_Assert_Interface {

    protected $_type_locked_field_update = array(
        1 => array('type'),
        2 => array('type'),
        3 => array('type'),
    );
    protected $_type_locked_field_access = array(
        1 => array( 'mail_topic', 'mail_body', 'taskmailergroup', 'taskfile'),
        2 => array( 'mail_topic', 'mail_body', 'taskmailergroup', 'taskfile'),
        3 => array(),
    );

    public function assert(\Zend_Acl $acl, \Zend_Acl_Role_Interface $role = null, \Zend_Acl_Resource_Interface $resource = null, $privilege = null) {
        
        $context = \Sl_Service_Acl::getContext();
        $resource_data = \Sl_Service_Acl::splitResourceName($resource);
        $resource_type = $resource_data['type'];
        $Obj = $context->getContext(); 
        
/*
        if ($resource_type == \Sl_Service_Acl::RES_TYPE_OBJ && $resource_data['name'] == 'task' && $context instanceof \Sl\Assertion\Context\Form) {


            
            if ($Obj instanceof \Sl\Module\Crm\Model\Task) {
                $type = $Obj->getType();

                if ($Obj->getId() && $privilege == \Sl_Service_Acl::PRIVELEGE_UPDATE) {

                    

                    if (array_search($resource_data['field'], $this->_type_locked_field_update[$type]) !== false) {
                        return FALSE;
                    }
                }

                if ($Obj->getId()) {                    

                    if (array_search($resource_data['field'], $this->_type_locked_field_access[$type]) !== false) {
                        return FALSE;
                    }
                }
            }
        } */
        if ($context instanceof \Sl\Assertion\Context\Form && $Obj instanceof \Sl\Module\Crm\Model\Task /*&& !$Obj->getId()*/) {
                    $resource_data = \Sl_Service_Acl::splitResourceName($resource);
                    $resource_type = $resource_data['type'];
                    if ($resource_type == \Sl_Service_Acl::RES_TYPE_OBJ && ( !in_array($resource_data['field'], array('name', 'taskmaintag', 'tasktag', 'taskuser', 'milestonetask', 'description','date_start','date_finish')))){
                        return false;
                    }   
                       
               
           }

        return true;
    }

}
