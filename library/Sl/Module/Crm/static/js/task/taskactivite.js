$(function() {

    $('body').on('click', '#online_taskactivite .item, #online_taskremind .item', function() {

        var request_obj = {};

        request_obj['is_iframe'] = 1;

        var url = $(this).attr('data-url');

        var $iframeDiv = $('<div />').css({
            width: 770,
            padding: 0,
        });
        var $iframe = $('<iframe />').attr({
            width: 770,
            height: 410
        }).css({
            border: 0
        }).attr('src', document.location.protocol + '//' + document.location.host + url + '?' + $.param(request_obj)).appendTo($iframeDiv);

        $('body').append($iframeDiv);

        $iframeDiv.dialog({
            autoOpen: false,
            width: 790,
            height: 410,
            modal: true,
            resizable: false,
            
            //  title : $(this).attr('title'),
        });
        $iframeDiv.dialog('open');

        closeIframeFunction = function() {

            refreshTaskAndRemind();
            $iframeDiv.dialog('close');
            $iframeDiv.remove();

        }
    });


});

function refreshTaskAndRemind()
{
 
    var jqxhr = $.post("/crm/task/ajaxtaskactivite/", function(data) {
        $(".task_count").text(data.count_task);
        $(".remind_count").text(data.count_remind);

        $("#online_taskactivite").replaceWith(data.content_task);
        $("#online_taskremind").replaceWith(data.content_remind);


    })
            .fail(function() {
        alert("error");
    });
return false;
}

