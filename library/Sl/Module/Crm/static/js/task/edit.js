$(function() {    
    
		$('body').on('click',  '.create_mailergroup', function() {

				var request_obj = {};

				request_obj['is_iframe'] = 1;
                                var relation_create = $(this).attr('data-relation');

				var url = '/crm/mailergroup/create/';
				var $iframeDiv = $('<div />').css({
					width : 1000,
					padding : 0
				});

				var $iframe = $('<iframe />').attr({
					width : 1000,
					height : 550
				}).css({
					border : 0
				}).attr('src', document.location.protocol + '//' + document.location.host + url + '?' + $.param(request_obj)).appendTo($iframeDiv);

				$('body').append($iframeDiv);

				//$iframe.closeIframe = f;

				$iframeDiv.dialog({
					title : '',
					autoOpen : false,
					width : 1010,
					height : 600,
					modal : true,
					resizable : false
				});
				//$iframeDiv.html($iframe);
				$iframeDiv.dialog('open');

				closeIframeFunction = function(relarion_id, relarion_tostring) {
                                      refreshListMailerGroup(relation_create, relarion_id, relarion_tostring);

					$iframeDiv.dialog('close');
					$iframeDiv.remove();

				}
			

			return false;
		});
    

//конопка для создания группы россылки
 $('#modulerelation_taskmailergroup_create').remove();
 $('#modulerelation_taskmailergroup').after('<button  class="ajax_create_modulerelation btn create_mailergroup" data-relation="modulerelation_taskmailergroup"></button>');
// от типа добавляются или удаляются поля 


    factor($('#type').val(),$('#status').val());

    $('body').on('change', '#type', function() {
        factor($(this).val(), $('#status').val());
    });
// отправить рассылку
    $('#bcbuttonssaveandsendmailer').click(function(){
        var $this = $(this);
        $('form:first').one('afterformvalidate.sl', function(){
            var $form = $(this);
            $form.append('<input type="hidden" data-type="saveandsendmailer" name="form_after_save_url" value="'+$this.attr('data-rel')+'">')
        });
        $('form:first').one('formerror.sl', function(){
            var $form = $(this);
            $form.find('[data-type="saveandsendmailer"]').remove();
        });
        $('#bcbuttonssave').trigger('click');
    });


});

function refreshListMailerGroup(relation_create, relarion_id, relarion_tostring)
{
  var val_id =  $('#'+relation_create).val();
  
  $('#'+relation_create).val(val_id+';'+relarion_id);
  $('#'+relation_create+'_list').append('<li><a href="/crm/mailergroup/edit/id/'+relarion_id+'" data-id="'+relarion_id+'">'+relarion_tostring+'</a></li>');
  $('#'+relation_create+'_list').parents('blockquote:first').show();
}

function factor(type, status)
{
   // console.log(type);
   // console.log(status);
    if (type == 1)
    {
        $('#remind').removeAttr('disabled').removeClass('muted');
        $('.remind').removeClass('muted');
        $('#date_finish').removeAttr('disabled').removeClass('muted').css('color','#666666');
        $('.date_finish').removeClass('muted');
        $('.date_finish .add-on').show();
        $('.mail_from').hide();
        $('#bcbuttonssaveandsendmailer').hide();
        

    }
    if (type == 2)
    {
        $('#remind').removeAttr('disabled').removeClass('muted');
        $('.remind').removeClass('muted');
        $('#date_finish').removeAttr('disabled').removeClass('muted').css('color','#666666');
        $('.date_finish').removeClass('muted');
        
        $('.date_finish .add-on').show();
        $('.mail_from').hide();
        $('#bcbuttonssaveandsendmailer').hide();

    }
    if (type == 3)
    {   
        $('#remind').attr('disabled', 'disabled').addClass('muted');
        $('.remind').addClass('muted');
        $('#date_finish').attr('disabled', 'disabled').addClass('muted').css('color','#CCCCCC');
        $('.date_finish').addClass('muted');
        
        $('.date_finish .add-on').hide();
        $('.mail_from').show();
        if(status ==2) $('#bcbuttonssaveandsendmailer').show();
            else $('#bcbuttonssaveandsendmailer').hide();
        

    }
}
