var callendar_get_request_info;
//var tasks_array = [];
var request_callendar_url = '/crm/main/ajaxcalendar';
var base_action_url = '/crm/main/calendar';
var request_save_event = '/crm/main/ajaxsavecalendarevent';
var calendar_source = '';
var calend = '#calendar';
var sql_datetime_format = 'yyyy-mm-dd hh:mm:ss';
var grafic_colors = [
    '#ff5f5f',
    '#3fcf7f',
    '#f4c414',
    '#5191d1',
    '#2fcff6',
    '#ea5e4d',
    '#4264ab',
    '#233445',
];

var events_array = [];
var events_info_array = [];
var tags_array = {};
var current_color_counter = 0;
var main_tag_id ='';
var base_tag_id;





$(document).ready(function() {

    if (base_tag_id>0) {
        var color;
        main_tag_id = base_tag_id;
        color = grafic_colors[current_color_counter] ? grafic_colors[current_color_counter++] : grafic_colors[(current_color_counter = 0)];
        tags_array[base_tag['id']] = {};
        tags_array[base_tag['id']]['val'] = 1;
        tags_array[base_tag['id']]['name'] = base_tag['name'];
        tags_array[base_tag['id']]['rem_obj'] = base_tag['remobj'];
        tags_array[base_tag['id']]['color'] = color;
    }

    build_calendar = function() {



        var $wrapper = $(document).find(calend);

        $wrapper.each(function() {

            $(this).fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                editable: true,
                droppable: true, // this allows things to be dropped onto the calendar !!

                drop: function(date, allDay) { // this function is called when something is dropped

                    // retrieve the dropped element's stored Event Object
                    var originalEventObject = $(this).data('eventObject');

                    // we need to copy it, so that multiple events don't have a reference to the same object
                    var copiedEventObject = $.extend({}, originalEventObject);

                    // assign it the date that was reported
                    copiedEventObject.start = date;
                    copiedEventObject.allDay = allDay;

                    // render the event on the calendar
                    // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                    $(calend).fullCalendar('renderEvent', copiedEventObject, true);

                    // is the "remove after drop" checkbox checked?
                    if ($('#drop-remove').is(':checked')) {
                        // if so, remove the element from the "Draggable Events" list
                        $(this).remove();
                    }

                },
                viewDisplay: function(a) {
                    callendar_get_request_info(a, null);
                },
                eventDrop: function(event) {
                    event.editable = false;
                    callendar_get_request_info(null, event);
                },
                eventResize: function(event) {
                    event.editable = false;
                    callendar_get_request_info(null, event);
                },
                events: [],
            });
        });

    };
    build_calendar();

    $('body').on('change', 'input[type="checkbox"]', function() {
        var id = $(this).attr('id').split('-')[1];
        
        var $l = $('label[for="'+$(this).attr('id')+'"]');
      //  console.log($l);
        if ($(this).is(':checked')) {
           //* $l.removeClass('disabled');*/
            $l.css({backgroundColor:$l.data('tagcolor'), color:'white'});
            tags_array[id]['val'] = 1;
            if (events_info_array[id]) {
                var temp_event_array = [];
                for (var i in events_info_array[id]) {
                    temp_event_array.push(events_info_array[id][i]);
                }
                $(calend).fullCalendar('addEventSource', temp_event_array);
            }

        } else {
            $l.css({backgroundColor:'transparent', color:'black'});
         /*  $l.addClass('disabled');*/
            tags_array[id]['val'] = 0;
            if ((events_info_array[id])) {
                for (var i in events_info_array[id]) {
                    $(calend).fullCalendar('removeEvents', events_info_array[id][i].id);
                }
            }

        }

    });
/*
    $('#docs pre code').each(function() {
        var $this = $(this);
        var t = $this.html();
        $this.html(t.replace(/</g, '&lt;').replace(/>/g, '&gt;'));
    });

    $(document).on('click', '.the-icons a', function(e) {
        e && e.preventDefault();
    });

    $(document).on('change', 'table thead [type="checkbox"]', function(e) {
        e && e.preventDefault();
        var $table = $(e.target).closest('table'), $checked = $(e.target).is(':checked');
        $('tbody [type="checkbox"]', $table).prop('checked', $checked);
    });

    $(document).on('click', '[data-toggle^="progress"]', function(e) {
        e && e.preventDefault();

        $el = $(e.target);
        $target = $($el.data('target'));
        $('.progress', $target).each(
                function() {
                    var $max = 50, $data, $ps = $('.progress-bar', this).last();
                    ($(this).hasClass('progress-mini') || $(this).hasClass('progress-small')) && ($max = 100);
                    $data = Math.floor(Math.random() * $max) + '%';
                    $ps.css('width', $data).attr('data-original-title', $data);
                }
        );
    });

    function addNotification($notes) {
        var $el = $('#panel-notifications'), $n = $('.count-n:first', $el), $item = $('.list-group-item:first', $el).clone(), $v = parseInt($n.text());
        $('.count-n', $el).fadeOut().fadeIn().text($v + 1);
        $item.attr('href', $notes.link);
        $item.find('.pull-left').html($notes.icon);
        $item.find('.media-body').html($notes.title);
        $item.hide().prependTo($el.find('.list-group')).slideDown().css('display', 'block');
    }
    var $noteMail = {
        icon: '<i class="fa fa-envelope-o fa-2x text-default"></i>',
        title: 'Added the mail app, Check it out.<br><small class="text-muted">2 July 13</small>',
        link: 'mail.html'
    }
    var $noteCalendar = {
        icon: '<i class="fa fa-calendar fa-2x text-default"></i>',
        title: 'Added the calendar, Get it.<br><small class="text-muted">10 July 13</small>',
        link: 'calendar.html'
    }
    var $noteTimeline = {
        icon: '<i class="fa fa-clock-o fa-2x text-default"></i>',
        title: 'Added the timeline, view it here.<br><small class="text-muted">1 minute ago</small>',
        link: 'timeline.html'
    }
    window.setTimeout(function() {
        addNotification($noteMail)
    }, 2000);
    window.setTimeout(function() {
        addNotification($noteCalendar)
    }, 3500);
    window.setTimeout(function() {
        addNotification($noteTimeline)
    }, 5000);

    // fullcalendar
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    var addDragEvent = function($this) {
        // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
        // it doesn't need to have a start or end
        var eventObject = {
            title: $.trim($this.text()), // use the element's text as the event title
            className: $this.attr('class').replace('label', '')
        };

        // store the Event Object in the DOM element so we can get to it later
        $this.data('eventObject', eventObject);

        // make the event draggable using jQuery UI
        $this.draggable({
            zIndex: 999,
            revert: true, // will cause the event to go back to its
            revertDuration: 0  //  original position after the drag
        });
    };
*/

});


callendar_get_request_info = function(obj, event, sync) {
    var event_data;
    if (event) {
        var start = event.start.format(sql_datetime_format);
        if (event.end instanceof Date) {
            var end = event.end.format(sql_datetime_format);
        } else {
            event.end = start;
        }
        event_data = {
            id: event.obj_id,
            end: end,
            start: start,
            alias: event.alias,
        };
    }

    var start_date = $(calend).fullCalendar('getView').start;
    var end_date = $(calend).fullCalendar('getView').end;
    var diff = Math.floor(((end_date.getTime()) - start_date.getTime()));
    start_date.setTime(start_date.getTime() - diff);
    end_date.setTime(end_date.getTime() + diff);
    start_date = start_date.format(sql_datetime_format);
    end_date = end_date.format(sql_datetime_format);
    $.ajax({
        type: 'POST',
        cache: false,
        async: !sync,
        url: request_callendar_url,
        data: {
            calendar_source: '',
            start_date: start_date,
            end_date: end_date,
            edited_event: event_data,
        },
        success: function(data) {

            if (data.result) {
                var temp_array = [];
                var $array = $(data.calendar_tasks);
                $array.each(function() {
                    var element = this;
                    var end = new Date(element.end);
                    var start = new Date(element.start);
                    element.end = end;
                    element.start = start;
                    temp_array.push(element);

                });

                //alert(main_tag_id);
                events_array = temp_array;
                form_tags_array(events_array);
                form_events_array();
                build_leftnav_tree();
                $(calend).fullCalendar('removeEvents');
                add_current_source();
                $(calend).fullCalendar('rerenderEvents');
            } else {
                console.log(data.description);

            }
        }
    });
};

var add_current_source = function() {
    for (var i in events_info_array) {
        if ((tags_array[i]['val'] === 1)) {
            var event_array = [];
            for (var j in events_info_array[i]) {
                event_array.push(events_info_array[i][j]);
            }
            $(calend).fullCalendar('addEventSource', event_array);
        }
    }
};

var form_events_array = function() {

    for (var i in events_array) {
        if (!events_info_array.hasOwnProperty(events_array[i].tag_id)) {
            events_info_array[events_array[i].tag_id] = {};
        }
        events_info_array[events_array[i].tag_id][events_array[i].id] = events_array[i];

    }



}

var form_tags_array = function() {
    var new_tags = {};
    var color;
    if (main_tag_id === '') {
        for (var i in events_array) {
            if (!new_tags.hasOwnProperty(events_array[i].tag_id)) {
                new_tags[events_array[i].tag_id] = {};
                new_tags[events_array[i].tag_id]['val'] = 1;
                new_tags[events_array[i].tag_id]['rem_obj'] = events_array[i].rem_obj;
                new_tags[events_array[i].tag_id]['name'] = events_array[i].tag.split(/\s/)[0];
                color = grafic_colors[current_color_counter] ? grafic_colors[current_color_counter++] : grafic_colors[(current_color_counter = 0)];
                new_tags[events_array[i].tag_id]['color'] = color;
            }
        }
        for (var j in tags_array) {
            new_tags[j] = tags_array[j];

        }
        tags_array = new_tags;
    } else {
        for (var i in events_array) {
            if (!new_tags.hasOwnProperty(events_array[i].tag_id)) {
                new_tags[events_array[i].tag_id] = {};
                if (events_array[i].tag_id === main_tag_id) {
                    new_tags[events_array[i].tag_id]['val'] = 1;
                } else {
                    new_tags[events_array[i].tag_id]['val'] = 0;
                }
                new_tags[events_array[i].tag_id]['rem_obj'] = events_array[i].rem_obj;
                new_tags[events_array[i].tag_id]['name'] = events_array[i].tag;
                color = grafic_colors[current_color_counter] ? grafic_colors[current_color_counter++] : grafic_colors[(current_color_counter = 0)];
                new_tags[events_array[i].tag_id]['color'] = color;
            }
        }
        for (var j in tags_array) {

            new_tags[j] = tags_array[j];
        }
        //new_tags[main_tag_id]['val'] = 1;
        tags_array = new_tags;
    }
}
;

var build_leftnav_tree = function() {
    //переробити потім по-людськи
    var menu_array = {};
    for (var k in tags_array) {
        if (!menu_array.hasOwnProperty(tags_array[k]['rem_obj']) && (tags_array[k]['rem_obj'] !== '')) {
            menu_array[tags_array[k]['rem_obj']] = {};
        }
        if ((!menu_array.hasOwnProperty(tags_array[k]['rem_obj'][k])) && (tags_array[k]['rem_obj'] !== '')) {
            //console.log(tags_array[k]['rem_obj']);
            menu_array[tags_array[k]['rem_obj']][k] = {};
            menu_array[tags_array[k]['rem_obj']][k] = tags_array[k]['name'];
        }

    }
    var colors_ids = [];
    var $subnav = $('<div> /').addClass('subnav_tags');
    var $dash_line = $('<div />').addClass('line').addClass('line-dashed');
    $subnav.append($dash_line);
    for (var i in menu_array) {
        var $subnav_title = $('<div> /').addClass('subnav-title');
        var $span_toggle_subnav = $('<span />').addClass('toggle-subnav').attr('href', '#');
        var $i_icon_angle_down = $('<i />').addClass('icon-angle-down');
        var $span = $('<span />').text(i);
        var $div_subnav_menu = $('<div />').addClass('subnav-menu').addClass('checkbox');
        $span_toggle_subnav.append($i_icon_angle_down);
        $span_toggle_subnav.append($span);
        $subnav_title.append($span_toggle_subnav);
        $subnav.append($subnav_title);
        for (var j in menu_array[i]) {
            var $div_checkbox = $('<div />').addClass('checkbox').addClass('nav_tag');
            var $input_checkbox = $('<input />').attr({type: 'checkbox', id:'input_tag-'+j});
            
            var $tagspan = $('<label />').attr({'for':'input_tag-'+j,ob_id:j}).addClass('tag-class').data('tagcolor', tags_array[j]['color']).addClass('label').text(menu_array[i][j]);
            
            if (tags_array[j]['val'] === 1) {
                $input_checkbox.attr("checked", "checked");
                $tagspan.css('background-color', $tagspan.data('tagcolor'));
            } else {
                 $tagspan.css({backgroundColor:'transparent', color:'black'});
            }
            
            colors_ids[j] = tags_array[j]['color'];
            $div_checkbox.append($input_checkbox);
            $div_checkbox.append($tagspan);
            $div_subnav_menu.append($div_checkbox);
        }
        $subnav_title.append($div_subnav_menu);
    }
    if (tags_array['others']){
    var $div_other_checkbox = $('<div />').addClass('checkbox').addClass('others').addClass('nav_tag');
    var $other_checkbox = $('<input />').attr({type: 'checkbox', id:'input_tag-others'});
    
    var $other_tagspan = $('<label />').attr({'for':'input_tag-others',ob_id:'others'}).addClass('tag-class').data('tagcolor', tags_array['others']['color']).addClass('label').text('others');
    if (tags_array['others']['val'] === 1) {
        $other_checkbox.attr("checked", "checked");
         $other_tagspan.css('background-color', $other_tagspan.data('tagcolor'));
    }
    colors_ids['others'] = tags_array['others']['color'];
    $div_other_checkbox.append($other_checkbox);
    $div_other_checkbox.append($other_tagspan);
    }
    $subnav.append($subnav_title);
    $subnav.append($div_other_checkbox);
    var $nav = $('body').find('#nav ul.nav');
    $('body').find('#nav .subnav_tags').remove();
    $subnav.appendTo($nav);

    for (var k in events_array) {
        var tag_id = events_array[k].tag_id;
        for (var r in colors_ids) {
            if (r === tag_id) {
                events_array[k].color = colors_ids[r];
            }

        }
    }
    
};


/*modify_event = function(event, revertFunc) {
 var start = event.start.format(sql_datetime_format);
 var end = event.end.format(sql_datetime_format);
 
 var event_data = {
 id: event.id,
 end: end,
 start: start,
 alias: event.alias,
 };
 
 $.ajax({
 type: 'POST',
 cache: false,
 url: request_save_event,
 data: {
 edited_event: event_data,
 },
 success: function(data) {
 
 if (data.result) {
 
 callendar_get_request_info();
 } else {
 console.log(data.description);
 alert('You should never do that again!');
 }
 }
 });
 };*/