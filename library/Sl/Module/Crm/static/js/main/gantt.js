var crm_object_tag_id;
var gantt_sets_tpl = '<div class="pull-right gantt-ms-edit"><span class="fa fa-angle-down"></span><span class="fa fa-angle-up"></span></div>' +  
                     '<div class="pull-right gantt-ms-add"><span class="fa fa-plus-circle"></span></div>' +  
                     '<div class="btn-group pull-left gantt-hotsets"><span class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-gear"></span></span>'+
                        '<ul class="dropdown-menu" style="display:none;">'+
                          '<li><a href="#" data-scale="%d" data-subscale-date="%F" data-subscale-unit="month" data-action="scale" data-value="day">Day</a></li>'+
                          '<li><a href="#" data-subscale-date="%F" data-scale="%W week" data-subscale-unit="month" data-action="scale" data-value="week">Week</a></li>'+
                          '<li><a href="#" data-scale="%F" data-subscale-unit="year" data-subscale-date="%Y" data-action="scale" data-value="month">Month</a></li>'+
                          '<li><a href="#" data-action="export">Export to Xls <span class="fa fa-download"></span></a></li>'+
                          '<li><a href="#" data-action="screen">Screen</a></li>'+
                        '</ul>'+
                     '</div>';

var gantt_options = {
    url:'/crm/main/ajaxgantt'
}
        
        gantt.config.work_time = true;
     //   gantt.config.sort = true;
        gantt.config.types["ms"] = "ms";
	gantt.locale.labels["type_ms"] = "Milestone";
        
	//sections for tasks with 'meeting' type

	gantt.locale.labels.section_title = "Name";
	gantt.locale.labels.section_details = "Details";
        
	gantt.config.lightbox["ms_sections"] = [
		{name: "title", height: 20, map_to: "text", type: "textarea", focus: true},
		{name: "details", height: 70, map_to: "details", type: "textarea", focus: true},
	];

	
	
	gantt.templates.task_text = function(start, end, task){
		if(task.type == gantt.config.types.ms){
			return "Milestone: <b>" + task.text + "</b>";
		}
		return task.text;
	};

        gantt.templates.tooltip_text = function(start,end,task){
            if (task.id.toString().match(/^new-ms-/)) return 'Add new task';
            return "<b>"+task.text+"</b>"+((task.hasOwnProperty('username') && task.username.length)?" Assigned to <b>"+task.username+"</b>":'')+"<br/><b>From:</b> " + 
            gantt.templates.tooltip_date_format(start)+ 
            " <b>To:</b> "+gantt.templates.tooltip_date_format(end)+"<br><b>Duration:</b> "+task.duration + " days";
        };
  
        gantt.templates.leftside_text = function(start, end, task){
            return start.format("d/mm") + ((task.hasOwnProperty('username') && task.username.length)?"<br>" + task.username:"");
        };
        
        gantt.templates.rightside_text = function(start, end, task){
            return end.format("d/mm") + " <br> "+task.duration + " days";
        };


var $ge;


function loadGanttFromServer(wrapper, cust_opts, just_milestones) {
    var gantt_title = 'Task name';
    
    if ($(wrapper).data('gantt_title')) gantt_title = $(wrapper).data('gantt_title');
    
    var gant_base_opts = {
        readonly:false,
        xml_date : "%Y-%n-%d %H:%i:%s",
        api_date : "%Y-%n-%d %H:%i:%s",
        links: {
            "finish_to_start":"0"
        },
        subscales:[{
            unit:"month", 
            step:1, 
            date:"%F",

        }],
        
        min_column_width: 30,
        task_scroll_offset: 500,
        scroll_size:15,
        scale_height: 30,
        date_scale: '%d',
        grid_width:250, 
        columns:[
            {name:"text", label:gantt_sets_tpl+"<span class='gantt-title'>"+gantt_title+"</span>",  tree:true, width:210, template:tree_text_template},
         /*   {name:"start_date", width:95, label:"Start time", align: "center", template:tree_date_template},
            {name:"duration", label:"Duration",   align: "center", template:tree_duration_template }, */
          //{name:"add", width:44}
        ]
    }
    
    if (crm_object_tag_id > 0){  
        $(wrapper).html('');
        
      /*  $('<div/>').addClass('gantt-ctrls').html('<div class="btn-group scale-ctrl"><button type="button" data-scale="%d" data-subscale-date="%F" data-subscale-unit="month" class="btn btn-xs btn-white active" value="day">Day</button><button type="button" class="btn btn-xs btn-white" data-subscale-date="%F" data-scale="%W week" data-subscale-unit="month" value="week">Week</button><button type="button" class="btn btn-xs btn-white" data-scale="%F" data-subscale-unit="year" data-subscale-date="%Y" value="month">Month</button></div>').appendTo(wrapper);
            
            

        $('.gantt-ctrls').on('click', '.scale-ctrl button:not(.active)', function(){
            $(this).siblings('.active').removeClass('active');
            $(this).addClass('active');
            console.log([$(this).val(),$(this).data('scale')]);
            gantt.config.scale_unit = $(this).val();
            gantt.config.subscales[0].unit = $(this).data('subscale-unit');
            gantt.config.subscales[0].date = $(this).data('subscale-date');
            gantt.config.date_scale = $(this).data('scale');
            var opts = $ge.serialize();
            $ge.clearAll();
            $ge.parse(opts);
            $ge.showDate(new Date());
        });
        */
        
        
        $('<div/>').addClass('gantt-wrap').appendTo(wrapper);
        $('<div/>').attr('id','gantt-context-menu').html('<ul class="dropdown-menu" role="menu"><li><a href="#" data-action="up">Up</a></li><li><a href="#" data-action="down">Down</a></li></ul>').appendTo('body');
        
            //console.debug(response);
                var opts = $.extend({},gant_base_opts, cust_opts);
                
                opts['form_blocks'] = {};
                
                opts['lightbox'] = {};    
                opts.lightbox['sections'] = [
                {
                    name:"description", 
                    height:200, 
                    map_to:"text", 
                    type:"custom_editor", 
                    focus:true
                },
                {
                    name:"time", 
                    height:72, 
                    type:"duration", 
                    map_to:"auto"
                }
                ];       
                $ge = $('div.gantt-wrap',wrapper).dhx_gantt(opts);
                $ge.showDate(new Date());
                
                $ge.templates.grid_row_class = function(start, end, task){
                    var cl = task.id.toString().match(/^[^\d_]*/);
                    return cl.length?cl:'task';
                };
                
                $ge.templates.task_cell_class = function(task, date){
                    
                    if(gantt.config.date_scale == '%d' && !gantt.isWorkTime(date))
                                return "week_end";
                        return "";
                };

            
                $ge.templates.grid_file = function(item) {
                    
                    var s, c = '', d = '';
                    if (!gantt.config.readonly && $ge.users_string.length) {
                        c = 'dropdown-toggle';
                        d= " data-toggle='dropdown' ";
                    }
                    
                    if (item.id.toString().match(/new-.+/)){
                        s = "<div class='gantt_tree_icon user-icon'></div>";
                    } else if (item.hasOwnProperty('userinits') && item.userinits != null && item.userinits.length) {
                        s = "<div class='gantt_tree_icon user-icon'><span class='user-icon-initials "+c+"'"+ d +
                                   ((item.hasOwnProperty('usercolor') && item.usercolor != null && item.usercolor.length)?" style='background-color:#"+item.usercolor+"'":'')+
                                   "title='" + item.username + "'"+
                                   ">"+
                                   item.userinits+
                                   "</span>"+(c.length?$ge.users_string:'')+"</div>";
                    } else {
                        c += item.id.toString().match(/milestone/)?' fa-flag-checkered':' fa-tasks';
                        s = "<div class='gantt_tree_icon user-icon'><span class='user-icon-initials fa "+c+"' "+d+"></span>"+(c.length?$ge.users_string:'')+"</div>";
                    }
                    return s;
                };
                $ge.templates.grid_blank = function(item) {
                    var s = '';
                    if (!gantt.config.readonly && !item.id.toString().match(/new-/)) {
                        s = "<input type='checkbox' "+(item.progress==1?"checked='checked'":"")+">";
                    } 
                    return "<div class='gantt_tree_icon gantt_blank'>"+s+"</div>";
                }; 
                $ge.templates.grid_folder = function(item) {
                    var s, c='', d='';
                    
                    if (!gantt.config.readonly && $ge.users_string.length) {
                        c = 'dropdown-toggle';
                        d= " data-toggle='dropdown' ";
                    }
                    
                    if (item.hasOwnProperty('userinits') && item.userinits != null && item.userinits.length) {
                        s = "<div class='gantt_tree_icon user-icon'><span class='user-icon-initials "+c+"'"+ d +
                                   ((item.hasOwnProperty('usercolor') && item.usercolor != null && item.usercolor.length)?" style='background-color:#"+item.usercolor+"'":'')+
                                   "title='" + item.username + "'"+
                                   ">"+
                                   item.userinits+
                                   "</span>"+(c.length?$ge.users_string:'')+"</div>";
                    } else {
                        c += item.id.toString().match(/milestone/)?' fa-flag-checkered':' fa-tasks';
                        s = "<div class='gantt_tree_icon user-icon'><span class='user-icon-initials fa "+c+"' "+d+"></span>"+(c.length?$ge.users_string:'')+"</div>";
                    }
                    return s;
                };
                
                $ge.templates.task_class=function(start, end, task){
                    var cl = task.id.toString().match(/^[^\d_]*/);
                    var addition = task.hasOwnProperty('level')?' level-'+task.level:'';
                    return (cl.length?cl:'task')+addition;
                };
                
                $ge.setUsers = function(users){
                    this.users = {};
                    this.users_string = '';
                    if (users instanceof Array && users.length){
                        for (var i in users){
                            this.users[users[i].id] = users[i];
                            this.users_string += '<li><a href="#" class="set-user" data-user_id="'+users[i].id+'">'+users[i].name+'</a></li>';
                        }
                    }
                    if (this.users_string.length){
                        this.users_string = '<ul class="dropdown-menu text-left">'+this.users_string+'</ul>';
                    }
                  //  console.log(this.users);
                } 
                
                $ge.loadData = function(response, active){
//                    /console.log('loadData');
                    var opts = {};
                    var scr = $ge.getScrollState();
                    this.setUsers(response.users);
                    this.data_to_load = null;
                    
                    if (response.result){
                        this.clearAll();
                        
                        if (response.hasOwnProperty('holidays') && response.holidays instanceof Array) {
                            for (var i in response.holidays) {
                                var d_r = response.holidays[i].split('-');
                                
                                var d = new Date(d_r[0], parseInt(d_r[1])-1, d_r[2]);
                                
                                if (this.isWorkTime(d)) this.setWorkTime({date:d, hours:false});
                            }
                        }
                        
                        opts['data'] =(response.tasks instanceof Array && response.tasks.length?response.tasks:[]);
                        opts['links'] =(response.links instanceof Array && response.links.length?response.links:[]);
                        
                        $(this.$container).trigger('render.gantt', opts);
                        
                        this.parse(opts);
                        /*if (active) {
                            this.showTask(active);

                        } else {
                            this.showDate(new Date());
                        } */
                        $ge.scrollTo(scr.x, scr.y);
                        
                    }
                    if (this.currentInput instanceof Object && this.currentInput.hasOwnProperty('id')) {
                        $('div[task_id="'+this.currentInput.id+'"] input[data-field="'+this.currentInput.field+'"]').focus();
                    }
                    
                    $(this.$container).trigger('rendered.gantt');
                };
                //First load data
                $ge.registerData = function(response){
                    this.data_to_load = response; 
                }
                $ge.reloadData = function(response, active){
                    var opts = {};
                    var task_fields = ['description', 'duration','end_date', 'start_date', 'text', 'type', 'parent'];
                    var _self = this;
                    if (response.result){
                        //this.clearAll();
                        $ge.registerData(response);
                        
                        opts['data'] =(response.tasks instanceof Array && response.tasks.length?response.tasks:[]);
                        opts['links'] =(response.links instanceof Array && response.links.length?response.links:[]);
                        
                        var links = $ge.serialize().links;
                        var data = $ge.serialize().data;
                        
                        var link_assoc = {}, data_assoc = {},curr_data_assoc = {};
                        var data_to_upd = {};

                        for (var i in opts.data) {
                            data_assoc[opts.data[i].id] = opts.data[i];
                        }

                        for (var i in data) {
                            if ( data_assoc[data[i].id] instanceof Object && !task_comparise(data_assoc[data[i].id], data[i], task_fields)){
                                data_to_upd[data[i].id] = data_assoc[data[i].id];
                            }
                        }
                        var v,t;

                        for (var id in data_to_upd) {
                           t =  _self.getTask(id);
                           for (var f in task_fields) {
                               v = data_to_upd[id][task_fields[f]];
                               if (task_fields[f].match(/_date$/)){
                                 v = v.split(/[^\d]/);

                                 v = new Date(parseInt(v[0]),parseInt(v[1]-1), parseInt(v[2]), parseInt(v[3]), parseInt(v[4]),parseInt(v[5]));
                               }
                               t[task_fields[f]] = v;
                           }   
                           _self.refreshTask(id);
                        }
             
                      
                    }
                    $(this.$container).trigger('rendered.gantt');
                    
                };
        
                $ge.reload = function(just_milestones){
                         this.unselectTask();
                         this.clearAll();
                         this.just_milestones = just_milestones?1:0;
                         $.getJSON(gantt_options.url, {
                                    gantt_tag:crm_object_tag_id,
                                    just_milestones:just_milestones
                                }, function(response) {
                                    $ge.loadData(response);
                                });
                }
                
                $ge.reload(just_milestones);
                
                $ge.form_blocks["custom_editor"] = {
                    render:function(sns) {
                        return "<div class='dhx_cal_ltext'><div class='form-row'>Title&nbsp;<input type='text' class='task-title'></div>"
                        + "<div class='form-row'>Assign to&nbsp;<input type='hidden' class='task-user autocomplete' data-rel='/auth/user/ajaxautocomplete' data-type='21' placeholder='User' title='User'></div>"
                        + "<div>Description<br><textarea class='task-description'></textarea></div></div>";
                    },
                    set_value:function(node, value, task) {
                        $('input.task-title',node).val(value);
                        $('input.task-user',node).val(task.user).attr('data-name',task.username);
                        var u_k = Object.keys($ge.users);
                        
                        if (u_k.length) {
                            $('input.task-user',node).attr('data-filter0','id-in--val:'+u_k.join(','));
                        } else {
                            $('input.task-user',node).attr('data-filter0','id-in--val:0');
                        }
                        $('textarea.task-description',node).val(task.description).html(task.description);
                            
                    },
                    get_value:function(node, task) {
                        task.description =  $('textarea.task-description',node).val() || $('textarea.task-description',node).html();
                        task.user =  $('input.task-user',node).val();    
                        task.username =  $('input.task-user',node).data('name');    
                        return $('input.task-title',node).val();
                    },
                    focus:function(node) {
                        $('input.task-title',node).select().focus();
                            
                    }
                        
                };
                
                
                $ge.ajaxUpdateTask = function (id, task, reload){
                   // console.log(task);
                    var _self = this;
                    if (task instanceof Object && task.hasOwnProperty('id')){
                      
                                
                                    var type=task.id.split('_');
                                    var subaction;

                                    if (type[0] == 'task' && type[1].match(/^[\d]+$/)){
                                        subaction = 'updateTask';
                                    } else if(type[0] == 'task' ){
                                        if (task.text.length){
                                            subaction = 'createTask';
                                        }  else {
                                           /* console.log('1');
                                            _self.deleteTask(id);
                                          
                                            _self.refreshData();
                                            console.log('2');
                                            return false; */
                                            return false;
                                        }

                                        
                                    } else {
                                        subaction = 'updateMilestone';
                                    }

                                    $.get(gantt_options.url,{
                                        subaction:subaction, 
                                        gantt_tag:crm_object_tag_id,
                                        id:task.id,
                                        date_start:task.start_date.format('yyyy-mm-dd HH:MM:ss'),
                                        date_finish:task.end_date.format('yyyy-mm-dd HH:MM:ss'),
                                        status:Math.round(task.progress*100),
                                        name: task.text,
                                        user: task.user,
                                        parent:task.parent,
                                        description: task.description
                                    }, function(data){
                                            if (data.hasOwnProperty('id') && data.id.length){
                                                 _self.changeTaskId(task.id, data.id);
                                            }
                                            if (reload){
                                                _self.reloadData(data, id);
                                            } else {
                                                _self.loadData(data, id);
                                            }
                                       });
                              
                          
                        }
                }
                
                $ge.ajaxReorder = function(task1,task2){
                    if (task1 instanceof Object &&  task1.hasOwnProperty('id') && task2 instanceof Object &&  task2.hasOwnProperty('id')){
                      
                                    var type=task1.id.split('_');
                                    var subaction;

                                    if (type[0] == 'task' ){
                                        subaction = 'reorderTasks';
                                    } else {
                                        subaction = 'reorderMilestones';
                                    }

                                    $.get(gantt_options.url,{
                                        subaction:subaction, 
                                        obj: [
                                           {id: task1.id.split('_')[1], projectorder:task1.projectorder},
                                           {id: task2.id.split('_')[1], projectorder:task2.projectorder},
                                        ],
                                        
                                    }, function(data){
                                         
                                       });
                              
                          
                        }
                }
                
                $ge.upTask = function(id){
                    var task = this.getTask(id);
                    var p_task = this.getPrevTask(task);
                    
                    if (p_task){
                        var o = task.projectorder;
                        task.projectorder = p_task.projectorder;
                        p_task.projectorder=o;

                        this.sort('projectorder', false);
                        $(this.$container).trigger('rendered.gantt');
                        this.ajaxReorder(task, p_task);
                    }
                }
                
                $ge.downTask = function(id){
                    var task = this.getTask(id);
                    var p_task = this.getNextTask(task);
                    if (p_task){
                        var o = task.projectorder;
                        task.projectorder = p_task.projectorder;
                        p_task.projectorder=o;

                        this.sort('projectorder', false);
                        $(this.$container).trigger('rendered.gantt');
                        this.ajaxReorder(task, p_task);
                    }
                }
                
                $ge.getNextTask = function (task){
                    var o = $ge.serialize();
                    var temp_index;
                    var temp_order = 0;
                    
                    for (var i in o.data){
                        if (o.data[i].parent == task.parent && 
                            !o.data[i].id.toString().match(/new/) &&     
                            o.data[i].projectorder > task.projectorder && 
                            (o.data[i].projectorder < temp_order || !temp_order)) {
                            temp_order = o.data[i].projectorder;
                            temp_index = i;
                        }
                    }
                    
                    return (temp_index? this.getTask(o.data[temp_index].id):false);
                }
                
                $ge.getPrevTask = function (task){
                    var o = $ge.serialize();
                    var temp_index;
                    var temp_order = 0;
                    
                    for (var i in o.data){
                        if (o.data[i].parent == task.parent && 
                            o.data[i].projectorder < task.projectorder && 
                            o.data[i].projectorder > temp_order) {
                            temp_order = o.data[i].projectorder;
                            temp_index = i;
                        }
                    }
                    
                    return (temp_index? this.getTask(o.data[temp_index].id):false);
                }
                
                                
                $ge.attachEvent('onTaskIdChange', function(id, new_id){
                   // console.log([id, new_id, $ge.currentInput]);
                     if ($ge.currentInput instanceof Object && $ge.currentInput.hasOwnProperty('id') && $ge.currentInput.id == id){
                            $ge.currentInput.id = new_id; 
                            //console.log($('.gantt_row[task_id="'+new_id+'"] input[data-field="'+$ge.currentInput.field+'"]'));
                            $('.gantt_row[task_id="'+new_id+'"] input[data-field="'+$ge.currentInput.field+'"]').focus();
                    }
                    
                });
                
                
                // по кліку або фокусу на будь-що оновити таск, якщо змінювався:
                $('body').on('click contextmenu.context.data-api',':not(.ui-datepicker-calendar *)', function(e){ if (e.target == e.currentTarget) $ge.confirmChanges(e.target);});
                $('body').on('focus', 'input', function(e){$ge.confirmChanges(e.target); });
                ////////////////////////////////////////////////////////////////        
                
                
                $($ge.$container).on('click', '.set-user', function(){
                    var id = $(this).closest('.gantt_row').attr('task_id');
                    
                    if (id) {
                      
                      $ge.getTask(id).user = $(this).data('user_id');
                      $ge.updateTask(id);
                      
                      
                    }
                });
                $($ge.$container).on('change', '.gantt_tree_icon input[type="checkbox"]', function(){
                    
                    var t_id = $(this).closest('.gantt_row').attr('task_id');
                    if (t_id) {
                       $ge.getTask(t_id).progress = $(this).is(':checked')?1:0;
                       $ge.updateTask(t_id);
                    }
                });
                
                $($ge.$container).on('focus','.new-ms-milestone input', function(){
                    $('#gantt-context-menu.open').removeClass('open');
                });
                
                $($ge.$container).on('focus','.new-ms-milestone input', function(e){
                    
                    $ge.confirmChanges(e.target);
                    $ge.setCurrentInput(e.target);
                    var def_id =  $(this).closest('.gantt_row').attr('task_id');
                    var c =  $ge.getTask(def_id);
                    var cd = new Date();
                    var _cd = new Date();
                    
                    var t = $ge.getChildren(c.parent);
                    for (var i in t){
                        if ($ge.getTask(t[i]).end_date > c.start_date){
                            c.start_date.setTime($ge.getTask(t[i]).end_date.getTime());
                        }
                    }
                    
                    var temp_id = 'task_new-'+cd.getTime();
                    
                    
                    c.end_date.setTime($ge.calculateEndDate(c.start_date, 1).getTime());
                    
                    c.duration = 1;
                    //$ge.refreshTask(def_id);
                    
                    cd.setTime(c.end_date.getTime());
                    _cd.setTime(c.end_date.getTime());
                    
                    $ge.changeTaskId(def_id,temp_id);
                    
                    $ge.refreshTask(temp_id);
                    
                    //console.log(t);
                    var taskId = $ge.addTask({
                        id:def_id,
                        text: '',
                        start_date:cd,
                        end_date:_cd,
                        duration:0,
                        projectorder: c.projectorder + 10,
                        description:''
                    }, c.parent);
                    //console.log([taskId, $ge.getTask(taskId)]);
                    
                    $('.gantt_row[task_id="'+temp_id+'"] input:first').focus().change();
                });
                
                $ge.ajaxCreateTask = function (id, task){
                    var temp_id = id;
                    var _self = this;
                    
                    
                    $.ajax(
                        {type: "GET",
                         url:gantt_options.url,
                         cache: false,
                         data:{
                                subaction:task.parent.length?'createTask':'createMilestone', 
                                gantt_tag:crm_object_tag_id,
                                date_start:task.start_date.format('yyyy-mm-dd HH:MM:ss'),
                                date_finish:task.end_date.format('yyyy-mm-dd HH:MM:ss'),
                                name: task.text,
                                parent:task.parent,
                                user:task.user,    
                                description: task.description
                            }, 
                        success: function(data){
                            if (data.result){
                              //  _self.loadData(data)
                            //  console.log(['ajaxCreateTask2', _self.serialize(),task, temp_id, data.id]);
                              if (task.parent.length){
                                  _self.changeTaskId(temp_id, data.id);
                                  
                                  for (var i in data.tasks) {
                                  if (data.tasks[i].id == data.id) {
                                            if (data.tasks[i].hasOwnProperty('level')){
                                              _self.getTask(data.id).level = data.tasks[i].level;
                                              _self.refreshTask(data.id)

                                              }
                                            break;
                                        }
                                    }
                              } else {
                                  _self.loadData(data, data.id);
                                  _self.showLightbox(data.id);
                              }
                            }
                           // $('div[task_id="'+data.id+'"] input[data-field="text"]').focus();
                        }
                    });
                }
                
                $ge.attachEvent("onLightbox", function (){
                 
                    if ($('.dhx_cal_light input.task-user.autocomplete').length){
                        assignAutocomplete($('.dhx_cal_light'));
                    }
                });
                $ge.attachEvent("onTaskClick", function(id,e){
                     this.showTask(id);   //any custom logic here
                 });
                 
                $ge.attachEvent("onAfterTaskAdd", function(id, task){
                    if (id.toString().match(/^[\d]+$/)) this.ajaxCreateTask(id,task);
                  
                });
                $ge.attachEvent("onTaskCreated", function(task){
                    task.text = task.parent.length?'New Task':"New Milestone";
                    task.type = task.parent.length?'task':"ms";
                });
                $ge.attachEvent("onAfterTaskUpdate", function(id,task){
                    
                    this.ajaxUpdateTask(id,task);
                     
                });
                /*
                 * 
                $ge.attachEvent("onGanttRender", function(id) {
                    //console.log($('input.field-date',$ge.$container));
                    $('input.field-date',$ge.$container).datepicker(datepicker_options);
                }); */
                $ge.attachEvent("onBeforeLightbox", function(id) {
                    if (id.toString().match(/^[\w]+_[\d]+$/)) return true;
                });
                $ge.attachEvent("onAfterLinkDelete", function(id,item){
                    var _self = this;
              
                    $.get(gantt_options.url,{
                        subaction:'deleteRelation', 
                        gantt_tag:crm_object_tag_id,
                        relation:item
                                                
                    }, function(data){
                        _self.loadData(data, item.source);
                    });
                });
                $ge.attachEvent("onAfterTaskDelete", function(id,item){
                    var _self = this;
                    var type=item.id.split('_');
                    $.get(gantt_options.url,{
                        subaction:(type[0]=='task'?'deleteTask':'deleteMilestone'),
                        gantt_tag:crm_object_tag_id,
                        id:id
                                                
                    }, function(data){
                        _self.loadData(data);
                    });
                });
                $ge.attachEvent("onAfterLinkAdd", function(id,item){
                    var _self = this;
                    $.get(gantt_options.url,{
                        subaction:'relateTasks', 
                        gantt_tag:crm_object_tag_id,
                        relation:item

                    }, function(data){
                        if (!data.result){
                        //   _self.deleteLink(id);

                        } else {
                            _self.loadData(data, item.source);
                        }
                    });
                });
                
                $ge.setCurrentInput = function(el){
                    if ($(el).is('.gantt_row input') ){
                        this.currentInput = {id:$(el).closest('.gantt_row').attr('task_id'),field:$(el).data('field')}; 
                    }
                }
                
                $($ge.$container).on('click', '.gantt-ms-add', function(){
                    
                    var taskId = $ge.addTask({
                        text: 'new Milestone',
                        duration:1,
                        start_date:new Date(),
                        type:'ms',
                        description:''
                    });
                     $ge.registerChanges(taskId);
                    
                     
                });
                
                $($ge.$container).on('focus','input', function(){
                    
                    var task_id = $(this).closest('.gantt_row').attr('task_id');
                    if (!task_id.match(/^new-/)){ 
                        $ge.setCurrentInput(this);
                       // console.log(['focus','input', this, task_id]);
                    }
                    
                });
                
                $('body').on('select2-selecting', 'input.task-user', function(e){
                    $(this).data('name', e.object.text);
                    
                });
                
                
                // Призначення datepicker на льоту
                $($ge.$container).on('click focus','input.field-date:not(.hasDatepicker)', function(){
                    $(this).datepicker(datepicker_options);
                    $(this).focus();
                });
                
                // Обробка швидких змін
                $ge.registerChanges = function(task_id){
                    
                    this.changes = task_id?task_id:'';
                }
                
                $ge.getChangesId = function(){
                    return (this.hasOwnProperty('changes'))?this.changes:'';
                }
                
                $ge.confirmChanges = function(el){
                    
                    var task_id = null;
                    if ($(el).is('.gantt_row input')){
                        
                        task_id = $(el).closest('.gantt_row').attr('task_id');
                       // console.log(['confirmChanges', 'is gantt_row input', task_id]);
                    } else {
                       // console.log(['confirmChanges', 'is not gantt_row input', el]);
                        this.currentInput = {};
                    }
                    if (this.getChangesId().length && this.getChangesId() != task_id){
                        this.refreshTask(this.getChangesId());
                        this.ajaxUpdateTask(this.getChangesId(), this.getTask(this.getChangesId()), task_id); 
                        this.registerChanges('');
                    } else if (!task_id && !this.getChangesId().length && this.data_to_load instanceof Object){
                        this.loadData(this.data_to_load);
                    }
                }
                
               
                ////////////////////////////////////////
                // Обробка іконки редагування
                //////////////////////////////////////////////
                
                $($ge.$container).on('click','.open-lightbox', function(e){
                    var id = $(this).closest('div.gantt_row').attr('task_id');
                    if (id.toString().length) $ge.showLightbox(id);
                    
                });
                
                
 
                ////////////////////////////////////////////////////////////////
                // Швидке редагування тасків:
                
                // Обробка Enter
                $($ge.$container).on('keypress','input', function(e){
                    if  (e.keyCode === 13) {
                        
                        $(this).change().blur();
                        
                        $(this).closest('.gantt_row').next('.gantt_row:has(input)').find('input:first').focus();
                    }
                });
                
                $($ge.$container).on('change','input', function(e){
                 //   console.log(this);
                    var task_id = $(this).closest('.gantt_row').attr('task_id');
                    if ($ge.currentInput instanceof Object && $ge.currentInput.hasOwnProperty('id') && $ge.currentInput.id == task_id){
                             $ge.currentInput = {};
                    }
                    if ($ge.getTask(task_id)){
                        if ($(this).data('field') == 'start_date'){
                            var val = $(this).val().split('-');
                            var date = new Date(val);
                            $ge.getTask(task_id).start_date = date;
                            $ge.getTask(task_id).end_date = new Date(date.getTime());
                            $ge.getTask(task_id).end_date.setDate(date.getDate() + $ge.getTask(task_id).duration);
                        } else if ($(this).data('field') == 'duration'){
                            $ge.getTask(task_id).end_date = new Date($ge.getTask(task_id).start_date.getTime());
                            $ge.getTask(task_id).end_date.setDate($ge.getTask(task_id).start_date.getDate() + parseInt($(this).val())); 
                            $ge.getTask(task_id).duration = $(this).val();
                        } else {
                            $ge.getTask(task_id)[$(this).data('field')] = $(this).val();
                        }
                        $ge.registerChanges(task_id);
                    }
                      
                });
               $($ge.$container).on('click', '.gantt-hotsets ul.dropdown-menu li a', function(){
                   switch ($(this).data('action')){
                       case ('scale'):  gantt.config.scale_unit = $(this).data('value');
                                        gantt.config.subscales[0].unit = $(this).data('subscale-unit');
                                        gantt.config.subscales[0].date = $(this).data('subscale-date');
                                        gantt.config.date_scale = $(this).data('scale');
                                        var opts = $ge.serialize();
                                        $ge.clearAll();
                                        $($ge.$container).trigger('gantt.change.scale', opts);
                                        $ge.parse(opts);
                                        $ge.showDate(new Date());
                                        break;
                        case ('export'): window.open('/crm/main/ganttxlsexport/tag_id/'+crm_object_tag_id+'/just_milestones/'+$ge.just_milestones, '_blank');
                                        break;
                                        
                        case ('screen'): 
                                        if ($('div#milestone-gantt').css('position') != 'fixed') {
                                                $('div#milestone-gantt').addClass('gantt-fixed').css({
                                                    position:'fixed',
                                                    top:0,
                                                    left:0,
                                                    height:'100%',
                                                    width:'100%',
                                                    zIndex:9000
                                                });
                                                $('div#milestone-gantt .gantt-wrap').data('returnHeight',$('div#milestone-gantt .gantt-wrap').css('height')).css('height','100%');
                                                
                                            } else {
                                               
                                               $('div#milestone-gantt').removeClass('gantt-fixed').css({
                                                    position:'relative',
                                                    top:null,
                                                    left:null,
                                                    height:'auto',
                                                    width:'auto',
                                                    zIndex:'auto'
                                                }); 
                                                $('div#milestone-gantt .gantt-wrap').css('height',$('div#milestone-gantt .gantt-wrap').data('returnHeight'));
                                            }
                                            var opts = $ge.serialize();
                                            $ge.clearAll();
                                            $ge.parse(opts);
                                            //console.log([document.body.clientHeight,document.body.clientWidth]);
                                        break;            
                   }
                    
                });
                

                $('#gantt-context-menu').on('click', 'a', function(){
                   var id = $($('#gantt-context-menu').data('target')).attr('task_id');
                   $('#gantt-context-menu').removeClass('open');
                    switch($(this).data('action')){
                       case 'up': $ge.upTask(id);
                                    break;
                       case 'down': $ge.downTask(id);
                                    break;        
                   } 
                   
                });
         
                
            }
            
            $(wrapper).on('render.gantt gantt.change.scale',function(e, o){
                
                var opts = (o instanceof Object && o.hasOwnProperty('data'))?o:$ge.serialize();
                
                //console.log(opts);
                $($ge.$container).data('dates',null);
                if (opts instanceof Object && opts.hasOwnProperty('data') && opts.data instanceof Array && opts.data.length){
                    var min_date, max_date;
                    for (var i in opts.data) {
                        if (opts.data[i].start_date){
                            d = ((opts.data[i].start_date instanceof Date)?new DateTime(opts.data[i].start_date.getTime()):gantt.templates.api_date(opts.data[i].start_date));
                            min_date = (min_date && min_date < d)?min_date:d;
                        }
                        if (opts.data[i].end_date){
                            d = ((opts.data[i].end_date instanceof Date)?new DateTime(opts.data[i].end_date.getTime()):gantt.templates.api_date(opts.data[i].end_date));
                            max_date = (max_date && max_date > d)?max_date:d;
                        }
                   // console.log([gantt.config.scale_unit,opts.data[i].start_date,min_date,opts.data[i].end_date, max_date]);
                    }
                    
                    $($ge.$container).data('dates',{min:min_date.format('d/mm'), max:max_date.format('d/mm'), days:$ge.calculateDuration(min_date, max_date)});

                    
                    switch(gantt.config.scale_unit){
                        case 'day' : 
                        case 'week' : min_date.setDate(min_date.getDate() - 7); max_date.setDate(max_date.getDate() + 14); break;
                        case 'month' : min_date.setMonth(min_date.getMonth() - 1); max_date.setMonth(max_date.getMonth() + 2); break;
                            
                    }
                    
                    gantt.config.start_date = min_date;
                    gantt.config.end_date = max_date;
                    
                }
               // console.log([e, opts, min_date, max_date]);
                
            });
            
            $(wrapper).on('rendered.gantt',function(){
                if ($($ge.$container).data('dates') instanceof Object && $('.gantt-title:not([data-toggle="tooltip"])', $ge.$container).length) {
                        
                        $('.gantt-title:not([data-toggle="tooltip"])', $ge.$container).attr('data-toggle','tooltip')
                                                                                      .attr('data-placement','bottom')
                                                                                      .attr('data-original-title',$($ge.$container).data('dates').min + ' — ' + $($ge.$container).data('dates').max + ' ('+$($ge.$container).data('dates').days + ' days)').tooltip();
                }
                    
                if (!gantt.config.readonly) $('.gantt_row:not([data-toggle])', wrapper).attr({'data-toggle':'context','data-target':"#gantt-context-menu" }) ;
                
            });
            
}

function tree_text_template (task) {
    
    if (gantt.config.readonly || task.type =='ms'){
       return task.text;
    } else {
        return '<input type="text"  data-field="text" value="'+task.text+'">'+ (task.id.toString().match(/.?new-.+/)?'':' <i class="open-lightbox fa fa-angle-right"></i>');
    }
}

function tree_date_template (task) {
    if (gantt.config.readonly || task.type =='ms'){
       return gantt.templates.date_grid(task.start_date);
    } else if (task.id.toString().match(/^new-ms.+/)) {
        return '';
    }
    else
    {
        return '<input type="text" data-field="start_date" value="'+gantt.templates.date_grid(task.start_date)+'" class="field-date" size="10" readonly="readonly">';
    }
}

function tree_duration_template (task) {
    if (gantt.config.readonly || task.type =='ms'){
       return task.duration;
    } else if (task.id.toString().match(/^new-ms.+/)) {
        return '';
    } else {
        return '<input type="text" value="'+task.duration+'" class="int" data-field="duration">';
    }
}

function task_comparise (t1, t2, fields) {
 
    for (var i in fields){
        if (t1[fields[i]] != t2[fields[i]]) return false;
    }
    return true;
}



