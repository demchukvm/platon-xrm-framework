$(function() {

    $("#tabs").tabs();


    $('.datatable tbody input[type="checkbox"]:not(.select_all), .datatable tbody input[type="radio"]:not(.select_all)').live('change', function() {
        var id = $(this).parents('tr:first').attr('id').split('-');
        var name = $(this).parents('tr:first').find('.dt-customer-name').text();
        var close_popup = false;

        var $list = $('#modulerelation_mailergroupcustomer_list');
        var $input = $('#modulerelation_mailergroupcustomer');
        id = id[2];
        if (id > 0) {
            if ($(this).is(':checked')) {
                var $vals = $input.val().split(';');
                $vals[$vals.length] = id;
                $input.val($vals.join(';'));
                $list.append('<li data-id="' + id + '">' + name + '</li>');
                //$list.parents('blockquote ').show();


            } else {
                var $vals = $input.val().split(';');
                
                var index = $vals.indexOf(id);
                if (index > -1) {
                    $vals.splice(index, 1);
                }
                $input.val($vals.join(';'));
                $list.find('li[data-id="'+id+'"]').remove();

            }
        }

    });
    
        $('body').on('click', '.datatable .select_all', function() {

        var $t_wrap = $(this).parents('.table-controls-wrapper:first');
        var id_s = [];
        var $inp = $('.datatable tbody tr input[type="checkbox"]:not(:checked)', $t_wrap);
        var $input = $('#modulerelation_mailergroupcustomer');
        var $list = $('#modulerelation_mailergroupcustomer_list');
        $inp.each(function(i,v) {
           // console.log(i);
            var data = $(v).parents('tr:first').data();
            var id = data.id;
            var name = $('.datatable #data-id-'+id+'').find('.dt-customer-name').text();
            
            var $vals = $input.val().split(';');
            $vals[$vals.length] = id;
                $input.val($vals.join(';'));
                $list.append('<li data-id="' + id + '">' + name + '</li>');
               // $list.parents('blockquote ').show();
        });
   
    });
    
    $('body').on('click', '.datatable .deselect_all', function() {

        $('#modulerelation_mailergroupcustomer').val('');
        $('#modulerelation_mailergroupcustomer_list').html('');
        
    }); 
    $('body').on('click', '.selected_models .selected_data', function() {
        var $input = $('#modulerelation_mailergroupcustomer');
        var $list = $('#modulerelation_mailergroupcustomer_list');
        var $id = $(this).attr('id').split('-');
        var id=$id[1];

                var $vals = $input.val().split(';');
                
                var index = $vals.indexOf(id);
                if (index > -1) {
                    $vals.splice(index, 1);
                }
                $input.val($vals.join(';'));
                $list.find('li[data-id="'+id+'"]').remove();
    }); 

});

function selected_items(ids)
{
    $('#modulerelation_mailergroupcustomer').val('');
    $('#modulerelation_mailergroupcustomer_list').html('');
    $('#modulerelation_mailergroupcustomer').val(ids.join(';'));
}
