<?php

return array(
    'name' =>
    array(
        'label' => 'Name',
        'searchable' => true,
        'sortable' => true,
    ),
    'task_set' =>
    array(
        'label' => 'Task set',
    ),
    'type' =>
    array(
        'label' => 'Type',
        'searchable' => true,
        'select' => true,
        'sortable' => true,
    ),
    'status' =>
    array(
        'label' => 'Status',
        'searchable' => true,
        'sortable' => true,
    ),
    'description' =>
    array(
        'label' => 'Description',
    ),
    'remind' =>
    array(
        'label' => 'Remind',
        'searchable' => true,
        'sortable' => true,
    ),
    'taskuser.name' =>
    array(
        'label' => 'Ответственный',
    ),
    'date_start' =>
    array(
        'type' => 'date',
        'label' => 'Date',
    ),
    'date_finish' =>
    array(
        'label' => 'Due',
        'type' => 'date',
        'sortable' => true,
        'searchable' => true,
    ),
    'tasknote.name' =>
    array(
        'label' => 'Последняя заметка',
    ),
    'archived' =>
    array(
        'label' => 'ARCHIVED',
    ),
    'milestonetask.name' =>
    array(
        'label' => 'Milestone',
        'sWidth' => '53px',
    ),
    'tasktag.name' => array(
        'label' => 'Tags',
        'grouped' => true,
    ),
    'taskmaintag.name' => array(
        'label' => 'Main tag',
    ),
);
