<?php

return array(
    'name' =>
    array(
        'order' => 10,
        'label' => 'Title',
        'searchable' => true,
        'sortable' => true,
    ),
    'task_set' =>
    array(
        'order' => 20,
        'label' => 'Task set',
    ),
    'type' =>
    array(
        'order' => 30,
        'label' => 'Type',
        'searchable' => true,
        'select' => true,
        'sortable' => true,
    ),
    'status' =>
    array(
        'order' => 40,
        'label' => 'Status',
        'searchable' => true,
        'select' => true,
        'sortable' => true,
    ),
    'description' =>
    array(
        'order' => 50,
        'label' => 'Description',
    ),
    'remind' =>
    array(
        'order' => 60,
        'label' => 'Remind',
        'searchable' => true,
        'select' => true,
        'sortable' => true,
    ),
    'taskuser.name' =>
    array(
        'order' => 65,
        'label' => 'Ответственный',
    ),
    'date_start' =>
    array(
        'order' => 70,
        'label' => 'Date',
    ),
    'date_finish' =>
    array(
        'order' => 80,
        'label' => 'Due',
    ),
    'tasknote.name' =>
    array(
        'order' => 90,
        'label' => 'Последняя заметка',
    ),
    'archived' =>
    array(
        'order' => 1000,
        'label' => 'ARCHIVED',
    ),
    'milestonetask.name' =>
    array(
        'label' => 'Milestone',
    ),
);
