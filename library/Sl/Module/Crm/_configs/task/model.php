<?php
return array (
  'name' => 
  array (
    'label' => 'Название',
    'type' => 'text',
    'required' => true,
  ),
  'task_set' => 
  array (
    'label' => 'Задача',
    'type' => 'textarea',
  ),
  'type' => 
  array (
    'label' => 'Тип',
    'type' => 'select',
    'default_value' => 2,
  ),
  'status' => 
  array (
    'label' => 'Status',
    'type' => 'select',
    'default_value' => 2,
  ),
  'description' => 
  array (
    'label' => 'Description',
    'type' => 'textarea',
  ),
  'mail_topic' => 
  array (
    'label' => 'Тема',
    'type' => 'text',
    'class' => 'span7',
  ),
  'mail_body' => 
  array (
    'label' => 'Тело письма',
    'type' => 'textarea',
    'class' => 'span7',
  ),
  'remind' => 
  array (
    'label' => 'Напомнить за',
    'type' => 'select',
    'default_value' => 5,
  ),
  'date_start' => 
  array (
    'label' => 'Start date',
    'type' => 'date',
    'class' => 'current-datetime',
  ),
  'date_finish' => 
  array (
    'label' => 'Due',
    'type' => 'datetime',
    'class' => 'tommorow-datetime',
  ),
  'create' => 
  array (
    'label' => 'CREATE',
    'type' => 'text',
  ),
  'id' => 
  array (
    'label' => 'ID',
    'type' => 'text',
  ),
  'active' => 
  array (
    'label' => 'ACTIVE',
    'type' => 'checkbox',
  ),
  'archived' => 
  array (
    'label' => 'ARCHIVED',
    'type' => 'text',
  ),
  'milestonetask.name' => 
  array (
    'label' => 'Milestone',
  ),
  'projectorder' => 
  array (
    'label' => 'PROJECTORDER',
    'type' => 'text',
  ),
);
