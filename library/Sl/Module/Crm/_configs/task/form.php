<?php
return array (
  'modulerelation_taskmailergroup' => 
  array (
    'label' => 'Группы рассылки',
    'request_fields' => 
    array (
      1 => 'name',
      2 => 'description',
    ),
  ),
  'modulerelation_taskuser' => 
  array (
    'label' => 'Ответственный',
    'field_filters' => 
    array (
      0 => 
      array (
        'field' => 'userroles:id',
        'matching' => 'in',
        'value' => 'val:33,31,1,29',
      ),
    ),
    'request_fields' => 
    array (
      1 => 'name',
      2 => 'login',
      3 => 'email',
      4 => 'userroles:name',
    ),
  ),
  'modulerelation_taskfile' => 
  array (
    'label' => 'Прикрепить файл',
  ),
  'modulerelation_taskemail' => 
  array (
    'label' => 'Email',
    'request_fields' => 
    array (
      0 => 'emailemaildetails:name',
      1 => 'emailemaildetails:city',
      2 => 'emailemaildetails:phone',
      3 => 'emailemaildetails:country',
      4 => 'emailemaildetails:company',
      5 => 'emailemaildetails:ballans',
    ),
  ),
  'modulerelation_tasknote' => 
  array (
    'label' => 'Заметки',
    'include_decorator' => 'FilesListTable',
    'show_field' => 
    array (
      0 => 'description',
      1 => 'create',
    ),
    'show_field_label' => 
    array (
      0 => 'Событие',
      1 => 'Результат',
      2 => 'Дата',
    ),
  ),
  'taskuser.name' => array(
      'label' => 'Assignee',
  ),
  'milestonetask.name' => array(
      'label' => 'Milestone',
  ),
  'taskmaintag.name' => array(
      'label' => 'Main tag',
  ),
  'tasktag.name' => array(
      'label' => 'Tags',
  ),
  'date_start' => array(
      'label' => 'Date start',
      'type' => 'text',
  ),
  'date_finish' => array(
      'label' => 'Deadline',
      'type' => 'datetime',
  ),
);
