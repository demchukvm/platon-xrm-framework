<?php

return array(
    '_default' =>
    array(
        'fields' =>
        array(
            'name',
            'date_finish',
        ),
        'name' => '_default',
        'label' => 'По-умолчанию',
    ),
    '_popup' =>
    array(
        'fields' =>
        array(
              'id',
              'name',
              'taskuser.name',
              'milestonetask.name',
              'date_start',
              'date_finish',
              
                
        ),
        'name' => '_popup',
        'label' => 'По-умолчанию',
        'type' => 'popup',
    ),
    '_detailed' =>
    array(
        'fields' =>
        array(
            'name',
            'description',
            'taskuser.name',
            'date_start',
            'date_finish',
            'milestonetask.name',
            'taskmaintag.name',
            'tasktag.name',
        ),
        'name' => '_default',
        'label' => 'По-умолчанию',
    ),
);
