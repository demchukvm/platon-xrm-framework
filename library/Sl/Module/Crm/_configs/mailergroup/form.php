<?php
return array (
  'modulerelation_mailergroupemail' => 
  array (
    'label' => 'Email',
    'sort_order' => 20,
    'request_fields' => 
    array (
      1 => 'emailemaildetails:name',
      2 => 'emailemaildetails:city',
      3 => 'emailemaildetails:phone',
      4 => 'emailemaildetails:country',
      5 => 'emailemaildetails:company',
      6 => 'emailemaildetails:ballans',
    ),
  ),
  'modulerelation_mailergroupcustomer' => 
  array (
    'label' => 'Клиент',
  ),
  'name' => 
  array (
    'label' => 'Название',
    'type' => 'text',
    'sort_order' => 10,
  ),
  'description' => 
  array (
    'label' => 'Описание',
    'type' => 'textarea',
    'sort_order' => 200,
  ),
);
