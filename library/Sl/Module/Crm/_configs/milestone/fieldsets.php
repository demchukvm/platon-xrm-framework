<?php
return array (
 '_default' =>
    array(
        'fields' =>
        array(
            0 => 'name',
            1 => 'start',
            2 => 'end', 
        ),
        'name' => 'default',
        'label' => 'По-умолчанию',
    ),
 '_popup' =>
    array(
        'fields' =>
        array(
            'name',
        ),
        'name' => 'popup',
        'label' => 'По-умолчанию',
        'type' => 'popup',
    ),
);
