<?php
return array (
  'start' => 
  array (
    'label' => 'START',
    'type' => 'date',
  ),
  'end' => 
  array (
    'label' => 'END',
    'type' => 'date',
  ),
  'name' => 
  array (
    'label' => 'NAME',
    'type' => 'text',
  ),
  'description' => 
  array (
    'label' => 'DESCRIPTION',
    'type' => 'text',
  ),
  'status' => 
  array (
    'label' => 'STATUS',
    'type' => 'text',
  ),
  'projectorder' => 
  array (
    'label' => 'PROJECTORDER',
    'type' => 'text',
  ),
);
