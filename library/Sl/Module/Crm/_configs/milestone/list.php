<?php

return array(
    'name' =>
    array(
        'order' => 10,
        'label' => 'Name',
    ),
    'start' =>
    array(
        'order' => 20,
        'label' => 'Start',
    ),
    'end' =>
    array(
        'order' => 30,
        'label' => 'End',
    ),
);
