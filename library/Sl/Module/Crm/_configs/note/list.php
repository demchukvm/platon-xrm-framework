<?php
return array (
  'name' => 
  array (
    'order' => 10,
    'label' => 'Название',
  ),
  'description' => 
  array (
    'order' => 20,
    'label' => 'Описание',
  ),
  'archived' => 
  array (
    'order' => 30,
    'label' => 'ARCHIVED',
  ),
);
