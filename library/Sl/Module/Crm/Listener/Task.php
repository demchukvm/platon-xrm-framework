<?php

namespace Sl\Module\Crm\Listener;

class Task extends \Sl_Listener_Abstract implements \Sl_Listener_Model_Interface, \Sl\Listener\Cron, \Sl\Listener\Action\Before\Create {

    const RELATED_NEXT_TASK_RELATION = 'relatednexttask';
    const RELATED_PREV_TASK_RELATION = 'reverserelatednexttask';
    const RELATED_MILESTONE_RELATION = 'milestonetask';
    const RELATED_MILESTONE_MAINTAG = 'milestonemaintag';

    protected $_updated_tasks_chain = array();
    protected $_updated_ms_chain = array();

    public function onAfterSave(\Sl_Event_Model $event) {
        //Перевірка зміни дат. Якщо є наступні пов'язані завдання, перемімтити їх:
        $task = $event->getModel();
       
        if ($task instanceof \Sl\Module\Crm\Model\Task && !$task->getArchived() && $task->getActive() && !isset($this->_updated_tasks_chain[$task->getId()])) {
             
            $task = $task->issetRelated(self::RELATED_NEXT_TASK_RELATION) ? $task : \Sl_Model_Factory::mapper($task)->findRelation($task, self::RELATED_NEXT_TASK_RELATION);
            $next = $task->fetchRelated(self::RELATED_NEXT_TASK_RELATION);
            
            $first_task = !count($this->_updated_tasks_chain);
            
            $this->_updated_tasks_chain[$task->getId()]=$task->getId();
            $prev = $event->getModelBeforeUpdate();
            
            $task = $task->issetRelated(self::RELATED_MILESTONE_RELATION) ? $task : \Sl_Model_Factory::mapper($task)->findRelation($task, self::RELATED_MILESTONE_RELATION);
            $milestone = $task->fetchOneRelated(self::RELATED_MILESTONE_RELATION);
            if ($milestone instanceof \Sl\Module\Crm\Model\Milestone){
                $this->_updated_ms_chain[$milestone->getId()] = $milestone;
            }
            
            if ($prev->getId() && $prev->getDateFinish() != $task->getDateFinish()){
         
                $date_finish = \DateTime::createFromFormat($task::FORMAT_TIMESTAMP, $task->getDateFinish());
                $prev_date_finish = \DateTime::createFromFormat($prev::FORMAT_TIMESTAMP, $prev->getDateFinish());
               // print_r(array($prev_date_finish, $date_finish));
              //  $delta = \Sl_Model_Factory::mapper('holiday', 'crm')->countWorkDays($prev_date_finish>$date_finish?$date_finish:$prev_date_finish, $prev_date_finish>$date_finish?$prev_date_finish:$date_finish);
               // $method = $prev_date_finish>$date_finish?'sub':'add';
                foreach ($next as $id => $obj){
                    $obj = $obj instanceof \Sl\Module\Crm\Model\Task?$obj:\Sl_Model_Factory::mapper($task)->find($id);
                    if ($obj->getArchived()) continue;
                    
                    $date_start = \DateTime::createFromFormat($obj::FORMAT_TIMESTAMP, $obj->getDateStart());
                    $old_length = \Sl_Model_Factory::mapper('holiday', 'crm')->countWorkDays($prev_date_finish, $date_start);
                    
                    $new_date_start = \Sl_Model_Factory::mapper('holiday', 'crm')->getWorkEnd($date_finish,$old_length);
                    
                    switch($new_date_start->format('N')){
                        case 6: $new_date_start->add(new \DateInterval('P2D')); break;
                        case 7: $new_date_start->add(new \DateInterval('P1D')); break;
                    }
                    
                    $work_days = \Sl_Model_Factory::mapper($obj)->calculateTaskLength($obj);
                 //   print_r(array($work_days, $obj));
                    
               
                    $obj->setDateStart($new_date_start);
                    $obj = \Sl_Model_Factory::mapper($obj)->calculateTaskDateFinish($obj, $work_days);
                  //  print_r($obj);
                  //  die; 
                    $obj = \Sl_Model_Factory::mapper($obj)->save($obj, true);    

                }
                
            }
            
            //Якщо таск не змінювався, просто подивимось, чи нема серед наступних нового, якого треба посунути вперед
            if ($first_task) {
               $min_date = \DateTime::createFromFormat($task::FORMAT_TIMESTAMP, $task->getDateStart());
               foreach ($next as $id => $obj){
                    $obj = $obj instanceof \Sl\Module\Crm\Model\Task?$obj:\Sl_Model_Factory::mapper($task)->find($id);
                    if ($obj->getArchived()) continue;
                    
                    $date_start = \DateTime::createFromFormat($obj::FORMAT_TIMESTAMP, $obj->getDateStart());
                   

                    if ($min_date > $date_start) {

                        $task_length = $date_start->diff($date_finish);
                        $work_days = \Sl_Model_Factory::mapper($obj)->calculateTaskLength($obj);
                        $date_start = $min_date;
                        //$date_finish->add($delta);
                        $obj->setDateStart($date_start->format($obj::FORMAT_TIMESTAMP));
                        $obj = \Sl_Model_Factory::mapper($obj)->calculateTaskDateFinish($obj, $work_days);

                        \Sl_Model_Factory::mapper($obj)->save($obj);
                    }

                }
                
                if (count($this->_updated_ms_chain)){
                    foreach($this->_updated_ms_chain as $milestone)    {
                        \Sl_Model_Factory::mapper($milestone)->recalcBorders($milestone, true);
                    }
                    
              
                }
            }
            
        }
    }

    public function onBeforeSave(\Sl_Event_Model $event) {
        //Перевірка зміни дат. Якщо є попереднє пов'язане завдання, сильно не переміщувати:

        $object = $event->getModel();
            
        if ($object instanceof \Sl\Module\Crm\Model\Milestone ) {
          
            if ($object->getId() && !$object->getArchived() && $object->getActive()) {
                $object =   \Sl_Model_Factory::mapper($object)->recalcBorders($object);
            }
        
            if (!$object->getId() && !$object ->getProjectorder() && count($object->fetchRelated(self::RELATED_MILESTONE_MAINTAG))){
                
                $tag = $object->fetchOneRelated(self::RELATED_MILESTONE_MAINTAG);
                
                $tag = $ms instanceof \Sl_Model_Abstract?$tag:  \Sl_Model_Factory::mapper('tag', 'kcomments')->find($tag);
                $object ->setProjectorder(\Sl_Model_Factory::mapper($object)-> getMaxMilestoneOrderForTag($tag));
            }
          
        } elseif ($object instanceof \Sl\Module\Crm\Model\Task) {
            $task_b_u = $event->getModelBeforeUpdate();
            $prev_b_u = ($task_b_u->getId() && $task_b_u->issetRelated(self::RELATED_PREV_TASK_RELATION)) ? $task_b_u->fetchOneRelated(self::RELATED_PREV_TASK_RELATION) : null;
            $object = $object->issetRelated(self::RELATED_PREV_TASK_RELATION) ? $object : \Sl_Model_Factory::mapper($object)->findRelation($object, self::RELATED_PREV_TASK_RELATION);
            $prev = $object->fetchOneRelated(self::RELATED_PREV_TASK_RELATION);
            $prev = (is_int($prev) || (is_string($prev) && intval($prev) > 0)) ? \Sl_Model_Factory::mapper($object)->find($prev) : $prev;

            //Якщо є поточна прив'язка і або не було попередньої, або дата початку змінилася


            if ($prev instanceof \Sl\Module\Crm\Model\Task && !$prev->setArchived($archived)) {

                try {
                    $date_start = \DateTime::createFromFormat($object::FORMAT_TIMESTAMP, $object->getDateStart());
                    $date_finish = \DateTime::createFromFormat($object::FORMAT_TIMESTAMP, $object->getDateFinish());
                    $min_date = \DateTime::createFromFormat($object::FORMAT_TIMESTAMP, $prev->getDateFinish());
                    
                    if ($min_date > $date_start) {
                        $delta = $min_date->diff($date_start);
                        $task_length = $date_start->diff($date_finish);
                        
                        $date_start = $min_date;
                        //$date_finish->add($delta);
                        $object->setDateStart($date_start->format($object::FORMAT_TIMESTAMP));
                        $object->setDateFinish($date_start->add($task_length)->format($object::FORMAT_TIMESTAMP));
                    }
                } catch (\Exception $e) {
                    
                }
            }
            
            if (!$object->getId() && !$object ->getProjectorder() && count($object->fetchRelated(self::RELATED_MILESTONE_RELATION))){
                $ms = $object->fetchOneRelated(self::RELATED_MILESTONE_RELATION);
                $ms = $ms instanceof \Sl_Model_Abstract?$ms:  \Sl_Model_Factory::mapper('milestone', 'crm')->find($ms);
                $object ->setProjectorder(\Sl_Model_Factory::mapper($object)-> getMaxTaskOrderForMilestone($ms));
            }
            
        }
    }

    public function onRun(\Sl\Event\Cron $event) {

        $where = '(type = 1 or type = 2) and status = 2 and send_email = 0';
        $tasks = \Sl_Model_Factory::mapper('task', 'crm')->fetchAll($where);
        $log_data = array();
        if (count($tasks)) {
            $log_data[] = 'Найдено ' . count($tasks) . ' напоминаний';
        }
        foreach ($tasks as $task) {


            $date_remind = new \DateTime($task->getDateStart());
            $date_remind->sub(new \DateInterval('PT' . ($task->getRemind() * 60) . 'S'));


            if ($date_remind <= new \DateTime()) {
                if (!$task->issetRelated('taskuser')) {
                    $task = \Sl_Model_Factory::mapper($task)->findExtended($task->getId(), 'taskuser');
                }
                //print_r($task);
                $user_obj = current($task->fetchRelated('taskuser'));
                if ($user_obj) {
                    $mail = new \Zend_Mail('UTF-8');
                    $mail->addTo($user_obj->getEmail());
                    $mail->setSubject($task->getName());
                    $text = '<br /><a href="' . \Sl_Service_Settings::value('BASE_URL') . \Sl\Service\Helper::getToEditUrl($task) . '">' . \Sl_Service_Settings::value('BASE_URL') . \Sl\Service\Helper::getToEditUrl($task) . '</a>';
                    $mail->setBodyHtml($task->getTaskSet() . PHP_EOL . $task->getDescription() . $text);
                    if ($mail->send()) {
                        $task->setSendEmail(1);
                        \Sl_Model_Factory::mapper($task)->save($task, false, false);
                    }
                }
            }
        }

        $log_data[] = 'Обработка завершена.';
        $logfile = APPLICATION_PATH . '/../logs/crm_task.txt';
        if (file_exists($logfile)) {
            if ($fh = fopen($logfile, 'a+')) {
                fwrite($fh, PHP_EOL . '************ ' . date('Y-m-d H:i:s') . ' ************' . PHP_EOL);
                foreach ($log_data as $str) {
                    fwrite($fh, $str . PHP_EOL);
                }
                fwrite($fh, '*********************************************' . PHP_EOL);
                fclose($fh);
            }
        }
    }

    public function onBeforeCreateAction(\Sl_Event_Action $event) {
        $model = $event->getOption('model');
        if ($model instanceof \Sl\Module\Crm\Model\Task) {

            if (\Zend_Auth::getInstance()->hasIdentity()) {
                $current_user = \Zend_Auth::getInstance()->getIdentity();

                $relation = \Sl_Modulerelation_Manager::getObjectsRelations($model, $current_user);
                foreach (array_keys($relation) as $rel) {
                    $model->assignRelated($rel, array($current_user));
                }
            }
        }
    }

}