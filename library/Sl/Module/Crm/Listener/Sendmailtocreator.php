<?php

namespace Sl\Module\Crm\Listener;

class Sendmailtocreator extends \Sl_Listener_Abstract implements \Sl_Listener_Model_Interface, \Sl\Listener\Cron, \Sl\Listener\Action\Before\Create {
    
    public function onAfterSave(\Sl_Event_Model $event) {
        return;
        //error_reporting(E_ERROR);
        $model = $event->getOption('model');
        if ($model instanceof \Sl\Module\Crm\Model\Task) {
           $translator = \Zend_Registry::get('Zend_Translate');
           $model_before = $event->getModelBeforeUpdate();
          
           if($model->getStatus() != $model_before->getStatus())
               {          
               
                $logger = \Sl\Service\Loger::getObjectFieldsLog($model, 1,  array('user_id', 'create'));   
                $user_model =\Sl_Model_Factory::mapper('user', \Sl_Module_Manager::getInstance()->getModule('auth'))->find($logger['user_id']);      
                $mail = new \Zend_Mail('UTF-8');
               
                $mail->addTo($user_model->getEmail(), $model->getName());                
                $mail->setSubject($translator->translate($model->getName()));
                $pfid = \Sl_Service_Settings::value('TASK_CTEATOR_PRINTFORM_ID');
                $form = \Sl_Model_Factory::mapper('printform', \Sl_Module_Manager::getInstance()->getModule('home'))->find($pfid);

                //print_r($logger);
                //die();
                if (!$form) {
                    \Sl\Module\Home\Service\Errors::addError('TASK_CTEATOR_PRINTFORM_ID', $translator->translate('Can\'t find needed subform.'));
                    //throw new \Exception($translator->translate('Can\'t find needed subform.') . __METHOD__);
                }
               
               $printer = \Sl\Printer\Manager::getPrinter($form);
               $printer->setCurrentObject($model);
                $filename = '/tmp/'.md5($model->getId());
                $printer->printIt(null,null,$filename);
               
                $message = file_get_contents($filename);
                unlink($filename);
               
                $mail->setBodyHtml($message);
                $mail->send();
               
           
          
  }          
/*
            if (\Zend_Auth::getInstance()->hasIdentity()) {
                $current_user = \Zend_Auth::getInstance()->getIdentity();

                $relation = \Sl_Modulerelation_Manager::getObjectsRelations($model, $current_user);
                foreach (array_keys($relation) as $rel) {
                    $model->assignRelated($rel, array($current_user));
                }
            }
            */
        }
        
    }

    public function onBeforeCreateAction(\Sl_Event_Action $event) {
        
    }

    public function onBeforeSave(\Sl_Event_Model $event) {
        
    }

    public function onRun(\Sl\Event\Cron $event) {
        
    }
}

