<?php

namespace Sl\Module\Crm\Listener;

class Taskmailer extends \Sl_Listener_Abstract implements \Sl\Listener\Cron {

    public function onRun(\Sl\Event\Cron $event) {
        
        $where = ' type = 3 and status = 2 ';
        $tasks = \Sl_Model_Factory::mapper('task', 'crm')->fetchAll($where);


        foreach ($tasks as $task) {

            $date_remind = new \DateTime($task->getDateStart());

            if ($date_remind <= new \DateTime()) {
                if (!$task->issetRelated('taskuser')) {
                    $task = \Sl_Model_Factory::mapper($task)->findExtended($task->getId(), array('taskmailergroup','taskfile'));
                }

                $taskmailergroups = $task->fetchRelated('taskmailergroup');
                if($taskmailergroups){
                foreach ($taskmailergroups as $taskmailergroup) {
                    $mailergroup = \Sl_Model_Factory::mapper($taskmailergroup->findModelName(), $taskmailergroup->findModuleName())->findExtended($taskmailergroup->getId(), array('mailergroupcustomer'));

                    $customers = $mailergroup->fetchRelated('mailergroupcustomer');

                    foreach ($customers as $customer) {
                        $customer = \Sl_Model_Factory::mapper($customer->findModelName(), $customer->findModuleName())->findExtended($customer->getId(), array('customeremails'));
                        $emails = $customer->fetchRelated('customeremails');

                        foreach ($emails as $email) {
                            $em[$email->getId()] = \Sl_Model_Factory::mapper($email->findModelName(), $email->findModuleName())->find($email->getId());
                        }
                    }
                }
                
                
        $mail = new \Zend_Mail('UTF-8');

        foreach ($em as $email) {
            $mail->addTo($email->getMail());
        }

        foreach ($task->fetchRelated('taskfile') as $key => $value) {
            // print_r();
            $at = new \Zend_Mime_Part(file_get_contents($value->getLocation()));
            $at_header = $at->getHeadersArray();

            $at->type = $at_header[0][1];
            $at->disposition = \Zend_Mime::DISPOSITION_ATTACHMENT;
            $at->encoding = \Zend_Mime::ENCODING_BASE64;
            $at->filename = basename($value);
            $mail->addAttachment($at);
        }

        $mail->setSubject($task->mail_topic);
        $mail->setBodyHtml($task->mail_body);
        $mail->send();
        
        $task->setStatus(3);
        \Sl_Model_Factory::mapper($task)->save($task, false, false);
                
                
                }

                /*
                  $user_obj = current($task->fetchRelated('taskuser'));


                  $mail = new \Zend_Mail('UTF-8');
                  $mail->addTo($user_obj->getEmail());
                  $mail->setSubject($task->getName());
                  $text = '<br /><a href="' . \Sl_Service_Settings::value('BASE_URL') . \Sl\Service\Helper::getToEditUrl($task) . '">' . \Sl_Service_Settings::value('BASE_URL') . \Sl\Service\Helper::getToEditUrl($task) . '</a>';
                  $mail->setBodyHtml($task->getTaskSet() . PHP_EOL . $task->getDescription() . $text);
                  if ($mail->send()) {
                  $task->setSendEmail(1);
                  \Sl_Model_Factory::mapper($task)->save($task, false, false);
                  }
                 */
            }
        }
    }

}

