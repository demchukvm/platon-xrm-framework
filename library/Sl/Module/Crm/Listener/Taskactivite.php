<?php

namespace Sl\Module\Crm\Listener;

class Taskactivite extends \Sl_Listener_Abstract implements \Sl_Listener_View_Interface, \Sl\Listener\View\Informer\Informer {

    public function onAfterContent(\Sl_Event_View $event) {
        
    }

    public function onBeforeContent(\Sl_Event_View $event) {
        
    }

    public function onBeforePageHeader(\Sl_Event_View $event) {
        
    }

    public function onBodyBegin(\Sl_Event_View $event) {
        
    }

    public function onBodyEnd(\Sl_Event_View $event) {
        
    }

    public function onContent(\Sl_Event_View $event) {
        
    }

    public function onFooter(\Sl_Event_View $event) {
        
    }

    public function onHeadLink(\Sl_Event_View $event) {

        $event->getView()->headLink()->appendStylesheet('/crm/task/taskactivite.css');
    }

    public function onHeadScript(\Sl_Event_View $event) {
        $event->getView()->headScript()->appendFile('/crm/task/taskactivite.js');
    }

    public function onHeadTitle(\Sl_Event_View $event) {
        
    }

    public function onHeader(\Sl_Event_View $event) {
        
    }

    public function onLogo(\Sl_Event_View $event) {
        
    }

    public function onNav(\Sl_Event_View $event) {
        
    }

    public function onPageOptions(\Sl_Event_View $event) {
        
    }

    public function onInformer(\Sl_Event_View $event) {

        //$event->getView()->addScriptPath();
        $view = $event->getView();

        if ($event->getView()->is_iframe)
            return;

        $field_resource_taskactivite = \Sl_Service_Acl::joinResourceName(array(
                    'type' => \Sl_Service_Acl::RES_TYPE_MVC,
                    'module' => 'crm',
                    'controller' => 'task',
                    'action' => 'edit'
        ));


        $priv_access_taskactivite = \Sl_Service_Acl::isAllowed($field_resource_taskactivite, \Sl_Service_Acl::PRIVELEGE_ACCESS);



        if ($priv_access_taskactivite && !$view->is_iframe) {

            $task_model = \Sl_Model_Factory::object('\Sl\Module\Crm\Model\Task');
            $task_data = \Sl_Model_Factory::mapper($task_model)->findActiviteTask($task_model);
            $view->data = $task_data;

            if (count($task_data[2]) > 0) {
                $calc = new \Sl\Module\Home\Informer\Item();  // Объект информера
                $calc->setTitle($event->getView()->translate('Задачи')) /* Подсказка по наведению на информер */
                        ->setIcon('glyphicons_054_clock.png')
                        ->setShowBadge(true)
                        ->setBadge('<span class="task_count">' . count($task_data[2]) . '</span>')
                        ->setContent($view->render('task/task.phtml'));
                ;
                $event->getView()->informer_items[] = $calc;
            }
            if (count($task_data[2]) > 0) {
            $calc_remind = new \Sl\Module\Home\Informer\Item();  // Объект информера
            $calc_remind->setTitle($event->getView()->translate('Напоминания')) /* Подсказка по наведению на информер */
                    ->setIcon('glyphicons_064_lightbulb.png')
                    ->setShowBadge(true)
                    ->setBadge('<span class="remind_count">' . (isset($task_data[1])?count($task_data[1]):0) . '</span>')                    
                    ->setContent($view->render('task/remind.phtml'));
            ;
            $event->getView()->informer_items[] = $calc_remind;
            }
        }
    }

}
