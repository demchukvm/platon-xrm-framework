<?php
namespace Sl\Module\Crm;
class Module extends \Sl_Module_Abstract {

	public function getListeners() {
		return array(
                    new Listener\Task($this),
                    new Listener\Taskactivite($this),
                    new Listener\Taskmailer($this),
                    new Listener\Sendmailtocreator($this),
                );
	}

	public function getModulerelations() {
		if (!($config_relations = $this -> section(parent::MODULERELATION_CONFIG_SECTION))) {
			$config_relations = $this -> _saveModuleConfig(array(), parent::MODULERELATION_CONFIG_SECTION);
		};

		return array_merge($config_relations -> toArray(), array());

	}

	// /Customername
	public function getCalculators() {
		return array(			
                    array(
				'calculator' => new Calculator\Tasklist(),
				'sort_order' => 10
			),);
	}

}
