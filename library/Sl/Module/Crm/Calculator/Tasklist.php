<?php

namespace Sl\Module\Crm\Calculator;

class Tasklist extends \Sl\Calculator\Identitycalculator {

    protected $_model_name = 'Sl\\Module\\Crm\\Model\\Identity\\Task';
 
    protected $_updated_fields = array
        (
            'date_finish',
            //'delivery_date',
            //'total',
            //'total_bill',
        ); 

    public function calculate($Obj) {
       //print_r($Obj);
       if($Obj['date_finish'] == '0000-00-00 00:00:00') 
       $Obj['date_finish'] = '---';
        
        return $Obj;
    }

}
