<?php

return array(
    '_default' =>
    array(
        'fields' =>
        array(
            'name',
            'weight_factor',
            'weight_surcharge',
            'count_factor',
            'count_surcharge',
            'place_surcharge',
            'insurance_percent',
            'total_weight_surcharge',
            'total_count_surcharge',
        ),
        'name' => '_default',
        'label' => 'По-умолчанию',
    ),
    '_popup' =>
    array(
        'fields' =>
        array(
            'name',
        ),
        'name' => '_popup',
        'label' => 'По-умолчанию',
        'type' => 'popup',
    ),
);
