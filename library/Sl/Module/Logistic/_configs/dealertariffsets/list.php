<?php
return array (
  'name' => 
  array (
    'order' => 10,
    'label' => 'Название',
    'searchable' => true,
    'sortable' => true,
    'class' => 'span2',
  ),
  'use_total_surcharge' => 
  array (
    'order' => 12,
    'label' => 'Использовать общую наценку',
    'sortable' => true,
  ),
  'total_surcharge' => 
  array (
    'order' => 15,
    'label' => 'Общая наценка',
    'searchable' => true,
    'sortable' => true,
    'class' => 'span1',
  ),
  'total_surcharge_type' => 
  array (
    'order' => 19,
    'label' => 'Тип',
    'searchable' => true,
    'sortable' => true,
    'class' => 'span1',
  ),
  'use_weight_factor' => 
  array (
    'order' => 25,
    'label' => 'Не использовать весовую наценку',
    'sortable' => true,
  ),
  'weight_factor' => 
  array (
    'order' => 30,
    'label' => 'Весовая константа',
    'searchable' => true,
    'sortable' => true,
    'class' => 'span1',
  ),
  'weight_surcharge' => 
  array (
    'order' => 40,
    'label' => 'Наценка весовая',
    'searchable' => true,
    'sortable' => true,
    'class' => 'span1',
  ),
  'use_count_factor' => 
  array (
    'order' => 50,
    'label' => 'Не использовать количеств. наценку',
    'sortable' => true,
  ),
  'count_factor' => 
  array (
    'order' => 60,
    'label' => 'Количеств. константа',
    'searchable' => true,
    'sortable' => true,
    'class' => 'span1',
  ),
  'count_surcharge' => 
  array (
    'order' => 70,
    'label' => 'Наценка количеств.',
    'searchable' => true,
    'sortable' => true,
    'class' => 'span1',
  ),
  'place_surcharge' => 
  array (
    'order' => 80,
    'label' => 'Наценка за место',
    'searchable' => true,
    'sortable' => true,
    'class' => 'span1',
  ),
  'insurance_percent' => 
  array (
    'order' => 90,
    'label' => 'Страховка, %',
    'searchable' => true,
    'sortable' => true,
    'class' => 'span1',
  ),
  'total_weight_surcharge' => 
  array (
    'order' => 100,
    'label' => 'Общая весовая наценка',
    'searchable' => true,
    'sortable' => true,
    'class' => 'span1',
  ),
  'total_count_surcharge' => 
  array (
    'order' => 110,
    'label' => 'Общая кол-ная наценка',
    'searchable' => true,
    'sortable' => true,
    'class' => 'span1',
  ),
);
