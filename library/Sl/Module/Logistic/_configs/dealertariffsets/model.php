<?php
return array (
  'name' => 
  array (
    'label' => 'Название',
    'type' => 'hidden',
    'readonly' => true,
  ),
  'id' => 
  array (
    'type' => 'hidden',
    'label' => 'ID',
  ),
  'weight_factor' => 
  array (
    'type' => 'text',
    'label' => 'Весовая константа',
    'class' => 'float canEmpty',
  ),
  'weight_surcharge' => 
  array (
    'type' => 'text',
    'label' => 'Наценка весовая',
    'class' => 'float canEmpty',
  ),
  'total_weight_surcharge' => 
  array (
    'type' => 'text',
    'label' => 'Общая весовая наценка',
    'class' => 'float canEmpty',
  ),
  'total_count_surcharge' => 
  array (
    'type' => 'text',
    'label' => 'Общая кол-ная наценка',
    'class' => 'float canEmpty',
  ),
  'count_factor' => 
  array (
    'type' => 'text',
    'label' => 'Кол-ая константа',
    'class' => 'float canEmpty',
  ),
  'count_surcharge' => 
  array (
    'type' => 'text',
    'label' => 'Наценка количеств.',
    'class' => 'float canEmpty',
  ),
  'place_surcharge' => 
  array (
    'type' => 'text',
    'label' => 'Наценка за место',
    'class' => 'float canEmpty',
  ),
  'insurance_percent' => 
  array (
    'type' => 'text',
    'label' => 'Страховка, %',
    'class' => 'float canEmpty',
  ),
);
