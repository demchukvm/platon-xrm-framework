<?php
return array (
  'name' => 
  array (
    'label' => 'Название',
    'searchable' => true,
    'sortable' => true,
    'class' => 'span2',
  ),
  'use_total_surcharge' => 
  array (
    'label' => 'Использовать общую наценку',
    'sortable' => true,
  ),
  'total_surcharge' => 
  array (
    'label' => 'Общая наценка',
    'searchable' => true,
    'sortable' => true,
    'class' => 'span1',
  ),
  'total_surcharge_type' => 
  array (
    'label' => 'Тип',
    'searchable' => true,
    'sortable' => true,
    'class' => 'span1',
  ),
  'use_weight_factor' => 
  array (
    'label' => 'Не использовать весовую наценку',
    'sortable' => true,
  ),
  'weight_factor' => 
  array (
    'label' => 'Весовая константа',
    'searchable' => true,
    'sortable' => true,
    'class' => 'span1',
  ),
  'weight_surcharge' => 
  array (
    'label' => 'Наценка весовая',
    'searchable' => true,
    'sortable' => true,
    'class' => 'span1',
  ),
  'use_count_factor' => 
  array (
    'label' => 'Не использовать количеств. наценку',
    'sortable' => true,
  ),
  'count_factor' => 
  array (
    'label' => 'Количеств. константа',
    'searchable' => true,
    'sortable' => true,
    'class' => 'span1',
  ),
  'count_surcharge' => 
  array (
    'label' => 'Наценка количеств.',
    'searchable' => true,
    'sortable' => true,
    'class' => 'span1',
  ),
  'place_surcharge' => 
  array (
    'label' => 'Наценка за место',
    'searchable' => true,
    'sortable' => true,
    'class' => 'span1',
  ),
  'insurance_percent' => 
  array (
    'label' => 'Страховка, %',
    'searchable' => true,
    'sortable' => true,
    'class' => 'span1',
  ),
  'total_weight_surcharge' => 
  array (
    'label' => 'Общая весовая наценка',
    'searchable' => true,
    'sortable' => true,
    'class' => 'span1',
  ),
  'total_count_surcharge' => 
  array (
    'label' => 'Общая кол-ная наценка',
    'searchable' => true,
    'sortable' => true,
    'class' => 'span1',
  ),
);
