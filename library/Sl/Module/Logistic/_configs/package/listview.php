<?php

return array(
    'identifier' =>
    array(
        'label' => 'ID',
        'searchable' => true,
        'sortable' => true,
        'width' => '150px',
        'class' => 'custom'
    ),
    'status_date' =>
    array(
        'label' => 'Статус д.',
        'type' => 'date',
        'searchable' => true,
        'sortable' => true,
    ),
    'receive_date' =>
    array(
        'label' => 'Принято',
        'type' => 'date',
        'searchable' => true,
        'sortable' => true,
    ),
    'status' =>
    array(
        'label' => 'Статус',
        'searchable' => true,
        'sortable' => true,
    ),
    'add_date' =>
    array(
        'label' => 'Дата приема',
        'sortable' => true,
        'type' => 'text',
    ),
    'logistictariffpackage.name' =>
    array(
        'label' => 'Дорога',
        'sortable' => true,
        'searchable' => true,
        'class' => 'span1-5',
    ),
    'packagestock.name' =>
    array(
        'label' => 'Откуда',
        'sortable' => true,
        'searchable' => true,
        'class' => 'span1',
    ),
    'packageroute.name' =>
    array(
        'label' => 'Маршрут',
        'sortable' => true,
        'searchable' => true,
    ),
    'packagestockdestination.name' =>
    array(
        'label' => 'Куда',
        'sortable' => true,
        'searchable' => true,
        'class' => 'span1',
    ),
    'packagecustomer.id' =>
    array(
        'label' => 'Город получателя',
        'class' => 'span1',
    ),
    'calculated_weight' =>
    array(
        'class' => 'span0-5',
        'label' => 'р.&nbsp;вес',
        'sortable' => true,
        'searchable' => true,
        'calculate' => 'sum',
    ),
    'packages_count' =>
    array(
        'label' => 'Мест',
        'sortable' => true,
        'searchable' => true,
        'class' => 'span0-5',
        'calculate' => 'sum',
    ),
    'declarate_cost' =>
    array(
        'label' => 'Страховка',
        'class' => 'span0-5',
        'sortable' => true,
        'searchable' => true,
        'calculate' => 'sum',
    ),
    'total_sum' =>
    array(
        'label' => 'К оплате',
        'class' => 'span1',
        'sortable' => true,
        'searchable' => true,
        'calculate' => 'sum',
    ),
    /*'packagefinoperation.sum' =>
    array(
        'label' => 'Оплачено',
        'class' => 'span1',
        'sortable' => true,
        'searchable' => true,
        'calculate' => 'sum',
    ),
    'packagefinoperation.factor' =>
    array(
        'label' => 'Кассы',
        'class' => 'span1',
        'sortable' => true,
        'searchable' => true,
    ),*/
    'wrapping_cost' =>
    array(
        'label' => 'Упаковка',
        'class' => 'span1',
        'sortable' => true,
        'searchable' => true,
        'calculate' => 'sum',
    ),
    'base_cost_per_kg' =>
    array(
        'label' => 'За&nbsp;1&nbsp;кг',
        'class' => 'span0-5',
        'sortable' => true,
        'searchable' => true,
        'calculate' => 'average',
    ),
    'calculated_cost_per_kg' =>
    array(
        'label' => 'За&nbsp;1&nbsp;кг&nbsp;(рас)',
        'class' => 'span0-5',
        'sortable' => true,
        'searchable' => true,
        'calculate' => 'average',
    ),
    'create' =>
    array(
        'label' => 'Создан',
        'sortable' => true,
        'searchable' => true,
        'type' => 'date',
        'class' => 'date span1',
    ),
    'financial_status' =>
    array(
        'type' => 'select',
        'label' => 'Фин. статус',
        'sortable' => true,
        'searchable' => true,
    ),
    'description' =>
    array(
        'class' => 'span2',
        'sortable' => true,
        'searchable' => true,
    ),
    'volume' =>
    array(
        'label' => 'Объем',
        'class' => 'span1',
        'sortable' => true,
        'searchable' => true,
    ),
    'delcarate_cost' =>
    array(
        'label' => 'Декларация',
        'class' => 'span1',
        'sortable' => true,
        'searchable' => true,
    ),
    'base_cost' =>
    array(
        'label' => 'Вход',
        'class' => 'span1',
        'sortable' => true,
        'searchable' => true,
    ),
    'dealer_cost' =>
    array(
        'label' => 'Дил. стоим.',
        'class' => 'span1',
        'sortable' => true,
        'searchable' => true,
        'calculate' => 'sum',
    ),
    'weight' =>
    array(
        'label' => 'Вес',
        'sortable' => true,
        'searchable' => true,
        'class' => 'span1',
    ),
    'delivery_cost' =>
    array(
        'label' => 'Доставка',
        'sortable' => true,
        'searchable' => true,
        'class' => 'span1',
    ),
    'insurance_percent' =>
    array(
        'label' => 'Страховка, %',
        'sortable' => true,
        'searchable' => true,
        'class' => 'span1',
    ),
    'insurance_calculated_cost' =>
    array(
        'label' => 'Страховка',
        'class' => 'span0-5',
        'sortable' => true,
        'searchable' => true,
    ),
    'insurance_type' =>
    array(
        'label' => 'Тип страх.',
        'sortable' => true,
        'searchable' => true,
        'type' => 'select',
    ),
    'weight_ratio' =>
    array(
        'label' => 'кг/м3',
        'sortable' => true,
        'searchable' => true,
        'class' => 'span1',
    ),
    'calculate_description' =>
    array(
        'label' => 'Подр. рассчета',
        'sortable' => true,
        'searchable' => true,
        'class' => 'span2',
    ),
    'dealer_calculate_description' =>
    array(
        'label' => 'Подр. дил.',
        'sortable' => true,
        'searchable' => true,
        'class' => 'span2',
    ),
    'units_count' =>
    array(
        'label' => 'Кол-во ед.',
        'sortable' => true,
        'searchable' => true,
        'class' => 'span1',
    ),
    'base_count_surcharge' =>
    array(
        'label' => 'За шт',
        'sortable' => true,
        'searchable' => true,
        'class' => 'span1',
    ),
    'category' =>
    array(
        'label' => 'Категория груза',
    ),
    'payment_method' =>
    array(
        'label' => 'Форма оплаты',
    ),
    'first_count_surcharge' =>
    array(
        'label' => 'Шт.',
    ),
    'first_pack_factor' =>
    array(
        'label' => 'Место',
    ),
    'first_cost_per_kg' =>
    array(
        'label' => 'Кг.',
    ),
    'count_factor' =>
    array(
        'label' => 'КК',
    ),
    'weight_factor' =>
    array(
        'label' => 'ВК',
    ),
    'packagecustomer.name' =>
    array(
        'label' => 'Получатель',
    ),
    'packagecustomer.customerusersystem.email' =>
    array(
        'label' => 'Осн. email',
    ),
    'packagecustomer.customercity.name' =>
    array(
        'label' => 'Город получателя',
    ),
    '_roles_' =>
    array(
        30 =>
        array(
        ),
        31 =>
        array(
        ),
        33 =>
        array(
        ),
        32 =>
        array(
        ),
    ),
    'customer_description' =>
    array(
        'class' => 'span2',
        'sortable' => true,
        'searchable' => true,
    ),
    'itinerary' =>
    array(
        'label' => 'Рейс',
        'searchable' => true,
    ),
    'packagefinoperation_cash' => array(
        'label' => 'Кассы',
    ),
    'packagefinoperation_paid_sum' => array(
        'label' => 'Оплачено',
    ),
);
