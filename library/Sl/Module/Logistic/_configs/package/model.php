<?php
return array (
  'identifier' => 
  array (
    'label' => 'ID',
    'type' => 'text',
    'readonly' => true,
  ),
  'add_date' => 
  array (
    'type' => 'text',
    'label' => 'ADD_DATE',
  ),
  'status' => 
  array (
    'label' => 'Статус',
    'type' => 'select',
  ),
  'status_date' => 
  array (
    'label' => 'Посл. изм. статуса',
    'type' => 'date',
  ),
  'category' => 
  array (
    'label' => 'Категория груза',
    'type' => 'text',
  ),
  'wrapping_cost' => 
  array (
    'label' => 'Упаковка',
    'type' => 'text',
    'readonly' => true,
    'class' => 'floatPositive',
  ),
  'volume' => 
  array (
    'label' => 'Объем',
    'type' => 'text',
    'class' => 'floatPositive fixedDigits-2',
  ),
  'declarate_cost' => 
  array (
    'label' => 'Декл. стоимоть',
    'type' => 'text',
  ),
  'base_cost_per_kg' => 
  array (
    'label' => 'За 1 кг',
    'type' => 'text',
    'class' => 'floatPositive',
  ),
  'base_cost' => 
  array (
    'label' => 'Себестоимость',
    'type' => 'hidden',
    'readonly' => true,
  ),
  'dealer_cost' => 
  array (
    'label' => 'Дилерская',
    'type' => 'text',
    'class' => 'floatPositive',
    'readonly' => true,
  ),
  'weight' => 
  array (
    'label' => 'Вес',
    'type' => 'text',
    'class' => 'floatPositive',
    'readonly' => true,
  ),
  'calculated_weight' => 
  array (
    'label' => 'Расчет. вес',
    'type' => 'text',
    'class' => 'intPositive canEmpty',
  ),
  'delivery_cost' => 
  array (
    'label' => 'Доставка',
    'type' => 'text',
    'class' => 'floatPositive',
    'readonly' => true,
  ),
  'insurance_percent' => 
  array (
    'label' => 'Страховка %',
    'type' => 'text',
    'class' => 'floatPositive',
  ),
  'insurance_calculated_cost' => 
  array (
    'label' => 'Страховка',
    'type' => 'text',
    'readonly' => true,
    'class' => 'floatPositive',
  ),
  'insurance_type' => 
  array (
    'label' => 'Тип страховки',
    'type' => 'text',
  ),
  'mail' => 
  array (
    'label' => 'Email',
    'type' => 'checkbox',
  ),
  'sms' => 
  array (
    'label' => 'Sms',
    'type' => 'checkbox',
  ),
  'payment_method' => 
  array (
    'label' => 'Форма оплаты',
    'type' => 'text',
  ),
  'total_sum' => 
  array (
    'label' => 'Итого',
    'type' => 'text',
    'readonly' => true,
    'is_null' => true,
  ),
  'sum' => 
  array (
    'label' => 'К оплате',
    'type' => 'text',
    'class' => 'warning floatPositive',
    'required' => true,
    'is_null' => true,
  ),
  'weight_ratio' => 
  array (
    'label' => 'кг/м3',
    'type' => 'text',
  ),
  'id' => 
  array (
    'label' => 'id (сист.)',
    'type' => 'hidden',
  ),
  'active' => 
  array (
    'label' => 'ACTIVE',
    'type' => 'checkbox',
  ),
  'option' => 
  array (
    'label' => 'OPTION',
    'type' => 'text',
    'visible' => false,
  ),
  'declarate_cost' => 
  array (
    'label' => 'Декларация',
    'type' => 'text',
    'class' => 'floatPositive',
  ),
  'financial_status' => 
  array (
    'label' => 'Состояние оплаты',
    'type' => 'text',
    'disabled' => true,
  ),
  'create' => 
  array (
    'label' => 'Дата создания',
    'type' => 'hidden',
  ),
  'description' => 
  array (
    'label' => 'Служебный комментарий',
    'type' => 'textarea',
    'class' => 'autosize',
  ),
  'calculate_description' => 
  array (
    'label' => 'Подробности расчета',
    'type' => 'textarea',
    'readonly' => true,
    'class' => 'autosize',
  ),
  'dealer_calculate_description' => 
  array (
    'label' => 'Подробности расчета дилерских выплат',
    'type' => 'textarea',
    'readonly' => true,
    'class' => 'autosize',
  ),
  'packages_count' => 
  array (
    'label' => 'Кол-во ящиков',
    'type' => 'text',
    'readonly' => true,
    'class' => 'intPositive',
  ),
  'units_count' => 
  array (
    'label' => 'Кол-во единиц',
    'type' => 'text',
    'class' => 'intPositive',
  ),
  'base_count_surcharge' => 
  array (
    'label' => 'За шт',
    'type' => 'text',
    'class' => 'floatPositive canEmpty',
  ),
  'first_insurance_percent' => 
  array (
    'label' => 'Cтрах.',
    'type' => 'hidden',
    'class' => 'floatPositive canEmpty span05',
  ),
  'first_count_surcharge' => 
  array (
    'label' => 'Шт.',
    'type' => 'hidden',
    'class' => 'floatPositive canEmpty span05',
  ),
  'first_pack_factor' => 
  array (
    'label' => 'Место',
    'type' => 'hidden',
    'class' => 'floatPositive canEmpty span05',
  ),
  'first_cost_per_kg' => 
  array (
    'label' => 'Кг.',
    'type' => 'hidden',
    'class' => 'floatPositive canEmpty span05',
  ),
  'count_factor' => 
  array (
    'label' => 'КК',
    'type' => 'hidden',
    'class' => 'floatPositive canEmpty',
  ),
  'weight_factor' => 
  array (
    'label' => 'ВК',
    'type' => 'hidden',
    'class' => 'floatPositive canEmpty',
  ),
  'identifier_ext' => 
  array (
    'label' => 'Доп. код',
    'type' => 'text',
  ),
  'volume_factor' => 
  array (
    'label' => 'Об. конст.',
    'type' => 'text',
  ),
  'in_chain' => 
  array (
    'label' => 'В пути',
    'type' => 'hidden',
  ),
  'receive_date' => 
  array (
    'label' => 'Дата принятия',
    'type' => 'hidden',
  ),
  'calculated_cost_per_kg' => 
  array (
    'label' => 'Рас. стоимость/кг',
    'type' => 'hidden',
  ),
  'archived' => 
  array (
    'label' => 'ARCHIVED',
    'type' => 'text',
  ),
  'itinerary' => 
  array (
    'label' => 'Рейс',
    'type' => 'text',
  ),
  'logistictariffdays' => 
  array (
    'label' => 'LOGISTICTARIFFDAYS',
    'type' => 'hidden',
  ),
  'issued_boxes' => 
  array (
    'label' => 'ISSUED_BOXES',
    'type' => 'hidden',
  ),
  'customer_description' => 
  array (
    'label' => 'Комментарий для заказчика',
    'type' => 'textarea',
  ),
);
