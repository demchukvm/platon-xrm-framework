<?php
return array (
  'calculated_weight' => 
  array (
    'class' => 'floatPositive fixedDigits-1',
  ),
  'modulerelation_packagefile' => 
  array (
    'label' => 'Файлы',
  ),
  'modulerelation_logistictariffpackage' => 
  array (
    'label' => 'Тарифный план',
    'field_filters' => 
    array (
      0 => 
      array (
        'field' => 'logistictariffroute:id',
        'matching' => 'in',
        'value' => 'modulerelation_packageroute',
        'strong' => true,
      ),
      1 => 
      array (
        'field' => 'logistictariffprodtype:id',
        'matching' => 'in',
        'value' => 'modulerelation_packageproducttype',
      ),
      2 => 
      array (
        'field' => 'min_weight',
        'matching' => 'lte',
        'value' => 'calculated_weight',
      ),
    ),
  ),
  'modulerelation_packagepackinglist' => 
  array (
    'label' => 'Инвойс',
    'iframe_edit' => true,
  ),
  'modulerelation_packagecustomer' => 
  array (
    'label' => 'Получатель',
    'required' => true,
    'request_fields' => 
    array (
      0 => 'customeridentifiercustomer:name',
      1 => 'name',
    ),
  ),
  'modulerelation_packagestock' => 
  array (
    'label' => 'Откуда',
    'field_filters' => 
    array (
      0 => 
      array (
        'field' => 'id',
        'matching' => 'nin',
        'value' => 'modulerelation_packagestockdestination',
      ),
    ),
  ),
  'modulerelation_packageorder' => 
  array (
    'label' => 'Заказ товаров',
    'request_fields' => 
    array (
      0 => 'name',
      1 => 'total_sum',
      2 => 'orderstock:name',
    ),
    'field_filters' => 
    array (
      0 => 
      array (
        'field' => 'ordercustomer:id',
        'matching' => 'in',
        'value' => 'modulerelation_packagecustomer',
      ),
    ),
  ),
  'modulerelation_packagestockdestination' => 
  array (
    'label' => 'Куда',
    'field_filters' => 
    array (
      0 => 
      array (
        'field' => 'id',
        'matching' => 'nin',
        'value' => 'modulerelation_packagestock',
      ),
    ),
  ),
  'modulerelation_packageroute' => 
  array (
    'label' => 'Маршрут',
    'request_fields' => 
    array (
      0 => 'name',
    ),
    'readonly' => true,
    'field_filters' => 
    array (
      0 => 
      array (
        'field' => 'routecountry:id',
        'matching' => 'in',
        'value' => 'modulerelation_packagecountry',
      ),
      1 => 
      array (
        'field' => 'routecountrydestination:id',
        'matching' => 'in',
        'value' => 'modulerelation_packagecountrydestination',
      ),
    ),
  ),
  'modulerelation_packagepack' => 
  array (
    'label' => 'Ящики',
  ),
  'modulerelation_packageproducttype' => 
  array (
    'label' => 'Категория',
  ),
  'modulerelation_packageitemservice' => 
  array (
    'label' => 'Доп. услуги',
    'field_filters' => 
    array (
      0 => 
      array (
        'field' => 'packageitemservice:id',
        'matching' => 'in',
        'value' => 'id',
      ),
    ),
  ),
  'modulerelation_packageitemkeeping' => 
  array (
    'label' => 'Хранение',
    'field_filters' => 
    array (
      0 => 
      array (
        'field' => 'packageitemkeeping:id',
        'matching' => 'in',
        'value' => 'id',
      ),
    ),
  ),
  'modulerelation_packagecountry' => 
  array (
    'label' => 'Страна',
    'readonly' => true,
    'field_filters' => 
    array (
      0 => 
      array (
        'field' => 'id',
        'matching' => 'nin',
        'value' => 'modulerelation_packagecountrydestination',
      ),
    ),
  ),
  'logistictariffdays' => 
  array (
    'label' => 'LOGISTICTARIFFDAYS',
    'type' => 'hidden',
  ),
  'weight_ratio' => 
  array (
    'label' => 'кг/м3',
    'readonly' => true,
  ),
  'total_sum' => 
  array (
    'label' => 'Итого',
  ),
  'modulerelation_packagecountrydestination' => 
  array (
    'label' => 'Страна',
    'readonly' => true,
    'field_filters' => 
    array (
      0 => 
      array (
        'field' => 'id',
        'matching' => 'nin',
        'value' => 'modulerelation_packagecountry',
      ),
    ),
  ),
  'create' => 
  array (
    'label' => 'Дата создания',
    'sort_order' => 25,
    'type' => 'hidden',
    'class' => 'current-date span6',
  ),
  'status_date' => 
  array (
    'type' => 'hidden',
  ),
  'insurance_percent' => 
  array (
    'readonly' => true,
  ),
  'issued_boxes' => 
  array (
    'label' => 'Выдано',
    'readonly' => true,
    'class' => 'span1',
    'type' => 'hidden',
  ),
  '_roles_' => 
  array (
    1 => 
    array (
      'base_cost' => 
      array (
        'readonly' => false,
        'type' => 'text',
      ),
      'first_cost_per_kg' => 
      array (
        'type' => 'text',
      ),
      'first_insurance_percent' => 
      array (
        'type' => 'text',
      ),
      'first_count_surcharge' => 
      array (
        'type' => 'text',
      ),
      'first_pack_factor' => 
      array (
        'type' => 'text',
      ),
      'count_factor' => 
      array (
        'type' => 'text',
      ),
      'weight_factor' => 
      array (
        'type' => 'text',
      ),
      'insurance_percent' => 
      array (
        'readonly' => false,
      ),
    ),
    30 => 
    array (
      'modulerelation_packagecustomer' => 
      array (
        'readonly' => true,
      ),
      'mail' => 
      array (
        'label' => 'Email',
        'type' => 'hidden',
      ),
      'sms' => 
      array (
        'label' => 'Sms',
        'type' => 'hidden',
      ),
      'weight' => 
      array (
        'type' => 'hidden',
      ),
      'description' => 
      array (
        'type' => 'hidden',
      ),
      'dealer_calculate_description' => 
      array (
        'type' => 'hidden',
      ),
      'calculate_description' => 
      array (
        'readonly' => true,
      ),
      'dealer_cost' => 
      array (
        'type' => 'hidden',
      ),
      'base_cost_per_kg' => 
      array (
        'readonly' => true,
      ),
      'base_count_surcharge' => 
      array (
        'readonly' => true,
      ),
      'insurance_percent' => 
      array (
        'readonly' => true,
      ),
    ),
    31 => 
    array (
      'dealer_cost' => 
      array (
        'type' => 'hidden',
      ),
      'base_cost_per_kg' => 
      array (
        'readonly' => true,
      ),
      'base_count_surcharge' => 
      array (
        'readonly' => true,
      ),
      'insurance_percent' => 
      array (
        'readonly' => true,
      ),
      'first_cost_per_kg' => 
      array (
        'type' => 'hidden',
      ),
      'first_count_surcharge' => 
      array (
        'type' => 'hidden',
      ),
      'first_insurance_percent' => 
      array (
        'type' => 'hidden',
      ),
      'first_pack_factor' => 
      array (
        'type' => 'hidden',
      ),
      'status_date' => 
      array (
        'readonly' => true,
      ),
    ),
    33 => 
    array (
      'dealer_cost' => 
      array (
        'type' => 'hidden',
      ),
      'base_cost_per_kg' => 
      array (
        'readonly' => true,
      ),
      'base_count_surcharge' => 
      array (
        'readonly' => true,
      ),
      'insurance_percent' => 
      array (
        'readonly' => false,
      ),
      'first_cost_per_kg' => 
      array (
        'type' => 'hidden',
      ),
      'first_count_surcharge' => 
      array (
        'type' => 'hidden',
      ),
      'first_insurance_percent' => 
      array (
        'type' => 'hidden',
      ),
      'first_pack_factor' => 
      array (
        'type' => 'hidden',
      ),
      'status_date' => 
      array (
        'readonly' => false,
      ),
    ),
    32 => 
    array (
      'modulerelation_packagecustomer' => 
      array (
        'readonly' => true,
      ),
      'mail' => 
      array (
        'label' => 'Email',
        'type' => 'hidden',
      ),
      'sms' => 
      array (
        'label' => 'Sms',
        'type' => 'hidden',
      ),
      'weight' => 
      array (
        'type' => 'hidden',
      ),
      'description' => 
      array (
        'type' => 'hidden',
      ),
      'dealer_calculate_description' => 
      array (
        'type' => 'hidden',
      ),
      'calculate_description' => 
      array (
        'readonly' => true,
      ),
      'dealer_cost' => 
      array (
        'type' => 'hidden',
      ),
      'base_cost_per_kg' => 
      array (
        'readonly' => true,
      ),
      'base_count_surcharge' => 
      array (
        'readonly' => true,
      ),
      'insurance_percent' => 
      array (
        'readonly' => true,
      ),
    ),
  ),
);
