<?php

return array(
    '_default' =>
    array(
        'name' => '_default',
        'description' => 'По-умолчанию',
        'filter' => array(
            'type' => 'multi',
            'comparison' => 1, // AND
            'comps' => array(
                '_system' => array(
                    'type' => 'multi',
                    'comparison' => 1, // AND
                    'comps' => array(
                        'active' => array(
                            'type' => 'eq',
                            'field' => 'active',
                            'value' => 1
                        ),
                        'identifier' => array(
                            'type' => 'isnull',
                            'field' => 'identifier',
                            'value' => false
                        ),
                    ),
                ),
                '_user' => array(
                    'type' => 'multi',
                    'comparison' => 2, //OR,
                    'comps' => array(
                        '_custom' => array(
                            'type' => 'multi',
                            'comparison' => 1, // AND
                            'comps' => array(

                            ),
                        ),
                        '_id' => array(
                            'type' => 'in',
                            'field' => 'id',
                            'value' => array(),
                        ),
                    ),
                ),
            ),
        ),
    ),
);
