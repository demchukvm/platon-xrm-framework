<?php

return array(
    '_default' =>
    array(
        'fields' =>
        array(
            0 => 'identifier',
            1 => 'identifier_ext',
            2 => 'status',
            3 => 'receive_date',
            4 => 'status_date',
            5 => 'packagecustomer.name',
            6 => 'packagecustomer.customerusersystem.email',
            7 => 'packagestock.name',
            8 => 'packageroute.name',
            9 => 'packagestockdestination.name',
            10 => 'packagecustomer.customercity.name',
            11 => 'total_sum',
        ),
        'name' => 'default',
        'label' => 'По-умолчанию',
    ),
    '_popup' =>
    array(
        'fields' =>
        array(
            'identifier',
        ),
        'name' => 'popup',
        'label' => 'По-умолчанию',
        'type' => 'popup',
    ),
);
