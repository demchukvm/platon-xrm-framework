<?php
return array (
  'name' => 
  array (
    'label' => 'Идентификатор',
    'type' => 'text',
  ),
  'id' => 
  array (
    'type' => 'hidden',
    'label' => 'ID',
  ),
  'counter' => 
  array (
    'type' => 'hidden',
    'label' => 'COUNTER',
  ),
  'symbol' => 
  array (
    'type' => 'hidden',
    'label' => 'SYMBOL',
  ),
  'create' => 
  array (
    'label' => 'CREATE',
    'type' => 'text',
  ),
  'active' => 
  array (
    'label' => 'ACTIVE',
    'type' => 'checkbox',
  ),
);
