<?php

return array(
    '_default' =>
    array(
        'fields' =>
        array(
            0 => 'name',
            2 => 'counter',
            3 => 'symbol',
        ),
        'name' => '_default',
        'label' => 'По-умолчанию',
    ),
    '_popup' =>
    array(
        'fields' =>
        array(
            'name',
        ),
        'name' => 'popup',
        'label' => 'По-умолчанию',
        'type' => 'popup',
    ),
);
