<?php

return array(
    '_default' =>
    array(
        'fields' =>
        array(
            0 => 'name',
            1 => 'identifier',
            2 => 'description',
            4 => 'length',
            5 => 'width',
            6 => 'height',
            8 => 'weight',
            9 => 'price_per_wrapping',
            10 => 'option',
            11 => 'filling',
            12 => 'volume',
        ),
        'name' => '_default',
        'label' => 'По-умолчанию',
    ),
    '_popup' =>
    array(
        'fields' =>
        array(
            'identifier',
        ),
        'name' => 'popup',
        'label' => 'По-умолчанию',
        'type' => 'popup',
    ),
);
