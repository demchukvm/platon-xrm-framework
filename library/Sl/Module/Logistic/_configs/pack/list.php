<?php
return array (
  'name' => 
  array (
    'order' => 10,
    'label' => 'NAME',
  ),
  'identifier' => 
  array (
    'order' => 20,
    'label' => 'ID',
  ),
  'description' => 
  array (
    'order' => 30,
    'label' => 'Комментарий',
  ),
  'weight' => 
  array (
    'order' => 40,
    'label' => 'Вес',
  ),
  'price_per_wrapping' => 
  array (
    'order' => 50,
    'label' => 'Цена упаковки',
  ),
  'option' => 
  array (
    'order' => 60,
    'label' => 'OPTION',
  ),
  'filling' => 
  array (
    'order' => 70,
    'label' => 'Наполнен',
  ),
  'volume' => 
  array (
    'order' => 80,
    'label' => 'Объем ящика',
  ),
  'length' => 
  array (
    'label' => 'Длина, см',
    'order' => 90,
    'visible' => false,
    'hidable' => true,
  ),
  'width' => 
  array (
    'label' => 'Ширина, см',
    'order' => 100,
    'visible' => false,
    'hidable' => true,
  ),
  'height' => 
  array (
    'label' => 'Высота, см',
    'order' => 110,
    'visible' => false,
    'hidable' => true,
  ),
);
