<?php
return array (
  'name' => 
  array (
    'label' => 'NAME',
  ),
  'identifier' => 
  array (
    'label' => 'ID',
  ),
  'description' => 
  array (
    'label' => 'Комментарий',
  ),
  'weight' => 
  array (
    'label' => 'Вес',
  ),
  'price_per_wrapping' => 
  array (
    'label' => 'Цена упаковки',
  ),
  'option' => 
  array (
    'label' => 'OPTION',
  ),
  'filling' => 
  array (
    'label' => 'Наполнен',
  ),
  'volume' => 
  array (
    'label' => 'Объем ящика',
  ),
  'length' => 
  array (
    'label' => 'Длина, см',
  ),
  'width' => 
  array (
    'label' => 'Ширина, см',
  ),
  'height' => 
  array (
    'label' => 'Высота, см',
  ),
);
