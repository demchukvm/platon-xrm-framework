<?php
return array (
  'identifier' => 
  array (
    'label' => 'ID',
    'sort_order' => 1010,
  ),
  'weight' => 
  array (
    'label' => 'Вес, кг',
    'sort_order' => 1040,
  ),
  'height' => 
  array (
    'sort_order' => 1060,
  ),
  'width' => 
  array (
    'sort_order' => 1070,
  ),
  'length' => 
  array (
    'sort_order' => 1080,
  ),
  'volume' => 
  array (
    'sort_order' => 1090,
    'readonly' => true,
  ),
  'description' => 
  array (
    'sort_order' => 1100,
  ),
  'modulerelation_logistictariffprodtype' => 
  array (
    'label' => 'Товары',
  ),
  'modulerelation_packbox' => 
  array (
    'label' => 'Связь',
    'sort_order' => 100800,
    'request_fields' => 
    array (
      0 => 'name',
      1 => 'description',
    ),
    'field_filters' => 
    array (
      0 => 
      array (
        'field' => 'id',
        'matching' => 'nin',
        'value' => 'modulerelation_packagepack-modulerelation_packbox',
      ),
      1 => 
      array (
        'field' => 'boxstock:id',
        'matching' => 'in',
        'value' => 'modulerelation_packagestock',
      ),
    ),
  ),
  'price_per_wrapping' => 
  array (
    'sort_order' => 10350,
  ),
  'modulerelation_packwrapping' => 
  array (
    'label' => 'Упаковка',
    'sort_order' => 10450,
    'request_fields' => 
    array (
      0 => 'name',
      1 => 'default_price',
    ),
  ),
  'filling' => 
  array (
    'sort_order' => 10500,
  ),
);
