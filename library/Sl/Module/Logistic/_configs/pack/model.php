<?php
return array (
  'name' => 
  array (
    'label' => 'NAME',
    'type' => 'text',
  ),
  'identifier' => 
  array (
    'label' => 'ID',
    'type' => 'text',
    'readonly' => true,
    'searchable' => true,
  ),
  'description' => 
  array (
    'label' => 'Комментарий',
    'type' => 'text',
  ),
  'id' => 
  array (
    'label' => 'ID',
    'type' => 'hidden',
    'readonly' => true,
  ),
  'length' => 
  array (
    'label' => 'Длина, см',
    'type' => 'text',
    'class' => 'floatPositive volume_changer canEmpty',
  ),
  'width' => 
  array (
    'label' => 'Ширина, см',
    'type' => 'text',
    'class' => 'floatPositive volume_changer canEmpty',
  ),
  'height' => 
  array (
    'label' => 'Высота, см',
    'type' => 'text',
    'class' => 'floatPositive volume_changer canEmpty',
  ),
  'active' => 
  array (
    'label' => 'ACTIVE',
    'type' => 'checkbox',
  ),
  'weight' => 
  array (
    'label' => 'Вес',
    'type' => 'text',
    'class' => 'floatPositive canEmpty',
  ),
  'price_per_wrapping' => 
  array (
    'label' => 'Цена упаковки',
    'type' => 'text',
    'class' => 'floatPositive canEmpty',
  ),
  'option' => 
  array (
    'label' => 'OPTION',
    'type' => 'text',
  ),
  'filling' => 
  array (
    'label' => 'Наполнен',
    'type' => 'text',
  ),
  'volume' => 
  array (
    'label' => 'Объем ящика',
    'type' => 'text',
    'class' => 'floatPositive canEmpty fixedDigits-8',
  ),
  'create' => 
  array (
    'label' => 'CREATE',
    'type' => 'text',
  ),
);
