<?php
return array (
  'name' => 
  array (
    'label' => 'Название',
    'type' => 'text',
  ),
  'default_price' => 
  array (
    'label' => 'Цена по умолчанию',
    'type' => 'text',
    'class' => 'floatPositive',
  ),
  'create' => 
  array (
    'label' => 'CREATE',
    'type' => 'text',
  ),
  'id' => 
  array (
    'label' => 'ID',
    'type' => 'hidden',
  ),
  'active' => 
  array (
    'label' => 'ACTIVE',
    'type' => 'checkbox',
  ),
);
