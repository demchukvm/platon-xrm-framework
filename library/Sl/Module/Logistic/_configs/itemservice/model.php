<?php
return array (
  'name' => 
  array (
    'label' => 'Название',
    'type' => 'hidden',
    'sort_order' => 10,
    'readonly' => true,
  ),
  'price' => 
  array (
    'label' => 'Цена',
    'type' => 'text',
    'sort_order' => 20,
    'class' => 'floatPositive',
  ),
  'description' => 
  array (
    'label' => 'Комментарий',
    'type' => 'text',
    'sort_order' => 30,
  ),
  'id' => 
  array (
    'label' => 'ID',
    'type' => 'hidden',
  ),
  'active' => 
  array (
    'label' => 'ACTIVE',
    'type' => 'checkbox',
  ),
  'create' => 
  array (
    'label' => 'CREATE',
    'type' => 'text',
  ),
);
