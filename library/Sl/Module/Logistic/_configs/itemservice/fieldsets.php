<?php

return array(
    '_default' =>
    array(
        'fields' =>
        array(
            0 => 'name',
            1 => 'price',
            2 => 'description',
        ),
        'name' => '_default',
        'label' => 'По-умолчанию',
    ),
    '_popup' =>
    array(
        'fields' =>
        array(
            'name',
        ),
        'name' => 'popup',
        'label' => 'По-умолчанию',
        'type' => 'popup',
    ),
);
