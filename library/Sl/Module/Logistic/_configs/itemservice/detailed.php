<?php
return array (
  'modulerelation_itemserviceservice' => 
  array (
    'label' => 'Услуга',
    'sort_order' => 100,
  ),
  'price' => 
  array (
    'sort_order' => 300,
  ),
  'description' => 
  array (
    'sort_order' => 500,
  ),
  'modulerelation_packageitemservice' => 
  array (
    'label' => 'Посылка',
    'readonly' => true,
  ),
);
