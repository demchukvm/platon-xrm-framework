<?php
return array (
  'name' => 
  array (
    'order' => 10,
    'label' => 'Название',
  ),
  'price' => 
  array (
    'order' => 20,
    'label' => 'Цена',
  ),
  'description' => 
  array (
    'order' => 30,
    'label' => 'Комментарий',
  ),
);
