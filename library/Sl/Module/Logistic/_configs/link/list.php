<?php
return array (
  'name' => 
  array (
    'order' => 10,
    'label' => 'NAME',
  ),
  'measure' => 
  array (
    'order' => 20,
    'label' => 'MEASURE',
  ),
  'delivery_time' => 
  array (
    'order' => 30,
    'label' => 'DELIVERY_TIME',
  ),
  'necessity' => 
  array (
    'order' => 40,
    'label' => 'NECESSITY',
  ),
);
