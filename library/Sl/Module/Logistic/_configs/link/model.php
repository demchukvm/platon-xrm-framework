<?php
return array (
  'name' => 
  array (
    'label' => 'Название пункта',
    'type' => 'text',
  ),
  'measure' => 
  array (
    'label' => 'Ед. измерения времени',
    'type' => 'text',
  ),
  'delivery_time' => 
  array (
    'label' => 'Время доставки',
    'type' => 'text',
    'class' => 'intPositive',
  ),
  'necessity' => 
  array (
    'label' => 'Руч. перевод',
    'type' => 'checkbox',
  ),
  'delivery_notify' => 
  array (
    'label' => 'Ув. кл.',
    'type' => 'checkbox',
  ),
  'create' => 
  array (
    'label' => 'CREATE',
    'type' => 'text',
  ),
  'id' => 
  array (
    'label' => 'ID',
    'type' => 'hidden',
  ),
  'active' => 
  array (
    'label' => 'ACTIVE',
    'type' => 'checkbox',
  ),
  'delay_receive' => 
  array (
    'label' => 'Принят',
    'type' => 'text',
  ),
  'delay_process' => 
  array (
    'label' => 'Обработка',
    'type' => 'text',
  ),
  'delay_send' => 
  array (
    'label' => 'Отправлен',
    'type' => 'text',
  ),
);
