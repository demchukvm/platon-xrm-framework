<?php

return array(
    '_default' =>
    array(
        'fields' =>
        array(
            0 => 'name',
            1 => 'measure',
            2 => 'delivery_time',
            3 => 'necessity',
            4 => 'delivery_notify',
            8 => 'delay_receive',
            9 => 'delay_process',
            10 => 'delay_send',
        ),
        'name' => '_default',
        'label' => 'По-умолчанию',
    ),
    '_popup' =>
    array(
        'fields' =>
        array(
            'name',
        ),
        'name' => 'popup',
        'label' => 'По-умолчанию',
        'type' => 'popup',
    ),
);
