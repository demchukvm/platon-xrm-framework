<?php
return array (
  'modulerelation_linkstock' => 
  array (
    'sort_order' => 1000,
    'label' => 'Склад',
  ),
  'name' => 
  array (
    'sort_order' => 2000,
  ),
  'delivery_time' => 
  array (
    'sort_order' => 3000,
    'default' => 1,
  ),
  'measure' => 
  array (
    'sort_order' => 4000,
    'default' => 'd',
  ),
  'delay_receive' => 
  array (
    'sort_order' => 5000,
    'default' => '0',
  ),
  'delay_process' => 
  array (
    'sort_order' => 6000,
    'default' => '3',
  ),
  'delay_send' => 
  array (
    'sort_order' => 6000,
    'default' => '12',
  ),
  'necessity' => 
  array (
    'sort_order' => 7000,
    'default' => '',
  ),
  'delivery_notify' => 
  array (
    'sort_order' => 8000,
    'default' => '',
  ),
);
