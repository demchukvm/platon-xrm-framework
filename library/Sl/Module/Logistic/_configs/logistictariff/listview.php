<?php
return array (
  'name' => 
  array (
    'label' => 'Тариф',
    'sortable' => true,
    'searchable' => true,
    'class' => 'span2',
  ),
  'title' => 
  array (
    'label' => 'Тарифный план',
    'sortable' => true,
    'searchable' => true,
    'class' => 'span1',
  ),
  'min_days' => 
  array (
    'label' => 'Мин. срок, дни',
    'sortable' => true,
    'searchable' => true,
    'class' => 'span1',
  ),
  'max_days' => 
  array (
    'label' => 'Макс. срок, дни',
    'sortable' => true,
    'searchable' => true,
    'class' => 'span1',
  ),
  'price_per_kg' => 
  array (
    'label' => 'Ставка за кг',
    'sortable' => true,
    'searchable' => true,
    'class' => 'span1',
  ),
  'first_price_per_kg' => 
  array (
    'label' => 'Себест. за кг',
    'sortable' => true,
    'searchable' => true,
    'class' => 'span1',
  ),
  'min_weight' => 
  array (
    'label' => 'Мин. вес, кг.',
    'sortable' => true,
    'searchable' => true,
    'class' => 'span1',
  ),
  'insurance_percent' => 
  array (
    'label' => 'Страховка, %',
    'sortable' => true,
    'searchable' => true,
    'class' => 'span1',
  ),
  'weight_factor' => 
  array (
    'label' => 'Весовая константа',
    'sortable' => true,
    'searchable' => true,
    'class' => 'span1',
  ),
  'weight_sill' => 
  array (
    'label' => 'Весовой порог',
    'sortable' => true,
    'searchable' => true,
    'class' => 'span1',
  ),
  'under_sill_price_per_kg' => 
  array (
    'label' => 'Ставка за кг до порога',
    'sortable' => true,
    'searchable' => true,
    'class' => 'span1',
  ),
  'count_factor' => 
  array (
    'label' => 'Количетв. конст.',
    'sortable' => true,
    'searchable' => true,
    'class' => 'span1',
  ),
  'count_surcharge' => 
  array (
    'label' => 'Доплата за шт.',
    'sortable' => true,
    'searchable' => true,
    'class' => 'span1',
  ),
  'pack_factor' => 
  array (
    'label' => 'Доплата за место',
    'sortable' => true,
    'searchable' => true,
    'class' => 'span1',
  ),
  'first_insurance_percent' => 
  array (
    'label' => 'Себестоимость страховки',
    'sortable' => true,
    'searchable' => true,
  ),
  'first_count_surcharge' => 
  array (
    'label' => 'Себестоимость за шт.',
    'sortable' => true,
    'searchable' => true,
  ),
  'first_pack_factor' => 
  array (
    'label' => 'Себестоимость за место',
    'sortable' => true,
    'searchable' => true,
  ),
);
