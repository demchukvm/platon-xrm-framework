<?php
return array (
  'modulerelation_logistictariffprodtype' => 
  array (
    'label' => 'Товары',
  ),
  'modulerelation_logistictariffroute' => 
  array (
    'label' => 'Маршруты',
  ),
  'create' => 
  array (
    'label' => 'Дата создания',
    'sort_order' => 15,
    'type' => 'date',
    'class' => 'current-date',
  ),
);
