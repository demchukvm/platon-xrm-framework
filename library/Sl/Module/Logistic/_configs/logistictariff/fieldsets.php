<?php

return array(
    '_default' =>
    array(
        'fields' =>
        array(
            0 => 'name',
            
            4 => 'price_per_kg',
            5 => 'first_price_per_kg',
            6 => 'min_weight',
            7 => 'insurance_percent',
            8 => 'weight_factor',
            9 => 'weight_sill',
            10 => 'under_sill_price_per_kg',
            11 => 'count_factor',
            12 => 'count_surcharge',
            13 => 'pack_factor',
            
        ),
        'name' => '_default',
        'label' => 'По-умолчанию',
    ),
    '_popup' =>
    array(
        'fields' =>
        array(
            'name',
        ),
        'name' => '_popup',
        'label' => 'По-умолчанию',
        'type' => 'popup',
    ),
);
