<?php
return array (
  'name' => 
  array (
    'label' => 'Тариф',
  ),
  'title' => 
  array (
    'label' => 'Название',
  ),
  'min_days' => 
  array (
    'label' => 'Мин. срок, дни',
  ),
  'max_days' => 
  array (
    'label' => 'Макс. срок, дни',
  ),
  'price_per_kg' => 
  array (
    'label' => 'Ставка за кг',
  ),
  'first_price_per_kg' => 
  array (
    'label' => 'Себестоимость за кг',
  ),
  'min_weight' => 
  array (
    'label' => 'Мин. вес, кг.',
  ),
  'insurance_percent' => 
  array (
    'label' => 'Страховка, %',
  ),
  'weight_factor' => 
  array (
    'label' => 'Весовая константа',
  ),
  'weight_sill' => 
  array (
    'label' => 'Весовой порог',
  ),
  'under_sill_price_per_kg' => 
  array (
    'label' => 'Ставка за кг до порога',
  ),
  'count_factor' => 
  array (
    'label' => 'Количеств. конст.',
  ),
  'count_surcharge' => 
  array (
    'label' => 'Доплата за шт.',
  ),
  'pack_factor' => 
  array (
    'label' => 'Доплата за место',
  ),
  'first_insurance_percent' => 
  array (
    'label' => 'Себестоимость страховки',
  ),
  'first_count_surcharge' => 
  array (
    'label' => 'Себестоимость за шт.',
  ),
  'first_pack_factor' => 
  array (
    'label' => 'Себестоимость за место',
  ),
  'modulerelation_chaintariff' => 
  array (
  ),
  'modulerelation_logistictariffroute' => 
  array (
  ),
  'modulerelation_logistictariffprodtype' => 
  array (
  ),
);
