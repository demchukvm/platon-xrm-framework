<?php
return array (
  'name' => 
  array (
    'order' => 10,
    'label' => 'Тариф',
    'sortable' => true,
    'searchable' => true,
    'class' => 'span2',
  ),
  'title' => 
  array (
    'order' => 20,
    'label' => 'Тарифный план',
    'visible' => false,
    'sortable' => true,
    'searchable' => true,
    'hidable' => true,
    'class' => 'span1',
  ),
  'min_days' => 
  array (
    'order' => 30,
    'label' => 'Мин. срок, дни',
    'visible' => false,
    'sortable' => true,
    'searchable' => true,
    'hidable' => true,
    'class' => 'span1',
  ),
  'max_days' => 
  array (
    'order' => 40,
    'label' => 'Макс. срок, дни',
    'visible' => false,
    'sortable' => true,
    'searchable' => true,
    'hidable' => true,
    'class' => 'span1',
  ),
  'price_per_kg' => 
  array (
    'order' => 50,
    'label' => 'Ставка за кг',
    'sortable' => true,
    'searchable' => true,
    'class' => 'span1',
  ),
  'first_price_per_kg' => 
  array (
    'order' => 60,
    'label' => 'Себест. за кг',
    'sortable' => true,
    'searchable' => true,
    'class' => 'span1',
  ),
  'min_weight' => 
  array (
    'order' => 70,
    'label' => 'Мин. вес, кг.',
    'sortable' => true,
    'searchable' => true,
    'class' => 'span1',
  ),
  'insurance_percent' => 
  array (
    'order' => 80,
    'label' => 'Страховка, %',
    'sortable' => true,
    'searchable' => true,
    'class' => 'span1',
  ),
  'weight_factor' => 
  array (
    'order' => 90,
    'label' => 'Весовая константа',
    'sortable' => true,
    'searchable' => true,
    'class' => 'span1',
  ),
  'weight_sill' => 
  array (
    'order' => 100,
    'label' => 'Весовой порог',
    'sortable' => true,
    'searchable' => true,
    'class' => 'span1',
  ),
  'under_sill_price_per_kg' => 
  array (
    'order' => 110,
    'label' => 'Ставка за кг до порога',
    'sortable' => true,
    'searchable' => true,
    'class' => 'span1',
  ),
  'count_factor' => 
  array (
    'order' => 120,
    'label' => 'Количетв. конст.',
    'sortable' => true,
    'searchable' => true,
    'class' => 'span1',
  ),
  'count_surcharge' => 
  array (
    'order' => 130,
    'label' => 'Доплата за шт.',
    'sortable' => true,
    'searchable' => true,
    'class' => 'span1',
  ),
  'pack_factor' => 
  array (
    'order' => 140,
    'label' => 'Доплата за место',
    'sortable' => true,
    'searchable' => true,
    'class' => 'span1',
  ),
  'first_insurance_percent' => 
  array (
    'label' => 'Себестоимость страховки',
    'order' => 150,
    'sortable' => true,
    'searchable' => true,
    'visible' => false,
    'hidable' => true,
  ),
  'first_count_surcharge' => 
  array (
    'label' => 'Себестоимость за шт.',
    'order' => 160,
    'sortable' => true,
    'searchable' => true,
    'visible' => false,
    'hidable' => true,
  ),
  'first_pack_factor' => 
  array (
    'label' => 'Себестоимость за место',
    'order' => 170,
    'sortable' => true,
    'searchable' => true,
    'visible' => false,
    'hidable' => true,
  ),
);
