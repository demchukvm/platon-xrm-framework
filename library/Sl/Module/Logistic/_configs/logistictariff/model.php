<?php
return array (
  'name' => 
  array (
    'label' => 'Тариф',
    'type' => 'text',
    'readonly' => true,
  ),
  'title' => 
  array (
    'label' => 'Название',
    'type' => 'text',
  ),
  'min_days' => 
  array (
    'label' => 'Мин. срок, дни',
    'type' => 'text',
    'class' => 'intPositive',
  ),
  'max_days' => 
  array (
    'label' => 'Макс. срок, дни',
    'type' => 'text',
    'class' => 'intPositive',
  ),
  'price_per_kg' => 
  array (
    'label' => 'Ставка за кг',
    'type' => 'text',
    'class' => 'floatPositive',
  ),
  'first_price_per_kg' => 
  array (
    'label' => 'Себестоимость за кг',
    'type' => 'text',
    'class' => 'floatPositive',
  ),
  'min_weight' => 
  array (
    'label' => 'Мин. вес, кг.',
    'type' => 'text',
    'class' => 'floatPositive fixedDigits-1',
  ),
  'insurance_percent' => 
  array (
    'label' => 'Страховка, %',
    'type' => 'text',
    'class' => 'floatPositive',
  ),
  'weight_factor' => 
  array (
    'label' => 'Весовая константа',
    'type' => 'text',
    'class' => 'intPositive',
  ),
  'weight_sill' => 
  array (
    'label' => 'Весовой порог',
    'type' => 'text',
    'class' => 'intPositive',
  ),
  'under_sill_price_per_kg' => 
  array (
    'label' => 'Ставка за кг до порога',
    'type' => 'text',
    'class' => 'floatPositive',
  ),
  'count_factor' => 
  array (
    'label' => 'Количеств. конст.',
    'type' => 'text',
    'class' => 'floatPositive',
  ),
  'count_surcharge' => 
  array (
    'label' => 'Доплата за шт.',
    'type' => 'text',
    'class' => 'floatPositive',
  ),
  'pack_factor' => 
  array (
    'label' => 'Доплата за место',
    'type' => 'text',
    'class' => 'floatPositive',
  ),
  'first_insurance_percent' => 
  array (
    'label' => 'Себестоимость страховки',
    'type' => 'text',
    'class' => 'floatPositive',
  ),
  'first_count_surcharge' => 
  array (
    'label' => 'Себестоимость за шт.',
    'type' => 'text',
    'class' => 'floatPositive',
  ),
  'first_pack_factor' => 
  array (
    'label' => 'Себестоимость за место',
    'type' => 'text',
    'class' => 'floatPositive',
  ),
  'id' => 
  array (
    'label' => 'ID',
    'type' => 'hidden',
  ),
  'active' => 
  array (
    'label' => 'ACTIVE',
    'type' => 'checkbox',
  ),
  'create' => 
  array (
    'label' => 'CREATE',
    'type' => 'text',
  ),
);
