<?php
return array (
  'final' => 
  array (
    'logistic_statuses' => 
    array (
      0 => 15,
    ),
    'logistic_transport_process' => 
    array (
      0 => 90,
    ),
  ),
  'canceled' => 
  array (
    'logistic_statuses' => 
    array (
      0 => -1,
    ),
    'logistic_transport_process' => 
    array (
      0 => -1,
    ),
  ),
  'processed' => 
  array (
    'logistic_statuses' => 
    array (
      0 => 10,
    ),
    'logistic_transport_process' => 
    array (
      0 => 20,
    ),
  ),
);
