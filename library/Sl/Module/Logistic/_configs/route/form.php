<?php
return array (
  'modulerelation_routecountry' => 
  array (
    'label' => 'Откуда',
    'field_filters' => 
    array (
      0 => 
      array (
        'field' => 'id',
        'matching' => 'nin',
        'value' => 'modulerelation_routecountrydestination',
      ),
    ),
  ),
  'modulerelation_logistictariffroute' => 
  array (
    'label' => 'Тарифные планы',
  ),
  'modulerelation_routecountrydestination' => 
  array (
    'label' => 'Куда',
    'field_filters' => 
    array (
      0 => 
      array (
        'field' => 'id',
        'matching' => 'nin',
        'value' => 'modulerelation_routecountry',
      ),
    ),
  ),
);
