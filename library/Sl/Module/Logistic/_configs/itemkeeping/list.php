<?php
return array (
  'date_start' => 
  array (
    'order' => 10,
    'label' => 'Дата начала',
  ),
  'date_end' => 
  array (
    'order' => 20,
    'label' => 'Дата окончания',
  ),
  'name' => 
  array (
    'order' => 30,
    'label' => 'Название',
  ),
  'price' => 
  array (
    'order' => 40,
    'label' => 'Цена',
  ),
  'description' => 
  array (
    'order' => 50,
    'label' => 'Комментарий',
  ),
);
