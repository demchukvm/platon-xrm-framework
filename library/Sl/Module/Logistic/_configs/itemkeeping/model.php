<?php
return array (
  'date_start' => 
  array (
    'label' => 'Дата начала',
    'type' => 'date',
    'sort_order' => 10,
  ),
  'date_end' => 
  array (
    'label' => 'Дата окончания',
    'type' => 'date',
    'sort_order' => 20,
  ),
  'name' => 
  array (
    'label' => 'Название',
    'type' => 'text',
    'sort_order' => 30,
    'readonly' => true,
  ),
  'price' => 
  array (
    'label' => 'Цена',
    'type' => 'text',
    'sort_order' => 40,
    'class' => 'floatPositive',
  ),
  'description' => 
  array (
    'label' => 'Комментарий',
    'type' => 'text',
    'sort_order' => 50,
  ),
  'id' => 
  array (
    'label' => 'ID',
    'type' => 'hidden',
  ),
  'active' => 
  array (
    'label' => 'ACTIVE',
    'type' => 'checkbox',
  ),
  'create' => 
  array (
    'label' => 'CREATE',
    'type' => 'text',
  ),
);
