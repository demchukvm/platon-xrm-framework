<?php
return array (
  'date_start' => 
  array (
    'label' => 'Дата начала',
  ),
  'date_end' => 
  array (
    'label' => 'Дата окончания',
  ),
  'name' => 
  array (
    'label' => 'Название',
  ),
  'price' => 
  array (
    'label' => 'Цена',
  ),
  'description' => 
  array (
    'label' => 'Комментарий',
  ),
);
