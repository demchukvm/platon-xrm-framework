<?php

return array(
    '_default' =>
    array(
        'fields' =>
        array(
            0 => 'date_start',
            1 => 'date_end',
            2 => 'name',
            3 => 'price',
            4 => 'description',
        ),
        'name' => '_default',
        'label' => 'По-умолчанию',
    ),
    '_popup' =>
    array(
        'fields' =>
        array(
            'name',
        ),
        'name' => 'popup',
        'label' => 'По-умолчанию',
        'type' => 'popup',
    ),
);
