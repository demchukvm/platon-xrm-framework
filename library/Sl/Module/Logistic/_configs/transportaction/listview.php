<?php
return array (
  'name' => 
  array (
    'label' => 'Действие',
    'select' => 1,
    'searchable' => true,
    'sortable' => true,
  ),
  'action_date' => 
  array (
    'label' => 'Дата',
    'searchable' => true,
    'type' => 'date',
    'sortable' => true,
  ),
  'description' => 
  array (
    'label' => 'Комментарий',
  ),
  'weight' => 
  array (
    'label' => 'Контр. вес',
  ),
  'weight_type' => 
  array (
    'label' => 'Тип взвешивания',
  ),
  'identifier' => 
  array (
    'label' => 'Ящик/Посылка',
    'searchable' => true,
    'sortable' => true,
  ),
  'stock_name' => 
  array (
    'label' => 'Склад',
    'searchable' => true,
    'sortable' => true,
  ),
);
