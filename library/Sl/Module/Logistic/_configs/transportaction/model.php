<?php
return array (
  'name' => 
  array (
    'label' => 'Действие',
    'type' => 'text',
    'sort_order' => 10,
  ),
  'action_date' => 
  array (
    'label' => 'Дата',
    'type' => 'date',
    'searchable' => true,
    'sort_order' => 20,
  ),
  'description' => 
  array (
    'label' => 'Комментарий',
    'type' => 'textarea',
    'sort_order' => 110,
  ),
  'weight' => 
  array (
    'label' => 'Контр. вес',
    'type' => 'text',
    'class' => 'floatPositive fixedDigits-4',
    'sort_order' => 40,
  ),
  'weight_type' => 
  array (
    'label' => 'Посылка/Ящик',
    'type' => 'checkbox',
    'class' => '',
    'sort_order' => 50,
  ),
  'id' => 
  array (
    'label' => 'ID',
    'type' => 'hidden',
  ),
  'active' => 
  array (
    'label' => 'ACTIVE',
    'type' => 'hidden',
  ),
  'status' => 
  array (
    'label' => 'Статус',
    'type' => 'text',
    'sort_order' => 30,
  ),
  'create' => 
  array (
    'label' => 'CREATE',
    'type' => 'hidden',
  ),
  'stock_name' => 
  array (
    'label' => 'Название склада',
    'type' => 'hidden',
  ),
  'identifier' => 
  array (
    'label' => 'Посылка/Ящик',
    'type' => 'hidden',
  ),
);
