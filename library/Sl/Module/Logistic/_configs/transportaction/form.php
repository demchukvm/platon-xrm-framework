<?php
return array (
  'modulerelation_tractionuser' => 
  array (
    'label' => 'Ответственный',
    'sort_order' => 80,
  ),
  'modulerelation_tractionstock' => 
  array (
    'label' => 'Склад',
    'sort_order' => 90,
  ),
  'modulerelation_tractionpackage' => 
  array (
    'label' => 'Посылка',
    'sort_order' => 70,
  ),
  'modulerelation_tractionbox' => 
  array (
    'label' => 'Ящик',
    'sort_order' => 60,
  ),
  'modulerelation_tractionstockdestination' => 
  array (
    'label' => 'Склад назначения',
    'sort_order' => 100,
  ),
);
