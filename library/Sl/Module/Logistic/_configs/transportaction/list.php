<?php
return array (
  'name' => 
  array (
    'order' => 10,
    'label' => 'Действие',
    'select' => 1,
    'searchable' => true,
    'sortable' => true,
  ),
  'action_date' => 
  array (
    'order' => 20,
    'label' => 'Дата',
    'searchable' => true,
    'type' => 'date',
    'sortable' => true,
  ),
  'description' => 
  array (
    'label' => 'Комментарий',
    'order' => 100,
  ),
  'weight' => 
  array (
    'order' => 40,
    'label' => 'Контр. вес',
  ),
  'weight_type' => 
  array (
    'order' => 50,
    'visible' => false,
    'label' => 'Тип взвешивания',
  ),
  'identifier' => 
  array (
    'label' => 'Ящик/Посылка',
    'order' => 60,
    'searchable' => true,
    'sortable' => true,
  ),
  'stock_name' => 
  array (
    'label' => 'Склад',
    'order' => 90,
    'searchable' => true,
    'sortable' => true,
  ),
);
