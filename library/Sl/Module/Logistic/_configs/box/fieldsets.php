<?php

return array(
    '_default' =>
    array(
        'fields' =>
        array(
            0 => 'length',
            1 => 'width',
            2 => 'tare_weight',
            3 => 'height',
            4 => 'weight',
            5 => 'description',
            6 => 'filling',
            9 => 'name',
            10 => 'price_per_wrapping',
            11 => 'volume',
            13 => 'master_relation',
            15 => 'is_issued',
        ),
        'name' => '_default',
        'label' => 'По-умолчанию',
    ),
    '_popup' =>
    array(
        'fields' =>
        array(
            'name',
        ),
        'name' => 'popup',
        'label' => 'По-умолчанию',
        'type' => 'popup',
    ),
);
