<?php
return array (
  'length' => 
  array (
    'label' => 'Длина, см',
    'class' => 'intPositive',
    'type' => 'text',
  ),
  'width' => 
  array (
    'label' => 'Ширина, см',
    'class' => 'intPositive',
    'type' => 'text',
  ),
  'tare_weight' => 
  array (
    'label' => 'Вес тары, кг',
    'class' => 'floatPositive',
    'type' => 'text',
  ),
  'height' => 
  array (
    'label' => 'Высота, см',
    'class' => 'floatPositive',
    'type' => 'text',
  ),
  'weight' => 
  array (
    'label' => 'Вес, кг',
    'type' => 'text',
    'class' => 'floatPositive',
    'readonly' => true,
  ),
  'description' => 
  array (
    'label' => 'Описание',
    'type' => 'textarea',
  ),
  'filling' => 
  array (
    'label' => 'Наполнен',
    'type' => 'hidden',
  ),
  'id' => 
  array (
    'label' => 'ID',
    'type' => 'hidden',
  ),
  'active' => 
  array (
    'label' => 'ACTIVE',
    'type' => 'checkbox',
  ),
  'name' => 
  array (
    'label' => 'Идентификаторы',
    'type' => 'text',
    'readonly' => 'readonly',
  ),
  'price_per_wrapping' => 
  array (
    'label' => 'Цена упаковки',
    'type' => 'text',
    'class' => 'floatPositive',
  ),
  'volume' => 
  array (
    'label' => 'Объем м.куб.',
    'type' => 'text',
    'readonly' => true,
  ),
  'create' => 
  array (
    'label' => 'CREATE',
    'type' => 'text',
  ),
  'master_relation' => 
  array (
    'label' => 'MASTER_RELATION',
    'type' => 'text',
  ),
  'archived' => 
  array (
    'label' => 'ARCHIVED',
    'type' => 'text',
  ),
  'is_issued' => 
  array (
    'label' => 'IS_ISSUED',
    'type' => 'hidden',
  ),
);
