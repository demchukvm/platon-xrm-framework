<?php
return array (
  'length' => 
  array (
    'order' => 10,
    'label' => 'Длина, см',
  ),
  'width' => 
  array (
    'order' => 20,
    'label' => 'Ширина, см',
  ),
  'tare_weight' => 
  array (
    'order' => 30,
    'label' => 'Вес тары, кг',
  ),
  'height' => 
  array (
    'order' => 40,
    'label' => 'Высота, см',
  ),
  'weight' => 
  array (
    'order' => 50,
    'label' => 'Вес, кг',
  ),
  'description' => 
  array (
    'order' => 60,
    'label' => 'Описание',
  ),
  'filling' => 
  array (
    'order' => 70,
    'label' => 'Наполнен',
  ),
  'name' => 
  array (
    'order' => 80,
    'label' => 'Идентификаторы',
  ),
  'price_per_wrapping' => 
  array (
    'order' => 90,
    'label' => 'Цена упаковки',
  ),
  'volume' => 
  array (
    'order' => 100,
    'label' => 'Объем м.куб.',
  ),
);
