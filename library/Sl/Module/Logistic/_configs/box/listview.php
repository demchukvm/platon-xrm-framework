<?php
return array (
  'length' => 
  array (
    'label' => 'Длина, см',
  ),
  'width' => 
  array (
    'label' => 'Ширина, см',
  ),
  'tare_weight' => 
  array (
    'label' => 'Вес тары, кг',
  ),
  'height' => 
  array (
    'label' => 'Высота, см',
  ),
  'weight' => 
  array (
    'label' => 'Вес, кг',
  ),
  'description' => 
  array (
    'label' => 'Описание',
  ),
  'filling' => 
  array (
    'label' => 'Наполнен',
  ),
  'name' => 
  array (
    'label' => 'Идентификаторы',
  ),
  'price_per_wrapping' => 
  array (
    'label' => 'Цена упаковки',
  ),
  'volume' => 
  array (
    'label' => 'Объем м.куб.',
  ),
);
