<?php
return array (
  'name' => 
  array (
    'sort_order' => 20,
  ),
  'status' => 
  array (
    'sort_order' => 30,
  ),
  'fixation_date' => 
  array (
    'sort_order' => 40,
  ),
  'modulerelation_theftpackage' => 
  array (
    'label' => 'Посылка',
    'sort_order' => 50,
    'request_fields' => 
    array (
      0 => 'name',
      1 => 'packagecustomer:name',
    ),
  ),
  'modulerelation_theftpack' => 
  array (
    'label' => 'Ящики',
    'sort_order' => 60,
    'field_filters' => 
    array (
      0 => 
      array (
        'field' => 'packagepack:id',
        'matching' => 'in',
        'value' => 'modulerelation_theftpackage',
      ),
    ),
  ),
  'modulerelation_theftcustomer' => 
  array (
    'label' => 'Потерпевший',
    'sort_order' => 70,
    'readonly' => true,
  ),
  'insurance_percent' => 
  array (
    'label' => 'Страховка, %',
    'class' => 'floatPositive',
    'sort_order' => 100,
  ),
  'insurance_type' => 
  array (
    'label' => 'Тип страховки',
    'sort_order' => 120,
  ),
  'declarated_sum' => 
  array (
    'class' => 'floatPositive',
    'sort_order' => 150,
  ),
  'package_weight' => 
  array (
    'sort_order' => 160,
  ),
  'weight' => 
  array (
    'sort_order' => 170,
  ),
  'sum' => 
  array (
    'sort_order' => 1000,
  ),
  'description' => 
  array (
    'sort_order' => 17000,
  ),
);
