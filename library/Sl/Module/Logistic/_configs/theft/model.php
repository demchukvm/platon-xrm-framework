<?php
return array (
  'name' => 
  array (
    'label' => 'Название',
    'type' => 'text',
  ),
  'sum' => 
  array (
    'label' => 'Оцененная стоимость',
    'type' => 'text',
    'class' => 'floatPositive',
  ),
  'id' => 
  array (
    'label' => 'ID',
    'type' => 'hidden',
  ),
  'active' => 
  array (
    'label' => 'ACTIVE',
    'type' => 'checkbox',
  ),
  'fixation_date' => 
  array (
    'label' => 'Дата обнаружения',
    'type' => 'date',
  ),
  'description' => 
  array (
    'label' => 'Описание',
    'type' => 'textarea',
  ),
  'create' => 
  array (
    'label' => 'CREATE',
    'type' => 'text',
  ),
  'status' => 
  array (
    'label' => 'Статус',
    'type' => 'text',
  ),
  'package_weight' => 
  array (
    'label' => 'Вес посылки',
    'type' => 'text',
    'class' => 'floatPositive fixedDigits-3',
    'readonly' => true,
  ),
  'weight' => 
  array (
    'label' => 'Пропавший вес',
    'type' => 'text',
    'class' => 'floatPositive fixedDigits-3',
  ),
  'insurance_percent' => 
  array (
    'label' => 'Страховка, %',
    'type' => 'text',
    'class' => 'floatPositive',
    'readonly' => true,
  ),
  'insurance_type' => 
  array (
    'label' => 'Расчет вручную',
    'type' => 'checkbox',
  ),
  'declarated_sum' => 
  array (
    'label' => 'Декларация',
    'class' => 'floatPositive',
    'type' => 'text',
    'readonly' => true,
  ),
);
