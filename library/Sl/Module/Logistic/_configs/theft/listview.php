<?php
return array (
  'name' => 
  array (
    'label' => 'Название',
  ),
  'sum' => 
  array (
    'label' => 'Оцененная стоимость',
  ),
  'fixation_date' => 
  array (
    'label' => 'Дата обнаружения',
  ),
  'description' => 
  array (
    'label' => 'Описание',
  ),
  'status' => 
  array (
    'label' => 'STATUS',
  ),
  'package_weight' => 
  array (
    'label' => 'Вес посылки',
  ),
  'weight' => 
  array (
    'label' => 'Пропавший вес',
  ),
  'insurance_type' => 
  array (
    'label' => 'Расчет вручную',
  ),
  'declarated_sum' => 
  array (
    'label' => 'Декларация',
  ),
);
