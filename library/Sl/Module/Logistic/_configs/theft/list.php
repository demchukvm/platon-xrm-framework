<?php
return array (
  'name' => 
  array (
    'order' => 10,
    'label' => 'Название',
  ),
  'sum' => 
  array (
    'order' => 20,
    'label' => 'Оцененная стоимость',
  ),
  'fixation_date' => 
  array (
    'order' => 30,
    'label' => 'Дата обнаружения',
  ),
  'description' => 
  array (
    'order' => 40,
    'label' => 'Описание',
  ),
  'status' => 
  array (
    'order' => 50,
    'label' => 'STATUS',
  ),
  'package_weight' => 
  array (
    'label' => 'Вес посылки',
    'order' => 60,
    'visible' => false,
    'hidable' => true,
  ),
  'weight' => 
  array (
    'label' => 'Пропавший вес',
    'order' => 70,
    'visible' => false,
    'hidable' => true,
  ),
  'insurance_type' => 
  array (
    'label' => 'Расчет вручную',
    'order' => 90,
    'visible' => false,
    'hidable' => true,
  ),
  'declarated_sum' => 
  array (
    'label' => 'Декларация',
    'order' => 100,
    'visible' => false,
    'hidable' => true,
  ),
);
