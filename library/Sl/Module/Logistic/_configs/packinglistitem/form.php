<?php
return array (
  'name' => 
  array (
    'label' => 'Комментарий',
    'sort_order' => 20,
  ),
  'modulerelation_packinglistitemproduct' => 
  array (
    'sort_order' => 10,
    'label' => 'Номенклатура',
    'class' => 'span1',
  ),
  'count' => 
  array (
    'sort_order' => 40,
  ),
  'plcs' => 
  array (
    'sort_order' => 50,
  ),
  'gross_weight' => 
  array (
    'sort_order' => 60,
  ),
  'net_weight' => 
  array (
    'sort_order' => 70,
  ),
  'cost' => 
  array (
    'sort_order' => 80,
  ),
  'sum' => 
  array (
    'sort_order' => 90,
  ),
);
