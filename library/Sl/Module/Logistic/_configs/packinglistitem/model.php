<?php
return array (
  'product_code' => 
  array (
    'label' => 'Код',
    'type' => 'text',
  ),
  'name' => 
  array (
    'label' => 'Наименование',
    'type' => 'text',
  ),
  'count' => 
  array (
    'label' => 'Кол-во',
    'type' => 'text',
    'class' => 'intPositive',
  ),
  'cost' => 
  array (
    'label' => 'Стоимость',
    'type' => 'text',
    'class' => 'floatPositive',
  ),
  'sum' => 
  array (
    'label' => 'Сумма',
    'type' => 'text',
    'class' => 'floatPositive',
  ),
  'plcs' => 
  array (
    'label' => 'Мест',
    'type' => 'text',
    'class' => 'intPositive',
  ),
  'create' => 
  array (
    'label' => 'CREATE',
    'type' => 'text',
  ),
  'id' => 
  array (
    'label' => 'ID',
    'type' => 'hidden',
  ),
  'active' => 
  array (
    'label' => 'ACTIVE',
    'type' => 'checkbox',
  ),
  'archived' => 
  array (
    'label' => 'ARCHIVED',
    'type' => 'text',
  ),
  'net_weight' => 
  array (
    'label' => 'Нетто',
    'type' => 'text',
    'class' => 'floatPositive',
  ),
  'gross_weight' => 
  array (
    'label' => 'Брутто',
    'type' => 'text',
    'class' => 'floatPositive',
  ),
);
