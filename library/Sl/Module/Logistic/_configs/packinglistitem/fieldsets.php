<?php

return array(
    '_default' =>
    array(
        'fields' =>
        array(
            0 => 'product_code',
            1 => 'name',
            2 => 'count',
            3 => 'cost',
            4 => 'sum',
            5 => 'plcs',
            10 => 'net_weight',
            11 => 'gross_weight',
        ),
        'name' => '_default',
        'label' => 'По-умолчанию',
    ),
    '_popup' =>
    array(
        'fields' =>
        array(
            'name',
        ),
        'name' => 'popup',
        'label' => 'По-умолчанию',
        'type' => 'popup',
    ),
);
