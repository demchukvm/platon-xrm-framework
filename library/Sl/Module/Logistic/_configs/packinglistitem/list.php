<?php
return array (
  'count' => 
  array (
    'order' => 10,
    'label' => 'COUNT',
  ),
  'cost' => 
  array (
    'order' => 20,
    'label' => 'COST',
  ),
  'sum' => 
  array (
    'order' => 30,
    'label' => 'SUM',
  ),
  'plcs' => 
  array (
    'order' => 40,
    'label' => 'PLCS',
  ),
  'archived' => 
  array (
    'order' => 50,
    'label' => 'ARCHIVED',
  ),
);
