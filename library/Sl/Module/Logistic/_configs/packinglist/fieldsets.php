<?php

return array(
    '_default' =>
    array(
        'fields' =>
        array(
            0 => 'net_weight',
            1 => 'switch',
            2 => 'gross_weight',
            3 => 'identifier',
            5 => 'plcs',
            6 => 'sum',
        ),
        'name' => '_default',
        'label' => 'По-умолчанию',
    ),
    '_popup' =>
    array(
        'fields' =>
        array(
            'identifier',
        ),
        'name' => 'popup',
        'label' => 'По-умолчанию',
        'type' => 'popup',
    ),
);
