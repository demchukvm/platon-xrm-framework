<?php
return array (
  'net_weight' => 
  array (
    'label' => 'Нетто',
    'type' => 'text',
    'class' => 'floatPositive',
  ),
  'switch' => 
  array (
    'label' => 'switch',
    'type' => 'checkbox',
    'class' => 'make-switch',
  ),
  'gross_weight' => 
  array (
    'label' => 'Брутто',
    'type' => 'text',
    'class' => 'floatPositive',
  ),
  'identifier' => 
  array (
    'label' => 'Номер invoce',
    'type' => 'text',
  ),
  'create' => 
  array (
    'label' => 'CREATE',
    'type' => 'text',
  ),
  'plcs' => 
  array (
    'label' => 'Мест',
    'type' => 'text',
    'class' => 'intPositive',
  ),
  'sum' => 
  array (
    'label' => 'Сумма',
    'type' => 'text',
    'class' => 'floatPositive',
  ),
  'id' => 
  array (
    'label' => 'ID',
    'type' => 'hidden',
  ),
  'active' => 
  array (
    'label' => 'ACTIVE',
    'type' => 'checkbox',
  ),
  'archived' => 
  array (
    'label' => 'ARCHIVED',
    'type' => 'text',
  ),
);
