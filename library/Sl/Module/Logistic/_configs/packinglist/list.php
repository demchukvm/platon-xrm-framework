<?php
return array (
  'packinglistpack.identifier' => 
  array (
    'order' => 10,
    'label' => 'Ящик',
    'searchable' => true,
    'sortable' => true,
  ),
  'net_weight' => 
  array (
    'order' => 20,
    'label' => 'Вес нетто',
  ),
  'gross_weight' => 
  array (
    'order' => 30,
    'label' => 'Вес брутто',
  ),
  'sum' => 
  array (
    'order' => 40,
    'label' => 'Сумма',
  ),
);
