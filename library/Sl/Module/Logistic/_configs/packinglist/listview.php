<?php
return array (
  'packinglistpack.identifier' => 
  array (
    'label' => 'Ящик',
    'searchable' => true,
    'sortable' => true,
  ),
  'net_weight' => 
  array (
    'label' => 'Вес нетто',
  ),
  'gross_weight' => 
  array (
    'label' => 'Вес брутто',
  ),
  'sum' => 
  array (
    'label' => 'Сумма',
  ),
);
