<?php

return array(
    '_default' =>
    array(
        'fields' =>
        array(
            0 => 'name',
            1 => 'chainlink.name',
            2 => 'chaintariff.name',
        ),
        'name' => '_default',
        'label' => 'По-умолчанию',
    ),
    '_popup' =>
    array(
        'fields' =>
        array(
            'name',
        ),
        'name' => 'popup',
        'label' => 'По-умолчанию',
        'type' => 'popup',
    ),
);
