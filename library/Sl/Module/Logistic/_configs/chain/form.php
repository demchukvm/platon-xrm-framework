<?php
return array (
  'name' => 
  array (
    'required' => true,
  ),
  'modulerelation_chainlink' => 
  array (
    'label' => 'Пункты',
    'sort_order' => 0,
  ),
  'modulerelation_chaintariff' => 
  array (
    'label' => 'Тариф',
    'sort_order' => 10,
    'required' => true,
  ),
);
