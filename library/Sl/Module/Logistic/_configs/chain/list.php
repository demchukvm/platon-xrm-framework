<?php
return array (
  'name' => 
  array (
    'order' => 10,
    'label' => 'Название',
    'searchable' => true,
    'sortable' => true,
  ),
  'chaintariff' => 
  array (
    'label' => 'Тариф',
  ),
  'chaintariff.name' => 
  array (
    'order' => 20,
  ),
  'chainlink' => 
  array (
    'label' => 'Пункты',
  ),
  'chainlink.name' => 
  array (
    'order' => 30,
  ),
);
