$(document).ready(function(){
    _.each($('.logistic_dealertarifsets_informer'), function(informer){
        var $informer = $(informer);
        var hash = function() {
            var aTemp = [];
            _.each($('form', $informer).serializeArray(), function(el, i){
                if(el.value.length !== 0 && el.value !== '0.00') {
                    aTemp.push(el.name+el.value);
                }
            });
            return hex_md5(aTemp.join('-::-'));
        };
        var _hash = hash();
        var checker = function() {
            if(_hash === hash()) {
                $('.btn.update', $informer).addClass('disabled');
            } else {
                $('.btn.update', $informer).removeClass('disabled');
            }
        };
        $('form', $informer).on('change', 'input, select, textarea', checker);
        checker();

        $informer.on('click', '.btn.update:not(.disabled)', function() {
            var data = {
                ajaxed: 1
            };
            _.each($('form', $informer).serializeArray(), function(el){
                data[el.name] = el.value;
            });
            $.ajax({
                type: 'POST',
                cache: false,
                url: $informer.data('rel'),
                data: data,
                success: function(d) {
                    if(d.result) {
                        $informer.find('.btn.update').blink({
                            items: {
                                color: 'green'
                            },
                            cb: function() {
                                _hash = hash();
                                checker();
                            }
                        });
                    } else {
                        $.alert(d.description);
                    }
                }
            });
        });
    });
});