$(document).ready(function(){
    var $form = $('body[data-alias="logistic.package"] form.form-model.package:first');
    if($form && $form.length) {
        $('.breadcrumb').append($('<span class="status_description_wrapper pull-right" />'));
        var $wrapper = $('.breadcrumb .status_description_wrapper');
        $.ajax({
            type: 'POST',
            cache: false,
            url: '/logistic/main/ajaxstatusdatehistory/',
            data: {
                id: $('#id', $form).val()
            },
            success: function(data) {
                if(data.result) {
                    $('.status_description_wrapper .item').remove();
                    _.each(data.data, function(el, i){
                        if(el.new_value === '10') {
                            if($('#bcbuttonsreceivedate').length) {
                                $('#bcbuttonsreceivedate').find('input').addClass('received');
                            }
                        }
                        $('#logistic_package_status_date_tmpl').tmpl(el).appendTo($wrapper);
                    });
                } else {
                    $.alert(data.description);
                }
            }
        });
        console.log($('#status_date').val());
    }
});