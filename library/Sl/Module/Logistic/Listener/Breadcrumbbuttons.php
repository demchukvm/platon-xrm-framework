<?php
namespace Sl\Module\Logistic\Listener;

use Sl_Service_Acl as AclService;

class Breadcrumbbuttons extends \Sl_Listener_Abstract implements \Sl_Listener_View_Interface, \Sl_Listener_Acl_Interface {
    
    const STATUS_DATE = 'bcstatusdate';
    
    public function onHeadLink(\Sl_Event_View $event) {
        $event->getView()->headLink()->appendStylesheet('/logistic/buttons/'.self::STATUS_DATE.'.css');
    }

    public function onHeadScript(\Sl_Event_View $event) {
        $event->getView()->headScript()->appendFile('/logistic/buttons/'.self::STATUS_DATE.'.js');
    }
    
    public function onAfterAclCreate(\Sl_Event_Acl $event) {
        $resource = AclService::joinResourceName(array(
            'type' => AclService::RES_TYPE_MVC,
            'module' => $this->getModule()->getName(),
            'controller' => 'main',
            'action' => 'ajaxstatusdatehistory',
        ));
        
        if(!$event->getAcl()->has($resource)) {
            $event->getAcl()->addResource($resource);
        }
        
        if(!AclService::isAllowed($resource, AclService::PRIVELEGE_ACCESS)) {
            AclService::acl()->allow(null, $resource, AclService::PRIVELEGE_ACCESS);
        }
    }
    
    public function onBodyEnd(\Sl_Event_View $event) {
        echo $event->getView()->partial('partials/package_status_history.phtml');
    }
    
    public function onAfterContent(\Sl_Event_View $event) {}
    public function onBeforeContent(\Sl_Event_View $event) {}
    public function onBeforePageHeader(\Sl_Event_View $event) {}
    public function onBodyBegin(\Sl_Event_View $event) {}
    public function onContent(\Sl_Event_View $event) {}
    public function onFooter(\Sl_Event_View $event) {}
    public function onHeadTitle(\Sl_Event_View $event) {}
    public function onHeader(\Sl_Event_View $event) {}
    public function onLogo(\Sl_Event_View $event) {}
    public function onNav(\Sl_Event_View $event) {}
    public function onPageOptions(\Sl_Event_View $event) {}
    public function onBeforeAclCreate(\Sl_Event_Acl $event) {}
    public function onIsAllowed(\Sl_Event_Acl $event) {}

}