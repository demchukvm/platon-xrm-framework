<?php

namespace Sl\Module\Logistic\Listener;

class Packagelistview extends \Sl_Listener_Abstract implements \Sl\Listener\Fieldset, \Sl\Listener\Dataset {

    protected $_models = array();
    protected $_s;

    public function onAfterProcessAll(\Sl\Event\Dataset $event) {
        
    }

    public function onAfterProcessItem(\Sl\Event\Dataset $event) {

        if ($event->getModel() instanceof \Sl\Module\Logistic\Model\Package) {

            $item = $event->getItem();

            if (isset($item['create'])) {
                //print_R($Obj); die;
                $matches = array();
                if (preg_match('/^(.+) (.+)$/', $item['create'], $matches)) {
                    $item['create'] = $matches[1];
                }
            }

            if (isset($item['packagecustomer']['customerdealer']['id']) && $item['packagecustomer']['customerdealer']['id'] > 0) {
                $item['identifier'] .= '<a class="htmlify icon-star-empty pull-right" title="' . $this->getTranslator()->translate('Привлечен дилером') . '"></a>';
            }
            if (isset($item['packagecustomer']['customerisdealer']['id']) && $item['packagecustomer']['customerisdealer']['id'] > 0) {
                $item['identifier'] .= '<a class="htmlify icon-asterisk pull-right" title="' . $this->getTranslator()->translate('Дилер') . '"></a>';
            }

            if (isset($item['packagefile']['id']) && $item['packagefile']['id'] > 0) {
                $item['identifier'] .= '<a class="htmlify icon-paperclip pull-right" title="' . $this->getTranslator()->translate('files included') . '"></a>';
            }
            if (isset($item['packagepackinglist']['id']) && $item['packagepackinglist']['id'] > 0) {
                $item['identifier'] .= '<a class="htmlify icon-th-list pull-right" title="' . $this->getTranslator()->translate('invoice included') . '"></a>';
            }
            if (isset($item['identifier']) && isset($item['identifier_ext']) && $item['identifier_ext']) {
                $item['identifier'] = implode('/', array($item['identifier'], $item['identifier_ext']));
            }
            $event->setItem($item);
        }
    }

    public function onBeforeProcessAll(\Sl\Event\Dataset $event) {
        if ($event->getModel() instanceof \Sl\Module\Logistic\Model\Package) {
            //  $this->_s = microtime(true);
            $this->_models = \Sl_Model_Factory::mapper($event->getModel())->fetchAll('id in(' . implode(', ', array_map(function($el) {
                                        return $el['id'];
                                    }, $event->getDataset()->getData(true))) . ')');
            //echo microtime(true) - $s;die;
            //print_R($this->_models); die;
        }
    }

    public function onBeforeProcessItem(\Sl\Event\Dataset $event) {
        $item = $event->getItem();

        if ($event->getModel() instanceof \Sl\Module\Logistic\Model\Package) {
            $model = \Sl_Model_Factory::mapper($event->getModel())->find($item['id']);

            $billitems = \Sl\Module\Docs\Service\Billcreator::getBillInfo($model);

            if (count($billitems)) {
                $item['total_sum'] = '<span class = "dt-package-total_sum"><strong class="billed">' . $item['total_sum'] . '</strong></span>';
            } else {
                $item['total_sum'] = '<span class = "dt-package-total_sum">' . $item['total_sum'] . '</span>';
            }

            $event->setItem($item);

            //блок, що виводить проплачену суму і назву кас
            
            $need_cashdesk = $event->getFieldset()->getField('packagefinoperation_cash')->hasRole('render');
            $need_paid_sum = $event->getFieldset()->getField('packagefinoperation_paid_sum')->hasRole('render');
            
            if($need_cashdesk || $need_paid_sum) {
                $item['packagefinoperation_paid_sum'] = sprintf('%.2f', 0);
                $item['packagefinoperation_cash'] = '';
                if ($model->getFinancialStatus() != 0) { // Не оплачено
                    $bills = \Sl\Module\Docs\Service\Billcreator::getBillsInfo($model);
                    if (count($bills['bills']['package'][$model->getId()])) {
                        $res_data = array('sum' => 0, 'cashdesc' => array());
                        foreach (array_keys($bills['bills']['package'][$model->getId()]) as $bill_id) {
                            $bill = \Sl_Model_Factory::mapper('bill', 'docs')->findExtended($bill_id, array('billpayment'));
                            if ($bill) {
                                if ($bill->getFinStatus() != 0) {
                                    foreach ($bill->fetchRelated('billpayment') as $payment) {
                                        /* @var $payment \Sl\Module\Finance\Model\Payment */
                                        if ($payment->isFinal()) {
                                            $res_data['sum'] += 100 * $payment->getSummApprove();
                                            $payment = \Sl_Model_Factory::mapper($payment)->findRelation($payment, 'paymentcashdesc');
                                            if ($payment) {
                                                $cashdesk = $payment->fetchOneRelated('paymentcashdesc');
                                                if ($cashdesk) {
                                                    $res_data['cashdesc'][$cashdesk->getId()] = $cashdesk->getName();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        $item['packagefinoperation_paid_sum'] = sprintf('%.2f', ($res_data['sum'] / 100));
                        $item['packagefinoperation_cash'] = implode(', ', $res_data['cashdesc']);
                    }
                }
                $event->setItem($item);
            }
        }
    }

    public function onPrepare(\Sl\Event\Fieldset $event) {
        if ($event->getModel() instanceof \Sl\Module\Logistic\Model\Package) {

            $addition_fields = array(
                'packagecustomer.customerdealer.id' => array('label' => '',
                    'roles' => array(
                        'from',)),
                'packagecustomer.customerisdealer.id' => array('label' => '',
                    'roles' => array(
                        'from',)),
                'packagefile.id' => array('label' => '',
                    'roles' => array(
                        'from',)),
              /*  'packagepackinglist.id' => array('label' => '',
                    'roles' => array(
                        'from',)),
                'packagepackinglist.id' => array('label' => '',
                    'roles' => array(
                        'from',)),*/
                'packagepackinglist.id' => array('label' => '',
                    'roles' => array(
                        'from',)),
                /*'packagefinoperation_cash' => array(
                    'label' => 'Кассы',
                    'roles' => array(
                        'render',)),
                'packagefinoperation_paid_sum' => array(
                    'label' => 'Оплачено',
                    'roles' => array(
                        'render',)),*/
               'total_sum' => array(),
                'financial_status' => array(
                    'roles' => array(
                        'from',)),
		'identifier_ext' => array('roles' => array('from')),
            );

            foreach ($addition_fields as $field => $options) {
                if (!$event->getFieldset()->hasField($field)) {
                    $cur_field = $event->getFieldset()->createField($field);
                } else {
                    $cur_field = $event->getFieldset()->getField($field);
                }
                $cur_field->fill($options);
            }
        }
    }

    public function onPrepareAjax(\Sl\Event\Fieldset $event) {
        
    }

    /*
     * To change this template, choose Tools | Templates
     * and open the template in the editor.
     */
}
