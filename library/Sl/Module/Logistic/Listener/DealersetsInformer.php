<?php
namespace Sl\Module\Logistic\Listener;

use Sl_Service_Acl as AclService;

class DealersetsInformer extends \Sl_Listener_Abstract implements \Sl\Listener\View\Informer\Informer, \Sl_Listener_Router_Interface, \Sl_Listener_View_Interface {
    
    protected $_active = false;
    
    public function onRouteShutdown(\Sl_Event_Router $event) {
        if($event->getRequest()->getModuleName() === 'customers') {
            if($event->getRequest()->getControllerName() === 'dealer') {
                if($event->getRequest()->getActionName() === 'edit') {
                    if($id = $event->getRequest()->getParam('id', false)) {
                        $this->_active = $id;
                    }
                }
            }
        }
    }
    
    public function onInformer(\Sl_Event_View $event) {
        try {
            if($this->getActive()) {
                if($event->getView()->is_iframe) return;

                $dealer = \Sl_Model_Factory::mapper('dealer', 'customers')->findExtended($this->getActive(), array('dealertariffsetsdealer'));
                if(!$dealer) {
                    throw new \Exception('Can\'t determone dealer from id. '.__METHOD__);
                }
                $model = $dealer->fetchOneRelated('dealertariffsetsdealer');
                if(!$model) {
                    throw new \Exception('Dealer has no "dealertariffsetsdealer" relation. '.__METHOD__);
                }

                $edit_resource = AclService::joinResourceName(array(
                    'type' => AclService::RES_TYPE_MVC,
                    'module' => $model->findModuleName(),
                    'controller' => $model->findModelName(),
                    'action' => 'edit'
                ));
                if(AclService::isAllowed($edit_resource, AclService::PRIVELEGE_ACCESS)) {
                    $event->getView()->calc_script = \Sl\Serializer\Serializer::getCalculatorsJS($model);

                    $container = new \Sl\Module\Home\Informer\Item();
                    try {
                    $container  ->setTitle($this->getTranslator()->translate('Настройки дилера'))
                                ->setId('logistic_dealertarifsets_informer')
                                ->setIcon('glyphicons_137_cogwheels.png')
                                ->setContent($event->getView()->partial('partials/dealertariffsets.phtml', array(
                                    'form' => \Sl_Form_Factory::build($model, true, false, false, array('modulerelation_dealertariffsetsdealer')),
                                    'dealer' => $dealer,
                                    'alias' => \Sl\Service\Helper::getModelAlias($model),
                                    'tarifset' => $model,
                                    'save_url' => \Sl\Service\Helper::buildModelUrl($model, 'edit', array(
                                        'id' => $model->getId(),
                                    )),
                                )));
                    } catch(\Exception $e) {
                        echo $e->getMessage();
                        die;
                    } 
                    $event->getView()->informer_items[] = $container;
                }
            }
        } catch(\Exception $e) {
            echo $e->getMessage();
            die;
        }
    }
    
    public function onHeadLink(\Sl_Event_View $event) {
        $event->getView()->headLink()->appendStylesheet('/logistic/dealertariffsets/informer.css'); 
    }
    
    public function onHeadScript(\Sl_Event_View $event) {
        $event->getView()->headScript()->appendFile('/logistic/dealertariffsets/informer.js');
    }
    
    public function getActive() {
        return (int) $this->_active;
    }
    
    public function onAfterInit(\Sl_Event_Router $event) {}
    public function onBeforeInit(\Sl_Event_Router $event) {}
    public function onDispatchLoopShutdown(\Sl_Event_Router $event) {}
    public function onDispatchLoopStartup(\Sl_Event_Router $event) {}
    public function onGetRequest(\Sl_Event_Router $event) {}
    public function onGetResponse(\Sl_Event_Router $event) {}
    public function onPostDispatch(\Sl_Event_Router $event) {}
    public function onPreDispatch(\Sl_Event_Router $event) {}
    public function onRouteStartup(\Sl_Event_Router $event) {}
    public function onSetRequest(\Sl_Event_Router $event) {}
    public function onSetResponse(\Sl_Event_Router $event) {}
    public function onAfterContent(\Sl_Event_View $event) {}
    public function onBeforeContent(\Sl_Event_View $event) {}
    public function onBeforePageHeader(\Sl_Event_View $event) {}
    public function onBodyBegin(\Sl_Event_View $event) {}
    public function onBodyEnd(\Sl_Event_View $event) {}
    public function onContent(\Sl_Event_View $event) {}
    public function onFooter(\Sl_Event_View $event) {}
    public function onHeadTitle(\Sl_Event_View $event) {}
    public function onHeader(\Sl_Event_View $event) {}
    public function onLogo(\Sl_Event_View $event) {}
    public function onNav(\Sl_Event_View $event) {}
    public function onPageOptions(\Sl_Event_View $event) {}

}