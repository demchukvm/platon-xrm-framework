<?php

namespace Sl\Module\Logistic\Listener;

class Transportactionlistview extends \Sl_Listener_Abstract implements \Sl\Listener\Fieldset, \Sl\Listener\Dataset {

    public function onAfterProcessAll(\Sl\Event\Dataset $event) {
        
    }

    public function onAfterProcessItem(\Sl\Event\Dataset $event) {
        if ($event->getModel() instanceof \Sl\Module\Logistic\Model\Transportaction) {
            $item = $event->getItem();
            // print_r($item); die;


            if (isset($item['tractionstock']['id']) && $item['tractionstock']['id'] < 1) {
                $item['stock_name'] .= '<span class="htmlify icon-exclamation-sign pull-right" title="Чужой склад">&nbsp;</span>';
            }
            if (isset($item['identifier'])) {
                if (isset($item['tractionbox']['id']) && $item['tractionbox']['id']) {
                    $item['identifier'] .= '<span class="htmlify icon-stop pull-right" title="Ящик">&nbsp;</span>';
                } elseif (isset($item['tractionpackage']['id']) && $item['tractionpackage']['id']) {
                    $item['identifier'] .= '<span class="htmlify icon-th-large pull-right" title="Посылка">&nbsp;</span>';
                }
            }
            $event->setItem($item);
        }
    }

    public function onBeforeProcessAll(\Sl\Event\Dataset $event) {
        
    }

    public function onBeforeProcessItem(\Sl\Event\Dataset $event) {
        
    }

    public function onPrepare(\Sl\Event\Fieldset $event) {
        if ($event->getModel() instanceof \Sl\Module\Logistic\Model\Transportaction) {

            $addition_fields = array(
                'tractionstock.id' => array('label' => '',
                    'roles' => array(
                        'from',)),
                'tractionbox.id' => array('label' => '',
                    'roles' => array(
                        'from',)),
                'tractionpackage.id' => array('label' => '',
                    'roles' => array(
                        'from',)),
            );

            foreach ($addition_fields as $field => $options) {
                if (!$event->getFieldset()->hasField($field)) {
                    $cur_field = $event->getFieldset()->createField($field);
                } else {
                    $cur_field = $event->getFieldset()->getField($field);
                }
                $cur_field->fill($options);
            }
        }
    }

    public function onPrepareAjax(\Sl\Event\Fieldset $event) {
        
    }

    /*
     * To change this template, choose Tools | Templates
     * and open the template in the editor.
     */
}