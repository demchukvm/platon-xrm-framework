<?php
return array (
  'name' => 
  array (
    'label' => 'ФИО',
    'class' => 'span1',
    'sortable' => true,
    'searchable' => true,
  ),
  'email' => 
  array (
    'label' => 'Email',
    'class' => 'span1',
    'sortable' => true,
    'searchable' => true,
  ),
  'destination_country' => 
  array (
    'label' => 'Куда',
    'class' => 'span1',
    'sortable' => true,
    'searchable' => true,
  ),
  'destination_city' => 
  array (
    'label' => 'Город',
    'class' => 'span1',
    'sortable' => true,
    'searchable' => true,
  ),
  'country' => 
  array (
    'label' => 'Откуда',
    'class' => 'span1',
    'sortable' => true,
    'searchable' => true,
  ),
  'delivery_type' => 
  array (
    'label' => 'Тип доставки',
    'class' => 'span1',
    'sortable' => true,
    'searchable' => true,
  ),
  'weight' => 
  array (
    'label' => 'Вес',
    'class' => 'span1',
    'sortable' => true,
    'searchable' => true,
  ),
  'volume' => 
  array (
    'label' => 'Объем',
    'class' => 'span1',
    'sortable' => true,
    'searchable' => true,
  ),
  'category' => 
  array (
    'label' => 'Категория',
    'class' => 'span1',
    'sortable' => true,
    'searchable' => true,
  ),
);
