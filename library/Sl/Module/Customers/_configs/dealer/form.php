<?php
return array (
  'modulerelation_customerdealer' => 
  array (
    'label' => 'Привлек',
  ),
  'modulerelation_customerisdealer' => 
  array (
    'label' => 'Это клиент',
  ),
  'modulerelation_dealeremails' => 
  array (
    'label' => 'Email',
    'sort_order' => 420,
  ),
  'modulerelation_dealerphones' => 
  array (
    'label' => 'Контактный тел.',
    'sort_order' => 510,
  ),
);
