<?php
return array (
  'name' => 
  array (
    'label' => 'Имя',
    'searchable' => true,
    'sortable' => true,
  ),
  'post' => 
  array (
    'label' => 'Должность',
    'sortable' => true,
  ),
  'description' => 
  array (
    'label' => 'Описание',
  ),
  'contactemail.mail' => 
  array (
    'label' => 'Email',
    'searchable' => true,
  ),
  'contactphone.phone' => 
  array (
    'label' => 'Телефон',
  ),
  'archived' => 
  array (
    'label' => 'ARCHIVED',
  ),
);
