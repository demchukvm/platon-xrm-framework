<?php
return array (
  'name' => 
  array (
    'label' => 'ФИО',
    'searchable' => true,
    'sortable' => true,
  ),
  'customercity.name' => 
  array (
    'label' => 'Город',
    'searchable' => true,
    'sortable' => true,
    'class' => 'span1-5',
  ),
  'status' => 
  array (
    'label' => 'Статус',
    'searchable' => true,
    'sortable' => true,
  ),
  'customerphones.phone' => 
  array (
    'label' => 'Телефон получателя',
    'searchable' => true,
    'sortable' => true,
    'class' => 'span1-5',
  ),
  'sender_phone' => 
  array (
    'label' => 'Телефон отправителя',
    'searchable' => true,
    'sortable' => true,
    'class' => 'span1-5',
  ),
  'customerballance.ballance' => 
  array (
    'label' => 'Баланс',
    'searchable' => true,
    'sortable' => true,
    'class' => 'span1',
  ),
  'create' => 
  array (
    'label' => 'Создан',
    'sortable' => true,
  ),
  'is_dealer' => 
  array (
   'label' => 'Это дилер',
   'type'  => 'checkbox',
   'sortable' => true,
  ),
  'address' => 
  array (
    'label' => 'Адрес',
  ),
  'passport' => 
  array (
    'label' => 'Паспорт',
  ),
  'passport_date' => 
  array (
    'label' => 'Дата выдачи',
  ),
  'customercustsource.name' => 
  array (
    'label' => 'Канал привлечения',
    'searchable' => true,
    'sortable' => true,
  ),
  'notify_email' => 
  array (
    'label' => 'email',
   
  ),
  'notify_sms' => 
  array (
    'label' => 'sms',
    'type' => 'checkbox',
  ),
  'post_code' => 
  array (
    'label' => 'Индекс',
  ),
  'first_name' => 
  array (
    'label' => 'Имя',
  ),
  'last_name' => 
  array (
    'label' => 'Фамилия',
  ),
  'middle_name' => 
  array (
    'label' => 'Отчество',
  ),
  'notify_dealer' => 
  array (
    'label' => 'Извещать дилера',
  ),
  'description' => 
  array (
    'label' => 'Комментарий',
  ),
  'company_name' => 
  array (
    'label' => 'Компания',
    'searchable' => true,
    'class' => 'span1-5',
  ),
  'customeremails.mail' => 
  array (
    'label' => 'E-mail',
    'searchable' => true,
    'class' => 'span1-5',
  ),
  
);
