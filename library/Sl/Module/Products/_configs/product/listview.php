<?php
return array (
  'name' => 
  array (
   
    'label' => 'Название',
    'searchable' => true,
    'sortable' => true,
  ),
  'code' => 
  array (

    'label' => 'Артикул',
    'sortable' => true,
  ),
  'brief_description' => 
  array (

    'label' => 'Краткое описание',
  ),
  'productproductsettings.name' => 
  array (

    'label' => 'Свойства',
  ),
  'productfile.name' => 
  array (
    'label' => 'Изображения',
   
  ),
  'enabled' => 
  array (
    'label' => 'Видимость',
  ),
  'description' => 
  array (
    'label' => 'Описание',
   
  ),
);
