<?php
return array (
  'name' => 
  array (
    'label' => 'Название',
    'type' => 'text',
    'searchable' => true,
    'sortable' => true,
  ),
  'code' => 
  array (
    'label' => 'Артикул',
    'type' => 'text',
    'options' => 
    array (
      'disabled' => 'disabled',
    ),
  ),
  'enabled' => 
  array (
    'label' => 'Видимость',
    'type' => 'select',
  ),
  'description' => 
  array (
    'label' => 'Описание',
    'type' => 'textarea',
  ),
  'brief_description' => 
  array (
    'label' => 'Краткое описание',
    'type' => 'textarea',
  ),
  'id' => 
  array (
    'label' => 'ID',
    'type' => 'hidden',
  ),
  'active' => 
  array (
    'label' => 'ACTIVE',
    'type' => 'checkbox',
  ),
  'create' => 
  array (
    'label' => 'CREATE',
    'type' => 'text',
  ),
);
