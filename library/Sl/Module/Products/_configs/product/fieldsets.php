<?php

return array(
    '_default' =>
    array(
        'fields' =>
        array(
            0 => 'code',
            1 => 'name',
            2 => 'brief_description',
            3 => 'productproductsettings.name',
            4 => 'productfile.name',
        ),
        'name' => '_default',
        'label' => 'По-умолчанию',
    ),
    '_popup' =>
    array(
        'fields' =>
        array(
            'name',
        ),
        'name' => '_popup',
        'label' => 'По-умолчанию',
        'type' => 'popup',
    ),
);
