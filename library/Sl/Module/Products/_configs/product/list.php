<?php
return array (
  'name' => 
  array (
    'order' => 20,
    'label' => 'Название',
    'searchable' => 1,
    'sortable' => true,
  ),
  'code' => 
  array (
    'order' => 10,
    'label' => 'Артикул',
    'sortable' => true,
  ),
  'brief_description' => 
  array (
    'order' => 50,
    'label' => 'Краткое описание',
  ),
  'productproductsettings.name' => 
  array (
    'order' => 60,
    'label' => 'Свойства',
  ),
  'productfile.name' => 
  array (
    'label' => 'Изображения',
    'order' => 65,
  ),
  'productfile' => 
  array (
    'label' => 'Изображения',
  ),
  'enabled' => 
  array (
    'label' => 'Видимость',
    'order' => 75,
    'visible' => false,
    'hidable' => true,
  ),
  'description' => 
  array (
    'label' => 'Описание',
    'order' => 85,
    'visible' => false,
    'hidable' => true,
  ),
);
