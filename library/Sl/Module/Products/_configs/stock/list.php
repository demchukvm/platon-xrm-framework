<?php
return array (
  'name' => 
  array (
    'order' => 10,
    'label' => 'Название',
    'sortable' => true,
    'searchable' => true,
  ),
  'code' => 
  array (
    'order' => 20,
    'label' => 'Код',
  ),
  'description' => 
  array (
    'order' => 30,
    'label' => 'Описание',
  ),
  'address' => 
  array (
    'label' => 'Адрес',
    'order' => 40,
    'visible' => false,
    'hidable' => true,
  ),
);
