<?php
return array (
  'name' => 
  array (
    'label' => 'Название',
    'sortable' => true,
    'searchable' => true,
  ),
  'code' => 
  array (
    'label' => 'Код',
  ),
  'description' => 
  array (
    'label' => 'Описание',
  ),
  'address' => 
  array (
    'label' => 'Адрес',
  ),
);
