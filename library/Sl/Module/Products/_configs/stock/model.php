<?php
return array (
  'name' => 
  array (
    'label' => 'Название',
    'type' => 'text',
    'searchable' => true,
    'sortable' => true,
  ),
  'code' => 
  array (
    'label' => 'Код',
    'type' => 'text',
  ),
  'description' => 
  array (
    'label' => 'Описание',
    'type' => 'text',
  ),
  'address' => 
  array (
    'label' => 'Адрес',
    'type' => 'textarea',
  ),
  'id' => 
  array (
    'label' => 'ID',
    'type' => 'hidden',
  ),
  'active' => 
  array (
    'label' => 'ACTIVE',
    'type' => 'checkbox',
  ),
  'create' => 
  array (
    'label' => 'CREATE',
    'type' => 'text',
  ),
);
