<?php
return array (
  'name' => 
  array (
    'label' => 'Название',
    'searchable' => true,
  ),
  'code' => 
  array (
    'label' => 'Код',
  ),
  'description' => 
  array (
    'label' => 'Описание',
  ),
  'weight_surcharge' => 
  array (
    'label' => 'Вес. наценка',
  ),
  'count_surcharge' => 
  array (
    'label' => 'Кол. наценка',
  ),
);
