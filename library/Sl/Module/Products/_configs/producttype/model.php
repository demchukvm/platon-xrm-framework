<?php
return array (
  'name' => 
  array (
    'label' => 'Название',
    'type' => 'text',
  ),
  'code' => 
  array (
    'label' => 'Код',
    'type' => 'text',
  ),
  'description' => 
  array (
    'label' => 'Описание',
    'type' => 'text',
  ),
  'weight_surcharge' => 
  array (
    'label' => 'Вес. наценка',
    'type' => 'text',
    'class' => 'float',
  ),
  'count_surcharge' => 
  array (
    'label' => 'Кол-в. наценка',
    'type' => 'text',
    'class' => 'float',
  ),
  'id' => 
  array (
    'label' => 'ID',
    'type' => 'text',
  ),
  'active' => 
  array (
    'label' => 'ACTIVE',
    'type' => 'checkbox',
  ),
  'create' => 
  array (
    'label' => 'CREATE',
    'type' => 'text',
  ),
);
