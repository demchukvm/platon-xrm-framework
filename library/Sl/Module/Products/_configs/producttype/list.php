<?php
return array (
  'name' => 
  array (
    'order' => 10,
    'label' => 'Название',
    'searchable' => true,
  ),
  'code' => 
  array (
    'order' => 20,
    'label' => 'Код',
  ),
  'description' => 
  array (
    'order' => 30,
    'label' => 'Описание',
  ),
  'weight_surcharge' => 
  array (
    'order' => 40,
    'label' => 'Вес. наценка',
  ),
  'count_surcharge' => 
  array (
    'order' => 50,
    'label' => 'Кол. наценка',
  ),
);
