<?php
return array (
  'value' => 
  array (
    'label' => 'Значение',
    'type' => 'text',
  ),
  'has_value' => 
  array (
    'label' => 'Установить значение',
    'type' => 'checkbox',
    'class' => 'openValueInput',
  ),
  'create' => 
  array (
    'label' => 'CREATE',
    'type' => 'text',
  ),
  'id' => 
  array (
    'label' => 'ID',
    'type' => 'hidden',
  ),
  'active' => 
  array (
    'label' => 'ACTIVE',
    'type' => 'checkbox',
  ),
  'name' => 
  array (
    'label' => 'NAME',
    'type' => 'hidden',
  ),
);
