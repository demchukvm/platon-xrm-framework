<?php
return array (
  'modulerelation_productssettingsprodsettings' => 
  array (
    'label' => 'Свойство',
    'sort_order' => 100,
    'field_filters' => 
    array (
      0 => 
      array (
        'field' => 'id',
        'matching' => 'nin',
        'value' => 'modulerelation_productproductsettings-modulerelation_productssettingsprodsettings',
      ),
    ),
  ),
  'has_value' => 
  array (
    'sort_order' => 150,
  ),
  'value' => 
  array (
    'sort_order' => 1010,
  ),
);
