<?php
return array (
  'name' => 
  array (
    'label' => 'NAME',
    'type' => 'text',
  ),
  'count' => 
  array (
    'label' => 'COUNT',
    'type' => 'text',
  ),
  'price' => 
  array (
    'label' => 'PRICE',
    'type' => 'text',
  ),
  'sum' => 
  array (
    'label' => 'SUM',
    'type' => 'text',
  ),
  'description' => 
  array (
    'label' => 'DESCRIPTION',
    'type' => 'text',
  ),
  'create' => 
  array (
    'label' => 'CREATE',
    'type' => 'text',
  ),
  'id' => 
  array (
    'label' => 'ID',
    'type' => 'text',
  ),
  'active' => 
  array (
    'label' => 'ACTIVE',
    'type' => 'checkbox',
  ),
);
