<?php
return array (
  'amount' => 
  array (
    'label' => 'На склaде',
    'type' => 'text',
  ),
  'enabled' => 
  array (
    'label' => 'Открыт',
    'type' => 'checkbox',
  ),
  'id' => 
  array (
    'label' => 'ID',
    'type' => 'hidden',
  ),
  'active' => 
  array (
    'label' => 'ACTIVE',
    'type' => 'checkbox',
  ),
  'price' => 
  array (
    'label' => 'Цена',
    'type' => 'text',
    'class' => 'floatPositive',
  ),
);
