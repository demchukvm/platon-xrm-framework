<?php
return array (
  'stockproductstocksettings.name' => 
  array (
    'label' => 'Склад',
    'order' => 20,
    'searchable' => true,
  ),
  'productproductstocksettings.name' => 
  array (
    'label' => 'Продукт',
    'order' => 10,
    'searchable' => true,
  ),
  'amount' => 
  array (
    'label' => 'На складе',
    'order' => 50,
  ),
  'price' => 
  array (
    'label' => 'Цена',
    'order' => 30,
    'sortable' => true,
  ),
  'enabled' => 
  array (
    'visible' => false,
  ),
);
