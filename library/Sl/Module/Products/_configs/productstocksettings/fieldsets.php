<?php

return array(
    '_default' =>
    array(
        'fields' =>
        array(
            'productproductstocksettings.name',
            'stockproductstocksettings.name',
            'price',
            'amount',
        ),
        'name' => '_default',
        'label' => 'По-умолчанию',
    ),
    '_popup' =>
    array(
        'fields' =>
        array(
            'name',
        ),
        'name' => '_popup',
        'label' => 'По-умолчанию',
        'type' => 'popup',
    ),
);
