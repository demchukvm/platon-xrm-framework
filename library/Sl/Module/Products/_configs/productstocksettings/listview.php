<?php
return array (
  'stockproductstocksettings.name' => 
  array (
    'label' => 'Склад',
    'searchable' => true,
  ),
  'productproductstocksettings.name' => 
  array (
    'label' => 'Продукт',
    'searchable' => true,
  ),
  'amount' => 
  array (
    'label' => 'На складе',
  ),
  'price' => 
  array (
    'label' => 'Цена',
    'sortable' => true,
  ),
);
