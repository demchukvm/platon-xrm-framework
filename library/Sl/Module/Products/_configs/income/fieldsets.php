<?php

return array(
    '_default' =>
    array(
        'fields' =>
        array(
            0 => 'description',
            1 => 'type',
        ),
        'name' => '_default',
        'label' => 'По-умолчанию',
    ),
    '_popup' =>
    array(
        'fields' =>
        array(
            'description',
        ),
        'name' => 'popup',
        'label' => 'По-умолчанию',
        'type' => 'popup',
    ),
);
