<?php
return array (
  'description' => 
  array (
    'label' => 'DESCRIPTION',
    'type' => 'text',
  ),
  'type' => 
  array (
    'label' => 'TYPE',
    'type' => 'text',
  ),
  'create' => 
  array (
    'label' => 'CREATE',
    'type' => 'text',
  ),
  'id' => 
  array (
    'label' => 'ID',
    'type' => 'text',
  ),
  'active' => 
  array (
    'label' => 'ACTIVE',
    'type' => 'checkbox',
  ),
);
