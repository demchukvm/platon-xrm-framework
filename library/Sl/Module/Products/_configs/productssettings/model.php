<?php
return array (
  'name' => 
  array (
    'label' => 'Название',
    'type' => 'text',
    'sortable' => true,
    'searchable' => true,
  ),
  'create' => 
  array (
    'label' => 'CREATE',
    'type' => 'text',
  ),
  'id' => 
  array (
    'label' => 'ID',
    'type' => 'hidden',
  ),
  'active' => 
  array (
    'label' => 'ACTIVE',
    'type' => 'checkbox',
  ),
  'description' => 
  array (
    'label' => 'Описание',
    'type' => 'textarea',
  ),
);
