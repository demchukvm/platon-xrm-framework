<?php
return array (
  'name' => 
  array (
    'order' => 5,
    'label' => 'Название',
  ),
  'stockproductstocksettings.name' => 
  array (
    'label' => 'Склад',
    'order' => 20,
    'searchable' => true,
  ),
  'productproductstocksettings.name' => 
  array (
    'label' => 'Продукт',
    'order' => 10,
    'searchable' => true,
  ),
  'amount' => 
  array (
    'order' => 50,
  ),
  'price' => 
  array (
    'order' => 30,
  ),
  'enabled' => 
  array (
    'visible' => false,
    'hidable' => true,
  ),
  'description' => 
  array (
    'label' => 'Описание',
    'order' => 60,
    'visible' => false,
    'hidable' => true,
  ),
);
