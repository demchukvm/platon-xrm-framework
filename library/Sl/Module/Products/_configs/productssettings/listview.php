<?php
return array (
  'name' => 
  array (
    'label' => 'Название',
  ),
  'stockproductstocksettings.name' => 
  array (
    'label' => 'Склад',
    'searchable' => true,
  ),
  'productproductstocksettings.name' => 
  array (
    'label' => 'Продукт',
    'searchable' => true,
  ),
  'amount' => 
  array (
  ),
  'price' => 
  array (
  ),
  'enabled' => 
  array (
  ),
  'description' => 
  array (
    'label' => 'Описание',
  ),
);
