<?php
return array (
  'name' => 
  array (
    'order' => 10,
    'label' => 'Заказ',
    'sortable' => true,
    'searchable' => true,
    'hidable' => true,
    'class' => 'span2',
  ),
  'status' => 
  array (
    'order' => 20,
    'label' => 'Статус',
    'searchable' => true,
    'select' => true,
    'sortable' => true,
  ),
  'fin_status' => 
  array (
    'order' => 30,
    'label' => 'Статус оплаты',
    'searchable' => true,
    'select' => true,
    'sortable' => true,
  ),
  'discount' => 
  array (
    'order' => 40,
    'label' => 'Общая скидка, %',
    'searchable' => true,
    'sortable' => true,
    'hidable' => true,
    'class' => 'span1-5',
  ),
  'description' => 
  array (
    'order' => 60,
    'label' => 'Комментарий к заказу',
    'searchable' => true,
    'hidable' => true,
    'class' => 'span2',
  ),
  'total_sum' => 
  array (
    'order' => 80,
    'label' => 'Всего к оплате',
    'searchable' => true,
    'sortable' => true,
    'hidable' => true,
    'class' => 'span1-5',
  ),
  'orderorderitems' => 
  array (
    'label' => 'Товары',
  ),
  'orderorderitems.name' => 
  array (
    'order' => 83,
    'label' => 'Товары',
  ),
  'orderstock' => 
  array (
    'label' => 'Склад',
  ),
  'orderstock.name' => 
  array (
    'order' => 85,
    'label' => 'Склад',
    'searchable' => true,
    'sortable' => true,
    'hidable' => true,
    'class' => 'span1-5',
  ),
  'create' => 
  array (
    'order' => 95,
    'label' => 'Создан',
    'sortable' => true,
  ),
  'discount_description' => 
  array (
    'label' => 'Описание скидки',
    'order' => 105,
    'visible' => false,
    'hidable' => true,
  ),
  'items_sum' => 
  array (
    'label' => 'Всего за товары',
    'order' => 115,
    'visible' => false,
    'hidable' => true,
  ),
);
