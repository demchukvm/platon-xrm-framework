<?php
return array (
  'name' => 
  array (
    'label' => 'Заказ',
    'type' => 'hidden',
  ),
  'status' => 
  array (
    'label' => 'Статус',
    'type' => 'text',
  ),
  'fin_status' => 
  array (
    'label' => 'Статус оплаты',
    'type' => 'text',
    'disabled' => true,
  ),
  'discount' => 
  array (
    'label' => 'Общая скидка, %',
    'type' => 'text',
    'class' => 'floatPositive',
  ),
  'discount_description' => 
  array (
    'label' => 'Описание скидки',
    'type' => 'textarea',
  ),
  'description' => 
  array (
    'label' => 'Комментарий к заказу',
    'type' => 'textarea',
  ),
  'items_sum' => 
  array (
    'label' => 'Всего за товары',
    'type' => 'text',
    'readonly' => true,
    'class' => 'floatPositive',
  ),
  'total_sum' => 
  array (
    'label' => 'Всего к оплате',
    'type' => 'text',
    'class' => 'floatPositive',
    'readonly' => true,
  ),
  'create' => 
  array (
    'label' => 'Дата создания',
    'type' => 'text',
  ),
  'id' => 
  array (
    'label' => 'ID',
    'type' => 'hidden',
  ),
  'active' => 
  array (
    'label' => 'ACTIVE',
    'type' => 'checkbox',
  ),
);
