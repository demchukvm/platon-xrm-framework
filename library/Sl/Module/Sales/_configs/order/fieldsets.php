<?php

return array(
    '_default' =>
    array(
        'fields' =>
        array(
            0 => 'name',
            1 => 'status',
            2 => 'fin_status',
            3 => 'discount',
            4 => 'discount_description',
            5 => 'description',
            6 => 'items_sum',
            7 => 'total_sum',
            8 => 'orderorderitems.name',
            9 => 'orderstock.name',
            10 => 'create',
        ),
        'name' => '_default',
        'label' => 'По-умолчанию',
    ),
    '_popup' =>
    array(
        'fields' =>
        array(
            'name',
        ),
        'name' => 'popup',
        'label' => 'По-умолчанию',
        'type' => 'popup',
    ),
);
