<?php
return array (
  'modulerelation_ordercustomer' => 
  array (
    'label' => 'Покупатель',
    'sort_order' => 1000,
  ),
  'modulerelation_packageorder' => 
  array (
    'label' => 'Отправлен посылкой',
    'sort_order' => 1100,
  ),
  'modulerelation_orderstock' => 
  array (
    'label' => 'Склад',
    'sort_order' => 2000,
  ),
  'modulerelation_orderbill' => 
  array (
    'label' => 'Выставленные счета',
  ),
  'modulerelation_orderfile' => 
  array (
    'label' => 'Файлы',
    'sort_order' => 10600,
  ),
  'modulerelation_orderorderitems' => 
  array (
    'label' => 'Продукты',
    'sort_order' => 5000,
  ),
  'discount' => 
  array (
    'sort_order' => 10000,
  ),
  'total_sum' => 
  array (
    'sort_order' => 10500,
  ),
  'description' => 
  array (
    'sort_order' => 1200,
  ),
  'items_sum' => 
  array (
    'sort_order' => 5200,
  ),
  'status' => 
  array (
    'sort_order' => 10,
  ),
  'create' => 
  array (
    'sort_order' => 20,
  ),
  'fin_status' => 
  array (
    'sort_order' => 50,
  ),
);
