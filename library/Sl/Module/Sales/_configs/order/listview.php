<?php
return array (
  'name' => 
  array (
    'label' => 'Заказ',
    'sortable' => true,
    'searchable' => true,
    'class' => 'span2',
  ),
  'status' => 
  array (
    'label' => 'Статус',
    'searchable' => true,
    'sortable' => true,
  ),
  'fin_status' => 
  array (
    'label' => 'Статус оплаты',
    'searchable' => true,
    'sortable' => true,
  ),
  'discount' => 
  array (
    'label' => 'Общая скидка, %',
    'searchable' => true,
    'sortable' => true,
    'class' => 'span1-5',
  ),
  'description' => 
  array (
    'label' => 'Комментарий к заказу',
    'searchable' => true,
    'class' => 'span2',
  ),
  'total_sum' => 
  array (
    'label' => 'Всего к оплате',
    'searchable' => true,
    'sortable' => true,
    'class' => 'span1-5',
  ),
  'orderorderitems.name' => 
  array (
    'label' => 'Товары',
  ),
  
  'orderstock.name' => 
  array (
    'label' => 'Склад',
    'searchable' => true,
    'sortable' => true,
    'class' => 'span1-5',
  ),
  'create' => 
  array (
    'label' => 'Создан',
    'sortable' => true,
  ),
  'discount_description' => 
  array (
    'label' => 'Описание скидки',
  ),
  'items_sum' => 
  array (
    'label' => 'Всего за товары',
  ),
);
