<?php
return array (
  'modulerelation_orderitemsproduct' => 
  array (
    'label' => 'Продукт',
    'sort_order' => 900,
  ),
  'serial' => 
  array (
    'sort_order' => 1100,
  ),
  'price' => 
  array (
    'sort_order' => 1100,
  ),
  'count' => 
  array (
    'sort_order' => 1200,
  ),
  'discount' => 
  array (
    'sort_order' => 1250,
  ),
  'cost' => 
  array (
    'sort_order' => 1300,
  ),
);
