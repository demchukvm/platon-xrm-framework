<?php
return array (
  'name' => 
  array (
    'label' => 'Название позиции',
    'type' => 'hidden',
  ),
  'serial' => 
  array (
    'label' => 'SN/IMEI',
    'type' => 'text',
  ),
  'price' => 
  array (
    'label' => 'Цена, за шт',
    'type' => 'text',
    'readonly' => true,
    'class' => 'floatPositive',
  ),
  'discount' => 
  array (
    'label' => 'Скидка, %',
    'type' => 'text',
    'class' => 'floatPositive',
  ),
  'count' => 
  array (
    'label' => 'Кол-во',
    'type' => 'text',
    'class' => 'intPositive',
  ),
  'cost' => 
  array (
    'label' => 'Сумма',
    'type' => 'text',
    'class' => 'floatPositive',
    'readonly' => true,
  ),
  'create' => 
  array (
    'label' => 'CREATE',
    'type' => 'text',
  ),
  'id' => 
  array (
    'label' => 'ID',
    'type' => 'hidden',
  ),
  'active' => 
  array (
    'label' => 'ACTIVE',
    'type' => 'checkbox',
  ),
);
