<?php
return array (
  'name' => 
  array (
    'label' => 'Название позиции',
  ),
  'serial' => 
  array (
    'label' => 'SN/IMEI',
  ),
  'price' => 
  array (
    'label' => 'Цена, за шт',
  ),
  'discount' => 
  array (
    'label' => 'Скидка, %',
  ),
  'count' => 
  array (
    'label' => 'Кол-во',
  ),
  'cost' => 
  array (
    'label' => 'Сумма',
  ),
);
