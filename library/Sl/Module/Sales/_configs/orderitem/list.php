<?php
return array (
  'name' => 
  array (
    'order' => 10,
    'label' => 'Название позиции',
  ),
  'serial' => 
  array (
    'order' => 20,
    'label' => 'SN/IMEI',
  ),
  'price' => 
  array (
    'order' => 30,
    'label' => 'Цена, за шт',
  ),
  'discount' => 
  array (
    'order' => 40,
    'label' => 'Скидка, %',
  ),
  'count' => 
  array (
    'order' => 50,
    'label' => 'Кол-во',
  ),
  'cost' => 
  array (
    'order' => 60,
    'label' => 'Сумма',
  ),
);
