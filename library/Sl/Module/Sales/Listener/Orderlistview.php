<?php

namespace Sl\Module\Sales\Listener;

class Orderlistview extends \Sl_Listener_Abstract implements \Sl\Listener\Fieldset, \Sl\Listener\Dataset {

    public function onPrepare(\Sl\Event\Fieldset $event) {

        if ($event->getModel() instanceof \Sl\Module\Sales\Model\Order) {


            $addition_fields = array(
                'ordercustomer.customerdealer.id' => array(
                    'roles' => array(
                        'from',)),
                'ordercustomer.customerisdealer.id' => array(
                    'roles' => array(
                        'from',)),
            );
            //TO DO: нижний метод вынести куда-то в абстракт
            foreach ($addition_fields as $field => $options) {
                if (!$event->getFieldset()->hasField($field)) {
                    $cur_field = $event->getFieldset()->createField($field);
                } else {
                    $cur_field = $event->getFieldset()->getField($field);
                }
                $cur_field->fill($options);
            }
        }
    }

    public function onAfterProcessItem(\Sl\Event\Dataset $event) {
        if ($event->getModel() instanceof \Sl\Module\Sales\Model\Order) {
            $item = $event->getItem();

            if (isset($item['name'])) {
                if (isset($item['ordercustomer']['customerisdealer']['id']) && $item['ordercustomer']['customerisdealer']['id'] > 0) {
                    $item['name'] .= '<span class="htmlify icon-star-empty pull-right" title="' . $this->getTranslator()->translate('Дилер') . '" >&nbsp;</span>';
                }
                if (isset($item['ordercustomer']['customerdealer']['id']) && $item['ordercustomer']['customerdealer']['id'] > 0) {
                    $item['name'] .= '<span class="htmlify icon-asterisk pull-right" title="' . $this->getTranslator()->translate('Привлечен дилером') . '">&nbsp;</span>';
                }
            }

            $event->setItem($item);
        }
    }

    public function onPrepareAjax(\Sl\Event\Fieldset $event) {
        
    }

    public function onAfterProcessAll(\Sl\Event\Dataset $event) {
        
    }

    public function onBeforeProcessAll(\Sl\Event\Dataset $event) {
        
    }

    public function onBeforeProcessItem(\Sl\Event\Dataset $event) {
        
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
