<?php

return array(
    '_default' =>
    array(
        'fields' =>
        array(
            0 => 'date',
            1 => 'summ',
            2 => 'paymentcashdesc.name',
            3 => 'paymentcurrency.name',
            4 => 'course',
            5 => 'summ_approve',
            6 => 'status',
            7 => 'billpayment.name',
            8 => 'description',
        ),
        'name' => '_default',
        'label' => 'По-умолчанию',
    ),
    '_popup' =>
    array(
        'fields' =>
        array(
            'description',
        ),
        'name' => 'popup',
        'label' => 'По-умолчанию',
        'type' => 'popup',
    ),
);
