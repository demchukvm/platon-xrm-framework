<?php
return array (
  'modulerelation_finacntpayment' => 
  array (
    'label' => 'Статья расходов',
    'request_fields' => 
    array (
      0 => 'code',
      1 => 'name',
    ),
  ),
  'status' => 
  array (
    'sort_order' => 1000,
  ),
  'summ' => 
  array (
    'sort_order' => 160,
  ),
  'summ_approve' => 
  array (
    'sort_order' => 190,
  ),
  'course' => 
  array (
    'sort_order' => 165,
  ),
  'description' => 
  array (
    'sort_order' => 200,
  ),
  'date' => 
  array (
    'sort_order' => 150,
  ),
  'create' => 
  array (
    'label' => 'Дата создания',
    'sort_order' => 5,
    'type' => 'date',
    'class' => 'current-date',
  ),
  'modulerelation_paymentcashdesc' => 
  array (
    'label' => 'Касса',
    'sort_order' => 10,
    'required' => true,
    'request_fields' => 
    array (
      0 => 'name',
      1 => 'current_ballance',
    ),
  ),
  'modulerelation_paymentcustomer' => 
  array (
    'label' => 'Контрагент',
    'sort_order' => 30,
  ),
  'modulerelation_billpayment' => 
  array (
    'label' => 'Выставленный счет',
    'sort_order' => 50,
    'field_filters' => 
    array (
      0 => 
      array (
        'field' => 'billcustomer:id',
        'matching' => 'in',
        'value' => 'modulerelation_paymentcustomer',
      ),
    ),
    'request_fields' => 
    array (
      0 => 'name',
      1 => 'sum',
    ),
  ),
  'modulerelation_paymentcurrency' => 
  array (
    'label' => 'Валюта',
    'sort_order' => 170,
    'readonly' => true,
  ),
  '_roles_' => 
  array (
    29 => 
    array (
      'date' => 
      array (
        'type' => 'date',
      ),
    ),
  ),
);
