<?php
return array (
  'id' => 
  array (
    'order' => 10,
    'label' => 'Номер',
    'visible' => false,
    'hidable' => false,
  ),
  'name' => 
  array (
    'order' => 200,
    'label' => 'Номер платежа',
    'class' => 'span1',
    'visible' => false,
    'hidable' => true,
    'sortable' => true,
    'searchable' => true,
  ),
  'status' => 
  array (
    'order' => 60,
    'label' => 'Статус',
    'searchable' => true,
    'select' => true,
    'sortable' => true,
  ),
  'summ' => 
  array (
    'order' => 30,
    'label' => 'Оплачено',
    'class' => 'span1',
    'sortable' => true,
    'searchable' => true,
  ),
  'paymentcashdesc' => 
  array (
    'label' => 'Касса',
  ),
  'paymentcashdesc.name' => 
  array (
    'label' => 'Касса',
    'order' => 31,
    'searchable' => true,
    'sortable' => true,
    'class' => 'span1-5',
  ),
  'paymentcurrency' => 
  array (
    'label' => 'В валюте',
  ),
  'paymentcurrency.name' => 
  array (
    'order' => 35,
    'label' => 'В валюте',
    'searchable' => true,
    'sortable' => true,
    'class' => 'span1',
  ),
  'course' => 
  array (
    'order' => 40,
    'class' => 'span1',
    'label' => 'Курс',
    'sortable' => true,
    'searchable' => true,
  ),
  'summ_approve' => 
  array (
    'order' => 50,
    'label' => 'В $',
    'class' => 'span1',
    'sortable' => true,
    'searchable' => true,
  ),
  'billpayment.name' => 
  array (
    'label' => 'Счет',
    'order' => 100,
    'searchable' => true,
    'sortable' => true,
  ),
  'billpayment' => 
  array (
    'label' => 'Cчет&nbsp;/&nbsp;Код',
  ),
  'date' => 
  array (
    'order' => 15,
    'label' => 'Дата',
    'searchable' => true,
    'sortable' => true,
    'class' => 'span1-5',
  ),
  'description' => 
  array (
    'order' => 160,
    'label' => 'Комментарий',
    'sortable' => true,
    'searchable' => true,
  ),
  'verificated' => 
  array (
    'order' => 1600,
    'visible' => false,
    'hidable' => false,
  ),
);
