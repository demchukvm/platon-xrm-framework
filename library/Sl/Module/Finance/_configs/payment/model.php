<?php
return array (
  'id' => 
  array (
    'label' => 'Номер',
    'type' => 'text',
    'readonly' => true,
  ),
  'name' => 
  array (
    'label' => 'NAME',
    'type' => 'text',
  ),
  'status' => 
  array (
    'label' => 'Статус',
    'type' => 'text',
    'sort_order' => 100,
  ),
  'summ' => 
  array (
    'label' => 'Оплачено',
    'type' => 'text',
    'class' => 'float',
    'required' => true,
  ),
  'course' => 
  array (
    'label' => 'Курс валюты',
    'type' => 'text',
    'class' => 'floatPositive fixedDigits-6',
    'required' => true,
  ),
  'date' => 
  array (
    'label' => 'Дата',
    'type' => 'text',
    'required' => true,
    'searchable' => true,
    'readonly' => true,
    'single_date_search' => false,
    'class' => 'current-date',
  ),
  'active' => 
  array (
    'label' => 'ACTIVE',
    'type' => 'checkbox',
  ),
  'summ_approve' => 
  array (
    'label' => 'В пересчете, $',
    'type' => 'text',
    'class' => 'float',
  ),
  'description' => 
  array (
    'label' => 'Комментарий',
    'type' => 'textarea',
  ),
  'create' => 
  array (
    'label' => 'CREATE',
    'type' => 'text',
  ),
  'verificated' => 
  array (
    'lists_control' => true,
    'type' => 'hidden',
  ),
);
