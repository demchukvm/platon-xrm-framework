<?php
return array (
  'id' => 
  array (
  
    'label' => 'Номер',
  ),
  'name' => 
  array (
   
    'label' => 'Номер платежа',
    'class' => 'span1',
    'sortable' => true,
    'searchable' => true,
  ),
  'status' => 
  array (
  
    'label' => 'Статус',
    'searchable' => true,
    'sortable' => true,
  ),
  'summ' => 
  array (
   
    'label' => 'Оплачено',
    'class' => 'span1',
    'sortable' => true,
    'searchable' => true,
  ),
 
 
  'paymentcashdesc.name' => 
  array (
    'label' => 'Касса',
    
    'searchable' => true,
    'sortable' => true,
    'class' => 'span1-5',
  ),
 
  'paymentcurrency.name' => 
  array (
 
    'label' => 'В валюте',
    'searchable' => true,
    'sortable' => true,
    'class' => 'span1',
  ),
  'course' => 
  array (
    'class' => 'span1',
    'label' => 'Курс',
    'sortable' => true,
    'searchable' => true,
  ),
  'summ_approve' => 
  array (
    'label' => 'В $',
    'class' => 'span1',
    'sortable' => true,
    'searchable' => true,
  ),
  'billpayment.name' => 
  array (
    'label' => 'Счет',
   
    'searchable' => true,
    'sortable' => true,
  ),
 
  'date' => 
  array (
    'label' => 'Дата',
    'searchable' => true,
    'sortable' => true,
    'class' => 'span1-5',
  ),
  'description' => 
  array (
    
    'label' => 'Комментарий',
    'sortable' => true,
    'searchable' => true,
  ),
  'verificated' => 
  array (
  ),
);
