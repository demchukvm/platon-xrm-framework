<?php

return array(
    '_default' =>
    array(
        'fields' =>
        array(
            0 => 'doc_id',
            1 => 'reason_id',
            2 => 'account_id',
            3 => 'currency_id',
            4 => 'rate',
            5 => 'date',
            6 => 'factor',
            7 => 'qty',
            8 => 'currency_qty',
            9 => 'status',
            10 => 'ballance',
        ),
        'name' => '_default',
        'label' => 'По-умолчанию',
    ),
    '_popup' =>
    array(
        'fields' =>
        array(
            0 => 'doc_id',
        ),
        'name' => '_popup',
        'label' => 'По-умолчанию',
        'type' => 'popup',
    ),
);
