<?php

return array(
    '_default' =>
    array(
        'fields' =>
        array(
            0 => 'doc_id',
            1 => 'reason_id',
            2 => 'account_id',
            3 => 'qty',
            4 => 'factor',
            5 => 'date',
            6 => 'ballance',
            7 => 'status',
        ),
        'name' => '_default',
        'label' => 'По-умолчанию',
    ),
    '_popup' =>
    array(
        'fields' =>
        array(
            0 => 'doc_id',
        ),
        'name' => '_popup',
        'label' => 'По-умолчанию',
        'type' => 'popup',
    ),
);
