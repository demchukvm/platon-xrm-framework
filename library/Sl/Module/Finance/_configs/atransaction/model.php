<?php
return array (
  'doc_id' => 
  array (
    'label' => 'DOC_ID',
    'type' => 'text',
  ),
  'reason_id' => 
  array (
    'label' => 'REASON_ID',
    'type' => 'text',
  ),
  'account_id' => 
  array (
    'label' => 'ACCOUNT_ID',
    'type' => 'text',
  ),
  'qty' => 
  array (
    'label' => 'QTY',
    'type' => 'text',
  ),
  'factor' => 
  array (
    'label' => 'FACTOR',
    'type' => 'text',
  ),
  'date' => 
  array (
    'label' => 'DATE',
    'type' => 'text',
  ),
  'ballance' => 
  array (
    'label' => 'BALLANCE',
    'type' => 'text',
  ),
  'status' => 
  array (
    'label' => 'STATUS',
    'type' => 'text',
  ),
  'create' => 
  array (
    'label' => 'CREATE',
    'type' => 'text',
  ),
  'id' => 
  array (
    'label' => 'ID',
    'type' => 'text',
  ),
  'active' => 
  array (
    'label' => 'ACTIVE',
    'type' => 'checkbox',
  ),
);
