<?php
return array (
  'doc_id' => 
  array (
    'order' => 10,
    'label' => 'DOC_ID',
  ),
  'reason_id' => 
  array (
    'order' => 20,
    'label' => 'REASON_ID',
  ),
  'account_id' => 
  array (
    'order' => 30,
    'label' => 'ACCOUNT_ID',
  ),
  'qty' => 
  array (
    'order' => 40,
    'label' => 'QTY',
  ),
  'factor' => 
  array (
    'order' => 50,
    'label' => 'FACTOR',
  ),
  'date' => 
  array (
    'order' => 60,
    'label' => 'DATE',
  ),
  'ballance' => 
  array (
    'order' => 70,
    'label' => 'BALLANCE',
  ),
  'status' => 
  array (
    'order' => 80,
    'label' => 'STATUS',
  ),
);
