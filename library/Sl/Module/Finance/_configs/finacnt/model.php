<?php
return array (
  'name' => 
  array (
    'label' => 'Название',
    'type' => 'text',
  ),
  'master_relation' => 
  array (
    'label' => 'MASTER_RELATION',
    'type' => 'hidden',
  ),
  'code' => 
  array (
    'label' => 'Код',
    'type' => 'text',
  ),
  'create' => 
  array (
    'label' => 'CREATE',
    'type' => 'text',
  ),
  'id' => 
  array (
    'label' => 'ID',
    'type' => 'hidden',
  ),
  'active' => 
  array (
    'label' => 'ACTIVE',
    'type' => 'hidden',
  ),
);
