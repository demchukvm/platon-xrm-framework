<?php
return array (
  'name' => 
  array (
    'order' => 10,
    'label' => 'Название',
  ),
  'course' => 
  array (
    'order' => 20,
    'label' => 'Курс',
  ),
  'code' => 
  array (
    'order' => 30,
    'label' => 'Код ISO3',
  ),
);
