<?php
return array (
  'status' => 
  array (
    'sort_order' => 1000,
  ),
  'id' => 
  array (
    'sort_order' => 10,
  ),
  'summ' => 
  array (
    'sort_order' => 40,
  ),
  'summ_approve' => 
  array (
    'sort_order' => 80,
  ),
  'course' => 
  array (
    'sort_order' => 50,
  ),
  'description' => 
  array (
    'sort_order' => 180,
  ),
  'date' => 
  array (
    'sort_order' => 150,
    'class' => 'current-date',
  ),
  'modulerelation_paymovecashdesc' => 
  array (
    'label' => 'Списать с:',
    'sort_order' => 20,
    'required' => true,
    'field_filters' => 
    array (
      0 => 
      array (
        'field' => 'id',
        'matching' => 'nin',
        'value' => 'modulerelation_paymovecashdescdestination',
      ),
    ),
    'request_fields' => 
    array (
      0 => 'name',
      1 => 'current_ballance',
    ),
  ),
  'modulerelation_paymovecurrency' => 
  array (
    'label' => 'Валюта',
    'sort_order' => 30,
    'readonly' => true,
  ),
  'modulerelation_paymovecashdescdestination' => 
  array (
    'label' => 'Зачислить на:',
    'sort_order' => 60,
    'required' => true,
    'field_filters' => 
    array (
      0 => 
      array (
        'field' => 'id',
        'matching' => 'nin',
        'value' => 'modulerelation_paymovecashdesc',
      ),
    ),
    'request_fields' => 
    array (
      0 => 'name',
      1 => 'current_ballance',
    ),
  ),
  'modulerelation_paymovecurrencydestination' => 
  array (
    'label' => 'Валюта',
    'sort_order' => 70,
    'readonly' => true,
  ),
);
