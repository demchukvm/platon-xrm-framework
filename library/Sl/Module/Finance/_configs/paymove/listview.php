<?php
return array (
  'name' => 
  array (
    'label' => 'NAME',
  ),
  'status' => 
  array (
    'label' => 'Статус',
    'sortable' => true,
    'searchable' => true,
  ),
  'date' => 
  array (
    'label' => 'Дата',
    'type' => 'date',
    'sortable' => true,
    'searchable' => true,
  ),
  'paymovecashdesc.name' => 
  array (
    'label' => 'Касса списания',
    'class' => 'span2',
    'sortable' => true,
    'searchable' => true,
  ),
  'summ' => 
  array (
    'label' => 'Списано',
    'class' => 'span1',
    'sortable' => true,
    'searchable' => true,
  ),
  'course' => 
  array (
    'label' => 'Курс',
    'class' => 'span1',
    'sortable' => true,
    'searchable' => true,
  ),
  'summ_approve' => 
  array (
    'label' => 'Получено',
    'class' => 'span1',
    'sortable' => true,
    'searchable' => true,
  ),
  'paymovecashdescdestination.name' => 
  array (
    'label' => 'Касса зачисления',
    'class' => 'span2',
    'sortable' => true,
    'searchable' => true,
  ),
  'description' => 
  array (
    'label' => 'Подробности',
    'searchable' => true,
  ),
);
