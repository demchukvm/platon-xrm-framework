<?php
return array (
  'name' => 
  array (
    'label' => 'NAME',
    'type' => 'text',
  ),
  'status' => 
  array (
    'label' => 'Статус',
    'type' => 'text',
  ),
  'summ' => 
  array (
    'label' => 'Списано',
    'type' => 'text',
    'class' => 'floatPositive',
    'required' => true,
  ),
  'summ_approve' => 
  array (
    'label' => 'Получено',
    'type' => 'text',
    'class' => 'floatPositive',
    'readonly' => true,
  ),
  'course' => 
  array (
    'label' => 'Курс',
    'type' => 'text',
    'class' => 'floatPositive fixedDigits-6',
    'requried' => true,
  ),
  'description' => 
  array (
    'label' => 'Подробности',
    'type' => 'textarea',
  ),
  'date' => 
  array (
    'label' => 'Дата',
    'type' => 'date',
  ),
  'id' => 
  array (
    'label' => 'Номер',
    'type' => 'text',
  ),
  'active' => 
  array (
    'label' => 'ACTIVE',
    'type' => 'checkbox',
  ),
  'create' => 
  array (
    'label' => 'CREATE',
    'type' => 'text',
  ),
);
