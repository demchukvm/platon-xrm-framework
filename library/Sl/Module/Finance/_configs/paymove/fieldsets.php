<?php

return array(
    '_default' =>
    array(
        'fields' =>
        array(
            0 => 'status',
            1 => 'date',
            2 => 'paymovecashdesc.name',
            3 => 'summ',
            4 => 'course',
            5 => 'summ_approve',
            6 => 'paymovecashdescdestination.name',
            7 => 'description',
        ),
        'name' => '_default',
        'label' => 'По-умолчанию',
    ),
    '_popup' =>
    array(
        'fields' =>
        array(
            'description',
        ),
        'name' => 'popup',
        'label' => 'По-умолчанию',
        'type' => 'popup',
    ),
);
