<?php
return array (
  'name' => 
  array (
    'order' => 10,
    'label' => 'NAME',
  ),
  'status' => 
  array (
    'order' => 20,
    'label' => 'Статус',
    'select' => true,
    'sortable' => true,
    'searchable' => true,
  ),
  'date' => 
  array (
    'order' => 30,
    'label' => 'Дата',
    'type' => 'date',
    'sortable' => true,
    'searchable' => true,
  ),
  'paymovecashdesc.name' => 
  array (
    'order' => 40,
    'label' => 'Касса списания',
    'class' => 'span2',
    'sortable' => true,
    'searchable' => true,
  ),
  'summ' => 
  array (
    'order' => 50,
    'label' => 'Списано',
    'class' => 'span1',
    'sortable' => true,
    'searchable' => true,
  ),
  'course' => 
  array (
    'label' => 'Курс',
    'order' => 60,
    'class' => 'span1',
    'sortable' => true,
    'searchable' => true,
  ),
  'summ_approve' => 
  array (
    'order' => 70,
    'label' => 'Получено',
    'class' => 'span1',
    'sortable' => true,
    'searchable' => true,
  ),
  'paymovecashdescdestination.name' => 
  array (
    'order' => 80,
    'label' => 'Касса зачисления',
    'class' => 'span2',
    'sortable' => true,
    'searchable' => true,
  ),
  'description' => 
  array (
    'order' => 90,
    'label' => 'Подробности',
    'searchable' => true,
  ),
);
