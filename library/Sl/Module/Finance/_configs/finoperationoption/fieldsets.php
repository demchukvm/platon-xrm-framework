<?php

return array(
    '_default' =>
    array(
        'fields' =>
        array(
            0 => 'description',
            1 => 'desc',
            2 => 'cashdesk',
            3 => 'currency',
            4 => 'course',
            5 => 'units',
            6 => 'units_price',
            7 => 'weight',
            8 => 'weight_price',
            9 => 'insurance',
            10 => 'name',
        ),
        'name' => '_default',
        'label' => 'По-умолчанию',
    ),
    '_popup' =>
    array(
        'fields' =>
        array(
            'name',
        ),
        'name' => 'popup',
        'label' => 'По-умолчанию',
        'type' => 'popup',
    ),
);
