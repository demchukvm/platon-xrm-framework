<?php
return array (
  'description' => 
  array (
    'label' => 'Комментарий',
    'type' => 'text',
  ),
  'desc' => 
  array (
    'label' => 'DESC',
    'type' => 'text',
  ),
  'cashdesk' => 
  array (
    'label' => 'Касса',
    'type' => 'text',
  ),
  'currency' => 
  array (
    'label' => 'Валюта',
    'type' => 'text',
  ),
  'course' => 
  array (
    'label' => 'Курс',
    'type' => 'text',
  ),
  'units' => 
  array (
    'label' => 'UNITS',
    'type' => 'text',
  ),
  'units_price' => 
  array (
    'label' => 'UNITS_PRICE',
    'type' => 'text',
  ),
  'weight' => 
  array (
    'label' => 'WEIGHT',
    'type' => 'text',
  ),
  'weight_price' => 
  array (
    'label' => 'WEIGHT_PRICE',
    'type' => 'text',
  ),
  'insurance' => 
  array (
    'label' => 'INSURANCE',
    'type' => 'text',
  ),
  'name' => 
  array (
    'label' => 'Название',
    'type' => 'text',
  ),
  'create' => 
  array (
    'label' => 'CREATE',
    'type' => 'text',
  ),
  'id' => 
  array (
    'label' => 'ID',
    'type' => 'text',
  ),
  'active' => 
  array (
    'label' => 'ACTIVE',
    'type' => 'checkbox',
  ),
);
