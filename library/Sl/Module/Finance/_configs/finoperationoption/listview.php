<?php
return array (
  'description' => 
  array (
    'label' => 'Комментарий',
  ),
  'desc' => 
  array (
    'label' => 'Доп',
  ),
  'cashdesk' => 
  array (
    'label' => 'Касса',
  ),
  'currency' => 
  array (
    'label' => 'Валюта',
  ),
  'course' => 
  array (
    'label' => 'Курс',
  ),
  'units' => 
  array (
    'label' => 'Курс',
  ),
  'units_price' => 
  array (
    'label' => 'Курс',
  ),
  'weight' => 
  array (
    'label' => 'Курс',
  ),
  'weight_price' => 
  array (
    'label' => 'Курс',
  ),
  'insurance' => 
  array (
    'label' => 'Курс',
  ),
  'name' => 
  array (
    'label' => 'Курс',
  ),
);
