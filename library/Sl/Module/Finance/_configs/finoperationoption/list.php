<?php
return array (
  'description' => 
  array (
    'order' => 10,
    'label' => 'Комментарий',
  ),
  'desc' => 
  array (
    'order' => 15,
    'label' => 'Доп',
  ),
  'cashdesk' => 
  array (
    'order' => 20,
    'label' => 'Касса',
  ),
  'currency' => 
  array (
    'order' => 30,
    'label' => 'Валюта',
  ),
  'course' => 
  array (
    'order' => 40,
    'label' => 'Курс',
  ),
  'units' => 
  array (
    'order' => 50,
    'label' => 'Курс',
  ),
  'units_price' => 
  array (
    'order' => 60,
    'label' => 'Курс',
  ),
  'weight' => 
  array (
    'order' => 70,
    'label' => 'Курс',
  ),
  'weight_price' => 
  array (
    'order' => 80,
    'label' => 'Курс',
  ),
  'insurance' => 
  array (
    'order' => 90,
    'label' => 'Курс',
  ),
  'name' => 
  array (
    'order' => 100,
    'label' => 'Курс',
  ),
);
