<?php
return array (
  'name' => 
  array (
    'order' => 10,
    'label' => 'Название',
    'searchable' => true,
    'sortable' => true,
  ),
  'bonuscustomer.name' => 
  array (
    'order' => 15,
    'label' => 'Контрагент',
    'searchable' => true,
    'sortable' => true,
  ),
  'bonuscustomer' => 
  array (
    'order' => 16,
    'label' => 'Контрагент',
  ),
  'sum' => 
  array (
    'order' => 20,
    'label' => 'Сумма',
    'sortable' => true,
  ),
  'status' => 
  array (
    'order' => 30,
    'label' => 'Статус',
  ),
  'description' => 
  array (
    'order' => 40,
    'label' => 'Комментарий',
  ),
);
