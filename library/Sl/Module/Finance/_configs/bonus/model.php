<?php
return array (
  'name' => 
  array (
    'label' => 'Название',
    'type' => 'text',
  ),
  'sum' => 
  array (
    'label' => 'Сумма начисления',
    'type' => 'text',
  ),
  'status' => 
  array (
    'label' => 'Статус',
    'type' => 'text',
  ),
  'description' => 
  array (
    'label' => 'Комментарий',
    'type' => 'text',
  ),
  'create' => 
  array (
    'label' => 'Дата создания',
    'type' => 'text',
  ),
  'id' => 
  array (
    'label' => 'Номер',
    'type' => 'text',
  ),
  'active' => 
  array (
    'label' => 'ACTIVE',
    'type' => 'checkbox',
  ),
);
