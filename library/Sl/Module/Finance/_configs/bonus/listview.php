<?php
return array (
  'name' => 
  array (
    'label' => 'Название',
    'searchable' => true,
    'sortable' => true,
  ),
  'bonuscustomer.name' => 
  array (
    'label' => 'Контрагент',
    'searchable' => true,
    'sortable' => true,
  ),
 
  'sum' => 
  array (
    'label' => 'Сумма',
    'sortable' => true,
  ),
  'status' => 
  array (
    'label' => 'Статус',
  ),
  'description' => 
  array (
    'label' => 'Комментарий',
  ),
);
