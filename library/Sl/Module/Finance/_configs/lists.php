<?php
return array (
  'financial_statuses' => 
  array (
    0 => 'Не оплачен',
    1 => 'Частично оплачен',
    2 => 'Оплачен',
  ),
  'financial_payment_statuses' => 
  array (
    -1 => 'Отменен',
    0 => 'Ожидание',
    1 => 'Оплачен',
    2 => 'Принят',
    4294967295 => 'Отменен',
  ),
);
