<?php

return array(
    '_default' =>
    array(
        'fields' =>
        array(
            0 => 'doc_id',
            1 => 'reason_id',
            2 => 'account_id',
            3 => 'qty',
            4 => 'factor',
            5 => 'status',
        ),
        'name' => '_default',
        'label' => 'По-умолчанию',
    ),
    '_popup' =>
    array(
        'fields' =>
        array(
            'doc_id',
        ),
        'name' => 'popup',
        'label' => 'По-умолчанию',
        'type' => 'popup',
    ),
);
