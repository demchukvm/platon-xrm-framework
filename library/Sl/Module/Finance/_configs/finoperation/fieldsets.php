<?php

return array(
    '_default' =>
    array(
        'fields' =>
        array(
            'timestamp',
            'finoptions.name',
            //'transaction_sum', 
            'finoptions.desc',
            'finoptions.cashdesk',
            'finoptions.currency',
            'finoptions.course',
            'finoptions.description',
        ),
        'name' => '_default',
        'label' => 'По-умолчанию',
    ),
    '_popup' =>
    array(
        'fields' =>
        array(
            'finoptions.name',
        ),
        'name' => 'popup',
        'label' => 'По-умолчанию',
        'type' => 'popup',
    ),
);
