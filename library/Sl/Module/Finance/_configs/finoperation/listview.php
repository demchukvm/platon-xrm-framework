<?php
return array (
  'timestamp' => 
  array (
    'label' => 'Дата',
    'type' => 'date',
    'sortable' => true,
    'searchable' => true,
    'class' => 'span2',
  ),
  'document_sum' => 
  array (
    'label' => 'Cумма док.',
  ),
  'transaction_sum' => 
  array (
    'label' => 'Проведенная сумма',
    'calculate' => 'sum',
  ),
  'finoptions.name' => 
  array (
    'label' => 'Накладная',
    'sortable' => true,
    'searchable' => true,
    'class' => 'span2',
  ),
  'finoptions.desc' => 
  array (
    'label' => 'Название док.',
    'sortable' => true,
    'searchable' => true,
    'class' => 'span2',
  ),
  'finoptions.cashdesk' => 
  array (
    'label' => 'Касса',
    'sortable' => true,
    'searchable' => true,
    'class' => 'span2',
  ),
  'finoptions.currency' => 
  array (
    'label' => 'Валюта',
    'sortable' => true,
    'searchable' => true,
    'class' => 'span1',
  ),
  'finoptions.course' => 
  array (
    'label' => 'Курс',
    'sortable' => true,
    'searchable' => true,
    'class' => 'span1',
  ),
  'finoptions.units' => 
  array (
    'label' => 'Шт.',
    'sortable' => true,
    'searchable' => true,
    'class' => 'span1',
  ),
  'finoptions.units_price' => 
  array (
    'label' => 'Ст. шт.',
    'sortable' => true,
    'searchable' => true,
    'class' => 'span1',
  ),
  'finoptions.weight' => 
  array (
    'label' => 'Кг.',
    'sortable' => true,
    'searchable' => true,
    'class' => 'span1',
  ),
  'finoptions.weight_price' => 
  array (
    'label' => 'Ст. кг.',
    'sortable' => true,
    'searchable' => true,
    'class' => 'span1',
  ),
  'finoptions.insurance' => 
  array (
    'label' => 'Страх.',
    'sortable' => true,
    'searchable' => true,
    'class' => 'span1',
  ),
  'sum' => 
  array (
    'label' => 'SUM',
  ),
  'master_relation' => 
  array (
    'label' => 'Объект',
  ),
  'factor' => 
  array (
    'label' => 'FACTOR',
  ),
  'finoperationballance.id' => 
  array (
    'searchable' => true,
  ),
  'id' => 
  array (
  ),
  'finoptions.description' => 
  array (
    'label' => 'Комментарий',
    'sortable' => true,
    'searchable' => true,
    'class' => 'span2',
  ),
  'packagefinoperation.id'=>
    array(),
  
);
