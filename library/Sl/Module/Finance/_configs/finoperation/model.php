<?php
return array (
  'sum' => 
  array (
    'label' => 'SUM',
    'type' => 'text',
  ),
  'factor' => 
  array (
    'label' => 'FACTOR',
    'type' => 'text',
  ),
  'master_relation' => 
  array (
    'label' => 'MASTER_RELATION',
    'type' => 'text',
  ),
  'id' => 
  array (
    'label' => 'ID',
    'type' => 'text',
  ),
  'active' => 
  array (
    'label' => 'ACTIVE',
    'type' => 'checkbox',
  ),
  'timestamp' => 
  array (
    'label' => 'TIMESTAMP',
    'type' => 'text',
  ),
);
