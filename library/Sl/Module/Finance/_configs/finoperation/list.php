<?php
return array (
  'timestamp' => 
  array (
    'label' => 'Дата',
    'order' => 10,
    'type' => 'date',
    'visible' => true,
    'sortable' => true,
    'searchable' => true,
    'class' => 'span2',
  ),
  'document_sum' => 
  array (
    'label' => 'Cумма док.',
    'order' => 30,
    'visible' => false,
    'hidable' => true,
  ),
  'transaction_sum' => 
  array (
    'label' => 'Проведенная сумма',
    'order' => 40,
    'calculate' => 'sum',
  ),
  'finoptions.name' => 
  array (
    'label' => 'Накладная',
    'order' => 45,
    'sortable' => true,
    'searchable' => true,
    'class' => 'span2',
  ),
  'finoptions.desc' => 
  array (
    'label' => 'Назавние док.',
    'order' => 47,
    'sortable' => true,
    'searchable' => true,
    'class' => 'span2',
  ),
  'finoptions.cashdesk' => 
  array (
    'label' => 'Касса',
    'order' => 50,
    'sortable' => true,
    'searchable' => true,
    'class' => 'span2',
  ),
  'finoptions.currency' => 
  array (
    'label' => 'Валюта',
    'order' => 60,
    'sortable' => true,
    'searchable' => true,
    'class' => 'span1',
  ),
  'finoptions.course' => 
  array (
    'label' => 'Курс',
    'order' => 70,
    'sortable' => true,
    'searchable' => true,
    'class' => 'span1',
  ),
  'finoptions.units' => 
  array (
    'label' => 'Шт.',
    'order' => 80,
    'sortable' => true,
    'searchable' => true,
    'class' => 'span1',
    'visible' => false,
    'hidable' => true,
  ),
  'finoptions.units_price' => 
  array (
    'label' => 'Ст. шт.',
    'order' => 90,
    'sortable' => true,
    'searchable' => true,
    'class' => 'span1',
    'visible' => false,
    'hidable' => true,
  ),
  'finoptions.weight' => 
  array (
    'label' => 'Кг.',
    'order' => 100,
    'sortable' => true,
    'searchable' => true,
    'class' => 'span1',
    'visible' => false,
    'hidable' => true,
  ),
  'finoptions.weight_price' => 
  array (
    'label' => 'Ст. кг.',
    'order' => 110,
    'sortable' => true,
    'searchable' => true,
    'class' => 'span1',
    'visible' => false,
    'hidable' => true,
  ),
  'finoptions.insurance' => 
  array (
    'label' => 'Страх.',
    'order' => 120,
    'sortable' => true,
    'searchable' => true,
    'class' => 'span1',
    'visible' => false,
    'hidable' => true,
  ),
  'sum' => 
  array (
    'order' => 46,
    'label' => 'SUM',
  ),
  'master_relation' => 
  array (
    'order' => 47,
    'label' => 'Объект',
    'visible' => false,
    'list_hidden' => true,
  ),
  'factor' => 
  array (
    'order' => 48,
    'label' => 'FACTOR',
  ),
  'finoperationballance.id' => 
  array (
    'order' => 53,
    'visible' => false,
    'searchable' => true,
  ),
  'id' => 
  array (
    'order' => 54,
    'visible' => false,
  ),
  'finoptions.description' => 
  array (
    'label' => 'Комментарий',
    'order' => 510,
    'sortable' => true,
    'searchable' => true,
    'class' => 'span2',
  ),
);
