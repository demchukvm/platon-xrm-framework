<?php

return array(
    '_default' =>
    array(
        'fields' =>
        array(
            0 => 'name',
            1 => 'cashdesccurrency.name',
            2 => 'current_ballance',
        ),
        'name' => '_default',
        'label' => 'По-умолчанию',
    ),
    '_popup' =>
    array(
        'fields' =>
        array(
            'name',
        ),
        'name' => 'popup',
        'label' => 'По-умолчанию',
        'type' => 'popup',
    ),
);
