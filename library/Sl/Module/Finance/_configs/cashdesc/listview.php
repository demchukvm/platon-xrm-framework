<?php
return array (
  'name' => 
  array (
   
    'label' => 'Название',
    'searchable' => true,
    'sortable' => true,
  ),
  'current_ballance' => 
  array (
    
    'label' => 'Текущий баланс',
  ),
  'cashdesccurrency.name' => 
  array (
    
    'label' => 'Валюта по умолчанию',
    'searchable' => true,
    'sortable' => true,
  ),
 
);
