<?php
return array (
  'modulerelation_cashdescuser' => 
  array (
    'label' => 'Ответственный',
    'request_fields' => 
    array (
      1 => 'name',
      2 => 'email',
      3 => 'login',
      4 => 'userroles:name',
    ),
  ),
  'modulerelation_cashdesccurrency' => 
  array (
    'label' => 'Валюта по умолчанию',
  ),
);
