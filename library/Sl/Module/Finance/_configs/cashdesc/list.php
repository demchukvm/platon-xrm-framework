<?php
return array (
  'name' => 
  array (
    'order' => 10,
    'label' => 'Название',
    'searchable' => true,
    'sortable' => true,
  ),
  'current_ballance' => 
  array (
    'order' => 20,
    'label' => 'Текущий баланс',
  ),
  'cashdesccurrency.name' => 
  array (
    'order' => 15,
    'label' => 'Валюта по умолчанию',
    'searchable' => true,
    'sortable' => true,
  ),
  'cashdesccurrency' => 
  array (
    'label' => 'Валюта по умолчанию',
  ),
  'cashdeschistory' => 
  array (
    'label' => '',
    'order' => 45,
  ),
);
