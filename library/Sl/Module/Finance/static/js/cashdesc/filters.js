$(function(){
	
	$('body').on('click', '.cashdescballance_btn', function() {
			
			var cd_id = parseInt($(this).attr('data-ballance'));
			var $name_span = $(this).parents('tr:first').find('span.dt-cashdesc-name');
			var name = [$name_span.length?$name_span.text():''];
			var $ballance_span =  $(this).parents('tr:first').find('span.dt-cashdesc-current_ballance');
			
			if ($ballance_span.length){
				var $currency_span =  $(this).parents('tr:first').find('span.dt-currency-name');
				name[name.length] = $ballance_span.text()+($currency_span.length?$currency_span.text():'');
				
				 
			}
			title = name.join(': ');
			 
			if (cd_id > 0) {

				var request_obj = {};
				request_obj['id'] = cd_id;
				request_obj['is_iframe'] = 1;
				// /finance/finoperation/list/
				//var string = url+'?'+$.param(request_obj);
				var url = '/finance/cashdesc/history/';
				var $iframeDiv = $('<div />').css({
					width : 990,
					padding : 0,
					
				});

				var $iframe = $('<iframe />').attr({
					width : 990,
					height : 550
				}).css({
					border : 0
				}).attr('src', document.location.protocol + '//' + document.location.host + url + '?' + $.param(request_obj)).appendTo($iframeDiv);

				$('body').append($iframeDiv);

				//$iframe.closeIframe = f;

				$iframeDiv.dialog({
					title : '',
					autoOpen : false,
					width : 1000,
					height : 600,
					modal : true,
					resizable : false,
					title : title
				});
				//$iframeDiv.html($iframe);
				$iframeDiv.dialog('open');

				closeIframeFunction = function() {
					//bill_get_request_info();
					refreshCustomerBallance($ballance_div.attr('id'));
					$iframeDiv.dialog('close');
					$iframeDiv.remove();

				}
			} 

			return false;
		});
	
	/* $('body').on('click','.finoperationballance_btn',function(){
		var url = $(this).attr('data-rel');
		if (url.length){
			document.location.href=url;
		}
		return false;
	}); */
});
