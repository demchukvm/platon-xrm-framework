<?php

namespace Sl\Module\Finance\Listener;

class Financehistory extends \Sl_Listener_Abstract implements \Sl\Listener\Fieldset, \Sl\Listener\Dataset {

    protected $_fields = array('paymentfinoperation.id', 'packagefinoperation.id', 'theftfinoperation.id', 'bonusfinoperation.id', 'orderfinoperation.id', 'finoperationdealercopy.id');
    protected $_transaction_sum;

    public function onAfterProcessAll(\Sl\Event\Dataset $event) {
        
    }

    public function onAfterProcessItem(\Sl\Event\Dataset $event) {
        // print_R($event->getFieldset()->getFields()); die;
        if ($event->getModel() instanceof \Sl\Module\Finance\Model\Finoperation) {
            $item = $event->getItem();
            // обраховуємо і заносимо значення total_sum
            $item['transaction_sum'] = $item['sum'] * (isset($item['factor']) ? $item['factor'] : 1);

            //формуємо посилання на об'єкти в залежності від зв'язку фін.операції     
            $fields = $this->_fields;
            foreach ($fields as $val) {
                $field = explode('.', $val);
                if ($item[$field[0]][$field[1]]) {
                    $rel = \Sl_Modulerelation_Manager::getRelations($event->getModel(), $field[0]);
                    $related = $rel->getRelatedObject($event->getModel());
                    $item['_meta']['data']['link'] = \Sl\Service\Helper::buildModelUrl($related, 'edit', array(
                                'id' => $item[$field[0]][$field[1]],
                    ));
                }
            }
            $event->setItem($item);
        }
    }

    public function onBeforeProcessAll(\Sl\Event\Dataset $event) {
        
    }

    public function onBeforeProcessItem(\Sl\Event\Dataset $event) {
        
    }

    public function onPrepare(\Sl\Event\Fieldset $event) {
        return;
        if ($event->getModel() instanceof \Sl\Module\Finance\Model\Finoperation) {

// додаєм необхідні поля для обчислення total_sum
            if (!$event->getFieldset()->hasField('sum')) {
                $event->getfieldset()->createField('sum', array(
                    'label' => 'Сумма',
                    //'visible' => false,
                    'roles' => array(
                        'from',)
                ));
            }
            if (!$event->getFieldset()->hasField('factor')) {
                $event->getfieldset()->createField('factor', array(
                    'label' => 'Сумма',
                    //   'visible' => false,
                    'roles' => array(
                        'from',)
                ));
            }
// створюєм кастомну колонку "Проведенная сумма"
            if (!$event->getFieldset()->hasField('transaction_sum')) {
                $event->getFieldset()->createField('transaction_sum', array(
                    'label' => 'Проведенная сумма',
                    'sortable' => true,
                    'searchable' => true,
                    'order' => 11,
                    'roles' => array(
                        'render',
                    )
                ));
            } else {
                $event->getFieldset()->getField('transaction_sum')->addRole('render');
            }
            $event->getFieldset()->moveField('transaction_sum', $position = array('after' => 'finoptions.name'));

            $array = $this->_fields;
            foreach ($array as $val) {
                if (!$event->getFieldset()->hasField($val)) {
                    $event->getFieldset()->createField($val, array(
                        //  'visible' => false,
                        'type' => 'hidden',
                        'label' => 'test',
                        'roles' => array(
                            'from',
                        ),
                    ));
                }
            }
        }
    }

    public function onPrepareAjax(\Sl\Event\Fieldset $event) {
        
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
