<?php

namespace Sl\Module\Finance\Listener;

class Cashdesclistview extends \Sl_Listener_Abstract implements \Sl\Listener\Fieldset, \Sl\Listener\Dataset {

    public function onPrepare(\Sl\Event\Fieldset $event) {

        if ($event->getModel() instanceof \Sl\Module\Finance\Model\Cashdesc) {


            $addition_fields = array(
                'cashdeschistory' => array(
                    'label' => '  ',
                    'title' => $this->getTranslator()->translate('История транзакций'),
                    //'visible' => false,
                    'roles' => array(
                        'render',)),
                'id' => array(
                    'label' => '  ',
                    //'title' => $this->getTranslator()->translate('История транзакций'), 
                    //'visible' => false,
                    'roles' => array(
                        'from',))
            );
            //TO DO: нижний метод вынести куда-то в абстракт
            foreach ($addition_fields as $field => $options) {
                if (!$event->getFieldset()->hasField($field)) {
                    $cur_field = $event->getFieldset()->createField($field);
                } else {
                    $cur_field = $event->getFieldset()->getField($field);
                }
                $cur_field->fill($options);
            }
        }
    }

    public function onAfterProcessAll(\Sl\Event\Dataset $event) {
        
    }

    public function onAfterProcessItem(\Sl\Event\Dataset $event) {
        if ($event->getModel() instanceof \Sl\Module\Finance\Model\Cashdesc) {
            $item = $event->getItem();
            if (isset($item['id']) && ($item['id'])) {
                $item['cashdeschistory'] = '<a href="#" data-ballance="' . $item['id'] . '" class="cashdescballance_btn icon-question-sign" title="История транзакций"></a>';
            }
            $event->setItem($item);
        }
    }

    public function onBeforeProcessAll(\Sl\Event\Dataset $event) {
        
    }

    public function onBeforeProcessItem(\Sl\Event\Dataset $event) {
        
    }

    public function onPrepareAjax(\Sl\Event\Fieldset $event) {
        
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
