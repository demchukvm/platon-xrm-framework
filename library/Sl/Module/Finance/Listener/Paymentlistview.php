<?php

namespace Sl\Module\Finance\Listener;

class Paymentlistview extends \Sl_Listener_Abstract implements \Sl\Listener\Fieldset, \Sl\Listener\Dataset {

    public function onPrepare(\Sl\Event\Fieldset $event) {

        if ($event->getModel() instanceof \Sl\Module\Finance\Model\Payment) {


            $addition_fields = array(
                'name' => array(
                    'roles' => array(
                        'from',)),
                'billpayment.id' => array(
                    'roles' => array(
                        'from',)),
                'paymentcustomer.customerisdealer.id' => array(
                    'roles' => array(
                        'from',)),
                'paymentcustomer.customerdealer.id' => array(
                    'roles' => array(
                        'from',)),
                'billpayment.billcustomer.customerdealer.id' => array(
                    'roles' => array(
                        'from',)),
                'billpayment.billcustomer.customerisdealer.id' => array(
                    'roles' => array(
                        'from',)),
            );
            //TO DO: нижний метод вынести куда-то в абстракт
            foreach ($addition_fields as $field => $options) {
                if (!$event->getFieldset()->hasField($field)) {
                    $cur_field = $event->getFieldset()->createField($field);
                } else {
                    $cur_field = $event->getFieldset()->getField($field);
                }
                $cur_field->fill($options);
            }
        }
    }

    public function onAfterProcessItem(\Sl\Event\Dataset $event) {
        if ($event->getModel() instanceof \Sl\Module\Finance\Model\Payment) {
            $item = $event->getItem();

            if (isset($item['billpayment']['name'])) {

                if ($item['name']) {
                    if (!$item['billpayment']['id']) {
                        $item['billpayment']['name'] = preg_replace('/(.+) (.+) (.+)/', '$2', $item['name']);
                    }
                }

                if (isset($item['billpayment']['id']) && ($item['billpayment']['id'] > 0)) {

                    if (isset($item['billpayment']['billcustomer']['customerisdealer']['id']) && $item['billpayment']['billcustomer']['customerisdealer']['id'] > 0) {
                        $item['billpayment']['name'] .= '<span class="htmlify icon-star-empty pull-right" title="' . $this->getTranslator()->translate('Дилер') . '" >&nbsp;</span>';
                    }
                    if (isset($item['billpayment']['billcustomer']['customerdealer']['id']) && $item['billpayment']['billcustomer']['customerdealer']['id'] > 0) {
                        $item['billpayment']['name'] .= '<span class="htmlify icon-asterisk pull-right" title="' . $this->getTranslator()->translate('Привлечен дилером') . '">&nbsp;</span>';
                    }
                } else {

                    if (isset($item['paymentcustomer']['customerisdealer']['id']) && $item['paymentcustomer']['customerisdealer']['id'] > 0) {
                        $item['billpayment']['name'] .= '<span class="htmlify icon-star-empty pull-right" title="' . $this->getTranslator()->translate('Дилер') . '" >&nbsp;</span>';
                    }
                    if (isset($item['paymentcustomer']['customerdealer']['id']) && $item['paymentcustomer']['customerdealer']['id'] > 0) {
                        $item['billpayment']['name'] .= '<span class="htmlify icon-asterisk pull-right" title="' . $this->getTranslator()->translate('Привлечен дилером') . '">&nbsp;</span>';
                    }
                }
                $event->setItem($item);
            }
        }
    }

    public function onPrepareAjax(\Sl\Event\Fieldset $event) {
        
    }

    public function onAfterProcessAll(\Sl\Event\Dataset $event) {
        
    }

    public function onBeforeProcessAll(\Sl\Event\Dataset $event) {
        
    }

    public function onBeforeProcessItem(\Sl\Event\Dataset $event) {
        
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
