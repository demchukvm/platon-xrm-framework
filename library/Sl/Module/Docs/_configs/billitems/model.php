<?php
return array (
  'name' => 
  array (
    'label' => 'Оплата:',
    'type' => 'text',
    'sort_order' => 10,
  ),
  'sum' => 
  array (
    'label' => 'К оплате',
    'type' => 'text',
    'sort_order' => 20,
  ),
  'create' => 
  array (
    'label' => 'CREATE',
    'type' => 'hidden',
    'sort_order' => 50,
  ),
  'id' => 
  array (
    'label' => 'ID',
    'type' => 'hidden',
  ),
  'active' => 
  array (
    'label' => 'ACTIVE',
    'type' => 'checkbox',
  ),
);
