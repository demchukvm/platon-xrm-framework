<?php
return array (
  'modulerelation_billitemsfinoperation' => 
  array (
    'label' => 'Документ',
    'request_fields' => 
    array (
      0 => 'master_relation',
    ),
    'sort_order' => 20,
  ),
  'name' => 
  array (
    'sort_order' => 40,
  ),
  'sum' => 
  array (
    'sort_order' => 60,
    'label' => 'К оплате',
    'readonly' => true,
  ),
);
