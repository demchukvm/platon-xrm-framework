<?php
return array (
  'name' => 
  array (
    'order' => 10,
    'label' => 'Название',
    'searchable' => true,
    'sortable' => true,
  ),
  'fin_status' => 
  array (
    'order' => 15,
    'label' => 'Статус',
    'select' => true,
    'searchable' => true,
    'sortable' => true,
  ),
  'sum' => 
  array (
    'order' => 20,
    'label' => 'Сумма',
    'searchable' => true,
    'sortable' => true,
  ),
  'create' => 
  array (
    'order' => 75,
    'label' => 'Создан',
    'sortable' => true,
    'searchable' => true,
  ),
  'description' => 
  array (
    'label' => 'Комментарий',
    'order' => 90,
    'visible' => false,
    'hidable' => true,
    'searchable' => true,
  ),
);
