<?php
return array (
  'name' => 
  array (
    'label' => 'NAME',
    'type' => 'hidden',
  ),
  'sum' => 
  array (
    'label' => 'Сумма',
    'type' => 'text',
    'class' => 'floatPositive',
  ),
  'description' => 
  array (
    'label' => 'Комментарий',
    'type' => 'textarea',
  ),
  'fin_status' => 
  array (
    'label' => 'Статус оплаты',
    'type' => 'text',
    'disabled' => true,
  ),
  'create' => 
  array (
    'label' => 'CREATE',
    'type' => 'text',
  ),
  'id' => 
  array (
    'label' => 'ID',
    'type' => 'hidden',
  ),
  'active' => 
  array (
    'label' => 'ACTIVE',
    'type' => 'checkbox',
  ),
);
