<?php
return array (
  'name' => 
  array (
    'label' => 'Название',
    'searchable' => true,
    'sortable' => true,
  ),
  'fin_status' => 
  array (
    'label' => 'Статус',
    'searchable' => true,
    'sortable' => true,
  ),
  'sum' => 
  array (
    'label' => 'Сумма',
    'searchable' => true,
    'sortable' => true,
  ),
  'create' => 
  array (
    'label' => 'Создан',
    'sortable' => true,
    'searchable' => true,
  ),
  'description' => 
  array (
    'label' => 'Комментарий',
    'searchable' => true,
  ),
);
