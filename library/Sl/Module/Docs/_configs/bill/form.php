<?php
return array (
  'modulerelation_billbillitems' => 
  array (
    'label' => 'Оплата услуг',
    'sort_order' => 40,
  ),
  'modulerelation_billcustomer' => 
  array (
    'readonly' => true,
    'label' => 'Плательщик',
    'sort_order' => 20,
  ),
  'sum' => 
  array (
    'sort_order' => 80,
    'readonly' => true,
  ),
  'description' => 
  array (
    'sort_order' => 90,
  ),
  'modulerelation_billpayment' => 
  array (
    'label' => 'Оплаты по счету',
  ),
);
