<?php
namespace Sl\Module\Home\Service;

use Sl_Model_Abstract as AbstractModel;
use Sl\Service\Helper;
use Sl\Service\Config;
use Sl\Model\Identity\Fieldset;

class Quickforms {
    
    protected static $_forms = array();
    
    public static function get(AbstractModel $model = null, $type = null) {
        if(is_null($model)) {
            return self::$_forms;
        }
        $alias = Helper::getModelAlias($model);
        $forms = array_key_exists($alias, self::$_forms)?self::$_forms[$alias]:array();
        if(is_null($type)) {
            return $forms;
        } else {
            return array_key_exists($type, $forms)?$forms[$type]:null;
        }
    }
    
    public static function add(AbstractModel $model, $type = '_detailed') {
        if(self::check($model, $type)) {
            return;
        }
        $fieldset = Fieldset\Factory::build($model, 'form');
        $fieldsetData = Config::read($model, 'fieldsets/'.$type);

        $fieldsData = Config::read($model, 'form', Config::MERGE_FIELDS);
        foreach($fieldsData as $name=>$field) {
            $fieldset->createField($name, $field->toArray());
        }

        $fields = array();
        foreach($fieldset->getFields() as $field) {
            /*@var $field Field*/
            if($fieldsetData->fields) {
                if(!in_array($field->getName(), $fieldsetData->fields->toArray())) {
                    continue;
                }
                $fields[$field->getName()] = $field->toArray();
                $fields[$field->getName()]['label'] = \Zend_Registry::get('Zend_Translate')->translate($fields[$field->getName()]['label']);
            }
        }
        $alias = Helper::getModelAlias($model);
        if(!isset(self::$_forms[$alias])) {
            self::$_forms[$alias] = array();
        }
        self::$_forms[$alias][$type] = $fields;
    }
    
    public static function check(AbstractModel $model, $type) {
        $alias = Helper::getModelAlias($model);
        return isset(self::$_forms[$alias])?isset(self::$_forms[$alias][$type]):false;
    }
}