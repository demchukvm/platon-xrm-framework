<?php
namespace Sl\Module\Home\Listener;

use Sl_Listener_Abstract as AbstractListener;
use Sl_Model_Factory as ModelFactory;
use Sl\Module\Home\Service;

class Quickforms extends AbstractListener implements \Sl_Listener_View_Interface {
    
    public function onBodyEnd(\Sl_Event_View $event) {
        try {
            echo $event->getView()->partial('partials/quickforms.phtml', array(
                'forms' => Service\Quickforms::get(),
            ));
        } catch(\Exception $e) {
            
        }
    }
    
    public function onAfterContent(\Sl_Event_View $event) {}
    public function onBeforeContent(\Sl_Event_View $event) {}
    public function onBeforePageHeader(\Sl_Event_View $event) {}
    public function onBodyBegin(\Sl_Event_View $event) {}
    public function onContent(\Sl_Event_View $event) {}
    public function onFooter(\Sl_Event_View $event) {}
    public function onHeadLink(\Sl_Event_View $event) {}
    public function onHeadScript(\Sl_Event_View $event) {}
    public function onHeadTitle(\Sl_Event_View $event) {}
    public function onHeader(\Sl_Event_View $event) {}
    public function onLogo(\Sl_Event_View $event) {}
    public function onNav(\Sl_Event_View $event) {}
    public function onPageOptions(\Sl_Event_View $event) {}
}