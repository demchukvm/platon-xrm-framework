<?php
return array (
  'model' => 
  array (
    'field_name' => 
    array (
      'label' => 'FIELD_NAME',
      'type' => 'text',
    ),
    'old_value' => 
    array (
      'label' => 'OLD_VALUE',
      'type' => 'text',
    ),
    'new_value' => 
    array (
      'label' => 'NEW_VALUE',
      'type' => 'text',
    ),
    'user_id' => 
    array (
      'label' => 'USER_ID',
      'type' => 'text',
    ),
    'object_id' => 
    array (
      'label' => 'OBJECT_ID',
      'type' => 'text',
    ),
    'action' => 
    array (
      'label' => 'ACTION',
      'type' => 'text',
    ),
    'create' => 
    array (
      'label' => 'CREATE',
      'type' => 'text',
    ),
    'id' => 
    array (
      'label' => 'ID',
      'type' => 'text',
    ),
    'active' => 
    array (
      'label' => 'ACTIVE',
      'type' => 'checkbox',
    ),
    'archived' => 
    array (
      'label' => 'ARCHIVED',
      'type' => 'text',
    ),
  ),
);
