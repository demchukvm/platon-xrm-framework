<?php
return array (
  'model' => 
  array (
    'phone' => 
    array (
      'label' => 'Телефон',
      'type' => 'text',
      'sort_order' => 10,
      'class' => 'mask_phone'
    ),
    'id' => 
    array (
      'label' => 'ID',
      'type' => 'hidden',
    ),
    'active' => 
    array (
      'label' => 'ACTIVE',
      'type' => 'text',
    ),
    'create' => 
    array (
      'label' => 'CREATE',
      'type' => 'text',
    ),
    'archived' => 
    array (
      'label' => 'ARCHIVED',
      'type' => 'text',
    ),
  ),
);
