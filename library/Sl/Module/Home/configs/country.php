<?php
return array (
  'model' => 
  array (
    'name' => 
    array (
      'label' => 'Name',
      'type' => 'text',
      'sort_order' => 20,
    ),
    'code' => 
    array (
      'label' => 'Code',
      'type' => 'text',
      'sort_order' => 30,
    ),
    'id' => 
    array (
      'label' => 'ID',
      'type' => 'hidden',
      'sort_order' => 10,
    ),
    'active' => 
    array (
      'label' => 'ACTIVE',
      'type' => 'text',
      'sort_order' => 100,
    ),
    'create' => 
    array (
      'label' => 'CREATE',
      'type' => 'text',
    ),
    'archived' => 
    array (
      'label' => 'ARCHIVED',
      'type' => 'hidden',
    ),
  ),
);
