<?php
return array (
  'model' => 
  array (
    'description' => 
    array (
      'label' => 'Заголовок',
      'type' => 'textarea',
    ),
    'detail_description' => 
    array (
      'label' => 'Описание',
      'type' => 'textarea',
    ),
    'request' => 
    array (
      'label' => 'REQUEST',
      'type' => 'textarea',
    ),
    'user_id' => 
    array (
      'label' => 'id пользователя',
      'type' => 'text',
    ),
    'create' => 
    array (
      'label' => 'Дата возникновения',
      'type' => 'datetime',
    ),
    'id' => 
    array (
      'label' => 'ID',
      'type' => 'hidden',
    ),
    'active' => 
    array (
      'label' => 'ACTIVE',
      'type' => 'hidden',
    ),
    'archived' => 
    array (
      'label' => 'ARCHIVED',
      'type' => 'hidden',
    ),
  ),
);