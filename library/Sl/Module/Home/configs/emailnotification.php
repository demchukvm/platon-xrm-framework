<?php
return array (
  'model' => 
  array (
    'repeated' => 
    array (
      'label' => 'REPEATED',
      'type' => 'text',
    ),
    'repeat_data' => 
    array (
      'label' => 'REPEAT_DATA',
      'type' => 'text',
    ),
    'name' => 
    array (
      'label' => 'NAME',
      'type' => 'text',
    ),
    'data' => 
    array (
      'label' => 'DATA',
      'type' => 'text',
    ),
    'status' => 
    array (
      'label' => 'STATUS',
      'type' => 'text',
    ),
    'date_deadline' => 
    array (
      'label' => 'DATE_DEADLINE',
      'type' => 'text',
    ),
    'date_start' => 
    array (
      'label' => 'DATE_START',
      'type' => 'text',
    ),
    'create' => 
    array (
      'label' => 'CREATE',
      'type' => 'text',
    ),
    'id' => 
    array (
      'label' => 'ID',
      'type' => 'text',
    ),
    'active' => 
    array (
      'label' => 'ACTIVE',
      'type' => 'checkbox',
    ),
    'archived' => 
    array (
      'label' => 'ARCHIVED',
      'type' => 'text',
    ),
    'extend' => 
    array (
      'label' => 'EXTEND',
      'type' => 'text',
    ),
  ),
);
