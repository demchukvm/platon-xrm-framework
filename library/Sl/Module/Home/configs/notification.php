<?php
return array (
  'model' => 
  array (
    'repeated' => 
    array (
      'label' => 'Повторять',
      'type' => 'checkbox',
    ),
    'repeat_data' => 
    array (
      'label' => 'Настройка повторения',
      'type' => 'text',
    ),
    'name' => 
    array (
      'label' => 'Название',
      'type' => 'text',
    ),
    'data' => 
    array (
      'label' => 'Описание',
      'type' => 'textarea',
    ),
    'status' => 
    array (
      'label' => 'Состояние',
      'type' => 'select',
    ),
    'date_deadline' => 
    array (
      'label' => 'Deadline',
      'type' => 'date',
    ),
    'date_start' => 
    array (
      'label' => 'Дата начала',
      'type' => 'date',
    ),
    'create' => 
    array (
      'label' => 'CREATE',
      'type' => 'text',
    ),
    'id' => 
    array (
      'label' => 'ID',
      'type' => 'hidden',
    ),
    'active' => 
    array (
      'label' => 'ACTIVE',
      'type' => 'checkbox',
    ),
    'archived' => 
    array (
      'label' => 'ARCHIVED',
      'type' => 'text',
    ),
    'extend' => 
    array (
      'label' => 'EXTEND',
      'type' => 'text',
    ),

  ),
);
