<?php
return array (
  'model' => 
  array (
    'mail' => 
    array (
      'label' => 'E-mail',
      'type' => 'text',
      'sort_order' => 10,
      'options' => array(
            'data-rule-email'=>1
        )
    ),
    'id' => 
    array (
      'label' => 'ID',
      'type' => 'hidden',
    ),
    'active' => 
    array (
      'label' => 'ACTIVE',
      'type' => 'checkbox',
    ),
    'create' => 
    array (
      'label' => 'CREATE',
      'type' => 'text',
    ),
    'archived' => 
    array (
      'label' => 'ARCHIVED',
      'type' => 'text',
    ),
  ),
);
