function modelNotifyFeed() {
    var self = this;
    
    
    self.notifyMessages = ko.observableArray([]);
    self.ids = {} ;
    function modelMessage(d){
        var self = this;
        self.title = d.name;
        self.id = d.id;
        self.data = d.data?d.data.replace(/([^>])\n/g, '$1<br/>'):'';
        self.status = d.status;
        self.create = d.create;
    }
    self.addMessage = function(m){
        if (!self.ids.hasOwnProperty(m.id)){
            self.notifyMessages.push(m);
            self.ids[m.id] = m.id;
        }
    }
    
    self.openclose = function () {
        if (self.isHasNewNotifies()){
            var ids = [];
            var mess = self.notifyMessages.removeAll();
            
            for (var i in mess){
                if (mess[i].status == '0'){
                    ids.push(mess[i].id);
                    mess[i].status = '1';
                    self.notifyMessages.push(mess[i]);
                }
            }
            

            $.ajax({
                url : '/notification/ajaxgetnotifications',
                data: {subaction:'read', ids:ids},
                
            }); 
        }
        
    }
    
    self.NewNotifiesCount = ko.computed(function(){
        return _.where(self.notifyMessages(),{status:'0'}).length;
    });
    
    self.isHasNewNotifies = ko.computed(function(){
        return (_.where(self.notifyMessages(),{status:'0'}).length > 0);
    });
    
    self.load = function(){
        $.ajax({
            url : '/notification/ajaxgetnotifications'
            ,
            success: function(data) {
                if(data.result) {
                    
                    for (var i in data.messages){
                        self.addMessage(new modelMessage(data.messages[i]));
                    }
                    
                } else {
                    $.alert(data.description || 'Error');
                }
            },
            
        });
    };
    self.load();
    setInterval(function(){self.load()},20000);
    
}

function mainView() {
    var self = this;
    
    this.packages = ko.observableArray([]);
    
    this.load = function(){
        self.isLoading(true);
        $.ajax({
            data: {
                package: self.search(),
                // Массив уникальных, ненулевых значений одномерный
                box: _.unique(_.compact(_.flatten(_.map((self.searchExt()).split(','), function(o){
                    var a = o.split('-');
                    if(a.length > 1) {
                        return _.range(parseInt(a[0]), 1+parseInt(a[1]));
                    } else {
                        return parseInt(o);
                    }
                })))),
                stock: self.stock()
            },
            success: function(data) {
                self.isLoading(false);
                if(data.result) {
                    self.packages([]);
                    _.map(_.groupBy(data.data, function(o){
                        return o.identifier+(o.identifier_ext?('/'+o.identifier_ext):'');
                    }), function(o, i){
                        self.packages.push(new modelPackage({
                            identifier: i,
                            status: o[0].status,
                            boxes: o,
                            root: self
                        }));
                    });
                    //console.log(ko.toJS(self));
                } else {
                    console.log(data.description || 'Error');
                }
            },
            error: function() {
                self.isLoading(false);
            }
        });
    };
    
    self.search.subscribe(function(){
        self.load();
    });
    
    self.searchExt.subscribe(function(){
        self.load();
    });
    
    self.canSubmit = ko.computed(function() {
        return (_.reduce(this.packages(), function(memo, package){
            return (package.hasChecked()?1:0) + memo;
        }, 0) > 0);
    }, self);
    
}


$(document).ready(function(){
    ko.applyBindings(new modelNotifyFeed(),  document.getElementById("header"));
});