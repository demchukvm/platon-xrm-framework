$(function() {
    $('#module_name, #name').change(function() {
        var values = [$('#module_name').val()];
        var model_name = $('#name').val().split('\\');
        values[values.length] = model_name[model_name.length - 1];
        $('#table_name').val(values.join('_'));
    });

    $('#inherits').change(function(){
        if($(this).val() === '-') {
            $('#table_name').parents('.control-group:first').show(300);
        } else {
            $('#table_name').parents('.control-group:first').hide(300);
        }
    });
});
