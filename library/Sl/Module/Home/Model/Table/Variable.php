<?php

namespace Sl\Module\Home\Model\Table;

class Variable extends \Sl\Model\DbTable\DbTable {

    protected $_name = 'home_variable';
    protected $_primary = 'id';

    public function saveVariable($var, $value, $type) {
        $sql = "INSERT INTO ".$this->_name." SET name = :name, value = :value, type = :type, extend = :extend
                ON DUPLICATE KEY UPDATE  value = :value, type = :type, extend = :extend";
        
        $values = array("name"=>$var, "value"=>$value, "type"=>$type, "extend" =>  \Sl\Service\Helper::getModelInheritanceAlias(\Sl_Model_Factory::object($this)));
        $this->getAdapter()->query($sql,$values);
        
    }
    
    public function findByName($var) {
        $select = $this->getAdapter()->select();
        $select ->from($this->_name);
        $select ->where($this->_name.'.name = ?', strtoupper($var));
       
        return  $this->getAdapter()->fetchRow($select);
        
    }
    
}
