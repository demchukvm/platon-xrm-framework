<?php
namespace Sl\Module\Home\Model\Mapper;

class Notification extends \Sl_Model_Mapper_Abstract {
    //'status' - відповідає за прочитаність 'done' - за повідомлення по email
    protected $_custom_mandatory_fields = array('status', 'done');
    
    protected function _getMappedDomainName() {
        return '\Sl\Module\Home\Model\Notification';
    }

    protected function _getMappedRealName() {
        return '\Sl\Module\Home\Model\Table\Notification';
    }
}