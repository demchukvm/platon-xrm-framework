<?php

namespace Sl\Module\Home\Model\Mapper;

class Variable extends \Sl_Model_Mapper_Abstract {

    protected function _getMappedDomainName() {
        return '\Sl\Module\Home\Model\Variable';
    }

    protected function _getMappedRealName() {
        return '\Sl\Module\Home\Model\Table\Variable';
    }

    /* @return \Sl\Module\Home\Model\Variable
     */

    public function saveVariable($var, $value, $type) {
        $var = strtoupper($var);
        $this->_getDbTable()->saveVariable($var, $value, $type);
        $row = $this->_getDbTable()->findByName($var);

        if (!$row) {

            return null;
        }
        $object = $this->saveCollection($row);
        if ($object->getExtend() !== \Sl\Service\Helper::getModelInheritanceAlias($this->_model())) {
            return null;
        }
        if ($visible && !$object->getActive()) {
            return null;
        }

        return $object;
    }

}
