<?php
namespace Sl\Module\Home\Model\Mapper;
use \Sl\Module\Home\Model\Acnt as Acnts;
class Acnt extends \Sl_Model_Mapper_Abstract {
    
    protected $_custom_mandatory_fields = array('left_key', 'right_key', 'level', 'master_relation','name');
    
	protected function _getMappedDomainName() {
        return '\Sl\Module\Home\Model\Acnt';
    }

    protected function _getMappedRealName() {
        return '\Sl\Module\Home\Model\Table\Acnt';
    }
    
    function createRootNode($name){
        $object = \Sl_Model_Factory::object($this);
        $object -> setName($name);
        $object -> setLeftKey(1);
        $object -> setRightKey(2);
        $object -> setLevel(1);
        return $this->save($object, true);
    }
    
    function prepareNodesForInsert(Acnts $acnt){
        $self_relation_arr = \Sl_Modulerelation_Manager::getObjectsRelations($acnt, $acnt);    
    
        if (count($self_relation_arr)!=2)
            throw new \Exception(get_class($acnt).' must have just one self-relation, but has '.(count($self_relation_arr)/2));
        
        $relation = array_shift($self_relation_arr);
        $relation = $relation->isSelfInverted()?array_shift($self_relation_arr):$relation;
        
        if (!count($acnt->fetchRelated($relation->getName())) && $parent = $this->findRootNode()){
           $acnt->assignRelated($relation->getName(), array($parent));             
        }  
        
        if ($parent = $acnt->fetchOneRelated($relation->getName())){    
        
            if (!($parent instanceof Acnts)){
                
                    $parent = \Sl_Model_Factory::mapper($acnt)->find($parent);
                
            }
            $left_key =     $parent->getLeftKey();
            $right_key =    $parent->getRightKey();
            $level =        $parent->getLevel();
            $data = array();
            $data['right_key'] = new \Zend_DB_Expr('right_key+2');
            $data['left_key'] = new \Zend_DB_Expr("IF(left_key > {$right_key}, left_key + 2, left_key)");
            
            $this->_getDbTable()->update($data, array('right_key >= ?'=>$right_key));    
            
            $acnt -> setLeftKey($right_key)
                  -> setRightKey(++$right_key)
                  -> setLevel(++$level);
        } 
            
        
        return $acnt;
    }
    
    function findRootNode(){
        if ($row =  $this->_getDbTable()->fetchRow('active > 0', 'left_key')){
            
            $row = is_object($row)?$row->toArray():$row;
            
            return $this -> _createInstance($row); 
        }
        
    }
    
    
}