<?php
namespace Sl\Module\Home\Model;

class Phone extends \Sl_Model_Abstract {
    
    protected $_phone;
    
    public function setPhone($phone) {
        $this->_phone = $phone;
        return $this;
    }

    public function getPhone() {
        return $this->_phone;
    }
    
}

