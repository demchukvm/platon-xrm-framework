<?php
return array (
  'doc_id' => 
  array (
    'label' => 'DOC_ID',
    'type' => 'text',
  ),
  'reason_id' => 
  array (
    'label' => 'REASON_ID',
    'type' => 'text',
  ),
  'qty' => 
  array (
    'label' => 'QTY',
    'type' => 'text',
  ),
  'master_relation' => 
  array (
    'label' => 'MASTER_RELATION',
    'type' => 'text',
  ),
  'date' => 
  array (
    'label' => 'DATE',
    'type' => 'text',
  ),
  'create' => 
  array (
    'label' => 'CREATE',
    'type' => 'text',
  ),
  'id' => 
  array (
    'label' => 'ID',
    'type' => 'text',
  ),
  'active' => 
  array (
    'label' => 'ACTIVE',
    'type' => 'checkbox',
  ),
  'account_id' => 
  array (
    'label' => 'ACCOUNT_ID',
    'type' => 'text',
  ),
  'status' => 
  array (
    'label' => 'STATUS',
    'type' => 'text',
  ),
  'ballance' => 
  array (
    'label' => 'BALLANCE',
    'type' => 'text',
  ),
  'archived' => 
  array (
    'label' => 'ARCHIVED',
    'type' => 'text',
  ),
);
