<?php
return array (
  'doc_id' => 
  array (
    'order' => 10,
    'label' => 'DOC_ID',
  ),
  'reason_id' => 
  array (
    'order' => 20,
    'label' => 'REASON_ID',
  ),
  'qty' => 
  array (
    'order' => 30,
    'label' => 'QTY',
  ),
  'master_relation' => 
  array (
    'order' => 40,
    'label' => 'MASTER_RELATION',
  ),
  'date' => 
  array (
    'order' => 50,
    'label' => 'DATE',
  ),
  'account_id' => 
  array (
    'order' => 60,
    'label' => 'ACCOUNT_ID',
  ),
  'status' => 
  array (
    'order' => 70,
    'label' => 'STATUS',
  ),
  'ballance' => 
  array (
    'order' => 80,
    'label' => 'BALLANCE',
  ),
  'archived' => 
  array (
    'order' => 90,
    'label' => 'ARCHIVED',
  ),
);
