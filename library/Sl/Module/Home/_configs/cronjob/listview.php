<?php
return array (
  'name' => 
  array (
    'label' => 'Название',
  ),
  'minute' => 
  array (
    'label' => 'Минуты',
  ),
  'hour' => 
  array (
    'label' => 'Часы',
  ),
  'day' => 
  array (
    'label' => 'Дни',
  ),
  'month' => 
  array (
    'label' => 'Месяцы',
  ),
  'command' => 
  array (
    'label' => 'Команда',
  ),
  'description' => 
  array (
    'label' => 'Описание',
  ),
);
