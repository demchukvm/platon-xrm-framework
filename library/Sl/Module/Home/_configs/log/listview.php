<?php
return array (
  'field_name' => 
  array (
    'order' => 10,
    'label' => 'FIELD_NAME',
  ),
  'old_value' => 
  array (
    'order' => 20,
    'label' => 'OLD_VALUE',
  ),
  'new_value' => 
  array (
    'order' => 30,
    'label' => 'NEW_VALUE',
  ),
  'user_id' => 
  array (
    'order' => 40,
    'label' => 'USER_ID',
  ),
  'object_id' => 
  array (
    'order' => 50,
    'label' => 'OBJECT_ID',
  ),
  'action' => 
  array (
    'order' => 60,
    'label' => 'ACTION',
  ),
  'archived' => 
  array (
    'order' => 70,
    'label' => 'ARCHIVED',
  ),
);
