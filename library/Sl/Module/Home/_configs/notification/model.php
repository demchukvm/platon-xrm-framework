<?php
return array (
  'repeated' => 
  array (
    'label' => 'REPEATED',
    'type' => 'text',
  ),
  'repeat_data' => 
  array (
    'label' => 'REPEAT_DATA',
    'type' => 'text',
  ),
  'name' => 
  array (
    'label' => 'NAME',
    'type' => 'text',
  ),
  'data' => 
  array (
    'label' => 'DATA',
    'type' => 'text',
  ),
  'status' => 
  array (
    'label' => 'STATUS',
    'type' => 'text',
  ),
  'date_deadline' => 
  array (
    'label' => 'DATE_DEADLINE',
    'type' => 'timestamp',
  ),
  'date_start' => 
  array (
    'label' => 'DATE_START',
    'type' => 'timestamp',
  ),
);
