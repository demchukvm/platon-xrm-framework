<?php
return array (
  'description' => 
  array (
    'searchable' => true,
    'label' => 'Описание',
    'type' => 'text',
    'order' => 20,
  ),
  'create' => 
  array (
    'label' => 'Возникло',
    'type' => 'date',
    'searchable' => true,
    'order' => 30,
  ),
  'detail_description' => 
  array (
    'label' => 'Подробное описание',
    'type' => 'text',
    'order' => 40,
  ),
);
