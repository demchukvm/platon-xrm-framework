<?php
return array (
  'name' => 
  array (
    'label' => 'Название',
    'sortable' => true,
    'searchable' => true,
  ),
  'citycountry.name' => 
  array (
    'label' => 'Страна',
    'sortable' => true,
    'searchable' => true,
  ),
  'code' => 
  array (
    'label' => 'Код',
  ),
);
