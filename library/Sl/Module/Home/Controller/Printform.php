<?php

namespace Sl\Module\Home\Controller;

class Printform extends \Sl_Controller_Model_Action {

    public function ajaxprintformhelpAction() {

        $fullname = $this->getRequest()->getParam('name');
        ////////////////////////////формуємо масив властивостей моделі, яку обробляє прінтформа для виводу її на сторінку.    
        $name = explode(\Sl\Service\Helper::MODEL_ALIAS_SEPARATOR, $fullname); ////////////////////////////формуємо масив властивостей моделі, яку обробляє прінтформа для ви
        $model_name = $name[1];
        $module_name = $name[0];
        $model = \Sl_Model_Factory::object($model_name, $module_name);
        
        $pfid = $this->getRequest()->getParam('id', false);
        if($pfid) {
            $form = \Sl_Model_Factory::mapper('printform', \Sl_Module_Manager::getInstance()->getModule('home'))
                        ->find($pfid);
            if($form) {
                $printforms = array($form);
            }
        } else {
            $printforms = \Sl_Model_Factory::mapper('printform', \Sl_Module_Manager::getInstance()->getModule('home'))
                ->fetchAllByNameType(\Sl\Printer\Manager::type($model));
        }
        
        $fields_data = array();
        
        foreach($printforms as $form) {
            $form = \Sl_Model_Factory::mapper($form)->findRelation($form, 'printformfile');
            $printer = \Sl\Printer\Manager::getPrinter($form);  
            $printer->setCurrentObject($model);
            
            $event = new \Sl\Event\Printer ('beforePrintAction', array(
                'model' => $model,
                'printer' => $printer,
                'printform' => $form,
            ));
            \Sl_event_Manager::trigger($event);
            foreach(array_keys($event->getPrinter()->getPreparedData()) as $name) {
                if(false === array_search($name, $fields_data)) {
                    $fields_data[] = $name;
                }
            }
        }
        
        $template_names = array();
        
        $model_config = \Sl\Service\Config::read($model, 'form', \Sl\Service\Config::MERGE_FIELDS)->toArray();
        
        foreach($fields_data as $field_name) {
            $method = \Sl_Model_Abstract::buildMethodName($field_name, 'get');
            
            $priv_read = true;
            
            $type = (isset($model_config[$field_name]['type']) && ($model_config[$field_name]['type'] !== 'hidden'));
            $label = isset($model_config[$field_name]['label'])?$this->view->translate($model_config[$field_name]['label']):'';
            if(method_exists($model, $method)) {
                $priv_read = \Sl_Service_Acl::isAllowed(array(
                            $model,
                            $field_name
                ), \Sl_Service_Acl::PRIVELEGE_READ);
            } elseif(0 === strpos($field_name, \Sl_Modulerelation_Manager::RELATION_FIELD_PREFIX)) {
                $priv_read = \Sl_Service_Acl::isAllowed(array(
                            $model,
                            str_replace(\Sl_Modulerelation_Manager::RELATION_FIELD_PREFIX.\Sl_Modulerelation_Manager::RELATION_FIELD_SEPARATOR, '', $field_name)
                ), \Sl_Service_Acl::PRIVELEGE_READ);
            } elseif(0 === strpos($field_name, 'extras:')) {
                $type = true;
                if($this->view->translate('pf_extras_'.$field_name) !== 'pf_extras_'.$field_name) {
                    $label = $this->view->translate('pf_extras_'.$field_name);
                } else {
                    $label = $this->view->translate(strtoupper(str_replace('extras:', '', $field_name)));
                }
            }
            if($priv_read && $type && $label) {
                $template_names['%'.$field_name.'%'] = $label;
            }
        }
        /*
        $config_options = \Sl_Module_Manager::getInstance()
                ->getCustomConfig($module_name, 'detailed');

        if (!$config_options) {
            $config_options = \Sl_Module_Manager::getInstance()
                    ->getModule($module_name)
                    ->generateDetailedOptions();
        }


        $config_options = \Sl_Module_Manager::getInstance()
                ->getCustomConfig($module_name, 'detailed', $model_name);


        if (!$config_options) {
            $config_options = \Sl_Module_Manager::getInstance()
                    ->getModule($module_name)
                    ->generateDetailedOptions($model);
        }

        $config_options = \Sl_Module_Manager::getInstance()
                ->getCustomConfig($module_name, 'detailed', $model_name);

        $config_options = $config_options->toArray();
        $object = \Sl_Model_Factory::object($model_name, $module_name);
        $form_model_options = $object->describeFields();
        $copy_form_model_options = $form_model_options;


        $form_options = array_merge_recursive($config_options, $form_model_options);

        
        foreach ($form_options as $key => $option) {
            if (!(strpos($key, \Sl_Modulerelation_Manager::RELATION_FIELD_PREFIX) === 0)) {
                $priv_read = \Sl_Service_Acl::isAllowed(array(
                            $object,
                            $key
                                ), \Sl_Service_Acl::PRIVELEGE_READ);
                if (isset($option['label']) && ($option['type'] != 'hidden') && ($priv_read)) {
                    if (is_array($option['label'])) {
                        $template_names['%' . $key . '%'] = $this->view->translate(current($option['label']));
                    } else {
                        $template_names['%' . $key . '%'] = $this->view->translate($option['label']);
                    }
                }
            }
        }
        print_r(array(
            $template_names,
            $template_names2,
        ));die;*/
        $this->view->template_array = $template_names;
        $this->view->content = $this->view->render('partials/template_array.phtml');
        /////////////////// сформували массив у template_names    
    }

}