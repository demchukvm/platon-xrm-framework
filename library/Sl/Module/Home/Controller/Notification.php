<?php

namespace Sl\Module\Home\Controller;

class Notification extends \Sl_Controller_Model_Action {

    protected function _getMapper() {
        return \Sl_Model_Factory::mapper($this->_modelName, $this->_module);
    }

    public function ajaxgetnotificationsAction() {
        $result = true;
        try {
            switch ($this->getRequest()->getParam('subaction', false)) {
                case 'read': if ($ids = $this->getRequest()->getParam('ids', false)) {
                        if (is_array($ids) && count($ids)) {
                            $mess = $this->_getMapper()->fetchAll('id in ('.implode(',',$ids).')');
                            foreach($mess  as $m){
                                $this->_getMapper()->save($m->setStatus(1));
                            }

                        }
                    }
                    break;
                default: $user = \Sl_Service_Acl::getCurrentUser();
                    if ($user instanceof \Sl_Model_Abstract) {
                        $notifications = $this->_getMapper()->fetchByRelated($user->getId(), 'notificationuser');
                        foreach ($notifications as $n)
                            $mess[] = $n->toArray();

                        $this->view->messages = $mess;
                    }
            }
        } catch (\Exception $e) {
            $result = false;
            $this->view->error = $e->getMessage();
            $this->view->trace = $e->getTrace();
        }

        $this->view->result = $result;
    }

}
