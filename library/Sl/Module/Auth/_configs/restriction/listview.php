<?php
return array (
  'name' => 
  array (
    'label' => 'Название',
  ),
  'main_object' => 
  array (
    'label' => 'Объект ограничения',
  ),
  'type' => 
  array (
    'label' => 'Тип',
  ),
  'restrictionroles.name' => 
  array (
    'label' => 'Роли',
  ),
  'null_include' => 
  array (
    'label' => 'Включать непривязанные',
  ),
  'rules' => 
  array (
    'label' => 'Правила',
  ),
);
