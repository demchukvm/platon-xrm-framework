<?php
return array (
  'name' => 
  array (
    'order' => 10,
    'label' => 'NAME',
  ),
  'description' => 
  array (
    'order' => 20,
    'label' => 'DESCRIPTION',
  ),
  'archived' => 
  array (
    'order' => 30,
    'label' => 'ARCHIVED',
  ),
);
