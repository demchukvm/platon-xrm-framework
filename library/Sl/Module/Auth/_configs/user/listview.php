<?php
return array (
  'name' => 
  array (
    'label' => 'Имя',
    'searchable' => true,
    'sortable' => true,
  ),
  'phone' => 
  array (
    'label' => 'Контактный телефон',
  ),
  'login' => 
  array (
    'label' => 'Логин',
    'searchable' => true,
  ),
  'email' => 
  array (
    'label' => 'Email',
    'searchable' => true,
  ),
  'userroles.name' => 
  array (
    'label' => 'Роли',
    'searchable' => true,
  ),
);
