<?php
return array (
  'name' => 
  array (
    'label' => 'Name',
    'type' => 'text',
    'sort_order' => 10,
  ),
  'phone' => 
  array (
    'label' => 'Phone',
    'type' => 'text',
    'sort_order' => 15,
  ),
  'login' => 
  array (
    'label' => 'Login',
    'type' => 'text',
    'sort_order' => 27,
    'required' => true,
    'readonly' => true,
  ),
  'password' => 
  array (
    'label' => 'Password',
    'type' => 'hidden',
    'sort_order' => 30,
    'visible' => false,
    'options' => 
    array (
      'disabled' => 'disabled',
    ),
  ),
  'email' => 
  array (
    'label' => 'Email',
    'type' => 'text',
    'sort_order' => 25,
  ),
  'id' => 
  array (
    'label' => 'ID',
    'type' => 'hidden',
    'options' => 
    array (
      'readonly' => 'readonly',
    ),
  ),
  'active' => 
  array (
    'label' => 'Active',
    'type' => 'hidden',
    'sort_order' => 5,
  ),
  'create' => 
  array (
    'label' => 'CREATE',
    'type' => 'text',
  ),
  'archived' => 
  array (
    'label' => 'ARCHIVED',
    'type' => 'text',
  ),
  'blocked' => 
  array (
    'label' => 'Blocked',
    'type' => 'checkbox',
  ),
  'system' => 
  array (
    'label' => 'System',
    'type' => 'hidden',
  ),
  'color' => 
  array (
    'label' => 'Color',
    'type' => 'text',
  ),
);
