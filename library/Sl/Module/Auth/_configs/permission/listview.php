<?php
return array (
  'role_id' => 
  array (
    'order' => 10,
    'label' => 'ROLE_ID',
  ),
  'resource_id' => 
  array (
    'order' => 20,
    'label' => 'RESOURCE_ID',
  ),
  'privilege' => 
  array (
    'order' => 30,
    'label' => 'PRIVILEGE',
  ),
  'assert' => 
  array (
    'order' => 40,
    'label' => 'ASSERT',
  ),
  'archived' => 
  array (
    'order' => 50,
    'label' => 'ARCHIVED',
  ),
);
