<?php
return array (
  'role_id' => 
  array (
    'label' => 'ROLE_ID',
    'type' => 'text',
  ),
  'resource_id' => 
  array (
    'label' => 'RESOURCE_ID',
    'type' => 'text',
  ),
  'privilege' => 
  array (
    'label' => 'PRIVILEGE',
    'type' => 'text',
  ),
  'assert' => 
  array (
    'label' => 'ASSERT',
    'type' => 'text',
  ),
  'create' => 
  array (
    'label' => 'CREATE',
    'type' => 'text',
  ),
  'id' => 
  array (
    'label' => 'ID',
    'type' => 'text',
  ),
  'active' => 
  array (
    'label' => 'ACTIVE',
    'type' => 'checkbox',
  ),
  'archived' => 
  array (
    'label' => 'ARCHIVED',
    'type' => 'text',
  ),
);
