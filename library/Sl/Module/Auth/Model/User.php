<?php

namespace Sl\Module\Auth\Model;

class User extends \Sl_Model_Abstract {

    protected $_name;
    protected $_color;
    protected $_system;
    protected $_blocked;
    protected $_login;
    protected $_password;
    protected $_email;
    protected $_phone;

    public function setColor($color) {
        $this->_color = $color;
        return $this;
    }

    public function setSystem($system) {
        $this->_system = $system;
        return $this;
    }

    public function setBlocked($blocked) {
        $this->_blocked = $blocked;
        return $this;
    }

    public function fetchRelations() {
        return array('roles');
    }

    public function setName($name) {
        $this->_name = $name;
        return $this;
    }

    public function setPhone($phone) {
        $this->_phone = $phone;
        return $this;
    }

    public function setLogin($login) {
        $this->_login = $login;
        return $this;
    }

    public function setPassword($password) {
        $this->_password = $password;
        return $this;
    }

    public function setEmail($email) {
        $this->_email = $email;
        return $this;
    }

    public function getName() {
        return $this->_name;
    }

    public function getPhone() {
        return $this->_phone;
    }

    public function getLogin() {
        return $this->_login;
    }

    public function getPassword() {
        return $this->_password;
    }

    public function getEmail() {
        return $this->_email;
    }

    public function getResourceId() {
        return '';
    }

    public function getBlocked() {
        return $this->_blocked;
    }

    public function getSystem() {
        return $this->_system;
    }

    public function getColor() {
        return $this->_color;
    }
    
    public function findInitials() {
        $i = 0;
        $in = '';
        $f = explode(' ',trim(strlen($this->getName())?$this->getName():$this->getLogin()));
        if (count($f)){
            
            do {
               $in.= trim(substr($f[0],$i,1).substr($f[1],$i,1));
                
                
            } while (strlen($in) < 2 && strlen(trim(substr($f[0],++$i,1).substr($f[1],$i,1))));
        }
        return strtoupper(substr($in,0,2));
    }
    
    public function toArray($with_list_values = false, $recursive = false) {
        $data = parent::toArray($with_list_values, $recursive);
        if($with_list_values) {
            $data['initials'] = $this->findInitials();
        }
        return $data;
    }
}
