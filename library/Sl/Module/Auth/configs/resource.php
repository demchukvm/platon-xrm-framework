<?php
return array (
  'model' => 
  array (
    'name' => 
    array (
      'label' => 'NAME',
      'type' => 'text',
    ),
    'description' => 
    array (
      'label' => 'DESCRIPTION',
      'type' => 'text',
    ),
    'create' => 
    array (
      'label' => 'CREATE',
      'type' => 'text',
    ),
    'id' => 
    array (
      'label' => 'ID',
      'type' => 'text',
    ),
    'active' => 
    array (
      'label' => 'ACTIVE',
      'type' => 'checkbox',
    ),
    'archived' => 
    array (
      'label' => 'ARCHIVED',
      'type' => 'text',
    ),
  ),
);
