<?php
return array (
  'model' => 
  array (
    'name' => 
    array (
      'label' => 'Name',
      'type' => 'text',
    ),
    'parent' => 
    array (
      'label' => 'Parent',
      'type' => 'text',
    ),
    'description' => 
    array (
      'label' => 'Description',
      'type' => 'textarea',
    ),
    'id' => 
    array (
      'label' => 'ID',
      'type' => 'hidden',
    ),
    'active' => 
    array (
      'label' => 'ACTIVE',
      'type' => 'hidden',
    ),
    'create' => 
    array (
      'label' => 'CREATE',
      'type' => 'text',
    ),
    'archived' => 
    array (
      'label' => 'ARCHIVED',
      'type' => 'hidden',
    ),
  ),
);
