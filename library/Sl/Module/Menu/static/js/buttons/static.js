var bc_buttons_selector = 'div.page-header:first';
var bc_buttons_div_selector = 'div.btns';
var $bc_header;
var $bc_div;
var bc_top_position;
$(function() {
	
	$bc_header = $(bc_buttons_selector);
	$bc_div = $(bc_buttons_div_selector, $bc_header);
	if ($bc_header.length && $bc_div.length) {
		
		bc_top_position = 	parseInt($bc_header.offset().top);
		
		$(document).scroll(function(e) {
		
			var cur_height = bc_top_position - parseInt($(document).scrollTop());
			
			//console.log(cur_height);

			if (cur_height  < 0) {
				 $bc_div.addClass('fixed');
			} else {
				$bc_div.removeClass('fixed');
			}	

		});
	}

});
