var unlock_url = '/home/locker/ajaxunlockresource'
var date_format = '';

$(document).ready(function() {
    $('.archived_switcher').on('click', function() {
        $('#switch_archived').val($(this).attr('data-value'));
        $('table:first').dataTable().fnDraw();
        // return false;
    });

    $('#bcbuttonscreate').click(function() {
        document.location.href = $(this).data('rel');
    });

    var field = '#' + $('#bcbuttonscreatedate').attr('data-field');
    var field_clone = field + '_bcbuttons';
    if ($(field).parents('.control-group:first').length) {
        $(field).parents('.control-group:first').hide();
    } else {
        $(field).hide();
    }
    if ($(field_clone).attr('data-editable') == '1') {
        $(field_clone).datepicker({
            dateFormat: 'yy-mm-dd',
            changeYear: true,
            yearRange: '-65:+1',
            beforeShow: function() {
                setTimeout(function() {
                    $('.ui-datepicker').css('z-index', 3000);
                }, 0);
            }
        });
    } else {
        $(field_clone).attr('disabled', 'disabled');
    }
    $(field_clone).val(filterDate($(field).val()));
    $(field_clone).change(function() {
        $(field).val($(this).val());
    });

    $('#bcbuttonsarchive').click(function() {
        var title = '', message = '', $this = $(this);
        if ($this.attr('data-archived') == '0') {
            title = 'Архивирование записи';
            message = 'Вы уверены, что хотите отправить запись в архив?';
        } else {
            title = 'Разархивирование записи';
            message = 'Вы уверены, что хотите извлечь запись из архива?'
        }
        $.confirm(title, message, undefined, function() {
            $.ajax({
                type: 'POST',
                cache: false,
                url: $this.attr('data-rel'),
                data: {},
                success: function(data) {
                    if (data.result) {
                        var $dialog = $('<div>Подскажите, пожалуйста, что делать теперь ...</div>');
                        $dialog.dialog({
                            title: 'Что делать дальше',
                            modal: true,
                            width: '400',
                            height: '150',
                            buttons: {
                                0: {
                                    text: 'Остаться на странице',
                                    click: function() {
                                        $(this).dialog('close');
                                        reloadPage();
                                    }
                                },
                                1: {
                                    text: 'Вернуться к списку',
                                    click: function() {
                                        $(this).dialog('close');
                                        document.location.href = $this.attr('data-list-rel');
                                    }
                                }
                            }
                        })
                    } else {
                        $.alert(data.description);
                    }
                }
            });
        });
    });


    var changes = false;
    $('form:first').on('change', 'input, select, textarea', function() {
        changes = true;
    });
    $('#bcbuttonsback').click(function() {
        var $this = $(this);
        var goBack = function() {
            if ((is_iframe != undefined) && is_iframe) {
                window.parent.closeIframeFunction();
            } else {
                history.back();

            }
        }

        var unlock = function($el) {
            if ($el.attr('locker_resource') != undefined) {
                $.ajax({
                    type: 'POST',
                    cache: false,
                    url: unlock_url,
                    data: {
                        resource: $el.attr('locker_resource')
                    },
                    success: function(data) {
                        if (data.result) {
                            goBack();
                        } else {
                            $.alert(data.description);
                        }
                    }
                });
            } else {
                goBack();
            }
        }

        if (changes) {
            $.confirm('Отмена редактирования', 'Вы уверены, что хотите покинуть страницу? Все несохраненные данные будут потеряны', 'Ok', function() {
                unlock($this);
            });
        } else {
            unlock($this);
        }


    });


    $('#bcbuttonsdelete').click(function() {
        var title = '', message = '', $this = $(this);

        title = 'Delete record';
        message = 'Please, confirm action?'

        $.confirm(title, message, undefined, function() {
            $.ajax({
                type: 'POST',
                cache: false,
                url: $this.attr('data-rel'),
                data: {},
                success: function(data) {
                    if (data.result) {
                        document.location.href = $this.attr('data-list-rel');
                    } else {
                        $.alert(data.description);
                    }
                }
            });
        });
    });
    
    $('#bcbuttonsduplicate').click(function(){
        if (is_iframe != undefined && is_iframe) 
		{
			window.parent.closeIframeFunction();
		} else {
			var url = $(this).attr('data-rel');
			url = url!=undefined?url:'/';
			window.location.href=url;
		}
    });
    //bcbuttonemail
    $('ul.breadcrumb').on('click', '.emailbutton', function(e){
        
        closeIframeFunction = function() {
            $iframeDiv.dialog('close');
            $iframeDiv.remove();
        }
        
        var $a = $(this);
        url = $a.attr('rel');   
        //alert($a.attr('rel'));
        var request_obj = {};
 
        request_obj['is_iframe'] = 1;

        //var url = '/customers/main/ajaxgetemailpf';

        var $iframeDiv = $('<div />').css({
            width: 500,
            padding: 0,
        });
        var $iframe = $('<iframe />').attr({
            width: 490,
            height: 450
        }).css({
            border: 0
        }).attr('src', document.location.protocol + '//' + document.location.host + url + '?' + $.param(request_obj)).appendTo($iframeDiv);
        
        $('body').append($iframeDiv);
        
       //  var title = $(this).attr('data-title') || $(this).attr('title') || '';
	
	        $iframeDiv.dialog({
	            title: 'Форма отправки письма',
	            autoOpen: false,
	            width: 500,
	            height: 490,
	            modal: true,
	            resizable: false
	        });
	        $iframeDiv.dialog('open');
               
      
      
    });
    
     $('#bcbuttonsgoedit').click(function(){
        if((is_iframe != undefined) && is_iframe) {
			window.parent.closeIframeFunction();
		} else {
			var url = $(this).attr('data-rel');
			url = (url != undefined)?url:'/';
			window.location.href = url;
		}
    });
    
     $('#bcbuttonslog').click(function(){
        var url = $(this).attr('data-rel');
        var $iframeDiv = $('<div/>').iframePopup({
				url:document.location.protocol + '//' + document.location.host + url,
			});
		 closeIframeFunction = function() {
            $iframeDiv.dialog('close');
            $iframeDiv.remove();
        }
    });
    
    
    var field = '#'+$('#bcbuttonsreceivedate').attr('data-field');
    var field_clone = field+'_bcbuttons';
    if($(field).parents('.control-group:first').length) {
        $(field).parents('.control-group:first').hide();
    } else {
        $(field).hide();
    }
    if($(field_clone).attr('data-editable') == '1') {
        $(field_clone).datepicker({
            dateFormat:'yy-mm-dd',
            changeYear: true,
            yearRange: '-65:+1',
            beforeShow: function() {
                setTimeout(function(){
                    $('.ui-datepicker').css('z-index', 3000);
                }, 0);
            }
        });
    } else {
        $(field_clone).attr('disabled', 'disabled');
    }
    $(field_clone).val(filterDate($(field).val()));
    $(field_clone).change(function(){
        $(field).val($(this).val());
    });
    
    $('#status').change(function(){
        if(parseInt($(this).val()) < 10) {
            $(field_clone).val('');
        }
    });
    
      $('#bcbuttonsrefresh, .bcbuttonsrefresh').click(function(){
        $('table.datatable').each(function(){
            var ctrl = $(this).data('controller');
            if(ctrl) {
                ctrl.redrawTable();
            } else {
            	var $t = $(this); 
            	use_prev_data = true;
            	$('.dt_search_input',this).each(function(){
            		var $this = $(this);
			        var val = $this.val();
			        var ind = $this.attr('data-ind');
			        setCookie($this.attr('id') + document.location.href, val);
			        //console.log($this.attr('id')+document.location.href+' :: '+val);
			        table.fnFilter(val, ind);
            	});
            	use_prev_data = false;
            	$(this).dataTable().fnDraw();
            	
            }
        });
    });
    
     $('#bcbuttonsreturntoedit').click(function(){
        var url = $(this).attr('data-rel');

        if (is_iframe){
            window.location.href=url+'/is_iframe/1';
        } else {
            window.location.href=url; 
        }
    });
    
    $('#bcbuttonssaveandreturn').click(function(){
        var $this = $(this);
        if($('#bcbuttonssave').attr('disabled') === 'disabled') return;
        $('form:first').one('afterformvalidate.sl', function(){
            var $form = $(this); 
            $form.append('<input type="hidden" data-type="saveandreturn" name="form_after_save_url" value="'+$this.attr('data-rel')+'">');
            _setLoading(false);
        });
        $('form:first').one('formerror.sl', function(){
            var $form = $(this); 
            $form.find('[data-type="saveandreturn"]').remove();
            _setLoading(false);
           
        });
        _setLoading();
        $('#bcbuttonssave').trigger('click');
    });
    
    $(document).on('calculator_start', function(event, data) {
        if (data.wrapper != undefined) {
            _setLoading();
        }
    });

    $(document).on('calculator_finish', function(event, data) {
        if (data.wrapper != undefined) {
            _setLoading(false);
        }
    });
    
    $('#bcbuttonssave').click(function(){
        var $form = $('form:first');
        
        if(parseInt($form.attr('calculating')) > 0) {
            return;
        }
        
        if ($(this).attr('validate_action').length){
            var $save = $(this);
            if($save.attr('disabled') === 'disabled') return;
            $save.attr('disabled','disabled');
            
            $('.form-top-errors',$form).remove();
            _setLoading();
            $.ajax({
                type: 'POST',
                url: $(this).attr('validate_action'),
                data: $form.serialize(), 
                success: function(res) {
                    if(res.result){
                        $form.trigger('afterformvalidate.sl');
                        $form.submit();
                    } else {
                        showErrors(res.description,$form);
                        $('html,body').scrollTop(0);
                       // $form.trigger('validate.fail');
                        $form.trigger('formerror.sl');
                    }
                    _setLoading(false);
                    $save.removeAttr('disabled');
                },
                error: function(a,b,c){
                    $.alert('Validation error');
                    $save.removeAttr('disabled');
                    $form.trigger('formerror.sl');
                    _setLoading(false);
                     
                }
            });
        };
    });
    
     $('body').on('click',".dropdown-menu a.bcprintbutton", function(){
        var href;
        if (($(this).attr('href')) !=undefined){
            href = $(this).attr('href');
            $("div.dropdown.pull-right.open").removeClass("open");
            window.open( href );
            return false;

        }
        
    });
    
    
});


// for bcbuttonscreatedate
function filterDate(date) {
    var d = new Date();
    if (Date.parse(date) === NaN) {
        return '';
    }
    d.setTime(Date.parse(date));
    var da = [];
    da[0] = d.getFullYear() + '';
    da[1] = parseInt(d.getMonth() + 1) + ''; //Months are zero based
    da[2] = d.getDate() + '';
    if (da[1].length == 1) {
        da[1] = '0' + da[1];
    }
    if (da[2].length == 1) {
        da[2] = '0' + da[2];
    }
    return da.join('-');
}


/* //bcbuttonsreceivedate
function filterDate(date) {
    var d = new Date();
    var t = Date.parse(date);
    if(isNaN(t)) {
        return '';
    }
    console.log(Date.parse(date));
    d.setTime(Date.parse(date));
    var da = [];
    da[0] = d.getFullYear()+'';
    da[1] = parseInt(d.getMonth() + 1)+''; //Months are zero based
    da[2] = d.getDate()+'';
    if(da[1].length == 1) {
        da[1] = '0'+da[1];
    }
    if(da[2].length == 1) {
        da[2] = '0'+da[2];
    }
    return da.join('-');
}
*/

 var _setLoading = function(l) {
        if(l === false) {
            $('#bcbuttonssaveandreturn').removeClass('icon-loading').addClass('icon-floppy-disk');
        } else {
            $('#bcbuttonssaveandreturn').removeClass('icon-floppy-disk').addClass('icon-loading');
        }
    }