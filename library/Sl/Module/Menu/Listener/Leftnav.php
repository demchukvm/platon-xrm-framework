<?php
namespace Sl\Module\Menu\Listener;

interface Leftnav {
    
    public function onLeftNavPrepare(\Sl\Module\Menu\Event\Leftnav $event);
    
    
}
