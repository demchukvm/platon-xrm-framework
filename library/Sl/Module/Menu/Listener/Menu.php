<?php
namespace Sl\Module\Menu\Listener;

class Menu extends \Sl_Listener_Abstract implements \Sl_Listener_View_Interface, \Sl\Module\Menu\Listener\Pages, \Sl\Module\Menu\Listener\Leftnav {
   
    public function onNav(\Sl_Event_View $event) {
        $cache_id = 'c_page_menu_'.implode('_', \Sl_Service_Acl::getCurrentRoles(true)).'_'.($event->getView()->is_iframe?1:0);
        if(\Sl\Service\Cache::test($cache_id)) {
            $content = \Sl\Service\Cache::load($cache_id);
        } else {
            $content = '';
            $res = \Sl_Service_Acl::joinResourceName(array(
                'type' => \Sl_Service_Acl::RES_TYPE_MVC,
                'module' => 'menu',
                'controller' => 'main',
                'action' => 'sidenav',
            ));

            if(\Sl_Service_Acl::isAllowed($res)) {
                if (!$event->getView()->is_iframe){
                    $content = $event->getView()->action('sidenav', 'main', 'menu');
                }
            }
            \Sl\Service\Cache::save($content, $cache_id);
        }
        echo $content;
    }
    
    public function onAfterContent(\Sl_Event_View $event) {}
	
	public function onHeadLink(\Sl_Event_View $event) {}
	
    public function onBeforeContent(\Sl_Event_View $event) {
    	$res = \Sl_Service_Acl::joinResourceName(array(
            'type' => \Sl_Service_Acl::RES_TYPE_MVC,
            'module' => 'menu',
            'controller' => 'main',
            'action' => 'breadcrumb',
        ));
        
        if(\Sl_Service_Acl::isAllowed($res)) {
            if (!$event->getView()->is_iframe){
                echo $event->getView()->action('breadcrumb', 'main', 'menu');
            } else {
                echo $event->getView()->action('breadcrumb', 'main', 'menu', array('empty' => true));
            }
        }
		
    }

    public function onHeader(\Sl_Event_View $event) {}

    public function onLogo(\Sl_Event_View $event) {
        echo \Sl_Service_Settings::value('SYSTEM_NAME');
        
    }

    public function onPageOptions(\Sl_Event_View $event) {}

    public function onBodyBegin(\Sl_Event_View $event) {}

    public function onBodyEnd(\Sl_Event_View $event) {}

    public function onFooter(\Sl_Event_View $event) {}

    public function onHeadScript(\Sl_Event_View $event) {
        $event->getView()->headScript()->appendFile('/menu/context/contextmenu.js');
    }

    public function onHeadTitle(\Sl_Event_View $event) {}

    public function onContent(\Sl_Event_View $event) {
        
    }

    public function onBeforePageHeader(\Sl_Event_View $event) {
        
    }

    public function onPagesPrepare(\Sl\Module\Menu\Event\Pages $event) {
      
        $pages = array_merge_recursive($event->getPages(), array(
            
        ));
        
        $event->setPages($pages);
        
    }

    public function onLeftNavPrepare(\Sl\Module\Menu\Event\Leftnav $event) {
        $cache_id = 'c_page_leftnav'.implode('_', array(
            $event->getView()->is_iframe?1:0,
            implode('_', \Sl_Service_Acl::getCurrentRoles(true)),
        ));
        
        $res = \Sl_Service_Acl::joinResourceName(array(
            'type' => \Sl_Service_Acl::RES_TYPE_MVC,
            'module' => 'menu',
            'controller' => 'main',
            'action' => 'leftmenu',
        )); 
        if(\Sl_Service_Acl::isAllowed($res)) {
            if (!$event->getView()->is_iframe){
                if(\Sl\Service\Cache::test($cache_id)) {
                    $l = \Sl\Service\Cache::load($cache_id);
                } else {
                    $l = $event->getView()->action('leftmenu', 'main', 'menu');
                    \Sl\Service\Cache::save($l, $cache_id);
                }

                $sections = array_merge_recursive($event->getSections(), array(
                    $l
                ));
                $event->setSections($sections);
            }
        }
    }
}
