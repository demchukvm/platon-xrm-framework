<?php
namespace Sl\Module\Menu\Event;

class Leftnav extends \Sl_Event_View {
    
    protected $_sections;
    
    public function __construct($type, array $options = array()) {
        if(!isset($options['sections']) || !is_array($options['sections'])) {
            throw new \Exception('Param \'sections\' is required or must me an array.');
        }
        $this->setSections($options['sections']);
        parent::__construct($type, $options);
    }
    
    public function setSections(array $sections) {
        $this->_sections = $sections;
    }
    
    public function getSections() {
        return $this->_sections;
    }
}

