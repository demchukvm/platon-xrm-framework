<?php
namespace Sl\Module\Menu\Controller;

use Sl_Service_Acl as AclService;

class Main extends \Sl_Controller_Action {

    protected $_menu;
    protected $_left_menu;


    public function init() {
        
        $pages = $this->_prepareMenuPages();
        $l_pages = $this->_prepareLeftMenuPages();
        $this->_menu = new \Zend_Navigation($pages);
        $this->_left_menu = new \Zend_Navigation($l_pages);
       
        
        \Zend_Registry::set('Zend_Navigation', $this->_menu);
        
      
    }

    public function indexAction() {
        $this->view->menu = $this->_menu;
    }

    public function breadcrumbAction() {
        $empty = (bool) $this->getRequest()->getParam('empty', false);
        if ($empty) {
            $this->view->menu = new \Zend_Navigation(array(
                array(
                    'label' => '',
                    'type' => 'uri',
                    'href' => '#',
                    'resource' => '',
                )
            ));
        } else {
            $this->view->menu = $this->_menu;
        }
    }

    /**
     * Основное меню
     */
    public function sidenavAction() {
        
        $this->view->menu = $this->_menu;
    }

    
        
    
    /**
     * Меню пользователя
     */
    public function usernavAction() {
        $pages = array(array(
                'controller' => 'auth',
                'action' => 'logout',
                'label' => 'Выйти',
            ),);

        $menu = new \Zend_Navigation($pages);

        $this->view->menu = $menu;
    }

    protected function _prepareMenuPages(){
        $cache_id = 'menu';
        if(\Sl\Service\Cache::test($cache_id)) {
            $menu = \Sl\Service\Cache::load($cache_id);
        } else {
            $pages_configs = array();
            foreach (\Sl_Module_Manager::getInstance()->getModules() as $module){
                $pages_configs = array_merge($pages_configs, $module->getMenuPages());                
            }
            $menu = $this->_recBuildMenuArray($pages_configs);
            \Sl\Service\Cache::save($menu, $cache_id);
        }
        return $menu;
    }
    
    protected function _prepareLeftMenuPages(){
        $cache_id = 'menu_left';
        if(\Sl\Service\Cache::test($cache_id)) {
            $menu = \Sl\Service\Cache::load($cache_id);
        } else {
            $pages_configs = array();
            foreach (\Sl_Module_Manager::getInstance()->getModules() as $module){
                $pages_configs = array_merge($pages_configs, $module->getLeftMenuPages());                
            }
            $menu = $this->_recBuildMenuArray($pages_configs);
            \Sl\Service\Cache::save($menu, $cache_id);
        }
        return $menu;
        
    }
    
    protected function _recBuildMenuArray(array $pages, $parent = ''){
        $pages_assoc = array();
        foreach($pages as $page){
            $page['parent'] = isset($page['parent'])?$page['parent']:'';
            if ($page['parent'] == $parent){
                $order = isset($page['order'])?$page['order']:0;
                $page = $this->_buildPageNode($page, $pages);
                if (!isset($pages_assoc[$order]))$pages_assoc[$order] = array();
                $pages_assoc[$order][]=$page;
            }
            
        }
        ksort($pages_assoc);
        $pages = array();
        foreach($pages_assoc as $page_arr){
            $pages = array_merge($pages, $page_arr);    
        }
        return $pages;
        
    }
    
    protected function _buildPageNode(array $page, array $pages){
           $p = array();
           $p['label'] = $page['label'];
           $p['title'] = $page['label'];
           $p['route'] = $page['route'];
           
           if (isset($page['icon'])) $p['icon'] = $page['icon'];
           if (isset($page['nolabel'])) $p['nolabel'] = $page['nolabel'];
           
           if (isset($page['attribs'])) {$p['attribs'] = $page['attribs'];}
           $p['visible'] = isset($page['visible'])?(bool) $page['visible']:true;
           
           if(isset($page['module']) && 
                    isset($page['controller']) && 
                    isset($page['action'])) {
                        
               $p['module'] = $page['module'];
               $p['controller'] = $page['controller'];
               $p['action'] = $page['action'];
               $p['id'] = $page['id']?$page['id']:implode('.',array($page['module'],$page['controller'],$page['action']));
               $p['resource'] = (isset($page['resource']) && $page['resource'])?$page['resource']:\Sl_Service_Acl::joinResourceName(array('type'=>\Sl_Service_Acl::RES_TYPE_MVC, 
                                                                                          'module'=>$p['module'], 
                                                                                          'controller'=>$p['controller'],
                                                                                          'action'=>$p['action']));
               $p['privilege'] = isset( $page['privilege'])?$page['privilege']: (string) \Sl_Service_Acl::PRIVELEGE_ACCESS;                                                                                           
           } else {
             $p['id'] = $page['id'];    
             $p['type'] = 'uri';
             $p['href'] = isset($page['href'])?$page['href']:'#';
             $p['resource'] = (isset($page['resource']) && $page['resource'])?$page['resource']:null;  
           
           }
              $subpages = isset($p['id'])? $this-> _recBuildMenuArray($pages, $p['id']):array();
              if (strlen($page['parent']) && $p['action'] == \Sl\Service\Helper::LIST_ACTION){
                  $pp = $p;
                  $pp['action'] = \Sl\Service\Helper::CREATE_ACTION;
                  $pp['title'] = $pp['label'] = $this->view -> translate('Создать');
                  $pp['class']= 'create_model';
                  $pp['image']= '<i class="icon-plus"></i>' ;
                  $pp['id'] = implode('.',array($pp['module'],$pp['controller'],$pp['action']));
                  $pp['resource'] = \Sl_Service_Acl::joinResourceName(array('type'=>\Sl_Service_Acl::RES_TYPE_MVC, 
                                                                                          'module'=>$pp['module'], 
                                                                                          'controller'=>$pp['controller'],
                                                                                          'action'=>$pp['action']));
                  array_unshift($subpages, $pp); 
                                                                                                   
              }
               //\Sl\Service\Helper::CREATE_ACTION
              if (count($subpages)) $p['pages'] = $subpages;
              
              return $p;         
    }
    
    public function leftmenuAction() {
         $this->view->menu = $this->_left_menu;
    }
    
    public function leftnavAction() {
        
        $event = new \Sl\Module\Menu\Event\Leftnav('leftNavPrepare', array('sections' => array(), 'view'=>$this->view));
        \Sl_Event_Manager::trigger($event);
        $sections = $event->getSections();
       
       $this->_helper->viewRenderer('leftnav');  
       $this->view->sections = $sections;
       
    }
    
}
