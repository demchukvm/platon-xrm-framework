<?php
namespace Sl\Module\Comments;
class Module extends \Sl_Module_Abstract {
    
    const COMMENTED_MODELS_CONFIG_SECTION ='commented_models';
    
    public function commentedModelsArray() {
        
        $config = $this->section(self::COMMENTED_MODELS_CONFIG_SECTION);
        return $config?$config->toArray():array();
    }
    
    
    /**
     * Обновляет секцию зарегистрированных моделей конфига
     * 
     * @param array $relation_array Массив настроек связей
     * @return \Sl_Module_Abstract
     */
    public function updateCommentedModelsSection(array $models_array){
        
        $this->_saveModuleConfig($models_array,self::COMMENTED_MODELS_CONFIG_SECTION);
        
        return $this;
    }
    
    public function getListeners() {
        
         return array(
            array('listener' => new Listener\Commentsinformer($this), 
                  'order' => 10000),
         
         );
    }

    public function getModulerelations() {
        if (!($config_relations = $this -> section(parent::MODULERELATION_CONFIG_SECTION))) {
            $config_relations = $this -> _saveModuleConfig(array(), parent::MODULERELATION_CONFIG_SECTION);
        };

        return array_merge($config_relations -> toArray(), array(
             )
             );
    }

    public function getCalculators() {
        return array();
    }

}
