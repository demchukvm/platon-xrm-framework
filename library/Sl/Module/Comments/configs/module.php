<?php
return array (
  'modulerelations' => 
  array (
    0 => 
    array (
      'type' => '21',
      'db_table' => 'Sl\\Module\\Comments\\Modulerelation\\Table\\Commentuser',
    ),
  ),
  'commented_models' => 
  array (
    'logistic.package' => 
    array (
    ),
    'sales.order' => 
    array (
    ),
  ),
  'listview_options' => 
  array (
    'comment' => 
    array (
      'name' => 
      array (
        'order' => 10,
        'label' => 'NAME',
      ),
      'model' => 
      array (
        'order' => 20,
        'label' => 'MODEL',
      ),
      'model_id' => 
      array (
        'order' => 30,
        'label' => 'MODEL_ID',
      ),
      'field' => 
      array (
        'order' => 40,
        'label' => 'FIELD',
      ),
      'comment' => 
      array (
        'order' => 50,
        'label' => 'COMMENT',
      ),
      'archived' => 
      array (
        'order' => 60,
        'label' => 'ARCHIVED',
      ),
    ),
  ),
  'navigation_pages' => 
  array (
    0 => 
    array (
      'module' => 'comments',
      'controller' => 'main',
      'action' => 'setmodelcommented',
      'label' => 'Включить комментарии',
      'parent' => 'admin',
    ),
  ),
  'detailed' => 
  array (
    'comment' => 
    array (
      'name' => 
      array (
        'label' => 'NAME',
        'type' => 'text',
      ),
      'model' => 
      array (
        'label' => 'MODEL',
        'type' => 'text',
      ),
      'model_id' => 
      array (
        'label' => 'MODEL_ID',
        'type' => 'text',
      ),
      'field' => 
      array (
        'label' => 'FIELD',
        'type' => 'text',
      ),
      'comment' => 
      array (
        'label' => 'COMMENT',
        'type' => 'text',
      ),
      'create' => 
      array (
        'label' => 'CREATE',
        'type' => 'text',
      ),
      'id' => 
      array (
        'label' => 'ID',
        'type' => 'text',
      ),
      'active' => 
      array (
        'label' => 'ACTIVE',
        'type' => 'checkbox',
      ),
      'archived' => 
      array (
        'label' => 'ARCHIVED',
        'type' => 'text',
      ),
    ),
  ),
);
