<?php
return array (
  'name' => 
  array (
    'label' => 'NAME',
    'type' => 'text',
  ),
  'model' => 
  array (
    'label' => 'MODEL',
    'type' => 'text',
  ),
  'model_id' => 
  array (
    'label' => 'MODEL_ID',
    'type' => 'text',
  ),
  'field' => 
  array (
    'label' => 'FIELD',
    'type' => 'text',
  ),
  'comment' => 
  array (
    'label' => 'COMMENT',
    'type' => 'text',
  ),
  'create' => 
  array (
    'label' => 'CREATE',
    'type' => 'text',
  ),
  'id' => 
  array (
    'label' => 'ID',
    'type' => 'text',
  ),
  'active' => 
  array (
    'label' => 'ACTIVE',
    'type' => 'checkbox',
  ),
  'archived' => 
  array (
    'label' => 'ARCHIVED',
    'type' => 'text',
  ),
  'master_relation' => 
  array (
    'label' => 'MASTER_RELATION',
    'type' => 'text',
  ),
);
