<?php

return array(
    '_default' =>
    array(
        'fields' =>
        array(
            0 => 'name',
            1 => 'model',
            2 => 'model_id',
            3 => 'field',
            4 => 'comment',
        ),
        'name' => '_default',
        'label' => 'По-умолчанию',
    ),
    '_popup' =>
    array(
        'fields' =>
        array(
            0 => 'name',
        ),
        'name' => '_popup',
        'label' => 'По-умолчанию',
        'type' => 'popup',
    ),
);
