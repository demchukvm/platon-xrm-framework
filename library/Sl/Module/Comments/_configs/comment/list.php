<?php
return array (
  'name' => 
  array (
    'order' => 10,
    'label' => 'NAME',
  ),
  'model' => 
  array (
    'order' => 20,
    'label' => 'MODEL',
  ),
  'model_id' => 
  array (
    'order' => 30,
    'label' => 'MODEL_ID',
  ),
  'field' => 
  array (
    'order' => 40,
    'label' => 'FIELD',
  ),
  'comment' => 
  array (
    'order' => 50,
    'label' => 'COMMENT',
  ),
  'archived' => 
  array (
    'order' => 60,
    'label' => 'ARCHIVED',
  ),
);
