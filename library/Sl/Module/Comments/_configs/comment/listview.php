<?php
return array (
  'name' => 
  array (
    'label' => 'NAME',
  ),
  'model' => 
  array (
    'label' => 'MODEL',
  ),
  'model_id' => 
  array (
    'label' => 'MODEL_ID',
  ),
  'field' => 
  array (
    'label' => 'FIELD',
  ),
  'comment' => 
  array (
    'label' => 'COMMENT',
  ),
  'archived' => 
  array (
    'label' => 'ARCHIVED',
  ),
);
