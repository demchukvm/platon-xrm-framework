<?php
namespace Sl\Module\Comments\Modulerelation\Table;

class Commentuser extends \Sl\Modulerelation\DbTable {
	protected $_name = 'comments_comment_user';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Sl\Module\Comments\Model\Comment' => array(
			'columns' => 'comment_id',
			'refTableClass' => 'Sl\Module\Comments\Model\Table\Comment',
		'refColums' => 'id'),
                		'Sl\Module\Auth\Model\User' => array(
			'columns' => 'user_id',
			'refTableClass' => 'Sl\Module\Auth\Model\Table\User',
				'refColums' => 'id'	),
	);
}