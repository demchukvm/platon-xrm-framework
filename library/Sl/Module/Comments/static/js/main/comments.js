var create_comment_url = '/comments/comment/ajaxcreate';
var c_scrolled = false;

var commentsline_mouseover = false;

var scroll_comments_line, fields_comments_mouseover;
$(function(){
	if (informer !=undefined){
		var comments_informer_data = {
			 id: 'comments_informer_data', 
			 getRequestData : {model_alias:comments_model_alias, model_id:comments_model_id},
			 update : update_comments_informer,
			 period : 30 
			
		}
		informer.addInformerAutorequest(comments_informer_data); 
	}
	//where is mouse flag:
	$('#commentsline').mouseenter(function(){
	    commentsline_mouseover = true;
	}).mouseleave(function(){
	    commentsline_mouseover = false;
	});
	
	$('#commentsline').on('mouseenter','.simple-comment',function(){
	    if ($(this).data('field') && $('#'+$(this).data('field')).length) $('#'+$(this).data('field')).addClass('fire-commented');
	});
	$('#commentsline').on('mouseleave','.simple-comment',function(){
	
	    $('.fire-commented').removeClass('fire-commented');
	});
	
	
	//send new comment in informer
	
	$('body').on('click','#send_new_comment', function(){
		var content =  $('#create_new_comment').val().trim();
		if (content.length){
			create_new_comment(comments_model_alias,comments_model_id, content);
			
		}
	});
	
	//send new comment in comments div
	$('#fields_comments_block').on('click','.send_new_comment', function(){
		var content =  $('#fields_comments_block .create_new_comment').val().trim();
		if (content.length){
			create_new_comment(comments_model_alias,comments_model_id, content, $('#fc-field').val());
			
		}
	});
	
	// auto scroll informer after show
	$(document).on('informer.item.click',function(e, obj){if ($(obj).is('.open > #comments_informer')) scroll_comments_line()});
	
	//show field comments
	
	var $fc = $('#fields_comments_block');
	$fc.appendTo('body');
	
	$('.commentsline',$fc).mouseenter(function(){
	    fields_comments_mouseover = true;
	}).mouseleave(function(){
	    fields_comments_mouseover = false;
	});
	
	$('textarea.create_new_comment').keypress(function(e){
    if (e.ctrlKey && e.keyCode == 10) {
        $(this).next('button.send_new_comment').click();
    }
	});
	
		
	$('input[class^="field-"], select[class^="field-"], textarea[class^="field-"]','form').on('mouseenter', function(){
		
		if ($(this).is('.form_list *') || !$(this).attr('id').length || $fc.is('.open')) return;
		
		var os = $(this).offset();
		if ($fc.attr('rel')!=$(this).attr('id').length){
			$fc.removeClass('open');
			get_comments_by_field($fc, $(this).attr('id'));
		}
		$fc.stop().css({top:os.top+$(this).outerHeight() -2, left:os.left+$(this).width()-20, opacity:1}).show();
	}).on('mouseleave', function(){
		if ($fc.is(':not(.open)'))$fc.fadeOut();
	});
	
	$fc.on('mouseenter',function(){
		$(this).stop().css('opacity',1).show();
	}).on('mouseleave', function(){
		if ($(this).is(':not(.open)')) $(this).fadeOut();
	});
	
	$('.field-comments-counter',$fc).click(function(){
		$fc.toggleClass('open');
		if (($fc.offset().left + $fc.width()) > $('body').width()) $fc.css({left:'', right:0});
		scroll_comments_line();
	});
	
	$fc.on('click','.close-ctrl', function(){
		$fc.toggleClass('open');		
	});
	
	
});

var get_comments_by_field = function($fc, field){
	var c = $('.simple-comment.field-'+field, $('#commentsline'));
	
	$('.fc-commentsline',$fc).html(comments_informer_default_string);
	$('.field-comments-counter',$fc).html(c.length);
	var fieldname = ($('#'+field).attr('placeholder') && $('#'+field).attr('placeholder').length)?$('#'+field).attr('placeholder'):field;
	var f_fieldname = fieldname.length > 23?fieldname.substr(0,23)+'&hellip;':fieldname;
	$('.field-title', $fc).attr('title',fieldname).html(f_fieldname);
	$('#fc-field').val(field);
	if (c.length){
		$('.fc-commentsline',$fc).html('');
		$fc.attr('rel',field);
		c.each(function(){
			 
			$($(this).clone(false)).appendTo($('.fc-commentsline',$fc));
			
		});
		
	} else {
		
	}
}

var update_comments_informer = function(data){
	
	$('#commentsline').html(comments_informer_default_string);
	if ((typeof data == "object") && (data instanceof Array)){
		$('#commentsline').html('');
		for (i in data){
			add_comment_line($('#commentsline'),data[i]);
		}
		if ($('#fields_comments_block').is('.open')){
			get_comments_by_field($('#fields_comments_block'), $('#fc-field').val());
		}
		
	}
	$('#comments_count').html(data.length);
	if (data.length){
		$('#comments_count:parent').parent().show();
	} else {
		$('#comments_count').parent().hide();
	}
	scroll_comments_line();
	//$('#commentsline').html(comments_content);
}



var add_comment_line = function($wrapper,data){
	$wrapper
	var $c_div = $('<div />').addClass('simple-comment').addClass('field-'+data.field).attr('data-field',data.field);
	var $f = $('#'+data.field);
	var f_ph =  ($f.length&&$f.attr('placeholder').length)?$f.attr('placeholder').trim():data.field.trim();
	f_ph =  f_ph.length > 35?f_ph.substr(0,35)+'&hellip;':f_ph;
	
	var title = ((data.user !=undefined && data.user.length)? data.user:'?')+(f_ph.length?' <small class="fn">'+ f_ph+'</small>':'');
	var d = data.create.split(/[:\-\s]/);
	
	$('<strong/>').html(title).prepend($('<small/>').html(d[2]+'/'+d[1]+' '+d[3]+':'+d[4])).appendTo($c_div);
	
	
	$('<div/>').html(data.comment.replace(/([^>])\n/g, '$1<br/>')).appendTo($c_div);
	$c_div.appendTo($wrapper);
	if (data.field.length){
		
	}
	//scroll_comments_line();
			
}

scroll_comments_line = function(){
	if (!commentsline_mouseover){
		var cl    = $('#commentsline');
		var height = cl[0].scrollHeight;
		if (cl.is(':visible')) 	cl.scrollTop(height,1000);
	}
	if (!fields_comments_mouseover){
		var cl    = $('#fields_comments_block .fc-commentsline');
		var height = cl[0].scrollHeight;
		if (cl.is(':visible')) cl.scrollTop(height,1000);
		
	} else {
		
	}
}

var create_new_comment = function (alias, id, content,field){
	$('.create_new_comment').attr('disable','disable').addClass('ui-autocomplete-loading');
			$.post(create_comment_url, {comment:content, model:alias, model_id:id, field:field}, function(data){
				if (data.result){
					$('.create_new_comment').val('');
					add_comment_line($('#commentsline'), data.data);
					if ($('#fields_comments_block').is('.open')){
						get_comments_by_field($('#fields_comments_block'), $('#fc-field').val());
						
					}
				} else {
					alert(data.description);
				}
			}).done(function(){
				$('.create_new_comment').removeAttr('disable').removeClass('ui-autocomplete-loading');	
				scroll_comments_line();
			});
}
