<?php
namespace Sl\Module\Comments\Controller;

class Comment extends \Sl_Controller_Model_Action {

    public function ajaxcreateAction() {

        $this -> view -> result = true;
        if ($this -> getRequest() -> isPost()) {
            try {
                $Obj = \Sl_Model_Factory::object($this -> getModelName(), $this -> _getModule());
                if ($current_user = \Zend_Auth::getInstance() -> getIdentity()) {
                    $Obj -> assignRelated($Obj -> getMasterRelation(), array($current_user));
                }
                $Obj -> setOptions($this -> getRequest() -> getParams());
                $Obj = \Sl_Model_Factory::mapper($Obj) -> save($Obj, true);
                $this -> view -> data = $Obj -> toArray();
            } catch (Exception $e) {
                $this -> view -> result = false;
                $this -> view -> description = $e -> getMessage() . ($e -> getPrevious() ? PHP_EOL . $e -> getPrevious() -> getMessage() : '');
            }
        }
    }

}
