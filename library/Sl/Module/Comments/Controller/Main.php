<?php
namespace Sl\Module\Comments\Controller;

class Main extends \Sl_Controller_Action {

    public function setmodelcommentedAction() {

        if ($this -> getRequest() -> isPost()) {

            $model = $this -> getRequest() -> getParam('modelname');
            $module = $this -> getRequest() -> getParam('modulename');
            //commentedModelsArray
            $config_array = $this -> _getModule() -> commentedModelsArray();
            $alias = \Sl\Service\Helper::getModelAlias($model, $module);

            if ($this -> getRequest() -> getParam('value', false)) {
                if (!isset($config_array[$alias])) {
                    $config_array[$alias] = array();
                }

            } else {
                unset($config_array[$alias]);

            }
            $this -> _getModule() -> updateCommentedModelsSection($config_array);
            die();
        }

        $modules = \Sl_Module_Manager::getModules();
        $modules_resources = array();
        $models = array();
        $modules_list = array();
        $models_arr = \Sl_Module_Manager::getAvailableModels();
        $modules = array();
        $commented_config = $this -> _getModule() -> commentedModelsArray();

        foreach ($models_arr as $module => $models) {
            $modules[$module] = array();
            foreach ($models as $model) {

                $modules[$module][$model] = isset($commented_config[\Sl\Service\Helper::getModelAlias($model, $module)]);

            }

        }

        $this -> view -> modules = $modules;

    }

}
