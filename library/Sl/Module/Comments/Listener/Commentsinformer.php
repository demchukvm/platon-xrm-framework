<?php

namespace Sl\Module\Comments\Listener;

class Commentsinformer extends \Sl_Listener_Abstract implements \Sl_Listener_View_Interface, 
                                                                \Sl\Listener\View\Informer\Informer,
                                                                \Sl\Listener\Informer\InformerListener, 
                                                                \Sl_Listener_Model_Interface,
                                                                \Sl_Listener_Router_Interface {
    //Виставляється тільки якщо треба видавати коментарі
    protected $_model_alias = false;
    protected $_model_id = false;
    protected $_create_access = false;


    public function onInformerRequest(\Sl_Event_Informer $event) {
        $informer_id = 'comments_informer_data';
        if ($request = $event->getInformer()->getRequest($informer_id)){
            
            if (($alias = $request['model_alias']) && ($id = $request['model_id'])){
                $comments_arr = array();    
                $comments = \Sl_Model_Factory::mapper('comment','comments')-> fetchAll(" model like '{$alias}' AND model_id = {$id}");
                foreach ($comments as $comment){
                    $comments_arr[] = $comment->toArray();
                }
                if (is_array($comments)){
                    $event->getInformer()->setAnswer($informer_id, $comments_arr);
                } 
            }
            
            
        }
        
    }

    

    public function onRouteStartup(\Sl_Event_Router $event) {
    }
    
    
    
    public function onRouteShutdown(\Sl_Event_Router $event) {
            
        
        if (($module = $event -> getRequest() -> getParam('module', false)) && 
            ($model = $event -> getRequest() -> getParam('controller', false)) &&
            ($id = $event -> getRequest() -> getParam('id', false)) && 
            in_array($event -> getRequest() -> getParam('action', false), array(\Sl\Service\Helper::TO_EDIT_ACTION,\Sl\Service\Helper::TO_DETAILED_ACTION)) ) {
            $alias = \Sl\Service\Helper::getModelAlias($model, $module);
            $commented_models = $this -> getModule() -> commentedModelsArray();
           
            if (isset($commented_models[$alias])) {

                $resource = \Sl_Service_Acl::joinResourceName(array(
                    'type' => \Sl_Service_Acl::RES_TYPE_MVC,
                    'module' => 'comments',
                    'controller' => 'comment',
                    'action' => \Sl\Service\Helper::LIST_ACTION
                ));
                $priv_access = \Sl_Service_Acl::isAllowed($resource, \Sl_Service_Acl::PRIVELEGE_ACCESS);
    
                if ($priv_access) {
                    $this -> _model_alias = $alias;
                    $this -> _model_id = $id;

                    $resource = \Sl_Service_Acl::joinResourceName(array(
                        'type' => \Sl_Service_Acl::RES_TYPE_MVC,
                        'module' => 'comments',
                        'controller' => 'comment',
                        'action' => \Sl\Service\Helper::AJAX_CREATE_ACTION
                    ));
                    $this->_create_access = \Sl_Service_Acl::isAllowed($resource, \Sl_Service_Acl::PRIVELEGE_ACCESS);

                }

            }
        }
    }

    public function onPreDispatch(\Sl_Event_Router $event) {
    }

    public function onPostDispatch(\Sl_Event_Router $event) {
    }

    public function onDispatchLoopStartup(\Sl_Event_Router $event) {
    }

    public function onDispatchLoopShutdown(\Sl_Event_Router $event) {
    }

    public function onGetRequest(\Sl_Event_Router $event) {
    }

    public function onGetResponse(\Sl_Event_Router $event) {
    }

    public function onSetRequest(\Sl_Event_Router $event) {
    }

    public function onSetResponse(\Sl_Event_Router $event) {
    }

    public function onBeforeInit(\Sl_Event_Router $event) {
    }

    public function onAfterInit(\Sl_Event_Router $event) {
    }

    public function onAfterContent(\Sl_Event_View $event) {

    }

    public function onBeforeContent(\Sl_Event_View $event) {

    }

    public function onBeforePageHeader(\Sl_Event_View $event) {

    }

    public function onBodyBegin(\Sl_Event_View $event) {

    }

    public function onBodyEnd(\Sl_Event_View $event) {

    }

    public function onContent(\Sl_Event_View $event) {

    }

    public function onFooter(\Sl_Event_View $event) {

    }

    public function onHeadLink(\Sl_Event_View $event) {
        if ($this -> _model_alias) {
            $event -> getView() -> headLink() -> appendStylesheet('/comments/main/comments.css');
            
        }
    }

    public function onHeadScript(\Sl_Event_View $event) {
        if ($this -> _model_alias) {
                
            $event -> getView() -> headScript() -> appendFile('/comments/main/comments.js');
            
            echo '<script>var comments_model_alias ="' . $this -> _model_alias . '"; 
                          var comments_model_id='.$this -> _model_id.';
                          var comments_create_access='.intval($this->_create_access).';
                          var comments_informer_default_string = "'.$event -> getView()->translate('Здесь пока нет комментариев').'";
                          </script>';
            
        }
    }

    public function onHeadTitle(\Sl_Event_View $event) {

    }

    public function onHeader(\Sl_Event_View $event) {

    }

    public function onLogo(\Sl_Event_View $event) {

    }

    public function onNav(\Sl_Event_View $event) {

    }

    public function onPageOptions(\Sl_Event_View $event) {

    }

    public function onInformer(\Sl_Event_View $event) {
    
        if ((bool) $this -> _model_alias) {
            $event -> getView() -> addScriptPath();
            $view = $event -> getView();
            $view -> create_access = $this->_create_access;
            
            if ($event -> getView() -> is_iframe)
                return;

            
                $informer = new \Sl\Module\Home\Informer\Item();
                    
                $informer -> setTitle($event -> getView() -> translate('Комментарии'))
                                       -> setIcon('glyphicons_244_conversation.png') 
                                       -> setShowBadge(true)
                                       -> setBadgeDefaultVisibility(false)
                                       -> setId('comments_informer') 
                                       -> setBadge('<span id="comments_count">0</span>') 
                                       -> setContent($view -> render('main/commentsline.phtml'));
                    ;
                $event -> getView() -> informer_items[] = $informer;
                
                
            
        }

    }

    public function onAfterSave(\Sl_Event_Model $event) {
       // error_reporting(E_ALL);
        $comment = $event->getModel();
        if ($comment instanceof \Sl\Module\Comments\Model\Comment) {
            $cur_user =  \Sl_Service_Acl::getCurrentUser();
            $cur_user_email = $cur_user->getEmail();
            $emails = \Sl_Model_Factory::mapper($comment)
                    ->getUserCommentedEmails($comment);
            $obj = \Sl\Service\Helper::getModelByAlias($comment->getModel());
            $cur_obj = \Sl_Model_Factory::mapper($obj)->find($comment->getModelId());
           
            if ($cur_obj->isLoged()) {
                $field = 'user_id';
                $user_ids = \Sl_Model_Factory::mapper('log', 'home')
                        ->getObjectEditorsIds($cur_obj, $field);
                $user_emails = array();
                $user = \Sl_Model_Factory::object('user', 'auth');
                foreach ($user_ids as $id) {
                    $c_user = \Sl_Model_Factory::mapper($user)->find($id);
                    $user_emails[] = $c_user->getEmail();
                }
            }
            foreach ($emails as $key => $email) {
                if (in_array($email, $user_emails)) {
                    unset($emails[$key]);
                }
            }
            $email_list = array_merge($emails, $user_emails);
            foreach($email_list as $key => $email){
                if ($email == $cur_user_email){
                    unset($email_list[$key]);
                }
            }
            if(count($email_list)){
            $pfid = \Sl_Service_Settings::value('USERCOMMENTS_PRINTFORM_ID');
            $form = \Sl_Model_Factory::mapper('printform', \Sl_Module_Manager::getInstance()->getModule('home'))->find($pfid);
            if (!$form) {
                \Sl\Module\Home\Service\Errors::addError('USERCOMMENTS_PRINTFORM_ID', $translator->translate('Can\'t find needed subform.'));
                //throw new \Exception($translator->translate('Can\'t find needed subform.') . __METHOD__);
            }
            $mail = new \Zend_Mail('UTF-8');
            foreach ($email_list as $key => $email) {
                $mail->addTo($email);
            }
            $field = $comment->getField(); 
            if (strlen(trim($field)) ){
                $label = strlen($cur_obj->describeField($field, true)->label)?$cur_obj->describeField($field, true)->label:$field;
                $field_name = $this->getTranslator()->translate('в поле ').$label;
            } else{
                $field_name = '';
            }
            $printer = \Sl\Printer\Manager::getPrinter($form);
            $printer->setCurrentObject($comment);
            $printer->addExtras(array(
                'username'   => $cur_user->getName(),
                'identifier' => $cur_obj . '',
                'url' => \Sl_Service_Settings::value('BASE_URL').\Sl\Service\Helper::modelEditViewUrl($cur_obj),
                'field' => $field_name,
            ));
            try {
                $filename = '/tmp/' . md5($comment->getId());
                $printer->printIt(null, null, $filename);
            } catch (\Exception $e) {
                
            }
            $message = file_get_contents($filename);
            unlink($filename);
            $mail->setBodyHtml($message);
            $mail->send();
            }  
        }//print_r('adfaDS');die;
    }

    public function onBeforeSave(\Sl_Event_Model $event) {
        
    }

}
