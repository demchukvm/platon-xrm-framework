<?php
namespace Sl\Module\Comments\Model\Mapper;

class Comment extends \Sl_Model_Mapper_Abstract {
	protected function _getMappedDomainName() {
        return '\Sl\Module\Comments\Model\Comment';
    }

    protected function _getMappedRealName() {
        return '\Sl\Module\Comments\Model\Table\Comment';
    }
    
    public function getUserCommentedEmails(\Sl_Model_Abstract $comment){
        
       $alias = $comment->getModel();
       $model_id = $comment->getModelId();
       //$alias ='logistic.package';
       //$model_id = '1269'; 
       $commentuser = \Sl_Modulerelation_Manager::getRelations(\Sl_Model_Factory::object($comment), 'commentuser');
       $emails = $this->_getDbTable()->getUserCommentedEmails($alias, $model_id, $commentuser);  
       return $emails;
        
    }
    
}