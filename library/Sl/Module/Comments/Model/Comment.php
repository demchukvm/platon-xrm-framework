<?php
namespace Sl\Module\Comments\Model;

class Comment extends \Sl_Model_Abstract implements \Sl\Model\Masterrelation{

	protected $_name;
	protected $_model;
	protected $_model_id;
	protected $_field;
	protected $_comment;
    protected $_loged = true;
    const MASTER_RELATION = 'commentuser';
    
    
    public function getMasterRelation (){
        return self::MASTER_RELATION;
    }
    public function setMasterRelation ($master_relation){
        
    }
	
    public function toArray(){
        $array = parent::toArray();
        if (count($this->fetchRelated(self::MASTER_RELATION))){
            $user = $this->fetchOneRelated(self::MASTER_RELATION);
            
            $array['user'] = is_object($user )?$user ->__toString():$user ;
        }
        return $array;
    }
    
	public function setName ($name) {
		$this->_name = $name;
		return $this;
	}
	public function setModel ($model) {
		$this->_model = $model;
		return $this;
	}
	public function setModelId ($model_id) {
		$this->_model_id = $model_id;
		return $this;
	}
	public function setField ($field) {
		$this->_field = $field;
		return $this;
	}
	public function setComment ($comment) {
		$this->_comment = $comment;
		return $this;
	}

	public function getName () {
		return $this->_name;
	}
	public function getModel () {
		return $this->_model;
	}
	public function getModelId () {
		return $this->_model_id;
	}
	public function getField () {
		return $this->_field;
	}
	public function getComment () {
		return $this->_comment;
	}



}