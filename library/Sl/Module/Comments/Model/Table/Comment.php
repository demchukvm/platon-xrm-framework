<?php
namespace Sl\Module\Comments\Model\Table;

class Comment extends \Sl\Model\DbTable\DbTable {
	protected $_name = 'comments_comment';
	protected $_primary = 'id';
        
                

        public function getUserCommentedEmails($alias, $model_id, \Sl\Modulerelation\Modulerelation $commentuser){
            
            $select = $this->getAdapter()->select();
            $select ->from($this->_name, array());
            
            $comment = \Sl_Model_Factory::object($this);
            $user = $commentuser->getRelatedObject($comment);
            $emailcol = $commentuser->getName().'.email';
            $select = $this->_buildInnerJoin($select, $user, $commentuser, array('email' => 'distinct('.$emailcol.')'));
            
            $select->where('comments_comment.model = "'.$alias.'"');
            $select->where('comments_comment.model_id ="'.$model_id.'"');
            
            return $this->getAdapter()->fetchCol($select);
        
    }
        
        
}

