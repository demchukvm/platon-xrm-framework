<?php

namespace Sl\Service;

class CalendarConfig {

    const CONFIG_PATH_SEPARATOR = '/';
    const FILE_NAME = '../application/configs/calendarsettings.php';

    public static function read($alias = FALSE) {


        $filename = self::FILE_NAME;

        if (!file_exists($filename)) {
            throw new \Exception('File "' . $filename . '" doesn\'t exists. ' . __METHOD__);
        }
        try {
            $config = new \Zend_Config(require $filename, true);
            if ($config) {
                if ($alias) {
                    return $config->$alias;
                } else {
                    return $config;
                }
            }
            
        } catch (\Exception $e) {
          echo $e->getMessage();
        }
    }
}