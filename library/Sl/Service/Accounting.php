<?php

namespace Sl\Service;

class Accounting {

    protected static $_instance;
    protected static $_config_path;
    protected static $_config;
    protected static $_acntcreation_rels;

    const ACCOUNTINGS_MODELS_KEY = 'accountings';
    const TX_SUBKEY = 'tx';
    const ACNT_SUBKEY = 'acnt';
    const DEAL_MODELS_KEY = 'deal_models';
    const DOCS_MODELS_KEY = 'docs_models';
    const ACNTS_KEY = 'acntcreation';
    const DOCUMENTING_KEY = 'documenting';
    const TXS_KEY = 'transactioning';
    const FIELDS_KEY = 'fields';
    const DOCUMENTING_AUTOCREATE_KEY = 'autocreate';

    public function __construct() {
        $this->getConfig();
    }

    /**
     *
     * @return Zend_Config
     */
    public function getConfig() {
        if (!isset($this->_config)) {
            try {
                $this->_config = new \Zend_Config(require self::getConfigPath(), true);
            } catch (Exception $e) {
                die('Can\'t read modules config file');
            }
        }
        return $this->_config;
    }

    /** Повертає список зв'язків, по яким автоматично будуються Acnt-и

     */
    public function getActncreationRelations() {
        if (!is_array(self::$_acntcreation_rels)) {
            $config = self::config(self::ACNTS_KEY);
            $acntcreation_rels = array();
            $config = is_array($config) ? $config : $config->toArray();

            foreach ($config as $model => $rels) {
                if (is_array($rels)) {
                    $acntcreation_rels = array_merge($acntcreation_rels, $rels);
                }
            }

            self::$_acntcreation_rels = $acntcreation_rels;
        }
        return self::$_acntcreation_rels;
    }

    public static function getConfigPath() {
        if (!isset(self::$_config_path)) {
            self::$_config_path = APPLICATION_PATH . '/configs/accountings.php';
        }
        return self::$_config_path;
    }

    public static function isModelFinal(\Sl_Model_Abstract $model) {
        return ($model->getActive() > 0 && $model->getId() && $model->isFinal());
    }

    public static function setConfigPath($path) {
        if (file_exists($path) && preg_match('/\.php$/', $path)) {
            self::$_config_path = $path;
        } else {
            throw new Exception('Not an ini-file: ' . $path);
        }
    }

    public static function config($field = null) {
        if ($field)
            return self::getInstance()->getConfig()->$field;

        return self::getInstance()->getConfig();
    }

    public static function configDocumenting() {


        return self::getInstance()->getConfig()->{self::DOCUMENTING_KEY};
    }

    public static function configTransactioning() {


        return self::getInstance()->getConfig()->{self::TXS_KEY};
    }

    public static function getInstance() {
        if (!isset(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public static function getTxClass($model, $module = null) {
        $alias = \Sl\Service\Helper::getModelAlias($model, $module);
        return self::config(self::ACCOUNTINGS_MODELS_KEY)->{$alias}->{self::TX_SUBKEY};
    }

    public static function getAcntClass($model, $module = null) {
        $alias = \Sl\Service\Helper::getModelAlias($model, $module);
        return self::config(self::ACCOUNTINGS_MODELS_KEY)->{$alias}->{self::ACNT_SUBKEY};
    }

    public function setModelDealFlag($model, $module = null) {
        $alias = \Sl\Service\Helper::getModelAlias($model, $module);
        $this->_config->{self::DEAL_MODELS_KEY}->{$alias} = true;

        $writer = new \Zend_Config_Writer_Array(array('config' => $this->_config));

        try {
            $writer->write($this->getConfigPath());
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function removeModelDealFlag($model, $module = null) {
        $alias = \Sl\Service\Helper::getModelAlias($model, $module);
        unset($this->_config->{self::DEAL_MODELS_KEY}->{$alias});

        $writer = new \Zend_Config_Writer_Array(array('config' => $this->_config));

        try {
            $writer->write($this->getConfigPath());
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function setModelDocumentFlag($model, $module = null) {
        $alias = \Sl\Service\Helper::getModelAlias($model, $module);
        $this->_config->{self::DOCS_MODELS_KEY}->{$alias} = true;

        $writer = new \Zend_Config_Writer_Array(array('config' => $this->_config));

        try {
            $writer->write($this->getConfigPath());
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function removeModelDocumentFlag($model, $module = null) {
        $alias = \Sl\Service\Helper::getModelAlias($model, $module);
        unset($this->_config->{self::DOCS_MODELS_KEY}->{$alias});

        $writer = new \Zend_Config_Writer_Array(array('config' => $this->_config));

        try {
            $writer->write($this->getConfigPath());
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function registerTxRelation($relation, $model, $module = null) {
        //error_reporting(E_ALL);
        $relation = strtolower(($relation instanceof \Sl\Modulerelation\Modulerelation) ? $relation->getName() : trim($relation));
        $alias = \Sl\Service\Helper::getModelAlias($model, $module);

        $transaction_relations_names = ($this->_config->{self::TXS_KEY}->{$alias}) ? $this->_config->{self::TXS_KEY}->{$alias}->toArray() : array();

        if (!in_array($relation, $transaction_relations_names)) {
            $transaction_relations_names[] = $relation;

            if (!isset($this->_config->{self::TXS_KEY}->{$alias}))
                $this->_config->{self::TXS_KEY}->{$alias} = new \Zend_Config(array(), true);

            $this->_config->{self::TXS_KEY}->{$alias} = $transaction_relations_names;
            $writer = new \Zend_Config_Writer_Array(array('config' => $this->_config));

            try {
                $writer->write($this->getConfigPath());
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }
    }

    static public function isTxRelationRegistered($relation, $model = null, $module = null) {
        $relation = strtolower(($relation instanceof \Sl\Modulerelation\Modulerelation) ? $relation->getName() : trim($relation));
        $config = self::config(self::TXS_KEY);

        if ($model != null) {
            $alias = \Sl\Service\Helper::getModelAlias($model, $module);
            return in_array($relation, $config->{$alias}->toArray());
        } else {
            foreach ($config->toArray() as $node) {

                if (in_array($relation, $node))
                    return true;
            }
        }




        return false;
    }

    /* перевіряє, чи створювати для цієї моделі Acnt
     * 
     */

    static public function AcntCreation($model, $module = null) {
        $alias = \Sl\Service\Helper::getModelAlias($model, $module);
        $config = self::config(self::ACNTS_KEY);

        if ($config->{$alias})
            return $config->{$alias}->toArray();


        return false;
    }

    public function unregisterTxRelation($relation, $model, $module = null) {
        $relation = ($relation instanceof \Sl\Modulerelation\Modulerelation) ? $relation->getName() : trim($relation);
        $alias = \Sl\Service\Helper::getModelAlias($model, $module);

        $transaction_relations_names = ($this->_config->{self::TXS_KEY}->{$alias}) ? $this->_config->{self::TXS_KEY}->{$alias}->toArray() : array();

        if (in_array($relation, $transaction_relations_names)) {
            $key = array_search($relation, $transaction_relations_names);
            unset($transaction_relations_names[$key]);
            $this->_config->{self::TXS_KEY}->{$alias} = $transaction_relations_names;
            $writer = new \Zend_Config_Writer_Array(array('config' => $this->_config));

            try {
                $writer->write($this->getConfigPath());
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }
    }

    public static function setDealFlag($model, $module = null) {
        self::getInstance()->setModelDealFlag($model, $module);
    }

    public static function removeDealFlag($model, $module = null) {
        self::getInstance()->removeModelDealFlag($model, $module);
    }

    public static function setDocumentFlag($model, $module = null) {
        self::getInstance()->setModelDocumentFlag($model, $module);
    }

    public static function removeDocumentFlag($model, $module = null) {
        self::getInstance()->removeModelDocumentFlag($model, $module);
    }

    public static function isModelDeal($model, $module = null) {
        $alias = \Sl\Service\Helper::getModelAlias($model, $module);
        return self::getInstance()->getConfig()->{self::DEAL_MODELS_KEY}->{$alias};
    }

    public static function isModelDocument($model, $module = null) {
        $alias = \Sl\Service\Helper::getModelAlias($model, $module);

        return self::getInstance()->getConfig()->{self::DOCS_MODELS_KEY}->{$alias};
    }

//$writer -> write(self::getConfigPath());

    public static function fillByConfig(\Sl_Model_Abstract $object, \Sl_Model_Abstract $target_object, \Zend_Config $config) {

        $target_object = \Sl\Service\Model\Adapter::translate($target_object, $object, $config);
        return $target_object;
    }

    public static function testConditions(\Sl_Model_Abstract $object, \Zend_Config $config) {

        foreach ($config->toArray() as $field => $value) {
            $object_method = $object->buildMethodName($field, 'get');
            if (!method_exists($object, $object_method))
                return false;
            $object_value = $object->{$object_method}();
            if (is_array($value)) {
                if (!in_array($object_value, $value))
                    return false;
            } else {
                if ($object_value != $value)
                    return false;
            }
        }
        return true;
    }

    public static function getAcntByModel(\Sl_Model_Abstract $model) {
        $model_alias = \Sl\Service\Helper::getModelAlias($model);
        if ($acnt_alias = self::getAcntClass($model)) {
            if ($model_alias != $acnt_alias) {
                $acnt_obj = \Sl_Model_Factory::object(\Sl\Service\Helper::getModelnameByAlias($acnt_alias), Helper::getModulenameByAlias($acnt_alias));
                $acnt_relations = \Sl_Modulerelation_Manager::getObjectsRelations($model, $acnt_obj, \Sl_Modulerelation_Manager::RELATION_ONE_TO_ONE);
                if (count($acnt_relations) == 1) {
                    $acnt_relation = current($acnt_relations);
                    if (!$model->issetRelated($acnt_relation->getName())) {
                        $model = \Sl_Model_Factory::mapper($model)->findRelation($model, $acnt_relation);
                    }
                    return $model->fetchOneRelated($acnt_relation->getName());
                }
            } else {
                return $model;
            }
        }
    }

    public static function getAcntAliasByTx($model, $module = null) {
        $tx_alias = Helper::getModelAlias($model, $module);
        foreach (self::config(self::ACCOUNTINGS_MODELS_KEY)->toArray() as $node) {
            if ($node[self::TX_SUBKEY] == $tx_alias)
                return $node[self::ACNT_SUBKEY];
        }
    }

    public static function calculateModel(\Sl_Model_Abstract $model) {

        if (($acnt = self::getAcntByModel($model)) instanceof \Sl\Module\Home\Model\Acnt) {
            $tx_alias = self::getTxClass($model);

            $table = \Sl_Model_Factory::dbTable(Helper::getModelnameByAlias($tx_alias), Helper::getModuleByAlias($tx_alias));

            if ($table instanceof \Sl\Module\Home\Model\Table\Transaction) {
                //print_r()    
                return $table->calculateAcnt($acnt);
            }
        } 
    }

}
