<?php
namespace Sl\Service;

class Cache {
    
    protected static $_adapter;
    
    public static function test($cache_id) {
        return self::getDefaultAdapter()->test($cache_id);
    }
    
    public static function load($cache_id) {
        return self::getDefaultAdapter()->load($cache_id);
    }
    
    public static function save($data, $cache_id) {
        return self::getDefaultAdapter()->save($data, $cache_id);
    }
    
    public static function clean() {
        return self::getDefaultAdapter()->clean();
    }
    
    public static function setDefaultAdapter(Cache\Adapter $adapter) {
        self::$_adapter = $adapter;
    }
    
    /**
     * Возвращает адаптер по-умолчанию
     * 
     * @return \Sl\Service\Cache\Adapter
     */
    public static function getDefaultAdapter() {
        if(!isset(self::$_adapter)) {
            self::$_adapter = new Cache\Adapter\Simple();
        }
        return self::$_adapter;
    }
}