<?php
namespace Sl\Service\Cache\Adapter;

use Exception;

class Simple extends \Sl\Service\Cache\Adapter {
    
    protected $_cache;
    
    public function __construct() {
        try {
            $this->_cache = \Zend_Registry::get('cache')->getBackend();
        } catch (Exception $e) {
            $this->_cache = new \Zend_Cache_Backend_File();
        }
    }
    
    public function load($cache_id) {
        return unserialize($this->_getCache()->load($this->_prepareCacheId($cache_id)));
    }

    public function save($data, $cache_id) {
        return $this->_getCache()->save(serialize($data), $this->_prepareCacheId($cache_id));
    }

    public function test($cache_id) {
        return $this->_getCache()->test($this->_prepareCacheId($cache_id));
    }
    
    public function getIds() {
        return $this->_getCache()->getIds();
    }
    
    /**
     * 
     * @return \Zend_Cache_Backend
     */
    protected function _getCache() {
        return $this->_cache;
    }
    
    protected function _prepareCacheId($cache_id) {
        return $cache_id.APPLICATION_NAME;
    }

    public function clean() {
        return $this->_getCache()->clean();
    }

}