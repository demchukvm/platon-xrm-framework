<?php
namespace Sl\Service\Cache\Adapter;

class Mock extends \Sl\Service\Cache\Adapter {
    
    public function load($cache_id) {
        return null;
    }
    
    public function save($data, $cache_id) {
        return false;
    }
    
    public function test($cache_id) {
        return false;
    }

    public function clean() {
        
    }

}