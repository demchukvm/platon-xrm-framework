<?php
namespace Sl\Service\Cache;

abstract class Adapter {
    
    abstract public function load($cache_id);
    abstract public function save($data, $cache_id);
    abstract public function test($cache_id);
    abstract public function clean();
}