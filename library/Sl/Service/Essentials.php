<?php
namespace Sl\Service;

use Sl_Model_Abstract as AbstractModel;
use Sl_Modulerelation_Manager as RelationsManager;
use Sl_Model_Factory as ModelFactory;

class Essentials {
    
    const ESSENTIALS_TABLE = 'essentials';
    
    protected static $_essentials;
    
    protected static function _init($rebuild = false) {
        if(!isset(self::$_essentials) || $rebuild) {
            $data = self::_fetchAll();
            foreach($data as $item) {
                if(!isset(self::$_essentials[$item['model']])) {
                    self::$_essentials[$item['model']] = array();
                }
                if(!isset(self::$_essentials[$item['model']][$item['relation']])) {
                    self::$_essentials[$item['model']][$item['relation']] = array();
                }
                self::$_essentials[$item['model']][$item['relation']][$item['mid']] = array_diff(explode('|', $item['data']), array(''));
            }
        }
        return self::$_essentials;
    }
    
    public static function exists(AbstractModel $model, $relation) {
        self::_init();
        $relation = self::_relation($model, $relation);
        return isset(self::$_essentials[\Sl\Service\Helper::getModelAlias($model)][$relation->getName()][$model->getId()]);
    }
    
    public static function read(AbstractModel $model, $relation, $ids_only = false) {
        self::_init();
        $relation = self::_relation($model, $relation);
        $models = array();
        if(self::exists($model, $relation)) {
            $ids = self::$_essentials[\Sl\Service\Helper::getModelAlias($model)][$relation->getName()][$model->getId()];
            if($ids_only) {
                foreach($ids as $id) {
                    $models[$id] = $id;
                }
            } else {
                foreach($ids as $id) {
                    $rModel = ModelFactory::mapper($relation->getRelatedObject($model))->find($id);
                    if($rModel) {
                        $models[$rModel->getId()] = $rModel;
                    }
                }
            }
        }
        return $models;
    }
    
    public static function write(AbstractModel $model, $relation, $data = null) {
        self::_init();
        $relation = self::_relation($model, $relation);
        if(is_null($data)) {
            $data = array();
        }
        if(!is_array($data)) {
            $data = array($data);
        }
        self::_dbInsert(\Sl\Service\Helper::getModelAlias($model), $relation->getName(), $model->getId(), $data);
        self::_init(true);
    }
    
    public static function clean(AbstractModel $model, $relation) {
        return self::write($model, $relation);
    }
    
    /**
     * Возвращает нормальную связь
     * 
     * @param type $model
     * @param type $relation
     * @return \Sl\Modulerelation\Modulerelation
     */
    protected static function _relation(AbstractModel $model, $relation) {
        if(!$model->getId()) {
            throw new \Exception('Model id must be set');
        }
        $relation = RelationsManager::getRelations(ModelFactory::model($model), ($relation instanceof \Sl\Modulerelation\Modulerelation)?$relation->getName():$relation);
        if(!$relation) {
            throw new \Exception('Wrong relation specified. '.__METHOD__);
        }
        return $relation;
    }


    protected static function _fetchAll() {
        $select = \Zend_Db_Table::getDefaultAdapter()->select();
        
        $select ->from(array('e' => self::ESSENTIALS_TABLE));
        return \Zend_Db_Table::getDefaultAdapter()->fetchAll($select, array(), \Zend_Db::FETCH_ASSOC);
    }
    
    protected static function _dbInsert($alias, $relation, $id, array $data = array()) {
        $db = \Zend_Db_Table::getDefaultAdapter();
        // Delete 
        $statement = $db->prepare('DELETE FROM `'.self::ESSENTIALS_TABLE.'` WHERE `model` = ? AND `relation` = ? AND `mid` = ?');
        $statement->execute(array(
            $alias,
            $relation,
            $id
        ));
        // Insert
        $db->insert(self::ESSENTIALS_TABLE, array(
            'model' => $alias,
            'relation' => $relation,
            'mid' => $id,
            'data' => '|'.implode('|', array_map(function($el){
                return ($el instanceof AbstractModel)?$el->getId():$el;
            }, $data)).'|'
        ));
    }
}