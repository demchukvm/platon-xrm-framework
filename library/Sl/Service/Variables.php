<?php

class Sl_Service_Variables {

    protected static $_variables;

    const TYPE = 'type';
    const VALUE = 'value';
    const TYPE_INT = 0;
    const TYPE_FLOAT = 1;
    const TYPE_DATE = 2;
    const TYPE_DATETIME = 3;
    const TYPE_BOOL = 4;
    
    protected static $_variables_model_class = '\Sl\Module\Home\Model\Variable';

    private static function variable($name, $request) {
        $name = strtoupper($name);
        if (!isset(self::$_variables)) {
            self::$_variables = array();
            $object = \Sl_Model_Factory::object(self::$_variables_model_class);
            $result = Sl_Model_Factory::mapper($object)->fetchAll();
            foreach ($result as $key => $value) {

                self::_setVariable($value);
            }
        }
        if (isset(self::$_variables[$name])) {

            switch ($request) {
                case self::VALUE:
                    return self::_convertStrToValue(self::$_variables[$name]->getValue(), self::$_variables[$name]->getType()) ;
                case self::TYPE :
                    return self::$_variables[$name]->getType();
                default :
                    throw new Exception('Wrong parameter of "variable" method!');
            }
        } /* else {
          throw new Exception('No such settings in the system!');
          } */
        return null;
    }

    public static function value($name, $default = null) {
        // @TODO Сделать по-человечески
        // Принудительное чтение перед тем как определять есть ли эта настройка
        $val = self::variable($name, self::VALUE);
        if (is_null($val) && !isset(self::$_variables[$name])) {
            return $default;
        }
        return $val;
    }

    public static function type($name) {
        return self::settings($name, self::TYPE);
    }

    protected static function _setVariable(\Sl\Module\Home\Model\Variable $var) {
        self::$_variables[strtoupper($var->getName())] = $var;
        
    }
    
    protected static function _convertValueToStr ($value, $type = null){
        $val = '';
        switch ($type){
            case self::TYPE_DATE    : $val = $value -> format(\Sl_Model_Abstract::FORMAT_DATE); break; 
            case self::TYPE_DATETIME: $val = $value -> format(\Sl_Model_Abstract::FORMAT_TIMESTAMP); break; 
            case self::TYPE_BOOL    : $val = $value?'1':'0';break;
            default: $val.=$value;
        } 
        return $val;
        
    }
    
    protected static function _checkType ($value){
        if ($value instanceof \DateTime && intval($value->format('His'))==0){
            return self::TYPE_DATE;
        }elseif ($value instanceof \DateTime){
            return self::TYPE_DATETIME;
        }elseif (is_bool($value)){
            return self::TYPE_BOOL;
        }elseif(is_int($value) || (is_string($value) && preg_match('/^-?[\d]+$/',$value))){
            return self::TYPE_INT;
        }elseif(is_float($value) || (is_string($value) && preg_match('/^-?[\d]+\.[\d]+$/',$value))){
            return self::TYPE_FLOAT;
        }
        
        return null;
    }
    
    protected static function _convertStrToValue ($value, $type = null){
        
        switch ($type){
            case self::TYPE_DATE: $val = date_create_from_format(\Sl_Model_Abstract::FORMAT_DATE, $value); break; 
            case self::TYPE_DATETIME: $val = date_create_from_format(\Sl_Model_Abstract::FORMAT_TIMESTAMP, $value); break; 
            case self::TYPE_INT: $val = intval($value); break; 
            case self::TYPE_FLOAT: $val = floatval($value); break; 
            case self::TYPE_BOOL: $val = ($value=='1'); break; 
            default: $val=''.$value;
        } 
        return $val;
    }
    
    public static function set($name, $val, $type = null) {
        $type = is_null($type)?self::_checkType($val):$type;
        $old_val = self::variable($name, self::VALUE);
        if ($old_val !== $val) {
            $var = \Sl_Model_Factory::mapper(self::$_variables_model_class)->saveVariable($name, self::_convertValueToStr($val,$type), $type);
            self::_setVariable($var);
        }
    }

}

?>
