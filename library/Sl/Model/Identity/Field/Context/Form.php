<?php
namespace Sl\Model\Identity\Field\Context;

class Form extends \Sl\Model\Identity\Field\Context {
    
    protected $_visible;
    
    public function setVisible($visible) {
        $this->_visible = (bool) $visible;
        return $this;
    }
    
    public function getVisible() {
        return $this->_visible;
    }
}