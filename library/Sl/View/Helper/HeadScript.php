<?php
namespace Sl\View\Helper;

use Sl_Module_Manager as ModuleManager;
use Sl_Service_Settings as Settings;

class HeadScript extends \Zend_View_Helper_HeadScript {
    
    protected $_cachePath;
    protected $_cacheUri;
    protected $_paths;
    protected $_raw_placement = 'prepend';
    protected $_available_placements;
    protected $_baseProtocol;
    protected $_baseHost;
    
    const PLACEMENT_PREPEND = 'prepend';
    const PLACEMENT_APPEND = 'append';
    
    public function headScript($mode = \Zend_View_Helper_HeadScript::FILE, $spec = null, $placement = 'APPEND', array $attrs = array(), $type = 'text/javascript') {
        return parent::headScript($mode, $spec, $placement, $attrs, $type);
    }
    
    public function toString($indent = null) {
        try {
            $html = "\r\n";
            if(Settings::value('DEBUG_MODE', false)) {
                foreach($this->getContainer() as $item) {
                    if(isset($item->attributes['src'])) {
                        $html .= '<script type="text/javascript" src="'.$this->getBaseUri().$item->attributes['src'].'"></script>'."\r\n";
                    } elseif(isset($item->source) && $item->source) {
                        $html .= '<script type="text/javascript">;'.$item->source.';</script>'."\r\n";
                    }
                }
            } else {
                $scripts = $raw = array();
                foreach($this->getContainer() as $k=>$v) {
                    $filename = trim($v->attributes['src'], '/');
                    if($filename) {
                        $existingFile = null;
                        foreach($this->getScriptPaths() as $path) {
                            if($existingFile) break;
                            $tmpPath = $path.'/'.$filename;
                            if(file_exists($tmpPath) && is_readable($tmpPath)) {
                                $existingFile = $tmpPath;
                            }
                        }
                        if(!$existingFile) {
                            list($module, $controller, $file) = explode('/', $filename);
                            foreach($this->getScriptPaths() as $path) {
                                if($existingFile) break;
                                $tmpPath = $path.'/'.ucfirst($module).'/static/js/'.$controller.'/'.$file;
                                if(file_exists($tmpPath) && is_readable($tmpPath)) {
                                    $existingFile = $tmpPath;
                                }
                            }
                        }
                        if($existingFile) {
                            $scripts[] = $existingFile;
                        }
                    } elseif($v->source) {
                        $raw[] = $v->source;
                    }
                }
                if(($this->getRawPlacement() === self::PLACEMENT_PREPEND) && count($raw)) {
                    $html .= '<script type="text/javascript">;'.implode(";\r\n", $raw).'</script>';
                }
                if(count($scripts)) {
                    $max_time = max(array_map('filemtime', $scripts));
                    $cache_id = md5(implode(',', array_map(function($el) { return $el.':'.filemtime($el); }, $scripts)));
                    $cacheFile = $cache_id.'.js';
                    $cacheDir = $this->getCachePath().DIRECTORY_SEPARATOR.$max_time;
                    if(!is_dir($cacheDir)) {
                        mkdir($cacheDir, 0777, true);
                    }
                    $cacheFilePath = $cacheDir.'/'.$cacheFile;
                    if(!file_exists($cacheFilePath)) {
                        if(class_exists('Minify')) {
                            $content = \Minify::combine($scripts);
                        } else {
                            $content = '';
                            foreach($scripts as $script) {
                                $content .= ";\r\n".$script;
                            }
                        }
                        $fh = fopen($cacheFilePath, 'w+');
                        if($fh) {
                            fwrite($fh, $content);
                            fclose($fh);
                        }
                    }
                    $this->cleanGarbage($max_time);
                    $html .= '<script type="text/javascript" src="'.$this->getBaseUri().$this->getCacheUri().$max_time.'/'.$cacheFile.'"></script>';
                }
                if(($this->getRawPlacement() === self::PLACEMENT_APPEND) && count($raw)) {
                    $html .= '<script type="text/javascript">;'.implode(";\r\n", $raw).'</script>';
                }
            }
            return $html;
        } catch(\Exception $e) {
            return $e->getMessage()."\r\n".$e->getTraceAsString();
        }
    }
    
    public function setRawPlacement($placement) {
        if(!in_array($placement, $this->_getAvailablePlacements())) {
            throw new \Exception('Wrong placement given. '.__METHOD__);
        }
        $this->_raw_placement = $placement;
        return $this;
    }
    
    public function getRawPlacement() {
        return $this->_raw_placement;
    }
    
    public function setCachedUri($uri) {
        $this->_cacheUri = '/'.trim($uri, '/').'/';
        return $this;
    }
    
    public function getCacheUri() {
        if(!isset($this->_cacheUri)) {
            $this->setCachedUri('/cached/js/');
        }
        return $this->_cacheUri;
    }
    
    public function setCachePath($path) {
        if(!is_dir($path)) {
            throw new \Exception('Path must exists. '.__METHOD__);
        }
        $this->_cachePath = rtrim($path, '/');
        return $this;
    }
    
    public function getCachePath() {
        if(!isset($this->_cachePath)) {
            $this->setCachePath(APPLICATION_PATH.'/../public/cached/js/');
        }
        return $this->_cachePath;
    }
    
    public function addScriptPath($path, $priority = null, $overwrite = false) {
        if(!is_dir($path)) {
            throw new \Exception('Path must exists. '.__METHOD__);
        }
        if(is_null($priority)) {
            $this->_paths[] = $path;
        } else {
            $priority = intval($priority);
            if(!$overwrite && key_exists($priority, $this->_paths)) {
                array_splice($this->_paths, $priority, 0, $path);
            } else {
                $this->_paths[$priority] = $path;
            }
        }
        return $this;
    }
    
    public function setScriptPath($path, $priority) {
        return $this->addScriptPath($path, $priority, true);
    }
    
    public function getScriptPaths() {
        if(!isset($this->_paths)) {
            $this->addScriptPath(APPLICATION_PATH.'/../public');
            foreach(ModuleManager::getModulesDirectories() as $dir) {
                $this->addScriptPath(APPLICATION_PATH.'/'.$dir);
            }
        }
        return $this->_paths;
    }
    
    public function setBaseHost($host) {
        if(!\Zend_Validate::is($host, 'Hostname')) {
            throw new \Exception('Wrong hostname given. '.__METHOD__);
        }
        $this->_baseHost = $host;
        return $this;
    }
    
    public function getBaseHost() {
        if(!isset($this->_baseHost)) {
            $this->_baseHost = Settings::value('STATIC_BASE_HOST', false);
        }
        return $this->_baseHost;
    }
    
    public function setBaseProtocol($protocol) {
        $protocol = strtolower($protocol);
        if(!in_array($protocol, array('http', 'https'))) {
            throw new \Exception('Wrong protocol givem. '.__METHOD__);
        }
        $this->_baseProtocol = $protocol;
        return $this;
    }
    
    public function getBaseProtocol() {
        if(!isset($this->_baseProtocol)) {
            $this->setBaseProtocol('http');
        }
        return $this->_baseProtocol;
    }
    
    public function getBaseUri() {
        $uri = '';
        if($this->getBaseHost()) {
            return $this->getBaseProtocol().'://'.$this->getBaseHost();
        }
        return $uri;
    }
    
    public function cleanGarbage($exclude_dir = false) {
        $dh = opendir($this->getCachePath());
        if($dh) {
            while(false !== ($filename = readdir($dh))) {
                if(!in_array($filename, array('.', '..'))) {
                    if(is_dir($this->getCachePath().$filename) && ($filename != $exclude_dir)) {
                        \Sl\Service\Common::rmdir($this->getCachePath().$filename);
                    }
                }
            }
            closedir($dh);
        }
    }
    
    protected function _getAvailablePlacements() {
        if(!isset($this->_available_placements)) {
            $placements = array();
            $refClass = new \ReflectionClass($this);
            foreach($refClass->getConstants() as $name=>$value) {
                if(preg_match('/^PLACEMENT_/', $name)) {
                    $placements[] = $value;
                }
            }
            $this->_available_placements = $placements;
        }
        return $this->_available_placements;
    }
}