<?php
namespace Sl\View\Helper;

class Navigation extends \Zend_View_Helper_Navigation {
    
    protected function _acceptAcl(\Zend_Navigation_Page $page) {
        if (!$acl = $this->getAcl()) {
            return true;
        }
        $resource = $page->getResource();
        $privilege = $page->getPrivilege();
        
        $result = true;
        if($resource || $privilege) {
            $result = false;
            foreach(\Sl_Service_Acl::getCurrentRoles() as $role) {
                $result |= $acl->isAllowed($role->getName(), $resource, $privilege);
            }
            return $result;
        }
        return $result;
    }
}