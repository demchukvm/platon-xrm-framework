<?php
namespace Sl\View\Helper;

class ModelLink extends \Zend_View_Helper_Abstract {
    
    protected $_protocol;
    protected $_host;
    protected $_available_protocols;
    
    const PROTOCOL_HTTP = 'http';
    const PROTOCOL_HTTPS = 'https';
    
    public function modelLink(\Sl_Model_Abstract $model, $action = 'detailed', $text = null) {
        return '<a href="'.$this->getBaseUrl().\Sl\Service\Helper::buildModelUrl($model, $action, array('id' => $model->getId())).'">'.((string) is_null($text)?$model:$text).'</a>';
    }
    
    public function setProtocol($protocol) {
        if(!in_array($protocol, $this->_getAvailableProtocols())) {
            throw new \Exception('Wrong protocol given. '.__METHOD__);
        }
        $this->_protocol = $protocol;
        return $this;
    }
    
    public function getProtocol() {
        if(!isset($this->_protocol)) {
            $this->setProtocol(self::PROTOCOL_HTTP);
        }
        return $this->_protocol;
    }
    
    public function setHost($host) {
        $this->_host = $host;
        return $this;
    }
    
    public function getHost() {
        if(!isset($this->_host)) {
            $this->setHost($_SERVER['HTTP_HOST']);
        }
        return $this->_host;
    }
    
    public function getBaseUrl() {
        return $this->getProtocol().'://'.$this->getHost();
    }
    
    protected function _getAvailableProtocols() {
        if(!isset($this->_available_protocols)) {
            $protocols = array();
            $reflaction = new \ReflectionClass($this);
            foreach($reflaction->getConstants() as $name=>$value) {
                if(preg_match('/^PROTOCOL_.+$/', $name)) {
                    $protocols[] = strtolower($value);
                }
            }
            $this->_available_protocols = $protocols;
        }
        return $this->_available_protocols;
    }
}