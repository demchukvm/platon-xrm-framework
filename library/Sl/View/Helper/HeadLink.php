<?php
namespace Sl\View\Helper;

use Sl_Module_Manager as ModuleManager;
use Sl_Service_Settings as Settings;

class HeadLink extends \Zend_View_Helper_HeadLink {
    
    protected $_cachePath;
    protected $_cacheUri;
    protected $_paths;
    protected $_baseProtocol;
    protected $_baseHost;
    
    public function headLink($content = null, $placement = 'APPEND', $attributes = array()) {
        return parent::headLink($content, $placement, $attributes);
    }
    
    public function toString($indent = null) {
        try {
            $html = "\r\n";
            $scriptData = (array) $this->getContainer();
            if(Settings::value('DEBUG_MODE', false)) {
                foreach($scriptData as $item) {
                    if(isset($item->href) && $item->href) {
                        $html .= '<link rel="stylesheet" href="'.$this->getBaseUri().$item->href.'" />'."\r\n";
                    }
                }
            } else {
                $scripts = array();
                foreach($scriptData as $k=>$v) {
                    $filename = trim($v->href, '/');
                    if($filename) {
                        $existingFile = null;
                        foreach($this->getScriptPaths() as $path) {
                            if($existingFile) break;
                            $tmpPath = $path.'/'.$filename;
                            if(file_exists($tmpPath) && is_readable($tmpPath)) {
                                $existingFile = $tmpPath;
                            }
                        }
                        if(!$existingFile) {
                            list($module, $controller, $file) = explode('/', $filename);
                            foreach($this->getScriptPaths() as $path) {
                                if($existingFile) break;
                                $tmpPath = $path.'/'.ucfirst($module).'/static/css/'.$controller.'/'.$file;
                                if(file_exists($tmpPath) && is_readable($tmpPath)) {
                                    $existingFile = $tmpPath;
                                }
                            }
                        }
                        if($existingFile) {
                            $scripts[] = $existingFile;
                        }
                    }
                }
                if(count($scripts)) {
                    $max_time = max(array_map('filemtime', $scripts));
                    $cache_id = md5(implode(',', array_map(function($el) { return $el.':'.filemtime($el); }, $scripts)));
                    $cacheFile = $cache_id.'.css';
                    $cacheDir = $this->getCachePath().'/'.$max_time;
                    if(!is_dir($cacheDir)) {
                        mkdir($cacheDir, 0777, true);
                    }
                    $cacheFilePath = $cacheDir.'/'.$cacheFile;
                    if(!file_exists($cacheFilePath)) {
                        // Пока минимизатор неправильно сливает, просто собираем в кучу
                        if(false && class_exists('Minify')) {
                            $content = \Minify::combine($scripts);
                        } else {
                            $content = '';
                            foreach($scripts as $script) {
                                $content .= "\r\n".file_get_contents($script);
                            }
                        }
                        $fh = fopen($cacheFilePath, 'w+');
                        if($fh) {
                            fwrite($fh, $content);
                            fclose($fh);
                        }
                    }
                    $this->cleanGarbage($max_time);
                    $html .= '<link rel="stylesheet" href="'.$this->getBaseUri().$this->getCacheUri().$max_time.'/'.$cacheFile.'" />';
                }
            }
            return $html;
        } catch(\Exception $e) {
            echo $e->getMessage()."\r\n".$e->getTraceAsString();die;
        }
    }
    
    public function setCachedUri($uri) {
        $this->_cacheUri = '/'.trim($uri, '/').'/';
        return $this;
    }
    
    public function getCacheUri() {
        if(!isset($this->_cacheUri)) {
            $this->setCachedUri('/cached/css/');
        }
        return $this->_cacheUri;
    }
    
    public function setCachePath($path) {
        if(!is_dir($path)) {
            throw new \Exception('Path must exists. '.__METHOD__);
        }
        $this->_cachePath = rtrim($path, '/');
        return $this;
    }
    
    public function getCachePath() {
        if(!isset($this->_cachePath)) {
            $this->setCachePath(APPLICATION_PATH.'/../public/cached/css/');
        }
        return $this->_cachePath;
    }
    
    public function addScriptPath($path, $priority = null, $overwrite = false) {
        if(!is_dir($path)) {
            throw new \Exception('Path must exists. '.__METHOD__);
        }
        if(is_null($priority)) {
            $this->_paths[] = $path;
        } else {
            $priority = intval($priority);
            if(!$overwrite && key_exists($priority, $this->_paths)) {
                array_splice($this->_paths, $priority, 0, $path);
            } else {
                $this->_paths[$priority] = $path;
            }
        }
        return $this;
    }
    
    public function setScriptPath($path, $priority) {
        return $this->addScriptPath($path, $priority, true);
    }
    
    public function getScriptPaths() {
        if(!isset($this->_paths)) {
            $this->addScriptPath(APPLICATION_PATH.'/../public');
            foreach(ModuleManager::getModulesDirectories() as $dir) {
                $this->addScriptPath(APPLICATION_PATH.'/'.$dir);
            }
        }
        return $this->_paths;
    }
    
    public function setBaseHost($host) {
        if(!\Zend_Validate::is($host, 'Hostname')) {
            throw new \Exception('Wrong hostname given. '.__METHOD__);
        }
        $this->_baseHost = $host;
        return $this;
    }
    
    public function getBaseHost() {
        if(!isset($this->_baseHost)) {
            $this->_baseHost = Settings::value('STATIC_BASE_HOST', false);
        }
        return $this->_baseHost;
    }
    
    public function setBaseProtocol($protocol) {
        $protocol = strtolower($protocol);
        if(!in_array($protocol, array('http', 'https'))) {
            throw new \Exception('Wrong protocol givem. '.__METHOD__);
        }
        $this->_baseProtocol = $protocol;
        return $this;
    }
    
    public function getBaseProtocol() {
        if(!isset($this->_baseProtocol)) {
            $this->setBaseProtocol('http');
        }
        return $this->_baseProtocol;
    }
    
    public function getBaseUri() {
        $uri = '';
        if($this->getBaseHost()) {
            return $this->getBaseProtocol().'://'.$this->getBaseHost();
        }
        return $uri;
    }
    
    public function cleanGarbage($exclude_dir = false) {
        $dh = opendir($this->getCachePath());
        if($dh) {
            while(false !== ($filename = readdir($dh))) {
                if(!in_array($filename, array('.', '..'))) {
                    if(is_dir($this->getCachePath().$filename) && ($filename != $exclude_dir)) {
                        \Sl\Service\Common::rmdir($this->getCachePath().$filename);
                    }
                }
            }
            closedir($dh);
        }
    }
}