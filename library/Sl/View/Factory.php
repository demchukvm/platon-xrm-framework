<?php
namespace Sl\View;

use Sl_Exception_View as Exception;
use Sl_View as SlView;
use Sl_Module_Manager as ModuleManager;
use Sl_Module_Abstract as AbstractModule;

class Factory {
    
    protected static $_view;
    
    public static function build($data = null) {
        if($data instanceof \Zend_Controller_Request_Abstract) {
            return self::_fromRequest($data);
        } elseif($data instanceof AbstractModule) {
            return self::_fromModule($data);
        } elseif(is_array($data)) {
            throw new Exception('Not implemented');
        } elseif(is_string($data)) {
            throw new Exception('Not implemented');
        } elseif(is_null($data)) {
            return self::getView();
        } else {
            throw new Exception('Can\'t build view from :'.print_r($data, true));
        }
    }
    
    protected static function _fromRequest(\Zend_Controller_Request_Abstract $request) {
        $view = new SlView();
        try {
            $view_dir = ModuleManager::getViewDirectory($request->getModuleName());
            $view->setScriptPath($view_dir);
            return $view;
        } catch(Sl_Exception_View $e) {
            die($e->getMessage());
        }
    }
    
    protected static function _fromModule(AbstractModule $module) {
        throw new Exception('Not implemented. '.__METHOD__);
    }
    
    public static function setView(SlView $view) {
        self::$_view = $view;
    }
    
    public static function getView() {
        if(!isset(self::$_view)) {
            // Пока не доработано ...
            self::$_view = new \Sl_View();
        }
        return self::$_view;
    }
}

