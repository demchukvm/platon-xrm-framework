<?php
namespace  Sl\View\Control;

class Simple extends \Sl\View\Control {
    
    protected $_id;
    protected $_like_btn;
    protected $_pull_rigth = true;
    public function setId($id) {
        $this->_id = $id;
        return $this;
    }
    
    public function getId() {
        return $this->_id;
    }
    
    public function setLikeBtn($like_btn = true) {
        $this->_like_btn = $like_btn;
        return $this;
    }
    
    public function getLikeBtn() {
        return $this->_like_btn;
    }
    
    protected function _prepareViewData() {
        parent::_prepareViewData();
        $attribs = array();
        foreach($this->getAttribs() as $k=>$v) {
            $attribs[] = $k.'="'.$v.'"';
        }
        $this->getView()->attribs = implode(' ', $attribs);
    }
}