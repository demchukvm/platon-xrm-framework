<?php

namespace Sl\View\Control\Section;

class Item {

    protected $_href;
    protected $_label;
    protected $_class;
    protected $_icon;

    public function __construct(array $options = array()) {
        foreach ($options as $key => $value) {
            $method_name = 'set' . implode('', array_map('ucfirst', explode("_", $key)));
            if (method_exists($this, $method_name)) {
                try {
                    $this->$method_name($value);
                } catch (\Exception $e) {
                    // Не получилось :)
                }
            }
        }
    }

    public function getSubitems() {
        return $this->_subitems;
    }

    public function getIcon() {
        return $this->_icon;
    }

    public function setIcon($icon) {
        $this->_icon = $icon;
        return $this;
    }

    public function setSubitems(array $subitems) {
        $this->_subitems = $subitems;
        return $this;
    }

    public function addSubitems($subitem) {
        $this->_subitems[] = $subitem;
        return $this;
    }

    public function setHref($href) {
        $this->_href = $href;
        return $this;
    }

    public function setLabel($label) {
        $this->_label = $label;
        return $this;
    }

    public function setClass($class) {
        $this->_class = $class;
        return $this;
    }

    public function getHref() {
        return $this->_href;
    }

    public function getLabel() {
        return $this->_label;
    }

    public function getClass() {
        return $this->_class;
    }

}