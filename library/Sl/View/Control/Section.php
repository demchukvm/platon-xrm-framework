<?php

namespace Sl\View\Control;

class Section extends \Sl\View\Control {

    protected $_id;
    protected $_label;
    protected $_class;
    protected $_items = array();
    
    public function setId($id) {
        $this->_id = $id;
        return $this;
    }

    public function setLabel($label) {
        $this->_label = $label;
        return $this;
    }

    public function getLabel() {
        return $this->_label;
    }

    public function setClass($class) {
        $this->_class = $class;
        return $this;
    }

    public function getClass() {
        return $this->_class;
    }
    
     public function getId() {
        return $this->_id;
    }

    public function addItem(Section\Item $item, $key = null) {
        if (!is_null($key)) {
            $this->_items[$key] = $item;
        } else {
            $this->_items[] = $item;
        }
        return $this;
    }

    public function addItems(array $items) {
        foreach ($items as $k => $item) {
            if ($item instanceof Section\Item) {
                $this->addItem($item, $k);
            }
        }
        return $this;
    }

    public function setItems(array $items) {
        return $this->addItems($items);
    }

    public function getItem($key) {
        if (isset($this->_items[$key])) {
            return $this->_items[$key];
        }
        return null;
    }

    public function getItems() {
        return $this->_items;
    }

}