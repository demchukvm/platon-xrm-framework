<?php
namespace Sl\Listener;

use Sl\Event;

interface Mapper {
    
    public function onFetchAll(Event\Mapper $event);
}