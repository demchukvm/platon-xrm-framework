<?php
namespace Sl\Listener;

interface Dispatcher {
    
    public function onBeforeDispatch(\Sl\Event\Dispatcher $event);
    public function onAfterDispatch(\Sl\Event\Dispatcher $event);
    
}