<?php
namespace Sl\Listener;

interface Form {
    
    public function onAfterCreate(\Sl\Event\Form $event);
    
}