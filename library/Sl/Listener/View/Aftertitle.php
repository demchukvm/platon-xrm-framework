<?php
namespace Sl\Listener\View;

interface Aftertitle {
    
    public function onAftertitle(\Sl_Event_View $event);
}