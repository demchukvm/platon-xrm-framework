<?php

interface Sl_Listener_View_Table_Interface {
    
    public function onTableTitleLeft (\Sl_Event_View $event);
    public function onTableTitleRight (\Sl_Event_View $event);
    
}
?>
