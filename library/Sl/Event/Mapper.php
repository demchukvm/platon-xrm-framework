<?php
namespace Sl\Event;

class Mapper extends \Sl_Event_Abstract {
    
    protected $_model;
    protected $_where;
    
    public function __construct($type, array $options = array()) {
        if(!isset($options['model']) || !($options['model'] instanceof \Sl_Model_Abstract)) {
            throw new \Exception($this->getTranslator()->translate('Param \'model\' is required. '.__METHOD__));
        }
        if(!isset($options['where']) || !is_array($options['where'])) {
            throw new \Exception($this->getTranslator()->translate('Param \'where\' is required and must be array. '.__METHOD__));
        }
        $this->setModel($options['model'])->setWhere($options['where']);
        parent::__construct($type, $options);
    }
    
    /**
     * 
     * @param \Sl_Model_Abstract $model
     * @return \Sl\Event\Model\Mapper
     */
    public function setModel(\Sl_Model_Abstract $model) {
        $this->_model = $model;
        return $this;
    }
    
    /**
     * 
     * @param array $where
     * @return \Sl\Event\Model\Mapper
     */
    public function setWhere(array $where = array()) {
        $this->_where = $where;
        return $this;
    }
    
    /**
     * 
     * @return \Sl_Model_Abstract
     */
    public function getModel() {
        return $this->_model;
    }
    
    /**
     * 
     * @return mixed[]
     */
    public function getWhere() {
        return $this->_where;
    }
}