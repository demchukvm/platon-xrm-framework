<?php
namespace Sl\Event;

use Sl_Model_Abstract as AbstractModel;

class Form extends \Sl_Event_Abstract {
    
    /**
     *
     * @var \Sl\Form\Form
     */
    protected $_form;
    
    /**
     *
     * @var AbstractModel 
     */
    protected $_model;
    
    public function __construct($type, $options = array()) {
        if(!isset($options['model'])) {
            throw new \Exception('"model" param is required. '.__METHOD__);
        }
        if(!($options['model'] instanceof AbstractModel)) {
            throw new \Exception('"model" param must be instance of \Sl_Model_Abstract. '.__METHOD__);
        }
        if(!isset($options['form'])) {
            throw new \Exception('"form" param is required. '.__METHOD__);
        }
        if(!($options['form'] instanceof \Sl\Form\Form)) {
            throw new \Exception('"form" param must be instance of \Sl\Form\Form. '.__METHOD__);
        }
        $this->setModel($options['model'])->setForm($options['form']);
        parent::__construct($type, $options);
    }
    
    /**
     * 
     * @param AbstractModel $model
     * @return \Sl\Event\Form
     */
    public function setModel(AbstractModel $model) {
        $this->_model = $model;
        return $this;
    }
    
    /**
     * 
     * @return AbstractModel
     */
    public function getModel() {
        return $this->_model;
    }
    
    /**
     * 
     * @param \Sl\Form\Form $form
     * @return \Sl\Event\Form
     */
    public function setForm(\Sl\Form\Form $form) {
        $this->_form = $form;
        return $this;
    }
    
    /**
     * 
     * @return \Sl\Form\Form
     */
    public function getForm() {
        return $this->_form;
    }
    
}