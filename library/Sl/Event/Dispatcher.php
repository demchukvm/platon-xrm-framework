<?php
namespace Sl\Event;

use Sl_Event_Abstract as AbstractEvent;

class Dispatcher extends AbstractEvent {
    
    /**
     *
     * @var \Zend_Controller_Request_Http
     */
    protected $_request;
    
    /**
     *
     * @var \Zend_Controller_Response_Abstract
     */
    protected $_response;
    
    public function __construct($type, $options = array()) {
        if(!isset($options['request']) || !($options['request'] instanceof \Zend_Controller_Request_Http)) {
            throw new \Exception('Param \'request\' is required');
        }
        if(!isset($options['response']) || !($options['response'] instanceof \Zend_Controller_Response_Abstract)) {
            throw new \Exception('Param \'response\' is required');
        }
        $this->setRequest($options['request']);

        parent::__construct($type, $options);
    }
    
    /**
     * 
     * @param \Zend_Controller_Request_Http $request
     * @return \Sl\Event\Dispatcher
     */
    public function setRequest(\Zend_Controller_Request_Http $request) {
        $this->_request = $request;
        return $this;
    }
    
    /**
     * 
     * @return \Zend_Controller_Request_Http
     */
    public function getRequest() {
        return $this->_request;
    }
    
    /**
     * 
     * @param \Zend_Controller_Response_Abstract $response
     * @return \Sl\Event\Dispatcher
     */
    public function setResponse(\Zend_Controller_Response_Abstract $response) {
        $this->_response = $response;
        return $this;
    }
    
    /**
     * 
     * @return \Zend_Controller_Response_Abstract
     */
    public function getResponse() {
        return $this->_response;
    }
}