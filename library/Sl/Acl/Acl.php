<?php
namespace Sl\Acl;

/**
 * Список контроля доступа
 *
 */

class Acl  extends \Zend_Acl {
    
    /**
     * Проверка прав доступа
     *
     * @param mixed $role
     * @param mixed $resource
     * @param mixed $privilege
     * @return boolean
     */
    public function isAllowed($role = null, $resource = null, $privilege = null) {
        $resource_name = false;
        // Строим имя ресурса
        if (is_string($resource)) {
            $resource_name = $resource;
        } elseif (is_array($resource) && $resource[0] instanceof \Sl_Model_Abstract) {
            $resource_name = $resource[0]->buildResourceName($resource[1]);
        }

        if (!$resource_name || !$this -> has($resource_name))
            return false;
        
        return parent::isAllowed($role, $resource_name, $privilege);
    }
    
}
