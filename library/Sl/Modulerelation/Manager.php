<?php

use Sl_Module_Manager as ModuleManager;

class Sl_Modulerelation_Manager {
    
    protected static $_instance;
    
    /**
     *
     * @var \Sl\Modulerelation\Modulerelation[]
     */
    protected static $_relations=array();
    
    protected static $_config;
    const CONFIG_PATH = '../application/configs/modulerelations.php';
    const CONFIG_BASE_KEY = 'config';
    const CONFIG_MODELS_KEY = 'models';
    
	const RELATION_ONE_TO_ONE = 11;
	const RELATION_ONE_TO_MANY = 12;
	const RELATION_MANY_TO_ONE = 21;
	const RELATION_MANY_TO_MANY = 22;
	
	const RELATION_ITEM_OWNER = 2;
	const RELATION_MODEL_ITEM = 20;
    
    const RELATION_FILE_ONE = 3;
    const RELATION_FILE_MANY = 4;
	
	const RELATION_FIELD_PREFIX = 'modulerelation';
	const RELATION_FIELD_SEPARATOR = '_';  
    const SELFRELATION_PREFIX = 'reverse';
    
    protected function __construct() {
        try {
            $this->_config = self::getConfig()->toArray();
        } catch(\Exception $e) {
            throw new \Exception ('Can not open modulerelations config file '.self::CONFIG_PATH, $e);
        }
        
    }
    
    public static function init() {
        $cache_id = 'relationmanager_relations';
        if(!\Sl\Service\Cache::test($cache_id)) {
            foreach(ModuleManager::getModules() as $module) {
                $module->registerModulerelations();
            }
            \Sl\Service\Cache::save(self::$_relations, $cache_id);
        } else {
            self::$_relations = \Sl\Service\Cache::load($cache_id);
        }
    }
    
   /**
     *
     * @return Zend_Config
    */
    public static function getConfig() {
        try {
            self::$_config = new \Zend_Config(require self::CONFIG_PATH, true);
            return self::$_config;
        } catch(\Exception $e) {
            die('Can\'t read modulerelations config file');
        }
    }

      /**
     * Вертає назви зв'язків по моделі
     * 
     * @param $object - об'єкт моделі
     * @param $option
     * @return \Sl\Modulerelation\Modulerelation[] масив назв зв'язків
     */
   public static function getRelationsByOption($object, $option, $option_values = array()) {
            $option_values = is_array($option_values)?$option_values:array($option_values);
            $object_class = $object instanceof \Sl_Model_Abstract? get_class($object):trim($object);
      
            foreach(self::$_relations[$object_class] as $relation_name => $relation){
                if (!$relation->getOption($option)) continue;
                if (count($option_values) && !is_array($relation->getOption($option)) && !in_array($relation->getOption($option),$option_values)) continue;
                if (count($option_values) && is_array($relation->getOption($option)) && !array_intersect($relation->getOption($option),$option_values)) continue;
                //if (count($type) && !in_array($relation->getType(),$type)) continue;      
                if (!is_array($relations_arr)) $relations_arr=array();
                $relations_arr[$relation_name]=$relation;
            }
            
            return $relations_arr;
   }
    
	/**
     * Вертає назви зв'язків по моделі
	 * 
     * @param Sl_Model_Abstract $Obj - об'єкт моделі
	 * @return \Sl\Modulerelation\Modulerelation[] масив назв зв'язків
	 */
	public static function getRelations(\Sl_Model_Abstract $Obj = null, $rel_name = null) {
       //  error_reporting(E_ALL);
        if(is_null($Obj)) {
            $rels = array();
            foreach(array_keys(self::$_relations) as $modelClass) {
                $rels[$modelClass] = self::getRelations(new $modelClass());
            }
            return $rels;
        }
        if(is_null($rel_name)) {
            $rels = self::$_relations[get_class($Obj)];
            foreach ($rels as $modulerelation){
                if (is_array($modulerelation)) self::createModulerelation($modulerelation);
            }
            return (isset(self::$_relations[get_class($Obj)])) ? self::$_relations[get_class($Obj)] : array();
        } else {
        	$rel_name = mb_strtolower($rel_name);
            if(isset(self::$_relations[get_class($Obj)][$rel_name])) {
                if (is_array(self::$_relations[get_class($Obj)][$rel_name])) self::createModulerelation(self::$_relations[get_class($Obj)][$rel_name]);


                
                return self::$_relations[get_class($Obj)][$rel_name];
            }
            return array();
        }
    }
   
   /**
     * Вертає зв'язок між об'єктом і іншою моделлю
	 * 
     * @param Sl_Model_Abstract $object - об'єкт або клас моделі
     * @param Sl_Model_Abstract $destination_object - об'єкт або клас моделі
     * @param array $type - встановленого типу
     * @param string $option - зі встановленим параметром
	 * @return \Sl\Modulerelation\Modulerelation[] масив назв зв'язків
	 */
	public static function getObjectsRelations($object, $destination_object, $type = array(), $option = null) {
	    if (!is_array($type)) $type = array();
		$object_class = $object instanceof \Sl_Model_Abstract? get_class($object):trim($object);
		$destination_object_class = $destination_object instanceof \Sl_Model_Abstract? get_class($destination_object):trim($destination_object);
        if (strlen($object_class) && strlen($destination_object_class) && isset(self::$_relations[$object_class]) && count(self::$_relations[$object_class])){
        	$relations_arr = null;	
            $rels = self::$_relations[$object_class];
            foreach ($rels as $modulerelation){
                if (is_array($modulerelation))
                self::createModulerelation($modulerelation);
            }
        	foreach(self::$_relations[$object_class] as $relation_name => $relation){
        		if ($relation->getRelatedObject($object_class) instanceof $destination_object_class){
        			if ($option !== null && !$relation->getOption($option)) continue;
                    if (count($type) && !in_array($relation->getType(),$type)) continue;      
        			if (!is_array($relations_arr)) $relations_arr=array();
					$relations_arr[$relation_name]=$relation;
        		}
        	}
			
			return $relations_arr;
        }
        
    }
   
   
    protected static function _invertType($type){
        if ($type == self::RELATION_ONE_TO_MANY) return self::RELATION_MANY_TO_ONE;
    	if ($type == self::RELATION_MANY_TO_ONE) return self::RELATION_ONE_TO_MANY;
    	if ($type == self::RELATION_ITEM_OWNER) return self::RELATION_MODEL_ITEM;
    	if ($type == self::RELATION_MODEL_ITEM) return self::RELATION_ITEM_OWNER;
		return $type;
    }
   
    protected static function _buildInvertName(\Sl\Modulerelation\Modulerelation $relation){
        $name = $relation->getName();    
        if ($relation->isSelfRelation()){
            if (strpos($name,self::SELFRELATION_PREFIX) === 0){
                $name=substr($name,strlen(self::SELFRELATION_PREFIX));
            } else {
                $name=self::SELFRELATION_PREFIX.$name;
            }
        }
        return $name;
    }
    
    public static function invertRelation(\Sl\Modulerelation\Modulerelation $relation){
        $type = self::_invertType($relation->getType());
        $name = self::_buildInvertName($relation);
        $new_relation = clone $relation;
        $new_relation ->setType($type)->setName($name);
        return $new_relation;
    }
   
   
   
   	public static function getInstance() {
		if (!isset(self::$_instance)) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}
	
	public static function setModulerelation(\Sl\Modulerelation\Modulerelation $Modulerelation, $model_key){
	           
			$model_key = ($Modulerelation->isSelfRelation() && $model_key==self::SELFRELATION_PREFIX)?$Modulerelation->getDbTable()->findRelatedModelsKeys($model_key):$model_key;    
			if (!isset(self::$_relations[$model_key]))self::$_relations[$model_key]=array();
			self::$_relations[$model_key][$Modulerelation->getName()]=$Modulerelation;
		    
		/*	
		$related_models_keys=$Modulerelation->getDbTable()->findRelatedModelsKeys();
		foreach($related_models_keys as $model_key){
			if (!isset(self::$_relations[$model_key]))self::$_relations[$model_key]=array();
			self::$_relations[$model_key][]=$Modulerelation; 
		} */
        
	}
    public static function buildModulerelationAlias($name) {
		return self::RELATION_FIELD_PREFIX . self::RELATION_FIELD_SEPARATOR . strtolower($name);
                
    }
    public static function buildModulerelationName($classname){
        
        $name_array = explode('\\', $classname);
        $name = array_pop($name_array);

        return strtolower($name);
    }
    
    protected static function createModulerelation(array $modulerelation, $return_models = false){
        $manager = self::getInstance();    
        $related_models_keys=\Sl\Service\DbTable::get($modulerelation['db_table'])->findRelatedModelsKeys();
            
        $options = (isset($modulerelation['options'])&& is_array($modulerelation['options']))?$modulerelation['options']:array();
            
        $new_relation = new \Sl\Modulerelation\Modulerelation($modulerelation['type'], 
                                                                  \Sl\Service\DbTable::get($modulerelation['db_table']), 
                                                                  $options,
                                                                  isset($modulerelation['custom_configs'])?(bool) $modulerelation['custom_configs']:false);
           
        $model = array_shift($related_models_keys);
        $models = array($model);   
        $manager -> setModulerelation($new_relation, $model);
            
        $new_relation = $manager -> invertRelation($new_relation); 
        
        $model = array_shift($related_models_keys);
        $models[] = $model;              
        $manager -> setModulerelation($new_relation, $model);
        if ($return_models){
            return $models;
        }
    }
    
    public function setModulerelations(array $modulerelations, \Sl_Module_Abstract $module, $rebuild_modulerelations_info = false){
        
        $to_update = false;
        $module_name = $module->getName();
        
        if (!isset($this->_config[$module_name])){
            $this->_config[$module_name] = array();
            $to_update = true;
        }       
        
        foreach ($modulerelations as $modulerelation => $config_arr) {
            if (!isset($config_arr['db_table']) || !isset($config_arr['type'])){
                throw new \Exception ('Invalid config for modulerelation in '.mb_strtoupper($module));
            }
            $mr_name = self::buildModulerelationName($config_arr['db_table']);
            if ($rebuild_modulerelations_info || 
                !isset($this->_config[$module_name][$mr_name]) || 
                    count(array_intersect($this->_config[$module_name][$mr_name][self::CONFIG_BASE_KEY] , $config_arr)) != 
                    count($this->_config[$module_name][$mr_name][self::CONFIG_BASE_KEY])) {
                    
                $to_update = true;
                $models = self::createModulerelation($config_arr, true);
                
                $this->_config[$module_name][$mr_name] = array();
                $this->_config[$module_name][$mr_name][self::CONFIG_MODELS_KEY] = $models; 
                $this->_config[$module_name][$mr_name][self::CONFIG_BASE_KEY] = $config_arr;
                
            } else {
                list($model1, $model2) = $this->_config[$module_name][$mr_name][self::CONFIG_MODELS_KEY]; 
                self::$_relations[$model1][$mr_name] = $this->_config[$module_name][$mr_name][self::CONFIG_BASE_KEY];
                 
                if ($model2==self::SELFRELATION_PREFIX){
                    $mr_name =  self::SELFRELATION_PREFIX.$mr_name;
                    $model2 = $model1;   
                }
                @self::$_relations[$model2][$mr_name] = @self::$_relations[$model1][$mr_name];
                 
            } 
                
                
                
        }
        
        if ($to_update) {
             
            
            $writer = new \Zend_Config_Writer_Array( array('config' =>new \Zend_Config($this->_config)));

            try {
                $writer -> write(self::CONFIG_PATH);
            } catch(\Exception $e) {
                throw new \Exception('Can not save modularelations config: '.$e->getMessage());
            }
            
        } 

        
    }
    
    /**
     * Ищем ограничивающую связь user с объектом, если такая есть
     * 
     * @param \Sl_Model_Abstract $object
     * @return \Sl\Modulerelation\Modulerelation
     */
    public static function findHandlingRelation(\Sl_Model_Abstract $object) {
    	
        $relation = null;
        $user = \Zend_Auth::getInstance()->getIdentity();
		if ($user instanceof \Sl_Model_Abstract){
	        foreach(self::getRelations($user) as $rel) {
	            if($rel->getHandling()) {
	                if(get_class($object) == get_class($rel->getRelatedObject($user))) {
	                    return $rel;
	                }
	            }
	        }
		}
        return $relation;
    }
    
     /**
     * Повертає назви всі керуючі зв'язків об'єкта
     * 
     * @param \Sl_Model_Abstract $object
     * @return array 
     */
    public static function findHandlingRelations(\Sl_Model_Abstract $object) {
        $relations = array();
        
        foreach(self::getRelations($object) as $rel) {
            if($rel->getHandling()) {
               $relations[get_class($rel->getRelatedObject($object))]=$rel->getName();
            }
        }
        return $relations;
    }
	
		
	 /**
     * Повертає назви всіх зв'язків з можливістю кастомізації конфігів
     * 
     * @param \Sl_Model_Abstract $object
     * @return array 
     */
    public static function findCustomConfigsRelations(\Sl_Model_Abstract $object) {
        $relations = array();
        
        foreach(self::getRelations($object) as $rel) {
            	
            if($rel->getCustomConfigs()) {
            		
            	
               $relations[get_class($rel->getRelatedObject($object))]=$rel->getName();
            }
        }
        return $relations;
    }
    
    public static function relationExists($relation_name, \Sl_Model_Abstract $model = null) {
        $result = false;
        if(!is_null($model)) {
            $result = isset(self::$_relations[get_class($model)][$relation_name]);
        } else {
            foreach(self::$_relations as $rels) {
                if($result) break;
                $result |= isset($rels[$relation_name]);
            }
        }
        return $result;
    }
    
    public static function getInvertion(\Sl\Modulerelation\Modulerelation $relation, \Sl_Model_Abstract $Object) {
        if ($relation->isSelfRelation()){
            $rel_name = self::_buildInvertName($relation);
            $r = self::getRelations($Object, $rel_name);
            
            if ($r instanceof \Sl\Modulerelation\Modulerelation) return $r;
        } 
        
        return $relation;
    }
    
}
?>
