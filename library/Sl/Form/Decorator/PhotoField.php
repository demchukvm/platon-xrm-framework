<?php

namespace Sl\Form\Decorator;

class PhotoField extends \Zend_Form_Decorator_HtmlTag {

    public function render($content) {
        $placement = $this->getPlacement();
        $this->removeOption('placement');

        $attribs = $this->getOptions();

        switch ($placement) {
            case self::PREPEND:
                return $this->_buildList($attribs)
                        . $content;
            case self::APPEND:
            default:
                return $content
                        . $this->_buildList($attribs);
        }
    }

    protected function _buildList($attribs = array()) {
       
        $items = $this->getOption('items');
        if (!is_array($items)) {
            $items = array();
        }
$translator = \Zend_Registry::get('Zend_Translate');
        if (count($items) > 0) {
            $photo_model = \Sl_Model_Factory::mapper('file', \Sl_Module_Manager::getInstance()->getModule('home'))->find($items[0]['id']);
            
            if (file_exists($photo_model->getLocation())) {
                $text = '<div class="thumbnail_image full_photo">
                                <div class="remove_avatar" title="'.$translator->translate('Удалить фото').'"><i class="icon-remove"></i></div>
                            <img src="' . \Sl\Service\Helper::modelEditViewUrl($photo_model) . '" title="'.$translator->translate('Увеличить фото').'" class="img-polaroid">
                        </div>';
            }
            else {
            $text = '<div class="thumbnail_image no_avatar">
                 <div class="remove_avatar"><i class="icon-remove"></i></div>
                <img src="/img/no_avatar.png" class="img-polaroid" title="'.$translator->translate('Добавить фото').'" ></div>';
        }
        } else {
            $text = '<div class="thumbnail_image no_avatar">
 <div class="remove_avatar"><i class="icon-remove"></i></div>                    
<img src="/img/no_avatar.png" class="img-polaroid" title="'.$translator->translate('Добавить фото').'" >
                
                </div>';
        }
        return $text;
    }

}