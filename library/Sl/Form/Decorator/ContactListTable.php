<?php

namespace Sl\Form\Decorator;

class ContactListTable extends \Zend_Form_Decorator_HtmlTag {

    public function render($content) {
        
        $placement = $this->getPlacement();
        $this->removeOption('placement');

        $attribs = $this->getOptions();
        
        switch ($placement) {
            case self::PREPEND:
                return $this->_buildList($attribs)
                        . $content;
            case self::APPEND:
            default:
                return $content
                        . $this->_buildList($attribs);
        }
    }

    protected function _buildList($attribs = array()) {
        $items = $this->getOption('items');
        $labels = $this->getOption('field_label');
        $model = $this->getOption('model');
        
//       die;
        if (!is_array($items)) {
            $items = array();
        }
        
        $attr_string = '';
        if (count($attribs)) {
            $attr_string = $this->_htmlAttribs($attribs);
        }
        
        $relatedModel = $this->getOption('relatedModel');
        if(!$relatedModel || !($relatedModel instanceof \Sl_Model_Abstract)) {
            
            throw new \Exception('No related model set. '.__METHOD__);
        }
        $priv_delete = \Sl_Service_Acl::isAllowed(\Sl_Service_Acl::joinResourceName(array(
                                'type' => \Sl_Service_Acl::RES_TYPE_MVC,
                                'module' => $relatedModel->findModuleName(),
                                'controller' => $relatedModel->findModelName(),
                                'action' => \Sl\Service\Helper::AJAX_DELETE_ACTION,
                            )), \Sl_Service_Acl::PRIVELEGE_ACCESS);
        
        $priv_edit = \Sl_Service_Acl::isAllowed(\Sl_Service_Acl::joinResourceName(array(
                                'type' => \Sl_Service_Acl::RES_TYPE_MVC,
                                'module' => $relatedModel->findModuleName(),
                                'controller' => $relatedModel->findModelName(),
                                'action' => \Sl\Service\Helper::POPUP_EDIT_ACTION,
                            )), \Sl_Service_Acl::PRIVELEGE_ACCESS);
        
        
       
        $tr_tpl = ''.
        ($this->getOption('readonly')?'<td class=\'set-radio\'></td>':'<td class=\'set-radio\'><input type=\'radio\' name=\'essential\' value=\'{id}\'>');
       
        foreach($this->getOption('returnfields') as $field){
            $tr_tpl.='<td class=\''.str_replace('.','-', $field).'\' data-value=\'{'.str_replace('.','-', $field).'}\'>{'.str_replace('.','-', $field).'}</td>';
        }
        
        if($priv_edit) {
            $ajaxedit_url = \Sl\Service\Helper::buildModelUrl($relatedModel, \Sl\Service\Helper::POPUP_EDIT_ACTION);
            $relation = \Sl_Modulerelation_Manager::getRelations($relatedModel, $attribs['data-name']);
            $relation = \Sl_Modulerelation_Manager::invertRelation($relation);
            $tr_tpl .= '<td class=\'fa-hover files_list_table_edit\' data-id=\'{id}\'><i class=\'ajax_edit_modulerelation fa fa-pencil\' data-iframe=\'0\' data-type=\''.$relation->getType().'\' data-name=\'modulerelation_'.$attribs['data-name'].'\' data-rel=\''.$ajaxedit_url.'/id/{id}\' data-returnfields=\''.implode(',', $this->getOption('returnfields')).'\'></i></td>';
        }
        if($priv_delete) {
            $tr_tpl .= '<td class=\'files_list_table_delete\' data-id=\'{id}\'><i class=\'fa fa-trash-o\'></i></td>';
        }
        
        $text_header = '<div class="well wellform">
       
       

       <table class="datatable table table-striped table-bordered dataTable blockquote" ' . $attr_string .' data-tpl="'.$tr_tpl.'"'. '>';
        
        $essentional = array_keys($model->fetchEssentials($this->getOption('relation')->getName()));
        //print_r(array($this->getOption('relation')->getName(), $essentional, $model));
        //die;
        foreach ($items as $item) {

            $object = $item['object'];            
            $model = \Sl\Service\Helper::getModelByExtend($object->getExtend());
            $object = \Sl_Model_Factory::mapper($model)->find($object->getId(), true,false);
          
            $item['href'] = \Sl\Service\Helper::getToEditUrl($object);
            $text_row .= '<tr>';
            
            if (!$this->getOption('readonly')){
                $text_row .= '<td class="set-radio"><input name="essential" type="radio" value="'.$object->getId().'"'.(in_array($object->getId(), $essentional)?' checked="checked" ':'').'></td>';
            } else {
                $text_row .= '<td class="set-radio">'.(in_array($object->getId(), $essentional)?' + ':'').'</td>';
            }
                    
            $first_col = false;
            $i = 0;
            foreach ($item['fields'] as $name) {
                $i++;
                if ($first_col) {
                   
                    $text_row .= '<td class="tostring" data-value="' . $object->__toString() . '" data-url="' . $item['href'] . '" ><a href="' . $item['href'] . '" ' . ($item['target'] ? ' target="' . $item['target'] . '"' : '') . '>' . $object->__toString() . '</a></td>';
                    
                }
                $field = explode('.', $name);
                if (count($field) > 1) {
                    $priv_read = \Sl_Service_Acl::isAllowed(array( $object, $field[0] ), \Sl_Service_Acl::PRIVELEGE_READ);

                    if ($priv_read) {
                        if(!$object->issetRelated( $field[0] )) {
                            $object = \Sl_Model_Factory::mapper($object)->findExtended($object->getId(), $field[0]);
                        }
                        
                        $rel_obj = $object->fetchRelated($field[0]);

                        $field_val = array();
                        $field_url = array();
                        if(count($rel_obj)) {
                            foreach ($rel_obj as $r_o) {
                                if (strtolower($r_o->findModelName()) == 'file') {

                                    $text_row .= '<td class="' . str_replace('.', '-', $name) . '">
                                        <span data-url="/file/detailed/id/' . $r_o->getId() . '" data-value="' . $r_o->Lists($field[1]) . '">
                                            <a href="/file/detailed/id/' . $r_o->getId() . '" target="_blank">' . $r_o->Lists($field[1]) . '</a>
                                        </span>
                                        </td>';
                                } else {
                                    $field_val[] = $r_o->Lists($field[1]);
                                    $field_url[] = \Sl\Service\Helper::modelEditViewUrl($r_o);
                                    $text_row .= '<td class="' . str_replace('.', '-', $name) . '">
                                    <span data-url="' . implode(', ', $field_url) . '" data-value="' . implode(', ', $field_val) . '">' . implode(', ', $field_val) . '</span>
                                         </td>';
                                }
                            }
                        } else { 
                            $text_row .= '<td class="' . str_replace('.', '-', $name) . '"></td>';
                        }
                    } else {
                        $text_row .= '<td class="' . str_replace('.', '-', $name) . '"></td>';
                        unset($labels[$i]);
                    }
                } else {
                    $priv_read = \Sl_Service_Acl::isAllowed(array(
                                $object,
                                $name
                                    ), \Sl_Service_Acl::PRIVELEGE_READ);
                    
                    if ($priv_read) {
                        if ($name === 'paid') {
                             if ($object->Lists($name) == '1') {$active = 'active';}
                                else {$active = '';}
                             $text_row .= '<td class="fa-hover ' . $name . '" data-value="' . $object->Lists($name) . '" data-id="' . $object->getId() . '"><a data-toggle="class" class="' . $active . '" href="#"><i class="fa fa-check-square-o fa-lg text-active"></i><i class="fa fa-minus-square-o fa-lg text"></i></a></td>';
                        
                        } else {
                            $text_row .= '<td class="' . $name . '" data-value="' . $object->Lists($name) . '">' . $object->Lists($name) . '</td>';
                        }
                    } else {
                        $text_row .= '<td class="' . $name . '"></td>';
                        unset($labels[$i]);
                    }
                }
                $first_col = false;
            }
            
            if($priv_edit) {
               
                $text_row .= '<td class="fa-hover files_list_table_edit" data-id="'.$object->getId().'"><i class="ajax_edit_modulerelation fa fa-pencil" data-iframe="0" data-type="'.$relation->getType().'" data-name="modulerelation_'.$attribs['data-name'].'" data-rel="'.$ajaxedit_url.'/id/'.$object->getId().'" data-returnfields="'.implode(',', $item['fields']->toArray()).'"></i></td>';
            }
            
            if($priv_delete) {
                $text_row .= '<td class="files_list_table_delete" data-id="'.$object->getId().'"><i class="fa fa-trash-o"></i></td>';
            }
            
            $text_row .= '</tr>';
        }
        $text_thead .= '<thead><tr>';
        
        
        $text_thead .= '<th class="essential">Main</th>';
        
        
        foreach ($labels as $label) {
            $text_thead .= '<th>' . $label . '</th>';
        }
        
        if($priv_edit) {
            $text_thead .= '<th class="ajax-editable" data-alias="'.\Sl\Service\Helper::getModelAlias($relatedModel).'"></th>';
        }
        if($priv_delete) {
            $text_thead .= '<th class="ajax-deletable" data-alias="'.\Sl\Service\Helper::getModelAlias($relatedModel).'"></th>';
        }
        $text_thead .= '</tr></thead><tbody>';

        $text_footer .= '</tbody></table></div>';
        $text = $text_header . $text_thead . $text_row . $text_footer;
        return $text;
    }

}