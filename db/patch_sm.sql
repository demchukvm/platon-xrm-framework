ALTER TABLE `logistic_links` ADD `delivery_notify` TINYINT( 4 ) NOT NULL AFTER `necessity`;
ALTER TABLE  `logistic_packing_list` ADD  `switch` SMALLINT( 4 ) NOT NULL AFTER  `identifier`;
ALTER TABLE  `logistic_packinglist_item` ADD  `product_code` VARCHAR( 255 ) NOT NULL AFTER  `gross_weight`;