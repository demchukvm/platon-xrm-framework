ALTER TABLE  `logistic_packinglist_item` ADD  `net_weight` FLOAT( 12, 2 ) NOT NULL AFTER  `plcs`;
ALTER TABLE  `logistic_packinglist_item` ADD  `gross_weight` FLOAT( 12, 2 ) NOT NULL AFTER  `net_weight`;
