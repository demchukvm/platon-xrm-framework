<?php
return array (
  'permissions' => 
  array (
    'id' => 
    array (
      'table_name' => 'permissions',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'resource_id' => 
    array (
      'table_name' => 'permissions',
      'column_name' => 'resource_id',
      'column_position' => 2,
      'data_type' => 'int',
    ),
    'role_id' => 
    array (
      'table_name' => 'permissions',
      'column_name' => 'role_id',
      'column_position' => 3,
      'data_type' => 'int',
    ),
    'privilege' => 
    array (
      'table_name' => 'permissions',
      'column_name' => 'privilege',
      'column_position' => 4,
      'data_type' => 'smallint',
    ),
    'assert' => 
    array (
      'table_name' => 'permissions',
      'column_name' => 'assert',
      'column_position' => 5,
      'data_type' => 'text',
    ),
    'active' => 
    array (
      'table_name' => 'permissions',
      'column_name' => 'active',
      'column_position' => 6,
      'data_type' => 'tinyint',
    ),
    'create' => 
    array (
      'table_name' => 'permissions',
      'column_name' => 'create',
      'column_position' => 7,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'last_update' => 
    array (
      'table_name' => 'permissions',
      'column_name' => 'last_update',
      'column_position' => 8,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'archived' => 
    array (
      'table_name' => 'permissions',
      'column_name' => 'archived',
      'column_position' => 9,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
  ),
  'resources' => 
  array (
    'id' => 
    array (
      'table_name' => 'resources',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'name' => 
    array (
      'table_name' => 'resources',
      'column_name' => 'name',
      'column_position' => 2,
      'data_type' => 'varchar',
      'length' => '250',
    ),
    'description' => 
    array (
      'table_name' => 'resources',
      'column_name' => 'description',
      'column_position' => 3,
      'data_type' => 'text',
      'nullable' => true,
    ),
    'active' => 
    array (
      'table_name' => 'resources',
      'column_name' => 'active',
      'column_position' => 4,
      'data_type' => 'tinyint',
    ),
    'create' => 
    array (
      'table_name' => 'resources',
      'column_name' => 'create',
      'column_position' => 5,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'last_update' => 
    array (
      'table_name' => 'resources',
      'column_name' => 'last_update',
      'column_position' => 6,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
  ),
  'auth_restriction' => 
  array (
    'id' => 
    array (
      'table_name' => 'auth_restriction',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'auth_restriction',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'create' => 
    array (
      'table_name' => 'auth_restriction',
      'column_name' => 'create',
      'column_position' => 3,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'auth_restriction',
      'column_name' => 'timestamp',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'name' => 
    array (
      'table_name' => 'auth_restriction',
      'column_name' => 'name',
      'column_position' => 5,
      'data_type' => 'varchar',
      'length' => '250',
    ),
    'main_object' => 
    array (
      'table_name' => 'auth_restriction',
      'column_name' => 'main_object',
      'column_position' => 6,
      'data_type' => 'varchar',
      'length' => '250',
    ),
    'rules' => 
    array (
      'table_name' => 'auth_restriction',
      'column_name' => 'rules',
      'column_position' => 7,
      'data_type' => 'text',
    ),
    'type' => 
    array (
      'table_name' => 'auth_restriction',
      'column_name' => 'type',
      'column_position' => 8,
      'data_type' => 'smallint',
    ),
    'null_include' => 
    array (
      'table_name' => 'auth_restriction',
      'column_name' => 'null_include',
      'column_position' => 9,
      'data_type' => 'smallint',
    ),
  ),
  'roles' => 
  array (
    'id' => 
    array (
      'table_name' => 'roles',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'name' => 
    array (
      'table_name' => 'roles',
      'column_name' => 'name',
      'column_position' => 2,
      'data_type' => 'varchar',
      'length' => '50',
    ),
    'parent' => 
    array (
      'table_name' => 'roles',
      'column_name' => 'parent',
      'column_position' => 3,
      'data_type' => 'varchar',
      'nullable' => true,
      'length' => '250',
    ),
    'description' => 
    array (
      'table_name' => 'roles',
      'column_name' => 'description',
      'column_position' => 4,
      'data_type' => 'text',
    ),
    'active' => 
    array (
      'table_name' => 'roles',
      'column_name' => 'active',
      'column_position' => 5,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'create' => 
    array (
      'table_name' => 'roles',
      'column_name' => 'create',
      'column_position' => 6,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'last_update' => 
    array (
      'table_name' => 'roles',
      'column_name' => 'last_update',
      'column_position' => 7,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
  ),
  'auth_users' => 
  array (
    'id' => 
    array (
      'table_name' => 'auth_users',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'name' => 
    array (
      'table_name' => 'auth_users',
      'column_name' => 'name',
      'column_position' => 2,
      'data_type' => 'varchar',
      'nullable' => true,
      'length' => '250',
    ),
    'email' => 
    array (
      'table_name' => 'auth_users',
      'column_name' => 'email',
      'column_position' => 3,
      'data_type' => 'varchar',
      'length' => '250',
    ),
    'password' => 
    array (
      'table_name' => 'auth_users',
      'column_name' => 'password',
      'column_position' => 4,
      'data_type' => 'char',
      'nullable' => true,
      'length' => '32',
    ),
    'active' => 
    array (
      'table_name' => 'auth_users',
      'column_name' => 'active',
      'column_position' => 5,
      'data_type' => 'tinyint',
    ),
    'login' => 
    array (
      'table_name' => 'auth_users',
      'column_name' => 'login',
      'column_position' => 6,
      'data_type' => 'varchar',
      'length' => '100',
    ),
    'phone' => 
    array (
      'table_name' => 'auth_users',
      'column_name' => 'phone',
      'column_position' => 7,
      'data_type' => 'varchar',
      'length' => '200',
    ),
    'role_id' => 
    array (
      'table_name' => 'auth_users',
      'column_name' => 'role_id',
      'column_position' => 8,
      'data_type' => 'int',
    ),
    'create' => 
    array (
      'table_name' => 'auth_users',
      'column_name' => 'create',
      'column_position' => 9,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'last_update' => 
    array (
      'table_name' => 'auth_users',
      'column_name' => 'last_update',
      'column_position' => 10,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
  ),
  'cust_customers' => 
  array (
    'id' => 
    array (
      'table_name' => 'cust_customers',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'cust_customers',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'name' => 
    array (
      'table_name' => 'cust_customers',
      'column_name' => 'name',
      'column_position' => 3,
      'data_type' => 'varchar',
      'length' => '200',
    ),
    'address' => 
    array (
      'table_name' => 'cust_customers',
      'column_name' => 'address',
      'column_position' => 4,
      'data_type' => 'text',
    ),
    'passport' => 
    array (
      'table_name' => 'cust_customers',
      'column_name' => 'passport',
      'column_position' => 5,
      'data_type' => 'varchar',
      'length' => '20',
    ),
    'passport_date' => 
    array (
      'table_name' => 'cust_customers',
      'column_name' => 'passport_date',
      'column_position' => 6,
      'data_type' => 'date',
    ),
    'create' => 
    array (
      'table_name' => 'cust_customers',
      'column_name' => 'create',
      'column_position' => 7,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'cust_customers',
      'column_name' => 'timestamp',
      'column_position' => 8,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'attracts' => 
    array (
      'table_name' => 'cust_customers',
      'column_name' => 'attracts',
      'column_position' => 9,
      'data_type' => 'int',
    ),
    'skype' => 
    array (
      'table_name' => 'cust_customers',
      'column_name' => 'skype',
      'column_position' => 10,
      'data_type' => 'varchar',
      'length' => '20',
    ),
    'qq' => 
    array (
      'table_name' => 'cust_customers',
      'column_name' => 'qq',
      'column_position' => 11,
      'data_type' => 'varchar',
      'length' => '20',
    ),
    'notify_email' => 
    array (
      'table_name' => 'cust_customers',
      'column_name' => 'notify_email',
      'column_position' => 12,
      'data_type' => 'tinyint',
    ),
    'notify_sms' => 
    array (
      'table_name' => 'cust_customers',
      'column_name' => 'notify_sms',
      'column_position' => 13,
      'data_type' => 'tinyint',
    ),
    'post_code' => 
    array (
      'table_name' => 'cust_customers',
      'column_name' => 'post_code',
      'column_position' => 14,
      'data_type' => 'varchar',
      'nullable' => true,
      'length' => '12',
    ),
    'first_name' => 
    array (
      'table_name' => 'cust_customers',
      'column_name' => 'first_name',
      'column_position' => 15,
      'data_type' => 'varchar',
      'length' => '30',
    ),
    'last_name' => 
    array (
      'table_name' => 'cust_customers',
      'column_name' => 'last_name',
      'column_position' => 16,
      'data_type' => 'varchar',
      'length' => '30',
    ),
    'middle_name' => 
    array (
      'table_name' => 'cust_customers',
      'column_name' => 'middle_name',
      'column_position' => 17,
      'data_type' => 'varchar',
      'length' => '30',
    ),
    'notify_dealer' => 
    array (
      'table_name' => 'cust_customers',
      'column_name' => 'notify_dealer',
      'column_position' => 18,
      'data_type' => 'tinyint',
    ),
    'manual_identifier_edition' => 
    array (
      'table_name' => 'cust_customers',
      'column_name' => 'manual_identifier_edition',
      'column_position' => 19,
      'data_type' => 'tinyint',
    ),
    'description' => 
    array (
      'table_name' => 'cust_customers',
      'column_name' => 'description',
      'column_position' => 20,
      'data_type' => 'text',
      'nullable' => true,
    ),
    'is_dealer' => 
    array (
      'table_name' => 'cust_customers',
      'column_name' => 'is_dealer',
      'column_position' => 21,
      'data_type' => 'tinyint',
    ),
    'sender_phone' => 
    array (
      'table_name' => 'cust_customers',
      'column_name' => 'sender_phone',
      'column_position' => 22,
      'data_type' => 'varchar',
      'length' => '100',
    ),
    'company_name' => 
    array (
      'table_name' => 'cust_customers',
      'column_name' => 'company_name',
      'column_position' => 23,
      'data_type' => 'varchar',
      'length' => '100',
    ),
    'web_site' => 
    array (
      'table_name' => 'cust_customers',
      'column_name' => 'web_site',
      'column_position' => 24,
      'data_type' => 'varchar',
      'length' => '100',
    ),
    'contact' => 
    array (
      'table_name' => 'cust_customers',
      'column_name' => 'contact',
      'column_position' => 25,
      'data_type' => 'varchar',
      'length' => '250',
    ),
  ),
  'cust_customer_source' => 
  array (
    'id' => 
    array (
      'table_name' => 'cust_customer_source',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'cust_customer_source',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'name' => 
    array (
      'table_name' => 'cust_customer_source',
      'column_name' => 'name',
      'column_position' => 3,
      'data_type' => 'varchar',
      'nullable' => true,
      'length' => '20',
    ),
    'create' => 
    array (
      'table_name' => 'cust_customer_source',
      'column_name' => 'create',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'cust_customer_source',
      'column_name' => 'timestamp',
      'column_position' => 5,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
  ),
  'cust_dealers' => 
  array (
    'id' => 
    array (
      'table_name' => 'cust_dealers',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'cust_dealers',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'type' => 
    array (
      'table_name' => 'cust_dealers',
      'column_name' => 'type',
      'column_position' => 3,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'name' => 
    array (
      'table_name' => 'cust_dealers',
      'column_name' => 'name',
      'column_position' => 4,
      'data_type' => 'varchar',
      'length' => '200',
    ),
    'create' => 
    array (
      'table_name' => 'cust_dealers',
      'column_name' => 'create',
      'column_position' => 5,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'cust_dealers',
      'column_name' => 'timestamp',
      'column_position' => 6,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
  ),
  'docs_bills' => 
  array (
    'id' => 
    array (
      'table_name' => 'docs_bills',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'docs_bills',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'name' => 
    array (
      'table_name' => 'docs_bills',
      'column_name' => 'name',
      'column_position' => 3,
      'data_type' => 'varchar',
      'length' => '200',
    ),
    'create' => 
    array (
      'table_name' => 'docs_bills',
      'column_name' => 'create',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'docs_bills',
      'column_name' => 'timestamp',
      'column_position' => 5,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'sum' => 
    array (
      'table_name' => 'docs_bills',
      'column_name' => 'sum',
      'column_position' => 6,
      'data_type' => 'float',
      'default' => '0.00',
      'scale' => '2',
      'precision' => '10',
    ),
    'description' => 
    array (
      'table_name' => 'docs_bills',
      'column_name' => 'description',
      'column_position' => 7,
      'data_type' => 'text',
    ),
    'fin_status' => 
    array (
      'table_name' => 'docs_bills',
      'column_name' => 'fin_status',
      'column_position' => 8,
      'data_type' => 'int',
    ),
  ),
  'docs_billitems' => 
  array (
    'id' => 
    array (
      'table_name' => 'docs_billitems',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'docs_billitems',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'create' => 
    array (
      'table_name' => 'docs_billitems',
      'column_name' => 'create',
      'column_position' => 3,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'docs_billitems',
      'column_name' => 'timestamp',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'name' => 
    array (
      'table_name' => 'docs_billitems',
      'column_name' => 'name',
      'column_position' => 5,
      'data_type' => 'varchar',
      'length' => '250',
    ),
    'sum' => 
    array (
      'table_name' => 'docs_billitems',
      'column_name' => 'sum',
      'column_position' => 6,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '10',
    ),
  ),
  'docs_contracts' => 
  array (
    'id' => 
    array (
      'table_name' => 'docs_contracts',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'docs_contracts',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'name' => 
    array (
      'table_name' => 'docs_contracts',
      'column_name' => 'name',
      'column_position' => 3,
      'data_type' => 'varchar',
      'length' => '200',
    ),
    'create' => 
    array (
      'table_name' => 'docs_contracts',
      'column_name' => 'create',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'docs_contracts',
      'column_name' => 'timestamp',
      'column_position' => 5,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
  ),
  'fin_atransaction' => 
  array (
    'id' => 
    array (
      'table_name' => 'fin_atransaction',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'fin_atransaction',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'create' => 
    array (
      'table_name' => 'fin_atransaction',
      'column_name' => 'create',
      'column_position' => 3,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'fin_atransaction',
      'column_name' => 'timestamp',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'doc_id' => 
    array (
      'table_name' => 'fin_atransaction',
      'column_name' => 'doc_id',
      'column_position' => 5,
      'data_type' => 'int',
      'nullable' => true,
    ),
    'reason_id' => 
    array (
      'table_name' => 'fin_atransaction',
      'column_name' => 'reason_id',
      'column_position' => 6,
      'data_type' => 'int',
      'nullable' => true,
    ),
    'account_id' => 
    array (
      'table_name' => 'fin_atransaction',
      'column_name' => 'account_id',
      'column_position' => 7,
      'data_type' => 'int',
      'nullable' => true,
    ),
    'qty' => 
    array (
      'table_name' => 'fin_atransaction',
      'column_name' => 'qty',
      'column_position' => 8,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '15',
    ),
    'factor' => 
    array (
      'table_name' => 'fin_atransaction',
      'column_name' => 'factor',
      'column_position' => 9,
      'data_type' => 'tinyint',
    ),
    'date' => 
    array (
      'table_name' => 'fin_atransaction',
      'column_name' => 'date',
      'column_position' => 10,
      'data_type' => 'date',
    ),
    'ballance' => 
    array (
      'table_name' => 'fin_atransaction',
      'column_name' => 'ballance',
      'column_position' => 11,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '15',
    ),
    'status' => 
    array (
      'table_name' => 'fin_atransaction',
      'column_name' => 'status',
      'column_position' => 12,
      'data_type' => 'int',
    ),
  ),
  'fin_ballances' => 
  array (
    'id' => 
    array (
      'table_name' => 'fin_ballances',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'fin_ballances',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'create' => 
    array (
      'table_name' => 'fin_ballances',
      'column_name' => 'create',
      'column_position' => 3,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'fin_ballances',
      'column_name' => 'timestamp',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'ballance' => 
    array (
      'table_name' => 'fin_ballances',
      'column_name' => 'ballance',
      'column_position' => 5,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '14',
    ),
  ),
  'fin_bonus' => 
  array (
    'id' => 
    array (
      'table_name' => 'fin_bonus',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'fin_bonus',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'create' => 
    array (
      'table_name' => 'fin_bonus',
      'column_name' => 'create',
      'column_position' => 3,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'fin_bonus',
      'column_name' => 'timestamp',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'name' => 
    array (
      'table_name' => 'fin_bonus',
      'column_name' => 'name',
      'column_position' => 5,
      'data_type' => 'varchar',
      'length' => '100',
    ),
    'sum' => 
    array (
      'table_name' => 'fin_bonus',
      'column_name' => 'sum',
      'column_position' => 6,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '10',
    ),
    'status' => 
    array (
      'table_name' => 'fin_bonus',
      'column_name' => 'status',
      'column_position' => 7,
      'data_type' => 'int',
    ),
    'description' => 
    array (
      'table_name' => 'fin_bonus',
      'column_name' => 'description',
      'column_position' => 8,
      'data_type' => 'text',
    ),
  ),
  'fin_cash_descs' => 
  array (
    'id' => 
    array (
      'table_name' => 'fin_cash_descs',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'fin_cash_descs',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'name' => 
    array (
      'table_name' => 'fin_cash_descs',
      'column_name' => 'name',
      'column_position' => 3,
      'data_type' => 'varchar',
      'length' => '200',
    ),
    'create' => 
    array (
      'table_name' => 'fin_cash_descs',
      'column_name' => 'create',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'fin_cash_descs',
      'column_name' => 'timestamp',
      'column_position' => 5,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'current_ballance' => 
    array (
      'table_name' => 'fin_cash_descs',
      'column_name' => 'current_ballance',
      'column_position' => 6,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '20',
    ),
  ),
  'fin_cdtransaction' => 
  array (
    'id' => 
    array (
      'table_name' => 'fin_cdtransaction',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'fin_cdtransaction',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'create' => 
    array (
      'table_name' => 'fin_cdtransaction',
      'column_name' => 'create',
      'column_position' => 3,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'fin_cdtransaction',
      'column_name' => 'timestamp',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'doc_id' => 
    array (
      'table_name' => 'fin_cdtransaction',
      'column_name' => 'doc_id',
      'column_position' => 5,
      'data_type' => 'int',
      'nullable' => true,
    ),
    'reason_id' => 
    array (
      'table_name' => 'fin_cdtransaction',
      'column_name' => 'reason_id',
      'column_position' => 6,
      'data_type' => 'int',
      'nullable' => true,
    ),
    'account_id' => 
    array (
      'table_name' => 'fin_cdtransaction',
      'column_name' => 'account_id',
      'column_position' => 7,
      'data_type' => 'int',
    ),
    'currency_id' => 
    array (
      'table_name' => 'fin_cdtransaction',
      'column_name' => 'currency_id',
      'column_position' => 8,
      'data_type' => 'int',
    ),
    'rate' => 
    array (
      'table_name' => 'fin_cdtransaction',
      'column_name' => 'rate',
      'column_position' => 9,
      'data_type' => 'float',
      'scale' => '6',
      'precision' => '16',
    ),
    'date' => 
    array (
      'table_name' => 'fin_cdtransaction',
      'column_name' => 'date',
      'column_position' => 10,
      'data_type' => 'date',
    ),
    'factor' => 
    array (
      'table_name' => 'fin_cdtransaction',
      'column_name' => 'factor',
      'column_position' => 11,
      'data_type' => 'tinyint',
    ),
    'qty' => 
    array (
      'table_name' => 'fin_cdtransaction',
      'column_name' => 'qty',
      'column_position' => 12,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '15',
    ),
    'currency_qty' => 
    array (
      'table_name' => 'fin_cdtransaction',
      'column_name' => 'currency_qty',
      'column_position' => 13,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '15',
    ),
    'status' => 
    array (
      'table_name' => 'fin_cdtransaction',
      'column_name' => 'status',
      'column_position' => 14,
      'data_type' => 'int',
    ),
    'ballance' => 
    array (
      'table_name' => 'fin_cdtransaction',
      'column_name' => 'ballance',
      'column_position' => 15,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '15',
    ),
  ),
  'fin_currencies' => 
  array (
    'id' => 
    array (
      'table_name' => 'fin_currencies',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'fin_currencies',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'name' => 
    array (
      'table_name' => 'fin_currencies',
      'column_name' => 'name',
      'column_position' => 3,
      'data_type' => 'varchar',
      'length' => '200',
    ),
    'create' => 
    array (
      'table_name' => 'fin_currencies',
      'column_name' => 'create',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'fin_currencies',
      'column_name' => 'timestamp',
      'column_position' => 5,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'code' => 
    array (
      'table_name' => 'fin_currencies',
      'column_name' => 'code',
      'column_position' => 6,
      'data_type' => 'varchar',
      'length' => '3',
    ),
    'course' => 
    array (
      'table_name' => 'fin_currencies',
      'column_name' => 'course',
      'column_position' => 7,
      'data_type' => 'float',
      'scale' => '6',
      'precision' => '10',
    ),
  ),
  'fin_acnt' => 
  array (
    'id' => 
    array (
      'table_name' => 'fin_acnt',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'fin_acnt',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'create' => 
    array (
      'table_name' => 'fin_acnt',
      'column_name' => 'create',
      'column_position' => 3,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'fin_acnt',
      'column_name' => 'timestamp',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'name' => 
    array (
      'table_name' => 'fin_acnt',
      'column_name' => 'name',
      'column_position' => 5,
      'data_type' => 'varchar',
      'length' => '100',
    ),
    'master_relation' => 
    array (
      'table_name' => 'fin_acnt',
      'column_name' => 'master_relation',
      'column_position' => 6,
      'data_type' => 'varchar',
      'length' => '100',
    ),
    'code' => 
    array (
      'table_name' => 'fin_acnt',
      'column_name' => 'code',
      'column_position' => 7,
      'data_type' => 'varchar',
      'length' => '100',
    ),
  ),
  'fin_operations' => 
  array (
    'id' => 
    array (
      'table_name' => 'fin_operations',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'fin_operations',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'create' => 
    array (
      'table_name' => 'fin_operations',
      'column_name' => 'create',
      'column_position' => 3,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'fin_operations',
      'column_name' => 'timestamp',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'sum' => 
    array (
      'table_name' => 'fin_operations',
      'column_name' => 'sum',
      'column_position' => 5,
      'data_type' => 'float',
      'nullable' => true,
      'scale' => '2',
      'precision' => '14',
    ),
    'factor' => 
    array (
      'table_name' => 'fin_operations',
      'column_name' => 'factor',
      'column_position' => 6,
      'data_type' => 'int',
      'default' => '1',
    ),
    'master_relation' => 
    array (
      'table_name' => 'fin_operations',
      'column_name' => 'master_relation',
      'column_position' => 7,
      'data_type' => 'varchar',
      'length' => '250',
    ),
  ),
  'fin_finoperationoption' => 
  array (
    'id' => 
    array (
      'table_name' => 'fin_finoperationoption',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'fin_finoperationoption',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'create' => 
    array (
      'table_name' => 'fin_finoperationoption',
      'column_name' => 'create',
      'column_position' => 3,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'fin_finoperationoption',
      'column_name' => 'timestamp',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'description' => 
    array (
      'table_name' => 'fin_finoperationoption',
      'column_name' => 'description',
      'column_position' => 5,
      'data_type' => 'varchar',
      'nullable' => true,
      'length' => '250',
    ),
    'cashdesk' => 
    array (
      'table_name' => 'fin_finoperationoption',
      'column_name' => 'cashdesk',
      'column_position' => 6,
      'data_type' => 'varchar',
      'nullable' => true,
      'length' => '250',
    ),
    'currency' => 
    array (
      'table_name' => 'fin_finoperationoption',
      'column_name' => 'currency',
      'column_position' => 7,
      'data_type' => 'varchar',
      'nullable' => true,
      'length' => '250',
    ),
    'course' => 
    array (
      'table_name' => 'fin_finoperationoption',
      'column_name' => 'course',
      'column_position' => 8,
      'data_type' => 'varchar',
      'length' => '250',
    ),
  ),
  'fin_otransaction' => 
  array (
    'id' => 
    array (
      'table_name' => 'fin_otransaction',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'fin_otransaction',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'create' => 
    array (
      'table_name' => 'fin_otransaction',
      'column_name' => 'create',
      'column_position' => 3,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'fin_otransaction',
      'column_name' => 'timestamp',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'doc_id' => 
    array (
      'table_name' => 'fin_otransaction',
      'column_name' => 'doc_id',
      'column_position' => 5,
      'data_type' => 'int',
      'nullable' => true,
    ),
    'reason_id' => 
    array (
      'table_name' => 'fin_otransaction',
      'column_name' => 'reason_id',
      'column_position' => 6,
      'data_type' => 'int',
      'nullable' => true,
    ),
    'account_id' => 
    array (
      'table_name' => 'fin_otransaction',
      'column_name' => 'account_id',
      'column_position' => 7,
      'data_type' => 'int',
      'nullable' => true,
    ),
    'qty' => 
    array (
      'table_name' => 'fin_otransaction',
      'column_name' => 'qty',
      'column_position' => 8,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '15',
    ),
    'factor' => 
    array (
      'table_name' => 'fin_otransaction',
      'column_name' => 'factor',
      'column_position' => 9,
      'data_type' => 'tinyint',
    ),
    'status' => 
    array (
      'table_name' => 'fin_otransaction',
      'column_name' => 'status',
      'column_position' => 10,
      'data_type' => 'int',
    ),
  ),
  'fin_payments' => 
  array (
    'id' => 
    array (
      'table_name' => 'fin_payments',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'fin_payments',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'name' => 
    array (
      'table_name' => 'fin_payments',
      'column_name' => 'name',
      'column_position' => 3,
      'data_type' => 'varchar',
      'length' => '200',
    ),
    'create' => 
    array (
      'table_name' => 'fin_payments',
      'column_name' => 'create',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'fin_payments',
      'column_name' => 'timestamp',
      'column_position' => 5,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'summ' => 
    array (
      'table_name' => 'fin_payments',
      'column_name' => 'summ',
      'column_position' => 6,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '10',
    ),
    'summ_approve' => 
    array (
      'table_name' => 'fin_payments',
      'column_name' => 'summ_approve',
      'column_position' => 7,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '10',
    ),
    'date' => 
    array (
      'table_name' => 'fin_payments',
      'column_name' => 'date',
      'column_position' => 8,
      'data_type' => 'date',
    ),
    'status' => 
    array (
      'table_name' => 'fin_payments',
      'column_name' => 'status',
      'column_position' => 9,
      'data_type' => 'int',
      'default' => '2',
    ),
    'description' => 
    array (
      'table_name' => 'fin_payments',
      'column_name' => 'description',
      'column_position' => 10,
      'data_type' => 'text',
    ),
    'course' => 
    array (
      'table_name' => 'fin_payments',
      'column_name' => 'course',
      'column_position' => 11,
      'data_type' => 'float',
      'scale' => '6',
      'precision' => '15',
    ),
  ),
  'fin_paymoves' => 
  array (
    'id' => 
    array (
      'table_name' => 'fin_paymoves',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'fin_paymoves',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'name' => 
    array (
      'table_name' => 'fin_paymoves',
      'column_name' => 'name',
      'column_position' => 3,
      'data_type' => 'varchar',
      'length' => '200',
    ),
    'create' => 
    array (
      'table_name' => 'fin_paymoves',
      'column_name' => 'create',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'fin_paymoves',
      'column_name' => 'timestamp',
      'column_position' => 5,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'summ' => 
    array (
      'table_name' => 'fin_paymoves',
      'column_name' => 'summ',
      'column_position' => 6,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '10',
    ),
    'summ_approve' => 
    array (
      'table_name' => 'fin_paymoves',
      'column_name' => 'summ_approve',
      'column_position' => 7,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '10',
    ),
    'date' => 
    array (
      'table_name' => 'fin_paymoves',
      'column_name' => 'date',
      'column_position' => 8,
      'data_type' => 'date',
    ),
    'status' => 
    array (
      'table_name' => 'fin_paymoves',
      'column_name' => 'status',
      'column_position' => 9,
      'data_type' => 'int',
    ),
    'description' => 
    array (
      'table_name' => 'fin_paymoves',
      'column_name' => 'description',
      'column_position' => 10,
      'data_type' => 'text',
    ),
    'course' => 
    array (
      'table_name' => 'fin_paymoves',
      'column_name' => 'course',
      'column_position' => 11,
      'data_type' => 'float',
      'scale' => '6',
      'precision' => '15',
    ),
  ),
  'acnt' => 
  array (
    'id' => 
    array (
      'table_name' => 'acnt',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'acnt',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'create' => 
    array (
      'table_name' => 'acnt',
      'column_name' => 'create',
      'column_position' => 3,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'acnt',
      'column_name' => 'timestamp',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'name' => 
    array (
      'table_name' => 'acnt',
      'column_name' => 'name',
      'column_position' => 5,
      'data_type' => 'varchar',
      'nullable' => true,
      'length' => '100',
    ),
    'master_relation' => 
    array (
      'table_name' => 'acnt',
      'column_name' => 'master_relation',
      'column_position' => 6,
      'data_type' => 'varchar',
      'length' => '200',
    ),
  ),
  'cities' => 
  array (
    'id' => 
    array (
      'table_name' => 'cities',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'cities',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'name' => 
    array (
      'table_name' => 'cities',
      'column_name' => 'name',
      'column_position' => 3,
      'data_type' => 'varchar',
      'length' => '200',
    ),
    'create' => 
    array (
      'table_name' => 'cities',
      'column_name' => 'create',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'cities',
      'column_name' => 'timestamp',
      'column_position' => 5,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'code' => 
    array (
      'table_name' => 'cities',
      'column_name' => 'code',
      'column_position' => 6,
      'data_type' => 'varchar',
      'length' => '20',
    ),
  ),
  'countries' => 
  array (
    'id' => 
    array (
      'table_name' => 'countries',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'countries',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'name' => 
    array (
      'table_name' => 'countries',
      'column_name' => 'name',
      'column_position' => 3,
      'data_type' => 'varchar',
      'length' => '200',
    ),
    'create' => 
    array (
      'table_name' => 'countries',
      'column_name' => 'create',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'countries',
      'column_name' => 'timestamp',
      'column_position' => 5,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'code' => 
    array (
      'table_name' => 'countries',
      'column_name' => 'code',
      'column_position' => 6,
      'data_type' => 'varchar',
      'length' => '20',
    ),
  ),
  'cronjobs' => 
  array (
    'id' => 
    array (
      'table_name' => 'cronjobs',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'cronjobs',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'create' => 
    array (
      'table_name' => 'cronjobs',
      'column_name' => 'create',
      'column_position' => 3,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'cronjobs',
      'column_name' => 'timestamp',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'name' => 
    array (
      'table_name' => 'cronjobs',
      'column_name' => 'name',
      'column_position' => 5,
      'data_type' => 'varchar',
      'nullable' => true,
      'length' => '250',
    ),
    'minute' => 
    array (
      'table_name' => 'cronjobs',
      'column_name' => 'minute',
      'column_position' => 6,
      'data_type' => 'varchar',
      'nullable' => true,
      'length' => '10',
    ),
    'hour' => 
    array (
      'table_name' => 'cronjobs',
      'column_name' => 'hour',
      'column_position' => 7,
      'data_type' => 'varchar',
      'nullable' => true,
      'length' => '10',
    ),
    'day' => 
    array (
      'table_name' => 'cronjobs',
      'column_name' => 'day',
      'column_position' => 8,
      'data_type' => 'varchar',
      'nullable' => true,
      'length' => '10',
    ),
    'month' => 
    array (
      'table_name' => 'cronjobs',
      'column_name' => 'month',
      'column_position' => 9,
      'data_type' => 'varchar',
      'nullable' => true,
      'length' => '10',
    ),
    'command' => 
    array (
      'table_name' => 'cronjobs',
      'column_name' => 'command',
      'column_position' => 10,
      'data_type' => 'varchar',
      'nullable' => true,
      'length' => '250',
    ),
    'description' => 
    array (
      'table_name' => 'cronjobs',
      'column_name' => 'description',
      'column_position' => 11,
      'data_type' => 'text',
      'nullable' => true,
    ),
  ),
  'emails' => 
  array (
    'id' => 
    array (
      'table_name' => 'emails',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'mail' => 
    array (
      'table_name' => 'emails',
      'column_name' => 'mail',
      'column_position' => 2,
      'data_type' => 'varchar',
      'length' => '100',
    ),
    'active' => 
    array (
      'table_name' => 'emails',
      'column_name' => 'active',
      'column_position' => 3,
      'data_type' => 'int',
    ),
    'create' => 
    array (
      'table_name' => 'emails',
      'column_name' => 'create',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'last_update' => 
    array (
      'table_name' => 'emails',
      'column_name' => 'last_update',
      'column_position' => 5,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
  ),
  'files' => 
  array (
    'id' => 
    array (
      'table_name' => 'files',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'files',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'create' => 
    array (
      'table_name' => 'files',
      'column_name' => 'create',
      'column_position' => 3,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'files',
      'column_name' => 'timestamp',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'name' => 
    array (
      'table_name' => 'files',
      'column_name' => 'name',
      'column_position' => 5,
      'data_type' => 'text',
    ),
    'type' => 
    array (
      'table_name' => 'files',
      'column_name' => 'type',
      'column_position' => 6,
      'data_type' => 'varchar',
      'length' => '250',
    ),
    'location' => 
    array (
      'table_name' => 'files',
      'column_name' => 'location',
      'column_position' => 7,
      'data_type' => 'varchar',
      'length' => '250',
    ),
  ),
  'locked_objects' => 
  array (
    'id' => 
    array (
      'table_name' => 'locked_objects',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'locked_objects',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'name' => 
    array (
      'table_name' => 'locked_objects',
      'column_name' => 'name',
      'column_position' => 3,
      'data_type' => 'char',
      'length' => '32',
    ),
    'create' => 
    array (
      'table_name' => 'locked_objects',
      'column_name' => 'create',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'locked_objects',
      'column_name' => 'timestamp',
      'column_position' => 5,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'user_id' => 
    array (
      'table_name' => 'locked_objects',
      'column_name' => 'user_id',
      'column_position' => 6,
      'data_type' => 'int',
      'nullable' => true,
    ),
  ),
  'phones' => 
  array (
    'id' => 
    array (
      'table_name' => 'phones',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'phone' => 
    array (
      'table_name' => 'phones',
      'column_name' => 'phone',
      'column_position' => 2,
      'data_type' => 'varchar',
      'length' => '100',
    ),
    'active' => 
    array (
      'table_name' => 'phones',
      'column_name' => 'active',
      'column_position' => 3,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'create' => 
    array (
      'table_name' => 'phones',
      'column_name' => 'create',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'last_update' => 
    array (
      'table_name' => 'phones',
      'column_name' => 'last_update',
      'column_position' => 5,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
  ),
  'print_forms' => 
  array (
    'id' => 
    array (
      'table_name' => 'print_forms',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'print_forms',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'create' => 
    array (
      'table_name' => 'print_forms',
      'column_name' => 'create',
      'column_position' => 3,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'print_forms',
      'column_name' => 'timestamp',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'name' => 
    array (
      'table_name' => 'print_forms',
      'column_name' => 'name',
      'column_position' => 5,
      'data_type' => 'varchar',
      'length' => '32',
    ),
    'description' => 
    array (
      'table_name' => 'print_forms',
      'column_name' => 'description',
      'column_position' => 6,
      'data_type' => 'text',
    ),
    'type' => 
    array (
      'table_name' => 'print_forms',
      'column_name' => 'type',
      'column_position' => 7,
      'data_type' => 'varchar',
      'length' => '100',
    ),
    'data' => 
    array (
      'table_name' => 'print_forms',
      'column_name' => 'data',
      'column_position' => 8,
      'data_type' => 'text',
    ),
    'email' => 
    array (
      'table_name' => 'print_forms',
      'column_name' => 'email',
      'column_position' => 9,
      'data_type' => 'smallint',
    ),
    'mask' => 
    array (
      'table_name' => 'print_forms',
      'column_name' => 'mask',
      'column_position' => 10,
      'data_type' => 'varchar',
      'nullable' => true,
      'length' => '100',
    ),
  ),
  'system_settings' => 
  array (
    'id' => 
    array (
      'table_name' => 'system_settings',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'system_settings',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'create' => 
    array (
      'table_name' => 'system_settings',
      'column_name' => 'create',
      'column_position' => 3,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'system_settings',
      'column_name' => 'timestamp',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'name' => 
    array (
      'table_name' => 'system_settings',
      'column_name' => 'name',
      'column_position' => 5,
      'data_type' => 'varchar',
      'length' => '100',
    ),
    'value' => 
    array (
      'table_name' => 'system_settings',
      'column_name' => 'value',
      'column_position' => 6,
      'data_type' => 'text',
    ),
    'type' => 
    array (
      'table_name' => 'system_settings',
      'column_name' => 'type',
      'column_position' => 7,
      'data_type' => 'int',
    ),
  ),
  'logistic_boxes' => 
  array (
    'id' => 
    array (
      'table_name' => 'logistic_boxes',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'logistic_boxes',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'create' => 
    array (
      'table_name' => 'logistic_boxes',
      'column_name' => 'create',
      'column_position' => 3,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'logistic_boxes',
      'column_name' => 'timestamp',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'length' => 
    array (
      'table_name' => 'logistic_boxes',
      'column_name' => 'length',
      'column_position' => 5,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '6',
    ),
    'height' => 
    array (
      'table_name' => 'logistic_boxes',
      'column_name' => 'height',
      'column_position' => 6,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '6',
    ),
    'width' => 
    array (
      'table_name' => 'logistic_boxes',
      'column_name' => 'width',
      'column_position' => 7,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '6',
    ),
    'weight' => 
    array (
      'table_name' => 'logistic_boxes',
      'column_name' => 'weight',
      'column_position' => 8,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '8',
    ),
    'tare_weight' => 
    array (
      'table_name' => 'logistic_boxes',
      'column_name' => 'tare_weight',
      'column_position' => 9,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '8',
    ),
    'description' => 
    array (
      'table_name' => 'logistic_boxes',
      'column_name' => 'description',
      'column_position' => 10,
      'data_type' => 'varchar',
      'length' => '200',
    ),
    'filling' => 
    array (
      'table_name' => 'logistic_boxes',
      'column_name' => 'filling',
      'column_position' => 11,
      'data_type' => 'tinyint',
    ),
    'volume' => 
    array (
      'table_name' => 'logistic_boxes',
      'column_name' => 'volume',
      'column_position' => 12,
      'data_type' => 'float',
      'scale' => '8',
      'precision' => '11',
    ),
    'name' => 
    array (
      'table_name' => 'logistic_boxes',
      'column_name' => 'name',
      'column_position' => 13,
      'data_type' => 'varchar',
      'length' => '200',
    ),
    'price_per_wrapping' => 
    array (
      'table_name' => 'logistic_boxes',
      'column_name' => 'price_per_wrapping',
      'column_position' => 14,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '6',
    ),
  ),
  'logistic_chains' => 
  array (
    'id' => 
    array (
      'table_name' => 'logistic_chains',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'logistic_chains',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'create' => 
    array (
      'table_name' => 'logistic_chains',
      'column_name' => 'create',
      'column_position' => 3,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'logistic_chains',
      'column_name' => 'timestamp',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'name' => 
    array (
      'table_name' => 'logistic_chains',
      'column_name' => 'name',
      'column_position' => 5,
      'data_type' => 'varchar',
      'length' => '100',
    ),
    'total_delay' => 
    array (
      'table_name' => 'logistic_chains',
      'column_name' => 'total_delay',
      'column_position' => 6,
      'data_type' => 'varchar',
      'nullable' => true,
      'length' => '200',
    ),
  ),
  'logistic_customer_identifier' => 
  array (
    'id' => 
    array (
      'table_name' => 'logistic_customer_identifier',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'logistic_customer_identifier',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'name' => 
    array (
      'table_name' => 'logistic_customer_identifier',
      'column_name' => 'name',
      'column_position' => 3,
      'data_type' => 'varchar',
      'nullable' => true,
      'length' => '20',
    ),
    'symbol' => 
    array (
      'table_name' => 'logistic_customer_identifier',
      'column_name' => 'symbol',
      'column_position' => 4,
      'data_type' => 'varchar',
      'nullable' => true,
      'length' => '4',
    ),
    'counter' => 
    array (
      'table_name' => 'logistic_customer_identifier',
      'column_name' => 'counter',
      'column_position' => 5,
      'data_type' => 'int',
      'nullable' => true,
    ),
    'create' => 
    array (
      'table_name' => 'logistic_customer_identifier',
      'column_name' => 'create',
      'column_position' => 6,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'logistic_customer_identifier',
      'column_name' => 'timestamp',
      'column_position' => 7,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
  ),
  'logistic_dealtariffsets' => 
  array (
    'id' => 
    array (
      'table_name' => 'logistic_dealtariffsets',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'logistic_dealtariffsets',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'create' => 
    array (
      'table_name' => 'logistic_dealtariffsets',
      'column_name' => 'create',
      'column_position' => 3,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'logistic_dealtariffsets',
      'column_name' => 'timestamp',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'name' => 
    array (
      'table_name' => 'logistic_dealtariffsets',
      'column_name' => 'name',
      'column_position' => 5,
      'data_type' => 'varchar',
      'length' => '100',
    ),
    'weight_factor' => 
    array (
      'table_name' => 'logistic_dealtariffsets',
      'column_name' => 'weight_factor',
      'column_position' => 6,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '6',
    ),
    'weight_surcharge' => 
    array (
      'table_name' => 'logistic_dealtariffsets',
      'column_name' => 'weight_surcharge',
      'column_position' => 7,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '6',
    ),
    'count_factor' => 
    array (
      'table_name' => 'logistic_dealtariffsets',
      'column_name' => 'count_factor',
      'column_position' => 8,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '6',
    ),
    'count_surcharge' => 
    array (
      'table_name' => 'logistic_dealtariffsets',
      'column_name' => 'count_surcharge',
      'column_position' => 9,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '6',
    ),
    'use_weight_factor' => 
    array (
      'table_name' => 'logistic_dealtariffsets',
      'column_name' => 'use_weight_factor',
      'column_position' => 10,
      'data_type' => 'tinyint',
    ),
    'use_count_factor' => 
    array (
      'table_name' => 'logistic_dealtariffsets',
      'column_name' => 'use_count_factor',
      'column_position' => 11,
      'data_type' => 'tinyint',
    ),
    'place_surcharge' => 
    array (
      'table_name' => 'logistic_dealtariffsets',
      'column_name' => 'place_surcharge',
      'column_position' => 12,
      'data_type' => 'float',
      'nullable' => true,
      'scale' => '2',
      'precision' => '6',
    ),
    'insurance_percent' => 
    array (
      'table_name' => 'logistic_dealtariffsets',
      'column_name' => 'insurance_percent',
      'column_position' => 13,
      'data_type' => 'float',
      'nullable' => true,
      'scale' => '2',
      'precision' => '4',
    ),
    'total_surcharge' => 
    array (
      'table_name' => 'logistic_dealtariffsets',
      'column_name' => 'total_surcharge',
      'column_position' => 14,
      'data_type' => 'float',
      'nullable' => true,
      'scale' => '2',
      'precision' => '6',
    ),
    'total_surcharge_type' => 
    array (
      'table_name' => 'logistic_dealtariffsets',
      'column_name' => 'total_surcharge_type',
      'column_position' => 15,
      'data_type' => 'int',
      'nullable' => true,
    ),
    'use_total_surcharge' => 
    array (
      'table_name' => 'logistic_dealtariffsets',
      'column_name' => 'use_total_surcharge',
      'column_position' => 16,
      'data_type' => 'tinyint',
      'nullable' => true,
    ),
    'total_weight_surcharge' => 
    array (
      'table_name' => 'logistic_dealtariffsets',
      'column_name' => 'total_weight_surcharge',
      'column_position' => 17,
      'data_type' => 'decimal',
      'nullable' => true,
      'scale' => '2',
      'precision' => '8',
    ),
    'total_count_surcharge' => 
    array (
      'table_name' => 'logistic_dealtariffsets',
      'column_name' => 'total_count_surcharge',
      'column_position' => 18,
      'data_type' => 'decimal',
      'nullable' => true,
      'scale' => '2',
      'precision' => '8',
    ),
  ),
  'logistic_packagekeeping' => 
  array (
    'id' => 
    array (
      'table_name' => 'logistic_packagekeeping',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'logistic_packagekeeping',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'name' => 
    array (
      'table_name' => 'logistic_packagekeeping',
      'column_name' => 'name',
      'column_position' => 3,
      'data_type' => 'varchar',
      'length' => '200',
    ),
    'create' => 
    array (
      'table_name' => 'logistic_packagekeeping',
      'column_name' => 'create',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'logistic_packagekeeping',
      'column_name' => 'timestamp',
      'column_position' => 5,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'price_per_kg' => 
    array (
      'table_name' => 'logistic_packagekeeping',
      'column_name' => 'price_per_kg',
      'column_position' => 6,
      'data_type' => 'float',
      'scale' => '4',
      'precision' => '8',
    ),
    'period' => 
    array (
      'table_name' => 'logistic_packagekeeping',
      'column_name' => 'period',
      'column_position' => 7,
      'data_type' => 'int',
    ),
    'price' => 
    array (
      'table_name' => 'logistic_packagekeeping',
      'column_name' => 'price',
      'column_position' => 8,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '10',
    ),
    'description' => 
    array (
      'table_name' => 'logistic_packagekeeping',
      'column_name' => 'description',
      'column_position' => 9,
      'data_type' => 'text',
    ),
    'date_start' => 
    array (
      'table_name' => 'logistic_packagekeeping',
      'column_name' => 'date_start',
      'column_position' => 10,
      'data_type' => 'date',
      'nullable' => true,
    ),
    'date_end' => 
    array (
      'table_name' => 'logistic_packagekeeping',
      'column_name' => 'date_end',
      'column_position' => 11,
      'data_type' => 'date',
      'nullable' => true,
    ),
  ),
  'logistic_packageservices' => 
  array (
    'id' => 
    array (
      'table_name' => 'logistic_packageservices',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'logistic_packageservices',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'name' => 
    array (
      'table_name' => 'logistic_packageservices',
      'column_name' => 'name',
      'column_position' => 3,
      'data_type' => 'varchar',
      'length' => '200',
    ),
    'create' => 
    array (
      'table_name' => 'logistic_packageservices',
      'column_name' => 'create',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'logistic_packageservices',
      'column_name' => 'timestamp',
      'column_position' => 5,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'price_per_kg' => 
    array (
      'table_name' => 'logistic_packageservices',
      'column_name' => 'price_per_kg',
      'column_position' => 6,
      'data_type' => 'float',
      'scale' => '4',
      'precision' => '8',
    ),
    'period' => 
    array (
      'table_name' => 'logistic_packageservices',
      'column_name' => 'period',
      'column_position' => 7,
      'data_type' => 'int',
    ),
    'price' => 
    array (
      'table_name' => 'logistic_packageservices',
      'column_name' => 'price',
      'column_position' => 8,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '10',
    ),
    'description' => 
    array (
      'table_name' => 'logistic_packageservices',
      'column_name' => 'description',
      'column_position' => 9,
      'data_type' => 'text',
    ),
  ),
  'logistic_links' => 
  array (
    'id' => 
    array (
      'table_name' => 'logistic_links',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'logistic_links',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'create' => 
    array (
      'table_name' => 'logistic_links',
      'column_name' => 'create',
      'column_position' => 3,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'logistic_links',
      'column_name' => 'timestamp',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'name' => 
    array (
      'table_name' => 'logistic_links',
      'column_name' => 'name',
      'column_position' => 5,
      'data_type' => 'varchar',
      'length' => '100',
    ),
    'measure' => 
    array (
      'table_name' => 'logistic_links',
      'column_name' => 'measure',
      'column_position' => 6,
      'data_type' => 'varchar',
      'length' => '20',
    ),
    'delivery_time' => 
    array (
      'table_name' => 'logistic_links',
      'column_name' => 'delivery_time',
      'column_position' => 7,
      'data_type' => 'int',
    ),
    'necessity' => 
    array (
      'table_name' => 'logistic_links',
      'column_name' => 'necessity',
      'column_position' => 8,
      'data_type' => 'tinyint',
    ),
    'delay_receive' => 
    array (
      'table_name' => 'logistic_links',
      'column_name' => 'delay_receive',
      'column_position' => 9,
      'data_type' => 'int',
      'nullable' => true,
    ),
    'delay_process' => 
    array (
      'table_name' => 'logistic_links',
      'column_name' => 'delay_process',
      'column_position' => 10,
      'data_type' => 'int',
      'nullable' => true,
    ),
    'delay_send' => 
    array (
      'table_name' => 'logistic_links',
      'column_name' => 'delay_send',
      'column_position' => 11,
      'data_type' => 'int',
      'nullable' => true,
    ),
  ),
  'logistic_tariff' => 
  array (
    'id' => 
    array (
      'table_name' => 'logistic_tariff',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'logistic_tariff',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'name' => 
    array (
      'table_name' => 'logistic_tariff',
      'column_name' => 'name',
      'column_position' => 3,
      'data_type' => 'varchar',
      'length' => '200',
    ),
    'create' => 
    array (
      'table_name' => 'logistic_tariff',
      'column_name' => 'create',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'logistic_tariff',
      'column_name' => 'timestamp',
      'column_position' => 5,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'min_days' => 
    array (
      'table_name' => 'logistic_tariff',
      'column_name' => 'min_days',
      'column_position' => 6,
      'data_type' => 'int',
    ),
    'max_days' => 
    array (
      'table_name' => 'logistic_tariff',
      'column_name' => 'max_days',
      'column_position' => 7,
      'data_type' => 'int',
    ),
    'price_per_kg' => 
    array (
      'table_name' => 'logistic_tariff',
      'column_name' => 'price_per_kg',
      'column_position' => 8,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '10',
    ),
    'min_weight' => 
    array (
      'table_name' => 'logistic_tariff',
      'column_name' => 'min_weight',
      'column_position' => 9,
      'data_type' => 'float',
      'scale' => '1',
      'precision' => '5',
    ),
    'insurance_percent' => 
    array (
      'table_name' => 'logistic_tariff',
      'column_name' => 'insurance_percent',
      'column_position' => 10,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '4',
    ),
    'weight_factor' => 
    array (
      'table_name' => 'logistic_tariff',
      'column_name' => 'weight_factor',
      'column_position' => 11,
      'data_type' => 'smallint',
      'default' => '100',
    ),
    'count_factor' => 
    array (
      'table_name' => 'logistic_tariff',
      'column_name' => 'count_factor',
      'column_position' => 12,
      'data_type' => 'float',
      'default' => '100.00',
      'scale' => '2',
      'precision' => '6',
    ),
    'pack_factor' => 
    array (
      'table_name' => 'logistic_tariff',
      'column_name' => 'pack_factor',
      'column_position' => 13,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '6',
    ),
    'title' => 
    array (
      'table_name' => 'logistic_tariff',
      'column_name' => 'title',
      'column_position' => 14,
      'data_type' => 'varchar',
      'length' => '20',
    ),
    'first_price_per_kg' => 
    array (
      'table_name' => 'logistic_tariff',
      'column_name' => 'first_price_per_kg',
      'column_position' => 15,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '10',
    ),
    'weight_sill' => 
    array (
      'table_name' => 'logistic_tariff',
      'column_name' => 'weight_sill',
      'column_position' => 16,
      'data_type' => 'smallint',
    ),
    'count_surcharge' => 
    array (
      'table_name' => 'logistic_tariff',
      'column_name' => 'count_surcharge',
      'column_position' => 17,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '6',
    ),
    'under_sill_price_per_kg' => 
    array (
      'table_name' => 'logistic_tariff',
      'column_name' => 'under_sill_price_per_kg',
      'column_position' => 18,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '6',
    ),
    'first_pack_factor' => 
    array (
      'table_name' => 'logistic_tariff',
      'column_name' => 'first_pack_factor',
      'column_position' => 19,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '8',
    ),
    'first_count_surcharge' => 
    array (
      'table_name' => 'logistic_tariff',
      'column_name' => 'first_count_surcharge',
      'column_position' => 20,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '8',
    ),
    'first_insurance_percent' => 
    array (
      'table_name' => 'logistic_tariff',
      'column_name' => 'first_insurance_percent',
      'column_position' => 21,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '5',
    ),
  ),
  'logistic_packs' => 
  array (
    'id' => 
    array (
      'table_name' => 'logistic_packs',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'logistic_packs',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'name' => 
    array (
      'table_name' => 'logistic_packs',
      'column_name' => 'name',
      'column_position' => 3,
      'data_type' => 'varchar',
      'length' => '200',
    ),
    'create' => 
    array (
      'table_name' => 'logistic_packs',
      'column_name' => 'create',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'logistic_packs',
      'column_name' => 'timestamp',
      'column_position' => 5,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'identifier' => 
    array (
      'table_name' => 'logistic_packs',
      'column_name' => 'identifier',
      'column_position' => 6,
      'data_type' => 'varchar',
      'length' => '30',
    ),
    'description' => 
    array (
      'table_name' => 'logistic_packs',
      'column_name' => 'description',
      'column_position' => 7,
      'data_type' => 'text',
    ),
    'weight' => 
    array (
      'table_name' => 'logistic_packs',
      'column_name' => 'weight',
      'column_position' => 8,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '7',
    ),
    'filling' => 
    array (
      'table_name' => 'logistic_packs',
      'column_name' => 'filling',
      'column_position' => 9,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '3',
    ),
    'volume' => 
    array (
      'table_name' => 'logistic_packs',
      'column_name' => 'volume',
      'column_position' => 10,
      'data_type' => 'float',
      'scale' => '4',
      'precision' => '7',
    ),
    'price_per_wrapping' => 
    array (
      'table_name' => 'logistic_packs',
      'column_name' => 'price_per_wrapping',
      'column_position' => 11,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '6',
    ),
    'length' => 
    array (
      'table_name' => 'logistic_packs',
      'column_name' => 'length',
      'column_position' => 12,
      'data_type' => 'float',
      'default' => '0.00',
      'scale' => '2',
      'precision' => '6',
    ),
    'height' => 
    array (
      'table_name' => 'logistic_packs',
      'column_name' => 'height',
      'column_position' => 13,
      'data_type' => 'float',
      'default' => '0.00',
      'scale' => '2',
      'precision' => '6',
    ),
    'width' => 
    array (
      'table_name' => 'logistic_packs',
      'column_name' => 'width',
      'column_position' => 14,
      'data_type' => 'float',
      'default' => '0.00',
      'scale' => '2',
      'precision' => '6',
    ),
  ),
  'logistic_packages' => 
  array (
    'id' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'create' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'create',
      'column_position' => 3,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'timestamp',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'identifier' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'identifier',
      'column_position' => 5,
      'data_type' => 'varchar',
      'nullable' => true,
      'length' => '30',
    ),
    'status' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'status',
      'column_position' => 6,
      'data_type' => 'int',
      'nullable' => true,
    ),
    'category' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'category',
      'column_position' => 7,
      'data_type' => 'int',
      'nullable' => true,
    ),
    'volume' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'volume',
      'column_position' => 8,
      'data_type' => 'float',
      'nullable' => true,
      'scale' => '2',
      'precision' => '4',
    ),
    'declarate_cost' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'declarate_cost',
      'column_position' => 9,
      'data_type' => 'float',
      'nullable' => true,
      'scale' => '2',
      'precision' => '12',
    ),
    'base_cost_per_kg' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'base_cost_per_kg',
      'column_position' => 10,
      'data_type' => 'float',
      'nullable' => true,
      'scale' => '2',
      'precision' => '8',
    ),
    'base_cost' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'base_cost',
      'column_position' => 11,
      'data_type' => 'float',
      'nullable' => true,
      'scale' => '2',
      'precision' => '12',
    ),
    'dealer_cost' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'dealer_cost',
      'column_position' => 12,
      'data_type' => 'float',
      'nullable' => true,
      'scale' => '2',
      'precision' => '12',
    ),
    'weight' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'weight',
      'column_position' => 13,
      'data_type' => 'float',
      'nullable' => true,
      'scale' => '2',
      'precision' => '9',
    ),
    'calculated_weight' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'calculated_weight',
      'column_position' => 14,
      'data_type' => 'int',
      'nullable' => true,
    ),
    'delivery_cost' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'delivery_cost',
      'column_position' => 15,
      'data_type' => 'float',
      'nullable' => true,
      'scale' => '2',
      'precision' => '12',
    ),
    'calculated_cost' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'calculated_cost',
      'column_position' => 16,
      'data_type' => 'decimal',
      'nullable' => true,
      'scale' => '2',
      'precision' => '12',
    ),
    'insurance_percent' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'insurance_percent',
      'column_position' => 17,
      'data_type' => 'smallint',
      'nullable' => true,
    ),
    'insurance_calculated_cost' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'insurance_calculated_cost',
      'column_position' => 18,
      'data_type' => 'float',
      'nullable' => true,
      'scale' => '2',
      'precision' => '8',
    ),
    'mail' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'mail',
      'column_position' => 19,
      'data_type' => 'tinyint',
      'nullable' => true,
    ),
    'sms' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'sms',
      'column_position' => 20,
      'data_type' => 'tinyint',
      'nullable' => true,
    ),
    'insurance_type' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'insurance_type',
      'column_position' => 21,
      'data_type' => 'int',
      'nullable' => true,
    ),
    'payment_method' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'payment_method',
      'column_position' => 22,
      'data_type' => 'smallint',
      'nullable' => true,
    ),
    'total_sum' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'total_sum',
      'column_position' => 23,
      'data_type' => 'float',
      'nullable' => true,
      'scale' => '2',
      'precision' => '11',
    ),
    'weight_ratio' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'weight_ratio',
      'column_position' => 24,
      'data_type' => 'float',
      'nullable' => true,
      'scale' => '2',
      'precision' => '6',
    ),
    'financial_status' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'financial_status',
      'column_position' => 25,
      'data_type' => 'int',
      'nullable' => true,
    ),
    'transport_status' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'transport_status',
      'column_position' => 26,
      'data_type' => 'smallint',
      'default' => '20',
      'nullable' => true,
    ),
    'description' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'description',
      'column_position' => 27,
      'data_type' => 'text',
      'nullable' => true,
    ),
    'calculate_description' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'calculate_description',
      'column_position' => 28,
      'data_type' => 'text',
      'nullable' => true,
    ),
    'packages_count' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'packages_count',
      'column_position' => 29,
      'data_type' => 'int',
    ),
    'units_count' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'units_count',
      'column_position' => 30,
      'data_type' => 'int',
    ),
    'dealer_calculate_description' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'dealer_calculate_description',
      'column_position' => 31,
      'data_type' => 'text',
    ),
    'wrapping_cost' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'wrapping_cost',
      'column_position' => 32,
      'data_type' => 'float',
      'default' => '0.00',
      'scale' => '2',
      'precision' => '10',
    ),
    'base_count_surcharge' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'base_count_surcharge',
      'column_position' => 33,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '8',
    ),
    'add_date' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'add_date',
      'column_position' => 34,
      'data_type' => 'date',
      'nullable' => true,
    ),
    'first_insurance_percent' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'first_insurance_percent',
      'column_position' => 35,
      'data_type' => 'decimal',
      'nullable' => true,
      'scale' => '2',
      'precision' => '8',
    ),
    'first_count_surcharge' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'first_count_surcharge',
      'column_position' => 36,
      'data_type' => 'decimal',
      'nullable' => true,
      'scale' => '2',
      'precision' => '8',
    ),
    'first_cost_per_kg' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'first_cost_per_kg',
      'column_position' => 37,
      'data_type' => 'decimal',
      'nullable' => true,
      'scale' => '2',
      'precision' => '8',
    ),
    'first_pack_factor' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'first_pack_factor',
      'column_position' => 38,
      'data_type' => 'decimal',
      'nullable' => true,
      'scale' => '2',
      'precision' => '5',
    ),
    'weight_factor' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'weight_factor',
      'column_position' => 39,
      'data_type' => 'decimal',
      'nullable' => true,
      'scale' => '2',
      'precision' => '10',
    ),
    'count_factor' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'count_factor',
      'column_position' => 40,
      'data_type' => 'decimal',
      'nullable' => true,
      'scale' => '2',
      'precision' => '10',
    ),
    'identifier_ext' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'identifier_ext',
      'column_position' => 41,
      'data_type' => 'varchar',
      'nullable' => true,
      'length' => '250',
    ),
    'in_chain' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'in_chain',
      'column_position' => 42,
      'data_type' => 'tinyint',
      'nullable' => true,
    ),
    'sum' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'sum',
      'column_position' => 43,
      'data_type' => 'decimal',
      'nullable' => true,
      'scale' => '2',
      'precision' => '11',
    ),
    'status_date' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'status_date',
      'column_position' => 44,
      'data_type' => 'date',
      'nullable' => true,
    ),
    'volume_factor' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'volume_factor',
      'column_position' => 45,
      'data_type' => 'decimal',
      'nullable' => true,
      'scale' => '2',
      'precision' => '8',
    ),
    'receive_date' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'receive_date',
      'column_position' => 46,
      'data_type' => 'date',
      'nullable' => true,
    ),
    'calculated_cost_per_kg' => 
    array (
      'table_name' => 'logistic_packages',
      'column_name' => 'calculated_cost_per_kg',
      'column_position' => 47,
      'data_type' => 'decimal',
      'nullable' => true,
      'scale' => '2',
      'precision' => '10',
    ),
  ),
  'logistic_routes' => 
  array (
    'id' => 
    array (
      'table_name' => 'logistic_routes',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'logistic_routes',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'name' => 
    array (
      'table_name' => 'logistic_routes',
      'column_name' => 'name',
      'column_position' => 3,
      'data_type' => 'varchar',
      'length' => '200',
    ),
    'create' => 
    array (
      'table_name' => 'logistic_routes',
      'column_name' => 'create',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'logistic_routes',
      'column_name' => 'timestamp',
      'column_position' => 5,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
  ),
  'logistic_services' => 
  array (
    'id' => 
    array (
      'table_name' => 'logistic_services',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'logistic_services',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'name' => 
    array (
      'table_name' => 'logistic_services',
      'column_name' => 'name',
      'column_position' => 3,
      'data_type' => 'varchar',
      'length' => '200',
    ),
    'create' => 
    array (
      'table_name' => 'logistic_services',
      'column_name' => 'create',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'logistic_services',
      'column_name' => 'timestamp',
      'column_position' => 5,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'price_per_kg' => 
    array (
      'table_name' => 'logistic_services',
      'column_name' => 'price_per_kg',
      'column_position' => 6,
      'data_type' => 'float',
      'scale' => '4',
      'precision' => '8',
    ),
    'period' => 
    array (
      'table_name' => 'logistic_services',
      'column_name' => 'period',
      'column_position' => 7,
      'data_type' => 'int',
    ),
  ),
  'logistic_thefts' => 
  array (
    'id' => 
    array (
      'table_name' => 'logistic_thefts',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'logistic_thefts',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'name' => 
    array (
      'table_name' => 'logistic_thefts',
      'column_name' => 'name',
      'column_position' => 3,
      'data_type' => 'varchar',
      'length' => '200',
    ),
    'create' => 
    array (
      'table_name' => 'logistic_thefts',
      'column_name' => 'create',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'logistic_thefts',
      'column_name' => 'timestamp',
      'column_position' => 5,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'fixation_date' => 
    array (
      'table_name' => 'logistic_thefts',
      'column_name' => 'fixation_date',
      'column_position' => 6,
      'data_type' => 'date',
      'nullable' => true,
    ),
    'description' => 
    array (
      'table_name' => 'logistic_thefts',
      'column_name' => 'description',
      'column_position' => 7,
      'data_type' => 'text',
    ),
    'sum' => 
    array (
      'table_name' => 'logistic_thefts',
      'column_name' => 'sum',
      'column_position' => 8,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '10',
    ),
    'status' => 
    array (
      'table_name' => 'logistic_thefts',
      'column_name' => 'status',
      'column_position' => 9,
      'data_type' => 'int',
    ),
    'declarated_sum' => 
    array (
      'table_name' => 'logistic_thefts',
      'column_name' => 'declarated_sum',
      'column_position' => 10,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '10',
    ),
    'insurance_type' => 
    array (
      'table_name' => 'logistic_thefts',
      'column_name' => 'insurance_type',
      'column_position' => 11,
      'data_type' => 'int',
    ),
    'weight' => 
    array (
      'table_name' => 'logistic_thefts',
      'column_name' => 'weight',
      'column_position' => 12,
      'data_type' => 'float',
      'scale' => '3',
      'precision' => '6',
    ),
    'package_weight' => 
    array (
      'table_name' => 'logistic_thefts',
      'column_name' => 'package_weight',
      'column_position' => 13,
      'data_type' => 'float',
      'scale' => '3',
      'precision' => '6',
    ),
  ),
  'logistic_transport_actions' => 
  array (
    'id' => 
    array (
      'table_name' => 'logistic_transport_actions',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'logistic_transport_actions',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'name' => 
    array (
      'table_name' => 'logistic_transport_actions',
      'column_name' => 'name',
      'column_position' => 3,
      'data_type' => 'tinyint',
    ),
    'create' => 
    array (
      'table_name' => 'logistic_transport_actions',
      'column_name' => 'create',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'logistic_transport_actions',
      'column_name' => 'timestamp',
      'column_position' => 5,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'action_date' => 
    array (
      'table_name' => 'logistic_transport_actions',
      'column_name' => 'action_date',
      'column_position' => 6,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'description' => 
    array (
      'table_name' => 'logistic_transport_actions',
      'column_name' => 'description',
      'column_position' => 7,
      'data_type' => 'text',
      'nullable' => true,
    ),
    'weight' => 
    array (
      'table_name' => 'logistic_transport_actions',
      'column_name' => 'weight',
      'column_position' => 8,
      'data_type' => 'float',
      'nullable' => true,
      'scale' => '4',
      'precision' => '7',
    ),
    'status' => 
    array (
      'table_name' => 'logistic_transport_actions',
      'column_name' => 'status',
      'column_position' => 9,
      'data_type' => 'int',
    ),
    'weight_type' => 
    array (
      'table_name' => 'logistic_transport_actions',
      'column_name' => 'weight_type',
      'column_position' => 10,
      'data_type' => 'smallint',
    ),
    'stock_name' => 
    array (
      'table_name' => 'logistic_transport_actions',
      'column_name' => 'stock_name',
      'column_position' => 11,
      'data_type' => 'varchar',
      'nullable' => true,
      'length' => '250',
    ),
    'identifier' => 
    array (
      'table_name' => 'logistic_transport_actions',
      'column_name' => 'identifier',
      'column_position' => 12,
      'data_type' => 'varchar',
      'nullable' => true,
      'length' => '250',
    ),
  ),
  'logistic_wrapping' => 
  array (
    'id' => 
    array (
      'table_name' => 'logistic_wrapping',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'logistic_wrapping',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'create' => 
    array (
      'table_name' => 'logistic_wrapping',
      'column_name' => 'create',
      'column_position' => 3,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'logistic_wrapping',
      'column_name' => 'timestamp',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'name' => 
    array (
      'table_name' => 'logistic_wrapping',
      'column_name' => 'name',
      'column_position' => 5,
      'data_type' => 'varchar',
      'length' => '100',
    ),
    'default_price' => 
    array (
      'table_name' => 'logistic_wrapping',
      'column_name' => 'default_price',
      'column_position' => 6,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '6',
    ),
  ),
  'prod_income' => 
  array (
    'id' => 
    array (
      'table_name' => 'prod_income',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'prod_income',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'create' => 
    array (
      'table_name' => 'prod_income',
      'column_name' => 'create',
      'column_position' => 3,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'prod_income',
      'column_name' => 'timestamp',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'description' => 
    array (
      'table_name' => 'prod_income',
      'column_name' => 'description',
      'column_position' => 5,
      'data_type' => 'text',
    ),
    'type' => 
    array (
      'table_name' => 'prod_income',
      'column_name' => 'type',
      'column_position' => 6,
      'data_type' => 'int',
    ),
  ),
  'prod_incomeitem' => 
  array (
    'id' => 
    array (
      'table_name' => 'prod_incomeitem',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'prod_incomeitem',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'create' => 
    array (
      'table_name' => 'prod_incomeitem',
      'column_name' => 'create',
      'column_position' => 3,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'prod_incomeitem',
      'column_name' => 'timestamp',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'name' => 
    array (
      'table_name' => 'prod_incomeitem',
      'column_name' => 'name',
      'column_position' => 5,
      'data_type' => 'varchar',
      'length' => '100',
    ),
    'count' => 
    array (
      'table_name' => 'prod_incomeitem',
      'column_name' => 'count',
      'column_position' => 6,
      'data_type' => 'int',
    ),
    'price' => 
    array (
      'table_name' => 'prod_incomeitem',
      'column_name' => 'price',
      'column_position' => 7,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '10',
    ),
    'sum' => 
    array (
      'table_name' => 'prod_incomeitem',
      'column_name' => 'sum',
      'column_position' => 8,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '10',
    ),
    'description' => 
    array (
      'table_name' => 'prod_incomeitem',
      'column_name' => 'description',
      'column_position' => 9,
      'data_type' => 'varchar',
      'length' => '100',
    ),
  ),
  'prod_products' => 
  array (
    'id' => 
    array (
      'table_name' => 'prod_products',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'prod_products',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'name' => 
    array (
      'table_name' => 'prod_products',
      'column_name' => 'name',
      'column_position' => 3,
      'data_type' => 'varchar',
      'length' => '200',
    ),
    'create' => 
    array (
      'table_name' => 'prod_products',
      'column_name' => 'create',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'prod_products',
      'column_name' => 'timestamp',
      'column_position' => 5,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'enabled' => 
    array (
      'table_name' => 'prod_products',
      'column_name' => 'enabled',
      'column_position' => 6,
      'data_type' => 'tinyint',
    ),
    'brief_description' => 
    array (
      'table_name' => 'prod_products',
      'column_name' => 'brief_description',
      'column_position' => 7,
      'data_type' => 'text',
    ),
    'description' => 
    array (
      'table_name' => 'prod_products',
      'column_name' => 'description',
      'column_position' => 8,
      'data_type' => 'text',
    ),
    'code' => 
    array (
      'table_name' => 'prod_products',
      'column_name' => 'code',
      'column_position' => 9,
      'data_type' => 'varchar',
      'length' => '20',
    ),
  ),
  'prod_product_movements' => 
  array (
    'id' => 
    array (
      'table_name' => 'prod_product_movements',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'prod_product_movements',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'create' => 
    array (
      'table_name' => 'prod_product_movements',
      'column_name' => 'create',
      'column_position' => 3,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'prod_product_movements',
      'column_name' => 'timestamp',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'amount' => 
    array (
      'table_name' => 'prod_product_movements',
      'column_name' => 'amount',
      'column_position' => 5,
      'data_type' => 'float',
      'scale' => '6',
      'precision' => '10',
    ),
    'direction' => 
    array (
      'table_name' => 'prod_product_movements',
      'column_name' => 'direction',
      'column_position' => 6,
      'data_type' => 'tinyint',
    ),
  ),
  'prod_product_settings' => 
  array (
    'id' => 
    array (
      'table_name' => 'prod_product_settings',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'has_value' => 
    array (
      'table_name' => 'prod_product_settings',
      'column_name' => 'has_value',
      'column_position' => 2,
      'data_type' => 'tinyint',
    ),
    'active' => 
    array (
      'table_name' => 'prod_product_settings',
      'column_name' => 'active',
      'column_position' => 3,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'create' => 
    array (
      'table_name' => 'prod_product_settings',
      'column_name' => 'create',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'prod_product_settings',
      'column_name' => 'timestamp',
      'column_position' => 5,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'value' => 
    array (
      'table_name' => 'prod_product_settings',
      'column_name' => 'value',
      'column_position' => 6,
      'data_type' => 'varchar',
      'nullable' => true,
      'length' => '250',
    ),
    'name' => 
    array (
      'table_name' => 'prod_product_settings',
      'column_name' => 'name',
      'column_position' => 7,
      'data_type' => 'varchar',
      'nullable' => true,
      'length' => '100',
    ),
  ),
  'prod_products_settings' => 
  array (
    'id' => 
    array (
      'table_name' => 'prod_products_settings',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'prod_products_settings',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'create' => 
    array (
      'table_name' => 'prod_products_settings',
      'column_name' => 'create',
      'column_position' => 3,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'prod_products_settings',
      'column_name' => 'timestamp',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'description' => 
    array (
      'table_name' => 'prod_products_settings',
      'column_name' => 'description',
      'column_position' => 5,
      'data_type' => 'text',
      'nullable' => true,
    ),
    'name' => 
    array (
      'table_name' => 'prod_products_settings',
      'column_name' => 'name',
      'column_position' => 6,
      'data_type' => 'varchar',
      'length' => '200',
    ),
  ),
  'prod_product_stocksettings' => 
  array (
    'id' => 
    array (
      'table_name' => 'prod_product_stocksettings',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'enabled' => 
    array (
      'table_name' => 'prod_product_stocksettings',
      'column_name' => 'enabled',
      'column_position' => 2,
      'data_type' => 'tinyint',
    ),
    'active' => 
    array (
      'table_name' => 'prod_product_stocksettings',
      'column_name' => 'active',
      'column_position' => 3,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'create' => 
    array (
      'table_name' => 'prod_product_stocksettings',
      'column_name' => 'create',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'prod_product_stocksettings',
      'column_name' => 'timestamp',
      'column_position' => 5,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'amount' => 
    array (
      'table_name' => 'prod_product_stocksettings',
      'column_name' => 'amount',
      'column_position' => 6,
      'data_type' => 'float',
      'default' => '0.000000',
      'scale' => '6',
      'precision' => '10',
    ),
    'price' => 
    array (
      'table_name' => 'prod_product_stocksettings',
      'column_name' => 'price',
      'column_position' => 7,
      'data_type' => 'float',
      'default' => '0.00',
      'scale' => '2',
      'precision' => '10',
    ),
  ),
  'prod_types' => 
  array (
    'id' => 
    array (
      'table_name' => 'prod_types',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'prod_types',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'name' => 
    array (
      'table_name' => 'prod_types',
      'column_name' => 'name',
      'column_position' => 3,
      'data_type' => 'varchar',
      'length' => '200',
    ),
    'code' => 
    array (
      'table_name' => 'prod_types',
      'column_name' => 'code',
      'column_position' => 4,
      'data_type' => 'varchar',
      'length' => '20',
    ),
    'description' => 
    array (
      'table_name' => 'prod_types',
      'column_name' => 'description',
      'column_position' => 5,
      'data_type' => 'text',
    ),
    'create' => 
    array (
      'table_name' => 'prod_types',
      'column_name' => 'create',
      'column_position' => 6,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'prod_types',
      'column_name' => 'timestamp',
      'column_position' => 7,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'weight_surcharge' => 
    array (
      'table_name' => 'prod_types',
      'column_name' => 'weight_surcharge',
      'column_position' => 8,
      'data_type' => 'decimal',
      'nullable' => true,
      'scale' => '2',
      'precision' => '10',
    ),
    'count_surcharge' => 
    array (
      'table_name' => 'prod_types',
      'column_name' => 'count_surcharge',
      'column_position' => 9,
      'data_type' => 'decimal',
      'nullable' => true,
      'scale' => '2',
      'precision' => '10',
    ),
  ),
  'prod_stocks' => 
  array (
    'id' => 
    array (
      'table_name' => 'prod_stocks',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'prod_stocks',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'name' => 
    array (
      'table_name' => 'prod_stocks',
      'column_name' => 'name',
      'column_position' => 3,
      'data_type' => 'varchar',
      'length' => '200',
    ),
    'code' => 
    array (
      'table_name' => 'prod_stocks',
      'column_name' => 'code',
      'column_position' => 4,
      'data_type' => 'varchar',
      'length' => '20',
    ),
    'description' => 
    array (
      'table_name' => 'prod_stocks',
      'column_name' => 'description',
      'column_position' => 5,
      'data_type' => 'text',
    ),
    'create' => 
    array (
      'table_name' => 'prod_stocks',
      'column_name' => 'create',
      'column_position' => 6,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'prod_stocks',
      'column_name' => 'timestamp',
      'column_position' => 7,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'address' => 
    array (
      'table_name' => 'prod_stocks',
      'column_name' => 'address',
      'column_position' => 8,
      'data_type' => 'text',
    ),
  ),
  'sales_orders' => 
  array (
    'id' => 
    array (
      'table_name' => 'sales_orders',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'sales_orders',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'create' => 
    array (
      'table_name' => 'sales_orders',
      'column_name' => 'create',
      'column_position' => 3,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'sales_orders',
      'column_name' => 'timestamp',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'name' => 
    array (
      'table_name' => 'sales_orders',
      'column_name' => 'name',
      'column_position' => 5,
      'data_type' => 'varchar',
      'length' => '100',
    ),
    'status' => 
    array (
      'table_name' => 'sales_orders',
      'column_name' => 'status',
      'column_position' => 6,
      'data_type' => 'int',
    ),
    'fin_status' => 
    array (
      'table_name' => 'sales_orders',
      'column_name' => 'fin_status',
      'column_position' => 7,
      'data_type' => 'int',
    ),
    'discount' => 
    array (
      'table_name' => 'sales_orders',
      'column_name' => 'discount',
      'column_position' => 8,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '4',
    ),
    'discount_description' => 
    array (
      'table_name' => 'sales_orders',
      'column_name' => 'discount_description',
      'column_position' => 9,
      'data_type' => 'text',
    ),
    'description' => 
    array (
      'table_name' => 'sales_orders',
      'column_name' => 'description',
      'column_position' => 10,
      'data_type' => 'text',
    ),
    'items_sum' => 
    array (
      'table_name' => 'sales_orders',
      'column_name' => 'items_sum',
      'column_position' => 11,
      'data_type' => 'float',
      'default' => '0.00',
      'scale' => '2',
      'precision' => '10',
    ),
    'total_sum' => 
    array (
      'table_name' => 'sales_orders',
      'column_name' => 'total_sum',
      'column_position' => 12,
      'data_type' => 'float',
      'default' => '0.00',
      'scale' => '2',
      'precision' => '10',
    ),
  ),
  'sales_orderitems' => 
  array (
    'id' => 
    array (
      'table_name' => 'sales_orderitems',
      'column_name' => 'id',
      'column_position' => 1,
      'data_type' => 'int',
      'primary' => true,
      'primary_position' => 1,
      'identity' => true,
    ),
    'active' => 
    array (
      'table_name' => 'sales_orderitems',
      'column_name' => 'active',
      'column_position' => 2,
      'data_type' => 'tinyint',
      'default' => '1',
    ),
    'create' => 
    array (
      'table_name' => 'sales_orderitems',
      'column_name' => 'create',
      'column_position' => 3,
      'data_type' => 'timestamp',
      'nullable' => true,
    ),
    'timestamp' => 
    array (
      'table_name' => 'sales_orderitems',
      'column_name' => 'timestamp',
      'column_position' => 4,
      'data_type' => 'timestamp',
      'default' => 'CURRENT_TIMESTAMP',
    ),
    'name' => 
    array (
      'table_name' => 'sales_orderitems',
      'column_name' => 'name',
      'column_position' => 5,
      'data_type' => 'varchar',
      'length' => '100',
    ),
    'price' => 
    array (
      'table_name' => 'sales_orderitems',
      'column_name' => 'price',
      'column_position' => 6,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '10',
    ),
    'count' => 
    array (
      'table_name' => 'sales_orderitems',
      'column_name' => 'count',
      'column_position' => 7,
      'data_type' => 'int',
    ),
    'cost' => 
    array (
      'table_name' => 'sales_orderitems',
      'column_name' => 'cost',
      'column_position' => 8,
      'data_type' => 'float',
      'scale' => '2',
      'precision' => '10',
    ),
    'discount' => 
    array (
      'table_name' => 'sales_orderitems',
      'column_name' => 'discount',
      'column_position' => 9,
      'data_type' => 'float',
      'default' => '0.00',
      'scale' => '2',
      'precision' => '4',
    ),
    'serial' => 
    array (
      'table_name' => 'sales_orderitems',
      'column_name' => 'serial',
      'column_position' => 10,
      'data_type' => 'varchar',
      'length' => '200',
    ),
  ),
);
