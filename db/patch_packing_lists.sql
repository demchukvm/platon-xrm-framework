
-- --------------------------------------------------------

--
-- Table structure for table `logistic_packinglist_item`
--

CREATE TABLE IF NOT EXISTS `logistic_packinglist_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `archived` tinyint(4) DEFAULT '0',
  `create` timestamp NULL DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `count` int(11) NOT NULL,
  `cost` float(16,2) NOT NULL,
  `sum` float(16,2) NOT NULL,
  `plcs` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `archived` (`archived`),
  KEY `active` (`active`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `logistic_packinglist_item_log`
--

CREATE TABLE IF NOT EXISTS `logistic_packinglist_item_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `create` timestamp NULL DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `field_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `old_value` text COLLATE utf8_unicode_ci,
  `new_value` text COLLATE utf8_unicode_ci,
  `action` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `object_id` (`object_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `logistic_packinglist_pack`
--

CREATE TABLE IF NOT EXISTS `logistic_packinglist_pack` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `packinglist_id` int(11) NOT NULL,
  `pack_id` int(11) NOT NULL,
  `create` timestamp NULL DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `mr_logistic_packinglist_pack_index_packinglist_id` (`packinglist_id`),
  KEY `mr_logistic_packinglist_pack_index_pack_id` (`pack_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `logistic_packing_list`
--

CREATE TABLE IF NOT EXISTS `logistic_packing_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `archived` tinyint(4) DEFAULT '0',
  `create` timestamp NULL DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `net_weight` float(12,2) NOT NULL,
  `gross_weight` float(12,2) NOT NULL,
  `sum` float(16,2) NOT NULL,
  `plcs` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `archived` (`archived`),
  KEY `active` (`active`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `logistic_packing_list_log`
--

CREATE TABLE IF NOT EXISTS `logistic_packing_list_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `create` timestamp NULL DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `field_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `old_value` text COLLATE utf8_unicode_ci,
  `new_value` text COLLATE utf8_unicode_ci,
  `action` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `object_id` (`object_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `logistic_packlist_packlistitems`
--

CREATE TABLE IF NOT EXISTS `logistic_packlist_packlistitems` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `packinglist_id` int(11) NOT NULL,
  `packinglistitem_id` int(11) NOT NULL,
  `create` timestamp NULL DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `mr_logistic_packlist_packlistitems_index_packinglist_id` (`packinglist_id`),
  KEY `mr_logistic_packlist_packlistitems_index_packinglistitem_id` (`packinglistitem_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE IF NOT EXISTS `logistic_packinglistitem_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `packinglistitem_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `create` timestamp NULL DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `mr_logistic_packinglistitem_product_index_packinglistitem_id` (`packinglistitem_id`),
  KEY `mr_logistic_packinglistitem_product_index_product_id` (`product_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `logistic_package_packinglist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `package_id` int(11) NOT NULL,
  `packinglist_id` int(11) NOT NULL,
  `create` timestamp NULL DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `mr_logistic_package_packinglist_index_package_id` (`package_id`),
  KEY `mr_logistic_package_packinglist_index_packinglist_id` (`packinglist_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;