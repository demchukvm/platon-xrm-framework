CREATE TABLE IF NOT EXISTS `essentials` (
  `model` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `relation` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `mid` int(11) NOT NULL,
  `data` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
CREATE TABLE IF NOT EXISTS kcompany_circle_product  (
									`id` int(11) NOT NULL AUTO_INCREMENT,
									`circle_id` int(11) NOT NULL,
									`product_id` int(11) NOT NULL,
									`create` timestamp NULL DEFAULT NULL,
									`timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
									PRIMARY KEY (`id`),
									KEY `mr_kcompany_circle_product_index_circle_id` (`circle_id`),
  									KEY `mr_kcompany_circle_product_index_product_id` (`product_id`)
									) ENGINE = MYISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS kcompany_circle_deal  (
									`id` int(11) NOT NULL AUTO_INCREMENT,
									`circle_id` int(11) NOT NULL,
									`deal_id` int(11) NOT NULL,
									`create` timestamp NULL DEFAULT NULL,
									`timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
									PRIMARY KEY (`id`),
									KEY `mr_kcompany_circle_deal_index_circle_id` (`circle_id`),
  									KEY `mr_kcompany_circle_deal_index_deal_id` (`deal_id`)
									) ENGINE = MYISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
