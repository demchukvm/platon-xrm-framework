CREATE TABLE IF NOT EXISTS `fin_finoperationdealercopy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `finoperation_id` int(11) NOT NULL,
  `finoperation2_id` int(11) NOT NULL,
  `create` timestamp NULL DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `mr_fin__index_finoperation_id` (`finoperation_id`),
  KEY `mr_fin__index_finoperation2_id` (`finoperation2_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;