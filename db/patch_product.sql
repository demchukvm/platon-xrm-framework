CREATE TABLE IF NOT EXISTS `business_department_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `create` timestamp NULL DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `mr_business_department_lead_index_department_id` (`department_id`),
  KEY `mr_business_department_lead_index_lead_id` (`product_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

RENAME TABLE  `business_lead` TO  `business_product` ;
update `business_product` set `extend` = '|business.product|';
RENAME TABLE `business_lead_log` TO `business_product_log` ;