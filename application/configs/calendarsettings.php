<?php

return array(
    'crm.task' =>
    array(
        'start' => 'date_start',
        'end' => 'date_finish',
        'tag_rel' => 'taskmaintag',
    ),
    'crm.milestone' =>
    array(
        'start' => 'start',
        'end' => 'end',
        'tag_rel' => 'milestonemaintag',
    ),
)
?>
