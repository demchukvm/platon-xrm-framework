<?php
return array (
  'auth' => 
  array (
    'file' => '../library/Sl/Module/Auth/Module.php',
    'type' => 'Sl_Module',
    'dir' => '../library/Sl/Module/Auth',
  ),
  'home' => 
  array (
    'file' => '../library/Sl/Module/Home/Module.php',
    'type' => 'Sl_Module',
    'dir' => '../library/Sl/Module/Home',
  ),
  'menu' => 
  array (
    'file' => '../library/Sl/Module/Menu/Module.php',
    'type' => 'Sl_Module',
    'dir' => '../library/Sl/Module/Menu',
  ),
  'main' => 
  array (
    'file' => '../application/Module/Main/Module.php',
    'type' => 'Application_Module',
    'dir' => '../application/Module/Main',
  ),
  'business' => 
  array (
    'file' => '../application/Module/Business/Module.php',
    'type' => 'Application_Module',
    'dir' => '../application/Module/Business',
  ),
  'kfinance' => 
  array (
    'file' => '../application/Module/Kfinance/Module.php',
    'type' => 'Application_Module',
    'dir' => '../application/Module/Kfinance',
  ),
  'crm' => 
  array (
    'file' => '../library/Sl/Module/Crm/Module.php',
    'type' => 'Sl_Module',
    'dir' => '../library/Sl/Module/Crm',
  ),
  'kcompany' => 
  array (
    'file' => '../application/Module/Kcompany/Module.php',
    'type' => 'Application_Module',
    'dir' => '../application/Module/Kcompany',
  ),
    
  'kcomments' => 
  array (
    'file' => '../application/Module/Kcomments/Module.php',
    'type' => 'Application_Module',
    'dir' => '../application/Module/Kcomments',
  ), 
);
