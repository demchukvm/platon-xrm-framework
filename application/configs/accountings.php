<?php
return array(
    'accountings' => array(
        'finance.finacnt' => array(
            'tx' => 'finance.otransaction',
            'acnt' => 'finance.finacnt',
        ),
        'finance.cashdesc' => array(
            'tx' => 'finance.cdtransaction',
            'acnt' => 'finance.cdacnt',
        ),
        'customers.customer' => array(
            'tx' => 'customers.custtransaction',
            'acnt' => 'customers.custacnt',
        ),
    ),
    'deal_models' => array(
        'logistic.package' => true,
        'finance.paymove' => true,
    ),
    'docs_models' => array(
        'finance.payment' => true,
        'finance.ecashorder' => true,
    ),
    'acntcreation' => array('finance.cashdesc' => array(0 => 'cashdesccdacnt', ), 
                            'customers.customer' => array(0 => 'customercustacnt', )
    ),
    'documenting' => array(
        'logistic.package' => array('outlayreasonacnt' => array(
                'autocreate' => array('status' => array(0 => 90, ), ),
                'fields' => array(
                    'name' => 'identifier',
                    'sum' => 'base_cost',
                    'status' => 50,
                    'modulerelation_finacntoutlay' => 17,
                ),
            ), ),
        'finance.paymove' => array(
            'paymentreasonacnt' => array(
                'autocreate' => array('status' => array(100), ),
                'fields' => array(
                    'modulerelation_paymentcashdesc' => 'modulerelation_paymovecashdescdestination',
                    'date' => 'date',
                    'status' => 2,
                    'summ_approve' => 'summ_approve',
                    'description' => 'description'
                )
            ),
            'ecashorderreasonacnt' => array(
                'autocreate' => array('status' => array(100), ),
                'fields' => array(
                    'modulerelation_ecashordercashdesc' => 'modulerelation_paymovecashdesc',
                    'date' => 'date',
                    'sum' => 'summ',
                    'status' => 2,
                    'description' => 'description'
                )
            )
        ),
        'finance.cashorder' => array(),
    ),
    'transactioning' => array(
        'finance.outlay' => array('finacntoutlay' => array(
                'acnt' => array(0 => '20', ),
                'qty' => 'sum',
                'factor' => '-1',
                'reason_id' => 'outlayreasonacnt',
            )),

        'finance.payment' => array(
            'paymentcashdesc' => array(
                    'acnt' => 'cashdesccdacnt',
                    'qty' => 'summ_approve',
                    'factor' => '1',
                    'reason_id' => 'paymentreasonacnt',
                    'rate' => 'cource',
                    'currency_qty' => 'summ'
                ),
             
             'paymentcustomer' => array(
                    'acnt' => 'customercustacnt',
                    'qty' => 'summ_approve',
                    'factor' => '1',
                    'reason_id' => 'paymentreasonacnt',
                    
               )  
            ),

        'finance.ecashorder' => array('ecashordercashdesc' => array(
                'acnt' => 'cashdesccdacnt',
                'qty' => 'sum',
                'factor' => '-1',
                'reason_id' => 'ecashorderreasonacnt',
                'rate' => 1,
                'currency_qty' => 'sum'
            )),
    ),
);
