<?php
namespace Application\Module\Business\Listener;

use Sl_Listener_Abstract as AbstractListener;
use Sl_Model_Factory as ModelFactory;

use Application\Module\Business;

class Counterparty extends AbstractListener implements \Sl\Listener\Modelaction {
    
    protected $_parts = array(
        'sellside' => array(
            'relations' => array(
                'dealcounterpartysell'
            ),
            'multiple' => false,
            'usertype' => 'counterparty',
        ),
        'sell' => array(
            'relations' => array(
                'dealusertaxandaccsupport'
            ),
            'multiple' => false,
            'usertype' => 'user',
        ),
        'fin' => array(
            'relations' => array(
                'dealcounterpartyfinpart',
            ),
            'multiple' => true,
            'usertype' => 'counterparty',
        ),
        'operations' => array(
            'relations' => array(
                'dealusertrader',
                'dealuserlegalsupport'
            ),
            'multiple' => false,
            'usertype' => 'user',
        ),
        'buy' => array(
            'relations' => array(
                'dealuserprojectmanager'
            ),
            'multiple' => false,
            'usertype' => 'user',
        ),
        'buyside' => array(
            'relations' => array(
                'dealcounterparty'
            ),
            'multiple' => false,
            'usertype' => 'counterparty',
        ),
    );


    public function onBefore(\Sl\Event\Modelaction $event) {
        $model = $event->getModel();
        if($model instanceof Business\Model\Deal) {
            // Загрузка шаблонов попапчиков
            \Sl\Module\Home\Service\Quickforms::add(ModelFactory::model('user', 'auth'));
            \Sl\Module\Home\Service\Quickforms::add(ModelFactory::model('counterparty', 'business'));
            \Sl\Module\Home\Service\Quickforms::add(ModelFactory::model('contact', 'business'));
            if(in_array($event->getCurrentAction(), array(
                'edit',
                'detailed'
            ))) {
                if($model->getRates()) {
                    $event->getView()->rates = $model->getRates(true);
                } else {
                    $event->getView()->rates = array(
                        'sellside' => array('rate' => 10),
                        'sell' => array('rate' => 20),
                        'fin' => array('rate' => 20),
                        'operations' => array('rate' => 20),
                        'buy' => array('rate' => 20),
                        'buyside' => array('rate' => 10),
                    );
                }
                //print_r($model->getRates(true));die;
                $participators = array();
                $parts = array();
                foreach($this->_parts as $part=>$data) {
                    foreach($data['relations'] as $relation) {
                        if(!$model->issetRelated($relation)) {
                            $model = ModelFactory::mapper($model)->findRelation($model, $relation);
                        }
                        $related = $model->fetchRelated($relation);
                        foreach($related as $relModel) {
                            if(!isset($participators[$part])) {
                                $participators[$part] = array();
                            }
                            if($data['multiple']) {
                                $participators[$part][] = array(
                                    'relation' => $relation,
                                    'model' => $relModel
                                );
                            } else {
                                $participators[$part][0] = array(
                                    'relation' => $relation,
                                    'model' => $relModel
                                );
                            }
                            
                        }
                    }
                    $parts[$data['usertype']][] = $part;
                }
                $event->getView()->parts = $this->_parts;
                $event->getView()->parts_restrictions = $parts;
                $event->getView()->participators = $participators;
            }
        }
    }

    public function onBeforePost(\Sl\Event\Modelaction $event) {}
    public function onAfter(\Sl\Event\Modelaction $event) {}
    public function onAfterPost(\Sl\Event\Modelaction $event) {}

}