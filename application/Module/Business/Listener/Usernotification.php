<?php

namespace Application\Module\Business\Listener;

use Sl_Model_Abstract as AbstractModel;
use Sl_Model_Factory as ModelFactory;
use Sl_Module_Manager as ModuleManager;

use Application\Module\Business;

class Usernotification extends \Sl_Listener_Abstract implements \Sl_Listener_Model_Interface {

    const PRODUCT_DEAL_RELATION = 'productdeal';
    const PRODUCT_DEPARTMENT_RELATION = 'departmentproduct';
    const NOTIFICATION_USER_RELATION = 'notificationuser';
    const DEPARTMENT_CIRCLE_RELATION = 'departmentmaincircle';
    const CIRCLE_USERS_RELATION = 'circleusers';
    const TASK_USERS_RELATION = 'taskuser';
    const MILESTONE_USERS_RELATION = 'milestoneuser';
    
    const TASK_TAG = 'taskmaintag';
    const MILESTONE_TAG = 'milestonemaintag';
    
    protected $_view;
    
    public function onBeforeSave(\Sl_Event_Model $event) {
        
    }

    /**
     * Множественное создание уведомлений
     * 
     * @param \Sl_Model_Abstract $model
     * @param array $users
     */
    protected function _createNotification(\Sl_Model_Abstract $model, array $users) {
        $c_user = \Sl_Service_Acl::getCurrentUser();

        foreach ($users as $u) {
            if ($c_user instanceof \Sl\Module\Auth\Model\User && $u instanceof \Sl\Module\Auth\Model\User && $c_user->getId() != $u->getId()) {
                $notification = \Sl_Model_Factory::object('notification', 'home');
                $notification->setName(implode(' ', array(ucfirst($model->findModelName()), $model->__toString(), $this->getTranslator()->translate('created'))))
                        ->setStatus(0)
                        ->setData($this->_getView()->partial('partials/emails/'.$model->findModelName().'.phtml', array('user' => $c_user, 'model' => $model)))
                        ->assignRelated(self::NOTIFICATION_USER_RELATION, array($u));

                \Sl_Model_Factory::mapper($notification)->save($notification);
            }
        }
    }

    public function onAfterSave(\Sl_Event_Model $event) {
        $model = $event->getModel();
        $model_bu = $event->getModelBeforeUpdate();
        if(in_array(\Sl\Service\Helper::getModelAlias($model), array(
            'business.product',
            'business.deal',
        ))) { // То, что можно/нужно превратить в "Круг"
            if(!$model_bu->getId() && ($circle = $this->_getCircle($model))) {
                $this->_createNotification($model, $circle->fetchRelated(self::CIRCLE_USERS_RELATION));
            }
        } elseif ($model instanceof \Sl\Module\Crm\Model\Task) {
            // Уведомление формируется если:
            // Изменен получатель (назначен или переназначен)
            // Получатель не является текущим пользователем
            !$model->issetRelated(self::TASK_USERS_RELATION) && $model = \Sl_Model_Factory::mapper($model)->findRelation($model, self::TASK_USERS_RELATION);
            !$model_bu->issetRelated(self::TASK_USERS_RELATION) && $model_bu = \Sl_Model_Factory::mapper($model_bu)->findRelation($model_bu, self::TASK_USERS_RELATION);
            
            $user = $model->fetchOneRelated(self::TASK_USERS_RELATION);
            $user_bu = $model_bu->fetchOneRelated(self::TASK_USERS_RELATION);
            $c_user = \Sl_Service_Acl::getCurrentUser();
            
            if($user) {
                if(!$user_bu || !$user_bu->getId() || ($user->getId() != $user_bu->getId())) { // Изменен получатель
                    if($user->getId() != $c_user->getId()) { // Получатель не является текущим пользователем
                        if(!$model->issetRelated(self::TASK_TAG)) {
                            $model = ModelFactory::mapper($model)->findRelation($model, self::TASK_TAG);
                        }
                        $link = null;
                        $tag = $model->fetchOneRelated(self::TASK_TAG);
                        if($tag) {
                            $tag = ModelFactory::mapper($tag)->findRelation($tag, $tag->getMasterRelation());
                            if($tag) {
                                $link = $tag->fetchOneRelated($tag->getMasterRelation());
                            }
                        }
                        $notification = \Sl_Model_Factory::object('notification', 'home');
                        $notification->setName(implode(' ', array(ucfirst($model->findModelName()), $model->__toString(), $this->getTranslator()->translate('assigned to You'))))
                                ->setStatus(0)
                                ->setData($this->_getView()->partial('partials/emails/task.phtml', array('user' => $c_user, 'model' => $model, 'linkModel' => $link?$link:$model)))
                                ->assignRelated(self::NOTIFICATION_USER_RELATION, array($user));

                        \Sl_Model_Factory::mapper($notification)->save($notification);
                    }
                }
            }
        } elseif ($model instanceof \Sl\Module\Crm\Model\Milestone) {
            // Milestone обновляется если:
            // Назначен на пользователя
            // Достиг 80% выполнения, но не 100%
            !$model->issetRelated(self::MILESTONE_USERS_RELATION) && $model = \Sl_Model_Factory::mapper($model)->findRelation($model, self::MILESTONE_USERS_RELATION);
            !$model_bu->issetRelated(self::MILESTONE_USERS_RELATION) && $model_bu = \Sl_Model_Factory::mapper($model_bu)->findRelation($model_bu, self::MILESTONE_USERS_RELATION);
            
            $user = $model->fetchOneRelated(self::MILESTONE_USERS_RELATION);
            $user_bu = $model_bu->fetchOneRelated(self::MILESTONE_USERS_RELATION);
            $c_user = \Sl_Service_Acl::getCurrentUser();
            if($user) {
                if(!$user_bu || !$user_bu->getId() || ($user->getId() != $user_bu->getId())) { // Изменился получатель
                    if($user->getId() != $c_user->getId()) { // Получатель не является текущим пользователем
                        if(!$model->issetRelated(self::MILESTONE_TAG)) {
                            $model = ModelFactory::mapper($model)->findRelation($model, self::MILESTONE_TAG);
                        }
                        $link = null;
                        $tag = $model->fetchOneRelated(self::MILESTONE_TAG);
                        if($tag) {
                            $tag = ModelFactory::mapper($tag)->findRelation($tag, $tag->getMasterRelation());
                            if($tag) {
                                $link = $tag->fetchOneRelated($tag->getMasterRelation());
                            }
                        }
                        $notification = ModelFactory::model('notification', 'home');
                        $notification->setName(implode(' ', array(ucfirst($model->findModelName()), $model->__toString(), $this->getTranslator()->translate('assigned to You'))))
                                ->setStatus(0)
                                ->setData($this->_getView()->partial('partials/emails/milestone.phtml', array('user' => $c_user, 'model' => $model, 'linkModel' => $link?$link:$model)))
                                ->assignRelated(self::NOTIFICATION_USER_RELATION, array($user));

                        \Sl_Model_Factory::mapper($notification)->save($notification);
                    }
                }
            }
        }
    }
    
    protected function _getCircle(AbstractModel $model = null) {
        if(is_null($model)) {
            return null;
        }
        if($model instanceof Business\Model\Product) {
            !$model->issetRelated(self::PRODUCT_DEPARTMENT_RELATION) && $model = \Sl_Model_Factory::mapper($model)->findRelation($model, self::PRODUCT_DEPARTMENT_RELATION);
            $dep = $model->fetchOneRelated(self::PRODUCT_DEPARTMENT_RELATION);
            // Нашли подразделение
            if ($dep instanceof \Sl_Model_Abstract) {
                !$dep->issetRelated(self::DEPARTMENT_CIRCLE_RELATION) && $dep = \Sl_Model_Factory::mapper($dep)->findRelation($dep, self::DEPARTMENT_CIRCLE_RELATION);
                $circle = $dep->fetchOneRelated(self::DEPARTMENT_CIRCLE_RELATION);
                // Нашли "круг"
                if ($circle instanceof \Sl_Model_Abstract) {
                    !$circle->issetRelated(self::CIRCLE_USERS_RELATION) && $circle = \Sl_Model_Factory::mapper($circle)->findRelation($circle, self::CIRCLE_USERS_RELATION);
                    return $circle;
                }
            }
        } elseif($model instanceof Business\Model\Deal) {
            if(!$model->issetRelated(self::PRODUCT_DEAL_RELATION)) {
                $model = \Sl_Model_Factory::mapper($model)->findRelation($model, self::PRODUCT_DEAL_RELATION);
            }
            return $this->_getCircle($model->fetchOneRelated(self::PRODUCT_DEAL_RELATION));
        } else {
            throw new \Exception('Unknown model type. '.__METHOD__);
        }
    }
    
    /**
     * Возвращает view
     * 
     * @return \Sl_View
     */
    protected function _getView() {
        if(!isset($this->_view)) {
            $view = new \Sl_View();
            $view->addScriptPath(ModuleManager::getViewDirectory($this->getModule()->getName()));
            $this->_view = $view;
        }
        return $this->_view;
    }
}
