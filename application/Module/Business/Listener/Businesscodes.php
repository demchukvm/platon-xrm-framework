<?

namespace Application\Module\Business\Listener;

use Application\Module\Business\Model as Models;

class Businesscodes extends \Sl_Listener_Abstract implements \Sl_Listener_Model_Interface {

    public function onBeforeSave(\Sl_Event_Model $event) {

        $model = $event->getModel();
        if ($model instanceof Models\Product  || ($model instanceof Models\Subproduct)) {

            $model_before = $event->getModelBeforeUpdate();
            if (!strlen($model->getIdentifier())) {

                // print_r(\Sl_Model_Factory::mapper($model)->buildIdentifier($model)); die;
                $model->setIdentifier(\Sl_Model_Factory::mapper($model)->buildIdentifier($model));
            } elseif ($model_before->getIdentifier() != $model->getIdentifier()) {
                $model->setIdentifier($model_before->getIdentifier());
            }
        }
    }

    function onAfterSave(\Sl_Event_Model $event) {

        $model = $event->getModel();
        if ($model instanceof Models\Deal) {
            if (!strlen($model->getIdentifier())) {
                $model->setIdentifier(\Sl_Model_Factory::mapper($model)->buildIdentifier($model));
                $model = \Sl_Model_Factory::mapper($model)->save($model, true);
            }
        }
    }

}