<?php
return array (
  'model' => 
  array (
    'full_legal_company_name' => 
    array (
      'label' => '-',
      'type' => 'text',
    ),
    'registered_addr' => 
    array (
      'label' => 'Registered address',
      'type' => 'text',
    ),
    'regulatory_body' => 
    array (
      'label' => 'Regulatory body',
      'type' => 'text',
    ),
    'physical_addr' => 
    array (
      'label' => 'Physical address',
      'type' => 'text',
    ),
    'phone' => 
    array (
      'label' => 'Phone',
      'type' => 'text',
    ),
    'fax' => 
    array (
      'label' => 'Fax',
      'type' => 'text',
    ),
    'email' => 
    array (
      'label' => 'Email',
      'type' => 'text',
    ),
    'establishment_date' => 
    array (
      'label' => 'Establishment date',
      'type' => 'date',
    ),
    'business_line' => 
    array (
      'label' => 'Business line',
      'type' => 'text',
    ),
    'create' => 
    array (
      'label' => 'CREATE',
      'type' => 'text',
    ),
    'id' => 
    array (
      'label' => 'ID',
      'type' => 'hidden',
    ),
    'active' => 
    array (
      'label' => 'ACTIVE',
      'type' => 'checkbox',
    ),
    'archived' => 
    array (
      'label' => 'ARCHIVED',
      'type' => 'checkbox',
    ),
    'extend' => 
    array (
      'label' => 'EXTEND',
      'type' => 'text',
    ),
    'name' => array (
      'label' => 'Full legal company name',
      'type' => 'text',
    ),
  ),
);
