<?php
return array (
  'model' => 
  array (
    'name' => 
    array (
      'label' => 'Name',
      'type' => 'text',
    ),
    'phone' => 
    array (
      'label' => 'Phone',
      'type' => 'text',
    ),
    'fax' => 
    array (
      'label' => 'Fax',
      'type' => 'text',
    ),
    'email' => 
    array (
      'label' => 'Email',
      'type' => 'text',
    ),
    'create' => 
    array (
      'label' => 'CREATE',
      'type' => 'text',
    ),
    'id' => 
    array (
      'label' => 'ID',
      'type' => 'hidden',
    ),
    'active' => 
    array (
      'label' => 'ACTIVE',
      'type' => 'checkbox',
    ),
    'archived' => 
    array (
      'label' => 'ARCHIVED',
      'type' => 'checkbox',
    ),
    'extend' => 
    array (
      'label' => 'EXTEND',
      'type' => 'text',
    ),
    'main' => 
    array (
      'label' => 'MAIN',
      'type' => 'checkbox',
    ),
  ),
);
