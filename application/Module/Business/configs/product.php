<?php
return array (
  'model' => 
  array (
    'name' => 
    array (
      'label' => 'Name',
      'type' => 'text',
    ),
    'description' => 
    array (
      'label' => 'Description',
      'type' => 'textarea',
    ),
    'identifier' => 
    array (
      'label' => 'Id',
      'type' => 'hidden',
    ),
    'status' => 
    array (
      'label' => 'Status',
      'type' => 'hidden',
    ),
    'create' => 
    array (
      'label' => 'CREATE',
      'type' => 'hidden',
    ),
    'id' => 
    array (
      'label' => 'ID',
      'type' => 'hidden',
    ),
    'active' => 
    array (
      'label' => 'ACTIVE',
      'type' => 'hidden',
    ),
    'archived' => 
    array (
      'label' => 'ARCHIVED',
      'type' => 'text',
    ),
    'extend' => 
    array (
      'label' => 'EXTEND',
      'type' => 'text',
    ),
    'master_target' => 
    array (
      'label' => 'Master target',
      'type' => 'textarea',
    ),
    'target_date' => 
    array (
      'label' => 'Target Date',
      'type' => 'date',
      
    ),
    'target_sum' => 
    array (
      'label' => 'Target Sum',
      'type' => 'text',
      'class' => 'intPositive'  
    ),
  ),
);
