<?php
return array (
  'model' => 
  array (
    'name' => 
    array (
      'label' => 'Description',
      'type' => 'text',
    ),
    'description' => 
    array (
      'label' => 'Description',
      'type' => 'text',
    ),
    'identifier' => 
    array (
      'label' => 'Id.',
      'type' => 'text',
      'readonly' => 'readonly',
    ),
    'status' => 
    array (
      'label' => 'Status',
      'type' => 'text',
    ),
    'date' => 
    array (
      'label' => 'Date',
      'type' => 'date',
    ),
    'create' => 
    array (
      'label' => 'CREATE',
      'type' => 'hidden',
    ),
    'id' => 
    array (
      'label' => 'ID',
      'type' => 'hidden',
    ),
    'active' => 
    array (
      'label' => 'ACTIVE',
      'type' => 'hidden',
    ),
    'archived' => 
    array (
      'label' => 'ARCHIVED',
      'type' => 'text',
    ),
    'extend' => 
    array (
      'label' => 'EXTEND',
      'type' => 'text',
    ),
    'master_target' => 
    array (
      'label' => 'Master target',
      'type' => 'text',
    ),
    'quantity' => 
    array (
      'label' => 'Quantity',
      'type' => 'text',
    ),
    'quality' => 
    array (
      'label' => 'Quality',
      'type' => 'text',
    ),
    'buy_from' => 
    array (
      'label' => 'Buy from',
      'type' => 'text',
    ),
    'sell_to' => 
    array (
      'label' => 'Sell to',
      'type' => 'text',
    ),
    'our_role' => 
    array (
      'label' => 'Our role',
      'type' => 'text',
    ),
    'deal_type' => 
    array (
      'label' => 'Deal type',
      'type' => 'text',
    ),
    'shipping_from' => 
    array (
      'label' => 'Shipping from',
      'type' => 'text',
    ),
    'shipping_date' => 
    array (
      'label' => 'Shipping date',
      'type' => 'date',
    ),
    'delivery_destination' => 
    array (
      'label' => 'Delivery destination',
      'type' => 'text',
    ),
    'delivery_date' => 
    array (
      'label' => 'Delivery date',
      'type' => 'date',
    ),
    'payment_schedule_seller' => 
    array (
      'label' => 'Payment schedule seller',
      'type' => 'text',
    ),
    'total_amount_to_be_paid' => 
    array (
      'label' => 'Total amount to be paid',
      'type' => 'text',
    ),
    'risk_exposure_seller' => 
    array (
      'label' => 'Risk exposure seller',
      'type' => 'text',
    ),
    'payment_schedule_buyer' => 
    array (
      'label' => 'Payment schedule buyer',
      'type' => 'text',
    ),
    'total_amount_to_be_recieved' => 
    array (
      'label' => 'Total amount to be recieved',
      'type' => 'text',
    ),
    'risk_exposure_buyer' => 
    array (
      'label' => 'Risk exposure buyer',
      'type' => 'text',
    ),
    'type_of_transportation' => 
    array (
      'label' => 'Type of transportation',
      'type' => 'text',
    ),
    'delivery_schedule' => 
    array (
      'label' => 'Delivery schedule',
      'type' => 'text',
    ),
    'cost_of_goods_sold' => 
    array (
      'label' => 'Cost of goods sold',
      'type' => 'text',
    ),
    'gross_profit' => 
    array (
      'label' => 'Gross profit',
      'type' => 'text',
    ),
    'accounting_and_legal_fees' => 
    array (
      'label' => 'Accounting and legal fees',
      'type' => 'text',
    ),
    'bank_service_charges' => 
    array (
      'label' => 'Bank service charges',
      'type' => 'text',
    ),
    'financing_costs' => 
    array (
      'label' => 'Financing costs',
      'type' => 'text',
    ),
    'comissions' => 
    array (
      'label' => 'Comissions',
      'type' => 'text',
    ),
    'agency_fees' => 
    array (
      'label' => 'Agency fees',
      'type' => 'text',
    ),
    'insurance' => 
    array (
      'label' => 'Insurance',
      'type' => 'text',
    ),
    'interest' => 
    array (
      'label' => 'Interest',
      'type' => 'text',
    ),
    'transportation_logistics' => 
    array (
      'label' => 'Transportation logistics',
      'type' => 'text',
    ),
    'travel' => 
    array (
      'label' => 'Travel',
      'type' => 'text',
    ),
    'services' => 
    array (
      'label' => 'Services',
      'type' => 'text',
    ),
    'inventory_purchase' => 
    array (
      'label' => 'Inventory purchase',
      'type' => 'text',
    ),
    'sales_and_marketing' => 
    array (
      'label' => 'Sales and marketing',
      'type' => 'text',
    ),
    'research_and_development' => 
    array (
      'label' => 'Research and development',
      'type' => 'text',
    ),
    'general_administrative' => 
    array (
      'label' => 'General administrative',
      'type' => 'text',
    ),
    'miscellaneous' => 
    array (
      'label' => 'Miscellaneous',
      'type' => 'text',
    ),
    'deprecation_and_amortization' => 
    array (
      'label' => 'Deprecation and amortization',
      'type' => 'text',
    ),
    'taxes' => 
    array (
      'label' => 'Taxes',
      'type' => 'text',
    ),
    'net_profit' => 
    array (
      'label' => 'Net profit',
      'type' => 'text',
    ),
    'our_company_buy_name' => 
    array (
      'label' => 'Our company buy name',
      'type' => 'text',
    ),
    'our_company_buy_registerede_addr' => 
    array (
      'label' => 'Our company buy registerede addr',
      'type' => 'text',
    ),
    'our_company_buy_physical_addr' => 
    array (
      'label' => 'Our company buy physical addr',
      'type' => 'text',
    ),
    'our_company_buy_phone' => 
    array (
      'label' => 'Our company buy phone',
      'type' => 'text',
    ),
    'our_company_buy_fax' => 
    array (
      'label' => 'Our company buy fax',
      'type' => 'text',
    ),
    'our_company_buy_email' => 
    array (
      'label' => 'Our company buy email',
      'type' => 'text',
    ),
    'our_company_sell_name' => 
    array (
      'label' => 'Our company sell name',
      'type' => 'text',
    ),
    'our_company_sell_registerede_addr' => 
    array (
      'label' => 'Our company sell registerede addr',
      'type' => 'text',
    ),
    'our_company_sell_physical_addr' => 
    array (
      'label' => 'Our company sell physical addr',
      'type' => 'text',
    ),
    'our_company_sell_phone' => 
    array (
      'label' => 'Our company sell phone',
      'type' => 'text',
    ),
    'our_company_sell_fax' => 
    array (
      'label' => 'Our company sell fax',
      'type' => 'text',
    ),
    'our_company_sell_email' => 
    array (
      'label' => 'Our company sell email',
      'type' => 'text',
    ),
    'counterparty_seller_full_legal_company_name' => 
    array (
      'label' => 'Counterparty seller full legal company name',
      'type' => 'text',
    ),
    'counterparty_seller_registered_address' => 
    array (
      'label' => 'Counterparty seller registered address',
      'type' => 'text',
    ),
    'counterparty_seller_regulatory_body' => 
    array (
      'label' => 'Counterparty seller regulatory body',
      'type' => 'text',
    ),
    'counterparty_seller_physical_addr' => 
    array (
      'label' => 'Counterparty seller physical addr',
      'type' => 'text',
    ),
    'counterparty_seller_phone' => 
    array (
      'label' => 'Counterparty seller phone',
      'type' => 'text',
    ),
    'counterparty_seller_fax' => 
    array (
      'label' => 'Counterparty seller fax',
      'type' => 'text',
    ),
    'counterparty_seller_email' => 
    array (
      'label' => 'Counterparty seller email',
      'type' => 'text',
    ),
    'counterparty_seller_establishment_date' => 
    array (
      'label' => 'Counterparty seller establishment date',
      'type' => 'date',
    ),
    'counterparty_seller_business_line' => 
    array (
      'label' => 'Counterparty seller business line',
      'type' => 'text',
    ),
    'counterparty_seller_contact_name' => 
    array (
      'label' => 'Counterparty seller contact name',
      'type' => 'text',
    ),
    'counterparty_seller_contact_phone' => 
    array (
      'label' => 'Counterparty seller contact phone',
      'type' => 'text',
    ),
    'counterparty_seller_contact_fax' => 
    array (
      'label' => 'Counterparty seller contact fax',
      'type' => 'text',
    ),
    'counterparty_seller_contact_email' => 
    array (
      'label' => 'Counterparty seller contact email',
      'type' => 'text',
    ),
    'counterparty_seller_signee_name' => 
    array (
      'label' => 'Counterparty seller signee name',
      'type' => 'text',
    ),
    'counterparty_seller_signee_phone' => 
    array (
      'label' => 'Counterparty seller signee phone',
      'type' => 'text',
    ),
    'counterparty_seller_signee_fax' => 
    array (
      'label' => 'Counterparty seller signee fax',
      'type' => 'text',
    ),
    'counterparty_seller_signee_email' => 
    array (
      'label' => 'Counterparty seller signee email',
      'type' => 'text',
    ),
    'counterparty_seller_signee_autorized_by' => 
    array (
      'label' => 'Counterparty seller signee autorized by',
      'type' => 'text',
    ),
    'counterparty_buyer_full_legal_company_name' => 
    array (
      'label' => 'Counterparty buyer full legal company name',
      'type' => 'text',
    ),
    'counterparty_buyer_registered_address' => 
    array (
      'label' => 'Counterparty buyer registered address',
      'type' => 'text',
    ),
    'counterparty_buyer_regulatory_body' => 
    array (
      'label' => 'Counterparty buyer regulatory body',
      'type' => 'text',
    ),
    'counterparty_buyer_physical_addr' => 
    array (
      'label' => 'Counterparty buyer physical addr',
      'type' => 'text',
    ),
    'counterparty_buyer_phone' => 
    array (
      'label' => 'Counterparty buyer phone',
      'type' => 'text',
    ),
    'counterparty_buyer_fax' => 
    array (
      'label' => 'Counterparty buyer fax',
      'type' => 'text',
    ),
    'counterparty_buyer_email' => 
    array (
      'label' => 'Counterparty buyer email',
      'type' => 'text',
    ),
    'counterparty_buyer_establishment_date' => 
    array (
      'label' => 'Counterparty buyer establishment date',
      'type' => 'text',
    ),
    'counterparty_buyer_business_line' => 
    array (
      'label' => 'Counterparty buyer business line',
      'type' => 'text',
    ),
    'counterparty_buyer_contact_name' => 
    array (
      'label' => 'Counterparty buyer contact name',
      'type' => 'text',
    ),
    'counterparty_buyer_contact_phone' => 
    array (
      'label' => 'Counterparty buyer contact phone',
      'type' => 'text',
    ),
    'counterparty_buyer_contact_fax' => 
    array (
      'label' => 'Counterparty buyer contact fax',
      'type' => 'text',
    ),
    'counterparty_buyer_contact_email' => 
    array (
      'label' => 'Counterparty buyer contact email',
      'type' => 'text',
    ),
    'counterparty_buyer_signee_name' => 
    array (
      'label' => 'Counterparty buyer signee name',
      'type' => 'text',
    ),
    'counterparty_buyer_signee_phone' => 
    array (
      'label' => 'Counterparty buyer signee phone',
      'type' => 'text',
    ),
    'counterparty_buyer_signee_fax' => 
    array (
      'label' => 'Counterparty buyer signee fax',
      'type' => 'text',
    ),
    'counterparty_buyer_signee_email' => 
    array (
      'label' => 'Counterparty buyer signee email',
      'type' => 'text',
    ),
    'counterparty_buyer_signee_autorized_by' => 
    array (
      'label' => 'Counterparty buyer signee autorized by',
      'type' => 'text',
    ),
    'expenses' => 
    array (
      'label' => 'EXPENSES',
      'type' => 'text',
    ),
    'target_date' => 
    array (
      'label' => 'Target Date',
      'type' => 'date',
    ),
    'target_sum' => 
    array (
      'label' => 'Target Sum',
      'type' => 'text',
      'class' => 'intPositive',
    ),
    'rates' => 
    array (
      'label' => 'RATES',
      'type' => 'text',
    ),
  ),
);
