<?php
return array (
  'modulerelations' => 
  array (
    0 => 
    array (
      'type' => '12',
      'db_table' => 'Application\\Module\\Business\\Modulerelation\\Table\\Departmentproduct',
      'options' => 
      array (
      ),
    ),
    1 => 
    array (
      'type' => '12',
      'db_table' => 'Application\\Module\\Business\\Modulerelation\\Table\\Productpost',
      'options' => 
      array (
      ),
    ),
    2 => 
    array (
      'type' => '12',
      'db_table' => 'Application\\Module\\Business\\Modulerelation\\Table\\Productdeal',
      'options' => 
      array (
      ),
    ),
    3 => 
    array (
      'type' => '12',
      'db_table' => 'Application\\Module\\Business\\Modulerelation\\Table\\Productsubproduct',
      'options' => 
      array (
      ),
    ),
    4 => 
    array (
      'type' => '12',
      'db_table' => 'Application\\Module\\Business\\Modulerelation\\Table\\Subproductdeal',
      'options' => 
      array (
      ),
    ),
    5 => 
    array (
      'type' => '12',
      'db_table' => 'Application\\Module\\Business\\Modulerelation\\Table\\Dealpost',
      'options' => 
      array (
      ),
    ),
    6 => 
    array (
      'type' => '21',
      'db_table' => 'Application\\Module\\Business\\Modulerelation\\Table\\Dealincotermsbuy',
      'options' => 
      array (
      ),
    ),
    7 => 
    array (
      'type' => '21',
      'db_table' => 'Application\\Module\\Business\\Modulerelation\\Table\\Dealincotermssell',
      'options' => 
      array (
      ),
    ),
    8 => 
    array (
      'type' => '21',
      'db_table' => 'Application\\Module\\Business\\Modulerelation\\Table\\Dealcounterparty',
      'options' => 
      array (
      ),
    ),
    9 => 
    array (
      'type' => '21',
      'db_table' => 'Application\\Module\\Business\\Modulerelation\\Table\\Dealcontact',
      'options' => 
      array (
      ),
    ),
    10 => 
    array (
      'type' => '12',
      'db_table' => 'Application\\Module\\Business\\Modulerelation\\Table\\Counterpartycontact',
      'options' => 
      array (
         'essentials' => array(
              'business.contact' 
          )
        
      ),
    ),
    11 => 
    array (
      'type' => '21',
      'db_table' => 'Application\\Module\\Business\\Modulerelation\\Table\\Dealcounterpartysell',
      'options' => 
      array (
      ),
    ),
    12 => 
    array (
      'type' => '11',
      'db_table' => 'Application\\Module\\Business\\Modulerelation\\Table\\Dealcontact',
      'options' => 
      array (
      ),
    ),
    13 => 
    array (
      'type' => '21',
      'db_table' => 'Application\\Module\\Business\\Modulerelation\\Table\\Dealcontactsell',
      'options' => 
      array (
      ),
    ),
    14 => 
    array (
      'type' => '21',
      'db_table' => 'Application\\Module\\Business\\Modulerelation\\Table\\Dealcontactsignee',
      'options' => 
      array (
      ),
    ),
    15 => 
    array (
      'type' => '21',
      'db_table' => 'Application\\Module\\Business\\Modulerelation\\Table\\Dealcontactsellsignee',
      'options' => 
      array (
      ),
    ),
    16 => 
    array (
      'type' => '21',
      'db_table' => 'Application\\Module\\Business\\Modulerelation\\Table\\Dealuserprojectmanager',
      'options' => 
      array (
      ),
    ),
    17 => 
    array (
      'type' => '21',
      'db_table' => 'Application\\Module\\Business\\Modulerelation\\Table\\Dealusertrader',
      'options' => 
      array (
      ),
    ),
    18 => 
    array (
      'type' => '21',
      'db_table' => 'Application\\Module\\Business\\Modulerelation\\Table\\Dealuserlegalsupport',
      'options' => 
      array (
      ),
    ),
    19 => 
    array (
      'type' => '21',
      'db_table' => 'Application\\Module\\Business\\Modulerelation\\Table\\Dealusertaxandaccsupport',
      'options' => 
      array (
      ),
    ),
    20 => 
    array (
      'type' => '12',
      'db_table' => 'Application\\Module\\Business\\Modulerelation\\Table\\Dealfile',
      'options' => 
      array (
      ),
    ),
    21 => 
    array (
      'type' => '12',
      'db_table' => 'Application\\Module\\Business\\Modulerelation\\Table\\Productfile',
      'options' => 
      array (
      ),
    ),
    22 => 
    array (
      'type' => '22',
      'db_table' => 'Application\\Module\\Business\\Modulerelation\\Table\\Dealcounterpartyfinpart',
      'options' => 
      array (
      ),
    ),
    23 => 
    array (
      'type' => '12',
      'db_table' => 'Application\\Module\\Business\\Modulerelation\\Table\\Pipedepartment',
      'options' => 
      array (
      ),
    ),
  ),
  'forms' => 
  array (
    'model_pipe_form' => 
    array (
      'identifier' => 
      array (
        'required' => true,
        'validators' => 
        array (
          'sl' => 
          array (
            'name' => 'StringLength',
            'options' => 
            array (
              'min' => 1,
              'max' => 1,
            ),
          ),
        ),
      ),
    ),
    'model_product_form' => 
    array (
      'modulerelation_departmentproduct' => 
      array (
        'sort_order' => 10,
        'label' => 'Department',
        'required' => true,
      ),
      'name' => 
      array (
        'sort_order' => 20,
      ),
      'description' => 
      array (
        'sort_order' => 40,
      ),
    ),
    'model_counterparty_form' => 
    array (
      
      'modulerelation_counterpartycontact' => 
      array (
        'label' => 'Contacts',
        'include_decorator' => 'ContactListTable',
        'show_field' => 
        array (
          0 => 'name',
          1 => 'phone',
          2 => 'email',
          
        ),
        'show_field_label' => 
        array (
          0 => 'Name',
          1 => 'Phone',
          2 => 'Email',
          
        ),
        'skipId' => '1',
        'class' => 'form-control',
      ),
    ),
    'model_subproduct_form' => 
    array (
      'modulerelation_productsubproduct' => 
      array (
        'label' => 'Project',
        'required' => true,
      ),
      'modulerelation_subproductdeal' => 
      array (
        'label' => 'Deal',
        'required' => true,
      ),
    ),
    'model_deal_form' => 
    array (
      'modulerelation_productdeal' => 
      array (
        'label' => 'Project',
        'required' => true,
        'sort_order' => 40,
      ),
      'modulerelation_dealuserprojectmanager' => 
      array (
        'label' => 'Project Manager',
      ),
      'modulerelation_dealusertrader' => 
      array (
        'label' => 'Trader',
      ),
      'modulerelation_dealusertaxandaccsupport' => 
      array (
        'label' => 'Tax and Accounting Support',
      ),
      'modulerelation_dealuserlegalsupport' => 
      array (
        'label' => 'Legal Support',
      ),
      'modulerelation_dealcounterparty' => 
      array (
        'label' => 'Counterparty (buyer)',
      ),
      'modulerelation_dealcounterpartysell' => 
      array (
        'label' => 'Counterparty (seller)',
      ),
      'modulerelation_dealcontact' => 
      array (
        'label' => 'Contact Person',
      ),
      'modulerelation_dealcontactsell' => 
      array (
        'label' => 'Contact Person',
      ),
      'modulerelation_dealcontactsignee' => 
      array (
        'label' => 'Signee Person',
      ),
      'modulerelation_dealcontactsellsignee' => 
      array (
        'label' => 'Signee Person',
      ),
      'name' => 
      array (
        'sort_order' => 80,
      ),
      'modulerelation_subproductdeal' => 
      array (
        'label' => 'Product',
        'required' => true,
        'sort_order' => 50,
        'field_filters' => 
        array (
          0 => 
          array (
            'field' => 'productsubproduct:id',
            'matching' => 'in',
            'value' => 'modulerelation_productdeal',
            'strong' => true,
          ),
        ),
      ),
      'modulerelation_dealincotermsbuy' => 
      array (
        'label' => 'Incoterms (buy)',
      ),
      'modulerelation_dealincotermssell' => 
      array (
        'label' => 'Incoterms (sell)',
      ),
    ),
  ),
  'left_navigation_pages' => 
  array (
    1 => 
    array (
      'module' => 'business',
      'controller' => 'product',
      'action' => 'list',
      'label' => 'Projects',
      'id' => 'Products',
      'icon' => 'fa fa-th-large fa-lg',
      'order' => 10,
    ),
    2 => 
    array (
      'module' => 'business',
      'controller' => 'deal',
      'action' => 'list',
      'label' => 'Deals',
      'icon' => 'fa fa-check-square-o fa-lg',
      'id' => 'Deals',
      'order' => 30,
    ),
    3 => 
    array (
      'parent' => 'others',
      'module' => 'home',
      'controller' => 'country',
      'action' => 'list',
      'label' => 'Countries',
      'icon' => 'fa fa-globe fa-lg',
      'order' => 50,
    ),
    4 => 
    array (
      'href' => '#deal',
      'parent' => 'add_new',
      'resource' => 'mvc:business|deal|ajaxcreate',
      'label' => 'Deal',
      'icon' => 'fa fa-globe fa-lg',
      'attribs' => 
      array (
        'data-toggle' => 'popover',
        'data-alias' => 'business.deal',
        'rel' => 'popover',
        'data-html' => 'true',
        'data-placement' => 'bottom',
        'data-original-title' => '<button type=\'button\' class=\'close pull-right\' data-dismiss=\'popover\'><i class=\'fa fa-times\'></i></button>Create',
        'data-content' => '<span id=\'form-create\' class=\'fa fa-spinner\' data-alias=\'business.deal\' data-rel =\'/business/deal/ajaxcreate\'> </span>',
      ),
    ),
    6 => 
    array (
      'parent' => 'others',
      'module' => 'business',
      'controller' => 'incoterms',
      'action' => 'list',
      'label' => 'Incoterms',
      'icon' => 'fa fa-globe fa-lg',
      'order' => 60,
    ),
    7 => 
    array (
      'parent' => 'others',
      'module' => 'business',
      'controller' => 'counterparty',
      'action' => 'list',
      'label' => 'Counterparties',
      'order' => 70,
    ),
    8 => 
    array (
      'parent' => 'others',
      'module' => 'business',
      'controller' => 'contact',
      'action' => 'list',
      'label' => 'Contacts',
      'order' => 80,
    ),
    13 => 
    array (
      'href' => '#product',
      'parent' => 'add_new',
      'resource' => 'mvc:business|product|ajaxcreate',
      'label' => 'Project',
      'icon' => 'fa fa-globe fa-lg',
      'attribs' => 
      array (
        'data-toggle' => 'popover',
        'data-alias' => 'business.product',
        'rel' => 'popover',
        'data-html' => 'true',
        'data-placement' => 'bottom',
        'data-original-title' => '<button type=\'button\' class=\'close pull-right\' data-dismiss=\'popover\'><i class=\'fa fa-times\'></i></button>Create',
        'data-content' => '<span id=\'form-create\' class=\'fa fa-spinner\' data-alias=\'business.product\' data-rel =\'/business/product/ajaxcreate\'> </span>',
      ),
    ),
    14 => 
    array (
      'href' => '#task',
      'parent' => 'add_new',
      'resource' => 'mvc:crm|task|ajaxcreate',
      'label' => 'Task',
      'icon' => 'fa fa-globe fa-lg',
      'attribs' => 
      array (
        'data-toggle' => 'popover',
        'data-alias' => 'crm.task',
        'rel' => 'popover',
        'data-html' => 'true',
        'data-placement' => 'bottom',
        'data-original-title' => '<button type=\'button\' class=\'close pull-right\' data-dismiss=\'popover\'><i class=\'fa fa-times\'></i></button>Create',
        'data-content' => '<span id=\'form-create\' class=\'fa fa-spinner\' data-alias=\'crm.task\' data-rel =\'/crm/task/ajaxcreate\'> </span>',
      ),
    ),
    15 => 
    array (
      'parent' => 'others',
      'module' => 'business',
      'controller' => 'pipe',
      'action' => 'list',
      'label' => 'Pipes',
      'order' => 100,
    ),
  ),
  'navigation_pages' => 
  array (
  ),
  'listview_options' => 
  array (
    'product' => 
    array (
      'name' => 
      array (
        'order' => 10,
        'label' => 'Name',
      ),
      'description' => 
      array (
        'order' => 20,
        'label' => 'Description',
      ),
      'identifier' => 
      array (
        'order' => 30,
        'label' => 'Id',
      ),
      'status' => 
      array (
        'order' => 40,
        'label' => 'Status',
      ),
      'archived' => 
      array (
        'order' => 50,
        'label' => 'ARCHIVED',
      ),
      'extend' => 
      array (
        'order' => 60,
        'label' => 'EXTEND',
      ),
      'master_target' => 
      array (
        'order' => 70,
        'label' => 'Master target',
      ),
    ),
    'subproduct' => 
    array (
      'name' => 
      array (
        'order' => 10,
        'label' => 'Name',
      ),
      'code' => 
      array (
        'order' => 20,
        'label' => 'Code',
      ),
      'archived' => 
      array (
        'order' => 30,
        'label' => 'ARCHIVED',
      ),
      'extend' => 
      array (
        'order' => 40,
        'label' => 'EXTEND',
      ),
      'midulrelation_productsubproduct' => 
      array (
        'order' => 50,
        'label' => 'Project',
      ),
      'midulrelation_subproductdeal' => 
      array (
        'order' => 60,
        'label' => 'Deal',
      ),
    ),
    'deal' => 
    array (
      'name' => 
      array (
        'order' => 10,
        'label' => 'Name',
      ),
      'description' => 
      array (
        'order' => 20,
        'label' => 'Description',
      ),
      'identifier' => 
      array (
        'order' => 30,
        'label' => 'Id.',
      ),
      'status' => 
      array (
        'order' => 40,
        'label' => 'Status',
      ),
      'date' => 
      array (
        'order' => 50,
        'label' => 'Date',
      ),
      'archived' => 
      array (
        'order' => 60,
        'label' => 'ARCHIVED',
      ),
      'extend' => 
      array (
        'order' => 70,
        'label' => 'EXTEND',
      ),
      'productdeal.name' => 
      array (
        'order' => 80,
        'label' => 'Project',
      ),
      'subproductdeal.name' => 
      array (
        'order' => 90,
        'label' => 'Product',
      ),
    ),
    'counterparty' => 
    array (
      'full_legal_company_name' => 
      array (
        'order' => 10,
        'label' => 'Full legal company name',
      ),
      'registered_addr' => 
      array (
        'order' => 20,
        'label' => 'Registered address',
      ),
      'regulatory_body' => 
      array (
        'order' => 30,
        'label' => 'Regulatory body',
      ),
      'physical_addr' => 
      array (
        'order' => 40,
        'label' => 'Physical address',
      ),
      'phone' => 
      array (
        'order' => 50,
        'label' => 'Phone',
      ),
      'fax' => 
      array (
        'order' => 60,
        'label' => 'Fax',
      ),
      'email' => 
      array (
        'order' => 70,
        'label' => 'Email',
      ),
      'establishment_date' => 
      array (
        'order' => 80,
        'label' => 'Establishment date',
      ),
      'business_line' => 
      array (
        'order' => 90,
        'label' => 'Business line',
      ),
      'archived' => 
      array (
        'order' => 100,
        'label' => 'ARCHIVED',
      ),
      'extend' => 
      array (
        'order' => 110,
        'label' => 'EXTEND',
      ),
    ),
    'contact' => 
    array (
      'name' => 
      array (
        'order' => 10,
        'label' => 'Name',
      ),
      'phone' => 
      array (
        'order' => 20,
        'label' => 'Phone',
      ),
      'fax' => 
      array (
        'order' => 30,
        'label' => 'Fax',
      ),
      'email' => 
      array (
        'order' => 40,
        'label' => 'Email',
      ),
      'archived' => 
      array (
        'order' => 50,
        'label' => 'ARCHIVED',
      ),
      'extend' => 
      array (
        'order' => 60,
        'label' => 'EXTEND',
      ),
    ),
  ),
  'lists' => 
  array (
    'business_dealstatus' => 'business.dealstatus',
    'business_dealtype' => 'business.dealtype',
    'business_dealtypetransportation' => 'business.dealtypetransportation',
  ),
  'duplicate' => 
  array (
    'deal' => 
    array (
      'name' => 
      array (
        'label' => 'Description',
      ),
      'description' => 
      array (
        'label' => 'Description',
      ),
      'status' => 
      array (
        'label' => 'Status',
      ),
      'date' => 
      array (
        'label' => 'Date',
      ),
      'extend' => 
      array (
        'label' => 'EXTEND',
      ),
      'master_target' => 
      array (
        'label' => 'Master target',
      ),
      'quantity' => 
      array (
        'label' => 'Quantity',
      ),
      'quality' => 
      array (
        'label' => 'Quality',
      ),
      'buy_from' => 
      array (
        'label' => 'Buy from',
      ),
      'sell_to' => 
      array (
        'label' => 'Sell to',
      ),
      'our_role' => 
      array (
        'label' => 'Our role',
      ),
      'deal_type' => 
      array (
        'label' => 'Deal type',
      ),
      'shipping_from' => 
      array (
        'label' => 'Shipping from',
      ),
      'shipping_date' => 
      array (
        'label' => 'Shipping date',
      ),
      'delivery_destination' => 
      array (
        'label' => 'Delivery destination',
      ),
      'delivery_date' => 
      array (
        'label' => 'Delivery date',
      ),
      'payment_schedule_seller' => 
      array (
        'label' => 'Payment schedule seller',
      ),
      'total_amount_to_be_paid' => 
      array (
        'label' => 'Total amount to be paid',
      ),
      'risk_exposure_seller' => 
      array (
        'label' => 'Risk exposure seller',
      ),
      'payment_schedule_buyer' => 
      array (
        'label' => 'Payment schedule buyer',
      ),
      'total_amount_to_be_recieved' => 
      array (
        'label' => 'Total amount to be recieved',
      ),
      'risk_exposure_buyer' => 
      array (
        'label' => 'Risk exposure buyer',
      ),
      'type_of_transportation' => 
      array (
        'label' => 'Type of transportation',
      ),
      'delivery_schedule' => 
      array (
        'label' => 'Delivery schedule',
      ),
      'cost_of_goods_sold' => 
      array (
        'label' => 'Cost of goods sold',
      ),
      'gross_profit' => 
      array (
        'label' => 'Gross profit',
      ),
      'accounting_and_legal_fees' => 
      array (
        'label' => 'Accounting and legal fees',
      ),
      'bank_service_charges' => 
      array (
        'label' => 'Bank service charges',
      ),
      'financing_costs' => 
      array (
        'label' => 'Financing costs',
      ),
      'comissions' => 
      array (
        'label' => 'Comissions',
      ),
      'agency_fees' => 
      array (
        'label' => 'Agency fees',
      ),
      'insurance' => 
      array (
        'label' => 'Insurance',
      ),
      'interest' => 
      array (
        'label' => 'Interest',
      ),
      'transportation_logistics' => 
      array (
        'label' => 'Transportation logistics',
      ),
      'travel' => 
      array (
        'label' => 'Travel',
      ),
      'services' => 
      array (
        'label' => 'Services',
      ),
      'inventory_purchase' => 
      array (
        'label' => 'Inventory purchase',
      ),
      'sales_and_marketing' => 
      array (
        'label' => 'Sales and marketing',
      ),
      'research_and_development' => 
      array (
        'label' => 'Research and development',
      ),
      'general_administrative' => 
      array (
        'label' => 'General administrative',
      ),
      'miscellaneous' => 
      array (
        'label' => 'Miscellaneous',
      ),
      'deprecation_and_amortization' => 
      array (
        'label' => 'Deprecation and amortization',
      ),
      'taxes' => 
      array (
        'label' => 'Taxes',
      ),
      'net_profit' => 
      array (
        'label' => 'Net profit',
      ),
      'our_company_buy_name' => 
      array (
        'label' => 'Our company buy name',
      ),
      'our_company_buy_registerede_addr' => 
      array (
        'label' => 'Our company buy registerede addr',
      ),
      'our_company_buy_physical_addr' => 
      array (
        'label' => 'Our company buy physical addr',
      ),
      'our_company_buy_phone' => 
      array (
        'label' => 'Our company buy phone',
      ),
      'our_company_buy_fax' => 
      array (
        'label' => 'Our company buy fax',
      ),
      'our_company_buy_email' => 
      array (
        'label' => 'Our company buy email',
      ),
      'our_company_sell_name' => 
      array (
        'label' => 'Our company sell name',
      ),
      'our_company_sell_registerede_addr' => 
      array (
        'label' => 'Our company sell registerede addr',
      ),
      'our_company_sell_physical_addr' => 
      array (
        'label' => 'Our company sell physical addr',
      ),
      'our_company_sell_phone' => 
      array (
        'label' => 'Our company sell phone',
      ),
      'our_company_sell_fax' => 
      array (
        'label' => 'Our company sell fax',
      ),
      'our_company_sell_email' => 
      array (
        'label' => 'Our company sell email',
      ),
      'counterparty_seller_full_legal_company_name' => 
      array (
        'label' => 'Counterparty seller full legal company name',
      ),
      'counterparty_seller_registered_address' => 
      array (
        'label' => 'Counterparty seller registered address',
      ),
      'counterparty_seller_regulatory_body' => 
      array (
        'label' => 'Counterparty seller regulatory body',
      ),
      'counterparty_seller_physical_addr' => 
      array (
        'label' => 'Counterparty seller physical addr',
      ),
      'counterparty_seller_phone' => 
      array (
        'label' => 'Counterparty seller phone',
      ),
      'counterparty_seller_fax' => 
      array (
        'label' => 'Counterparty seller fax',
      ),
      'counterparty_seller_email' => 
      array (
        'label' => 'Counterparty seller email',
      ),
      'counterparty_seller_establishment_date' => 
      array (
        'label' => 'Counterparty seller establishment date',
      ),
      'counterparty_seller_business_line' => 
      array (
        'label' => 'Counterparty seller business line',
      ),
      'counterparty_seller_contact_name' => 
      array (
        'label' => 'Counterparty seller contact name',
      ),
      'counterparty_seller_contact_phone' => 
      array (
        'label' => 'Counterparty seller contact phone',
      ),
      'counterparty_seller_contact_fax' => 
      array (
        'label' => 'Counterparty seller contact fax',
      ),
      'counterparty_seller_contact_email' => 
      array (
        'label' => 'Counterparty seller contact email',
      ),
      'counterparty_seller_signee_name' => 
      array (
        'label' => 'Counterparty seller signee name',
      ),
      'counterparty_seller_signee_phone' => 
      array (
        'label' => 'Counterparty seller signee phone',
      ),
      'counterparty_seller_signee_fax' => 
      array (
        'label' => 'Counterparty seller signee fax',
      ),
      'counterparty_seller_signee_email' => 
      array (
        'label' => 'Counterparty seller signee email',
      ),
      'counterparty_seller_signee_autorized_by' => 
      array (
        'label' => 'Counterparty seller signee autorized by',
      ),
      'counterparty_buyer_full_legal_company_name' => 
      array (
        'label' => 'Counterparty buyer full legal company name',
      ),
      'counterparty_buyer_registered_address' => 
      array (
        'label' => 'Counterparty buyer registered address',
      ),
      'counterparty_buyer_regulatory_body' => 
      array (
        'label' => 'Counterparty buyer regulatory body',
      ),
      'counterparty_buyer_physical_addr' => 
      array (
        'label' => 'Counterparty buyer physical addr',
      ),
      'counterparty_buyer_phone' => 
      array (
        'label' => 'Counterparty buyer phone',
      ),
      'counterparty_buyer_fax' => 
      array (
        'label' => 'Counterparty buyer fax',
      ),
      'counterparty_buyer_email' => 
      array (
        'label' => 'Counterparty buyer email',
      ),
      'counterparty_buyer_establishment_date' => 
      array (
        'label' => 'Counterparty buyer establishment date',
      ),
      'counterparty_buyer_business_line' => 
      array (
        'label' => 'Counterparty buyer business line',
      ),
      'counterparty_buyer_contact_name' => 
      array (
        'label' => 'Counterparty buyer contact name',
      ),
      'counterparty_buyer_contact_phone' => 
      array (
        'label' => 'Counterparty buyer contact phone',
      ),
      'counterparty_buyer_contact_fax' => 
      array (
        'label' => 'Counterparty buyer contact fax',
      ),
      'counterparty_buyer_contact_email' => 
      array (
        'label' => 'Counterparty buyer contact email',
      ),
      'counterparty_buyer_signee_name' => 
      array (
        'label' => 'Counterparty buyer signee name',
      ),
      'counterparty_buyer_signee_phone' => 
      array (
        'label' => 'Counterparty buyer signee phone',
      ),
      'counterparty_buyer_signee_fax' => 
      array (
        'label' => 'Counterparty buyer signee fax',
      ),
      'counterparty_buyer_signee_email' => 
      array (
        'label' => 'Counterparty buyer signee email',
      ),
      'counterparty_buyer_signee_autorized_by' => 
      array (
        'label' => 'Counterparty buyer signee autorized by',
      ),
      'expenses' => 
      array (
        'label' => 'EXPENSES',
      ),
      'target_date' => 
      array (
        'label' => 'Target Date',
      ),
      'target_sum' => 
      array (
        'label' => 'Target Sum',
      ),
      'rates' => 
      array (
        'label' => 'RATES',
      ),
      'modulerelation_productdeal' => 
      array (
      ),
      'modulerelation_subproductdeal' => 
      array (
      ),
      'modulerelation_dealincotermsbuy' => 
      array (
      ),
      'modulerelation_dealincotermssell' => 
      array (
      ),
      'modulerelation_dealcounterparty' => 
      array (
      ),
      'modulerelation_dealcounterpartysell' => 
      array (
      ),
      'modulerelation_dealcontactsell' => 
      array (
      ),
      'modulerelation_dealcontactsignee' => 
      array (
      ),
      'modulerelation_dealcontactsellsignee' => 
      array (
      ),
      'modulerelation_dealuserprojectmanager' => 
      array (
      ),
      'modulerelation_dealusertrader' => 
      array (
      ),
      'modulerelation_dealuserlegalsupport' => 
      array (
      ),
      'modulerelation_dealusertaxandaccsupport' => 
      array (
      ),
      'modulerelation_dealcounterpartyfinpart' => 
      array (
      ),
    ),
  ),
);
