<?php
return array (
  'model' => 
  array (
    'name' => 
    array (
      'label' => 'Name',
      'type' => 'text',
    ),
    'identifier' => 
    array (
      'label' => 'Identifier',
      'type' => 'text',
      'required' => true
    ),
    'create' => 
    array (
      'label' => 'CREATE',
      'type' => 'text',
    ),
    'id' => 
    array (
      'label' => 'ID',
      'type' => 'hidden',
    ),
    'active' => 
    array (
      'label' => 'ACTIVE',
      'type' => 'checkbox',
    ),
    'archived' => 
    array (
      'label' => 'ARCHIVED',
      'type' => 'text',
    ),
    'extend' => 
    array (
      'label' => 'EXTEND',
      'type' => 'text',
    ),
  ),
);
