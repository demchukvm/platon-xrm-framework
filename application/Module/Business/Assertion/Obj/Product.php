<?
namespace Application\Module\Business\Assertion\Obj;
class Product implements \Zend_Acl_Assert_Interface {

    /**
     * Returns true if and only if the assertion conditions are met
     *
     * This method is passed the ACL, Role, Resource, and privilege to which the authorization query applies. If the
     * $role, $resource, or $privilege parameters are null, it means that the query applies to all Roles, Resources, or
     * privileges, respectively.
     *
     * @param  Zend_Acl                    $acl
     * @param  Zend_Acl_Role_Interface     $role
     * @param  Zend_Acl_Resource_Interface $resource
     * @param  string                      $privilege
     * @return boolean
     */

    public function assert(\Zend_Acl $acl, \Zend_Acl_Role_Interface $role = null, \Zend_Acl_Resource_Interface $resource = null, $privilege = null) {
               
           $context = \Sl_Service_Acl::getContext();
           $Obj = $context->getContext();
           
           if ($Obj instanceof \Application\Module\Business\Model\Product) {
            if(($privilege == \Sl_Service_Acl::PRIVELEGE_UPDATE /*|| $privilege == \Sl_Service_Acl::PRIVELEGE_READ*/) && 
               $Obj->getId()) {
                    $resource_data = \Sl_Service_Acl::splitResourceName($resource);
                    $resource_type = $resource_data['type'];
                    if ($resource_type == \Sl_Service_Acl::RES_TYPE_OBJ && $resource_data['field'] == 'departmentproduct' ){
                        return false;
                    }   
                       
               }
           }
           
           return true;
    }

}
