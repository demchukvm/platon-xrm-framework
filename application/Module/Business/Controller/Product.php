<?php

namespace Application\Module\Business\Controller;

class Product extends \Sl_Controller_Model_Action {

    public function detailedAction() {

        parent::detailedAction();


        $Obj = \Sl_Model_Factory::mapper($this->getModelName(), $this->_getModule())->findExtended($this->getRequest()->getParam('id', 0), array('maintagproduct'));

        $this->view->object_tag_id = ($Obj->fetchOneRelated('maintagproduct') instanceof \Sl_Model_Abstract ? $Obj->fetchOneRelated('maintagproduct')->getId() : null);

        $this->view->object = $Obj;
    }

    public function editAction() {
        $this->_forward('detailed');
    }

}