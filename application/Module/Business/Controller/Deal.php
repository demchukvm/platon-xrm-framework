<?php

namespace Application\Module\Business\Controller;

class Deal extends \Sl_Controller_Model_Action {

    public function ajaxcreateAction() {
        parent::ajaxcreateAction();

        $resource_edit = \Sl_Service_Acl::joinResourceName(array(
                    'type' => \Sl_Service_Acl::RES_TYPE_MVC,
                    'module' => 'business',
                    'controller' => 'deal',
                    'action' => \Sl\Service\Helper::TO_EDIT_ACTION,
                ));

        if (\Sl_Service_Acl::isAllowed($resource_edit, \Sl_Service_Acl::PRIVELEGE_ACCESS)) {

            $model = \Sl_Model_Factory::_mapper($this->getModelName(), $this->_getModule())->find($this->view->result['id']);

            $edit_url = \Sl\Service\Helper::buildModelUrl($model, \Sl\Service\Helper::TO_EDIT_ACTION);
            $this->view->forvard_url = $edit_url;
        }
    }

    public function detailedAction() {
        $this->_helper->viewRenderer->setRender('edit');

        $Obj = \Sl_Model_Factory::mapper($this->getModelName(), $this->_getModule())->findExtended($this->getRequest()->getParam('id', 0), array('productdeal', 'maintagdeal'));
        \Sl_Event_Manager::trigger(new \Sl\Event\Modelaction('before', array(
            'model' => $Obj,
            'view' => $this->view,
            'request' => $this->getRequest(),
        )));
        $this->view->object_tag_id = ($Obj->fetchOneRelated('maintagdeal') instanceof \Sl_Model_Abstract ? $Obj->fetchOneRelated('maintagdeal')->getId() : null);

        $this->view->product = ($Obj->fetchOneRelated('productdeal') instanceof \Sl_Model_Abstract ? $Obj->fetchOneRelated('productdeal') : null);

        $this->view->object = $Obj;
        \Sl_Event_Manager::trigger(new \Sl\Event\Modelaction('after', array(
            'model' => $Obj,
            'view' => $this->view,
            'request' => $this->getRequest(),
        )));
    }

    public function editAction() {
        $this->_forward('detailed');
    }

    public function ajaxeditAction() {
        parent::ajaxeditAction();
        if ($this->view->result && $this->getRequest()->isPost() && $this->getRequest()->getParam('ajax_update', false) > 0 && $this->getRequest()->getParam('id', false)) {
            $this->view->e1 = '1';
            try {
                $Obj = \Sl_Model_Factory::mapper($this->getModelName(), $this->_getModule())->findAllowExtended($this->getRequest()->getParam('id', 0));
                $Obj->setOptions($this->getRequest()->getParams());

                \Sl_Model_Factory::mapper($Obj)->save($Obj);
                
            } catch (\Exception $e) {

                $this->view->result = false;
                $this->view->description = $e->getMessage();
                $this->view->trace = $e->getTrace();
            }
        }
    }

}