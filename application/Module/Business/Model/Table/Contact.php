<?php
namespace Application\Module\Business\Model\Table;

class Contact extends \Sl\Model\DbTable\DbTable {
	protected $_name = 'business_contact';
	protected $_primary = 'id';

}