<?php
namespace Application\Module\Business\Model\Table;

class Lead extends \Sl\Model\DbTable\DbTable {
	protected $_name = 'business_lead';
	protected $_primary = 'id';

}