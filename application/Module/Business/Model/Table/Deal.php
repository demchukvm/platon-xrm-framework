<?php

namespace Application\Module\Business\Model\Table;

class Deal extends \Sl\Model\DbTable\DbTable {

    protected $_name = 'business_deal';
    protected $_primary = 'id';


    public function getMaxIdentifierByCode($department_code) {
        $select = $this->select()->from(
                        $this->_name, array('max' => new \Zend_Db_Expr('IFNULL( MAX( CONVERT( SUBSTRING(  `identifier` , 9 ) , UNSIGNED INTEGER ) ) , -1 )')
                ))
                ->where('identifier like "' . $department_code . '%"');

        return $this->getAdapter()->fetchOne($select, array(), \Zend_Db::FETCH_ASSOC);
    }

}
