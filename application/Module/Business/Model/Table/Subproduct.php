<?php
namespace Application\Module\Business\Model\Table;

class Subproduct extends \Sl\Model\DbTable\DbTable {
	protected $_name = 'business_subproduct';
	protected $_primary = 'id';

         public function getMaxIdentifierByProdIdent ($prod_identifier){
            $select = $this->select()->from(
                                    $this->_name, array('max'=> new \Zend_Db_Expr('IFNULL( MAX( CONVERT( SUBSTRING(  `identifier` , 6 ) , UNSIGNED INTEGER ) ) , 0 )')
                                                            
                                             ))
                    ->where('identifier like "'.$prod_identifier.'%"');
            
        return $this->getAdapter()->fetchOne($select, array(), \Zend_Db::FETCH_ASSOC);
            
            
/*
        SELECT IFNULL( MAX( CONVERT( SUBSTRING(  `identifier` , 3 ) , UNSIGNEDINTEGER ) ) , 0 ) AS num
        FROM business_product
        WHERE identifier LIKE  'DE%'
        LIMIT 0 , 30
  * 
  * 
  * 
  */
        }
        
}