<?php
namespace Application\Module\Business\Model\Table;

class Counterparty extends \Sl\Model\DbTable\DbTable {
	protected $_name = 'business_counterparty';
	protected $_primary = 'id';

}