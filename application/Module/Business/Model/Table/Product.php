<?php
namespace Application\Module\Business\Model\Table;

class Product extends \Sl\Model\DbTable\DbTable {
	protected $_name = 'business_product';
	protected $_primary = 'id';
        
        public function getMaxIdentifierByDepCode ( $department_code){
            $select = $this->select()->from(
                                    $this->_name, array('max'=> new \Zend_Db_Expr('IFNULL( MAX( CONVERT( SUBSTRING(  `identifier` , 3 ) , UNSIGNED INTEGER ) ) , 0 )')
                                                         
                                             ))
                    ->where('identifier like "'.$department_code.'%"');
            
        return $this->getAdapter()->fetchOne($select, array(), \Zend_Db::FETCH_ASSOC);
            
            
/*
        SELECT IFNULL( MAX( CONVERT( SUBSTRING(  `identifier` , 3 ) , UNSIGNEDINTEGER ) ) , 0 ) AS num
        FROM business_product
        WHERE identifier LIKE  'DE%'
        LIMIT 0 , 30
  * 
  * 
  * 
  */
        }
        
}