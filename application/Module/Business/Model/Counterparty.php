<?php

namespace Application\Module\Business\Model;

class Counterparty extends \Sl_Model_Abstract {

    protected $_full_legal_company_name;
    protected $_name;
    protected $_registered_addr;
    protected $_regulatory_body;
    protected $_physical_addr;
    protected $_phone;
    protected $_fax;
    protected $_email;
    protected $_establishment_date;
    protected $_business_line;

    public function setName($name) {
        $this->_name = $name;
        return $this;
    }

    public function setFullLegalCompanyName($full_legal_company_name) {
        $this->_full_legal_company_name = $full_legal_company_name;
        return $this;
    }

    public function setRegisteredAddr($registered_addr) {
        $this->_registered_addr = $registered_addr;
        return $this;
    }

    public function setRegulatoryBody($regulatory_body) {
        $this->_regulatory_body = $regulatory_body;
        return $this;
    }

    public function setPhysicalAddr($physical_addr) {
        $this->_physical_addr = $physical_addr;
        return $this;
    }

    public function setPhone($phone) {
        $this->_phone = $phone;
        return $this;
    }

    public function setFax($fax) {
        $this->_fax = $fax;
        return $this;
    }

    public function setEmail($email) {
        $this->_email = $email;
        return $this;
    }

    public function setEstablishmentDate($establishment_date) {
        if($establishment_date instanceof \DateTime) {
            $establishment_date = $establishment_date->format(self::FORMAT_DATE);
        }
        $this->_establishment_date = $establishment_date;
        return $this;
    }

    public function setBusinessLine($business_line) {
        $this->_business_line = $business_line;
        return $this;
    }

    public function getFullLegalCompanyName() {
        return $this->_full_legal_company_name;
    }

    public function getRegisteredAddr() {
        return $this->_registered_addr;
    }

    public function getRegulatoryBody() {
        return $this->_regulatory_body;
    }

    public function getPhysicalAddr() {
        return $this->_physical_addr;
    }

    public function getPhone() {
        return $this->_phone;
    }

    public function getFax() {
        return $this->_fax;
    }

    public function getEmail() {
        return $this->_email;
    }

    public function getEstablishmentDate($as_object = false) {
        if($as_object) {
            return \DateTime::createFromFormat(self::FORMAT_DATE, $this->getEstablishmentDate());
        }
        return $this->_establishment_date;
    }

    public function getBusinessLine() {
        return $this->_business_line;
    }

    public function getName() {
        return $this->_name;
    }

}
