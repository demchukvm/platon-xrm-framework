<?php

namespace Application\Module\Business\Model;

class Product extends \Sl_Model_Abstract {

    protected $_name;
    protected $_target_date;
    protected $_target_sum;
    protected $_master_target;
    protected $_description;
    protected $_identifier;
    protected $_status;

    public function setTargetDate($target_date) {
        if ($target_date instanceof \DateTime) {
            $target_date = $target_date->format(self::FORMAT_DATE);
        }
        $this->_target_date = $target_date;
        return $this;
    }

    public function setTargetSum($target_sum) {
        $this->_target_sum = $target_sum;
        return $this;
    }

    public function setMasterTarget($master_target) {
        $this->_master_target = $master_target;
        return $this;
    }

    public function setName($name) {
        $this->_name = $name;
        return $this;
    }

    public function setDescription($description) {
        $this->_description = $description;
        return $this;
    }

    public function setIdentifier($identifier) {
        $this->_identifier = $identifier;
        return $this;
    }

    public function setStatus($status) {
        $this->_status = $status;
        return $this;
    }

    public function getName() {
        return $this->_name;
    }

    public function getDescription() {
        return $this->_description;
    }

    public function getIdentifier() {
        return $this->_identifier;
    }

    public function getStatus() {
        return $this->_status;
    }

    public function getMasterTarget() {
        return $this->_master_target;
    }

    public function __toString() {

        $strs = array();
        if ($this->_identifier) {
            $strs[] = $this->_identifier;
        }
        $strs[] = parent::__toString();
        return implode(' ', $strs);
    }

    public function getTargetDate($as_object = false) {
        if ($as_object) {
            return \DateTime::createFromFormat(self::FORMAT_DATE, $this->getTargetDate());
        }
        return $this->_target_date;
    }

    public function getTargetSum() {
        return $this->_target_sum;
    }

}
