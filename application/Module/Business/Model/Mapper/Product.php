<?php
namespace Application\Module\Business\Model\Mapper;

class Product extends \Sl_Model_Mapper_Abstract {
            protected function _getMappedDomainName() {
            return '\Application\Module\Business\Model\Product';
        }

        protected function _getMappedRealName() {
            return '\Application\Module\Business\Model\Table\Product';
        }
        protected $_custom_mandatory_fields = array('productfile');
        
        public function buildIdentifier (\Application\Module\Business\Model\Product $model){
            $relation = \Sl_Modulerelation_Manager::getRelations($model, 'departmentproduct');
            if ($relation instanceof \Sl\Modulerelation\Modulerelation){
                
                
                !$model->issetRelated($relation->getName()) && $model = $this->findRelation($model, $relation);
                $department = $model ->fetchOneRelated($relation->getName());
                $department =  ($department instanceof \Sl_Model_Abstract)?$department:\Sl_Model_Factory::mapper($relation->getRelatedObject($model))->find($department);
                
                if (strlen($department->getCode())){
                    !$department->issetRelated('pipedepartment') && $department =  \Sl_Model_Factory::mapper($department)->findRelation($department, 'pipedepartment');
                    $pipe = $department->fetchOneRelated('pipedepartment');
                    if ($pipe  instanceof \Sl_Model_Abstract){
                        $dep_code = $pipe->getIdentifier().$department->getCode();
                        $max_code = $this->_getDbTable() -> getMaxIdentifierByDepCode($dep_code);    
                        //die( sprintf('%s%03d',$department->getCode(), 999999));
                        return sprintf('%s%03d',$dep_code, ++$max_code) ;
                    }        
                    
                }
                
            }
            
        }
        
}