<?php
namespace Application\Module\Business\Model\Mapper;

class Incoterms extends \Sl_Model_Mapper_Abstract {
            protected function _getMappedDomainName() {
            return '\Application\Module\Business\Model\Incoterms';
        }

        protected function _getMappedRealName() {
            return '\Application\Module\Business\Model\Table\Incoterms';
        }
}