<?php
namespace Application\Module\Business\Model\Mapper;

class Pipe extends \Sl_Model_Mapper_Abstract {
            protected function _getMappedDomainName() {
            return '\Application\Module\Business\Model\Pipe';
        }

        protected function _getMappedRealName() {
            return '\Application\Module\Business\Model\Table\Pipe';
        }
}