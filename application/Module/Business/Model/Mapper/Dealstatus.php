<?php
namespace Application\Module\Business\Model\Mapper;

class Dealstatus extends \Sl_Model_Mapper_Abstract {
            protected function _getMappedDomainName() {
            return '\Application\Module\Business\Model\Dealstatus';
        }

        protected function _getMappedRealName() {
            return '\Application\Module\Business\Model\Table\Dealstatus';
        }
}