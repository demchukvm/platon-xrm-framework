<?php

namespace Application\Module\Business\Model\Mapper;

class Counterparty extends \Sl_Model_Mapper_Abstract {

    protected function _getMappedDomainName() {
        return '\Application\Module\Business\Model\Counterparty';
    }

    protected function _getMappedRealName() {
        return '\Application\Module\Business\Model\Table\Counterparty';
    }
    
    public function getRequiredRelations() {
        return array(
            'counterpartycontact',
        );
    }
}
