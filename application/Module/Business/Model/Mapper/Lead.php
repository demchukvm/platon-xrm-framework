<?php
namespace Application\Module\Business\Model\Mapper;

class Lead extends \Sl_Model_Mapper_Abstract {
            protected function _getMappedDomainName() {
            return '\Application\Module\Business\Model\Lead';
        }

        protected function _getMappedRealName() {
            return '\Application\Module\Business\Model\Table\Lead';
        }
}