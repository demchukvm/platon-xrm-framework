<?php
namespace Application\Module\Business\Model\Mapper;

class Deal extends \Sl_Model_Mapper_Abstract {

    protected function _getMappedDomainName() {
        return '\Application\Module\Business\Model\Deal';
    }

    protected function _getMappedRealName() {
        return '\Application\Module\Business\Model\Table\Deal';
    }
    
    protected $_custom_mandatory_fields = array('dealfile');
    
    public function buildIdentifier(\Application\Module\Business\Model\Deal $model) {
        $relation = \Sl_Modulerelation_Manager::getRelations($model, 'subproductdeal');
        if ($relation instanceof \Sl\Modulerelation\Modulerelation) {
            !$model->issetRelated($relation->getName()) && $model = $this->findRelation($model, $relation);
            $subproduct = $model->fetchOneRelated($relation->getName());
            $subproduct = ($subproduct instanceof \Sl_Model_Abstract) ? $subproduct : \Sl_Model_Factory::mapper($relation->getRelatedObject($model))->find($subproduct);
            if ($subproduct instanceof \Sl_Model_Abstract && strlen($subproduct->getIdentifier())) {
                $max_code =  $this->_getDbTable()->getMaxIdentifierByCode($subproduct->getIdentifier()); 
                return (sprintf('%s%0'.max(5,strlen(''.(++$max_code))).'d', $subproduct->getIdentifier(), $max_code));
            }
        }
    }

}