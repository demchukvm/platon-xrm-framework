<?php

namespace Application\Module\Business\Model\Mapper;

class Subproduct extends \Sl_Model_Mapper_Abstract {
    
   protected $_custom_mandatory_fields = array('identifier', 'name',
        'modulerelation_productsubproduct');
   
    protected function _getMappedDomainName() {
        return '\Application\Module\Business\Model\Subproduct';
    }

    protected function _getMappedRealName() {
        return '\Application\Module\Business\Model\Table\Subproduct';
    }

    public function buildIdentifier(\Application\Module\Business\Model\Subproduct $model) {
        $relation = \Sl_Modulerelation_Manager::getRelations($model, 'productsubproduct');
        if ($relation instanceof \Sl\Modulerelation\Modulerelation) {
        
           
            !$model->issetRelated($relation->getName()) && $model = $this->findRelation($model, $relation);
            $product = $model->fetchOneRelated($relation->getName());
            $product = ($product instanceof \Sl_Model_Abstract) ? $product : \Sl_Model_Factory::mapper($relation->getRelatedObject($model))->find($product);
            if (strlen($product->getIdentifier())) {
                $max_code = $this->_getDbTable()->getMaxIdentifierByProdIdent($product->getIdentifier()); 
             //   die( sprintf('%s%02d',$product->getIdentifier(), ++$max_code));
           //  var_dump($max_code);             var_dump($product->getIdentifier())
                return sprintf('%s%02d', $product->getIdentifier(), ++$max_code);
            }
        }
    }

}