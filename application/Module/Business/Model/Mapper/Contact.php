<?php

namespace Application\Module\Business\Model\Mapper;

class Contact extends \Sl_Model_Mapper_Abstract {
    
    protected $_custom_mandatory_fields = array('main');
    
    protected function _getMappedDomainName() {
        return '\Application\Module\Business\Model\Contact';
    }

    protected function _getMappedRealName() {
        return '\Application\Module\Business\Model\Table\Contact';
    }

}
