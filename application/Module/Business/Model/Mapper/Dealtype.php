<?php
namespace Application\Module\Business\Model\Mapper;

class Dealtype extends \Sl_Model_Mapper_Abstract {
            protected function _getMappedDomainName() {
            return '\Application\Module\Business\Model\Dealtype';
        }

        protected function _getMappedRealName() {
            return '\Application\Module\Business\Model\Table\Dealtype';
        }
}