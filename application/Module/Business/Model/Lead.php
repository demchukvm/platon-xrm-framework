<?php
namespace Application\Module\Business\Model;

class Lead extends \Sl_Model_Abstract {

	protected $_name; 
protected $_master_target;


	protected $_description;
	protected $_identifier;
	protected $_status;
	
	

	

	public function setMasterTarget ($master_target) {
		$this->_master_target = $master_target;
		return $this;
	}

	public function setName ($name) {
		$this->_name = $name;
		return $this;
	}
	public function setDescription ($description) {
		$this->_description = $description;
		return $this;
	}
	public function setIdentifier ($identifier) {
		$this->_identifier = $identifier;
		return $this;
	}
	public function setStatus ($status) {
		$this->_status = $status;
		return $this;
	}
	

	public function getName () {
		return $this->_name;
	}
	public function getDescription () {
		return $this->_description;
	}
	public function getIdentifier () {
		return $this->_identifier;
	}
	public function getStatus () {
		return $this->_status;
	}
	
	



	public function getMasterTarget () {
		return $this->_master_target;
	}
}