<?php

namespace Application\Module\Business\Model;

class Deal extends \Sl_Model_Abstract {

    protected $_name;
    protected $_rates;
    protected $_target_date;
    protected $_target_sum;
    protected $_counterparty_buyer_full_legal_company_name;
    protected $_counterparty_buyer_registered_address;
    protected $_counterparty_buyer_regulatory_body;
    protected $_counterparty_buyer_physical_addr;
    protected $_counterparty_buyer_phone;
    protected $_counterparty_buyer_fax;
    protected $_counterparty_buyer_email;
    protected $_counterparty_buyer_establishment_date;
    protected $_counterparty_buyer_business_line;
    protected $_counterparty_buyer_contact_name;
    protected $_counterparty_buyer_contact_phone;
    protected $_counterparty_buyer_contact_fax;
    protected $_counterparty_buyer_contact_email;
    protected $_counterparty_buyer_signee_name;
    protected $_counterparty_buyer_signee_phone;
    protected $_counterparty_buyer_signee_fax;
    protected $_counterparty_buyer_signee_email;
    protected $_counterparty_buyer_signee_autorized_by;
    protected $_counterparty_seller_full_legal_company_name;
    protected $_counterparty_seller_registered_address;
    protected $_counterparty_seller_regulatory_body;
    protected $_counterparty_seller_physical_addr;
    protected $_counterparty_seller_phone;
    protected $_counterparty_seller_fax;
    protected $_counterparty_seller_email;
    protected $_counterparty_seller_establishment_date;
    protected $_counterparty_seller_business_line;
    protected $_counterparty_seller_contact_name;
    protected $_counterparty_seller_contact_phone;
    protected $_counterparty_seller_contact_fax;
    protected $_counterparty_seller_contact_email;
    protected $_counterparty_seller_signee_name;
    protected $_counterparty_seller_signee_phone;
    protected $_counterparty_seller_signee_fax;
    protected $_counterparty_seller_signee_email;
    protected $_counterparty_seller_signee_autorized_by;
    protected $_our_company_buy_name;
    protected $_our_company_buy_registerede_addr;
    protected $_our_company_buy_physical_addr;
    protected $_our_company_buy_phone;
    protected $_our_company_buy_fax;
    protected $_our_company_buy_email;
    protected $_our_company_sell_name;
    protected $_our_company_sell_registerede_addr;
    protected $_our_company_sell_physical_addr;
    protected $_our_company_sell_phone;
    protected $_our_company_sell_fax;
    protected $_our_company_sell_email;
    protected $_accounting_and_legal_fees;
    protected $_bank_service_charges;
    protected $_financing_costs;
    protected $_comissions;
    protected $_agency_fees;
    protected $_insurance;
    protected $_interest;
    protected $_transportation_logistics;
    protected $_travel;
    protected $_services;
    protected $_inventory_purchase;
    protected $_sales_and_marketing;
    protected $_expenses;
    protected $_research_and_development;
    protected $_general_administrative;
    protected $_miscellaneous;
    protected $_deprecation_and_amortization;
    protected $_taxes;
    protected $_net_profit;
    protected $_cost_of_goods_sold;
    protected $_gross_profit;
    protected $_payment_schedule_seller;
    protected $_total_amount_to_be_paid;
    protected $_risk_exposure_seller;
    protected $_payment_schedule_buyer;
    protected $_total_amount_to_be_recieved;
    protected $_risk_exposure_buyer;
    protected $_type_of_transportation;
    protected $_delivery_schedule;
    protected $_quantity;
    protected $_quality;
    protected $_buy_from;
    protected $_sell_to;
    protected $_our_role;
    protected $_deal_type;
    protected $_shipping_from;
    protected $_shipping_date;
    protected $_delivery_destination;
    protected $_delivery_date;
    protected $_master_target;
    protected $_description;
    protected $_identifier;
    protected $_status;
    protected $_lists = array(
        'status' => 'business_dealstatus',
        'deal_type' => 'business_dealtype',
        'type_of_transportation' => 'business_dealtypetransportation'
    );

    public function setRates($rates) {
        if(is_array($rates)) {
            $rates = json_encode($rates);
        }
        $this->_rates = $rates;
        return $this;
    }

    public function setTargetDate($target_date) {
        if($target_date instanceof \DateTime) {
            $target_date = $target_date->format(self::FORMAT_DATE);
        }
        $this->_target_date = $target_date;
        return $this;
    }

    public function setTargetSum($target_sum) {
        $this->_target_sum = $target_sum;
        return $this;
    }

    public function setCounterpartyBuyerFullLegalCompanyName($counterparty_buyer_full_legal_company_name) {
        $this->_counterparty_buyer_full_legal_company_name = $counterparty_buyer_full_legal_company_name;
        return $this;
    }

    public function setCounterpartyBuyerRegisteredAddress($counterparty_buyer_registered_address) {
        $this->_counterparty_buyer_registered_address = $counterparty_buyer_registered_address;
        return $this;
    }

    public function setCounterpartyBuyerRegulatoryBody($counterparty_buyer_regulatory_body) {
        $this->_counterparty_buyer_regulatory_body = $counterparty_buyer_regulatory_body;
        return $this;
    }

    public function setCounterpartyBuyerPhysicalAddr($counterparty_buyer_physical_addr) {
        $this->_counterparty_buyer_physical_addr = $counterparty_buyer_physical_addr;
        return $this;
    }

    public function setCounterpartyBuyerPhone($counterparty_buyer_phone) {
        $this->_counterparty_buyer_phone = $counterparty_buyer_phone;
        return $this;
    }

    public function setCounterpartyBuyerFax($counterparty_buyer_fax) {
        $this->_counterparty_buyer_fax = $counterparty_buyer_fax;
        return $this;
    }

    public function setCounterpartyBuyerEmail($counterparty_buyer_email) {
        $this->_counterparty_buyer_email = $counterparty_buyer_email;
        return $this;
    }

    public function setCounterpartyBuyerEstablishmentDate($counterparty_buyer_establishment_date) {
        $this->_counterparty_buyer_establishment_date = $counterparty_buyer_establishment_date;
        return $this;
    }

    public function setCounterpartyBuyerBusinessLine($counterparty_buyer_business_line) {
        $this->_counterparty_buyer_business_line = $counterparty_buyer_business_line;
        return $this;
    }

    public function setCounterpartyBuyerContactName($counterparty_buyer_contact_name) {
        $this->_counterparty_buyer_contact_name = $counterparty_buyer_contact_name;
        return $this;
    }

    public function setCounterpartyBuyerContactPhone($counterparty_buyer_contact_phone) {
        $this->_counterparty_buyer_contact_phone = $counterparty_buyer_contact_phone;
        return $this;
    }

    public function setCounterpartyBuyerContactFax($counterparty_buyer_contact_fax) {
        $this->_counterparty_buyer_contact_fax = $counterparty_buyer_contact_fax;
        return $this;
    }

    public function setCounterpartyBuyerContactEmail($counterparty_buyer_contact_email) {
        $this->_counterparty_buyer_contact_email = $counterparty_buyer_contact_email;
        return $this;
    }

    public function setCounterpartyBuyerSigneeName($counterparty_buyer_signee_name) {
        $this->_counterparty_buyer_signee_name = $counterparty_buyer_signee_name;
        return $this;
    }

    public function setCounterpartyBuyerSigneePhone($counterparty_buyer_signee_phone) {
        $this->_counterparty_buyer_signee_phone = $counterparty_buyer_signee_phone;
        return $this;
    }

    public function setCounterpartyBuyerSigneeFax($counterparty_buyer_signee_fax) {
        $this->_counterparty_buyer_signee_fax = $counterparty_buyer_signee_fax;
        return $this;
    }

    public function setCounterpartyBuyerSigneeEmail($counterparty_buyer_signee_email) {
        $this->_counterparty_buyer_signee_email = $counterparty_buyer_signee_email;
        return $this;
    }

    public function setCounterpartyBuyerSigneeAutorizedBy($counterparty_buyer_signee_autorized_by) {
        $this->_counterparty_buyer_signee_autorized_by = $counterparty_buyer_signee_autorized_by;
        return $this;
    }

    public function setCounterpartySellerFullLegalCompanyName($counterparty_seller_full_legal_company_name) {
        $this->_counterparty_seller_full_legal_company_name = $counterparty_seller_full_legal_company_name;
        return $this;
    }

    public function setCounterpartySellerRegisteredAddress($counterparty_seller_registered_address) {
        $this->_counterparty_seller_registered_address = $counterparty_seller_registered_address;
        return $this;
    }

    public function setCounterpartySellerRegulatoryBody($counterparty_seller_regulatory_body) {
        $this->_counterparty_seller_regulatory_body = $counterparty_seller_regulatory_body;
        return $this;
    }

    public function setCounterpartySellerPhysicalAddr($counterparty_seller_physical_addr) {
        $this->_counterparty_seller_physical_addr = $counterparty_seller_physical_addr;
        return $this;
    }

    public function setCounterpartySellerPhone($counterparty_seller_phone) {
        $this->_counterparty_seller_phone = $counterparty_seller_phone;
        return $this;
    }

    public function setCounterpartySellerFax($counterparty_seller_fax) {
        $this->_counterparty_seller_fax = $counterparty_seller_fax;
        return $this;
    }

    public function setCounterpartySellerEmail($counterparty_seller_email) {
        $this->_counterparty_seller_email = $counterparty_seller_email;
        return $this;
    }

    public function setCounterpartySellerEstablishmentDate($counterparty_seller_establishment_date) {
        if($counterparty_seller_establishment_date instanceof \DateTime) {
            $counterparty_seller_establishment_date = $counterparty_seller_establishment_date->format(self::FORMAT_DATE);
        }
        $this->_counterparty_seller_establishment_date = $counterparty_seller_establishment_date;
        return $this;
    }

    public function setCounterpartySellerBusinessLine($counterparty_seller_business_line) {
        $this->_counterparty_seller_business_line = $counterparty_seller_business_line;
        return $this;
    }

    public function setCounterpartySellerContactName($counterparty_seller_contact_name) {
        $this->_counterparty_seller_contact_name = $counterparty_seller_contact_name;
        return $this;
    }

    public function setCounterpartySellerContactPhone($counterparty_seller_contact_phone) {
        $this->_counterparty_seller_contact_phone = $counterparty_seller_contact_phone;
        return $this;
    }

    public function setCounterpartySellerContactFax($counterparty_seller_contact_fax) {
        $this->_counterparty_seller_contact_fax = $counterparty_seller_contact_fax;
        return $this;
    }

    public function setCounterpartySellerContactEmail($counterparty_seller_contact_email) {
        $this->_counterparty_seller_contact_email = $counterparty_seller_contact_email;
        return $this;
    }

    public function setCounterpartySellerSigneeName($counterparty_seller_signee_name) {
        $this->_counterparty_seller_signee_name = $counterparty_seller_signee_name;
        return $this;
    }

    public function setCounterpartySellerSigneePhone($counterparty_seller_signee_phone) {
        $this->_counterparty_seller_signee_phone = $counterparty_seller_signee_phone;
        return $this;
    }

    public function setCounterpartySellerSigneeFax($counterparty_seller_signee_fax) {
        $this->_counterparty_seller_signee_fax = $counterparty_seller_signee_fax;
        return $this;
    }

    public function setCounterpartySellerSigneeEmail($counterparty_seller_signee_email) {
        $this->_counterparty_seller_signee_email = $counterparty_seller_signee_email;
        return $this;
    }

    public function setCounterpartySellerSigneeAutorizedBy($counterparty_seller_signee_autorized_by) {
        $this->_counterparty_seller_signee_autorized_by = $counterparty_seller_signee_autorized_by;
        return $this;
    }

    public function setOurCompanyBuyName($our_company_buy_name) {
        $this->_our_company_buy_name = $our_company_buy_name;
        return $this;
    }

    public function setOurCompanyBuyRegisteredeAddr($our_company_buy_registerede_addr) {
        $this->_our_company_buy_registerede_addr = $our_company_buy_registerede_addr;
        return $this;
    }

    public function setOurCompanyBuyPhysicalAddr($our_company_buy_physical_addr) {
        $this->_our_company_buy_physical_addr = $our_company_buy_physical_addr;
        return $this;
    }

    public function setOurCompanyBuyPhone($our_company_buy_phone) {
        $this->_our_company_buy_phone = $our_company_buy_phone;
        return $this;
    }

    public function setOurCompanyBuyFax($our_company_buy_fax) {
        $this->_our_company_buy_fax = $our_company_buy_fax;
        return $this;
    }

    public function setOurCompanyBuyEmail($our_company_buy_email) {
        $this->_our_company_buy_email = $our_company_buy_email;
        return $this;
    }

    public function setOurCompanySellName($our_company_sell_name) {
        $this->_our_company_sell_name = $our_company_sell_name;
        return $this;
    }

    public function setOurCompanySellRegisteredeAddr($our_company_sell_registerede_addr) {
        $this->_our_company_sell_registerede_addr = $our_company_sell_registerede_addr;
        return $this;
    }

    public function setOurCompanySellPhysicalAddr($our_company_sell_physical_addr) {
        $this->_our_company_sell_physical_addr = $our_company_sell_physical_addr;
        return $this;
    }

    public function setOurCompanySellPhone($our_company_sell_phone) {
        $this->_our_company_sell_phone = $our_company_sell_phone;
        return $this;
    }

    public function setOurCompanySellFax($our_company_sell_fax) {
        $this->_our_company_sell_fax = $our_company_sell_fax;
        return $this;
    }

    public function setOurCompanySellEmail($our_company_sell_email) {
        $this->_our_company_sell_email = $our_company_sell_email;
        return $this;
    }

    public function setAccountingAndLegalFees($accounting_and_legal_fees) {
        $this->_accounting_and_legal_fees = $accounting_and_legal_fees;
        return $this;
    }

    public function setBankServiceCharges($bank_service_charges) {
        $this->_bank_service_charges = $bank_service_charges;
        return $this;
    }

    public function setFinancingCosts($financing_costs) {
        $this->_financing_costs = $financing_costs;
        return $this;
    }

    public function setComissions($comissions) {
        $this->_comissions = $comissions;
        return $this;
    }

    public function setAgencyFees($agency_fees) {
        $this->_agency_fees = $agency_fees;
        return $this;
    }

    public function setInsurance($insurance) {
        $this->_insurance = $insurance;
        return $this;
    }

    public function setInterest($interest) {
        $this->_interest = $interest;
        return $this;
    }

    public function setTransportationLogistics($transportation_logistics) {
        $this->_transportation_logistics = $transportation_logistics;
        return $this;
    }

    public function setTravel($travel) {
        $this->_travel = $travel;
        return $this;
    }

    public function setServices($services) {
        $this->_services = $services;
        return $this;
    }

    public function setInventoryPurchase($inventory_purchase) {
        $this->_inventory_purchase = $inventory_purchase;
        return $this;
    }

    public function setSalesAndMarketing($sales_and_marketing) {
        $this->_sales_and_marketing = $sales_and_marketing;
        return $this;
    }

    public function setExpenses($expenses) {
        $this->_expenses = $expenses;
        return $this;
    }

    public function setResearchAndDevelopment($research_and_development) {
        $this->_research_and_development = $research_and_development;
        return $this;
    }

    public function setGeneralAdministrative($general_administrative) {
        $this->_general_administrative = $general_administrative;
        return $this;
    }

    public function setMiscellaneous($miscellaneous) {
        $this->_miscellaneous = $miscellaneous;
        return $this;
    }

    public function setDeprecationAndAmortization($deprecation_and_amortization) {
        $this->_deprecation_and_amortization = $deprecation_and_amortization;
        return $this;
    }

    public function setTaxes($taxes) {
        $this->_taxes = $taxes;
        return $this;
    }

    public function setNetProfit($net_profit) {
        $this->_net_profit = $net_profit;
        return $this;
    }

    public function setCostOfGoodsSold($cost_of_goods_sold) {
        $this->_cost_of_goods_sold = $cost_of_goods_sold;
        return $this;
    }

    public function setGrossProfit($gross_profit) {
        $this->_gross_profit = $gross_profit;
        return $this;
    }

    public function setPaymentScheduleSeller($payment_schedule_seller) {
        $this->_payment_schedule_seller = $payment_schedule_seller;
        return $this;
    }

    public function setTotalAmountToBePaid($total_amount_to_be_paid) {
        $this->_total_amount_to_be_paid = $total_amount_to_be_paid;
        return $this;
    }

    public function setRiskExposureSeller($risk_exposure_seller) {
        $this->_risk_exposure_seller = $risk_exposure_seller;
        return $this;
    }

    public function setPaymentScheduleBuyer($payment_schedule_buyer) {
        $this->_payment_schedule_buyer = $payment_schedule_buyer;
        return $this;
    }

    public function setTotalAmountToBeRecieved($total_amount_to_be_recieved) {
        $this->_total_amount_to_be_recieved = $total_amount_to_be_recieved;
        return $this;
    }

    public function setRiskExposureBuyer($risk_exposure_buyer) {
        $this->_risk_exposure_buyer = $risk_exposure_buyer;
        return $this;
    }

    public function setTypeOfTransportation($type_of_transportation) {
        $this->_type_of_transportation = $type_of_transportation;
        return $this;
    }

    public function setDeliverySchedule($delivery_schedule) {
        $this->_delivery_schedule = $delivery_schedule;
        return $this;
    }

    public function setQuantity($quantity) {
        $this->_quantity = $quantity;
        return $this;
    }

    public function setQuality($quality) {
        $this->_quality = $quality;
        return $this;
    }

    public function setBuyFrom($buy_from) {
        $this->_buy_from = $buy_from;
        return $this;
    }

    public function setSellTo($sell_to) {
        $this->_sell_to = $sell_to;
        return $this;
    }

    public function setOurRole($our_role) {
        $this->_our_role = $our_role;
        return $this;
    }

    public function setDealType($deal_type) {
        $this->_deal_type = $deal_type;
        return $this;
    }

    public function setShippingFrom($shipping_from) {
        $this->_shipping_from = $shipping_from;
        return $this;
    }

    public function setShippingDate($shipping_date) {
        if($shipping_date instanceof \DateTime) {
            $shipping_date = $shipping_date->format(self::FORMAT_DATE);
        }
        $this->_shipping_date = $shipping_date;
        return $this;
    }

    public function setDeliveryDestination($delivery_destination) {
        $this->_delivery_destination = $delivery_destination;
        return $this;
    }

    public function setDeliveryDate($delivery_date) {
        if($delivery_date instanceof \DateTime) {
            $delivery_date = $delivery_date->format(self::FORMAT_DATE);
        }
        $this->_delivery_date = $delivery_date;
        return $this;
    }

    public function setMasterTarget($master_target) {
        $this->_master_target = $master_target;
        return $this;
    }

    public function setName($name) {
        $this->_name = $name;
        return $this;
    }

    public function setDescription($description) {
        $this->_description = $description;
        return $this;
    }

    public function setIdentifier($identifier) {
        $this->_identifier = $identifier;
        return $this;
    }

    public function setStatus($status) {
        $this->_status = $status;
        return $this;
    }

    public function getName() {
        return $this->_name;
    }

    public function getDescription() {
        return $this->_description;
    }

    public function getIdentifier() {
        return $this->_identifier;
    }

    public function getStatus() {
        return $this->_status;
    }

    public function getMasterTarget() {
        return $this->_master_target;
    }

    public function __toString() {

        $strs = array();
        if($this->_identifier) {
            $strs[] = $this->_identifier;
        }
        $strs[] = parent::__toString();
        return implode(' ', $strs);
    }

    public function getQuantity() {
        return $this->_quantity;
    }

    public function getQuality() {
        return $this->_quality;
    }

    public function getBuyFrom() {
        return $this->_buy_from;
    }

    public function getSellTo() {
        return $this->_sell_to;
    }

    public function getOurRole() {
        return $this->_our_role;
    }

    public function getDealType() {
        return $this->_deal_type;
    }

    public function getShippingFrom() {
        return $this->_shipping_from;
    }

    public function getShippingDate($as_object = false) {
        if($as_object) {
            return \DateTime::createFromFormat(self::FORMAT_DATE, $this->getShippingDate());
        }
        return $this->_shipping_date;
    }

    public function getDeliveryDestination() {
        return $this->_delivery_destination;
    }

    public function getDeliveryDate($as_object = false) {
        if($as_object) {
            return \DateTime::createFromFormat(self::FORMAT_DATE, $this->getDeliveryDate());
        }
        return $this->_delivery_date;
    }

    public function getPaymentScheduleSeller() {
        return $this->_payment_schedule_seller;
    }

    public function getTotalAmountToBePaid() {
        return $this->_total_amount_to_be_paid;
    }

    public function getRiskExposureSeller() {
        return $this->_risk_exposure_seller;
    }

    public function getPaymentScheduleBuyer() {
        return $this->_payment_schedule_buyer;
    }

    public function getTotalAmountToBeRecieved() {
        return $this->_total_amount_to_be_recieved;
    }

    public function getRiskExposureBuyer() {
        return $this->_risk_exposure_buyer;
    }

    public function getTypeOfTransportation() {
        return $this->_type_of_transportation;
    }

    public function getDeliverySchedule() {
        return $this->_delivery_schedule;
    }

    public function getCostOfGoodsSold() {
        return $this->_cost_of_goods_sold;
    }

    public function getGrossProfit() {
        return $this->_gross_profit;
    }

    public function getAccountingAndLegalFees() {
        return $this->_accounting_and_legal_fees;
    }

    public function getBankServiceCharges() {
        return $this->_bank_service_charges;
    }

    public function getFinancingCosts() {
        return $this->_financing_costs;
    }

    public function getComissions() {
        return $this->_comissions;
    }

    public function getAgencyFees() {
        return $this->_agency_fees;
    }

    public function getInsurance() {
        return $this->_insurance;
    }

    public function getInterest() {
        return $this->_interest;
    }

    public function getTransportationLogistics() {
        return $this->_transportation_logistics;
    }

    public function getTravel() {
        return $this->_travel;
    }

    public function getServices() {
        return $this->_services;
    }

    public function getInventoryPurchase() {
        return $this->_inventory_purchase;
    }

    public function getSalesAndMarketing() {
        return $this->_sales_and_marketing;
    }

    public function getExpenses() {
        return $this->_expenses;
    }

    public function getResearchAndDevelopment() {
        return $this->_research_and_development;
    }

    public function getGeneralAdministrative() {
        return $this->_general_administrative;
    }

    public function getMiscellaneous() {
        return $this->_miscellaneous;
    }

    public function getDeprecationAndAmortization() {
        return $this->_deprecation_and_amortization;
    }

    public function getTaxes() {
        return $this->_taxes;
    }

    public function getNetProfit() {
        return $this->_net_profit;
    }

    public function getOurCompanyBuyName() {
        return $this->_our_company_buy_name;
    }

    public function getOurCompanyBuyRegisteredeAddr() {
        return $this->_our_company_buy_registerede_addr;
    }

    public function getOurCompanyBuyPhysicalAddr() {
        return $this->_our_company_buy_physical_addr;
    }

    public function getOurCompanyBuyPhone() {
        return $this->_our_company_buy_phone;
    }

    public function getOurCompanyBuyFax() {
        return $this->_our_company_buy_fax;
    }

    public function getOurCompanyBuyEmail() {
        return $this->_our_company_buy_email;
    }

    public function getOurCompanySellName() {
        return $this->_our_company_sell_name;
    }

    public function getOurCompanySellRegisteredeAddr() {
        return $this->_our_company_sell_registerede_addr;
    }

    public function getOurCompanySellPhysicalAddr() {
        return $this->_our_company_sell_physical_addr;
    }

    public function getOurCompanySellPhone() {
        return $this->_our_company_sell_phone;
    }

    public function getOurCompanySellFax() {
        return $this->_our_company_sell_fax;
    }

    public function getOurCompanySellEmail() {
        return $this->_our_company_sell_email;
    }

    public function getCounterpartySellerFullLegalCompanyName() {
        return $this->_counterparty_seller_full_legal_company_name;
    }

    public function getCounterpartySellerRegisteredAddress() {
        return $this->_counterparty_seller_registered_address;
    }

    public function getCounterpartySellerRegulatoryBody() {
        return $this->_counterparty_seller_regulatory_body;
    }

    public function getCounterpartySellerPhysicalAddr() {
        return $this->_counterparty_seller_physical_addr;
    }

    public function getCounterpartySellerPhone() {
        return $this->_counterparty_seller_phone;
    }

    public function getCounterpartySellerFax() {
        return $this->_counterparty_seller_fax;
    }

    public function getCounterpartySellerEmail() {
        return $this->_counterparty_seller_email;
    }

    public function getCounterpartySellerEstablishmentDate($as_object = false) {
        if($as_object) {
            return \DateTime::createFromFormat(self::FORMAT_DATE, $this->getCounterpartySellerEstablishmentDate());
        }
        return $this->_counterparty_seller_establishment_date;
    }

    public function getCounterpartySellerBusinessLine() {
        return $this->_counterparty_seller_business_line;
    }

    public function getCounterpartySellerContactName() {
        return $this->_counterparty_seller_contact_name;
    }

    public function getCounterpartySellerContactPhone() {
        return $this->_counterparty_seller_contact_phone;
    }

    public function getCounterpartySellerContactFax() {
        return $this->_counterparty_seller_contact_fax;
    }

    public function getCounterpartySellerContactEmail() {
        return $this->_counterparty_seller_contact_email;
    }

    public function getCounterpartySellerSigneeName() {
        return $this->_counterparty_seller_signee_name;
    }

    public function getCounterpartySellerSigneePhone() {
        return $this->_counterparty_seller_signee_phone;
    }

    public function getCounterpartySellerSigneeFax() {
        return $this->_counterparty_seller_signee_fax;
    }

    public function getCounterpartySellerSigneeEmail() {
        return $this->_counterparty_seller_signee_email;
    }

    public function getCounterpartySellerSigneeAutorizedBy() {
        return $this->_counterparty_seller_signee_autorized_by;
    }

    public function getCounterpartyBuyerFullLegalCompanyName() {
        return $this->_counterparty_buyer_full_legal_company_name;
    }

    public function getCounterpartyBuyerRegisteredAddress() {
        return $this->_counterparty_buyer_registered_address;
    }

    public function getCounterpartyBuyerRegulatoryBody() {
        return $this->_counterparty_buyer_regulatory_body;
    }

    public function getCounterpartyBuyerPhysicalAddr() {
        return $this->_counterparty_buyer_physical_addr;
    }

    public function getCounterpartyBuyerPhone() {
        return $this->_counterparty_buyer_phone;
    }

    public function getCounterpartyBuyerFax() {
        return $this->_counterparty_buyer_fax;
    }

    public function getCounterpartyBuyerEmail() {
        return $this->_counterparty_buyer_email;
    }

    public function getCounterpartyBuyerEstablishmentDate() {
        return $this->_counterparty_buyer_establishment_date;
    }

    public function getCounterpartyBuyerBusinessLine() {
        return $this->_counterparty_buyer_business_line;
    }

    public function getCounterpartyBuyerContactName() {
        return $this->_counterparty_buyer_contact_name;
    }

    public function getCounterpartyBuyerContactPhone() {
        return $this->_counterparty_buyer_contact_phone;
    }

    public function getCounterpartyBuyerContactFax() {
        return $this->_counterparty_buyer_contact_fax;
    }

    public function getCounterpartyBuyerContactEmail() {
        return $this->_counterparty_buyer_contact_email;
    }

    public function getCounterpartyBuyerSigneeName() {
        return $this->_counterparty_buyer_signee_name;
    }

    public function getCounterpartyBuyerSigneePhone() {
        return $this->_counterparty_buyer_signee_phone;
    }

    public function getCounterpartyBuyerSigneeFax() {
        return $this->_counterparty_buyer_signee_fax;
    }

    public function getCounterpartyBuyerSigneeEmail() {
        return $this->_counterparty_buyer_signee_email;
    }

    public function getCounterpartyBuyerSigneeAutorizedBy() {
        return $this->_counterparty_buyer_signee_autorized_by;
    }

    public function getTargetDate($as_object = false) {
        if($as_object) {
            return \DateTime::createFromFormat(self::FORMAT_DATE, $this->getTargetDate());
        }
        return $this->_target_date;
    }

    public function getTargetSum() {
        return $this->_target_sum;
    }

    public function getRates($as_array = false) {
        if($as_array) {
            return json_decode($this->getRates(), true);
        }
        return $this->_rates;
    }

}
