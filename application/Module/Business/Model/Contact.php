<?php
namespace Application\Module\Business\Model;

class Contact extends \Sl_Model_Abstract {

	protected $_name; 
	protected $_phone;
	protected $_fax;
	protected $_email;
	protected $_loged = false;


	public function setName ($name) {
		$this->_name = $name;
		return $this;
	}
	public function setPhone ($phone) {
		$this->_phone = $phone;
		return $this;
	}
	public function setFax ($fax) {
		$this->_fax = $fax;
		return $this;
	}
	public function setEmail ($email) {
		$this->_email = $email;
		return $this;
	}

	public function getName () {
		return $this->_name;
	}
	public function getPhone () {
		return $this->_phone;
	}
	public function getFax () {
		return $this->_fax;
	}
	public function getEmail () {
		return $this->_email;
	}


}