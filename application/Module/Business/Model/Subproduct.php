<?php
namespace Application\Module\Business\Model;

class Subproduct extends \Sl_Model_Abstract {

	protected $_name;
	protected $_identifier;
	protected $_loged = false;

        
       public function __toString(){
            
            $strs= array();
            if ($this->_identifier) {$strs[]= $this->_identifier;}
            $strs[] = parent::__toString();
            return implode(' ',$strs);
        }
        
	public function setName ($name) {
		$this->_name = $name;
		return $this;
	}
	public function setIdentifier ($identifier) {
		$this->_identifier = $identifier;
		return $this;
	}

	public function getName () {
		return $this->_name;
	}
	public function getIdentifier () {
		return $this->_identifier;
	}



}