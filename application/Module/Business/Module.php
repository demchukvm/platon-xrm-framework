<?php
namespace Application\Module\Business;

use Sl_Module_Abstract as AbstractModule;

class Module extends AbstractModule {
    
    public function getListeners() {
        return array(
            new Listener\Businesscodes($this),
            new Listener\Productedit($this),
            new Listener\Usernotification($this),
            new Listener\Counterparty($this),
        );
    }
}