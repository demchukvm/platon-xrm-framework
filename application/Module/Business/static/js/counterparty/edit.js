$(function() {
    if ($('#essentials-counterpartycontact').length) {
        $('body').on('click', 'td.set-radio input', function() {
            $('#essentials-counterpartycontact').val($('form.model-counterparty td.set-radio input:checked').length?$('form.model-counterparty td.set-radio input:checked').val():'');
        });
        $('body').on('change','#modulerelation_counterpartycontact', function(){
            $('#essentials-counterpartycontact').val($('form.model-counterparty td.set-radio input:checked').length?$('form.model-counterparty td.set-radio input:checked').val():''); 
        });
    }
    
     $('table.table.blockquote').on('click', '.files_list_table_delete', function(){
        var $this = $(this);
        var alias = $this.parents('table:first').find('.ajax-deletable').data('alias');
        
        $.ajax({
            type: 'POST',
            cache: false,
            url: '/'+alias.split('.').join('/')+'/ajaxdelete',
            data: {
                id: $this.data('id')
            },
            success: function(data){
                if(data.result) {
                    var ids = $('#modulerelation_counterpartycontact').val().split(';');
                    
                    ids.splice(ids.indexOf($this.data('id')+''));
                    $this.parents('tr:first').remove();
                    $('#modulerelation_counterpartycontact').val(ids.join(';')).change();
                } else {
                    $.alert(data.description);
                }
            }
       });
    });
    
});


/*
 $(document).ready(function(){
 setTimeout(function(){
 $('#modulerelation_counterpartycontact').each(function(){
 var $this = $(this);
 var $form = $this.closest('form');
 
 $this.on('change', function(e, data) {
 if(/^\:.+/.test($this.val())){
 $.ajax({
 type: 'POST',
 url: $this.data('create-rel'),
 data: {
 name: $this.val().split(':')[1],
 ajax_action: 1,
 'data-extended': 'name',
 },
 success: function(data) {
 if(data.result) {
 $this.select2('close');
 $this.data('name', data.extra.name);
 $this.select2('val', data.result.id);
 } else {
 $this.select2('val','');
 $.alert(data.description);
 }
 }
 })
 }
 });
 
 
 $this.select2('destroy');
 var opts = {
 createSearchChoice: function(term, data) {
 if(term.length) {
 data.push({id: ':' + term, text: 'Create contact "'+term+'"'});
 }
 return data;
 },
 ajax: {
 results: function(data, page) {
 var d = [];
 if(data.result && data.aaData.length) {
 for(var i in data.aaData) {
 d.push({id: data.aaData[i].id, text: data.aaData[i].name})
 }
 } else if($this.data('select2').search.val().length) {
 d.push({id: '', text: 'You can add new contact', disabled: true});
 }
 return {results: d};
 }
 }
 };
 assignAutocomplete($form, '#modulerelation_counterpartycontact', opts);
 });
 }, 1000);
 });
 */