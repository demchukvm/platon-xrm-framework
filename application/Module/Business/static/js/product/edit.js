var ajaxedit_url = '/business/product/ajaxedit/'
var form_wrapper = '#form';
var gant_wrapper = '#gant';
var new_subprod_name;

$(function() {

    $(window).on('scroll', function(){
       if (($('#milestone-gantt').height() + $('#milestone-gantt').offset().top) < $('body').scrollTop())
        $('.right-boxes').css({position:'fixed', top:50, width:$('.right-boxes').width()});
       else  
        $('.right-boxes').css({position:'', top:'', width:''});
    });
    
    $('#milestone-gantt').on('click', '.gantt-ms-edit', function(e){
           if (gantt.config.readonly){
               gantt.config.readonly = false; 
               gantt.config.row_height = 24; 
               $('#milestone-gantt').removeClass('gantt-readonly');
               $('.gantt-wrap').animate({height: 500});
               $ge.reload();
           } else {
               gantt.config.readonly = true; 
               gantt.config.row_height = 18; 
               $('#milestone-gantt').addClass('gantt-readonly');
               $('.gantt-wrap').animate({height: 200});
               $ge.reload(1);
           }
       //$(this).parent('.gantt-panel').slideUp(); 
        //$('a.gantt-tab:first').click();
    });
    
    $('.nav.nav-tabs.nav-justified a[href="#form"]').click(function() {

        show_ajaxedit_form($(form_wrapper), $(this).data('model_id'));
    });

    setTimeout(function(){loadGanttFromServer('#milestone-gantt',{readonly:true,row_height:18},1)},2000);

     $('body').on('shown.bs.tab', 'section.panel', function(e) {
        if ($('#gantt').is(':visible')) {
            $('.right-panel').hide();
            $('.gantt-panel').slideUp(function(){$('#milestone-gantt > div').appendTo('#gantt');gantt.config.readonly = false; gantt.config.row_height = 24; $ge.reload();});
            $('.main-panel').removeClass('col-lg-8').addClass('col-lg-12');
      
        } else {
            $('.main-panel').removeClass('col-lg-12').addClass('col-lg-8');
            $('.right-panel').show();
            
            $('.gantt-panel:not(:visible)').slideDown(function(){
                $('#gantt > div').appendTo('#milestone-gantt');
                gantt.config.readonly = true;
                gantt.config.row_height = 18;
                $ge.reload(1);
                //$ge.render();
                //loadGanttFromServer('#milestone-gantt',{readonly:true}, 1);
            });
            
            //$('#gantt').html('');
        }

    });

/*
    $('body').on('popover.opened.form', function(event, wrapper) {
            if($(wrapper).attr('data-alias') == 'business.deal'){
              var type_wrap = $('#modulerelation_subproductdeal_names', wrapper);
              if (type_wrap.length) assignCreateAutocomplete(type_wrap);
            } 

    });
*/
    
    $('body').on('on.adding.type.button', function(event, wrapper) {

        var $addingbutton = $('body').find('.create_cur_obj');
        if ($addingbutton.length === 0) {
            var $label = $(wrapper).parents('.control-group:first').find('.control-label');
            $addingbutton = $('<span />').addClass('create_cur_obj fa fa-plus');
            $label.append($addingbutton);
        }
    });

    $('body').on('off.adding.type.button', function(event, wrapper) {

        var $addingbutton = $(wrapper).parents('.control-group:first').find('.create_cur_obj');
        if ($addingbutton.lenghth !== 0) {
            $addingbutton.remove();
        }

    });

    $('body').on('shown.bs.tab', 'section.panel', function() {
         if ($('#tm').is(':visible') && !$('#tm .filter_selector').length) {
            var tag_filter = 'taskmaintag:id-in-' + crm_object_tag_id;
            var tag_filter1 = 'status-lt-100';
            
            $.get('/crm/task/popups/type/1/hot_status/1/subaction/tm/', {
                filter_fields: [tag_filter, tag_filter1]
            }, function(data) {
                data = $('[data-alias]:first', data);
                $(data).appendTo('#tm');
                var ctrl = new listviewController($('#tm').find('[data-alias]:first'), {
                    useFolders: true,
                    fnRedrawTable: function() {
                        loadData();
                    }
                });

              
            });
        }
    });

/*
    $('body').on('popover.ajaxcreate.submit', function(event, res){
        
      if ($(res.form).parents('#nav'))
       var url = res.res.forvard_url + '/id/'+ res.res.result.id;
       console.log(url, event, res);
       //window.location.replace(document.location.protocol + '//' + document.location.host+url);
       
         
             
        
    });
*/

  /*  $('body').on('click', '#create_deal_prod', function() {

       var prodname = $(this).attr('prod_name');
      // console.log($('body').find('.popover').find('#modulerelation_productdeal_names'));


    });*/

    
    $(form_wrapper).on('submit', 'form', function() {
        var $form = $(this);
        var $target = $($form.attr('data-target'));
        var model_id = $('input[name="id"]', $form).val();
        $('#Save_ajax').hide();
        $.ajax({
            type: $form.attr('method'),
            url: $form.attr('action'),
            data: $form.serialize(),
            success: function(res) {

                if (res.result) {
                    // show_ajaxedit_form($(form_wrapper), model_id);
                    window.location.href = window.location.href.replace('#','');
                } else {
                    $('#Save_ajax').show();
                    alertErrors(res.description);
                }
            }
        });
        event.preventDefault();
        return false;


    });

    $('.nav.nav-tabs.nav-justified a[href="#gant"]').click(function() {
        if ($(gant_wrapper) && !$(gant_wrapper).html().length) {
            var $gant = $('<div/>').attr('id', 'gant_workSpace');
            $gant.appendTo($(gant_wrapper));

        }

    });

});

var show_ajaxedit_form = function($wrapper, model_id) {

    var request_obj = {};
    $wrapper.html('');
    $.ajax({url: ajaxedit_url,
        data: {id: model_id},
        success: function(data) {
            if (data.result && data.hasOwnProperty('form')) {
                $(data.form).appendTo($wrapper);
                $(datepicker_selector, $wrapper).datepicker(datepicker_options);
                $(datetimepicker_selector, $wrapper).datetimepicker(datetimepicker_options);
                assignAutocomplete($wrapper);
            }
        },
        contentType: 'json'
    })

}

assignCreateAutocomplete = function(wrapper) {

    var $this = $(wrapper);
    var ids_filter;
    var $form_wrapper = $this.parents('form:first');
    var ids_field_name = $this.attr('id').replace(/_names$/, '');

    var create_button_id = ids_field_name + '_create';
    var $create_button = $form_wrapper.find('#' + create_button_id);


    $this.getRelType = function() {
        return $(this).attr('data-type') ? $(this).attr('data-type').charAt($(this).attr('data-type').length - 1) : false;
    }
    if ($this.getRelType() === '2') {
        var ids = $('#' + ids_field_name).val().length ? $('#' + ids_field_name).val().split(';') : [];
        var ids_filter = ids.length ? 'id-nin-' + ids.join(',') : '';

    }

    $this.on('keyup', function() {

        new_subprod_name = $(this).val();
    });

    $('body').on('click', function(event) {
        var target = event.target;
        if ($(target).hasClass('create_cur_obj')) {
            //console.log([$this, $(this)]);
            var prod_id = $this.parents('.ajaxcreate:first').find('.field-modulerelation_productdeal').val();
            var url = $this.parents('.ajaxcreate:first').find('#modulerelation_subproductdeal_create').attr('data-rel');
            var name = new_subprod_name;
            $this.parents('.ajaxcreate:first').find('input.field-modulerelation_subproductdeal').attr('data-name', name);
            $this.attr('readonly', 'readonly');
            $this.parents('.ajaxcreate:first').find('#Save_ajax').attr('disabled', 'disabled');
            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    name: name,
                    ajax_action: 1,
                    modulerelation_productsubproduct: [prod_id],
                    modulerelation_productsubproduct_names: 'plug'
                },
                success: function(data) {
                    $('body').trigger('off.adding.type.button', $this);
                    $this.removeAttr('readonly');
                    $this.parents('.ajaxcreate:first').find('#Save_ajax').removeAttr('disabled');
                    if (data.result) {
                        $this.parents('.ajaxcreate:first').find('input.field-modulerelation_subproductdeal').val(data.result.id);
                        $this.parents('.ajaxcreate:first').find('input.field-modulerelation_subproductdeal').attr('data-name', data.result.string);
                        $this.val(data.result.string);
                    } else {
                        $this.parents('.ajaxcreate:first').find('input.field-modulerelation_subproductdeal').val('');
                        $this.val('');

                        alert(data.description);
                    }
                }
            })

        } else if ($(target).hasClass('field-modulerelation_subproductdeal-names')) {
        } else {
            $('body').trigger('off.adding.type.button', $this);
        }

    });

    /* $this.on('change blur', function() {
     $('#' + ids_field_name);
     if ($this.getRelType() === '1') {
     if ($this.val().length && $this.val() != $('#' + ids_field_name).attr('data-name')) {
     $this.val($('#' + ids_field_name).attr('data-name'));
     } else if (!$this.val().length) {
     $('#' + ids_field_name).attr('data-name', '');
     // $('body').trigger('off.adding.type.button', $this);
     if ($('#' + ids_field_name).val().length) {
     $('#' + ids_field_name).val('').change();
     }
     
     }
     }
     });*/
    $this.autocomplete({
        open: function() {
            $(this).autocomplete('widget').css('z-index', 10000);
            return false;
        },
        minLength: 0,
        source: function(request, responce) {
            var url = $this.attr('data-rel');
            var filter_fields = getFilters($form_wrapper, $this);
            if (ids_filter && ids_filter.length) {
                if (filter_fields instanceof Array) {
                    filter_fields.push(ids_filter);
                } else {
                    filter_fields = [ids_filter];
                }

            }

            $.ajax({
                method: 'POST',
                url: url,
                data: {
                    name: request.term,
                    filter_fields: filter_fields,
                    quick_search: 1,
                    handling: $this.attr('data-handling')
                },
                success: function(data) {
                    $this.removeClass('ui-autocomplete-loading');
                    if (request.term !== '' && $create_button.length) {
                        $('body').trigger('on.adding.type.button', $this);
                    } else {
                        $('body').trigger('off.adding.type.button', $this);
                    }
                    if (data.result) {
                        var name_index = 3;
                        var value_index = 0;
                        if (data.hasOwnProperty('name_index') && data.name_index) {
                            name_index = data.name_index;
                        }
                        if (data.hasOwnProperty('value_index') && data.value_index) {
                            value_index = data.value_index;
                        }
                        if (data.hasOwnProperty('sort_array')) {
                            for (key in data.sort_array) {
                                if (data.sort_array[key] == 'name') {
                                    name_index = key;
                                    break;
                                }
                            }
                        }
                        if (data.aaData.length) {
                            responce($.map(data.aaData, function(item) {
                                var label;
                                try {
                                    if ($(item[name_index]).length) {
                                        label = $(item[name_index]).text();
                                    } else {
                                        label = item[name_index];
                                    }
                                } catch (e) {
                                    label = item[name_index];
                                }
                                return {
                                    value: item[value_index],
                                    label: label
                                };
                            }));
                        } else {
                        }
                    } else {

                        alert(data.description);
                    }
                }
            })
        },
        select: function(e, ui) {
            var ids_field_name = $this.attr('id').replace(/_names$/, '');
            $ids_field = $('#' + ids_field_name);
            if ($this.getRelType() === '1') {
                $this.val(ui.item.label);
                $ids_field.val(ui.item.value);
                $ids_field.attr('data-name', ui.item.label);

                $ids_field.trigger('change');
                $('body').trigger('off.adding.type.button', $this);
                return false;
            } else if ($this.getRelType() === '2') {
                $this.val(ui.item.label);
                $ids_field.attr('data-name', ui.item.label);
                if (ui.item.value) {
                    var ids = $ids_field.val().length ? $ids_field.val().split(';') : [];
                    ids.push(ui.item.value);
                    $this.attr('readonly', 'readonly');
                    $.ajax({
                        cache: false,
                        type: 'POST',
                        url: $this.attr('ajaxdetailed'),
                        data: {
                            ids: ui.item.value,
                            extended: 'url'
                        },
                        success: function(data) {
                            if (data.result) {
                                var names = [];
                                var els = {};
                                for (var i in data.objects) {
                                    els[i] = {
                                        name: data.objects[i],
                                        href: data.extra[i].url,
                                        id: i,
                                        extra: null
                                    };
                                }
                                setPopupInitiator($this);
                                setSelectionCallBack(ids, names, els);
                                $this.val('');
                                $this.removeAttr('readonly');
                                ids = $('#' + ids_field_name).val().length ? $('#' + ids_field_name).val().split(';') : [];
                                ids_filter = ids.length ? 'id-nin-' + ids.join(',') : '';
                            } else {
                                $this.parents('tr:first').show(500);
                                $.alert(data.description);
                                $this.val('');
                                $this.removeAttr('readonly');
                            }
                        }
                    });

                    return false;
                }

            }

        },
        focus: function(e, ui) {
            $('body').trigger('off.adding.type.button', $this);
            $(this).val(ui.item.label);
            return false;
        }
    });


}