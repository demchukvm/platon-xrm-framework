var ajaxedit_url = '/business/deal/ajaxedit/'
var form_wrapper = '#form';
var gant_wrapper = '#gant';
var temp_ctry_tpl = '<span class="temp-btns"><i class="fin" data-toggle="tooltip" data-placement="right" data-original-title="20%">fin</i><i class="sel" data-toggle="tooltip" data-placement="right" data-original-title="20%">sel</i><i class="buy" data-toggle="tooltip" data-placement="right" data-original-title="20%">buy</i><i class="op" data-toggle="tooltip" data-placement="right" data-original-title="20%">op</i></span>';


$(function() {
    
    $(window).on('scroll', function(){
       if (($('.members-panel').height() + $('.members-panel').offset().top) < $('body').scrollTop())
        $('.right-boxes').css({position:'fixed', top:50, width:$('.right-boxes').width()});
       else  
        $('.right-boxes').css({position:'', top:'', width:''});
    });
    
    
    $('.toggle-feed').on('click', function(){
        $('.feed-panel').toggleClass('feed');
    });
    
    $('#milestone-gantt').on('click', '.gantt-ms-edit', function(e){
           if (gantt.config.readonly){
               gantt.config.readonly = false; 
               gantt.config.row_height = 24; 
               $('#milestone-gantt').removeClass('gantt-readonly');
               $('.gantt-wrap').animate({height: 500});
               $ge.reload();
           } else {
               gantt.config.readonly = true; 
               gantt.config.row_height = 18; 
               $('#milestone-gantt').addClass('gantt-readonly');
               $('.gantt-wrap').animate({height: 200});
               $ge.reload(1);
           }
       //$(this).parent('.gantt-panel').slideUp(); 
        //$('a.gantt-tab:first').click();
    });
    
     setTimeout(function(){loadGanttFromServer('#milestone-gantt',{readonly:true,row_height:18},1)},2000);

     $('body').on('shown.bs.tab', 'section.panel', function(e) {
        if ($('#gantt').is(':visible')) {
            $('.right-panel').hide();
            $('.gantt-panel').slideUp(function(){$('#milestone-gantt > div').appendTo('#gantt');gantt.config.readonly = false;gantt.config.row_height = 24;$ge.reload();});
            $('.main-panel').removeClass('col-lg-8').addClass('col-lg-12');
            /* start of test gantt   */
            /*if (!$('#gantt').html().trim().length) {
                
                
                loadGanttFromServer('#gantt');

            } */
        } else {
            $('.main-panel').removeClass('col-lg-12').addClass('col-lg-8');
            $('.right-panel').show();
            if ($('#gantt > div').length){
                $('.gantt-panel:not(:visible)').slideDown(function(){
                    $('#gantt > div').appendTo('#milestone-gantt');
                    gantt.config.readonly = true;
                    gantt.config.row_height = 18;
                    $ge.reload(1);
                    //$ge.render();
                    //loadGanttFromServer('#milestone-gantt',{readonly:true}, 1);
                });
            }
            
            //$('#gantt').html('');
        }

    });
    
    $('body').on('form.opened.sl',function(e){
       var f = $(e.target);
       
       $('input[data-modelname], #projectmanager_ajax, #trader_ajax, #legal_support_ajax, #tax_and_accounting_support_ajax', f).each(function(){
           
           $('label[for="'+$(this).attr('id')+'_names"],label[for="'+$(this).attr('id')+'"]', f).after(temp_ctry_tpl);
          
       });
       $('.temp-btns i').tooltip();
    });
    
    $('body').on('click', '.temp-btns i', function(){
        $(this).toggleClass('selected');
    });
    
    
    $('body').on('click', '.custom_picture', function(){
        $('.temp-btns i').toggleClass('selected');
    });
    
    
    
    $('body #accordion_form').on('click', 'a.accordion-toggle', function(){
        var $form = $('.form',$(this).attr('href'));
        $form.html('');
        
        if (($(this).data('fields') || $(this).data('fieldgroups')) && $form.length && $form.is(':not(:visible)')){
            $form.html($('<div/>').html('loading...'));
            var form = new Dealform($(this).data('fields'), $(this).data('fieldgroups'));
            
            form.render($form, true);
            
        } 
    });
    
    $('body').on('shown.bs.tab', 'section.panel', function() {
         
          if ($('#tm').is(':visible') && !$('#tm .filter_selector').length) {
            var tag_filter = 'taskmaintag:id-in-' + crm_object_tag_id;
            var tag_filter1 = 'status-lt-100';
            
            $.get('/crm/task/popups/type/1/hot_status/1/subaction/tm/', {
                filter_fields: [tag_filter, tag_filter1]
            }, function(data) {
                data = $('[data-alias]:first', data);
                $(data).appendTo('#tm');
                var ctrl = new listviewController($('#tm').find('[data-alias]:first'), {
                    useFolders: true,
                    fnRedrawTable: function() {
                        loadData();
                    }
                });

              
            });
        }

    });

    $('.nav.nav-tabs.nav-justified a[href="#gant"]').click(function() {
        if ($(gant_wrapper) && !$(gant_wrapper).html().length) {
            var $gant = $('<div/>').attr('id', 'gant_workSpace');
            $gant.appendTo($(gant_wrapper));

        }

    });
    
   $('#form').on('change', 'input, textarea, select', function(){
       var _el = this;
       var $f = $(this).closest('.form');
       var data = {id:$('#post_feed').data('model_id'),'ajax_update':1}
            data[$(this).attr('name')] = $(this).val();
            
       $.ajax({
            url:ajaxedit_url,
            type:'POST',
            data:data,
            success:function(data){
                if (data.result){
                    $('select, input, textarea', $f).each(function(){
                        if (!$(this).is(_el) && $(this).is(':not(:focus):not(.autocomplete)') && $('#'+$(this).attr('id'),$(data.form)).length){
                            
                            $(this).val($('#'+$(this).attr('id'),$(data.form)).val());
                        }
                    })
                 }
            }
    });
   }); 
   new slParticipatorsController('.members-panel');
   $('body').data('DealFormController', new SlDealFormController('body', {
       id: $('#post_feed').data('model_id')
   }));
   new SlForms().checkForms(['auth.user', 'business.counterparty', 'business.contact']);
});

var Dealform = function(fields, fieldgroups){
    
    this.fields = {};
    var el_fields = {};

    if (fields){
        el_fields['_'] = (fields instanceof Array?fields:fields.split(/[^a-z_\d:|]/));
    }else if (fieldgroups instanceof Object){
    
        el_fields = fieldgroups; 
    
    } else if (fieldgroups){
        
        var el_fgs = fieldgroups.split('|');
        var el_fg;
        for (var i in el_fgs){
            el_fg = el_fgs[i].split(':');
            el_fields[el_fg[0]] = el_fg[1].split(/[^a-z_\d:|]/);
        }
    }
    for (var k in el_fields){
        if (!(el_fields[k] instanceof Array && el_fields[k].length)) continue;
        
        for (var i in el_fields[k]){
            var fs = el_fields[k][i].trim();
            if (!fs.length) continue;
            if (!this.fields.hasOwnProperty(k))this.fields[k] = [];
            
            this.fields[k].push(fs);   
            if (fs.match(/^modulerelation_/)){
                this.fields[k].push(fs+'-names');   
            }
        }
    }
}

Dealform.prototype.render = function($wrapper, clear){
    var _self=this;
    
    $.ajax({
        url:ajaxedit_url,
        type:'POST',
        data:{id:$('#post_feed').data('model_id')},
        success:function(data){
            if (data.result){
                var f = $('<div/>').addClass('form-horizontal');
                for (var i in _self.fields){
                  var fg = $('<div/>').addClass('field-gr');
                  
                  if (!/^_.*/.test(i)){
                      $('<h4/>').html(i).appendTo(fg);
                  }
                  for (var k in _self.fields[i]){  
                    var cl = '.field-'+_self.fields[i][k];

                    if ($(cl,$(data.form)).length){
                        if ($(cl,$(data.form)).closest('.control-group').length){
                            $(cl,$(data.form)).closest('.control-group').appendTo(fg);
                        } else {
                            $(cl,$(data.form)).appendTo(fg);
                        }
                    }
                  }
                  fg.appendTo(f);
                }
                if (clear) $wrapper.html('');
                $(f).appendTo($wrapper);
                $(datepicker_selector, f).datepicker(datepicker_options);
                $(datetimepicker_selector, f).datetimepicker(datetimepicker_options);
                assignAutocomplete(f);
                $wrapper.trigger('form.opened.sl');
            }

        }
            
    });
    
};

function slParticipatorsController(wrapper, opts) {
    function slParticipator(data) {
        var _self = this;
        
        var _options = _.extend({
            id: '',
            alias: '',
            name: '',
            initials: '',
            type: '',
            contact: '',
            color: '#bbb',
            rate: 0,
            relation: ''
        }, data || {});
        
        var _required = ['id', 'alias', 'type', 'relation'];
        
        this.get = function(what) {
            var method = 'get'+what.ucFirst();
            if(_self.hasOwnProperty(method) && (typeof _self[method] === 'function')) {
                return _self[method]();
            }
            if(_options.hasOwnProperty(what)) {
                return _options[what];
            }
            return null;
        };
        
        this.set = function(what, value) {
            var method = 'set'+what.ucFirst();
            if(_self.hasOwnProperty(method) && (typeof _self[method] === 'function')) {
                return _self[method](value);
            }
            if(_options.hasOwnProperty(what)) {
                _options[what] = value;
            }
            return self;
        };
        
        this.serialize = function() {
            var r = {};
            _.each(_options, function(o, i){
                r[i] = _self.get(i);
            });
            return r;
        };
        
        _.each(_required, function(what){
            if(!_self.get(what)) {
                throw what.ucFirst()+' must be set';
            }
        });
        
        if(_options.hasOwnProperty('toArrayRecirsive')) {
            _.extend(_options, _options.toArrayRecirsive);
        }
        
        if(_options.hasOwnProperty('_essentials')) {
            if(_options['_essentials'].hasOwnProperty('counterpartycontact')) {
                if(_options.hasOwnProperty('counterpartycontact')) {
                    var _essentials = _.keys(_options['counterpartycontact']);
                    var name = null, cid = null;
                    _.each(_options['counterpartycontact'], function(contact){
                        if(!name && !cid) {
                            if(-1 !== _.indexOf(_essentials, contact.id)) {
                                name = contact.name;
                                cid = contact.id;
                            }
                        }
                    });
                    if(cid) {
                        _options.contact = name;
                        _options.cid = cid;
                    }
                }
            }
        }
    };
    
    var self = this;
    
    var _options = _.extend({
        wrapper: $(wrapper),
        mode: 'view'
    }, opts || {});
    
    this.getWrapper = function() {
        if(!_options.wrapper) {
            throw 'No wrapper defined.';
        }
        return _options.wrapper;
    };
    
    /*var _parts_restrictions = {};
    $('.parts-restictions span', self.getWrapper()).each(function(){
        _parts_restrictions[$(this).data('name')] = $(this).data('value');
    });*/
    
    var _parts_data = {};
    
    var _parts = {
        sellside: 10,
        sell: 20,
        fin: 20,
        operations: 20,
        buy: 20,
        buyside: 10
    };
    
    $('.parts-data span', self.getWrapper()).each(function(){
        _parts_data[$(this).data('name')] = $(this).data();
        _parts[$(this).data('name')] = $(this).data('rate');
    });
    
    var _tpls = {
        user: null,
        counterparty: null
    };
    
    var _participators = {
        sellside: {},
        sell: {},
        fin: {},
        operations: {},
        buy: {},
        buyside: {}
    };
    
    var _validateParts = function() {
        return _.reduce(_parts, function(n, m){
            return parseInt(n)+parseInt(m);
        }) <= 100;
    };
    
    this.getPartsSum = function(exclude) {
        var _to_process = _parts;
        if(exclude) {
            _to_process = _.omit(_to_process, exclude);
        }
        return _.reduce(_to_process, function(n, m){
            return parseInt(n)+parseInt(m);
        });
    }
    
    this.getMode = function() {
        return _options.mode;
    };
    
    this.checkMode = function(mode) {
        return self.getMode() === mode;
    };
    
    this.updateMode = function() {
        if(self.checkMode('view')) {
            $('.b-participator_adder', self.getWrapper()).hide();
        } else if(self.checkMode('edit')) {
            $('.b-participator_adder', self.getWrapper()).show();
        }
        self.getWrapper().attr('data-mode', self.getMode());
    };
    
    this.toggleMode = function(mode) {
        if(!self.checkMode(mode)) {
            _options.mode = mode;
        }
        self.updateMode();
        self.updateBlock();
    };
    
    this.getParts = function() {
        return _parts;
    };
    
    this.checkPart = function(part) {
        return _parts.hasOwnProperty(part);
    };
    
    this.getPart = function(name) {
        if(!self.checkPart(name)) {
            throw 'No such part defined';
        }
        return _parts[name];
    };
    
    this.checkParticipatorPart = function(participator, part) {
        return _parts_data[part].usertype === participator.get('type');
    };
    
    this.updatePart = function(name, value) {
        if(!self.checkPart(name)) {
            throw 'No such part defined';
        }
        if(value < 0) {
            throw new SlError('Value must be positive.', 101);
        }
        var _sum = 0;
        _.each(self.getParticipators(name), function(p){
            _sum += parseInt(p.get('rate'));
        });
        if(value < _sum) {
            throw new SlError('Value must be greater or equal '+_sum+'. Change participators parts fisrt.', 103);
        }
        var _part = _parts[name];
        _parts[name] = parseInt(value);
        if(!_validateParts()) {
            _parts[name] = _part;
            throw new SlError('Max value is: '+(100-self.getPartsSum(name)), 100);
        }
        return self;
    };
    
    this.addParticipator = function(part, participator) {
        if(!(participator instanceof slParticipator)) {
            throw 'participator param must be instanceof slParticipator';
        }
        if(!self.checkPart(part)) {
            throw 'Unknown part';
        }
        if(!self.checkParticipatorPart(participator, part)) {
            throw 'Illegal part <-> type combination: "'+part+' <-> '+participator.get('type')+'"';
        }
        _participators[part][participator.get('id')] = participator;
        self.updateBlock();
        return self;
    };
    
    this.removeParticipator = function(part, participator) {
        if(!(participator instanceof slParticipator)) {
            throw 'participator param must be instanceof slParticipator';
        }
        if(!self.checkPart(part)) {
            throw 'Unknown part';
        }
        delete _participators[part][participator.get('id')];
        
        var formCtrl = self.getWrapper().closest('body').data('DealFormController');
        if(formCtrl && _parts_data[part].relations.length) {
            /*@var formCtrl SlDealFormController*/
            var d = {};
            _.each(_parts_data[part].relations, function(relname){
                _.each(_parts_data, function(_data, _part){
                    _.each(_data.relations, function(_rel){
                        if(_rel == relname) {
                            _.each(self.getParticipators(_part), function(_p){
                                d['modulerelation_'+relname+'['+_p.get('id')+']'] = _p.get('id');
                            });
                        }
                    });
                });
            });
            _.each(self.getParticipators(), function(ps, part){
                d['rates['+part+'][rate]'] = self.getPart(part);
                _.each(ps, function(p){
                    d['rates['+part+'][participators]['+p.get('id')+']'] = p.get('rate');
                });
            });
            formCtrl.saveData(d);
        } else {
            throw 'No controller found';
        }
        self.updateBlock();
        self.saveRates();
        return self;
    };
    
    this.getParticipators = function(name) {
        if(!name) {
            return _participators;
        } else {
            if(!self.checkPart(name)) {
                throw 'Unknown part "'+name+'"';
            }
            return _participators[name];
        }
    };
    
    this.findParticipator = function(part, id) {
        if(typeof id !== 'number') {
            id = $(id).closest('.b-participator').data('id');
        }
        var _p = null;
        if(id) {
            _.each(self.getParticipators(part), function(p){
                if(!_p && p.get('id') == id) {
                    _p = p;
                }
            });
        }
        return _p;
    };
    
    this.cleanPart = function(part, update) {
        if(self.checkPart(part)) {
            _participators[part] = {};
            if(update !== false) {
                self.updateBlock();
            }
        }
    };
    
    this.saveRates = function() {
        var formCtrl = self.getWrapper().closest('body').data('DealFormController');
        if(formCtrl) {
            /*@var formCtrl SlSlDealFormController*/
            var d = {};
            _.each(self.getParticipators(), function(ps, part){
                d['rates['+part+'][rate]'] = self.getPart(part);
                _.each(ps, function(p){
                    d['rates['+part+'][participators]['+p.get('id')+']'] = p.get('rate');
                });
            });
            formCtrl.saveData(d);
        } else {
            
        }
    };
    
    this.getTpl = function(participator) {
        if(!(participator instanceof slParticipator)) {
            throw 'participator param must be instance of slParticipator';
        }
        if(!_tpls[participator.get('type')]) {
            _tpls[participator.get('type')] = _.template($('#participatorTpl-'+participator.get('type')).html());
        }
        return _tpls[participator.get('type')](participator.serialize());
    };
    
    this.updateBlock = function() {
        $('.b-participator', self.getWrapper()).remove();
        _.each(self.getParts(), function(rate, part) {
            _.each(self.getParticipators(part), function(participator) {
                var tpl_name = '#participatorTpl-' + participator.get('type');
                $('tbody [data-part="' + part + '"]').find('.b-participator_adder').before(self.getTpl(participator));
            });
        });
       
        $('tbody .b-participator .holder .editable', self.getWrapper()).each(function(){
            var $this = $(this);
            var part = $this.closest('[data-part]').data('part');
            var _value = parseInt($this.text());

            var max = self.getPart(part);
            var participator = self.findParticipator(part, $this);

            $this.editable({
                type: 'text',
                mode: 'inline',
                tpl: '<input type="text" class="inline_edit" value="" />',
                showbuttons: false,
                highlight: false,
                savenochange: true,
                onblur: 'submit',
                success: function(r, value) {
                    try {
                        var sum = 0;
                        _.each(self.getParticipators(part), function(p){
                            if(participator.get('id') != p.get('id')) {
                                sum += parseInt(p.get('rate'));
                            }
                        });
                        var _max = max - sum;
                        if(value > _max) {
                            throw new SlError('Value must be lower or equal '+_max, 102);
                        }
                        participator.set('rate', value);
                        self.updateBlock();
                        self.saveRates();
                    } catch(e) {
                        if(!(e instanceof SlError)) throw e;
                        if(e.code >= 100 && e.code <= 104) {
                            self.pop($this.closest('.holder'), {content: e.message});
                            return false;
                        }
                    }
                },
                emptytext: 0,
                emptyclass: ''
            });
            if(self.checkMode('view')) {
                $this.editable('disable');
            } else if(self.checkMode('edit')) {
                $this.editable('enable');
            }
        });
    };
    
    this.pop = function(el, opts, initOnly) {
        var _options = _.extend({
            placement: 'top',
            trigger: '_test',
            content: '',
            html: true,
            title: 'No title',
            container: self.getWrapper(),
            autoHide: 2000,
            canClose: true,
            after: function(){}
        }, opts || {});
        
        if(_options.canClose) {
            _options.title = '<i class="pull-right close fa fa-times"></i>'+_options.title;
        }
        
        $(el).data('bs.popover', null).popover(_options);
        if(initOnly !== true) {
            $(el).popover('show');
        }
        if(_options.autoHide) {
            setTimeout(function(){
                $(el).popover('destroy');
            }, _options.autoHide);
        }
        $(el).data('bs.popover').$tip.on('click', function(e){
            // Крайне странно, но работает
            e.stopPropagation();
        });
        if(_options.canClose) {
            $(el).data('bs.popover').$tip.on('click', 'i.close', function(e){
                $(el).popover('destroy');
            });
        }
        if(typeof _options.after === 'function') {
            _options.after($(el).data('bs.popover'), _options);
        }
    };
    
    $('thead [data-part]', self.getWrapper()).each(function(){
        $(this).find('.holder .editable').text(self.getPart($(this).data('part')));
    });
    
    $('.fa-gear', self.getWrapper()).on('click', function(){
        if(self.checkMode('view')) {
            self.toggleMode('edit');
        } else {
            self.toggleMode('view');
        }
    });
    
    $('.participators-data', self.getWrapper()).find('span').each(function(ind, el){
        self.addParticipator($(el).data('partType'), new slParticipator($(el).data()));
    });
    
    $('.b-participator_adder .autocomplete', self.getWrapper()).each(function(){
        var $this = $(this);
        var user_type = $this.closest('[data-part][data-type]').data('type');
        var part = $this.closest('[data-part]').data('part');
        // Неясно как выбирать, поэтому берем первую
        var relation = _parts_data[part].relations[0];
        
        assignAutocomplete($this.parent(), '.autocomplete', {
            multiple: false,
            ajax: {
                url: $this.data('rel'),
                dataType: 'json',
                data : function(term, page){
                    var f = getFilters($(this).data('wrapper'), this);
                    if ($(this).val().length) f.push('id-nin-'+$(this).val());
                    return {name: term, filter_fields: f, extraFields: ['alias', 'toArrayRecirsive']};
                },
                results: function (data, page) { // parse the results into the format expected by Select2.
                    var d = [];
                    if(data.result && data.aaData.length){
                       for(var i in data.aaData){
                            d.push(_.extend(data.aaData[i], {
                                text: data.aaData[i].name,
                                part: part,
                                type: user_type,
                                relation: relation
                            }));
                       }
                   }
                   return {results:d};
                }
            }
        });
        
        $this.on('change', function(){
            var $this = $(this);
            var data;
            if(data = $this.select2('data')) {
                var part_data = _parts_data[data.part];
                if(!part_data.multiple) {
                    self.cleanPart(part, false);
                }
                self.addParticipator(data.part, new slParticipator(data));
                $this.select2('data', null);
                
                var formCtrl = self.getWrapper().closest('body').data('DealFormController');
                if(formCtrl && _parts_data[data.part].relations.length) {
                    /*@var formCtrl SlDealFormController*/
                    var d = {};
                    _.each(_parts_data[data.part].relations, function(relname){
                        var rel_data = [];
                        _.each(_parts_data, function(_data, _part){
                            _.each(_data.relations, function(_rel){
                                if(_rel == relname) {
                                    // Мы в группе, которая транслируется в связь
                                    _.each(self.getParticipators(_part), function(_p){
                                        d['modulerelation_'+relname+'['+_p.get('id')+']'] = _p.get('id');
                                    });
                                }
                            });
                        });
                    });
                    formCtrl.saveData(d);
                } else {
                    throw 'No controller found';
                }
                self.saveRates();
            }
        });
    });
    
    self.getWrapper().on('click', '.b-participator_delete', function(){
        var $this = $(this);
        var part = $this.closest('[data-part]').data('part');
        $.confirm('Are you sure?', 'Are you sure you want do delete this item?', null, function(){
            var p = self.findParticipator(part, $this);
            self.removeParticipator(part, p);
        });
    });
    
    $('thead [data-part] .holder .editable', self.getWrapper()).each(function(){
        var $this = $(this);
        var part = $this.closest('[data-part]').data('part');
        var _value = parseInt($this.text());
        $this.editable({
            type: 'text',
            mode: 'inline',
            tpl: '<input type="text" class="inline_edit" value="" />',
            showbuttons: false,
            highlight: false,
            savenochange: true,
            onblur: 'submit',
            success: function(r, value) {
                try {
                    self.updatePart(part, value);
                    self.saveRates();
                    self.updateBlock();
                } catch(e) {
                    if(!(e instanceof SlError)) throw e;
                    if(e.code >= 100 && e.code <= 104) {
                        self.pop($this.closest('.holder'), {content: e.message});
                        return false;
                    }
                }
            },
            emptytext: 0,
            emptyclass: ''
        });
    });
    
    self.getWrapper().on('click', '.popover', function(){
        $(this).remove();
    });
    
    self.getWrapper().on('click', '.b-participator_label', function(){
        var $this = $(this);
        var part = $this.closest('[data-part]').data('part');
        var participator = self.findParticipator(part, $this);
        /*var fields = [];
        if(participator.get('type') === 'user') {
            fields = ['name', 'login', 'email', 'userroles'];
        } else if(participator.get('type') === 'counterparty') {
            fields = ['name', 'business_line', 'phone', 'fax'];
        }*/
        if(participator) {
            self.pop($this, {
                title: 'Details',
                content: '<div style="text-align: center;"><div class="ui-autocomplete-loading" style="width: 28px; height: 15px; display: inline-block;"></div></div>',
                autoHide: false,
                after: function(popover, opts) {
                    var $div = $('<div style="display: none;"/>').appendTo(self.getWrapper());
                    var detailed = participator.serialize();
                    
                    var fC = new SlFormController($div, {
                        fields: _.map(new SlForms().getForm(participator.get('alias')).getFields(), function(field){
                            var f = field;
                            if(detailed.hasOwnProperty(field.name)) {
                                f.value = detailed[field.name];
                            }
                            return f;
                        }),
                        alias: participator.get('alias')
                    });
                    self.pop($this, {
                        title: 'Deatils',
                        content: $div.html(),
                        autoHide: false
                    });
                }
            });
        }
    });
    
    self.getWrapper().on('click', '.b-participator_contact', function(){
        var $this = $(this);
        var part = $this.closest('[data-part]').data('part');
        var participator = self.findParticipator(part, $this);
        //var fields = ['name', 'phone', 'email', 'fax'];
        if(participator) {
            self.pop($this, {
                title: 'Details',
                content: '<div style="text-align: center;"><div class="ui-autocomplete-loading" style="width: 28px; height: 15px; display: inline-block;"></div></div>',
                autoHide: false,
                after: function(popover, opts) {
                    var $div = $('<div style="display: none;"/>').appendTo(self.getWrapper());
                    var detailed = participator.get('counterpartycontact')[$this.data('id')];
                    
                    var fC = new SlFormController($div, {
                        fields: _.map(new SlForms().getForm('business.contact').getFields(), function(field){
                            var f = field;
                            if(detailed.hasOwnProperty(field.name)) {
                                f.value = detailed[field.name];
                            }
                            return f;
                        }),
                        alias: participator.get('alias')
                    });
                    self.pop($this, {
                        title: 'Deatils',
                        content: $div.html(),
                        autoHide: false
                    });
                    /*$.ajax({
                        type: 'POST',
                        cache: false,
                        url: '/business/contact/ajaxform',
                        data: {
                            id: $this.data('id')
                        },
                        success: function(data) {
                            if(data.result) {
                                var $div = $('<div style="display: none;"/>').appendTo(self.getWrapper());
                                var fC = new SlFormController($div, {
                                    fields: _.map(data.fields, function(field, name){
                                        if(typeof field.value === 'object') {
                                            field.value = _.toArray(field.value).join(', ');
                                        }
                                        return {
                                            name: name,
                                            label: field.label,
                                            value: field.value
                                        };
                                    }),
                                    alias: participator.get('alias'),
                                    templates: {
                                        text: '<div style="clear: both;" data-name="<%= name %>"><label><%= label %></label>:&nbsp;<span class="value_holder"><%= value %></span></div>'
                                    }
                                });
                                self.pop($this, {
                                    title: 'Deatils',
                                    content: $div.html(),
                                    autoHide: false,
                                });
                            } else {
                                $.alert(data.description);
                            }
                        }
                    });*/
                }
            });
        }
    });
};

function SlDealFormController(wrapper, opts) {
    var self = this;
    
    var _options = _.extend({
        container: $(wrapper),
        alias: false,
        ep: false,
        id: 0
    }, opts || {});
    
    /**
     * 
     * @returns {jQuery}
     */
    this.getContainer = function() {
        if(!_options.container || !_options.container.length) {
            throw 'No container (wrapper) specified. Aborting ..';
        }
        return _options.container;
    };
    
    this.getId = function() {
        return _options.id;
    };
    
    if(!self.getId()) {
        _options.id = self.getContainer().data('id');
        if(!_options.id) {
            throw 'No id set for controller. Aborting ...';
        }
    }
    
    /**
     * 
     * @returns {SlEntryPoint}
     */
    this.getEp = function() {
        if(!_options.ep) {
            _options.ep = new SlEntryPoint(self.getContainer());
        }
        return _options.ep;
    };
    
    this.saveData = function(fields, success, error) {
        success = (success && typeof success == 'function')?success:function(){};
        error = (error && (typeof error == 'function'))?error:function(){};
        var d = {
            id: self.getId(),
            'ajax_update': 1
        };
        _.each(fields, function(value, field){
            d[field] = value;
        });
        
        $.ajax({
            url: self.getEp().getUrl('ajaxedit'),
            type: 'POST',
            data: d,
            success: success,
            error: error
        });
    };
};
