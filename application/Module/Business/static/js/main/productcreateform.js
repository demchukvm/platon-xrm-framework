$(function(){
   
   $('body').on('popover.opened.form', function(e, f){
       //$(this).select2.defaults.formatNoMatches = 'Hello!';
       
       if ($(f).has('form.model-deal.ajaxcreate.editable') && $(f).has('#modulerelation_productdeal') && $(f).has('#modulerelation_subproductdeal') && $(f).has('#form.model-deal.ajaxcreate.editable')) {
           
           var l = $('#modulerelation_subproductdeal').closest('div.control-group');
          
           $('#modulerelation_subproductdeal')
               .on('change', function(e,a){
                    if($(this).val().length && $(this).val().match(/^\:.+/)){
                        
                
                        var $this = $('form.model-deal.ajaxcreate.editable #modulerelation_subproductdeal');
                        var prod_id = $('form.model-deal.ajaxcreate.editable #modulerelation_productdeal').val();
                        var url = $this.data('create-rel');
                        var name = $(this).val().match(/^\:(.+)/)[1];
                        $this.parents('.ajaxcreate:first').find('#Save_ajax').attr('disabled', 'disabled');
                        $.ajax({
                            type: 'POST',
                            url: url,
                            data: {
                                name: name,
                                ajax_action: 1,
                                'data-extended': 'name',
                                modulerelation_productsubproduct: [prod_id],
                                modulerelation_productsubproduct_names: 'plug'
                            },
                            success: function(data) {

                                $this.parents('.ajaxcreate:first').find('#Save_ajax').removeAttr('disabled');

                                if (data.result) {
                                    $this.select2('close');
                                    $this.data('name', data.extra.name);
                                    $this.select2('val',data.result.id);
                                    /*parents('.ajaxcreate:first').find('input.field-modulerelation_subproductdeal').val(data.result.id);
                                    $this.parents('.ajaxcreate:first').find('input.field-modulerelation_subproductdeal').attr('data-name', data.result.string);
                                    $this.val(data.result.string); */
                                } else {
                                    $this.select2('val','');
                                    alert(data.description);
                                }
                            }
                        })
                
                
                    }
               });
            
           if ($('#modulerelation_subproductdeal',f).data('create-rel')){
                $('#modulerelation_subproductdeal',f).select2('destroy');
                var opt = {createSearchChoice: function (term, data){
                        if ($('#modulerelation_productdeal').val().length && term.length){
                            return {id:':'+term,text:'Create "'+term+'"'}
                        }
                        
                },
                    ajax: {
                        url: $('#modulerelation_subproductdeal').data('rel'),
                        dataType: 'json',

                        data : function(term, page){
                            var f = getFilters($(this).data('wrapper'), this);
                            //f = $.extend([],$(this).data('addition-filters'));

                            if ($(this).val().length) f.push('id-nin-'+$(this).val());
                            return {name: term, filter_fields:f};
                        },
                        
                        results: function (data, page) { // parse the results into the format expected by Select2.
                        var d = [];
                        if (data.result && data.aaData.length){
                           for (var i in data.aaData){
                               d.push({id:data.aaData[i].id, text:data.aaData[i].name})
                           }
                           
                           
                       } else if ($('#modulerelation_productdeal').val().length) {
                           
                           d.push({id:'', text:'You can enter new subproduct', disabled:true})
                       }
                       
                       return {results:d};

                    }}
                        
                }
                assignAutocomplete(f,'#modulerelation_subproductdeal',opt);
           }
           
           
       } 
       
   });

    $('body').on('change', 'form.model-deal.ajaxcreate.editable #modulerelation_productdeal', function(){
       //console.log('change');
       $('#modulerelation_subproductdeal').select2('val','');
    });
    
    
    
});
