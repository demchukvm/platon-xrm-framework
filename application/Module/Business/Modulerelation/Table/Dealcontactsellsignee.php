<?php
namespace Application\Module\Business\Modulerelation\Table;

class Dealcontactsellsignee extends \Sl\Modulerelation\DbTable {
	protected $_name = 'business_deal_contact_sell_signee';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Application\Module\Business\Model\Deal' => array(
			'columns' => 'deal_id',
			'refTableClass' => 'Application\Module\Business\Model\Table\Deal',
		'refColums' => 'id'),
                		'Application\Module\Business\Model\Contact' => array(
			'columns' => 'contact_id',
			'refTableClass' => 'Application\Module\Business\Model\Table\Contact',
				'refColums' => 'id'	),
	);
}