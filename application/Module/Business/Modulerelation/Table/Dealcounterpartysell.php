<?php
namespace Application\Module\Business\Modulerelation\Table;

class Dealcounterpartysell extends \Sl\Modulerelation\DbTable {
	protected $_name = 'business_deal_counterparty_sell';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Application\Module\Business\Model\Deal' => array(
			'columns' => 'deal_id',
			'refTableClass' => 'Application\Module\Business\Model\Table\Deal',
		'refColums' => 'id'),
                		'Application\Module\Business\Model\Counterparty' => array(
			'columns' => 'counterparty_id',
			'refTableClass' => 'Application\Module\Business\Model\Table\Counterparty',
				'refColums' => 'id'	),
	);
}