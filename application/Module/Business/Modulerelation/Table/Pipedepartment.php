<?php
namespace Application\Module\Business\Modulerelation\Table;

class Pipedepartment extends \Sl\Modulerelation\DbTable {
	protected $_name = 'business_pipe_department';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Application\Module\Business\Model\Pipe' => array(
			'columns' => 'pipe_id',
			'refTableClass' => 'Application\Module\Business\Model\Table\Pipe',
		'refColums' => 'id'),
                		'Application\Module\Kcompany\Model\Department' => array(
			'columns' => 'department_id',
			'refTableClass' => 'Application\Module\Kcompany\Model\Table\Department',
				'refColums' => 'id'	),
	);
}