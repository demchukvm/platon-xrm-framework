<?php
namespace Application\Module\Business\Modulerelation\Table;

class Subproductdeal extends \Sl\Modulerelation\DbTable {
	protected $_name = 'business_subproduct_deal';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Application\Module\Business\Model\Subproduct' => array(
			'columns' => 'subproduct_id',
			'refTableClass' => 'Application\Module\Business\Model\Table\Subproduct',
		'refColums' => 'id'),
                		'Application\Module\Business\Model\Deal' => array(
			'columns' => 'deal_id',
			'refTableClass' => 'Application\Module\Business\Model\Table\Deal',
				'refColums' => 'id'	),
	);
}