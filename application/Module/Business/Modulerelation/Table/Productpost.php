<?php
namespace Application\Module\Business\Modulerelation\Table;

class Productpost extends \Sl\Modulerelation\DbTable {
	protected $_name = 'business_product_post';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Application\Module\Business\Model\Product' => array(
			'columns' => 'product_id',
			'refTableClass' => 'Application\Module\Business\Model\Table\Product',
		'refColums' => 'id'),
                		'Application\Module\Kcomments\Model\Post' => array(
			'columns' => 'post_id',
			'refTableClass' => 'Application\Module\Kcomments\Model\Table\Post',
				'refColums' => 'id'	),
	);
}