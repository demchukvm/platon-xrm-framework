<?php
namespace Application\Module\Business\Modulerelation\Table;

class Dealincotermssell extends \Sl\Modulerelation\DbTable {
	protected $_name = 'business_deal_incoterms_sell';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Application\Module\Business\Model\Deal' => array(
			'columns' => 'deal_id',
			'refTableClass' => 'Application\Module\Business\Model\Table\Deal',
		'refColums' => 'id'),
                		'Application\Module\Business\Model\Incoterms' => array(
			'columns' => 'incoterms_id',
			'refTableClass' => 'Application\Module\Business\Model\Table\Incoterms',
				'refColums' => 'id'	),
	);
}