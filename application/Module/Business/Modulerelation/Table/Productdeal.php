<?php
namespace Application\Module\Business\Modulerelation\Table;

class Productdeal extends \Sl\Modulerelation\DbTable {
	protected $_name = 'business_product_deal';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Application\Module\Business\Model\Product' => array(
			'columns' => 'product_id',
			'refTableClass' => 'Application\Module\Business\Model\Table\Product',
		'refColums' => 'id'),
                		'Application\Module\Business\Model\Deal' => array(
			'columns' => 'deal_id',
			'refTableClass' => 'Application\Module\Business\Model\Table\Deal',
				'refColums' => 'id'	),
	);
}