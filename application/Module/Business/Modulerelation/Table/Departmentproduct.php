<?php
namespace Application\Module\Business\Modulerelation\Table;

class Departmentproduct extends \Sl\Modulerelation\DbTable {
	protected $_name = 'business_department_product';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Application\Module\Kcompany\Model\Department' => array(
			'columns' => 'department_id',
			'refTableClass' => 'Application\Module\Kcompany\Model\Table\Department',
		'refColums' => 'id'),
                		'Application\Module\Business\Model\Product' => array(
			'columns' => 'product_id',
			'refTableClass' => 'Application\Module\Business\Model\Table\Product',
				'refColums' => 'id'	),
	);
}