<?php
namespace Application\Module\Business\Modulerelation\Table;

class Dealusertaxandaccsupport extends \Sl\Modulerelation\DbTable {
	protected $_name = 'business_deal_user_taxandaccsupport';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Application\Module\Business\Model\Deal' => array(
			'columns' => 'deal_id',
			'refTableClass' => 'Application\Module\Business\Model\Table\Deal',
		'refColums' => 'id'),
                		'Sl\Module\Auth\Model\User' => array(
			'columns' => 'user_id',
			'refTableClass' => 'Sl\Module\Auth\Model\Table\User',
				'refColums' => 'id'	),
	);
}