<?php
namespace Application\Module\Business\Modulerelation\Table;

class Dealfile extends \Sl\Modulerelation\DbTable {
	protected $_name = 'business_deal_file';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Application\Module\Business\Model\Deal' => array(
			'columns' => 'deal_id',
			'refTableClass' => 'Application\Module\Business\Model\Table\Deal',
		'refColums' => 'id'),
                		'Sl\Module\Home\Model\File' => array(
			'columns' => 'file_id',
			'refTableClass' => 'Sl\Module\Home\Model\Table\File',
				'refColums' => 'id'	),
	);
}