<?php
namespace Application\Module\Business\Modulerelation\Table;

class Counterpartycontact extends \Sl\Modulerelation\DbTable {
	protected $_name = 'business_counterparty_contact';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Application\Module\Business\Model\Counterparty' => array(
			'columns' => 'counterparty_id',
			'refTableClass' => 'Application\Module\Business\Model\Table\Counterparty',
		'refColums' => 'id'),
                		'Application\Module\Business\Model\Contact' => array(
			'columns' => 'contact_id',
			'refTableClass' => 'Application\Module\Business\Model\Table\Contact',
				'refColums' => 'id'	),
	);
}