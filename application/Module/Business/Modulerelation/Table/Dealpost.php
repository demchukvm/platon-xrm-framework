<?php
namespace Application\Module\Business\Modulerelation\Table;

class Dealpost extends \Sl\Modulerelation\DbTable {
	protected $_name = 'business_deal_post';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Application\Module\Business\Model\Deal' => array(
			'columns' => 'deal_id',
			'refTableClass' => 'Application\Module\Business\Model\Table\Deal',
		'refColums' => 'id'),
                		'Application\Module\Kcomments\Model\Post' => array(
			'columns' => 'post_id',
			'refTableClass' => 'Application\Module\Kcomments\Model\Table\Post',
				'refColums' => 'id'	),
	);
}