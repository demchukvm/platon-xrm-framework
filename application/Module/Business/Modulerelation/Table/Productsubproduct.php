<?php
namespace Application\Module\Business\Modulerelation\Table;

class Productsubproduct extends \Sl\Modulerelation\DbTable {
	protected $_name = 'business_product_subproduct';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Application\Module\Business\Model\Product' => array(
			'columns' => 'product_id',
			'refTableClass' => 'Application\Module\Business\Model\Table\Product',
		'refColums' => 'id'),
                		'Application\Module\Business\Model\Subproduct' => array(
			'columns' => 'subproduct_id',
			'refTableClass' => 'Application\Module\Business\Model\Table\Subproduct',
				'refColums' => 'id'	),
	);
}