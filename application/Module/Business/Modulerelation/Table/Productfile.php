<?php
namespace Application\Module\Business\Modulerelation\Table;

class Productfile extends \Sl\Modulerelation\DbTable {
	protected $_name = 'business_product_file';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Application\Module\Business\Model\Product' => array(
			'columns' => 'product_id',
			'refTableClass' => 'Application\Module\Business\Model\Table\Product',
		'refColums' => 'id'),
                		'Sl\Module\Home\Model\File' => array(
			'columns' => 'file_id',
			'refTableClass' => 'Sl\Module\Home\Model\Table\File',
				'refColums' => 'id'	),
	);
}