<?php
return array (
  '_default' => 
  array (
    'name' => '_default',
    'label' => 'По-умолчанию',
    'fields' => 
    array (
      0 => 'name',
    ),
  ),
  '_popup' => 
  array (
    'name' => '_popup',
    'label' => 'По-умолчанию',
    'fields' => 
    array (
      0 => 'name',
    ),
  ),
  '_detailed' => array(
      'name' => '_detailed',
      'label' => 'По-умолчанию',
      'fields' => array(
          'name',
          'registered_addr',
          'regulatory_body',
          'physical_addr',
          'phone',
          'fax',
          'email',
          'establishment_date',
          'business_line',
          'counterpartycontact.name',
      ),
  ),
);
