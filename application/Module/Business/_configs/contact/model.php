<?php
return array (
  'name' => 
  array (
    'label' => 'Name',
    'type' => 'text',
  ),
  'phone' => 
  array (
    'label' => 'Phone',
    'type' => 'text',
  ),
  'fax' => 
  array (
    'label' => 'Fax',
    'type' => 'text',
  ),
  'email' => 
  array (
    'label' => 'Email',
    'type' => 'text',
  ),
  'main' => 
  array (
    'label' => 'MAIN',
    'type' => 'text',
  ),
);
