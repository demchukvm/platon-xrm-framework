<?php
return array (
  '_default' => 
  array (
    'name' => '_default',
    'label' => 'По-умолчанию',
    'fields' => 
    array (
      0 => 'name',
    ),
  ),
  '_popup' => 
  array (
    'name' => '_popup',
    'label' => 'По-умолчанию',
    'fields' => 
    array (
      0 => 'name',
    ),
    'type' => 'popup',
  ),
  '_detailed' => array(
        'name' => '_detailed',
        'label' => 'По-умолчанию',
        'fields' => array(
            'name',
            'phone',
            'email',
        ),
    ),
);
