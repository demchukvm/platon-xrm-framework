<?php
return array (
    'name' => array(
        'sortable' => true,
        'searchable' => true,
    ),
    'identifier' => array(
        'sortable' => true,
        'searchable' => true,
    ),
   
    'status' => array(
        'sortable' => true,
    ),
    'description' => array(
        
    ),
);
