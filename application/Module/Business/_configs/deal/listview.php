<?php
return array (
    'name' => array(
        'sortable' => true,
        'searchable' => true,
    ),
    'identifier' => array(
        'sortable' => true,
        'searchable' => true,
    ),
    'create' => array(
        'type' => 'date',
        'sortable' => true,
        'searchable' => true,
    ),
    'description' => array(
        
    ),
);
