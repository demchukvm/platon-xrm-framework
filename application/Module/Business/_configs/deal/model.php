<?php
return array (
  'name' => 
  array (
    'label' => 'NAME',
    'type' => 'text',
  ),
  'description' => 
  array (
    'label' => 'DESCRIPTION',
    'type' => 'text',
  ),
  'identifier' => 
  array (
    'label' => 'IDENTIFIER',
    'type' => 'text',
  ),
  'status' => 
  array (
    'label' => 'STATUS',
    'type' => 'text',
  ),
  'create' => 
  array (
    'label' => 'DATE',
    'type' => 'date',
  ),
  'productdeal.name' => 
  array (
    'label' => 'Product',
  ),
  'subproductdeal.name' => 
  array (
    'label' => 'Producttype',
  ),
  'quantity' => 
  array (
    'label' => 'QUANTITY',
    'type' => 'text',
  ),
  'quality' => 
  array (
    'label' => 'QUALITY',
    'type' => 'text',
  ),
  'buy_from' => 
  array (
    'label' => 'BUY_FROM',
    'type' => 'text',
  ),
  'sell_to' => 
  array (
    'label' => 'SELL_TO',
    'type' => 'text',
  ),
  'our_role' => 
  array (
    'label' => 'OUR_ROLE',
    'type' => 'text',
  ),
  'deal_type' => 
  array (
    'label' => 'DEAL_TYPE',
    'type' => 'text',
  ),
  'shipping_from' => 
  array (
    'label' => 'SHIPPING_FROM',
    'type' => 'text',
  ),
  'shipping_date' => 
  array (
    'label' => 'SHIPPING_DATE',
    'type' => 'date',
  ),
  'delivery_destination' => 
  array (
    'label' => 'DELIVERY_DESTINATION',
    'type' => 'text',
  ),
  'delivery_date' => 
  array (
    'label' => 'DELIVERY_DATE',
    'type' => 'date',
  ),
  'payment_terms_seller' => 
  array (
    'label' => 'PAYMENT_TERMS_SELLER',
    'type' => 'text',
  ),
  'payment_schedule_seller' => 
  array (
    'label' => 'PAYMENT_SCHEDULE_SELLER',
    'type' => 'text',
  ),
  'total_amount_to_be_paid' => 
  array (
    'label' => 'TOTAL_AMOUNT_TO_BE_PAID',
    'type' => 'text',
  ),
  'risk_exposure_seller' => 
  array (
    'label' => 'RISK_EXPOSURE_SELLER',
    'type' => 'text',
  ),
  'payment_terms_buyer' => 
  array (
    'label' => 'PAYMENT_TERMS_BUYER',
    'type' => 'text',
  ),
  'payment_schedule_buyer' => 
  array (
    'label' => 'PAYMENT_SCHEDULE_BUYER',
    'type' => 'text',
  ),
  'total_amount_to_be_recieved' => 
  array (
    'label' => 'TOTAL_AMOUNT_TO_BE_RECIEVED',
    'type' => 'text',
  ),
  'risk_exposure_buyer' => 
  array (
    'label' => 'RISK_EXPOSURE_BUYER',
    'type' => 'text',
  ),
  'logistics_company' => 
  array (
    'label' => 'LOGISTICS_COMPANY',
    'type' => 'text',
  ),
  'type_of_transportation' => 
  array (
    'label' => 'TYPE_OF_TRANSPORTATION',
    'type' => 'text',
  ),
  'delivery_schedule' => 
  array (
    'label' => 'DELIVERY_SCHEDULE',
    'type' => 'text',
  ),
  'cost_of_goods_sold' => 
  array (
    'label' => 'COST_OF_GOODS_SOLD',
    'type' => 'text',
  ),
  'gross_profit' => 
  array (
    'label' => 'GROSS_PROFIT',
    'type' => 'text',
  ),
  'accounting_and_legal_frees' => 
  array (
    'label' => 'ACCOUNTING_AND_LEGAL_FREES',
    'type' => 'text',
  ),
  'bank_service_charges' => 
  array (
    'label' => 'BANK_SERVICE_CHARGES',
    'type' => 'text',
  ),
  'financing_costs' => 
  array (
    'label' => 'FINANCING_COSTS',
    'type' => 'text',
  ),
  'comissions' => 
  array (
    'label' => 'COMISSIONS',
    'type' => 'text',
  ),
  'agency_frees' => 
  array (
    'label' => 'AGENCY_FREES',
    'type' => 'text',
  ),
  'insurance' => 
  array (
    'label' => 'INSURANCE',
    'type' => 'text',
  ),
  'interest' => 
  array (
    'label' => 'INTEREST',
    'type' => 'text',
  ),
  'transportation_logistics' => 
  array (
    'label' => 'TRANSPORTATION_LOGISTICS',
    'type' => 'text',
  ),
  'travel' => 
  array (
    'label' => 'TRAVEL',
    'type' => 'text',
  ),
  'services' => 
  array (
    'label' => 'SERVICES',
    'type' => 'text',
  ),
  'inventory_purchase' => 
  array (
    'label' => 'INVENTORY_PURCHASE',
    'type' => 'text',
  ),
  'sales_and_marketing' => 
  array (
    'label' => 'SALES_AND_MARKETING',
    'type' => 'text',
  ),
  'expenses' => 
  array (
    'label' => 'EXPENSES',
    'type' => 'text',
  ),
  'research_and_development' => 
  array (
    'label' => 'RESEARCH_AND_DEVELOPMENT',
    'type' => 'text',
  ),
  'general_administrative' => 
  array (
    'label' => 'GENERAL_ADMINISTRATIVE',
    'type' => 'text',
  ),
  'miscellaneous' => 
  array (
    'label' => 'MISCELLANEOUS',
    'type' => 'text',
  ),
  'deprecation_and_amortization' => 
  array (
    'label' => 'DEPRECATION_AND_AMORTIZATION',
    'type' => 'text',
  ),
  'taxes' => 
  array (
    'label' => 'TAXES',
    'type' => 'text',
  ),
  'net_profit' => 
  array (
    'label' => 'NET_PROFIT',
    'type' => 'text',
  ),
  'our_company_buy_name' => 
  array (
    'label' => 'OUR_COMPANY_BUY_NAME',
    'type' => 'text',
  ),
  'our_company_buy_registerede_addr' => 
  array (
    'label' => 'OUR_COMPANY_BUY_REGISTEREDE_ADDR',
    'type' => 'text',
  ),
  'our_company_buy_physical_addr' => 
  array (
    'label' => 'OUR_COMPANY_BUY_PHYSICAL_ADDR',
    'type' => 'text',
  ),
  'our_company_buy_phone' => 
  array (
    'label' => 'OUR_COMPANY_BUY_PHONE',
    'type' => 'text',
  ),
  'our_company_buy_fax' => 
  array (
    'label' => 'OUR_COMPANY_BUY_FAX',
    'type' => 'text',
  ),
  'our_company_buy_email' => 
  array (
    'label' => 'OUR_COMPANY_BUY_EMAIL',
    'type' => 'text',
  ),
  'our_company_sell_name' => 
  array (
    'label' => 'OUR_COMPANY_SELL_NAME',
    'type' => 'text',
  ),
  'our_company_sell_registerede_addr' => 
  array (
    'label' => 'OUR_COMPANY_SELL_REGISTEREDE_ADDR',
    'type' => 'text',
  ),
  'our_company_sell_physical_addr' => 
  array (
    'label' => 'OUR_COMPANY_SELL_PHYSICAL_ADDR',
    'type' => 'text',
  ),
  'our_company_sell_phone' => 
  array (
    'label' => 'OUR_COMPANY_SELL_PHONE',
    'type' => 'text',
  ),
  'our_company_sell_fax' => 
  array (
    'label' => 'OUR_COMPANY_SELL_FAX',
    'type' => 'text',
  ),
  'our_company_sell_email' => 
  array (
    'label' => 'OUR_COMPANY_SELL_EMAIL',
    'type' => 'text',
  ),
  'trader' => 
  array (
    'label' => 'TRADER',
    'type' => 'text',
  ),
  'legal_support' => 
  array (
    'label' => 'LEGAL_SUPPORT',
    'type' => 'text',
  ),
  'tax_and_accounting_support' => 
  array (
    'label' => 'TAX_AND_ACCOUNTING_SUPPORT',
    'type' => 'text',
  ),
  'projectmanager' => 
  array (
    'label' => 'PROJECTMANAGER',
    'type' => 'text',
  ),
  'counterparty_seller_full_legal_company_name' => 
  array (
    'label' => 'COUNTERPARTY_SELLER_FULL_LEGAL_COMPANY_NAME',
    'type' => 'text',
  ),
  'counterparty_seller_registered_address' => 
  array (
    'label' => 'COUNTERPARTY_SELLER_REGISTERED_ADDRESS',
    'type' => 'text',
  ),
  'counterparty_seller_regulatory_body' => 
  array (
    'label' => 'COUNTERPARTY_SELLER_REGULATORY_BODY',
    'type' => 'text',
  ),
  'counterparty_seller_physical_addr' => 
  array (
    'label' => 'COUNTERPARTY_SELLER_PHYSICAL_ADDR',
    'type' => 'text',
  ),
  'counterparty_seller_phone' => 
  array (
    'label' => 'COUNTERPARTY_SELLER_PHONE',
    'type' => 'text',
  ),
  'counterparty_seller_fax' => 
  array (
    'label' => 'COUNTERPARTY_SELLER_FAX',
    'type' => 'text',
  ),
  'counterparty_seller_email' => 
  array (
    'label' => 'COUNTERPARTY_SELLER_EMAIL',
    'type' => 'text',
  ),
  'counterparty_seller_establishment_date' => 
  array (
    'label' => 'COUNTERPARTY_SELLER_ESTABLISHMENT_DATE',
    'type' => 'date',
  ),
  'counterparty_seller_business_line' => 
  array (
    'label' => 'COUNTERPARTY_SELLER_BUSINESS_LINE',
    'type' => 'text',
  ),
  'counterparty_seller_contact_name' => 
  array (
    'label' => 'COUNTERPARTY_SELLER_CONTACT_NAME',
    'type' => 'text',
  ),
  'counterparty_seller_contact_phone' => 
  array (
    'label' => 'COUNTERPARTY_SELLER_CONTACT_PHONE',
    'type' => 'text',
  ),
  'counterparty_seller_contact_fax' => 
  array (
    'label' => 'COUNTERPARTY_SELLER_CONTACT_FAX',
    'type' => 'text',
  ),
  'counterparty_seller_contact_email' => 
  array (
    'label' => 'COUNTERPARTY_SELLER_CONTACT_EMAIL',
    'type' => 'text',
  ),
  'counterparty_seller_signee_name' => 
  array (
    'label' => 'COUNTERPARTY_SELLER_SIGNEE_NAME',
    'type' => 'text',
  ),
  'counterparty_seller_signee_phone' => 
  array (
    'label' => 'COUNTERPARTY_SELLER_SIGNEE_PHONE',
    'type' => 'text',
  ),
  'counterparty_seller_signee_fax' => 
  array (
    'label' => 'COUNTERPARTY_SELLER_SIGNEE_FAX',
    'type' => 'text',
  ),
  'counterparty_seller_signee_email' => 
  array (
    'label' => 'COUNTERPARTY_SELLER_SIGNEE_EMAIL',
    'type' => 'text',
  ),
  'counterparty_seller_signee_autorized_by' => 
  array (
    'label' => 'COUNTERPARTY_SELLER_SIGNEE_AUTORIZED_BY',
    'type' => 'text',
  ),
  'counterparty_buyer_full_legal_company_name' => 
  array (
    'label' => 'COUNTERPARTY_BUYER_FULL_LEGAL_COMPANY_NAME',
    'type' => 'text',
  ),
  'counterparty_buyer_registered_address' => 
  array (
    'label' => 'COUNTERPARTY_BUYER_REGISTERED_ADDRESS',
    'type' => 'text',
  ),
  'counterparty_buyer_regulatory_body' => 
  array (
    'label' => 'COUNTERPARTY_BUYER_REGULATORY_BODY',
    'type' => 'text',
  ),
  'counterparty_buyer_physical_addr' => 
  array (
    'label' => 'COUNTERPARTY_BUYER_PHYSICAL_ADDR',
    'type' => 'text',
  ),
  'counterparty_buyer_phone' => 
  array (
    'label' => 'COUNTERPARTY_BUYER_PHONE',
    'type' => 'text',
  ),
  'counterparty_buyer_fax' => 
  array (
    'label' => 'COUNTERPARTY_BUYER_FAX',
    'type' => 'text',
  ),
  'counterparty_buyer_email' => 
  array (
    'label' => 'COUNTERPARTY_BUYER_EMAIL',
    'type' => 'text',
  ),
  'counterparty_buyer_establishment_date' => 
  array (
    'label' => 'COUNTERPARTY_BUYER_ESTABLISHMENT_DATE',
    'type' => 'text',
  ),
  'counterparty_buyer_business_line' => 
  array (
    'label' => 'COUNTERPARTY_BUYER_BUSINESS_LINE',
    'type' => 'text',
  ),
  'counterparty_buyer_contact_name' => 
  array (
    'label' => 'COUNTERPARTY_BUYER_CONTACT_NAME',
    'type' => 'text',
  ),
  'counterparty_buyer_contact_phone' => 
  array (
    'label' => 'COUNTERPARTY_BUYER_CONTACT_PHONE',
    'type' => 'text',
  ),
  'counterparty_buyer_contact_fax' => 
  array (
    'label' => 'COUNTERPARTY_BUYER_CONTACT_FAX',
    'type' => 'text',
  ),
  'counterparty_buyer_contact_email' => 
  array (
    'label' => 'COUNTERPARTY_BUYER_CONTACT_EMAIL',
    'type' => 'text',
  ),
  'counterparty_buyer_signee_name' => 
  array (
    'label' => 'COUNTERPARTY_BUYER_SIGNEE_NAME',
    'type' => 'text',
  ),
  'counterparty_buyer_signee_phone' => 
  array (
    'label' => 'COUNTERPARTY_BUYER_SIGNEE_PHONE',
    'type' => 'text',
  ),
  'counterparty_buyer_signee_fax' => 
  array (
    'label' => 'COUNTERPARTY_BUYER_SIGNEE_FAX',
    'type' => 'text',
  ),
  'counterparty_buyer_signee_email' => 
  array (
    'label' => 'COUNTERPARTY_BUYER_SIGNEE_EMAIL',
    'type' => 'text',
  ),
  'counterparty_buyer_signee_autorized_by' => 
  array (
    'label' => 'COUNTERPARTY_BUYER_SIGNEE_AUTORIZED_BY',
    'type' => 'text',
  ),
  'target_date' => 
  array (
    'label' => 'TARGET_DATE',
    'type' => 'date',
  ),
  'target_sum' => 
  array (
    'label' => 'TARGET_SUM',
    'type' => 'text',
  ),
  'rates' => 
  array (
    'label' => 'RATES',
    'type' => 'text',
  ),
);
