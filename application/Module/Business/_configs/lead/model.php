<?php
return array (
  'name' => 
  array (
    'label' => 'NAME',
    'type' => 'text',
  ),
  'description' => 
  array (
    'label' => 'DESCRIPTION',
    'type' => 'textarea',
  ),
  'identifier' => 
  array (
    'label' => 'IDENTIFIER',
    'type' => 'text',
  ),
  'status' => 
  array (
    'label' => 'STATUS',
    'type' => 'text',
  ),
  'date' => 
  array (
    'label' => 'DATE',
    'type' => 'date',
  ),
  'master_target' => 
  array (
    'label' => 'MASTER_TARGET',
    'type' => 'text',
  ),
);
