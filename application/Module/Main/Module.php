<?php

namespace Application\Module\Main;

use Sl_Module_Abstract as AbstractModule;

class Module extends AbstractModule {

    public function getListeners() {
        return array(
            array('listener' => new Listener\Home($this), 'order' => 90),
            array('listener' => new Listener\Leftnav($this), 'order' => 91),
            array('listener' => new Listener\Titlebuttons($this), 'order' => 92),
            array('listener' => new Listener\Calendaricon($this), 'order'=> 93),
            array('listener' => new Listener\Createcontext($this), 'order' => 96),
            array('listener' => new Listener\Addnewlistbuttons($this), 'order' =>97),
            new Listener\Customstyles($this),
            new Listener\Users($this),
            new Listener\Cron($this),
            array('listener' => new Listener\Tasklistview($this), 'order' => 30)
            
        );
    }

}