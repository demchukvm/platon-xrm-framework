<?php

namespace Application\Module\Main\Listener;

class Calendaricon extends \Sl_Listener_Abstract implements \Sl\Listener\View\Aftertitle, \Sl_Listener_Router_Interface {

    protected $_module;
    protected $_controller;
    protected $_action;
    protected $_calendar_url = '/crm/main/calendar';

    public function onAftertitle(\Sl_Event_View $event) {
        if ((in_array($this->_controller, array('deal', 'product'))) && (in_array($this->_action, array('edit', 'detailed')))) {
    
            $tag_id = $event->getView()->object_tag_id;
            if ($tag_id) {
                $calendar_button = new \Sl\View\Control\Simple(array(
                    'pull_right' => false,
                    'class' => 'fa fa-calendar fa-lg custom_calendar',
                    'attribs' => array(
                      // 'tag_id' => $tag_id,
                        'data-rel' => '/crm/main/calendar/tag_id/'.$tag_id.'')
                ));
            }
            echo $calendar_button;
        }
    }

    public function onAfterInit(\Sl_Event_Router $event) {
        
    }

    public function onBeforeInit(\Sl_Event_Router $event) {
        
    }

    public function onDispatchLoopShutdown(\Sl_Event_Router $event) {
        
    }

    public function onDispatchLoopStartup(\Sl_Event_Router $event) {
        
    }

    public function onGetRequest(\Sl_Event_Router $event) {
        
    }

    public function onGetResponse(\Sl_Event_Router $event) {
        
    }

    public function onPostDispatch(\Sl_Event_Router $event) {
        
    }

    public function onPreDispatch(\Sl_Event_Router $event) {
        
    }

    public function onRouteShutdown(\Sl_Event_Router $event) {

        $this->_module = $event->getRequest()->getModuleName();
        $this->_controller = $event->getRequest()->getControllerName();
        $this->_action = $event->getRequest()->getActionName();
    }

    public function onRouteStartup(\Sl_Event_Router $event) {
        
    }

    public function onSetRequest(\Sl_Event_Router $event) {
        
    }

    public function onSetResponse(\Sl_Event_Router $event) {
        
    }

}

