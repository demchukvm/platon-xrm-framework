<?

namespace Application\Module\Main\Listener;

class Users extends \Sl_Listener_Abstract implements \Sl_Listener_Model_Interface {
    protected $_colors = array('233445','3fcf7f', 'f4c414', 'ff5f5f', '5191d1', '13c4a5', 'bac3cc');
    public function onBeforeSave(\Sl_Event_Model $event) {
        $model = $event->getModel();
        if ($model instanceof \Sl\Module\Auth\Model\User && !strlen($model->getColor())) {
            $model->setColor($this->_colors[array_rand($this->_colors)]);
        } elseif ($model instanceof \Sl\Module\Home\Model\file && !$model->getId() && \Sl_Service_Acl::isAutorized()) {
            //fileuploader
            $model->assignRelated('fileuploader', array(\Sl_Service_Acl::getCurrentUser()));
        }
    }

    function onAfterSave(\Sl_Event_Model $event) {
            
        
    }

}
