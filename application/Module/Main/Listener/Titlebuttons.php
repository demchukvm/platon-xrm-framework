<?php

namespace Application\Module\Main\Listener;

class Titlebuttons extends \Sl_Listener_Abstract implements \Sl\Listener\View\Aftertitle, \Sl_Listener_Router_Interface {

    protected $_module;
    protected $_controller;
    protected $_action;

    public function onAftertitle(\Sl_Event_View $event) {

   /*     if ($this->_action == 'list') {

            $resource = \Sl_Service_Acl::joinResourceName(array(
                        'type' => \Sl_Service_Acl::RES_TYPE_MVC,
                        'module' => $this->_module,
                        'controller' => $this->_controller,
                        'action' => \Sl\Service\Helper::AJAX_CREATE_ACTION,
            ));
            if (\Sl_Service_Acl::isAllowed($resource, \Sl_Service_Acl::PRIVELEGE_ACCESS)) {
                $create_button = new \Sl\View\Control\Simple(array(
                    'pull_right' => false,
                    'label' => 'Add new',
                    'class' => 'btn btn-success title_button',
                    'attribs' => array(
                        'id' => 'createbaseobject',
                        'data-content' => "<span id='form-create' class='fa fa-spinner' data-alias='" . \Sl\Service\Helper::getModelAlias($this->_controller, $this->_module) . "' data-rel ='" . $event->getView()->url(array(
                            'module' => $this->_module,
                            'controller' => $this->_controller,
                            'action' => \Sl\Service\Helper::AJAX_CREATE_ACTION,
                        )) . "'</span>",
                        'data-original-title' => "<button type='button' class='close pull-right' data-dismiss='popover'><i class='fa fa-times'></i></button>" . $event->getView()->translate('Create'),
                        'data-toggle' => 'popover',
                        'rel' => 'popover',
                        'data-html' => 'true',
                        'data-placement' => 'bottom',
                    ),
                ));

                echo $create_button;
            }
        }*/
     /*   if (($event->getView()->object instanceof \Application\Module\Business\Model\Product) && ($this->_action == 'detailed')) {

         
            $ajaxcreatedeal = \Sl_Service_Acl::joinResourceName(array(
                        'type' => \Sl_Service_Acl::RES_TYPE_MVC,
                        'module' => 'business',
                        'controller' => 'deal',
                        'action' => \Sl\Service\Helper::AJAX_CREATE_ACTION,
            ));

            if (\Sl_Service_Acl::isAllowed($ajaxcreatedeal, \Sl_Service_Acl::PRIVELEGE_ACCESS)) {

                $data_content = "<span id='form-create' class='fa fa-spinner' data-alias='" . \Sl\Service\Helper::getModelAlias('deal', 'business') . "' data-rel ='" . $event->getView()->url(array(
                            'module' => 'business',
                            'controller' => 'deal',
                            'action' => \Sl\Service\Helper::AJAX_CREATE_ACTION,
                        )) . "/set_relation/productdeal/relation_values/" . $event->getView()->object->getId() . "'</span>";

                $create_button = new \Sl\View\Control\Simple(array(
                    'pull_right' => false,
                    'class' => 'btn btn-circle btn-sm fa fa-plus-circle',
                    'attribs' => array(
                        'data-content' => $data_content,
                        'data-original-title' => "<button type='button' class='close pull-right' data-dismiss='popover'><i class='fa fa-times'></i></button>" . $event->getView()->translate('Create'),
                        'id' => 'create_deal_prod',
                        'data-toggle' => 'popover',
                        'rel' => 'popover',
                        'data-html' => 'true',
                        'data-placement' => 'bottom',
                        'data-rel' => '/business/deal/create/prod_id/' . $event->getView()->object->getId() . '',
                        'prod_name' => $event->getView()->object->__toString(),
                    ),
                ));

                echo $create_button;
            }
        }*/
    }

    public function onAfterInit(\Sl_Event_Router $event) {
        
    }

    public function onBeforeInit(\Sl_Event_Router $event) {
        
    }

    public function onDispatchLoopShutdown(\Sl_Event_Router $event) {
        
    }

    public function onDispatchLoopStartup(\Sl_Event_Router $event) {
        
    }

    public function onGetRequest(\Sl_Event_Router $event) {
        
    }

    public function onGetResponse(\Sl_Event_Router $event) {
        
    }

    public function onPostDispatch(\Sl_Event_Router $event) {
        
    }

    public function onPreDispatch(\Sl_Event_Router $event) {
        
    }

    public function onRouteShutdown(\Sl_Event_Router $event) {

        $this->_module = $event->getRequest()->getModuleName();
        $this->_controller = $event->getRequest()->getControllerName();
        $this->_action = $event->getRequest()->getActionName();
    }

    public function onRouteStartup(\Sl_Event_Router $event) {
        
    }

    public function onSetRequest(\Sl_Event_Router $event) {
        
    }

    public function onSetResponse(\Sl_Event_Router $event) {
        
    }

}

