<?php

namespace Application\Module\Main\Listener;

use \Sl\View\Control as Section;

class Leftnav extends \Sl_Listener_Abstract implements \Sl\Module\Menu\Listener\Leftnav, \Sl_Listener_View_Interface {

    public function onAfterContent(\Sl_Event_View $event) {
     
        
    }

    public function onBeforeContent(\Sl_Event_View $event) {
        
    }

    public function onBeforePageHeader(\Sl_Event_View $event) {
        
    }

    public function onBodyBegin(\Sl_Event_View $event) {
        
    }

    public function onBodyEnd(\Sl_Event_View $event) {
        
    }

    public function onContent(\Sl_Event_View $event) {
        
    }

    public function onFooter(\Sl_Event_View $event) {
        
    }

    public function onHeadLink(\Sl_Event_View $event) {
        
    }

    public function onHeadScript(\Sl_Event_View $event) {
        if (\Sl_Service_Acl::isAutorized()) { 
            $event->getView()->headScript()->appendFile('/home/main/notifications.js');
        }
    }

    public function onHeadTitle(\Sl_Event_View $event) {
        
    }

    public function onHeader(\Sl_Event_View $event) {
        
    }

    public function onLeftNavPrepare(\Sl\Module\Menu\Event\Leftnav $event) {

        
        if (\Sl_Service_Acl::isAutorized()) {
            $sections = $event->getSections();
         

            $event->setSections($sections);
        }
        
    }

    public function onLogo(\Sl_Event_View $event) {
        
    }

    public function onNav(\Sl_Event_View $event) {
        if (\Sl_Service_Acl::isAutorized()) {
            
            try {
                
                echo $event->getView()->partial('notification/nav.phtml');
                
            } catch (\Exception $e) {
                
               
                
            }
            
        }
        
    }

    public function onPageOptions(\Sl_Event_View $event) {
        
    }

}