<?php

namespace Application\Module\Main\Listener;

use Sl_Model_Factory as ModelFactory;

class Cron extends \Sl_Listener_Abstract implements \Sl\Listener\Cron {
    
    /**
     *
     * @var \Sl_View
     */
    protected $_view;
    
    const EMAILS_COUNT = 20;
    
    public function onRun(\Sl\Event\Cron $event) {

        $today = new \DateTime ();

        if (intval($today->format('H')) > 9) {
            $today->setTime(0, 0, 0);
            $yesterday = clone $today;
            $yesterday->sub(new \DateInterval('P1D'));
            
            $value = \Sl_Service_Variables::value('last_milestone_check', false);

            if (!($value instanceof \DateTime) || ($value <= $yesterday)) {
                //Everyday at 9 o'clock
                //check milestones
                $mu_rel = 'milestoneuser';
                $milestones = \Sl_Model_Factory::mapper('milestone', 'crm')->fetchByRelation($mu_rel, 'status < 100 && start < \'' . $today->format(\Sl_Model_Abstract::FORMAT_DATE) . '\'');
                
                $users_ms = array();
                foreach ($milestones as $milestone) {
                    // Нужно определить состояние времени mileslote-а в процентах
                    $percentage = $this->_calcPercentage($milestone, $today);
                    $percentage_yesterday = $this->_calcPercentage($milestone, $yesterday);
                    $level = false;
                    if($percentage_yesterday < 95 && $percentage >= 95) {
                        $level = 95;
                    } elseif($percentage_yesterday < 90 && $percentage >= 90) {
                        $level = 90;
                    } elseif($percentage_yesterday < 80 && $percentage >= 80) {
                        $level = 80;
                    } elseif($percentage > 100 && $milestone->getStatus() < 100) {
                        $level = 100;
                    }
                    if(!$level) {
                        continue;
                    }
                    
                    $milestone = \Sl_Model_Factory::mapper($milestone)->findRelation($milestone, $mu_rel);
                    $link = null;
                    $milestone = \Sl_Model_Factory::mapper($milestone)->findRelation($milestone, 'milestonemaintag');
                    if($tag = $milestone->fetchOneRelated('milestonemaintag')) {
                        $tag = \Sl_Model_Factory::mapper($tag)->findRelation($tag, $tag->getMasterRelation());
                        $link = $tag->fetchOneRelated($tag->getMasterRelation());
                    }
                    $user = $milestone->fetchOneRelated($mu_rel);
                    if ($user instanceof \Sl_Model_Abstract) {
                        if (!isset($users_ms[$user->getId()])) {
                            $users_ms[$user->getId()] = array('user' => $user, 'ms' => array());
                        }
                        $users_ms[$user->getId()]['ms'][$milestone->getId()] = array(
                            'object' => $milestone,
                            'link' => $link,
                            'name' => $milestone->__toString(),
                            'level' => $level,
                        );
                    }
                }
                $tr = $this->getTranslator();
                foreach ($users_ms as $arr) {
                    $notification = \Sl_Model_Factory::object('notification', 'home');
                    
                    $message = $tr->translate('Some milestomes needs your attention').':'."\r\n";
                    foreach($arr['ms'] as $ms) {
                        if($ms['level'] != 100) {
                            $message .= "\r\n".$ms['name'].': '.vsprintf($tr->translate(' (reached %s%%)'), array($level));
                        } else {
                            $message .= "\r\n".$ms['name'].': '.$tr->translate(' (overdue)');
                        }
                    }
                    $message = $this->_getView()->partial('partials/emails/milestones.phtml', array(
                        'milestones' => $arr['ms'],
                        'user' => $arr['user'],
                    ));
                    $notification->setStatus(0)
                            ->setName($tr->translate('Do You remember?'))
                            ->setDone(0)
                            ->setData($message)
                            ->assignRelated('notificationuser', array($arr['user']));
                    \Sl_Model_Factory::mapper($notification)->save($notification);
                }
                \Sl_Service_Variables::set('last_milestone_check', $today);
            }
        }
        // Send notifications
        $notification = \Sl_Model_Factory::object('notification', 'home');
        $ns = \Sl_Model_Factory::mapper($notification)->fetchAll('(status = 0 OR status is null) AND done = 0', null, self::EMAILS_COUNT);
        $rel_name = 'notificationuser';
        
        foreach($ns as $notification) {
            if(!$notification->issetRelated($rel_name)) {
                $notification = ModelFactory::mapper($notification)->findRelation($notification, $rel_name);
            }
            $notify_user = $notification->fetchOneRelated($rel_name);
            if($notify_user && $notify_user->getId() && \Zend_Validate::is($notify_user->getEmail(), 'EmailAddress')) {
                try {
                    $mail = new \Zend_Mail('UTF-8');
                    $mail->addTo($notify_user->getEmail(), $notify_user->getName());

                    if ($copy_email = \Sl_Service_Settings::value('COPY_USERS_EMAIL')) {
                        $mail->addBcc($copy_email);
                    }
                    if(trim($notification->getName()) && trim($notification->getData())) {
                        $mail->setSubject($notification->getName());
                        $mail->setBodyHtml($notification->getData());
                        $mail->send();
                    }
                    
                    ModelFactory::mapper($notification)->save($notification->setDone(1));
                } catch (\Exception $e) {
                    
                }
            } else {
                ModelFactory::mapper($notification)->save($notification->setDone(2));
            }
        }
    }
    
    protected function _calcPercentage(\Sl\Module\Crm\Model\Milestone $milestone, \DateTime $date = null) {
        if(is_null($date)) {
            $date = new \DateTime();
        }
        return ceil((($date->format('U') - $milestone->getStart(true)->format('U'))/($milestone->getEnd(true)->format('U') - $milestone->getStart(true)->format('U')))*100);
    }
    
    /**
     * Возвращает view
     * 
     * @return \Sl_View
     */
    protected function _getView() {
        if(!isset($this->_view)) {
            // @TODO: Вынести создание в фабрику
            $view = new \Sl_View();
            $view->addScriptPath(\Sl_Module_Manager::getViewDirectory($this->getModule()->getName()));
            $view->addHelperPath('../library/Sl/View/Helper', '\\Sl\View\\Helper\\');
            $this->_view = $view;
        }
        return $this->_view;
    }
}
