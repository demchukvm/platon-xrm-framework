<?php

namespace Application\Module\Main\Listener;

class Createcontext extends \Sl_Listener_Abstract implements \Sl_Listener_Router_Interface, \Sl_Listener_View_Interface {

    protected $_module;
    protected $_controller;
    protected $_action;

    public function onAfterInit(\Sl_Event_Router $event) {
        
    }

    public function onBeforeInit(\Sl_Event_Router $event) {
        
    }

    public function onDispatchLoopShutdown(\Sl_Event_Router $event) {
        
    }

    public function onDispatchLoopStartup(\Sl_Event_Router $event) {
        
    }

    public function onGetRequest(\Sl_Event_Router $event) {
        
    }

    public function onGetResponse(\Sl_Event_Router $event) {
        
    }

    public function onPostDispatch(\Sl_Event_Router $event) {
        
    }

    public function onPreDispatch(\Sl_Event_Router $event) {
        
    }

    public function onRouteShutdown(\Sl_Event_Router $event) {

        $this->_module = $event->getRequest()->getModuleName();
        $this->_controller = $event->getRequest()->getControllerName();
        $this->_action = $event->getRequest()->getActionName();
    }

    public function onRouteStartup(\Sl_Event_Router $event) {
        
    }

    public function onSetRequest(\Sl_Event_Router $event) {
        
    }

    public function onSetResponse(\Sl_Event_Router $event) {
        
    }

    public function onAfterContent(\Sl_Event_View $event) {
        
    }

    public function onBeforeContent(\Sl_Event_View $event) {
        
    }

    public function onBeforePageHeader(\Sl_Event_View $event) {
        
    }

    public function onBodyBegin(\Sl_Event_View $event) {
        
    }

    public function onBodyEnd(\Sl_Event_View $event) {
        
    }

    public function onContent(\Sl_Event_View $event) {

        $context = array();
        if (in_array($this->_action, array('edit', 'detailed'))) {

            switch ($this->_controller) {
                case 'deal'    : 
                    
                    $context['crm.task'] = array(
                        'create_url' => '/crm/task/ajaxcreate/set_relation/taskmaintag/relation_values/' . $event->getView()->object_tag_id,
                    ); break;
                case 'product' :
                    $context['obj_name'] = $event->getView()->object->__toString();
                    $context['obj_id'] = $event->getView()->object->getId();
                    $context['business.deal'] = array(
                        'create_url' => '/business/deal/ajaxcreate/set_relation/productdeal/relation_values/' . $event->getView()->object->getId(),
                        'goto_url' => '/business/deal/create/prod_id/' . $event->getView()->object->getId(),
                    );
                    $context['business.subproduct'] = array(
                        'create_url' => '/business/subproduct/ajaxcreate/set_relation/productsubproduct/relation_values/' . $event->getView()->object->getId(),
                    );
                    $context['crm.task'] = array(
                        'create_url' => '/crm/task/ajaxcreate/set_relation/taskmaintag/relation_values/' . $event->getView()->object_tag_id,
                    );
                    
            }
        }
        ?>


        <script>
            var create_context = {<?
        foreach ($context as $key => $value) {
            if (!is_array($value)) {
                ?>'<?= $key ?>':'<?= $value ?>',<? } else {
                ?>'<?= $key ?>':{ <? foreach ($value as $k => $v) { ?>
                        '<?= $k ?>':'<?= $v ?>', <? } ?> },
            <? } ?>
        <? } ?>
            }
        </script>
        <?
    }

    public function onFooter(\Sl_Event_View $event) {
        
    }

    public function onHeadLink(\Sl_Event_View $event) {
        
    }

    public function onHeadScript(\Sl_Event_View $event) {
        
    }

    public function onHeadTitle(\Sl_Event_View $event) {
        
    }

    public function onHeader(\Sl_Event_View $event) {
        
    }

    public function onLogo(\Sl_Event_View $event) {
        
    }

    public function onNav(\Sl_Event_View $event) {
        
    }

    public function onPageOptions(\Sl_Event_View $event) {
        
    }

}

