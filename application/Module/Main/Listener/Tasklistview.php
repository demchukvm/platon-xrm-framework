<?php
namespace Application\Module\Main\Listener;

use Sl_Model_Factory as ModelFactory;

class Tasklistview extends \Sl_Listener_Abstract implements \Sl\Listener\Fieldset, \Sl\Listener\Dataset, \Sl_Listener_Router_Interface, \Sl\Listener\Dispatcher {

    
    protected $_s;
    protected $_ajaxedit;
    protected $_fields;
    protected $_tm_fields = array(
                'checkbox' => array('label' => '',
                    'roles' => array(
                       'render')
                    ),
                'id' => array('label' => '',
                    'roles' => array(
                       'from')
                    ),   
                'name' => array('label'=>'Name', 'roles' => array('from', 'render')),    
                'description' => array(
                      'roles' => array(
                        'from')
                 ),
                'milestonetask.name' => array(
                     'label'=>'Milestome', 'roles' => array('from', 'render')
                 ),
                'taskuser.name' => array(
                    'label' => 'Assignee',
                    'roles' => array('from', 'render')
                ),
                'date_finish' => array(
                      'roles' => array(
                        'from')
                 ), 
                'edit' => array('label' => '',
                    'roles' => array(
                        'render')
                    )
                );
    
    protected $_fp_fields = array(
                'checkbox' => array('label' => '',
                    'roles' => array(
                       'render')
                    ),
                'id' => array('label' => '',
                    'roles' => array(
                       'from')
                    ),
                'date_start' => array(
                    'label' => '',
                    'roles' => array('from'),
                ),
                'taskuser.name' => array(
                    'label' => array(),
                    'roles' => array('from'),
                ),
                'taskmaintag.name' => array(
                    'label' => array(),
                    'roles' => array('from'),
                ),
                'tasktag.name' => array(
                    'label' => array(),
                    'grouped' => true,
                    'roles' => array('from'),
                ),
                'name' => array('label'=>'Name', 'roles' => array('from', 'render')),    
                'description' => array(
                      'roles' => array(
                        'from')
                 ),
                'milestonetask.name' => array(
                     'label'=>'Milestome', 'roles' => array('from', 'render')
                 ),
                'date_finish' => array(
                      'roles' => array(
                        'from')
                 ), 
                'edit' => array('label' => '',
                    'roles' => array(
                        'render')
                    )
                );
    
    public function onAfterProcessItem(\Sl\Event\Dataset $event) {

        $item = $event->getItem();
  
        if ($event->getModel() instanceof \Sl\Module\Crm\Model\Task) {
            
            if ($this->_s)
                $item['checkbox'] = '<input class="task-manual-status" type="checkbox" value="'.$item['id'].'">';
            
            if ($this->_ajaxedit) {
                $item['edit'] = '<i class="fa fa-angle-right edit-task" data-toggle="1" data-content="<span id=\'form-edit\' class=\'fa fa-spinner\' data-alias=\'crm.task\' data-rel=\'/crm/task/ajaxedit/id/'.$item['id'].'\'> </span>" ></i>';
            }
            
            $event->setItem($item);
           
        }
    }

    public function onPrepare(\Sl\Event\Fieldset $event) {
        
        if ($this->_s && $event->getModel() instanceof \Sl\Module\Crm\Model\Task && count($this->_fields)) {
            
            if (!$this->_ajaxedit) unset($this->_fields['edit']);
            
            $addition_fields =  $this->_fields;
            $field_names = array_keys($this->_fields);
            
            $curr_fields = $event->getFieldset()->getFields(); 
            
            
            foreach($curr_fields as $field){
                if (!in_array($field -> getName(),$field_names)) {
                    $event->getFieldset()->getField($field -> getName())->setRoles(array());
                }
            }
            
            foreach($addition_fields as $field => $addition_field){
                if (!$event->getFieldset()->hasField($field)) {
                    $cur_field = $event->getFieldset()->createField($field);
                } else {
                    $cur_field = $event->getFieldset()->getField($field);
                }
                $cur_field->fill($addition_field);
            }
            
            $event->getFieldset()->moveField('checkbox','first');
            if ($this->_ajaxedit) $event->getFieldset()->moveField('edit','last');
            
        }
    }
    
    public function onBeforeDispatch(\Sl\Event\Dispatcher $event) {
        $request = $event->getRequest();
        if($request->getParam('module', false) == 'crm' &&
                $request->getParam('controller', false) == 'task') {


            $this->_fields = ($request->getParam('subaction', false) == 'tm') ? $this->_tm_fields : $this->_fp_fields;
            $this->_s = \Sl_Service_Acl::isAllowed(
                            \Sl_Service_Acl::joinResourceName(array(
                                'type' => \Sl_Service_Acl::RES_TYPE_MVC,
                                'module' => 'crm',
                                'controller' => 'task',
                                'action' => 'ajaxopenclose'
                            )), \Sl_Service_Acl::PRIVELEGE_ACCESS
            );

            $this->_ajaxedit = \Sl_Service_Acl::isAllowed(
                            \Sl_Service_Acl::joinResourceName(array(
                                'type' => \Sl_Service_Acl::RES_TYPE_MVC,
                                'module' => 'crm',
                                'controller' => 'task',
                                'action' => 'ajaxedit'
                            )), \Sl_Service_Acl::PRIVELEGE_ACCESS
            );
            \Sl\Module\Home\Service\Quickforms::add(ModelFactory::model('task', 'crm'));
        }
    }
    
    public function onAfterProcessAll(\Sl\Event\Dataset $event) {}
    public function onBeforeProcessAll(\Sl\Event\Dataset $event) {}
    public function onBeforeProcessItem(\Sl\Event\Dataset $event) {}
    public function onPrepareAjax(\Sl\Event\Fieldset $event) {}
    public function onAfterInit(\Sl_Event_Router $event) {}
    public function onBeforeInit(\Sl_Event_Router $event) {}
    public function onDispatchLoopShutdown(\Sl_Event_Router $event) {}
    public function onDispatchLoopStartup(\Sl_Event_Router $event) {}
    public function onGetRequest(\Sl_Event_Router $event) {}
    public function onGetResponse(\Sl_Event_Router $event) {}
    public function onPostDispatch(\Sl_Event_Router $event) {}
    public function onPreDispatch(\Sl_Event_Router $event) {}
    public function onRouteShutdown(\Sl_Event_Router $event) {}
    public function onRouteStartup(\Sl_Event_Router $event) {}
    public function onSetRequest(\Sl_Event_Router $event) {}
    public function onSetResponse(\Sl_Event_Router $event) {}
    public function onAfterDispatch(\Sl\Event\Dispatcher $event) {}

    
}
