<?php

namespace Application\Module\Main\Listener;

class Customstyles extends \Sl_Listener_Abstract implements \Sl_Listener_View_Interface{
    public function onAfterContent(\Sl_Event_View $event) {
        
    }

    public function onBeforeContent(\Sl_Event_View $event) {
        
    }

    public function onBeforePageHeader(\Sl_Event_View $event) {
        
    }

    public function onBodyBegin(\Sl_Event_View $event) {
        
    }

    public function onBodyEnd(\Sl_Event_View $event) {
        
    }

    public function onContent(\Sl_Event_View $event) {
        
    }

    public function onFooter(\Sl_Event_View $event) {
        
    }

    public function onHeadLink(\Sl_Event_View $event) {
         $event -> getView() -> headLink() -> appendStylesheet('/main/main/customstyles.css');
    }

    public function onHeadScript(\Sl_Event_View $event) {
        if (\Sl_Service_Acl::isAutorized()){
            $event->getView()->headScript()->appendFile('/main/main/customscript.js');
            $u = \Sl_Service_Acl::getCurrentUser();
            $event->getView()->headScript()->appendScript('var current_auth_user = {id:'.$u->getId().', name: "'.$u.'"}');
        }
         
    }

    public function onHeadTitle(\Sl_Event_View $event) {
        
    }

    public function onHeader(\Sl_Event_View $event) {
        
    }

    public function onLogo(\Sl_Event_View $event) {
        
    }

    public function onNav(\Sl_Event_View $event) {
        
    }

    public function onPageOptions(\Sl_Event_View $event) {
        
    }

}