<?php
namespace Application\Module\Main\Listener;

use Sl_Listener_Abstract as AbstractListener;
use Sl_Listener_View_Interface as ViewInterface;
use Sl_Module_Manager as ModuleManager;

class Home extends AbstractListener implements ViewInterface {
    
    public function onAfterContent(\Sl_Event_View $event) {
        
    }

    public function onBeforeContent(\Sl_Event_View $event) {
        $event->stopPropagation();
    }

    public function onBeforePageHeader(\Sl_Event_View $event) {
        
    }

    public function onBodyBegin(\Sl_Event_View $event) {
        $request = \Zend_Controller_Front::getInstance()->getRequest();
        if ($request->getParam('module') == 'home' && $request->getParam('controller') == 'main' && $request->getParam('action') == 'list')
            {
            $event->getView()->title = $event->getView()->translate('Activity Feed');
            $event->getView()->subtitle = '';
        } 
    }

    public function onBodyEnd(\Sl_Event_View $event) {
        $cache_id = md5(get_class($this));
        if(false && \Sl\Service\Cache::test($cache_id)) {
            echo \Sl\Service\Cache::load($cache_id);
        } else {
            $view = new \Sl_View();
            $view->addScriptPath(ModuleManager::getViewDirectory($this->getModule()->getName()));
            $html = $view->partial('partials/templates.phtml');
            \Sl\Service\Cache::save($html, $cache_id);
            echo $html;
        }
    }

    public function onContent(\Sl_Event_View $event) {
        
    }

    public function onFooter(\Sl_Event_View $event) {
        
    }

    public function onHeadLink(\Sl_Event_View $event) {
        
    }

    public function onHeadScript(\Sl_Event_View $event) {
        
    }

    public function onHeadTitle(\Sl_Event_View $event) {
        
    }

    public function onHeader(\Sl_Event_View $event) {
        
    }

    public function onLogo(\Sl_Event_View $event) {
        
    }

    public function onNav(\Sl_Event_View $event) {
        
    }

    public function onPageOptions(\Sl_Event_View $event) {
        
    }

}