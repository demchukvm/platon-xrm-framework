<?php
namespace Application\Module\Main\Modulerelation\Table;

class Notificationuser extends \Sl\Modulerelation\DbTable {
	protected $_name = 'main_notification_user';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Sl\Module\Home\Model\Notification' => array(
			'columns' => 'notification_id',
			'refTableClass' => 'Sl\Module\Home\Model\Table\Notification',
		'refColums' => 'id'),
                		'Sl\Module\Auth\Model\User' => array(
			'columns' => 'user_id',
			'refTableClass' => 'Sl\Module\Auth\Model\Table\User',
				'refColums' => 'id'	),
	);
}