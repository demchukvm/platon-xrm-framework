<?php
namespace Application\Module\Main\Modulerelation\Table;

class Fileuploader extends \Sl\Modulerelation\DbTable {
	protected $_name = 'main_user_file_uploader';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Sl\Module\Auth\Model\User' => array(
			'columns' => 'user_id',
			'refTableClass' => 'Sl\Module\Auth\Model\Table\User',
		'refColums' => 'id'),
                		'Sl\Module\Home\Model\File' => array(
			'columns' => 'file_id',
			'refTableClass' => 'Sl\Module\Home\Model\Table\File',
				'refColums' => 'id'	),
	);
}