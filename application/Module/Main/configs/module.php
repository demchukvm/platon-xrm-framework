<?php
return array (
  'modulerelations' => 
  array (
    0 => 
    array (
      'type' => '21',
      'db_table' => 'Application\\Module\\Main\\Modulerelation\\Table\\Notificationuser',
      'options' => 
      array (
      ),
    ),
    1 => 
    array (
      'type' => '12',
      'db_table' => 'Application\\Module\\Main\\Modulerelation\\Table\\Fileuploader',
      'options' => 
      array (
      ),
    ),
  ),
  'navigation_pages' => 
  array (
  ),
  'left_navigation_pages' => 
  array (
    1 => 
    array (
      'module' => 'crm',
      'controller' => 'main',
      'action' => 'calendar',
      'label' => 'Calendar',
      'id' => 'calendar_button',
      'icon' => 'fa fa-calendar fa-lg',
      'order' => 100,
    ),
    2 => 
    array (
      'href' => '#',
      'label' => 'Others',
      'id' => 'others',
      'icon' => 'fa fa-bars fa-lg',
      'order' => 140,
    ),
    3 => 
    array (
      'parent' => 'others',
      'module' => 'business',
      'controller' => 'subproduct',
      'action' => 'list',
      'label' => 'Products',
      'id' => 'subproduct_button',
      'icon' => '',
      'order' => 110,
    ),
    4 => 
    array (
      'href' => '#',
      'label' => 'Add new',
      'id' => 'add_new',
      'order' => 1,
      'icon' => 'fa fa-plus fa-lg',
    ),
    5 => 
    array (
      'parent' => 'others',
      'module' => 'crm',
      'controller' => 'holiday',
      'action' => 'list',
      'label' => 'Holidays',
      'id' => 'holidays_btn',
      'icon' => '',
      'order' => 190,
    ),
  ),
);
