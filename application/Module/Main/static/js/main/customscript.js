var addMeBtn = function(el) {
    
    $(el).closest('div.control-group').find('label').each(function(){
        if (!$(this).siblings('a').length){
            var a = $('<a/>').attr({href:'#', class:'assign-me'}).html('me').insertAfter(this);
        }
    });
}

$(function() {
    if (undefined != current_auth_user && current_auth_user instanceof Object) {
        
        $('form.form-model.editable input[data-modelname="user"]:not([readonly]):not([disabled])').each(function() {
            addMeBtn(this);
        });
        $(document).on('popover.opened.form', function(e, o) {
            $('form.form-model.editable input[data-modelname="user"]:not([readonly]):not([disabled])', o).each(function() {
                addMeBtn(this);
            });
        });
        $('body').on('click', 'a.assign-me', function(){
            
            $(this).closest('div.control-group').find('input[data-modelname="user"]').attr('data-name',current_auth_user.name).select2('val',current_auth_user.id);
        });
    }
    
    
    $('body').on('click', '.datatable tbody>tr td', function(){
         
         if(!$(this).find('input').length && !$(this).find('[data-toggle]').length && $(this).closest('tr').attr('data-link')) {
                        document.location.href =  $(this).closest('tr').attr('data-link');
                    }
    });
    
    
    $('body').on('change', '.datatable tbody>tr input.task-manual-status', function(){
       
        $(this).closest('tr').toggleClass('archived');
        $.get('/crm/task/ajaxopenclose/',{id:$(this).val()});
        //if ($(this).is(':checked'));
       
    });
    
    $('body').on('tabledraw.sl', 'table.table.datatable', function(e, table){
        var ctrl = $(table).data('controller');
        if(ctrl) {
            var _pop = function(el, opts, initOnly){
                var _options = _.extend({
                    placement: 'top',
                    trigger: '_test',
                    content: '',
                    html: true,
                    title: 'No title',
                    container: ctrl.getWrapper(),
                    autoHide: 2000,
                    canClose: true,
                    after: function(){}
                }, opts || {});

                if(_options.canClose) {
                    _options.title = '<button type="button" class="close pull-right" data-dismiss="popover"><i class="fa fa-times"></i></button>'+_options.title;
                }

                $(el).data('bs.popover', null).popover(_options);
                if(initOnly !== true) {
                    $(el).popover('show');
                }
                if(_options.autoHide) {
                    setTimeout(function(){
                        $(el).popover('destroy');
                    }, _options.autoHide);
                }
                $(el).data('bs.popover').$tip.on('click', function(e){
                    // Крайне странно, но работает
                    e.stopPropagation();
                });
                if(_options.canClose) {
                    $(el).data('bs.popover').$tip.on('click', '.close', function(e){
                        $(el).popover('destroy');
                    });
                }
                if(typeof _options.after === 'function') {
                    _options.after($(el).data('bs.popover'), _options);
                }
            };
            if(ctrl.getWrapper().closest('.tasks-panel').length > 0) {
                new SlForms().checkForms(['crm.task']);
                $('tbody tr', ctrl.getWrapper()).off('dblclick');
                $('tbody tr td', ctrl.getWrapper()).off('click');
                $('tbody tr', ctrl.getWrapper()).each(function(){
                    $(this).removeAttr('data-link');
                });
                
                ctrl.getWrapper().on('dblclick', 'tbody tr', function(e){
                    e.preventDefault();
                    e.stopPropagation();
                    /*if($(this).find('.edit-task').length > 0) {
                        $(this).find('.edit-task').click();
                    }*/
                });
                ctrl.getWrapper().on('click dblclick', 'tbody tr', function(e){
                    e.preventDefault();
                    e.stopPropagation();
                    var $this = $(this);
                    
                    var $div = $('<div style="display: none;"/>').appendTo(ctrl.getWrapper());
                    var detailed = $this.data('raw');
                    
                    var fC = new SlFormController($div, {
                        fields: _.map(new SlForms().getForm('crm.task').getFields(), function(field){
                            var f = field;
                            var path = field.name.split('.');
                            var container = detailed;
                            while(path.length) {
                                var curkey = path.shift();
                                if(container.hasOwnProperty(curkey)) {
                                    container = container[curkey];
                                }
                            }
                            if(container != detailed) {
                                f.value = container;
                            }
                            return f;
                        }),
                        alias: 'crm.task'
                    });
                    _pop($this, {
                        title: 'Task <strong>"'+detailed.name+'"</strong>',
                        placement: 'bottom',
                        content: $div.html(),
                        autoHide: false
                    });
                    $div.remove();
                });
                
            }
        }
        $("[data-toggle=tooltip]", this).tooltip();
        $("[data-toggle=popover]", this).popover(); 
    });
    
    // tasks ajax edit
    $('.tasks-panel, #tm').on('click', '.edit-task', function(){
            
            $('#edit-task-popover').attr('data-content', $(this).data('content'));
            console.log($('#edit-task-popover').attr('data-content'));
            $('#edit-task-popover').click();
            //nobody knows why one cklick somebody don`t work;
            $('div.popover .arrow').css('display', 'none');
            if (!$('div.popover').length){
                $('#edit-task-popover').click();
            }
        
            
    });
    
});

/****  popover create form ****/

$(function() {
    

    
    $(document).on('update.sl', function(e, o) {
        $('div.popover').not(this).remove();
    });
    
    $(document).on('update.sl', function(e, o) {
        var url;
        
        if ($('#form-create', o).length){
            var $o = $('#form-create', o).attr('class', '');
            var alias = $('#form-create', o).data('alias');
            if (create_context) {
                if (create_context.hasOwnProperty(alias)) {

                    url = create_context[alias].create_url;
                    if (create_context[alias].hasOwnProperty('goto_url')) {
                        var $span = $('body').find("span[data-alias='" + alias + "']");
                        if ($span) {
                            $span.attr('data-rel', create_context[alias].goto_url);
                        }

                    }
                } else {
                    url = $('#form-create', o).data('rel');
                }

            }
        }else if($('#form-edit', o).length) {
             url = $('#form-edit', o).data('rel');
             var $o = $('#form-edit', o).attr('class', '');
        }
        
        if (url){
            $.post(url, {}, function(data) {

                
                $o.html(data.form);
                // var autocomplete_field = $o.find('.autocomplete');
                assignAutocomplete($o);

                $o.find('input[type="file"]').fileupload({
                    dataType: 'json',
                    done: function(e, data) {

                    }
                });

                if (data.hasOwnProperty('calc_script'))
                    eval(data.calc_script);
                //   console.log(data.calc_script);

                $(datepicker_selector, $o).datepicker(datepicker_options);
                $(datetimepicker_selector, $o).datetimepicker(datetimepicker_options);
                $('form', $o).attr('calculating', '0');
                $(current_date_selector + '[value=""]', $o).val(today_string);
                $(current_datetime_selector + '[value=""]', $o).val(today_string + ' ' + todaytime_string);
                $(tommorow_date_selector + '[value=""]', $o).val(tommorow_string );
                $('body').trigger('popover.opened.form', $o);
            });
        }

    });


    $(document).on('submit', '.popover-content form', function(event) {
        
        /*
         var returnfields = $('input[name="returnfields"]').val();
         
         var extended_arr = ['url'];
         if (returnfields != undefined && returnfields.length > 0)
         {
         extended_arr[extended_arr.length] = 'fields:' + returnfields;
         }
         */

        var $form = $(this);
        /*  
         var $target = $($form.attr('data-target'));
         $form.append('<input type="hidden" name="data-extended" value="' + extended_arr.join(';') + '" />');
         */
        $('#Save_ajax').hide();
        $.ajax({
            type: $form.attr('method'),
            url: $form.attr('action'),
            data: $form.serialize(),
            success: function(res) {

                if (res.result) {
                    var $popover = $form.closest('.popover').prev();
                    /* var $fw = $form.parent('#form-create');
                    if ($fw && $fw.data('alias')) {
                        var $t = $('div[data-alias="' + $fw.data('alias') + '"] table.datatable');
                        $t.length && $t.dataTable().fnDraw(true);
                    } */
                    $('body').trigger('popover.submit', {form: $form, res: res});
                    $('body').trigger('close.nav.create.section', $('.popover'));
                   // $popover && $popover.popover('hide');
                  $('.popover').remove();
                } else {
                    $('#Save_ajax').show();
                    alertErrors(res.description);
                }
            }
        });
        event.preventDefault();
        return false;
    });
    
     $('body').on('popover.submit', function(e, o){
         console.log([e,o,o.form.next('.table-controls-wrapper:first')])
         ;
         if (undefined != o.form && o.form.closest('[data-alias').length){
             var a = o.form.closest('[data-alias').data('alias');
             $('.table-controls-wrapper[data-alias="'+a+'"]').each(function(){
                 $('table.datatable', this).dataTable().fnDraw(true);
             });
             
         }
     });
    
    $('body').on('click', '.controls i.del_rel_btn', function() {
        var obj_id = $(this).attr('obj_id');
        var $li = $(this).parent('li:first');
        var $id_input = $(this).parents('ul:first').attr('id').replace(/_list/, '');
        var $input = $('#' + $id_input);
        var ids = $input.val().split(';');
        if (ids.indexOf(obj_id) > -1) {
            ids.splice(ids.indexOf(obj_id), 1);
            var id_row = ids.join(';');
            $input.val(id_row);
            $li.remove();
        }
    });

    $('body').on('popover.opened.form', function(event, wrapper) {


    });
    
    $('body').on('close.nav.create.section', function(event, data){
       if ($(data).is('#nav .dropdown-submenu .close')||$(data).is('#nav .dropdown-submenu .popover')) {
            var $li = $(data).parents('#nav li.dropdown-submenu:last');
            $li.removeClass('hover');
        } 
        
        
    });
    


    $('body').on('click', '.popover .close', function() {
        
     $('body').trigger('close.nav.create.section', this);
        
        $('.popover').remove();

    });

    $('body').on('click', '[rel="popover"]', function(e) {

      //  console.log($('.popover'));
        // $('.popover').remove();

        // e.stopPropagation();
        if ($(e.target).is('#nav [rel="popover"]')) {
            var $li = $(e.target).parents('#nav li.dropdown-submenu:last');
            $li.addClass('hover');
        }

   
        
        
        
    





       /* var i = $(this);
       console.log(i);
        var thisPopover = i.parents('li:first').find('.popover');
        //var thisPopover = $('.popoverClose').filter('[data-info-id="' + i.data('info-id') + '"]').closest('.popover');
        console.log(thisPopover);
        $('.popover').each(function() {
            if ($(this) != thisPopover) {
               // $(this).hide();
            }else {
         $(this).popover('show');}
        })*/
        /*if (thisPopover.is(':visible')) {
         $('.popover').remove();
         }
         else {
         $(this).popover('show');
         }*/
    });
    // $('body').on('follow_new_obj',  )

    /*   $(document).on('click', '.popover-title .close', function(e){
     var $target = $(e.target), $popover = $target.closest('.popover').prev();
     $popover && $popover.popover('hide');
     });
     */

});