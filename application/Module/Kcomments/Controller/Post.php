<?php

namespace Application\Module\Kcomments\Controller;

class Post extends \Sl_Controller_Model_Action {

    protected $_default_limit = 3;

            
    const POST_CREATOR_RELATION = 'postcreator';
    const POST_TAG_RELATION = 'tagpost';
    const POST_FILES_RELATION = 'postfiles';
    const FILE_UPLOADER_RELATION = 'fileuploader';
    
    public function ajaxfactsfeedAction(){
        $this->view->result = true;
        try {
            
            $relation_name = $this->getRequest()->getParam('relation', false);
            $model_id = $this->getRequest()->getParam('model_id', false);
            
            $after =$this->getRequest()->getParam('after', false);
            
            
            $limit = 0;
            
            $update = $this->getRequest()->getParam('update', false); 
            if ($update && count($update)){
                
                $u_model = ($update['id'] > 0? \Sl_Model_Factory::mapper('post', $this->_getModule())->find($update['id']): \Sl_Model_Factory::object('post', $this->_getModule()));
                
                if ($u_model instanceof \Sl_Model_Abstract){
                    $u_model->setOptions($update['data']);
                    
                     $u_model = \Sl_Model_Factory::mapper($u_model)->save($u_model, true);
                   
                    
                }
            }
            
            
            
            $post = \Sl_Model_Factory::object('post', $this->_getModule());
            $post_mapper = \Sl_Model_Factory::mapper($post);
            $posts = $post_mapper->fetchFacts($after, $relation_name, $model_id);


            
            
            foreach ($posts as $p) {
                $postids_arr[] = $p->getId();
                $posts_arr[] = $p->toArray() ;
            }
     
            
            
            
            
            $this->view->posts = $posts_arr;
            
        } catch (\Exception $e) {
            $this->view->result = false;
            $this->view->description = $e->getMessage();
            $this->view->trace = $e->getTrace();
        }
    }
    
    public function ajaxfeedAction() {
        $this->view->result = true;
        try {
            
            $relation_name = $this->getRequest()->getParam('relation', false);
            $model_id = $this->getRequest()->getParam('model_id', false);
            
            $get_files = $this->getRequest()->getParam('get_files', false);
            
            $after =$this->getRequest()->getParam('after', false);
            $before =$this->getRequest()->getParam('before', false);
            
            
            
            $post = \Sl_Model_Factory::object('post', $this->_getModule());
            $post_mapper = \Sl_Model_Factory::mapper($post);
            
            if ($root_id = $this->getRequest()->getParam('to_post', false)){
                $root = $post_mapper -> find($root_id);
                if ($root instanceof \Sl_Model_Abstract){
                     $after = $root->getLastUpdate();
                }
            }
            
            $limit = 0;
            !$after && $limit = $this->getRequest()->getParam('limit', $this->_default_limit);
            
            $update = $this->getRequest()->getParam('update', false); 
            if ($update && count($update)){
                
                $u_model = ($update['id'] > 0? \Sl_Model_Factory::mapper($update['model'], $this->_getModule())->find($update['id']): \Sl_Model_Factory::object($update['model'], $this->_getModule()));
                
                if ($u_model instanceof \Sl_Model_Abstract){
                    $u_model->setOptions($update['data']);
                    
                    if ($update['data']['master_tag'] > 0){
                        $tag = \Sl_Model_Factory::mapper('tag',$this->_getModule())->find($update['data']['master_tag']);
                        if ($rel_name = $tag->getMasterRelation()){
                            $rel_obj = $tag->fetchOneRelated($rel_name);
                            
                            if ($post_rel_name  = $this-> _getRelationName($rel_name)){
                                $u_model -> setMasterRelation($post_rel_name)
                                     -> assignRelated($post_rel_name, array($rel_obj));
                            }
                            
                        }
                        
                    }
                    
                    $u_model = \Sl_Model_Factory::mapper($u_model)->save($u_model, true);
                   
                    
                }
            }

            
            $posts = $post_mapper->fetchPosts($after, $before, $limit, $relation_name, $model_id);


            if ($this->getRequest()->getParam('prepare', false)) {


                $permissions = array();

                foreach ($post->toArray() as $field => $value) {
                    $permissions[$field] = \Sl_Service_Acl::isAllowed(array($post, $field), \Sl_Service_Acl::PRIVELEGE_UPDATE);
                }
                $this->view->post_permissions = $permissions;

                $lists = array();
                foreach ($post->ListsAssociations() as $field => $list)
                    $lists[$field] = \Sl\Service\Lists::getList($list);
                $this->view->post_lists = $lists;
                
                
                
                $country_tags = array();
                
                $tags = \Sl_Model_Factory::mapper('tag','kcomments')->fetchAll('master_relation = "maintagcountry"');
                foreach ($tags as $tag){
                    $country_tags[$tag->getId()] = $tag->__toString();
                }
                $this->view->country_tags = $country_tags;
                
            }
            
            if ($get_files && $relation_name && $model_id) {
                    
                    $attachments = $post_mapper->findAttachmentsByRelated($relation_name, $model_id);
                    $files = array();

                    foreach ($attachments as $file){
                        $arr = $file->toArray();
                        
                        !$file->issetRelated(self::FILE_UPLOADER_RELATION) && $file = \Sl_Model_Factory::mapper($file)->findRelation($file, self::FILE_UPLOADER_RELATION);
                        $arr['user'] = $file->issetRelated(self::FILE_UPLOADER_RELATION)? ''.$file->fetchOneRelated(self::FILE_UPLOADER_RELATION):'';
                        $files[] = $arr;
                    }
                    
                    $attachments = \Sl_Model_Factory::mapper('file', 'home')->fetchByRelated($model_id, $get_files);
                    
                    foreach ($attachments as $file){
                        $arr = $file->toArray();
                        !$file->issetRelated(self::FILE_UPLOADER_RELATION) && $file = \Sl_Model_Factory::mapper($file)->findRelation($file, self::FILE_UPLOADER_RELATION);
                        $arr['user'] = $file->issetRelated(self::FILE_UPLOADER_RELATION)? ''.$file->fetchOneRelated(self::FILE_UPLOADER_RELATION):'';
                        $files[] = $arr;
                    }
                    
                    $this->view->files = $files;
            }
            
            
            $posts_arr = array();
            $relation = \Sl_Modulerelation_Manager::getRelations($post, self::POST_CREATOR_RELATION);
            $postids_arr = array();
            
            foreach ($posts as $p) {
                $postids_arr[] = $p->getId();
                $posts_arr[] = $this->_getPostData($p, !(bool)$relation_name);
            }
     
            
            
            
            $comments = array();
            count($postids_arr) && $comments = $post_mapper->fetchAll(array ('root in (?)'=>$postids_arr, ' level > ? ' => '0'),'level');
            
            
            foreach ($comments as $c) {
               $posts_arr[]  = $this->_getPostData($c);
            }
            
            $this->view->posts = $posts_arr;
            
        } catch (\Exception $e) {
            $this->view->result = false;
            $this->view->description = $e->getMessage();
            $this->view->trace = $e->getTrace();
        }
    }
    
    protected function _getPostData(\Application\Module\Kcomments\Model\Post $p, $get_master_relation){
        $post_arr= $p->toArray();
        $postids_arr[] = $p->getId();
        
             
            (!$p->issetRelated(self::POST_CREATOR_RELATION)) &&
                    $p = \Sl_Model_Factory::mapper($p)->findRelation($p, self::POST_CREATOR_RELATION);

            count($p->fetchRelated(self::POST_CREATOR_RELATION)) &&
                    $post_arr[self::POST_CREATOR_RELATION] = $p->fetchOneRelated(self::POST_CREATOR_RELATION)->toArray();
            
            
            (!$p->issetRelated(self::POST_TAG_RELATION)) &&
                    $p = \Sl_Model_Factory::mapper($p)->findRelation($p, self::POST_TAG_RELATION);
            
            
            if (count($p->fetchRelated(self::POST_TAG_RELATION))){
                $tags_arr = array();
                foreach($tags = $p->fetchRelated(self::POST_TAG_RELATION) as $tag){
                    $tags_arr[] = $tag->__toString();
                }
                $post_arr[self::POST_TAG_RELATION] = $tags_arr;
            }
            
            (!$p->issetRelated(self::POST_FILES_RELATION)) &&
                    $p = \Sl_Model_Factory::mapper($p)->findRelation($p, self::POST_FILES_RELATION);
            
            if (count($p->fetchRelated(self::POST_FILES_RELATION))){
                $files_arr = array();
                foreach($files = $p->fetchRelated(self::POST_FILES_RELATION) as $file){
                    $files_arr[] = $file->toArray();
                }
                $post_arr[self::POST_FILES_RELATION] = $files_arr;
            }
            
            
            
            if ($get_master_relation && strlen($p->getMasterRelation())){
                (!$p->issetRelated($p->getMasterRelation())) &&
                    $p = \Sl_Model_Factory::mapper($p)->findRelation($p, $p->getMasterRelation());

                if (count($p->fetchRelated($p->getMasterRelation()))){
                    $post_arr['master_object'] = $p->fetchOneRelated($p->getMasterRelation()).'' ;
                    $post_arr['master_object_url'] = \Sl\Service\Helper::buildModelUrl($p->fetchOneRelated($p->getMasterRelation()), \Sl\Service\Helper::TO_DETAILED_ACTION, array('id'=>$p->fetchOneRelated($p->getMasterRelation())->getId()));
                } 
                        
            }
            
        return $post_arr;        
    }
    
    protected function _getRelationName($rel_name) {
        switch($rel_name){
            case 'maintagproduct': return 'productpost';
            case 'maintagdeal':    return 'dealpost';
                
        }
    }
    
}