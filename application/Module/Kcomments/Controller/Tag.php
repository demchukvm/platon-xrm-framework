<?php
namespace Application\Module\Kcomments\Controller;

class Tag extends \Sl_Controller_Model_Action {
    
    public function listbytagAction() {
        if ($tagId = $this->getRequest()->getParam('id',false)){
            
            $Obj = \Sl_Model_Factory::mapper($this->getModelName(), $this->_getModule()->getName())->find($tagId);
            
            if ($Obj instanceof \Sl_Model_Abstract){
                $relations = \Sl_Modulerelation_Manager::getRelations($Obj);
                $relobjs = array();
                foreach($relations as $relation){
                    if ($relation->getName() == $Obj->getMasterRelation()) continue;
                    $count = \Sl_Model_Factory::mapper($relation->getRelatedObject($Obj))-> getCountByFields (array($relation->getName().'.id = '.$tagId), array($relation));
                    if (!$count) continue;
                    $o_name = $relation->getRelatedObject($Obj)->findModelName();
                    $relobjs[$o_name] = isset($relobjs[$o_name])?$relobjs[$o_name]:array();
                    $relobjs[$o_name][$relation->getName()] = $count;
                }
                
                
                $this->view->object = $Obj;
                
                print_r($relobjs);
                die(count($relobjs).'');
            }
            
        } else {
            $this->_redirect('list');
        }
        
    }
    
    public function ajaxlistbytagAction() {
        
    }
    
}