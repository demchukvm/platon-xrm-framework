<?php

namespace Application\Module\Kcomments\Model\Mapper;

class Post extends \Sl_Model_Mapper_Abstract {

    protected $_custom_mandatory_fields = array('content', 'type',
        'last_update', 'level',
        'root', 'parent', 'is_fact',
        'modulerelation_postcreator',
        'modulerelation_tagpost',
        'modulerelation_productpost',
        'modulerelation_dealpost',
        'modulerelation_postfiles',
        'modulerelation_postcircles');

    protected function _getMappedDomainName() {
        return '\Application\Module\Kcomments\Model\Post';
    }

    protected function _getMappedRealName() {
        return '\Application\Module\Kcomments\Model\Table\Post';
    }

    public function fetchPosts($after, $before, $limit, $relation_name, $model_id) {

        $relation_name && $relation = \Sl_Modulerelation_Manager::getRelations($this->_model(), $relation_name);

        $objects = array();
        $date_where = '';
        
        if (strlen($after) && strlen($before)) {
            
            $date_where = $this->_getDbTable()->info('name') . ".last_update >= '" . $after . "' AND ".$this->_getDbTable()->info('name') . ".last_update < '" . $before . "'";

            $rowset = $this->_getDbTable()
                    ->fetchPosts($date_where, false, $relation, $model_id);
            if (count($rowset)) {
                foreach ($rowset as $row)
                    $objects[] = $this->saveCollection($row);
            }
            
            
        }elseif (strlen($after)) {
            
            $date_where = $this->_getDbTable()->info('name') . ".last_update > '" . $after . "'";

            $rowset = $this->_getDbTable()
                    ->fetchPosts($date_where, 0, $relation, $model_id);
            if (count($rowset)) {
                foreach ($rowset as $row)
                    $objects[] = $this->saveCollection($row);
            }
        } else {

            strlen($before) && $date_where = $this->_getDbTable()->info('name') . ".last_update < '" . $before . "'";
            $rowset = $this->_getDbTable()
                    ->fetchPosts($date_where, $limit, $relation, $model_id);
            if (count($rowset)) {
                foreach ($rowset as $row)
                    $objects[] = $this->saveCollection($row);
            }
        }
        return $objects;
    }

    public function fetchFacts($after, $relation_name, $model_id) {

        $relation_name && $relation = \Sl_Modulerelation_Manager::getRelations($this->_model(), $relation_name);

        $objects = array();
        $date_where = '';
        if (strlen($after)) {

            $date_where = $this->_getDbTable()->info('name') . ".last_update > '" . $after . "'";
        }
        $rowset = $this->_getDbTable()
                ->fetchFacts($date_where, $relation, $model_id);
        if (count($rowset)) {
            foreach ($rowset as $row)
                $objects[] = $this->saveCollection($row);
        }
        return $objects;
    }

    public function findAttachmentsByRelated($relation_name, $model_id, $relations = array()) {
        $objects = array();
        
        $relation_name && $relation = \Sl_Modulerelation_Manager::getRelations($this->_model(), $relation_name);
        $filerelation = \Sl_Modulerelation_Manager::getRelations($this->_model(), 'postfiles');

        if ($relation instanceof \Sl\Modulerelation\Modulerelation && $filerelation instanceof \Sl\Modulerelation\Modulerelation) {

            $ids = $this->_getDbTable() ->fetchAttachmentIds($relation, $filerelation, $model_id);
            
            
            
            if (count($ids)) {
                $objects = \Sl_Model_Factory::mapper('file','home')->fetchAll('id in ('.implode(',',$ids).')');
                if (is_array($relations) && count($relations)){
                    foreach($objects as $key => $object) {
                        foreach($relations as $rel_name){
                            !$object->issetRelated($rel_name) && $object = \Sl_Model_Factory::mapper($object)->findRelation($object, $rel_name);
                        }
                    }
                }
                
            }
        }


        return $objects;
    }

}