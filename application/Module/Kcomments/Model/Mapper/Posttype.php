<?php
namespace Application\Module\Kcomments\Model\Mapper;

class Posttype extends \Sl_Model_Mapper_Abstract {
        
        protected $_custom_mandatory_fields = array('name');
        
        protected function _getMappedDomainName() {
            return '\Application\Module\Kcomments\Model\Posttype';
        }

        protected function _getMappedRealName() {
            return '\Application\Module\Kcomments\Model\Table\Posttype';
        }
}