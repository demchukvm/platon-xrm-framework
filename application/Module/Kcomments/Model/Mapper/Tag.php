<?php
namespace Application\Module\Kcomments\Model\Mapper;

class Tag extends \Sl_Model_Mapper_Abstract{
        
        protected $_custom_mandatory_fields = array('name');
    
        protected function _getMappedDomainName() {
            return '\Application\Module\Kcomments\Model\Tag';
        }

        protected function _getMappedRealName() {
            return '\Application\Module\Kcomments\Model\Table\Tag';
        }
        
   public function __construct(){
       
        $obj = \Sl_Model_Factory::object('tag', 'kcomments');
        $relations = \Sl_Modulerelation_Manager::getRelations($obj);
        foreach($relations as $relation) $this->_custom_mandatory_fields[] = \Sl_Modulerelation_Manager::buildModulerelationAlias ($relation->getName());
        
        parent::__construct();
    }
    
    
   
}