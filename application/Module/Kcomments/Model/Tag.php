<?php
namespace Application\Module\Kcomments\Model;

class Tag extends \Sl_Model_Abstract implements \Sl\Model\Masterrelation {

	protected $_name;
	protected $_master_relation;
	protected $_loged = false;

	public function setName ($name) {
		$this->_name = $name;
		return $this;
	}
	public function setMasterRelation ($master_relation) {
		$this->_master_relation = $master_relation;
		return $this;
	}

	public function getName () {
		return $this->_name;
	}
	public function getMasterRelation () {
		return $this->_master_relation;
	}



}