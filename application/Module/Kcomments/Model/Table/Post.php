<?php
namespace Application\Module\Kcomments\Model\Table;

class Post extends \Sl\Model\DbTable\DbTable {
	protected $_name = 'kcomments_post';
	protected $_primary = 'id';

        
        public function fetchPosts($date_where, $limit, $relation, $model_id){

            $select = $this->getAdapter()->select();
            $select ->from($this->_name);
            
            $post = \Sl_Model_Factory::object($this);
            $where =  array();
            $select -> where ($this->_name.'.active= 1');
            $select -> where ($this->_name.'.level = 0 ');
            
            if ($relation instanceof \Sl\Modulerelation\Modulerelation && $model_id > 0){
                $target = $relation->getRelatedObject($post);
                
                $select = $this->_buildInnerJoin($select, $target, $relation);
                $select  -> where ( $relation->getName().'.id = '.$model_id);
                
            }
            
            
            
            if ($date_where && strlen($date_where)){
                $select -> where ($date_where);
            } 
            
            
            //is_array($not_include) && count($not_include) && $where[] = $this->_name.'.id not in ('.implode(' , ',$not_include).')';
            
            
            $select -> order('last_update desc');
            ($limit > 0) && $select -> limit($limit);
            \Sl_Event_Manager::trigger(new \Sl\Event\Table('beforeQuery', array('query' => $select, 'model' => $post)));

            return $this->getAdapter()->fetchAll($select, array(), \Zend_Db::FETCH_ASSOC);
            
        }
        
        public function fetchFacts($date_where,$relation, $model_id){

            $select = $this->getAdapter()->select();
            $select ->from($this->_name);
            
            $post = \Sl_Model_Factory::object($this);
            $where =  array();
            $select -> where ($this->_name.'.active= 1');
            $select -> where ($this->_name.'.is_fact > 0');
            
            if ($relation instanceof \Sl\Modulerelation\Modulerelation && $model_id > 0){
                $target = $relation->getRelatedObject($post);
                
                $select = $this->_buildInnerJoin($select, $target, $relation);
                $select  -> where ( $relation->getName().'.id = '.$model_id);
                
            }
            
            
            
            if ($date_where && strlen($date_where)){
                $select -> where ($date_where);
            } 
            
            
            //is_array($not_include) && count($not_include) && $where[] = $this->_name.'.id not in ('.implode(' , ',$not_include).')';
            
            
            $select -> order('id desc');
            
            \Sl_Event_Manager::trigger(new \Sl\Event\Table('beforeQuery', array('query' => $select, 'model' => $post)));
            //echo $select; 
            return $this->getAdapter()->fetchAll($select, array(), \Zend_Db::FETCH_ASSOC);
            
        }
        
       public function fetchAttachmentIds(\Sl\Modulerelation\Modulerelation $relation, \Sl\Modulerelation\Modulerelation $filerelation, $model_id){

            $select = $this->getAdapter()->select();
            $select ->from($this->_name, array());
            
            $post = \Sl_Model_Factory::object($this);
            $where =  array();
            $select -> where ($this->_name.'.active= 1');
            
            
            
            $target = $relation->getRelatedObject($post);
                
            $select = $this->_buildInnerJoin($select, $target, $relation);
            $select  -> where ( $relation->getName().'.id = '.$model_id);
                
            $file = $filerelation->getRelatedObject($post);
            
            $select = $this->_buildInnerJoin($select, $file, $filerelation, array('id'));
            
            
            
            
            
            //is_array($not_include) && count($not_include) && $where[] = $this->_name.'.id not in ('.implode(' , ',$not_include).')';
            
            
            $select -> order('last_update desc');
            
            \Sl_Event_Manager::trigger(new \Sl\Event\Table('beforeQuery', array('query' => $select, 'model' => $post)));
            //echo $select; 
            
            
            return $this->getAdapter()->fetchAll($select, array(), \Zend_Db::FETCH_COLUMN);
            
        }
        
        
        
}