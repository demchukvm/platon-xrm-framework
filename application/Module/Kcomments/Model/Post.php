<?php
namespace Application\Module\Kcomments\Model;

class Post extends \Sl_Model_Abstract implements \Sl\Model\Masterrelation{

	protected $_content; 
protected $_is_fact;

 
        protected $_parent;
	protected $_root;
	protected $_level;
        
        protected $_master_relation;
 
        protected $_last_update;


	protected $_type;
	protected $_lists = array(
            'type' => 'list_post_types'
        );

	
        

	public function setIsFact ($is_fact) {
		$this->_is_fact = $is_fact;
		return $this;
	}

	public function setMasterRelation($master_relation) {
            $this->_master_relation = $master_relation;
            return $this;
        }
	

	public function setParent ($parent) {
		$this->_parent = $parent;
		return $this;
	}
	public function setRoot ($root) {
		$this->_root = $root;
		return $this;
	}
	public function setLevel ($level) {
		$this->_level = $level;
		return $this;
	}

	public function updateLastUpdate(){
            $this->setLastUpdate(new \DateTime);
        }
        
	public function setLastUpdate($last_update) {
		if($last_update instanceof \DateTime) {
			$last_update = $last_update->format(self::FORMAT_TIMESTAMP);
		}
		$this->_last_update = $last_update;
		return $this;
	}

	public function setContent ($content) {
		$this->_content = $content;
		return $this;
	}
	public function setType ($type) {
		$this->_type = $type;
		return $this;
	}

	public function getContent () {
		return $this->_content;
	}
	public function getType () {
		return $this->_type;
	}



	
	public function getLastUpdate($as_object = false) {
		if($as_object) {
			return \DateTime::createFromFormat(self::FORMAT_, $this->getLastUpdate());
		}
		return $this->_last_update;
	}
	public function getParent () {
		return $this->_parent;
	}
	public function getRoot () {
		return $this->_root;
	}
	public function getLevel () {
		return $this->_level;
	}

    public function getMasterRelation() {
        return $this->_master_relation;
    }

    
	public function getIsFact () {
		return $this->_is_fact;
	}
}