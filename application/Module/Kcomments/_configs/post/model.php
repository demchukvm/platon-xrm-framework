<?php
return array (
  'content' => 
  array (
    'label' => 'CONTENT',
    'type' => 'text',
  ),
  'type' => 
  array (
    'label' => 'TYPE',
    'type' => 'text',
  ),
  'last_update' => 
  array (
    'label' => 'LAST_UPDATE',
    'type' => 'timestamp',
  ),
  'parent' => 
  array (
    'label' => 'PARENT',
    'type' => 'text',
  ),
  'root' => 
  array (
    'label' => 'ROOT',
    'type' => 'text',
  ),
  'level' => 
  array (
    'label' => 'LEVEL',
    'type' => 'text',
  ),
  'is_fact' => 
  array (
    'label' => 'IS_FACT',
    'type' => 'text',
  ),
);
