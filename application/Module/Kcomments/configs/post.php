<?php
return array (
  'model' => 
  array (
    'content' => 
    array (
      'label' => 'CONTENT',
      'type' => 'text',
    ),
    'type' => 
    array (
      'label' => 'TYPE',
      'type' => 'text',
    ),
    'create' => 
    array (
      'label' => 'CREATE',
      'type' => 'text',
    ),
    'id' => 
    array (
      'label' => 'ID',
      'type' => 'hidden',
    ),
    'active' => 
    array (
      'label' => 'ACTIVE',
      'type' => 'checkbox',
    ),
    'archived' => 
    array (
      'label' => 'ARCHIVED',
      'type' => 'text',
    ),
    'extend' => 
    array (
      'label' => 'EXTEND',
      'type' => 'text',
    ),
    'last_update' => 
    array (
      'label' => 'LAST_UPDATE',
      'type' => 'text',
    ),
    'parent' => 
    array (
      'label' => 'PARENT',
      'type' => 'text',
    ),
    'root' => 
    array (
      'label' => 'ROOT',
      'type' => 'text',
    ),
    'level' => 
    array (
      'label' => 'LEVEL',
      'type' => 'text',
    ),
    'master_relation' => 
    array (
      'label' => 'MASTER_RELATION',
      'type' => 'text',
    ),
    'is_fact' => 
    array (
      'label' => 'IS_FACT',
      'type' => 'text',
    ),
  ),
);
