<?php
return array (
  'lists' => 
  array (
    'list_post_types' => 'kcomments.posttype',
  ),
  'modulerelations' => 
  array (
    0 => 
    array (
      'type' => '11',
      'db_table' => 'Application\\Module\\Kcomments\\Modulerelation\\Table\\Maintagproduct',
      'options' => 
      array (
      ),
    ),
    1 => 
    array (
      'type' => '11',
      'db_table' => 'Application\\Module\\Kcomments\\Modulerelation\\Table\\Maintagdepartment',
      'options' => 
      array (
      ),
    ),
    2 => 
    array (
      'type' => '22',
      'db_table' => 'Application\\Module\\Kcomments\\Modulerelation\\Table\\Tagpost',
      'options' => 
      array (
      ),
    ),
    3 => 
    array (
      'type' => '21',
      'db_table' => 'Application\\Module\\Kcomments\\Modulerelation\\Table\\Postcreator',
      'options' => 
      array (
      ),
    ),
    4 => 
    array (
      'type' => '11',
      'db_table' => 'Application\\Module\\Kcomments\\Modulerelation\\Table\\Maintagcountry',
      'options' => 
      array (
      ),
    ),
    5 => 
    array (
      'type' => '12',
      'db_table' => 'Application\\Module\\Kcomments\\Modulerelation\\Table\\Postfiles',
      'options' => 
      array (
      ),
    ),
    6 => 
    array (
      'type' => '11',
      'db_table' => 'Application\\Module\\Kcomments\\Modulerelation\\Table\\Maintagdeal',
      'options' => 
      array (
      ),
    ),
    7 => 
    array (
      'type' => '22',
      'db_table' => 'Application\\Module\\Kcomments\\Modulerelation\\Table\\Postcircles',
      'options' => 
      array (
      ),
    ),
    8 => 
    array (
      'type' => '22',
      'db_table' => 'Application\\Module\\Kcomments\\Modulerelation\\Table\\Tasktag',
      'options' => 
      array (
      ),
    ),
  ),
  'navigation_pages' => 
  array (
    0 => 
    array (
      'label' => 'Post Types',
      'module' => 'kcomments',
      'controller' => 'posttype',
      'action' => 'list',
      'parent' => 'admin',
    ),
  ),
  'left_navigation_pages' => 
  array (
  ),
  'listview_options' => 
  array (
    'tag' => 
    array (
      'name' => 
      array (
        'order' => 10,
        'label' => 'NAME',
      ),
      'master_relation' => 
      array (
        'order' => 20,
        'label' => 'MASTER_RELATION',
      ),
      'archived' => 
      array (
        'order' => 30,
        'label' => 'ARCHIVED',
      ),
      'extend' => 
      array (
        'order' => 40,
        'label' => 'EXTEND',
      ),
    ),
    'post' => 
    array (
      'content' => 
      array (
        'order' => 10,
        'label' => 'CONTENT',
      ),
      'type' => 
      array (
        'order' => 20,
        'label' => 'TYPE',
      ),
      'archived' => 
      array (
        'order' => 30,
        'label' => 'ARCHIVED',
      ),
      'extend' => 
      array (
        'order' => 40,
        'label' => 'EXTEND',
      ),
    ),
  ),
);
