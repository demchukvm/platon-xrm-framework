<?php
namespace Application\Module\Kcomments\Modulerelation\Table;

class Tasktag extends \Sl\Modulerelation\DbTable {
	protected $_name = 'kcomments_task_tag';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Sl\Module\Crm\Model\Task' => array(
			'columns' => 'task_id',
			'refTableClass' => 'Sl\Module\Crm\Model\Table\Task',
		'refColums' => 'id'),
                		'Application\Module\Kcomments\Model\Tag' => array(
			'columns' => 'tag_id',
			'refTableClass' => 'Application\Module\Kcomments\Model\Table\Tag',
				'refColums' => 'id'	),
	);
}