<?php
namespace Application\Module\Kcomments\Modulerelation\Table;

class Maintagdepartment extends \Sl\Modulerelation\DbTable {
	protected $_name = 'kcomments_main_tag_department';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Application\Module\Kcomments\Model\Tag' => array(
			'columns' => 'tag_id',
			'refTableClass' => 'Application\Module\Kcomments\Model\Table\Tag',
		'refColums' => 'id'),
                		'Application\Module\Kcompany\Model\Department' => array(
			'columns' => 'department_id',
			'refTableClass' => 'Application\Module\Kcompany\Model\Table\Department',
				'refColums' => 'id'	),
	);
}