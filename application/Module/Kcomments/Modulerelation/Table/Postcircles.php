<?php
namespace Application\Module\Kcomments\Modulerelation\Table;

class Postcircles extends \Sl\Modulerelation\DbTable {
	protected $_name = 'kcomments_post_circles';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Application\Module\Kcomments\Model\Post' => array(
			'columns' => 'post_id',
			'refTableClass' => 'Application\Module\Kcomments\Model\Table\Post',
		'refColums' => 'id'),
                		'Application\Module\Kcompany\Model\Circle' => array(
			'columns' => 'circle_id',
			'refTableClass' => 'Application\Module\Kcompany\Model\Table\Circle',
				'refColums' => 'id'	),
	);
}