<?php
namespace Application\Module\Kcomments\Modulerelation\Table;

class Postfiles extends \Sl\Modulerelation\DbTable {
	protected $_name = 'kcomments_post_file';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Application\Module\Kcomments\Model\Post' => array(
			'columns' => 'post_id',
			'refTableClass' => 'Application\Module\Kcomments\Model\Table\Post',
		'refColums' => 'id'),
                		'Sl\Module\Home\Model\File' => array(
			'columns' => 'file_id',
			'refTableClass' => 'Sl\Module\Home\Model\Table\File',
				'refColums' => 'id'	),
	);
}