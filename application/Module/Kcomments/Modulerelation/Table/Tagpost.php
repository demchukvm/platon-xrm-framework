<?php
namespace Application\Module\Kcomments\Modulerelation\Table;

class Tagpost extends \Sl\Modulerelation\DbTable {
	protected $_name = 'kcomments_tag_post';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Application\Module\Kcomments\Model\Tag' => array(
			'columns' => 'tag_id',
			'refTableClass' => 'Application\Module\Kcomments\Model\Table\Tag',
		'refColums' => 'id'),
                		'Application\Module\Kcomments\Model\Post' => array(
			'columns' => 'post_id',
			'refTableClass' => 'Application\Module\Kcomments\Model\Table\Post',
				'refColums' => 'id'	),
	);
}