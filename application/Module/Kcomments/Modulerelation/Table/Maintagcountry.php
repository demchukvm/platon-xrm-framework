<?php

namespace Application\Module\Kcomments\Modulerelation\Table;

class Maintagcountry extends \Sl\Modulerelation\DbTable {

    protected $_name = 'kcomments_main_tag_country';
    protected $_primary = 'id';
    protected $_referenceMap = array(
        'Application\Module\Kcomments\Model\Tag' => array(
            'columns' => 'tag_id',
            'refTableClass' => 'Application\Module\Kcomments\Model\Table\Tag',
            'refColums' => 'id'),
        'Sl\Module\Home\Model\Country' => array(
            'columns' => 'country_id',
            'refTableClass' => 'Sl\Module\Home\Model\Table\Country',
            'refColums' => 'id'),
    );

}