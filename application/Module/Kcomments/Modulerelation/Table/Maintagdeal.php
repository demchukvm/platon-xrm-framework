<?php
namespace Application\Module\Kcomments\Modulerelation\Table;

class Maintagdeal extends \Sl\Modulerelation\DbTable {
	protected $_name = 'kcomments_main_tag_deal';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Application\Module\Kcomments\Model\Tag' => array(
			'columns' => 'tag_id',
			'refTableClass' => 'Application\Module\Kcomments\Model\Table\Tag',
		'refColums' => 'id'),
                		'Application\Module\Business\Model\Deal' => array(
			'columns' => 'deal_id',
			'refTableClass' => 'Application\Module\Business\Model\Table\Deal',
				'refColums' => 'id'	),
	);
}