<?php
namespace Application\Module\Kcomments\Modulerelation\Table;

class Postcreator extends \Sl\Modulerelation\DbTable {
	protected $_name = 'kcomments_post_user_creator';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Application\Module\Kcomments\Model\Post' => array(
			'columns' => 'post_id',
			'refTableClass' => 'Application\Module\Kcomments\Model\Table\Post',
		'refColums' => 'id'),
                		'Sl\Module\Auth\Model\User' => array(
			'columns' => 'user_id',
			'refTableClass' => 'Sl\Module\Auth\Model\Table\User',
				'refColums' => 'id'	),
	);
}