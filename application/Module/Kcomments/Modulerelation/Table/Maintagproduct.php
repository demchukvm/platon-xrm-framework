<?php
namespace Application\Module\Kcomments\Modulerelation\Table;

class Maintagproduct extends \Sl\Modulerelation\DbTable {
	protected $_name = 'kcomments_main_tag_product';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Application\Module\Kcomments\Model\Tag' => array(
			'columns' => 'tag_id',
			'refTableClass' => 'Application\Module\Kcomments\Model\Table\Tag',
		'refColums' => 'id'),
                		'Application\Module\Business\Model\Product' => array(
			'columns' => 'product_id',
			'refTableClass' => 'Application\Module\Business\Model\Table\Product',
				'refColums' => 'id'	),
	);
}