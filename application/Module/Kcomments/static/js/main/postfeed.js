var post_feed_action = '/kcomments/post/ajaxfeed';
var post_feed_tags_autocomplete = "/kcomments/tag/ajaxautocomplete";
var post_feed_circles_autocomplete = "/kcompany/circle/ajaxautocomplete";
var post_facts_action = '/kcomments/post/ajaxfactsfeed';
var post_feed_selector = '#post_feed';
var post_feed_new_post_form_selector = '.custom-form.post-create'
var post_feed_new_post_sel = '#new-post';
var post_user_default_avatar = '/img/images/avatar.jpg';
var post_factbox_selector = '.factbox';
var post_factbox_helper_selector = '#factbox-helper';
var post_fileshare_selector = '.fileshare-panel';
var posts_fileshare_selector = '.fileshare';

var post_default_class= 'comment-item media arrow arrow-left';
var postfeed_dragfact_opts = {opacity: 0.7, 
      cursor: "move",
      cursorAt: {top: -12, left: -20},
      helper: function( e) {
        return $( "<div/>" ).attr('id','factbox-helper').addClass('drag-helper').data('id',$(e.currentTarget).closest('article').attr('id')).html($('.post-content',$(e.currentTarget).closest('article')).html()).appendTo('body');
      }
    };


var select2_filters = function(el, url, placeholder, filters, multiple) {
    $(el).data('addition-filters',filters instanceof Array?filters:[]);
    $(el).select2({
        placeholder: placeholder,
        multiple: multiple,
        addition_filters:filters,
        minimumInputLength: 0,
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: url,
            dataType: 'json',

            data : function(term, page){
                var f = $.extend([],$(this).data('addition-filters'));
                if ($(this).val().length)f.push('id-nin-'+$(this).val());
                return {name: term, filter_fields:f};
            },
            results: function (data, page) { // parse the results into the format expected by Select2.
               if (data.result){
                   var d = [];
                   for (var i in data.aaData){
                       d.push({id:data.aaData[i].id, text:data.aaData[i].name})
                   }
                   return {results:d};
               }

            },

        },
        initSelection: function(el, callback){
            callback([]);
        }
    });
}


var post_feed_limit = 10;

var post_autocomplete_opts = {
            open: function() {
                $(this).autocomplete('widget').css('z-index', 10000);
                return false;
            },
            minLength: 0,
            source: function(request, responce) {
                var $this = this.element;
                var url = $this.data('rel');
                var filter_fields = {};
                var term = request.term.split(', ');
                var ids = $('#tag-ids').val();
                
                $.ajax({
                    method: 'POST',
                    url: url,
                    data: {
                        name: term[term.length-1],
                        filter_fields:ids.length?['id-nin-'+ids]:[],
                        quick_search: 1
                        
                    },
                    success: function(data) {
                        $this.removeClass('ui-autocomplete-loading');
                        if (data.result) {
                            var name_index = 3;
                            var value_index = 0;
                            if(data.hasOwnProperty('name_index') && data.name_index) {
                                name_index = data.name_index;
                            }
                            if(data.hasOwnProperty('value_index') && data.value_index) {
                                value_index = data.value_index;
                            }
                            if (data.hasOwnProperty('sort_array')) {
                                for (key in data.sort_array) {
                                    if (data.sort_array[key] == 'name') {
                                        name_index = key;
                                        break;
                                    }
                                }
                            }
                            if (data.aaData.length) {
                                responce($.map(data.aaData, function(item) {
                                    var label;
                                    try {
                                        if($(item[name_index]).length) {
                                            label = $(item[name_index]).text();
                                        } else {
                                            label = item[name_index];
                                        }
                                    } catch(e) {
                                        label = item[name_index];
                                    }
                                    return {
                                        value: item[value_index],
                                        label: label
                                    };
                                }));
                            } else {
                                //responce([{value: 0, label: ''}]);
                            }
                        } else {
                            
                            alert(data.description);
                        }
                    }
                })
            },
            select: function(e, ui) {
                
                $('<li/>').addClass('label bg-info').attr('tagid',ui.item.value).html(ui.item.label).appendTo($('.tags-pillbox ul',$pf));
                
                this.value = '';
                
                return false;
            },
            focus: function(e, ui) {
                
               //$(this).val(ui.item.label);
                return false;
            }
        } 
var opts = {};
var facts_opts = {};
var $pf, $fbox;

$(function(){
    $pf = $(post_feed_selector);
    $fbox = $(post_factbox_selector);
    $fileshare = $(post_fileshare_selector);
    
    if ($fileshare.length) {  //Якщо є місце для файлів, виводимо атачі після показу постів
        $(document).on('post.rendered',function(e,p){

            if (p instanceof Postdata && p.postfiles.length) {
                    var files = [];
                    for (var i in p.postfiles){
                        var f = new Filedata(p.postfiles[i]);
                        f.render($(posts_fileshare_selector,$fileshare));
                    }
            }
        });
        
        $fileshare.on('click', 'i.attach', function(){
           $(this).siblings('input.hide[type="file"]').click();
        });
        
        $('input[type="file"]',$fileshare).each(function(){
            var url = $(this).data('rel');
            
            if ($fileshare.data('relation_name') && $fileshare.data('relation_value')){
                var o = {};
                o['modulerelation_'+$fileshare.data('relation_name')] = [$fileshare.data('relation_value')];
                url+='?'+$.param(o);
            }
            
            $(this).fileupload({
                    dataType: 'json',
                    url: url,
                    done: function(e, data) {
                        if (data.result.result) {
                           post_feed_data.getPostsData(null,null,1);
                        } else {
                            alert(data.result.description);
                        }
                    },
                    add: function(e, data) {
                        data.paramName = 'location';
                        data.submit();
                    }
                });
        });
    }
    
    $fbox.on('mouseup', function(){
         if ($(post_factbox_helper_selector).length){
             var factpost_id = $(post_factbox_helper_selector).data('id').split('-');
             
             
             
             post_fact_data.setFact(factpost_id[factpost_id.length-1]);
             
         }
    });
    
    $fbox.on('click','.fact-item', function(){
       post_feed_data.gotoPost( post_fact_data.getFact($(this).data('post_id')) );
    });
    
    if ($pf.length == 1 ){
        
        if ($pf.data('relation')) {
            opts['relation'] = $pf.data('relation');
            facts_opts['relation'] = $pf.data('relation');
            
        }
        if ($pf.data('model_id')){ 
            opts['model_id'] = $pf.data('model_id');
            facts_opts['model_id'] = $pf.data('model_id');
        }
        opts['limit'] = post_feed_limit;
        
        
        post_feed_data.getPostsData()
       
        
        if ($fbox.length){
            post_fact_data.getFactsData()
        }
        
        
        $(post_feed_selector).on('click', '.post-types .fa:not(.active)', function (){
            $(this).siblings('.active').removeClass('active');
            $(this).addClass('active');
            update_post_data ($(this).closest('article').data('id'),'type', $(this).data('type'))
        
        
        });
    
  
        
        var $post_form = $(new_post_form_tpl);
        
        //$('.autocomplete',$post_form).autocomplete(post_autocomplete_opts);
        if ($pf.data('relation')){
            $('.maintag',$post_form).remove();
            
        } else
        {
            assignAutocomplete($post_form);
        }
        
        
        $post_form.attachFile = function(id, label){
            if (parseInt(id) > 0){
        
              $('<li/>').addClass('file-label').attr('fileid', id).html(label).appendTo($('.files-pillbox ul',this));
        
            }
            
        };
        
        $post_form.checkSubmit = function(){
            if ($(this).data('uploads') > 0) $(post_feed_new_post_sel,this).attr('disabled','disabled');
            else $(post_feed_new_post_sel,this).removeAttr('disabled');
        };
        
        $post_form.startFileUpload = function(){
           $(this).data('uploads', 1+($(this).data('uploads') > 0?$(this).data('uploads'):0));
           $post_form.checkSubmit();
        };
        
        $post_form.finishFileUpload = function(){
           $(this).data('uploads', ($(this).data('uploads') > 1?$(this).data('uploads'):1) - 1);
           $post_form.checkSubmit();
        };
        
        $('textarea', $post_form).keydown(function (e) {
            if (e.ctrlKey && e.keyCode == 13) {
              $('#new-post', $post_form).click();
            } else if (e.keyCode == 27){
              $('#cancel-post', $post_form).click();   
            }
         });
        
        $('input[type="file"]',$post_form).each(function(){
            
            $(this).fileupload({
                    dataType: 'json',
                    url: $(this).attr('data-rel'),
                    done: function(e, data) {
                        if (data.result.result) {
                            for (var i in data.result.files)
                            $post_form.attachFile(data.result.files[i].id, data.result.files[i].name);
                        } else {
                            alert(data.result.description);
                        }
                        $post_form.finishFileUpload();
                    },
                    add: function(e, data) {
                        $post_form.startFileUpload();
                        data.paramName = 'location';
                        data.submit();
                    }
                });
        });
        
        $post_form.clearForm = function(){
          $('textarea',this).html('').val('');
          $('div.types .active',this).removeClass('active');
          $('#tag-ids').val('').data('name', '');
          //$('#tags option:selected').removeAttr('selected');
          
          $('#tags, #circles').val('').trigger('change');
          
          $('.maintag input',this).val('');
          $('.autocomplete',this).val('');
          $('input[type="file"]', this).val('');
          $('.files-pillbox ul',this).html('');
          $('.tags-pillbox ul',this).html('');
          $post_form.data('uploads',0);
          $post_form.checkSubmit();
          return this;
        };
        
        $post_form.showForm = function(){
            $('.post-creation').removeClass('post-creation');
            $(this).show();
            $(this).parent().addClass('post-creation');
            
        }
        $post_form.hideForm = function(){
            $('.post-creation').removeClass('post-creation');
            $(this).hide();
            
        }
        
        $post_form.on('click', '.types .fa', function (){
            
           $(this).addClass('active').siblings('.fa.active').removeClass('active');
           
        });
        
        $post_form.on('click', 'i.attach', function(){
           $(this).siblings('input.hide[type="file"]').click();
        });
        
        $pf.on('mouseup','.post-content, .post-tags', function(){
             if (!((window.getSelection && window.getSelection().toString().length) || 
                  (document.selection && document.selection.createRange().text.length)
              )){
                      $('.drag-helper').remove();
                      
                      $(this).parent().append($post_form);
                      $post_form.showForm();
                      if (!$('.select2-offscreen',$post_form).length){
                          //$('#tags option').remove();
                          select2_filters($('#tags',$post_form),post_feed_tags_autocomplete, 'Select tags', ['master_relation-eq-maintagcountry'], true);
                          select2_filters($('#circles',$post_form),post_feed_circles_autocomplete, 'Select circles',[], true);
                          select2_filters($('#maintag',$post_form),post_feed_tags_autocomplete, 'Post to', ["master_relation-in-\'maintagproduct\',\'maintagdeal\'"], false);
                      }
                      $post_form.find('textarea:first').focus();
                      var delta = 20 + $('.form-btns',$post_form).offset().top + $('.form-btns',$post_form).height() - $('body').scrollTop() - $(window).height();
                      
                      if (delta > 0) {
                          $('body').animate({scrollTop:(delta +  $('body').scrollTop() )});
                      }
                      return false;
            }
            
        });
        
        $(document).on('create.post_type_tpl', function(){
            
            $('div.types',$post_form).html(post_feed_data.post_type_tpl);
            $("[data-toggle=tooltip]", $post_form).tooltip();
         /*   $('#tags option',$post_form).remove();
            for (var t in post_feed_data.postTags) {
                $('<option/>').val(t).html(post_feed_data.postTags[t]).appendTo($('#tags',$post_form));
            }   
          */  //$('#tags',$post_form).select2(); 
        });
        
        $( document ).scroll(function() {
            
            if ($pf.is(':visible') && (($(this).height() - $(window).height() - $(this).scrollTop()) < 100) ) {
                
                post_feed_data.getPostsData(null, true);
            }
            
        });
        
        $pf.on('click','#cancel-post',function(){
            $post_form.clearForm().hideForm();
            
        });
        

        
        $pf.on('click', post_feed_new_post_sel, function(){
            var text = $('textarea',$post_form).val() || $('textarea',$post_form).text();
            var type = $('div.types .fa.active',$post_form).length?$('div.types .fa.active',$post_form).data('type'):0;
            var files = [];
            var tags = $('#tags',$post_form).val().split(',');
            var master_tag = $(this).closest('[id^="post-id-"]').length?null:$('#maintag',$post_form).val();
            var circles = $(this).closest('[id^="post-id-"]').length?null:$('#circles',$post_form).val().split(',');
            $('.files-pillbox .file-label[fileid]',$post_form).each(function(){
                files.push($(this).attr('fileid'));
            });
            
            
            
            /*$('.tags-pillbox .label[tagid]',$post_form).each(function(){
                tags.push($(this).attr('tagid'));
            });*/
            
            
            $post_form.clearForm().hideForm();
            
            new_post_data(text, type, $(this).closest('[id^="post-id-"]').data('id'), tags, files, master_tag, circles);
        });
    }
});

var new_post_data = function (content, type, parent, tags, files, master_tag, circles) {
    
    var create = {
        model:'post',
        id:false,
        data:{}
    }
    
    create.data['content'] = content;
    create.data['type'] = parseInt(type);
    create.data['parent'] = parseInt(parent);
    create.data['modulerelation_tagpost'] = tags;
    create.data['modulerelation_postfiles'] = files;
    if (opts.hasOwnProperty('relation') && opts.relation != '' &&
        opts.hasOwnProperty('model_id') && opts.model_id > 0){
        create.data['modulerelation_'+opts.relation] = [opts.model_id];
        create.data['master_relation'] = opts.relation;
    } else {
        create.data['master_tag'] = master_tag;
        create.data['modulerelation_postcircles'] = circles;
    }
    post_feed_data.getPostsData(create);
    
}


var update_post_data = function (id, field, value) {
    
    var update = {
        model:'post',
        id:id,
        data:{}
        
    }
    update.data[field] = value;
    
    post_feed_data.getPostsData(update);
}

var post_tpl =  '<a class="pull-left thumb-small avatar" name="data-post-{id}"><img src="'+post_user_default_avatar+'" class="img-circle" data-imgpost_{id}="username"></a>'+
        '<section class="media-body panel">'+
        '<header class="panel-heading clearfix">'+
        '<a href="#" class="not-update" data-post_{id}="username"></a>'+
        '<span class="post-types post-types-{id}">{type_tpl}</span>'+
        '<a class="post-master-object" data-post_{id}="master_object" target="_blank"></a>'+
        '<span class="text-muted m-l-small pull-right"><i class="fa fa-clock-o"></i> <span data-post_{id}="last_update"></span></span>'+
        '</header>'+
        '<div class="panel-body">'+
        '<div class="post-tags post-tags-{id}"></div>'+
        '<div data-post_{id}="content" class="post-content"></div>'+
        '<div class="post-files post-files-{id}"></div>'+
        // '<div class="comment-action m-t-small">'+
        '</div>'+
        '</div>'+
'</section>';


var fact_tpl =  
'<div class="fact-item" data-post_id="{id}"><div class="text-sm"><small data-post_{id}="create" class="post-create"></small></div><div data-post_{id}="content" class="post-content"></div></div>';


var fileshare_tpl =  
'<div data-post_{id}="content" class="post-content"><div class="text-sm"><b>{user}</b> <small>{create}</small></div><a href="/home/file/detailed/id/{id}">{name}</a></div>';

var post_type_tpl = '<i class="fa post-type-{id}" title="{name}" data-type="{id}" data-toggle="tooltip" data-placement="right" data-original-title="{name}" ></i>';           

var new_post_wrapper_form_tpl = '<div class="panel panel-body new-post"><div class="post-content"><small class="text-muted temp-title">Write new Post</small></div></div>'

//var post_tag_tpl = '<span class="label bg-info">{tag}</span>';
var post_tag_tpl = '<span href=#>{tag}</span>';
var post_file_tpl = '<span class="file-label"><a href="/home/file/detailed/id/{id}">{name}</a></span>';
    
var new_post_form_tpl = '<article class="comment-item media" id="comment-form">'+
'<section class="media-body">'+
'<div class="custom-form post-create">'+
'<div class="form-group">'+
'<textarea placeholder="Input new comment here" class="form-control" name="text"></textarea>'+
'<div class="pillbox tags-pillbox"><ul></ul></div>'+
'<div class="pillbox files-pillbox"><ul></ul></div>'+
'<div class="types"></div>'+
'<div class="files">'+
    '<i class="fa fa-paperclip attach" data-toggle="tooltip" data-placement="right" title="Attach file" data-original-title="Attach file"></i><input type="file" class="hide" value="" data-rel="/home/file/ajaxcreate"><input type="hidden" name="files" value="">'+
'</div>'+
'<div class="pull-right">'+
    '<div class="tags-input fa fa-tags"><input type="hidden"  id="tags" class="tags-select" value="" style="width:100px;"></div>'+
    '<div class="circles-input fa fa-circle-o"><input type="hidden"  id="circles" class="tags-select" value="" style="width:120px;"></div>'+
    '<div class="maintag fa fa-share-square-o"><input type="hidden" id="maintag" value="" style="width:150px;"></div>'+
    '<div class="form-btns"><button class="btn btn-white btn-sm" id="cancel-post" type="button">Cancel</button><button class="btn btn-primary btn-sm" id="new-post" type="button">POST</button></div>'+
'</div>'+
'</div>'+
'</div>'+
'</section>'+
'</article>';


var post_fact_data = {
    data: {posts:{}
            },
    postIds:[],
    
    maxTimeId: false,
    
    getFactsData: function(update){
        
        var _opts = facts_opts;
        var _self = this;
        _opts['update'] = update;
        _opts['after'] = _self.getMaxPostsTime();
        
        $.post(post_facts_action, 
            _opts, 
            function( data ){
                if (data.hasOwnProperty('result') && data.result) {
                    _self.setData(data, $fbox);
                }
            }
        );
    },
    getFact: function(factId){
         return (this.data.posts.hasOwnProperty(factId)?this.data.posts[factId]:null);
    },
    setFact: function (factId){
        var update = {
            id:factId,
            data: {is_fact:1}
        }
        this.getFactsData(update);
    },
    setData: function(data, $wrapper){

        if (data instanceof Object && data.hasOwnProperty('posts')){
            
            for (var i in data.posts){
                var post = new Factdata(data.posts[i]);
                this.maxTimeId = (!this.maxTimeId || this.data.posts[this.maxTimeId].time < post.time) ? post.id:this.maxTimeId;
                this.data.posts[post.id] = post; 
                post.render($wrapper);
            }
        }
        
        return this;
    },
    getPostIds: function (){
        return Object.keys(this.data.posts);
    },
    getMaxPostsTime: function () {
        return this.maxTimeId?this.data.posts[this.maxTimeId].last_update:null;
    }
}

var post_feed_data = {
    data: {posts:{}
            },
    postIds:[],
    postTags:{},
    post_type_tpl: '',
    maxTimeId: false,
    minTimeId: false,
    noBefore: false,
    getPostsData: function(update, earliest, getfiles){
        if (earliest && this.noBefore) return;
        var _opts = opts;
        var _self = this;
        _opts['prepare'] = (this.post_type_tpl.length == 0)?1:null;
        _opts['get_files'] = ((_opts['prepare'] || getfiles)&& $fileshare.length && $fileshare.data('relation_name'))?$fileshare.data('relation_name'):null;
        
        _opts['update'] = update;
        _opts['after'] = earliest?null:post_feed_data.getMaxPostsTime();
        
        if (earliest instanceof Factdata && parseInt(earliest.root) > 0) { //for gotoPost
            _opts['to_post'] =  earliest.root;
        }
         _opts['before'] =  (earliest)?  post_feed_data.getMinPostsTime() : null;
         
        $.post(post_feed_action, 
            _opts, 
            function( data ){
                if (data.hasOwnProperty('result') && data.result) {
                    
                    if (_opts['prepare'] && !$('.new-post',$pf).length) {
                        $pf.prepend($(new_post_wrapper_form_tpl));
                    }
                    
                    if (_opts['prepare'] && data.hasOwnProperty('country_tags') && data.country_tags instanceof Object && !_self.postTags.length) {
                        _self.postTags = data.country_tags;
                    }
                    
                    if (_opts['get_files'] && $fileshare.length && data.hasOwnProperty('files') && data.files instanceof Object) {
                        
                        for (var i in data.files){
                            var f = new Filedata(data.files[i]);
                            f.render($(posts_fileshare_selector,$fileshare));
                        }
                        
                        
                    }
                    
                    if (earliest && data.hasOwnProperty('posts') && !data.posts.length) _self.noBefore = true;
                    _self.setData(data, $pf);
                    
                     if (earliest instanceof Factdata && parseInt(earliest.root) > 0) {
                         _self.gotoPost(earliest);
                     }
                    
                    
                    $pf.trigger('newdata.sl');
                }
            }
            );

    },
    setData: function(data, $wrapper){
        
        if (data instanceof Object && data.hasOwnProperty('post_lists') &&
            data.post_lists instanceof Object && data.post_lists.hasOwnProperty('type') && 
            data.post_lists.type instanceof Object
            )
            {
            var tpl = '';
            for (var i in data.post_lists.type){
                tpl+=this.renderPostTypeTpl(i, data.post_lists.type[i]);
            }
            this.post_type_tpl = tpl;
            
            $(document).trigger('create.post_type_tpl');
            
            setInterval (function () {post_feed_data.getPostsData()}, 30000);
        }
        
        if (data instanceof Object && data.hasOwnProperty('posts')){
            
            for (var i in data.posts){
                var post = new Postdata(data.posts[i]);
                if (!parseInt(post.parent)) this.minTimeId = (!this.minTimeId || this.data.posts[this.minTimeId].time > post.time) ? post.id:this.minTimeId;
                
                if (!parseInt(post.parent)) this.maxTimeId = (!this.maxTimeId || this.data.posts[this.maxTimeId].time < post.time) ? post.id:this.maxTimeId;
                
                this.data.posts[post.id] = post; 
                post.render($wrapper, this.post_type_tpl);
                
            }
        }
        
        return this;
    },
    renderPostTypeTpl: function(id,name){
        var tpl = post_type_tpl;
        return tpl.replace(/{id}/g,id).replace(/{name}/g,name);
    },
    getPostIds: function (){
        
        return Object.keys(this.data.posts);
    },
    getMaxPostsTime: function () {
        return this.maxTimeId?this.data.posts[this.maxTimeId].last_update:null;
    } ,
    getMinPostsTime: function () {
        return this.minTimeId?this.data.posts[this.minTimeId].last_update:null;
    },
    gotoPost: function(fact) {
        if (fact instanceof Factdata){
            if (this.data.posts.hasOwnProperty(fact.id)){
                this.scrollToPost(fact.id);
            } else {
                post_feed_data.getPostsData(false, fact);
                
                
            }
        }
    },
    scrollToPost: function(id){
        
         var delta = $('#post-id-'+id).offset().top + $('#post-id-'+id+' > section').height() - $('body').scrollTop() - $(window).height();
         var _self = this;             
            
            if (delta > 0) {
                $('body').animate({scrollTop:(20+ delta +  $('body').scrollTop() )}, function(){
                    _self.highlightPost(id);
                });
            } else if ($('body').scrollTop() > $('#post-id-'+id).offset().top ) {
                $('body').animate({scrollTop:($('#post-id-'+id).offset().top -60)}, function(){
                    _self.highlightPost(id);
                });
            } else {
                _self.highlightPost(id);
            }
    
    },
    highlightPost: function (id) {
        
        $('#post-id-'+id+' > section').effect( 'highlight', {}, 1000);
        
    }
    
};            

function Filedata(data) {
    this.id = (data instanceof Object && data.hasOwnProperty('id'))?data.id:null;
    this.name = (data instanceof Object && data.hasOwnProperty('name'))?data.name:null;
    this.user = (data instanceof Object && data.hasOwnProperty('user'))?data.user:null;
    this.create = (data instanceof Object && data.hasOwnProperty('create'))?data.create:null;
    this.htmlId = this.id?'file-id-'+this.id:false;
}


Filedata.prototype.render = function ($wrapper){
    if (!this.htmlId) return false;
    
    var _self = this;
    var $p = $('#'+_self.htmlId,$wrapper);
    if (!$p.length){
        
        $p = $('<section/>').attr({
            id:_self.htmlId, 
            'class':'panel-body media-body file'
        }).html(fileshare_tpl.replace(/{id}/g,_self.id)
                             .replace(/{name}/g,_self.name)
                             .replace(/{create}/g,_self.create)
                             .replace(/{user}/g,_self.user))
                             .data('id',_self.id);
        $p.appendTo($wrapper);

    } 
    
    $('[data-post_'+_self.id+']', $p).each(function(){
        if ( _self.hasOwnProperty($(this).data('post_'+_self.id))) {
            $(this).html(_self[$(this).data('post_'+_self.id)]);
            if ($(this).is('.not-update'))$(this).removeAttr('data-post_'+_self.id);
        }
    })
    
    
    
    
};

function Factdata(data) {
    this.id = (data instanceof Object && data.hasOwnProperty('id'))?data.id:null;
    this.content = (data instanceof Object && data.hasOwnProperty('content'))?data.content.replace(/\n/g,"<br>"):null;
    this.create = (data instanceof Object && data.hasOwnProperty('create'))?data.create:null;
    this.last_update = (data instanceof Object && data.hasOwnProperty('last_update'))?data.last_update:null;
    this.master_object = (data instanceof Object && data.hasOwnProperty('master_object'))?data.master_object:'';
    this.root = (data instanceof Object && data.hasOwnProperty('root') && parseInt(data.root) > 0)?data.root:this.id;
    this.postcreator = (data instanceof Object && data.hasOwnProperty('postcreator'))?data.postcreator:{};
    this.username = '';
    if (this.postcreator.hasOwnProperty('name')) this.username = this.postcreator.name;
    this.htmlId = this.id?'fact-id-'+this.id:false;
}

Factdata.prototype.render = function ($wrapper){
    if (!this.htmlId) return false;
    
    var _self = this;
    var $p = $('#'+_self.htmlId,$wrapper);
    if (!$p.length){
        var time = _self.last_update.replace(/[^\d]/g,'');
        
        $p = $('<section/>').attr({
            id:_self.htmlId, 
            'class':'panel-body media-body fact'
        }).html(fact_tpl.replace(/{id}/g,_self.id)).data('id',_self.id).data('time',time);
        $p.prependTo($wrapper);

    } 
    
    $('[data-post_'+_self.id+']', $p).each(function(){
        if ( _self.hasOwnProperty($(this).data('post_'+_self.id))) {
            
            $(this).html(_self[$(this).data('post_'+_self.id)]);
            if ($(this).is('.not-update'))$(this).removeAttr('data-post_'+_self.id);
        }
    })
    
};

function Postdata(data) {
    this.id = (data instanceof Object && data.hasOwnProperty('id'))?data.id:null;
    this.type = (data instanceof Object && data.hasOwnProperty('type'))?data.type:0;
    this.parent = (data instanceof Object && data.hasOwnProperty('parent'))?data.parent:0;
    this.content = (data instanceof Object && data.hasOwnProperty('content'))?data.content.replace(/\n/g,"<br>"):null;
    this.last_update = (data instanceof Object && data.hasOwnProperty('last_update'))?data.last_update:null;
    this.time = this.last_update?parseInt(this.last_update.replace(/[^\d]/g, '')):0;
    this.tagpost = (data instanceof Object && data.hasOwnProperty('tagpost') && data.tagpost instanceof Array)?data.tagpost:[];
    this.postfiles = (data instanceof Object && data.hasOwnProperty('postfiles') && data.postfiles instanceof Object)?data.postfiles:{};
    this.master_object = (data instanceof Object && data.hasOwnProperty('master_object'))?data.master_object:'';
    this.master_object_url = (data instanceof Object && data.hasOwnProperty('master_object_url'))?data.master_object_url:'#';
     
    this.postcreator = (data instanceof Object && data.hasOwnProperty('postcreator'))?data.postcreator:{};
    this.username = '';
    if (this.postcreator.hasOwnProperty('name')) this.username = this.postcreator.name;
    
    this.htmlId = this.id?'post-id-'+this.id:false;
    
}

Postdata.prototype.render = function ($wrapper, posttype_tpl, $files_wrap){
    if (!this.htmlId) return false;
    
    var _self = this;
    var $p = $('#'+_self.htmlId,$wrapper);
    if (!$p.length){
        var time = _self.last_update.replace(/[^\d]/g,'');
        
        $p = $('<article/>').attr({
            id:_self.htmlId, 
            'class':post_default_class+ ' post'
        }).html(post_tpl.replace('{type_tpl}',posttype_tpl).replace(/{id}/g,_self.id)).data('id',_self.id).data('time',time);
        var $first_post = $('article.'+post_default_class.replace(/\s/g,'.')+':first',$wrapper);
        
        if (_self.parent > 0) {
            $('#post-id-'+_self.parent).append($p);
        }
        else if ($first_post.length && parseInt($first_post.data('time')) < parseInt(time)){
            $first_post.before($p);
        } else {
            $p.appendTo($wrapper);
        }
        $("[data-toggle=tooltip]",$p).tooltip();
        if ($fbox.length) $('header',$p).draggable(postfeed_dragfact_opts);
    } 
    
    $('[data-post_'+_self.id+']', $p).each(function(){
        if ( _self.hasOwnProperty($(this).data('post_'+_self.id))) {
            $(this).html(_self[$(this).data('post_'+_self.id)]);
            if ($(this).is('.not-update'))$(this).removeAttr('data-post_'+_self.id);
        }
    });
    
    $('.post-types-'+_self.id+' > .fa.active',$p).removeClass('active');
    $('.post-types'+_self.id,$p).data('id',_self.id);
    $('.post-types-'+_self.id+' > .fa.post-type-'+_self.type,$p).addClass('active');
    
    if (_self.tagpost.length) {
        var tags = [];
        
        for (var i in _self.tagpost){
        tags.push(post_tag_tpl.replace('{tag}', _self.tagpost[i]));    
        }
        
        $('.post-tags-'+_self.id,$p).html(tags.join(' '));
        
    }
    
    if (_self.postfiles.length) {
        var files = [];
        
        for (var i in _self.postfiles){
            files.push(post_file_tpl.replace('{name}', _self.postfiles[i].name).replace('{id}',_self.postfiles[i].id));   
            
        }
        
        $('.post-files-'+_self.id,$p).html(files.join(' '));
        
    }
    
     if (_self.master_object.length) {
        $('a.post-master-object',$p).attr('href', _self.master_object_url);
     }
     
    
    $(document).trigger('post.rendered', _self);
};
    
