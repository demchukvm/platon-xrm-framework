<?php
namespace Application\Module\Kcomments;

use Sl_Module_Abstract as AbstractModule;

class Module extends AbstractModule { 

	public function getListeners() {
		return array(
                    new Listener\Tagcreation($this),
                    new Listener\Postcreator($this),
                    new Listener\Firstpage($this)
                );
	}
}