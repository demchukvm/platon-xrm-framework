<?php

namespace Application\Module\Kcomments\Listener;

use Application\Module\Kcomments;

use Sl_Model_Factory as ModelFactory;
use Sl_Service_Acl as AclService;

class Postcreator extends \Sl_Listener_Abstract implements \Sl_Listener_Model_Interface {

    public function onBeforeSave(\Sl_Event_Model $event) {
        $model = $event->getModel();

        if ($model instanceof \Application\Module\Kcomments\Model\Post ) {
            $model->updateLastUpdate();
            
            
            if (!$model->getId()) {
                $user = \Zend_Auth::getInstance()->getIdentity();
                
                if ($user instanceof \Sl_Model_Abstract) {
                    if ($model instanceof \Application\Module\Kcomments\Model\Post){

                        $relation = \Sl_Modulerelation_Manager::getRelations($user, 'postcreator');
                        
                        if ($model->getParent() > 0){
                            
                            $parent = \Sl_Model_Factory::mapper($model)->findExtended($model->getParent(), array('postcircles'));
                            
                            ($parent instanceof \Sl_Model_Abstract) && 
                                    $model->setLevel($parent->getLevel()+1) 
                                          ->setRoot($parent->getRoot()? $parent->getRoot(): $parent->getId());

                                    if ($parent->getMasterRelation()){
                                        $model->setMasterRelation($parent->getMasterRelation());   
                                        $model->assignRelated($parent->getMasterRelation(), $parent->fetchRelated($parent->getMasterRelation()));
                                    }

                                    if (count($parent->fetchRelated('postcircles'))){
                                        $model->assignRelated('postcircles',$parent->fetchRelated('postcircles')); 
                                    }
                        }
                        
                    }
                    
                    ($relation instanceof \Sl\Modulerelation\Modulerelation) && $model->assignRelated($relation->getName(), array($user));
                }
            }
            
            if ($model->getRoot() > 0){
                            
                 $root = \Sl_Model_Factory::mapper($model)->find($model->getRoot());
                            
                            ($root instanceof \Sl_Model_Abstract) && 
                                    $root -> updateLastUpdate();
                                    \Sl_Model_Factory::mapper($root)->save($root);
            }
            
            
        }
    }

    function onAfterSave(\Sl_Event_Model $event) {
        $model = $event->getModel();
        $beforeModel = $event->getModelBeforeUpdate();
        
        if($model instanceof \Application\Module\Kcomments\Model\Post) {
            if($model->getId() && !$beforeModel->getId()) { // Создание
                $cUser = AclService::getCurrentUser();
                $post = ModelFactory::mapper($model)->findRelation($model, 'postcreator');
                if($post->getId() != $cUser->getId()) { // Комментировал не текущий пользователь
                    if($model->getParent()) { // Есть родитель
                        $parentPost = ModelFactory::mapper($model)->findExtended($model->getParent(), array('postcreator'));
                        if($parentPost) { // Нашли родителя
                            $poster = $parentPost->fetchOneRelated('postcreator');
                            if($poster && ($poster->getId() != $cUser->getId())) { // Не наш пост
                                // Кто-то откомментировал нащ пост
                                $notification = ModelFactory::model('notification', 'home');
                                $notification->setName()
                                        ->setName(implode(' ', array(
                                            (string) $post->fetchOneRelated('postcreator'),
                                            'commented your post'
                                        )))
                                        ->setStatus(0)
                                        ->setData(implode(' ', array(
                                            (string) $post->fetchOneRelated('postcreator'),
                                            'commented your post'
                                        )))
                                        ->assignRelated('notificationuser', array($poster));
                                ModelFactory::mapper($notification)->save($notification);
                            }
                        }
                    }
                }
            }
        }
    }

}