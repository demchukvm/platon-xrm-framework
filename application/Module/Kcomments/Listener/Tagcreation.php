<?
namespace Application\Module\Kcomments\Listener;

class Tagcreation extends \Sl_Listener_Abstract implements \Sl_Listener_Model_Interface {

    public function onBeforeSave(\Sl_Event_Model $event) {

    }

    

    function onAfterSave(\Sl_Event_Model $event) {
        
                
        $model = $event->getModel();
        
        if ($model instanceof \Application\Module\Business\Model\Product && strlen($model->getIdentifier())) {
            
            $this->_updateTag($model, 'maintagproduct');
        }elseif($model instanceof \Application\Module\Kcompany\Model\Department && strlen($model->getCode())){
            
            $this->_updateTag($model, 'maintagdepartment');
        }elseif($model instanceof \Application\Module\Business\Model\Deal && strlen($model->getName())){
            
            $this->_updateTag($model, 'maintagdeal');
            
        }elseif($model instanceof \Sl\Module\Home\Model\Country && strlen($model->getName())){
            $this->_updateTag($model, 'maintagcountry');
        }
        
        
    }
    
    
    protected function _updateTag($model, $relation_name) {
        $relation = \Sl_Modulerelation_Manager::getRelations($model, $relation_name);
        if ($relation instanceof \Sl\Modulerelation\Modulerelation){
            
                !$model->issetRelated($relation->getName()) && $model = \Sl_Model_Factory::mapper($model)->findRelation($model, $relation);
                $tag = $relation->getRelatedObject($model);
                if (!count($model->fetchRelated($relation->getName()))){
                    
                    $tag->setMasterRelation($relation->getName())->setName($model->__toString());
                    $tag->assignRelated($tag->getMasterRelation(),array($model));
                    \Sl_Model_Factory::mapper($tag)->save($tag, false, false);
                } else {
                    
                    $tag = $model->fetchOneRelated($relation->getName());
                    $tag = $tag instanceof \Sl_Model_Abstract?$tag:\Sl_Model_Factory::mapper($tag)->find(intval($tag));
                    if ($tag->getName() != $model->__toString()){
                        $tag->setName($model->__toString());
                        \Sl_Model_Factory::mapper($tag)->save($tag, false, false);
                    }
                }
                
            }
    }
}