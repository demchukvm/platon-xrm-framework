<?php
namespace Application\Module\Kcomments\Listener;

use Sl_Service_Acl as AclService;
use Sl_View_Factory as ViewFactory;

class Firstpage extends \Sl_Listener_Abstract implements \Sl\Listener\View\Dashboard\Dashboard, \Sl_Listener_View_Interface, \Sl_Listener_Router_Interface {

    protected $_onHome = false;
    
    public function onHeadLink(\Sl_Event_View $event) {
        if(AclService::isAutorized() && $this->_onHome) {
            $event->getView()->headLink()->appendStylesheet('/kcomments/main/postfeed.css');
        }
    }

    public function onHeadScript(\Sl_Event_View $event) {
        if(AclService::isAutorized() && $this->_onHome) {
            $event->getView()->headScript()
                                ->appendFile('/js/fuelux/fuelux.js')            
                                ->appendFile('/main/main/firstpage.js')
                                ->appendFile('/kcomments/main/postfeed.js')
                                ;
        }
    }
    
    public function onDashboard(\Sl_Event_View $event) {
        if(AclService::isAutorized()) {
            $ajaxcreate =   AclService::isAllowed(
                                AclService::joinResourceName(array(
                                    'type' => \Sl_Service_Acl::RES_TYPE_MVC,
                                    'module' => 'crm',
                                    'controller' => 'task',
                                    'action' => 'ajaxcreate'
                                )), AclService::PRIVELEGE_ACCESS
                            );
            $ajaxedit   =   AclService::isAllowed(
                                AclService::joinResourceName(array(
                                    'type' => \Sl_Service_Acl::RES_TYPE_MVC,
                                    'module' => 'crm',
                                    'controller' => 'task',
                                    'action' => 'ajaxcreate'
                                )), AclService::PRIVELEGE_ACCESS
                            );
            $list       =   AclService::isAllowed(
                                AclService::joinResourceName(array(
                                    'type' => \Sl_Service_Acl::RES_TYPE_MVC,
                                    'module' => 'crm',
                                    'controller' => 'task',
                                    'action' => 'list'
                                )), AclService::PRIVELEGE_ACCESS
                            );
            echo \Sl\View\Factory::getView()->partial('partials/taskslistview.phtml', array(
                'canCreate' => $ajaxcreate,
                'canEdit' => $ajaxedit,
                'canView' => $list,
            ));
        }
    }

    public function onAfterContent(\Sl_Event_View $event) {}
    public function onBeforeContent(\Sl_Event_View $event) {}
    public function onBeforePageHeader(\Sl_Event_View $event) {}
    public function onBodyBegin(\Sl_Event_View $event) {}
    public function onBodyEnd(\Sl_Event_View $event) {}
    public function onContent(\Sl_Event_View $event) {}
    public function onFooter(\Sl_Event_View $event) {}
    public function onHeadTitle(\Sl_Event_View $event) {}
    public function onHeader(\Sl_Event_View $event) {}
    public function onLogo(\Sl_Event_View $event) {}
    public function onNav(\Sl_Event_View $event) {}
    public function onPageOptions(\Sl_Event_View $event) {}

    public function onAfterInit(\Sl_Event_Router $event) {
        
    }

    public function onBeforeInit(\Sl_Event_Router $event) {
        
    }

    public function onDispatchLoopShutdown(\Sl_Event_Router $event) {
        
    }

    public function onDispatchLoopStartup(\Sl_Event_Router $event) {
        
    }

    public function onGetRequest(\Sl_Event_Router $event) {
        
    }

    public function onGetResponse(\Sl_Event_Router $event) {
        
    }

    public function onPostDispatch(\Sl_Event_Router $event) {
        
    }

    public function onPreDispatch(\Sl_Event_Router $event) {
        
    }

    public function onRouteShutdown(\Sl_Event_Router $event) {
        if($event->getRequest()->getParam('module') == 'home') {
            if($event->getRequest()->getParam('controller') == 'main') {
                if($event->getRequest()->getParam('action') == 'list') {
                    $this->_onHome = true;
                }
            }
        }
    }

    public function onRouteStartup(\Sl_Event_Router $event) {
        
    }

    public function onSetRequest(\Sl_Event_Router $event) {
        
    }

    public function onSetResponse(\Sl_Event_Router $event) {
        
    }

}
    