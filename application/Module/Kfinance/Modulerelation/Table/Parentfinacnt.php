<?php

namespace Application\Module\Kfinance\Modulerelation\Table;

class Parentfinacnt extends \Sl\Modulerelation\DbTable {

    protected $_name = 'kfinance_finacnt_finacnt';
    protected $_primary = 'id';
    protected $_referenceMap = array(
        'Application\Module\Kfinance\Model\Finacnt' => array(
            'columns' => 'finacnt_id',
            'refTableClass' => 'Application\Module\Kfinance\Model\Table\Finacnt',
            'refColums' => 'id'),
        'reverse' => array(
            'columns' => 'finacnt2_id',
            'refTableClass' => 'Application\Module\Kfinance\Model\Table\Finacnt',
            'refColums' => 'id'),
    );

}