<? namespace Application\Module\Kfinance\Listener;
use Sl\Module\Home\Model as Models;
class Acnttrees extends \Sl_Listener_Abstract implements \Sl_Listener_Model_Interface
{
        
    public function onBeforeSave(\Sl_Event_Model $event){
            
        $object = $event -> getModel();
        
        if ($object instanceof Models\Acnt && !$object->getId()){
            
            \Sl_Model_Factory::mapper($object)->prepareNodesForInsert($object);
            
         //   UPDATE my_tree SET right_key = right_key + 2, left_key = IF(left_key > $right_key, left_key + 2, left_key) WHERE right_key >= $right_key
            
        }
    }
    
    public function onAfterSave(\Sl_Event_Model $event){
        
    }
    
}