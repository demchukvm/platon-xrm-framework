<?

namespace Application\Module\Kfinance\Listener;

use Sl\Module\Home\Model as Models;
use Sl\Service\Accounting as Accounting;
use Sl\Service\Helper as Helper;

class Accountingdoctranslation extends \Sl_Listener_Abstract implements \Sl_Listener_Model_Interface, \Sl\Listener\Model\Table {

    public function onBeforeQuery(\Sl\Event\Table $event) {
        //Вибір транзакцій всіх дочірніх елементів від дерева
        
        $model = $event->getModel();
        error_reporting(E_ERROR);
        if ($model instanceof \Sl\Module\Home\Model\Transaction && $acnt_alias = Accounting::getAcntAliasByTx($model)) {

            $select = $event->getQuery();
            $table_name = \Sl_Model_Factory::dbTable($model)->info('name');
            $acnt_name = \Sl_Model_Factory::dbTable(Helper::getModelnameByAlias($acnt_alias), Helper::getModulenameByAlias($acnt_alias))->info('name');
            $select->join(array('acnt' => $acnt_name), $table_name . '.account_id = acnt.id', array());
            $where = $select->getPart(\Zend_Db_Select::WHERE);
          
            $select->reset(\Zend_Db_Select::WHERE);

            $where = preg_replace(array('/' . $table_name . '.left_key/', '/' . $table_name . '.right_key/'), array('acnt.left_key', 'acnt.right_key'), implode(' ', $where));
            $select->where($where);
            // echo $select;
            //  die;
        }
    }

    public function onAfterSave(\Sl_Event_Model $event) {

        $model = $event->getModel();

        if (Accounting::isModelDeal($model)) {
            //error_reporting(E_ERROR);

            $config = Accounting::configDocumenting();
            $model_alias = \Sl\Service\Helper::getModelAlias($model);
            $acnt = \Sl_Model_Factory::object('Sl\Module\Home\Model\Acnt');

            if ($model->getActive() && $config->{$model_alias} && count($config->{$model_alias}->toArray())) {
                $config = $config->{$model_alias};
                $relations = \Sl_Modulerelation_Manager::getObjectsRelations($model, $acnt, array(\Sl_Modulerelation_Manager::RELATION_ONE_TO_ONE), 'main');
                if (count($relations)) {

                    if (count($relations) > 1) {

                        throw new \Exception($model->findModelName() . ' has more than 1 relation to ' . $acnt->findModelName());
                    }

                    $relation = current($relations);
                    if (!$model->issetRelated($relation->getName())) {
                        $model = \Sl_Model_Factory::mapper($model)->findRelation($model, $relation);
                    }

                    if (count($model->fetchRelated($relation->getName())) != 1) {

                        throw new \Exception($model->findModelName() . ' has not 1 related acnt');
                    }

                    $acnt = $model->fetchOneRelated($relation->getName());


                    foreach ($config->toArray() as $relationname => $config_array) {
                        $reason_relation = \Sl_Modulerelation_Manager::getRelations($acnt, $relationname);
                        if (!($reason_relation instanceof \Sl\Modulerelation\Modulerelation)) {
                            throw new \Exception($acnt->findModelName() . ' has not relation ' . $relationname);
                        }

                        if (!$acnt->issetRelated($relationname))
                            $acnt = \Sl_Model_Factory::mapper($acnt)->findRelation($acnt, $reason_relation);

                        //Якщо встановлене автоматичне створення
                        if (!count($acnt->fetchRelated($relationname)) &&
                                $config->{$relationname}->{Accounting::DOCUMENTING_AUTOCREATE_KEY} &&
                                $config->{$relationname}->{Accounting::FIELDS_KEY} &&
                                Accounting::testConditions($model, $config->{$relationname}->{Accounting::DOCUMENTING_AUTOCREATE_KEY})
                        ) {

                            $related_object = $reason_relation->getRelatedObject($acnt);
                            $related_object = Accounting::fillByConfig($model, $related_object, $config->{$relationname}->{Accounting::FIELDS_KEY});
                            $related_object->assignRelated($relationname, array($acnt->getId()));



                            $related_object = \Sl_Model_Factory::mapper($related_object)->save($related_object);
                        }
         
                    }
                }
            }
        }

        if (Accounting::isModelDocument($model)) {
            error_reporting(E_ERROR);
            $before_update_model = $event->getModelBeforeUpdate();
            $config = Accounting::configTransactioning();
            $alias = \Sl\Service\Helper::getModelAlias($model);
            $acnt = \Sl_Model_Factory::object('Sl\Module\Home\Model\Acnt');

            $acnt_relations = \Sl_Modulerelation_Manager::getObjectsRelations($model, $acnt, array(\Sl_Modulerelation_Manager::RELATION_ONE_TO_ONE), 'main');
            if (count($acnt_relations)) {

                if (count($acnt_relations) > 1) {

                    throw new \Exception($model->findModelName() . ' has more than 1 relation to ' . $acnt->findModelName());
                }

                $acnt_relation = current($acnt_relations);
                if (!$model->issetRelated($acnt_relation->getName())) {
                    $model = \Sl_Model_Factory::mapper($model)->findRelation($model, $acnt_relation);
                }

                if (count($model->fetchRelated($acnt_relation->getName())) != 1) {

                    throw new \Exception($model->findModelName() . ' has not 1 related acnt');
                }

                $model_acnt = $model->fetchOneRelated($acnt_relation->getName());


                $relations = \Sl_Modulerelation_Manager::getRelations($model);
                //створення транзакції, якщо документ перейшов у фінальний стан

                if (Accounting::isModelFinal($model) &&
                        (!$before_update_model->getId() || !Accounting::isModelFinal($before_update_model)) && //Якщо об'єкт перейшов у фінальний статус
                        $model->getActive() && $config->{$alias} && count($config->{$alias}->toArray()) //Якщо встановлені правила створення транзакцій
                ) {


                    foreach ($config->{$alias}->toArray() as $relation_name => $rel_options) {
                        if (!($relation = $relations[$relation_name]))
                            continue;

                        if (
                                ($tx_alias = Accounting::getTxClass($relation->getRelatedObject($model))) && // якщо визначений клас транзакцій для дерева acnt
                                ($acnt_alias = Accounting::getAcntClass($relation->getRelatedObject($model)))) { // якщо визначений клас acnt для обліку 

                            list($tx_module, $tx_model) = explode(\Sl\Service\Helper::MODEL_ALIAS_SEPARATOR, $tx_alias);
                            //list($acnt_module, $acnt_model) = explode(\Sl\Service\Helper::MODEL_ALIAS_SEPARATOR,$acnt_alias);

                            if (!$model->issetRelated($relation->getName())) {
                                $model = \Sl_Model_Factory::mapper($model)->findRelation($model, $relation);
                            }


                            foreach ($model->fetchRelated($relation->getName()) as $id => $acnt) {
                                $tx = \Sl_Model_Factory::object($tx_model, $tx_module);
                                $acnt_id = false;
                                if ($alias == \Sl\Service\Helper::getModelAlias($relation->getRelatedObject($model))) {
                                    //якщо облік ведеться по статті обліку, а не по прив'язаному по ній об'єкту, в tx передати поточний id
                                    $acnt_id = $id;
                                }



                                //якщо облік ведеться не по статті обліку, а по по вказаному в acnt прив'язаному об'єкту, треба знайти його поточну статтю обліку
                                if ($alias != \Sl\Service\Helper::getModelAlias($relation->getRelatedObject($model))) {
                                    if (!is_object($acnt) || !$acnt->issetRelated($rel_options['acnt'])) {
                                        $acnt_model = \Sl_Model_Factory::mapper($relation->getRelatedObject($model))->findExtended($id, array($rel_options['acnt']));
                                    }

                                    if (count($acnt_model->fetchRelated($rel_options['acnt']))) {
                                        $acnt_id = current(array_keys($acnt_model->fetchRelated($rel_options['acnt'])));
                                    }
                                }
                                //print_R(array($acnt_model->fetchRelated(), $acnt_id, $rel_options, $relation->getName())); die; 
                                if (!$acnt_id)
                                    continue; //Якщо не знайшли статтю обліку, далі можна нічого не робити
                                unset($rel_options['acnt']);

                                $tx = \Sl\Service\Model\Adapter::translate($tx, $model, $rel_options);



                                $tx->setAccountId($acnt_id);
                                $tx->setDocId($model_acnt->getId());
                                $tx->setDate(date('Y-m-d'));
                                \Sl_Model_Factory::mapper($tx)->save($tx);
                            }
                        }
                    }
                }

                if (!Accounting::isModelFinal($model) && Accounting::isModelFinal($before_update_model) &&
                        $config->{$alias} && count($config->{$alias}->toArray())) {

                    //видалення транзакції, якщо документ перейшов у фінальний стан
                    $alias = \Sl\Service\Helper::getModelAlias($model);

                    foreach ($config->{$alias}->toArray() as $relation_name => $rel_options) {
                        if (!($relation = $relations[$relation_name]))
                            continue;
                        if ($tx_alias = Accounting::getTxClass($relation->getRelatedObject($model))) { // якщо визначений клас транзакцій для дерева acnt

                            list($tx_module, $tx_model) = explode(\Sl\Service\Helper::MODEL_ALIAS_SEPARATOR, $tx_alias);
                            \Sl_Model_Factory::mapper($tx_model, $tx_module)->voidByDocId($model_acnt->getId());
                        }
                    }
                }
            }
        }

        /* створення acnt для моделей */

        if (!($model instanceof \Sl\Module\Home\Model\Acnt) && $relations = Accounting::AcntCreation($model)) {

            foreach ($relations as $relation_name) {
                $relation = \Sl_Modulerelation_Manager::getRelations($model, $relation_name);
                if (!($relation instanceof \Sl\Modulerelation\Modulerelation))
                    continue;
                $acnt = $relation->getRelatedObject($model);
                if (!($acnt instanceof \Sl\Module\Home\Model\Acnt))
                    continue;
                //$relations = \Sl_Modulerelation_Manager::getObjectsRelations($model,$acnt,array(\Sl_Modulerelation_Manager::RELATION_ONE_TO_ONE),'main');


                if (!$model->issetRelated($relation->getName())) {
                    $model = \Sl_Model_Factory::mapper($model)->findRelation($model, $relation);
                }

                $acnts_arr = $model->fetchRelated($relation->getName());

                if (!count($acnts_arr)) {
                    $acnt->assignRelated($relation->getName(), array($model->getId() => $model));
                    if ($relation->getOption('main'))
                        $acnt->setMasterRelation($relation->getName());
                    \Sl_Model_Factory::mapper($acnt)->save($acnt);
                    $model = \Sl_Model_Factory::mapper($model)->findRelation($model, $relation);
                }
            }
        }
    }

    public function onBeforeSave(\Sl_Event_Model $event) {
        
    }

}
