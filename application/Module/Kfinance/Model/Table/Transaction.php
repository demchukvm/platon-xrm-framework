<?php
namespace Application\Module\Kfinance\Model\Table;

class Transaction extends \Sl\Model\DbTable\DbTable {
	protected $_name = 'kfinance_transaction';
	protected $_primary = 'id';

}