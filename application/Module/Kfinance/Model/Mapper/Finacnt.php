<?php
namespace Application\Module\Kfinance\Model\Mapper;

class Finacnt extends \Sl\Module\Home\Model\Mapper\Acnt {
	protected function _getMappedDomainName() {
        return '\Application\Module\Kfinance\Model\Finacnt';
    }

    protected function _getMappedRealName() {
        return '\Application\Module\Kfinance\Model\Table\Finacnt';
    }
}