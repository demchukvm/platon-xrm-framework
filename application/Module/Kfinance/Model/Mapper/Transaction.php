<?php
namespace Application\Module\Kfinance\Model\Mapper;

class Transaction extends \Sl_Model_Mapper_Abstract {
            protected function _getMappedDomainName() {
            return '\Application\Module\Kfinance\Model\Transaction';
        }

        protected function _getMappedRealName() {
            return '\Application\Module\Kfinance\Model\Table\Transaction';
        }
}