<?php
namespace Application\Module\Kfinance\Model;

class Finacnt  extends  \Sl\Module\Home\Model\Acnt {

	protected $_code;

	
	public function setCode ($code) {
		$this->_code = $code;
		return $this;
	}

	
	public function getCode () {
		return $this->_code;
	}

    public function __toString(){
        $name = trim(implode(' ',array($this->getCode(), $this->getName())));
        return (strlen($name)?$name:parent::__toString());
        
    }

}