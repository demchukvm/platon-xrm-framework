<?php

namespace Application\Module\Kfinance\Model;

class Transaction extends \Sl_Model_Abstract {

    protected $_name; 
    protected $_ammount;


    protected $_type;
    protected $_date;
    protected $_status;
    
    protected $_lists = array(
        'type' => 'kfinance_transaction_type',
        'status' => 'kfinance_fin_status',
    );

    

	public function setAmmount ($ammount) {
		$this->_ammount = $ammount;
		return $this;
	}

	public function setName($name) {
        $this->_name = $name;
        return $this;
    }

    public function setType($type) {
        $this->_type = $type;
        return $this;
    }

    public function setDate($date) {
        if ($date instanceof \DateTime) {
            $date = $date->format(self::FORMAT_);
        }
        $this->_date = $date;
        return $this;
    }

    public function setStatus($status) {
        $this->_status = $status;
        return $this;
    }

    public function getName() {
        return $this->_name;
    }

    public function getType() {
        return $this->_type;
    }

    public function getDate($as_object = false) {
        if ($as_object) {
            return \DateTime::createFromFormat(self::FORMAT_, $this->getDate());
        }
        return $this->_date;
    }

    public function getStatus() {
        return $this->_status;
    }

	public function getAmmount () {
		return $this->_ammount;
	}
}