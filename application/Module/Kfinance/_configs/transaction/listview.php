<?php
return array (
    'name' => array(
        'sortable' => true,
        'searchable' => true,
    ),
    'type' => array(
        'sortable' => true,
        'searchable' => true,
    ),
    'date' => array(
        'type' => 'date',
        'sortable' => true,
        'searchable' => true,
    ),
    'status' => array(
        'sortable' => true,
        'searchable' => true,
    ),
    'ammount' => array(
        'sortable' => true,
        'searchable' => true,
        'calculate' => 'sum',
    ),
);
