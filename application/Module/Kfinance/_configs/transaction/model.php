<?php
return array (
  'name' => 
  array (
    'label' => '#',
    'type' => 'text',
  ),
  'type' => 
  array (
    'label' => 'Type',
    'type' => 'select',
  ),
  'date' => 
  array (
    'label' => 'Date',
    'type' => 'timestamp',
  ),
  'status' => 
  array (
    'label' => 'Status',
    'type' => 'select',
  ),
  'ammount' => 
  array (
    'label' => 'Ammount',
    'type' => 'float',
  ),
);
