<?php
namespace Application\Module\Kfinance;

use Sl_Module_Abstract as AbstractModule;

class Module extends AbstractModule { 

	public function getListeners() {
		return array( new Listener\Acnttrees($this));
	}
}