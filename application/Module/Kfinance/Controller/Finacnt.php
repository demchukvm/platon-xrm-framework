<?php
namespace Application\Module\Kfinance\Controller;
use Sl\Service\Accounting as Accounting;

class Finacnt extends \Sl\Module\Home\Controller\Acnt {
    protected $_tree_relation = 'parentfinacnt';
    //protected $_transaction = "Sl\Module\Finance\Model\Otransaction";
    protected $_acnt = "Sl\Module\Finance\Model\Finacnt";
    
    public function listtreeAction() {
        $this -> view -> headScript() -> appendFile('/js/libs/jquery.cookie.js');
        $this -> view -> headScript() -> appendFile('/js/libs/jquery.hotkeys.js');
        $this -> view -> headScript() -> appendFile('/js/libs/jquery.jstree.js');
        //$this->view->headLink()->appendStylesheet('/css/jstree/apple/style.css');
        $this -> view -> relation_name = $this->_tree_relation;

        $edit_resource = \Sl_Service_Acl::joinResourceName(array(
            'type' => \Sl_Service_Acl::RES_TYPE_MVC,
            'module' => $this -> _getModule() -> getName(),
            'controller' => $this -> getModelName(),
            'action' => 'ajaxtreeedit',
        ));
        $create_resource = \Sl_Service_Acl::joinResourceName(array(
            'type' => \Sl_Service_Acl::RES_TYPE_MVC,
            'module' => $this -> _getModule() -> getName(),
            'controller' => $this -> getModelName(),
            'action' => 'ajaxtreecreate',
        ));
        
        $create_modulerelation = \Sl_Service_Acl::joinResourceName(array(
            'type' => \Sl_Service_Acl::RES_TYPE_MVC,
            'module' => 'home',
            'controller' => 'admin',
            'action' => 'createmodulerelation',
        ));
        //error_reporting(E_ALL);
        //findRootNode
        $this -> view -> priv_update = \Sl_Service_Acl::isAllowed($edit_resource, \Sl_Service_Acl::PRIVELEGE_ACCESS);
        $this -> view -> priv_create = \Sl_Service_Acl::isAllowed($create_resource, \Sl_Service_Acl::PRIVELEGE_ACCESS);
        $this -> view -> priv_create_modulerelation = \Sl_Service_Acl::isAllowed($create_modulerelation, \Sl_Service_Acl::PRIVELEGE_ACCESS);
        
     //   $transaction_obj = \Sl_Model_Factory::object($this->_transaction);
        
        
        
        $this -> view -> acnt_class = \Sl\Service\Helper::getModelAlias($this->getModelName(), $this->_getModule()->getName());
        $this -> view -> transaction_class = \Sl\Service\Helper::getModelAlias($transaction_obj); 
        
     
    }

    public function ajaxlisttreeAction() {
        
        //error_reporting(E_ALL);
        
        try {
            $id = $this -> getRequest() -> getParam('id', 0);
            $relation_name = $this -> getRequest() -> getParam('relation', false);

            $default_object = \Sl_Model_Factory::object($this -> getModelName(), $this -> _getModule());
            //$relation = \Sl_Modulerelation_Manager::getRelations($default_object, $relation_name);
            $children = \Sl_Model_Factory::mapper($default_object) -> fetchByParent($id, $relation_name, true);
        } catch (\Exception $e){
            //echo $e->getMessage().'<br>';
            //print_r($e->getTrace());
        }
        
        $result = array();
        
        if ($id == 0 && !count($children)) {
            
            $children = array(array('object'=>\Sl_Model_Factory::mapper($default_object) -> createRootNode($this -> view -> translate(implode('_', array(
                                'title',
                                strtolower($this -> _getModule() -> getName()),
                                strtolower($this -> getModelName()),
                                'tree'
                            ))))));

        }
        $acnt_obj = \Sl_Model_Factory::object($this->getModelName(), $this->_getModule());
        foreach ($children as $node) {
            
            
            $result[] = array(
                'state' => $node['children'] || count(\Sl_Modulerelation_Manager::getRelationsByOption(get_class($acnt_obj),'acnt',$node['object']->getId())) ? 'closed' : '',
                'data' => $node['object'] -> __toString(),
                'attr' => array('id' => 'node_' . $node['object'] -> getId(), 
                                
                                ),
            );
        }
        
        
        foreach ($relations = \Sl_Modulerelation_Manager::getRelationsByOption(get_class($acnt_obj),'acnt',$id) as $relation){
                $dest_obj = $relation->getRelatedObject($acnt_obj);    
                
              
                $result[] = array(
                     
                    'data' => $dest_obj->findModelName(),
                    'attr' => array('id' => 'node_' . $relation->getName(),
                                    'node_type' => (Accounting::isTxRelationRegistered($relation)?'relation':'_relation'),
                                    ),
                );
                
                
        }
        
        echo \Zend_Json::encode($result);
        exit();
    }

}
