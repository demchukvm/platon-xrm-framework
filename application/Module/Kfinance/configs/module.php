<?php

return array(
    'forms' =>
    array(
        'model_transaction_form' =>
        array(
            'date' =>
            array(
                'type' => 'date',
            ),
            'id' =>
            array(
                'type' => 'hidden',
            ),
        ),
        'model_finacnt_form' =>
        array(
            'modulerelation_parentfinacnt' =>
            array(
                'label' => 'Parent',
                'readonly' => 'true',
            ),
        ),
    ),
    'modulerelations' =>
    array(
        0 =>
        array(
            'type' => '21',
            'db_table' => 'Application\\Module\\Kfinance\\Modulerelation\\Table\\Parentfinacnt',
            'options' =>
            array(
            ),
        ),
    ),
    'navigation_pages' =>
    array(
        0 =>
        array(
            'id' => 'kfinance',
            'label' => 'Finance',
            'href' => '#',
            'order' => 40,
        ),
        1 =>
        array(
            'id' => 'kcrm',
            'label' => 'CRM',
            'href' => '#',
            'order' => 50,
        ),
        2 =>
        array(
            'id' => 'kfin_transaction_list',
            'label' => 'Transactions',
            'module' => 'kfinance',
            'controller' => 'transaction',
            'action' => 'list',
            'parent' => 'kfinance',
        ),
        3 =>
        array(
            'label' => 'Chart of Financial Accounts',
            'module' => 'kfinance',
            'controller' => 'finacnt',
            'action' => 'listtree',
            'parent' => 'kfinance',
        ),
    ),
    'lists' =>
    array(
        'kfinance_transaction_type' =>
        array(
            1 => 'Debet',
            2 => 'Credit',
        ),
        'kfinance_fin_status' =>
        array(
            0 => 'Draft',
            1 => 'Payed',
            2 => 'Confirmed',
        ),
        
    ),
    'left_navigation_pages' =>
    array(
        0 =>
        array(
            'id' => 'kfinance1',
            'label' => 'Finance',
            'href' => '#',
            'order' => 90,
            'icon' => 'fa fa-money fa-lg',
        ),
        1 =>
        array(
            'id' => 'kfin_transaction_list',
            'label' => 'Transactions',
            'module' => 'kfinance',
            'controller' => 'transaction',
            'action' => 'list',
            'parent' => 'kfinance1',
        ),
        2 =>
        array(
            'label' => 'Chart of Financial Accounts',
            'module' => 'kfinance',
            'controller' => 'finacnt',
            'action' => 'listtree',
            'parent' => 'kfinance1',
        ),
    ),
);
