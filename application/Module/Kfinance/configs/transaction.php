<?php
return array (
  'model' => 
  array (
    'name' => 
    array (
      'label' => 'NAME',
      'type' => 'text',
    ),
    'type' => 
    array (
      'label' => 'TYPE',
      'type' => 'text',
    ),
    'date' => 
    array (
      'label' => 'DATE',
      'type' => 'text',
    ),
    'status' => 
    array (
      'label' => 'STATUS',
      'type' => 'text',
    ),
    'create' => 
    array (
      'label' => 'CREATE',
      'type' => 'text',
    ),
    'id' => 
    array (
      'label' => 'ID',
      'type' => 'text',
    ),
    'active' => 
    array (
      'label' => 'ACTIVE',
      'type' => 'checkbox',
    ),
    'archived' => 
    array (
      'label' => 'ARCHIVED',
      'type' => 'text',
    ),
    'extend' => 
    array (
      'label' => 'EXTEND',
      'type' => 'text',
    ),
    'ammount' => 
    array (
      'label' => 'AMMOUNT',
      'type' => 'text',
    ),
  ),
);
