<?php
return array (
  'model' => 
  array (
    'name' => 
    array (
      'label' => 'Name',
      'type' => 'text',
    ),
    'master_relation' => 
    array (
      'label' => 'MASTER_RELATION',
      'type' => 'hidden',
    ),
    'code' => 
    array (
      'label' => 'Code',
      'type' => 'text',
    ),
    'create' => 
    array (
      'label' => 'CREATE',
      'type' => 'text',
    ),
    'id' => 
    array (
      'label' => 'ID',
      'type' => 'hidden',
    ),
    'active' => 
    array (
      'label' => 'ACTIVE',
      'type' => 'hidden',
    ),
  ),
);
