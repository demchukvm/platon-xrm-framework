<?php
namespace Application\Module\Kcompany\Controller;

use Sl_Model_Factory as ModelFactory;

class Circle extends \Sl_Controller_Model_Action {

    /**
     * Криги пользователя
     */
    public function viewAction() {
        $this->view->sub_title = '';
        $this->view->title = $this->view->translate('My circles');
        
        $circles = ModelFactory::mapper('circle', $this->_getModule())->fetchAllAllowed();
        $this->view->circles = $circles;
    }
    
    public function reassignAction() {
        $circles = ModelFactory::mapper('circle', $this->_getModule())->fetchAll();
        
        foreach($circles as $circle) {
            $circle = ModelFactory::mapper($circle)->findRelation($circle, 'circleusers');
            $circle = ModelFactory::mapper($circle)->findRelation($circle, 'circleowner');
            
            $users = $circle->fetchRelated('circleusers');
            $uids = array_keys($users);
            $rand = rand(0, count($uids)-1);
            \Sl\Service\Essentials::write($circle, 'circleusers', array($users[$uids[$rand]]));
            continue;
            
            $owner = $circle->fetchOneRelated('circleowner');
            $circle->assignRelated('circleowner', array());
            
            $users = $circle->fetchRelated('circleusers');
            $already_added = false;
            if($owner) {
                foreach($users as $user) {
                    if($user->getId() == $owner->getId()) {
                        $already_added = true;
                    }
                }
                if(!$already_added) {
                    $users[] = $owner;
                }
            }
            $circle->assignRelated('circleusers', $users);
            ModelFactory::mapper($circle)->save($circle);
        }
        die('Done');
    }
}