<?php
return array (
  'model' => 
  array (
    'name' => 
    array (
      'label' => 'Name',
      'type' => 'text',
    ),
    'code' => 
    array (
      'label' => 'Code',
      'type' => 'text',
        'validators' => 
        array (
            'StringLength' => array(
            
                'min' => 2, 
                'max' => 2,
            ),
           
        ),  
    ),
    'description' => 
    array (
      'label' => 'Description',
      'type' => 'text',
    ),
    'create' => 
    array (
      'label' => 'Create',
      'type' => 'text',
    ),
    'id' => 
    array (
      'label' => 'ID',
      'type' => 'hidden',
    ),
    'active' => 
    array (
      'label' => 'ACTIVE',
      'type' => 'checkbox',
    ),
    'archived' => 
    array (
      'label' => 'ARCHIVED',
      'type' => 'text',
    ),
    'extend' => 
    array (
      'label' => 'EXTEND',
      'type' => 'text',
    ),
  ),
);
