<?php
return array (
  'forms' => 
  array (
    'model_department_form' => 
    array (
      'code' => 
      array (
        'required' => true,
        'validators' => 
        array (
          'sl' => 
          array (
            'name' => 'StringLength',
            'options' => 
            array (
              'min' => 2,
              'max' => 2,
            ),
          ),
        ),
        'sort_order' => 20,
      ),
      'name' => 
      array (
        'sort_order' => 30,
      ),
      'description' => 
      array (
        'sort_order' => 40,
      ),
      'modulerelation_pipedepartment' => 
      array (
        'required' => true,
        'label' => 'Pipe',
        'sort_order' => 10,
      ),
      'modulerelation_departmentuser' => array(
          'label' => 'Manager',
          
      ),
    ),
    'model_circle_form' => 
    array (
      'modulerelation_circleusers' => 
      array (
        'label' => 'Members',
      ),
    ),
  ),
  'modulerelations' => 
  array (
    0 => 
    array (
      'type' => '22',
      'db_table' => 'Application\\Module\\Kcompany\\Modulerelation\\Table\\Departmentuser',
      'options' => 
      array (
      ),
    ),
    1 => 
    array (
      'type' => '22',
      'db_table' => 'Application\\Module\\Kcompany\\Modulerelation\\Table\\Circleusers',
      'options' => 
      array (
        'essentials' => 
        array (
          0 => 'auth.user',
        ),
      ),
    ),
    2 => 
    array (
      'type' => '21',
      'db_table' => 'Application\\Module\\Kcompany\\Modulerelation\\Table\\Circleowner',
      'options' => 
      array (
      ),
    ),
    3 => 
    array (
      'type' => '11',
      'db_table' => 'Application\\Module\\Kcompany\\Modulerelation\\Table\\Departmentmaincircle',
      'options' => 
      array (
      ),
    ),
    4 => 
    array (
      'type' => '21',
      'db_table' => 'Application\\Module\\Kcompany\\Modulerelation\\Table\\Circleproduct',
      'options' => 
      array (
      ),
    ),
    5 => 
    array (
      'type' => '21',
      'db_table' => 'Application\\Module\\Kcompany\\Modulerelation\\Table\\Circledeal',
      'options' => 
      array (
      ),
    ),
  ),
  'navigation_pages' => 
  array (
  ),
  'left_navigation_pages' => 
  array (
    0 => 
    array (
      'parent' => 'others',
      'label' => 'Departments',
      'controller' => 'department',
      'module' => 'kcompany',
      'action' => 'list',
      'order' => 100,
      'icon' => 'fa fa-group  fa-lg',
    ),
    1 => 
    array (
      'parent' => 'others',
      'label' => 'Circles',
      'controller' => 'circle',
      'module' => 'kcompany',
      'action' => 'view',
      'route' => 'kcompany_circles',
      'order' => 100,
    ),
  ),
  'listview_options' => 
  array (
    'department' => 
    array (
      'name' => 
      array (
        'order' => 10,
        'label' => 'Name',
      ),
      'code' => 
      array (
        'order' => 20,
        'label' => 'Code',
      ),
      'description' => 
      array (
        'order' => 30,
        'label' => 'Description',
      ),
      'archived' => 
      array (
        'order' => 40,
        'label' => 'ARCHIVED',
      ),
      'extend' => 
      array (
        'order' => 50,
        'label' => 'EXTEND',
      ),
    ),
    'circle' => 
    array (
      'name' => 
      array (
        'order' => 10,
        'label' => 'Circle',
      ),
      'archived' => 
      array (
        'order' => 20,
        'label' => 'ARCHIVED',
      ),
      'extend' => 
      array (
        'order' => 30,
        'label' => 'EXTEND',
      ),
    ),
  ),
);
