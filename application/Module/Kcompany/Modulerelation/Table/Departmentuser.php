<?php
namespace Application\Module\Kcompany\Modulerelation\Table;

class Departmentuser extends \Sl\Modulerelation\DbTable {
	protected $_name = 'kcompany_department_user';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Application\Module\Kcompany\Model\Department' => array(
			'columns' => 'department_id',
			'refTableClass' => 'Application\Module\Kcompany\Model\Table\Department',
		'refColums' => 'id'),
                		'Sl\Module\Auth\Model\User' => array(
			'columns' => 'user_id',
			'refTableClass' => 'Sl\Module\Auth\Model\Table\User',
				'refColums' => 'id'	),
	);
}