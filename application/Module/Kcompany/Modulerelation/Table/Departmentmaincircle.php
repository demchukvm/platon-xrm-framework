<?php
namespace Application\Module\Kcompany\Modulerelation\Table;

class Departmentmaincircle extends \Sl\Modulerelation\DbTable {
	protected $_name = 'kcompany_department_main_circle';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Application\Module\Kcompany\Model\Circle' => array(
			'columns' => 'circle_id',
			'refTableClass' => 'Application\Module\Kcompany\Model\Table\Circle',
		'refColums' => 'id'),
                		'Application\Module\Kcompany\Model\Department' => array(
			'columns' => 'department_id',
			'refTableClass' => 'Application\Module\Kcompany\Model\Table\Department',
				'refColums' => 'id'	),
	);
}