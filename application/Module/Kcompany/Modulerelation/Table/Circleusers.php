<?php

namespace Application\Module\Kcompany\Modulerelation\Table;

class Circleusers extends \Sl\Modulerelation\DbTable {

    protected $_name = 'kcompany_circle_users';
    protected $_primary = 'id';
    protected $_referenceMap = array(
        'Application\Module\Kcompany\Model\Circle' => array(
            'columns' => 'circle_id',
            'refTableClass' => 'Application\Module\Kcompany\Model\Table\Circle',
            'refColums' => 'id'),
        'Sl\Module\Auth\Model\User' => array(
            'columns' => 'user_id',
            'refTableClass' => 'Sl\Module\Auth\Model\Table\User',
            'refColums' => 'id'),
    );

}
