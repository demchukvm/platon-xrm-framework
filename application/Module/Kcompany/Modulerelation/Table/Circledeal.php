<?php
namespace Application\Module\Kcompany\Modulerelation\Table;

class Circledeal extends \Sl\Modulerelation\DbTable {
	protected $_name = 'kcompany_circle_deal';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Application\Module\Kcompany\Model\Circle' => array(
			'columns' => 'circle_id',
			'refTableClass' => 'Application\Module\Kcompany\Model\Table\Circle',
		'refColums' => 'id'),
                		'Application\Module\Business\Model\Deal' => array(
			'columns' => 'deal_id',
			'refTableClass' => 'Application\Module\Business\Model\Table\Deal',
				'refColums' => 'id'	),
	);
}