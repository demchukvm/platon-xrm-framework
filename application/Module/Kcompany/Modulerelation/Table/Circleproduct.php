<?php
namespace Application\Module\Kcompany\Modulerelation\Table;

class Circleproduct extends \Sl\Modulerelation\DbTable {
	protected $_name = 'kcompany_circle_product';
	protected $_primary = 'id';
	protected $_referenceMap = array(
			'Application\Module\Kcompany\Model\Circle' => array(
			'columns' => 'circle_id',
			'refTableClass' => 'Application\Module\Kcompany\Model\Table\Circle',
		'refColums' => 'id'),
                		'Application\Module\Business\Model\Product' => array(
			'columns' => 'product_id',
			'refTableClass' => 'Application\Module\Business\Model\Table\Product',
				'refColums' => 'id'	),
	);
}