$(document).ready(function(){
    /*$('.b').on('click', '.adder', function(){
        var $this = $(this);
        var $panel = $this.closest('.panel');
        var $list = $panel.find('ul:first');
        $list.find('li:first').toggleClass('hidden');
    });*/
    
    $('.b-circles').each(function(){
        new SlCirclesController(this);
    });
});

function SlCirclesController(wrapper, opts) {
    var self = this;
    
    function Circle(data) {
        var self = this;
        
        var _options = _.extend({
            id: false,
            name: '',
            controller: null
        }, data || {});
        
        var _users = {};
        var _moderators = {};
        
        this.notify = function(type, data) {
            if(_options.controller) {
                _options.controller.notify(type+'.circle', _.extend({
                    circle: self
                }, data));
            }
        };
        
        this.get = function(what) {
            var method = 'get'+what.ucFirst();
            if(self.hasOwnProperty(method)) {
                return self[method]();
            }
            return _options.hasOwnProperty(what)?_options[what]:null;
        };
        
        this.getUser = function(id) {
            return _users.hasOwnProperty(id)?_users[id]:null;
        };
        
        this.addUser = function(user, notify) {
            if(!(user instanceof User)) {
                throw new SlError('user must be instance of User', 400);
            }
            if(self.getUser(user.get('id'))) {
                throw new SlError('user already exists', 401);
            }
            _users[user.get('id')] = user;
            if(false !== notify) {
                self.notify('update');
            }
            return self;
        };
        
        this.getUsers = function() {
            return _.sortBy(_users, function(i){
                return i.get('name');
            });
        };
        
        this.removeUser = function(id, notify) {
            if(id instanceof User) {
                id = id.get('id');
            }
            delete(_users[id]);
            if(false !== notify) {
                self.notify('update');
            }
            return self;
        };
        
        this.isModerator = function(uid) {
            return _moderators.hasOwnProperty(uid);
        };
        
        this.setModerator = function(uid, notify) {
            if(self.getUser(uid)) {
                _moderators[uid] = self.getUser(uid);
            }
            if(false !== notify) {
                self.notify('update');
            }
            return self;
        };
        
        this.unsetModerator = function(uid, notify) {
            if(self.getUser(uid) && _moderators.hasOwnProperty(uid)) {
                delete(_moderators[uid]);
            }
            if(false !== notify) {
                self.notify('update');
            }
            return self;
        };
        
        this.getModerators = function(objects) {
            if(objects) {
                return _moderators;
            } else {
                return _.keys(_moderators);
            }
        };
        
        this.toggleModerator = function(uid, notify) {
            var user;
            if(typeof uid === 'object') {
                user = uid;
            } else {
                user = self.getUser(uid);
            }
            if(self.isModerator(user.get('id'))) {
                self.unsetModerator(user.get('id'), notify);
            } else {
                self.setModerator(user.get('id'), notify);
            }
        };
        
        this.serialize = function() {
            var data = {};
            _.each(_options, function(v, k){
                data[k] = self.get(k);
            });
            return data;
        };
        
        // Initialization ..
        if(!_options.id) {
            throw new SlError('id param is required', 350);
        }
    };
    
    function User(data) {
        var self = this;
        
        var _options = _.extend({
            id: false,
            name: ''
        }, data || {});
        
        this.get = function(what) {
            var method = 'get'+what.ucFirst();
            if(self.hasOwnProperty(method)) {
                return self[method]();
            }
            return _options.hasOwnProperty(what)?_options[what]:null;
        };
        
        this.serialize = function() {
            var data = {};
            _.each(_options, function(v, k){
                data[k] = self.get(k);
            });
            return data;
        };
        
        if(!_options.id) {
            throw new SlError('id param is required', 350);
        }
    };
    
    var _options = _.extend({
        wrapper: $(wrapper || opts.wrapper)
    }, opts || {});
    
    this.notify = function(type, data) {
        self.getWrapper().trigger(type+'.slcircles', _.extend({
            controller: self
        }, data || {}));
    };
    
    this.getWrapper = function() {
        if(!_options.wrapper || !_options.wrapper.length) {
            throw new SlError('Wrapper must be specified', 302);
        }
        return _options.wrapper;
    };
    
    var _circles = {};
    
    /**
     * Возвращает круг по id
     * 
     * @param {int} id
     * @returns {Circle}
     */
    this.getCircle = function(id) {
        return _circles.hasOwnProperty(id)?_circles[id]:null;
    };
    
    /**
     * Добавление круга
     * 
     * @param {Circle} circle
     * @returns {SlCirclesController}
     */
    this.addCircle = function(circle, notify) {
        if(!(circle instanceof Circle)) {
            throw new SlError('circle must be instance of Circle', 300);
        }
        if(self.getCircle(circle.get('id'))) {
            throw new SlError('circle already exists', 301);
        }
        _circles[circle.get('id')] = circle;
        if(false !== notify) {
            self.notify('update');
        }
        return self;
    };
    
    /**
     * Создание круга с добавлением
     * 
     * @param {Object} data
     * @returns {SlCirclesController}
     */
    this.createCircle = function(data, notify) {
        return self.addCircle(new Circle(_.extend(data, { controller: self })), notify);
    };
    
    /**
     * Возвращает все круги
     * 
     * @returns {Array}
     */
    this.getCircles = function() {
        return _circles;
    };
    
    /**
     * Удаляет круг
     * 
     * @param {int|Circle} id
     * @returns {SlCirclesController}
     */
    this.removeCircle = function(id, notify) {
        if(id instanceof Circle) {
            id = id.get('id');
        }
        delete(_circles[id]);
        if(false !== notify) {
            self.notify('update');
        }
        return self;
    };
    
    this.render = function() {
        $('.b-circle', self.getWrapper()).remove();
        _.each(self.getCircles(), function(circle){
            if(_.size(circle.getUsers()) > 0) {
                self.getWrapper().append(_.template($('#circleTemplate').html(), {
                    circle: circle,
                    curUser: new User({ id: current_auth_user.id })
                }));
            }
            $('.b-circle[data-id="'+circle.get('id')+'"]', self.getWrapper()).data('circle', circle);
        });
    };
    
    this.saveCircle = function(circle, success, error) {
        if(typeof circle !== 'object') {
            circle = self.getCircle(circle);
        }
        success = (success && typeof success == 'function')?success:function(){};
        error = (error && (typeof error == 'function'))?error:function(){};
        var d = {
            id: circle.get('id'),
            'ajax_action': 1,
            name: circle.get('name')
        };
        if(_.size(circle.getUsers()) > 0) {
            _.each(circle.getUsers(), function(user){
                d['modulerelation_circleusers['+user.get('id')+']'] = user.get('id');
            });
        } else {
            d['modulerelation_circleusers[]'] = '0';
        } 
        if(_.size(circle.getModerators()) > 0) {
            _.each(circle.getModerators(), function(mid){
                d['essentials[circleusers]['+mid+']'] = mid;
            });
        } else {
            d['essentials[circleusers][]'] = 0;
        }
        $.ajax({
            url: '/kcompany/circle/ajaxedit',
            type: 'POST',
            data: d,
            success: success,
            error: error
        });
    };
    
    self.getWrapper().data('controller', self);
    
    self.getWrapper().on('update.circle.slcircles', function(e, d){
        self.render();
        self.saveCircle(d.circle);
    });
    
    _.each(self.getWrapper().find('.b-circle_data'), function(el){
        var cData = $(el).data();
        self.createCircle(cData, false);
        _.each($(el).find('.b-circle_user-data'), function(user){
            var uData = $(user).data();
            self.getCircle(cData.id).addUser(new User(uData), false);
        });
        _.each(cData.moderators, function(mid){
            self.getCircle(cData.id).setModerator(mid);
        });
    });
    
    self.getWrapper().on('click', '.adder', function(){
        var $this = $(this);
        var $circle = $this.closest('.b-circle');
        var circle = $circle.data('circle');
        if($circle) {
            var $adder = $('.b-circle_ctrl-add', $circle);
            if(!$adder.hasClass('initialized')) {
                $adder.addClass('initialized');
                assignAutocomplete($circle, '.autocomplete', {
                    multiple: false,
                    ajax: {
                        url: '/auth/user/ajaxautocomplete',
                        dataType: 'json',
                        data : function(term, page){
                            if(_.size(circle.getUsers()) > 0) {
                                var f = getFilters($(this).data('wrapper'), this);
                                f.push('id-nin-'+_.map(circle.getUsers(), function(u){ return u.get('id'); }).join(','))
                            }
                            return {name: term, filter_fields: f, fields: ['color'], extraFields: ['alias', 'findInitials']};
                        },
                        results: function (data, page) { // parse the results into the format expected by Select2.
                            var d = [];
                            if (data.result && data.aaData.length){
                               for (var i in data.aaData){
                                   d.push(_.extend(data.aaData[i], {text: data.aaData[i].name, initials: data.aaData[i].findInitials, color: data.aaData[i].color?('#'+data.aaData[i].color):''}));
                               }
                            }
                            return {results:d};
                        }
                    }
                });
            }
            $adder.toggleClass('hidden');
        }
    });
    
    self.getWrapper().on('change', '.b-circle .autocomplete', function(){
        var circle = $(this).closest('.b-circle').data('circle');
        if(circle) {
            circle.addUser(new User($(this).select2('data')));
        }
    });
    
    self.getWrapper().on('click', '.b-circle_ctrl-moderate', function(){
        var $this = $(this);
        var circle = $this.closest('.b-circle').data('circle');
        if(circle) {
            var user = circle.getUser($this.closest('[data-user]').data('user'));
            if(user) {
                if($this.closest('[data-user]').data('current')) {
                    $.confirm('Are you sure?', 'Are you sure you want to remove moderation permissions from this circle?', null, function(){
                        circle.toggleModerator(user);
                    });
                } else {
                    circle.toggleModerator(user);
                }
            }
        }
    });
    
    self.getWrapper().on('click', '.b-circle_ctrl-delete', function(){
        var $this = $(this);
        var circle = $this.closest('.b-circle').data('circle');
        if(circle) {
            var user = circle.getUser($this.closest('[data-user]').data('user'));
            if(user) {
                if($this.closest('[data-user]').data('current')) {
                    $.confirm('Are you sure?', 'Are you sure you want to remove yourself from this circle?', null, function(){
                        circle.removeUser(user);
                    });
                } else {
                    circle.removeUser(user);
                }
                
            }
        }
    });
    
    self.render();
}