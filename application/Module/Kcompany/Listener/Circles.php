<?php
namespace Application\Module\Kcompany\Listener;

use Sl_Model_Factory as ModelFactory;
use Sl_Service_Acl as AclService;

class Circles extends \Sl_Listener_Abstract implements \Sl_Listener_Model_Interface, \Sl_Listener_Router_Interface {
    
    const DEPARTMENT_CIRCLE_RELATION = 'departmentmaincircle';
    const DEPARTMENT_USER_RELATION = 'departmentuser';
    const CIRCLE_USERS_RELATION = 'circleusers';
    const PRODUCT_CIRCLE_RELATION = 'circleproduct';
    const DEAL_CIRCLE_RELATION = 'circledeal';
    
    function onAfterSave(\Sl_Event_Model $event) {
        $model = $event->getModel();
        $model_bu = $event->getModelBeforeUpdate();
        if($model instanceof \Application\Module\Kcompany\Model\Circle) {
            if($model->getId() && !$model_bu->getId()) {
                // При создании круга добавляем создавшего и делаем ответственным
                $currentUser = AclService::getCurrentUser();
                
                if(!$model->issetRelated(self::CIRCLE_USERS_RELATION)) {
                    $model = ModelFactory::mapper($model, self::CIRCLE_USERS_RELATION);
                }
                $users = $model->fetchRelated(self::CIRCLE_USERS_RELATION);
                if(!isset($users[$currentUser->getId()])) {
                    $users[] = $currentUser;
                }
                $model->assignRelated(self::CIRCLE_USERS_RELATION, $users);
                ModelFactory::mapper($model)->save($model);
                
                $essentials = \Sl\Service\Essentials::read($model, self::CIRCLE_USERS_RELATION);
                if(!isset($essentials[$currentUser->getId()])) {
                    $essentials[] = $currentUser;
                }
                \Sl\Service\Essentials::write($model, self::CIRCLE_USERS_RELATION, $essentials);
            }
        } elseif($model instanceof \Application\Module\Kcompany\Model\Department) {
            if($model->getId() && !$model_bu->getId()) {
                // При создании отдела создаем круг
                if(!$model->issetRelated(self::DEPARTMENT_USER_RELATION)) {
                    $model = ModelFactory::mapper($model)->findRelation($model, self::DEPARTMENT_USER_RELATION);
                }
                // Руководитель
                $manager = $model->fetchOneRelated(self::DEPARTMENT_USER_RELATION);
                
                $circle = ModelFactory::model('circle', 'kcompany');
                $circle->setName((string) $model);
                if($manager) {
                    $circle->assignRelated(self::CIRCLE_USERS_RELATION, array($manager));
                }
                $circle->assignRelated(self::DEPARTMENT_CIRCLE_RELATION, array($model));
                $circle = ModelFactory::mapper($circle)->save($circle, true);
                
                if($manager) {
                    $essentials = \Sl\Service\Essentials::read($model, self::CIRCLE_USERS_RELATION);
                    if(!isset($essentials[$manager->getId()])) {
                        $essentials[] = $manager;
                    }
                    \Sl\Service\Essentials::write($circle, self::CIRCLE_USERS_RELATION, $essentials);
                }
            }
        } elseif($model instanceof \Application\Module\Business\Model\Product) {
            if($model->getId() && !$model_bu->getId()) {
                $circle = ModelFactory::model('circle', 'kcompany');
                $circle->setName((string) $model);
                $circle->assignRelated(self::PRODUCT_CIRCLE_RELATION, array($model));
                $circle = ModelFactory::mapper($circle)->save($circle, true);
            }
        } elseif($model instanceof \Application\Module\Business\Model\Deal) {
            if($model->getId() && !$model_bu->getId()) {
                $circle = ModelFactory::model('circle', 'kcompany');
                $circle->setName((string) $model);
                $circle->assignRelated(self::DEAL_CIRCLE_RELATION, array($model));
                $circle = ModelFactory::mapper($circle)->save($circle, true);
            }
        }
    }
    
    public function onAfterInit(\Sl_Event_Router $event) {
        $event->getRouter()->addRoute('kcompany_circles', new \Zend_Controller_Router_Route_Static('circles', array(
            'module' => 'kcompany',
            'controller' => 'circle',
            'action' => 'view',
        )));
    }
    
    public function onBeforeSave(\Sl_Event_Model $event) {}
    public function onRouteStartup(\Sl_Event_Router $event) {}
    public function onBeforeInit(\Sl_Event_Router $event) {}
    public function onDispatchLoopShutdown(\Sl_Event_Router $event) {}
    public function onDispatchLoopStartup(\Sl_Event_Router $event) {}
    public function onGetRequest(\Sl_Event_Router $event) {}
    public function onGetResponse(\Sl_Event_Router $event) {}
    public function onPostDispatch(\Sl_Event_Router $event) {}
    public function onPreDispatch(\Sl_Event_Router $event) {}
    public function onRouteShutdown(\Sl_Event_Router $event) {}
    public function onSetRequest(\Sl_Event_Router $event) {}
    public function onSetResponse(\Sl_Event_Router $event) {}
}
