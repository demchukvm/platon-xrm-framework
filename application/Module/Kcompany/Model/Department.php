<?php
namespace Application\Module\Kcompany\Model;

class Department extends \Sl_Model_Abstract {

	protected $_name;
	protected $_code;
	protected $_description;
	

	public function setName ($name) {
		$this->_name = $name;
		return $this;
	}
	public function setCode ($code) {
		$this->_code = $code;
		return $this;
	}
	public function setDescription ($description) {
		$this->_description = $description;
		return $this;
	}

	public function getName () {
		return $this->_name;
	}
	public function getCode () {
		return $this->_code;
	}
	public function getDescription () {
		return $this->_description;
	}

        public function __toString(){
            
            $strs= array();
            if ($this->_code) {$strs[]= '['.$this->_code.']'; }
            $strs[] = parent::__toString();
            return implode(' ',$strs);
        }

}