<?php
namespace Application\Module\Kcompany\Model\Mapper;

class Department extends \Sl_Model_Mapper_Abstract {
        
        protected $_custom_mandatory_fields = array('modulerelation_departmentmaincircle');
        
        protected function _getMappedDomainName() {
            return '\Application\Module\Kcompany\Model\Department';
        }

        protected function _getMappedRealName() {
            return '\Application\Module\Kcompany\Model\Table\Department';
        }
}