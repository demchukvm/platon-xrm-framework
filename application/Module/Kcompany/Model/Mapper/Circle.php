<?php

namespace Application\Module\Kcompany\Model\Mapper;

class Circle extends \Sl_Model_Mapper_Abstract {

    protected $_mandatory_fields = array('name', 'modulerelation_circleowner', 'modulerelation_departmentmaincircle');

    protected function _getMappedDomainName() {
        return '\Application\Module\Kcompany\Model\Circle';
    }

    protected function _getMappedRealName() {
        return '\Application\Module\Kcompany\Model\Table\Circle';
    }

    public function getRequiredRelations() {
        return array(
            'circleusers',
            'circleowner'
        );
    }
}
