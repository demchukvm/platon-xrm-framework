<?php
namespace Application\Module\Kcompany\Model\Table;

class Department extends \Sl\Model\DbTable\DbTable {
	protected $_name = 'kcompany_department';
	protected $_primary = 'id';

}